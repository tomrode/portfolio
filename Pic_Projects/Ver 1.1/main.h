/**********************************************************************************
PROJECT:		ACE_FU
Module:			Main.h
Description:	Main.c header file
Date:			5/23/06
PROGRAMMER:		Brian Saloka

CPU:			PIC18F8527
CLOCK:			16MHz crystal


*/


//*****************************************************************************
// External memory
//*****************************************************************************




//*****************************************************************************
// Global constants and macros
//*****************************************************************************
#define DISABLE_INTERRUPTS INTCONbits.GIE = 0
#define ENABLE_INTERRUPTS INTCONbits.GIE = 1



//*****************************************************************************
// External function prototypes
//*****************************************************************************
extern char cSerialBuf[10];					// Receive buffer





