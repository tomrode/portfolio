/*****************************************************************************/
/* FILE NAME:  serial_debug.h                                                */
/*****************************************************************************/

#include "typedefs.h"

#ifndef SERIAL_DEBUG
#define SERIAL_DEBUG 

/* Function prototypes*/
extern void serial_data_debug (uint8_t x);

#endif