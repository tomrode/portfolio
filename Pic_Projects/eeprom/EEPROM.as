opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 10920"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
	FNCALL	_main,_eeprom_write_block
	FNCALL	_main,_eeprom_read_block1
	FNCALL	_main,_eep_default
	FNCALL	_eep_default,_eeprom_write
	FNCALL	_eeprom_read_block1,_eeprom_read
	FNCALL	_eeprom_write_block,_eeprom_write
	FNROOT	_main
	global	main@F832
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"C:\pic_projects\eeprom\main.c"
	line	34

;initializer for main@F832
	retlw	052h
	retlw	04Fh
	retlw	044h
	retlw	045h
	retlw	054h
	retlw	041h
	retlw	054h
	retlw	033h
	retlw	031h
	retlw	036h
	retlw	036h
	retlw	037h
	retlw	039h
	retlw	031h
	retlw	033h
	retlw	038h
	global	_eep_navy_personnel_default
psect	stringtext,class=STRCODE,delta=2,reloc=256
global __pstringtext
__pstringtext:
;	global	stringtab,__stringbase
stringtab:
;	String table - string pointers are 2 bytes each
	btfsc	(btemp+1),7
	ljmp	stringcode
	bcf	status,7
	btfsc	(btemp+1),0
	bsf	status,7
	movf	indf,w
	incf fsr
skipnz
incf btemp+1
	return
stringcode:
	movf btemp+1,w
andlw 7Fh
movwf	pclath
	movf	fsr,w
incf fsr
skipnz
incf btemp+1
	movwf pc
__stringbase:
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\eeprom.c"
	line	6
_eep_navy_personnel_default:
	retlw	04Ah
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	048h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	053h
	retlw	0

	retlw	046h
	retlw	0

	retlw	043h
	retlw	0

	retlw	034h
	retlw	0

	retlw	036h
	retlw	0

	retlw	038h
	retlw	0

	retlw	036h
	retlw	0

	retlw	032h
	retlw	0

	retlw	038h
	retlw	0

	retlw	031h
	retlw	0

	retlw	033h
	retlw	0

	retlw	037h
	retlw	0

	retlw	052h
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	044h
	retlw	0

	retlw	045h
	retlw	0

	retlw	054h
	retlw	0

	retlw	041h
	retlw	0

	retlw	054h
	retlw	0

	retlw	033h
	retlw	0

	retlw	036h
	retlw	0

	retlw	036h
	retlw	0

	retlw	030h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	031h
	retlw	0

	retlw	036h
	retlw	0

	retlw	039h
	retlw	0

	retlw	048h
	retlw	0

	retlw	041h
	retlw	0

	retlw	057h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	04Ah
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	032h
	retlw	0

	retlw	037h
	retlw	0

	retlw	038h
	retlw	0

	retlw	032h
	retlw	0

	retlw	039h
	retlw	0

	retlw	035h
	retlw	0

	retlw	034h
	retlw	0

	retlw	033h
	retlw	0

	retlw	036h
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	041h
	retlw	0

	retlw	044h
	retlw	0

	retlw	049h
	retlw	0

	retlw	050h
	retlw	0

	retlw	059h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	036h
	retlw	0

	retlw	039h
	retlw	0

	retlw	037h
	retlw	0

	retlw	032h
	retlw	0

	retlw	039h
	retlw	0

	retlw	032h
	retlw	0

	retlw	030h
	retlw	0

	retlw	030h
	retlw	0

	retlw	034h
	retlw	0

	retlw	04Ah
	retlw	0

	retlw	045h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	053h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	041h
	retlw	0

	retlw	054h
	retlw	0

	retlw	033h
	retlw	0

	retlw	031h
	retlw	0

	retlw	036h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	031h
	retlw	0

	retlw	036h
	retlw	0

	retlw	037h
	retlw	0

	retlw	057h
	retlw	0

	retlw	041h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	04Bh
	retlw	0

	retlw	054h
	retlw	0

	retlw	050h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	039h
	retlw	0

	retlw	036h
	retlw	0

	retlw	036h
	retlw	0

	retlw	037h
	retlw	0

	retlw	032h
	retlw	0

	retlw	030h
	retlw	0

	retlw	038h
	retlw	0

	retlw	031h
	retlw	0

	retlw	032h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	041h
	retlw	0

	retlw	04Eh
	retlw	0

	retlw	054h
	retlw	0

	retlw	054h
	retlw	0

	retlw	042h
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	033h
	retlw	0

	retlw	031h
	retlw	0

	retlw	033h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	037h
	retlw	0

	retlw	036h
	retlw	0

	retlw	035h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	045h
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	04Ah
	retlw	0

	retlw	041h
	retlw	0

	retlw	044h
	retlw	0

	retlw	033h
	retlw	0

	retlw	033h
	retlw	0

	retlw	036h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	036h
	retlw	0

	retlw	036h
	retlw	0

	retlw	037h
	retlw	0

	retlw	057h
	retlw	0

	retlw	049h
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	04Ch
	retlw	0

	retlw	057h
	retlw	0

	retlw	041h
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	034h
	retlw	0

	retlw	031h
	retlw	0

	retlw	036h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	037h
	retlw	0

	retlw	036h
	retlw	0

	retlw	037h
	retlw	0

	retlw	047h
	retlw	0

	retlw	04Fh
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	045h
	retlw	0

	retlw	04Ah
	retlw	0

	retlw	054h
	retlw	0

	retlw	04Dh
	retlw	0

	retlw	032h
	retlw	0

	retlw	038h
	retlw	0

	retlw	035h
	retlw	0

	retlw	034h
	retlw	0

	retlw	032h
	retlw	0

	retlw	033h
	retlw	0

	retlw	035h
	retlw	0

	retlw	036h
	retlw	0

	retlw	033h
	retlw	0

	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	global	_settings
	global	_settings_absaddr
_settings_absaddr	set	0x8
	global	_eep_navy_personnel_default
	global	_settings
	global	_settings_absaddr
_settings_absaddr	set	0x8
psect	_settings_text,class=CODE,delta=2
global __p_settings_text
__p_settings_text:
_settings:
	retlw	01h
	retlw	05h
	retlw	0Ah
	retlw	032h
	retlw	064h
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"EEPROM.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"C:\pic_projects\eeprom\main.c"
	line	34
main@F832:
       ds      16

global btemp
psect inittext,class=CODE,delta=2
global init_fetch,btemp
;	Called with low address in FSR and high address in W
init_fetch:
	movf btemp,w
	movwf pclath
	movf btemp+1,w
	movwf pc
global init_ram
;Called with:
;	high address of idata address in btemp 
;	low address of idata address in btemp+1 
;	low address of data in FSR
;	high address + 1 of data in btemp-1
init_ram:
	fcall init_fetch
	movwf indf,f
	incf fsr,f
	movf fsr,w
	xorwf btemp-1,w
	btfsc status,2
	retlw 0
	incf btemp+1,f
	btfsc status,2
	incf btemp,f
	goto init_ram
; Initialize objects allocated to BANK0
psect cinit,class=CODE,delta=2
global init_ram, __pidataBANK0
	bcf	status, 7	;select IRP bank0
	movlw low(__pdataBANK0+16)
	movwf btemp-1,f
	movlw high(__pidataBANK0)
	movwf btemp,f
	movlw low(__pidataBANK0)
	movwf btemp+1,f
	movlw low(__pdataBANK0)
	movwf fsr,f
	fcall init_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackBANK3,class=BANK3,space=1
global __pcstackBANK3
__pcstackBANK3:
	global	main@new_guy1_array
main@new_guy1_array:	; 16 bytes @ 0x0
	ds	16
	global	main@new_guy1_copy
main@new_guy1_copy:	; 16 bytes @ 0x10
	ds	16
	global	main@my_buffer1
main@my_buffer1:	; 16 bytes @ 0x20
	ds	16
	global	main@ptr3
main@ptr3:	; 1 bytes @ 0x30
	ds	1
	global	main@x
main@x:	; 1 bytes @ 0x31
	ds	1
	global	main@ptr1
main@ptr1:	; 1 bytes @ 0x32
	ds	1
	global	main@ptr2
main@ptr2:	; 1 bytes @ 0x33
	ds	1
	global	main@ptr_new_guy1
main@ptr_new_guy1:	; 1 bytes @ 0x34
	ds	1
	global	main@new_guy2
main@new_guy2:	; 15 bytes @ 0x35
	ds	15
	global	main@new_guy1
main@new_guy1:	; 16 bytes @ 0x44
	ds	16
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_eep_default
?_eep_default:	; 0 bytes @ 0x0
	global	??_eeprom_read
??_eeprom_read:	; 0 bytes @ 0x0
	global	?_eeprom_write
?_eeprom_write:	; 1 bytes @ 0x0
	global	?_eeprom_read
?_eeprom_read:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	global	eeprom_write@value
eeprom_write@value:	; 1 bytes @ 0x0
	ds	1
	global	??_eeprom_write
??_eeprom_write:	; 0 bytes @ 0x1
	global	eeprom_read@addr
eeprom_read@addr:	; 1 bytes @ 0x1
	ds	1
	global	?_eeprom_read_block1
?_eeprom_read_block1:	; 0 bytes @ 0x2
	global	eeprom_read_block1@size
eeprom_read_block1@size:	; 1 bytes @ 0x2
	global	eeprom_write@addr
eeprom_write@addr:	; 1 bytes @ 0x2
	ds	1
	global	?_eeprom_write_block
?_eeprom_write_block:	; 0 bytes @ 0x3
	global	??_eep_default
??_eep_default:	; 0 bytes @ 0x3
	global	eeprom_write_block@size
eeprom_write_block@size:	; 1 bytes @ 0x3
	global	eeprom_read_block1@buffer
eeprom_read_block1@buffer:	; 1 bytes @ 0x3
	ds	1
	global	??_eeprom_read_block1
??_eeprom_read_block1:	; 0 bytes @ 0x4
	global	eeprom_write_block@buffer
eeprom_write_block@buffer:	; 1 bytes @ 0x4
	ds	1
	global	??_eeprom_write_block
??_eeprom_write_block:	; 0 bytes @ 0x5
	global	eeprom_read_block1@address
eeprom_read_block1@address:	; 1 bytes @ 0x5
	ds	1
	global	eep_default@i
eep_default@i:	; 1 bytes @ 0x6
	global	eeprom_write_block@i
eeprom_write_block@i:	; 1 bytes @ 0x6
	global	eeprom_read_block1@i
eeprom_read_block1@i:	; 1 bytes @ 0x6
	ds	1
	global	eeprom_write_block@address
eeprom_write_block@address:	; 1 bytes @ 0x7
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x8
	ds	4
;;Data sizes: Strings 0, constant 512, data 16, bss 0, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     12      12
;; BANK0           80      0      16
;; BANK1           80      0       0
;; BANK3           96     84      84
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; eeprom_read_block1@buffer	PTR unsigned char  size(1) Largest target is 16
;;		 -> main@my_buffer1(BANK3[16]), 
;;
;; sp__eeprom_read_block	PTR unsigned char  size(1) Largest target is 16
;;		 -> eeprom_read_block@holding_buffer(BANK0[16]), 
;;
;; eeprom_write_block@buffer	PTR unsigned char  size(1) Largest target is 16
;;		 -> main@new_guy2(BANK3[15]), main@new_guy1(BANK3[16]), 
;;
;; main@ptr_new_guy1	PTR struct . size(1) Largest target is 16
;;		 -> main@new_guy1(BANK3[16]), 
;;
;; main@ptr3	PTR PTR PTR unsigned char  size(1) Largest target is 2
;;		 -> main@ptr2(BANK3[1]), 
;;
;; main@ptr2	PTR PTR unsigned char  size(1) Largest target is 2
;;		 -> main@ptr1(BANK3[1]), 
;;
;; main@ptr1	PTR unsigned char  size(1) Largest target is 1
;;		 -> main@x(BANK3[1]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_eeprom_write_block
;;   _eep_default->_eeprom_write
;;   _eeprom_read_block1->_eeprom_read
;;   _eeprom_write_block->_eeprom_write
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 4, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                               107   107      0    1336
;;                                              8 COMMON     4     4      0
;;                                              0 BANK3     84    84      0
;;                 _eeprom_write_block
;;                 _eeprom_read_block1
;;                        _eep_default
;; ---------------------------------------------------------------------------------
;; (1) _eep_default                                          5     5      0     140
;;                                              3 COMMON     4     4      0
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (1) _eeprom_read_block1                                   5     3      2     136
;;                                              2 COMMON     5     3      2
;;                        _eeprom_read
;; ---------------------------------------------------------------------------------
;; (1) _eeprom_write_block                                   5     3      2     186
;;                                              3 COMMON     5     3      2
;;                       _eeprom_write
;; ---------------------------------------------------------------------------------
;; (2) _eeprom_read                                          2     2      0      22
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; (2) _eeprom_write                                         3     2      1      50
;;                                              0 COMMON     3     2      1
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _eeprom_write_block
;;     _eeprom_write
;;   _eeprom_read_block1
;;     _eeprom_read
;;   _eep_default
;;     _eeprom_write
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60     54      54       9       87.5%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      72      12        0.0%
;;ABS                  0      0      70       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       2       2        0.0%
;;BANK0               50      0      10       5       20.0%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      C       C       1       85.7%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 20 in file "C:\pic_projects\eeprom\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  new_guy1       16   68[BANK3 ] struct .
;;  my_buffer1     16   32[BANK3 ] struct .
;;  new_guy1_cop   16   16[BANK3 ] struct .
;;  new_guy1_arr   16    0[BANK3 ] unsigned char [16]
;;  eeprom_read_   16    0        unsigned char [16]
;;  new_guy2       15   53[BANK3 ] struct .
;;  ptr_new_guy1    1   52[BANK3 ] PTR struct .
;;		 -> main@new_guy1(16), 
;;  ptr2            1   51[BANK3 ] PTR PTR unsigned char 
;;		 -> main@ptr1(1), 
;;  ptr1            1   50[BANK3 ] PTR unsigned char 
;;		 -> main@x(1), 
;;  x               1   49[BANK3 ] unsigned char 
;;  ptr3            1   48[BANK3 ] PTR PTR PTR unsigned cha
;;		 -> main@ptr2(1), 
;;  base_size       1    0        unsigned char 
;;  base_addr       1    0        unsigned char 
;;  i               1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  2  842[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0      84       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0      84       0
;;Total ram usage:       88 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_eeprom_write_block
;;		_eeprom_read_block1
;;		_eep_default
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\eeprom\main.c"
	line	20
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 6
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	34
	
l5989:	
;main.c: 22: NAVY_PERSONEL new_guy1;
;main.c: 23: NAVY_PERSONEL new_guy1_copy;
;main.c: 24: STRUCT_UNION new_guy2;
;main.c: 25: NAVY_PERSONEL my_buffer1;
;main.c: 28: uint8_t i;
;main.c: 29: uint8_t base_addr;
;main.c: 30: uint8_t base_size;
;main.c: 34: uint8_t new_guy1_array[] = {'R','O','D','E','T','A','T','3','1','6','6','7','9','1','3','8'};
	movlw	(main@new_guy1_array)&0ffh
	movwf	fsr0
	movlw	low(main@F832)
	movwf	(??_main+0)+0
	movf	fsr0,w
	movwf	((??_main+0)+0+1)
	movlw	16
	movwf	((??_main+0)+0+2)
u2530:
	movf	(??_main+0)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_main+0)+0+3)
	incf	(??_main+0)+0,f
	movf	((??_main+0)+0+1),w
	movwf	fsr0
	bsf	status, 7	;select IRP bank3
	
	movf	((??_main+0)+0+3),w
	movwf	indf
	incf	((??_main+0)+0+1),f
	decfsz	((??_main+0)+0+2),f
	goto	u2530
	line	43
	
l5991:	
;main.c: 35: uint8_t eeprom_read_ar[16];
;main.c: 38: uint8_t x;
;main.c: 39: uint8_t * ptr1;
;main.c: 40: uint8_t * * ptr2;
;main.c: 41: uint8_t * * * ptr3;
;main.c: 43: ptr1 = &x;
	movlw	(main@x)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	movwf	(main@ptr1)^0180h
	line	44
	
l5993:	
;main.c: 44: ptr2 = &ptr1;
	movlw	(main@ptr1)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@ptr2)^0180h
	line	45
	
l5995:	
;main.c: 45: ptr3 = &ptr2;
	movlw	(main@ptr2)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@ptr3)^0180h
	line	47
	
l5997:	
;main.c: 47: *(*(*ptr3)) = 0x55;
	movlw	(055h)
	movwf	(??_main+0)+0
	movf	(main@ptr3)^0180h,w
	movwf	fsr0
	movf	indf,w
	movwf	fsr0
	movf	indf,w
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	57
;main.c: 50: NAVY_PERSONEL *ptr_new_guy1;
;main.c: 57: new_guy1.name[0] = 'J';
	movlw	(04Ah)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@new_guy1)^0180h
	line	58
;main.c: 58: new_guy1.name[1] = 'A';
	movlw	(041h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+01h
	line	59
;main.c: 59: new_guy1.name[2] = 'M';
	movlw	(04Dh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+02h
	line	60
;main.c: 60: new_guy1.name[3] = 'E';
	movlw	(045h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+03h
	line	61
;main.c: 61: new_guy1.name[4] = 'B';
	movlw	(042h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+04h
	line	62
;main.c: 62: new_guy1.rate[0] = 'A';
	movlw	(041h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+05h
	line	63
;main.c: 63: new_guy1.rate[1] = 'K';
	movlw	(04Bh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+06h
	line	64
;main.c: 64: new_guy1.social_security_num[0] = '0';
	movlw	(030h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+07h
	line	65
;main.c: 65: new_guy1.social_security_num[1] = '8';
	movlw	(038h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+08h
	line	66
;main.c: 66: new_guy1.social_security_num[2] = '1';
	movlw	(031h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+09h
	line	67
;main.c: 67: new_guy1.social_security_num[3] = '2';
	movlw	(032h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+0Ah
	line	68
;main.c: 68: new_guy1.social_security_num[4] = '5';
	movlw	(035h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+0Bh
	line	69
;main.c: 69: new_guy1.social_security_num[5] = '6';
	movlw	(036h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+0Ch
	line	70
;main.c: 70: new_guy1.social_security_num[6] = '7';
	movlw	(037h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+0Dh
	line	71
;main.c: 71: new_guy1.social_security_num[7] = '9';
	movlw	(039h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+0Eh
	line	72
;main.c: 72: new_guy1.social_security_num[8] = '2';
	movlw	(032h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@new_guy1)^0180h+0Fh
	line	75
	
l5999:	
;main.c: 75: new_guy1_copy = new_guy1;
	movlw	(main@new_guy1_copy)&0ffh
	movwf	fsr0
	movlw	low(main@new_guy1)
	movwf	(??_main+0)+0
	movf	fsr0,w
	movwf	((??_main+0)+0+1)
	movlw	16
	movwf	((??_main+0)+0+2)
u2540:
	movf	(??_main+0)+0,w
	movwf	fsr0
	bsf	status, 7	;select IRP bank3
	
	movf	indf,w
	movwf	((??_main+0)+0+3)
	incf	(??_main+0)+0,f
	movf	((??_main+0)+0+1),w
	movwf	fsr0
	
	movf	((??_main+0)+0+3),w
	movwf	indf
	incf	((??_main+0)+0+1),f
	decfsz	((??_main+0)+0+2),f
	goto	u2540
	line	77
	
l6001:	
;main.c: 77: ptr_new_guy1 = &new_guy1;
	movlw	(main@new_guy1)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@ptr_new_guy1)^0180h
	line	78
;main.c: 78: ptr_new_guy1 ->name[0] = 'T';
	movlw	(054h)
	movwf	(??_main+0)+0
	movf	(main@ptr_new_guy1)^0180h,w
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	79
;main.c: 79: ptr_new_guy1 ->name[1] = 'O';
	movlw	(04Fh)
	movwf	(??_main+0)+0
	movf	(main@ptr_new_guy1)^0180h,w
	addlw	01h
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	80
;main.c: 80: ptr_new_guy1 ->name[2] = 'M';
	movlw	(04Dh)
	movwf	(??_main+0)+0
	movf	(main@ptr_new_guy1)^0180h,w
	addlw	02h
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	81
;main.c: 81: ptr_new_guy1 ->name[3] = 'S';
	movlw	(053h)
	movwf	(??_main+0)+0
	movf	(main@ptr_new_guy1)^0180h,w
	addlw	03h
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	83
;main.c: 83: ptr_new_guy1 ->rate[0] = 'P';
	movlw	(050h)
	movwf	(??_main+0)+0
	movf	(main@ptr_new_guy1)^0180h,w
	addlw	05h
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	84
;main.c: 84: ptr_new_guy1 ->rate[1] = 'M';
	movlw	(04Dh)
	movwf	(??_main+0)+0
	movf	(main@ptr_new_guy1)^0180h,w
	addlw	06h
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	86
;main.c: 86: ptr_new_guy1->social_security_num[0]= 2;
	movlw	(02h)
	movwf	(??_main+0)+0
	movf	(main@ptr_new_guy1)^0180h,w
	addlw	07h
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	90
;main.c: 90: new_guy1_copy = new_guy1;
	movlw	(main@new_guy1_copy)&0ffh
	movwf	fsr0
	movlw	low(main@new_guy1)
	movwf	(??_main+0)+0
	movf	fsr0,w
	movwf	((??_main+0)+0+1)
	movlw	16
	movwf	((??_main+0)+0+2)
u2550:
	movf	(??_main+0)+0,w
	movwf	fsr0
	bsf	status, 7	;select IRP bank3
	
	movf	indf,w
	movwf	((??_main+0)+0+3)
	incf	(??_main+0)+0,f
	movf	((??_main+0)+0+1),w
	movwf	fsr0
	
	movf	((??_main+0)+0+3),w
	movwf	indf
	incf	((??_main+0)+0+1),f
	decfsz	((??_main+0)+0+2),f
	goto	u2550
	line	94
	
l6003:	
;main.c: 94: new_guy2.struct_member1[0] = 'H';
	movlw	low(048h)
	movwf	(main@new_guy2)^0180h
	movlw	high(048h)
	movwf	((main@new_guy2)^0180h)+1
	line	95
	
l6005:	
;main.c: 95: new_guy2.struct_member1[1] = 'E';
	movlw	low(045h)
	movwf	0+(main@new_guy2)^0180h+02h
	movlw	high(045h)
	movwf	(0+(main@new_guy2)^0180h+02h)+1
	line	96
	
l6007:	
;main.c: 96: new_guy2.struct_member1[2] = 'L';
	movlw	low(04Ch)
	movwf	0+(main@new_guy2)^0180h+04h
	movlw	high(04Ch)
	movwf	(0+(main@new_guy2)^0180h+04h)+1
	line	97
	
l6009:	
;main.c: 97: new_guy2.struct_member1[3] = 'L';
	movlw	low(04Ch)
	movwf	0+(main@new_guy2)^0180h+06h
	movlw	high(04Ch)
	movwf	(0+(main@new_guy2)^0180h+06h)+1
	line	98
	
l6011:	
;main.c: 98: new_guy2.struct_member1[4] = 'O';
	movlw	low(04Fh)
	movwf	0+(main@new_guy2)^0180h+08h
	movlw	high(04Fh)
	movwf	(0+(main@new_guy2)^0180h+08h)+1
	line	99
	
l6013:	
;main.c: 99: new_guy2.UNION_PART.union_member2 = 0xBEEF;
	movlw	low(0BEEFh)
	movwf	0+(main@new_guy2)^0180h+0Dh
	movlw	high(0BEEFh)
	movwf	(0+(main@new_guy2)^0180h+0Dh)+1
	line	103
	
l6015:	
;main.c: 103: eeprom_write_block(0xE0, sizeof(new_guy1), &new_guy2.struct_member1[0]);
	movlw	(010h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_eeprom_write_block)
	movlw	(main@new_guy2)&0ffh
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	0+(?_eeprom_write_block)+01h
	movlw	(0E0h)
	fcall	_eeprom_write_block
	line	108
	
l6017:	
;main.c: 108: eeprom_write_block(0xD0, sizeof(new_guy1), &new_guy1.name[0]);
	movlw	(010h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_eeprom_write_block)
	movlw	(main@new_guy1)&0ffh
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	0+(?_eeprom_write_block)+01h
	movlw	(0D0h)
	fcall	_eeprom_write_block
	line	112
	
l6019:	
;main.c: 112: eeprom_read_block1(0x50, 16, &my_buffer1.name[0]);
	movlw	(010h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_eeprom_read_block1)
	movlw	(main@my_buffer1)&0ffh
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	0+(?_eeprom_read_block1)+01h
	movlw	(050h)
	fcall	_eeprom_read_block1
	line	125
	
l6021:	
;main.c: 125: (GIE = 0);
	bcf	(95/8),(95)&7
	line	126
	
l6023:	
# 126 "C:\pic_projects\eeprom\main.c"
nop ;#
psect	maintext
	line	127
	
l6025:	
;main.c: 127: eep_default();
	fcall	_eep_default
	line	128
	
l6027:	
# 128 "C:\pic_projects\eeprom\main.c"
nop ;#
psect	maintext
	goto	l6029
	line	132
;main.c: 132: while(1)
	
l845:	
	line	134
	
l6029:	
# 134 "C:\pic_projects\eeprom\main.c"
nop ;#
psect	maintext
	goto	l6029
	line	135
	
l846:	
	line	132
	goto	l6029
	
l847:	
	line	136
	
l848:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_eep_default
psect	text179,local,class=CODE,delta=2
global __ptext179
__ptext179:

;; *************** function _eep_default *****************
;; Defined at:
;;		line 23 in file "C:\pic_projects\eeprom\eeprom.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    6[COMMON] unsigned char 
;;  const_byte_a    1    0        unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          3       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text179
	file	"C:\pic_projects\eeprom\eeprom.c"
	line	23
	global	__size_of_eep_default
	__size_of_eep_default	equ	__end_of_eep_default-_eep_default
	
_eep_default:	
	opt	stack 6
; Regs used in _eep_default: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	27
	
l5977:	
;eeprom.c: 24: uint8_t const_byte_addr;
;eeprom.c: 25: uint8_t i;
;eeprom.c: 27: for(i=0;i<=160;i++)
	clrf	(eep_default@i)
	
l5979:	
	movlw	(0A1h)
	subwf	(eep_default@i),w
	skipc
	goto	u2501
	goto	u2500
u2501:
	goto	l5983
u2500:
	goto	l1691
	
l5981:	
	goto	l1691
	line	28
	
l1689:	
	line	29
	
l5983:	
;eeprom.c: 28: {
;eeprom.c: 29: eeprom_write(i,eep_navy_personnel_default[i]);
	movf	(eep_default@i),w
	movwf	(??_eep_default+0)+0
	clrf	(??_eep_default+0)+0+1
	movlw	01h
	movwf	btemp+1
u2515:
	clrc
	rlf	(??_eep_default+0)+0,f
	rlf	(??_eep_default+0)+1,f
	decfsz	btemp+1,f
	goto	u2515
	movlw	high(_eep_navy_personnel_default|8000h)
	addwf	1+(??_eep_default+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movlw	low(_eep_navy_personnel_default|8000h)
	addwf	0+(??_eep_default+0)+0,w
	movwf	fsr0
	skipnc
	incf	btemp+1,f
	fcall	stringtab
	movwf	(??_eep_default+2)+0
	movf	(??_eep_default+2)+0,w
	movwf	(?_eeprom_write)
	movf	(eep_default@i),w
	fcall	_eeprom_write
	line	27
	
l5985:	
	movlw	(01h)
	movwf	(??_eep_default+0)+0
	movf	(??_eep_default+0)+0,w
	addwf	(eep_default@i),f
	
l5987:	
	movlw	(0A1h)
	subwf	(eep_default@i),w
	skipc
	goto	u2521
	goto	u2520
u2521:
	goto	l5983
u2520:
	goto	l1691
	
l1690:	
	line	33
	
l1691:	
	return
	opt stack 0
GLOBAL	__end_of_eep_default
	__end_of_eep_default:
;; =============== function _eep_default ends ============

	signat	_eep_default,88
	global	_eeprom_read_block1
psect	text180,local,class=CODE,delta=2
global __ptext180
__ptext180:

;; *************** function _eeprom_read_block1 *****************
;; Defined at:
;;		line 124 in file "C:\pic_projects\eeprom\eeprom.c"
;; Parameters:    Size  Location     Type
;;  address         1    wreg     unsigned char 
;;  size            1    2[COMMON] unsigned char 
;;  buffer          1    3[COMMON] PTR unsigned char 
;;		 -> main@my_buffer1(16), 
;; Auto vars:     Size  Location     Type
;;  address         1    5[COMMON] unsigned char 
;;  i               1    6[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         2       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         5       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_eeprom_read
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text180
	file	"C:\pic_projects\eeprom\eeprom.c"
	line	124
	global	__size_of_eeprom_read_block1
	__size_of_eeprom_read_block1	equ	__end_of_eeprom_read_block1-_eeprom_read_block1
	
_eeprom_read_block1:	
	opt	stack 6
; Regs used in _eeprom_read_block1: [wreg-fsr0h+status,2+status,0+pclath+cstack]
;eeprom_read_block1@address stored from wreg
	line	128
	movwf	(eeprom_read_block1@address)
	
l5963:	
;eeprom.c: 125: unsigned char i;
;eeprom.c: 128: if (size != 0)
	movf	(eeprom_read_block1@size),w
	skipz
	goto	u2480
	goto	l1719
u2480:
	line	130
	
l5965:	
;eeprom.c: 129: {
;eeprom.c: 130: for (i = 0; i < size; i++)
	clrf	(eeprom_read_block1@i)
	goto	l5975
	line	131
	
l1717:	
	line	134
	
l5967:	
;eeprom.c: 131: {
;eeprom.c: 134: *buffer = (unsigned char)(eeprom_read(address));
	movf	(eeprom_read_block1@address),w
	fcall	_eeprom_read
	movwf	(??_eeprom_read_block1+0)+0
	movf	(eeprom_read_block1@buffer),w
	movwf	fsr0
	movf	(??_eeprom_read_block1+0)+0,w
	bsf	status, 7	;select IRP bank2
	movwf	indf
	line	136
	
l5969:	
;eeprom.c: 136: buffer++;
	movlw	(01h)
	movwf	(??_eeprom_read_block1+0)+0
	movf	(??_eeprom_read_block1+0)+0,w
	addwf	(eeprom_read_block1@buffer),f
	line	138
	
l5971:	
;eeprom.c: 138: address++;
	movlw	(01h)
	movwf	(??_eeprom_read_block1+0)+0
	movf	(??_eeprom_read_block1+0)+0,w
	addwf	(eeprom_read_block1@address),f
	line	130
	
l5973:	
	movlw	(01h)
	movwf	(??_eeprom_read_block1+0)+0
	movf	(??_eeprom_read_block1+0)+0,w
	addwf	(eeprom_read_block1@i),f
	goto	l5975
	
l1716:	
	
l5975:	
	movf	(eeprom_read_block1@size),w
	subwf	(eeprom_read_block1@i),w
	skipc
	goto	u2491
	goto	u2490
u2491:
	goto	l5967
u2490:
	goto	l1719
	
l1718:	
	goto	l1719
	line	140
	
l1715:	
	line	141
	
l1719:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_read_block1
	__end_of_eeprom_read_block1:
;; =============== function _eeprom_read_block1 ends ============

	signat	_eeprom_read_block1,12408
	global	_eeprom_write_block
psect	text181,local,class=CODE,delta=2
global __ptext181
__ptext181:

;; *************** function _eeprom_write_block *****************
;; Defined at:
;;		line 62 in file "C:\pic_projects\eeprom\eeprom.c"
;; Parameters:    Size  Location     Type
;;  address         1    wreg     unsigned char 
;;  size            1    3[COMMON] unsigned char 
;;  buffer          1    4[COMMON] PTR unsigned char 
;;		 -> main@new_guy2(15), main@new_guy1(16), 
;; Auto vars:     Size  Location     Type
;;  address         1    7[COMMON] unsigned char 
;;  i               1    6[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         2       0       0       0       0
;;      Locals:         2       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         5       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_eeprom_write
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text181
	file	"C:\pic_projects\eeprom\eeprom.c"
	line	62
	global	__size_of_eeprom_write_block
	__size_of_eeprom_write_block	equ	__end_of_eeprom_write_block-_eeprom_write_block
	
_eeprom_write_block:	
	opt	stack 6
; Regs used in _eeprom_write_block: [wreg-fsr0h+status,2+status,0+pclath+cstack]
;eeprom_write_block@address stored from wreg
	line	66
	movwf	(eeprom_write_block@address)
	
l5949:	
;eeprom.c: 64: uint8_t i;
;eeprom.c: 66: if (size <= (16))
	movlw	(011h)
	subwf	(eeprom_write_block@size),w
	skipnc
	goto	u2461
	goto	u2460
u2461:
	goto	l1704
u2460:
	line	68
	
l5951:	
;eeprom.c: 67: {
;eeprom.c: 68: for(i=0;i<=size;i++)
	clrf	(eeprom_write_block@i)
	goto	l5961
	line	69
	
l1701:	
	line	70
	
l5953:	
;eeprom.c: 69: {
;eeprom.c: 70: eeprom_write(address, * buffer);
	movf	(eeprom_write_block@buffer),w
	movwf	fsr0
	bsf	status, 7	;select IRP bank2
	movf	indf,w
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	movwf	(?_eeprom_write)
	movf	(eeprom_write_block@address),w
	fcall	_eeprom_write
	line	71
	
l5955:	
;eeprom.c: 71: buffer++;
	movlw	(01h)
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	addwf	(eeprom_write_block@buffer),f
	line	72
	
l5957:	
;eeprom.c: 72: address = address + 1;
	movf	(eeprom_write_block@address),w
	addlw	01h
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	movwf	(eeprom_write_block@address)
	line	68
	
l5959:	
	movlw	(01h)
	movwf	(??_eeprom_write_block+0)+0
	movf	(??_eeprom_write_block+0)+0,w
	addwf	(eeprom_write_block@i),f
	goto	l5961
	
l1700:	
	
l5961:	
	movf	(eeprom_write_block@i),w
	subwf	(eeprom_write_block@size),w
	skipnc
	goto	u2471
	goto	u2470
u2471:
	goto	l5953
u2470:
	goto	l1704
	
l1702:	
	line	74
;eeprom.c: 73: }
;eeprom.c: 74: }
	goto	l1704
	line	75
	
l1699:	
	goto	l1704
	line	78
;eeprom.c: 75: else
;eeprom.c: 76: {
	
l1703:	
	line	79
	
l1704:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_write_block
	__end_of_eeprom_write_block:
;; =============== function _eeprom_write_block ends ============

	signat	_eeprom_write_block,12408
	global	_eeprom_read
psect	text182,local,class=CODE,delta=2
global __ptext182
__ptext182:

;; *************** function _eeprom_read *****************
;; Defined at:
;;		line 7 in file "eeread.c"
;; Parameters:    Size  Location     Type
;;  addr            1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  addr            1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_eeprom_read_block1
;;		_eeprom_read_block
;; This function uses a non-reentrant model
;;
psect	text182
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\eeread.c"
	line	7
	global	__size_of_eeprom_read
	__size_of_eeprom_read	equ	__end_of_eeprom_read-_eeprom_read
	
_eeprom_read:	
	opt	stack 6
; Regs used in _eeprom_read: [wreg+status,2+status,0]
;eeprom_read@addr stored from wreg
	line	9
	movwf	(eeprom_read@addr)
	line	8
	
l2640:	
	line	9
# 9 "C:\Program Files\HI-TECH Software\PICC\9.83\sources\eeread.c"
clrwdt ;#
psect	text182
	line	10
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3169/8)^0180h,(3169)&7
	goto	u2451
	goto	u2450
u2451:
	goto	l2640
u2450:
	goto	l5945
	
l2641:	
	line	11
	
l5945:	
	movf	(eeprom_read@addr),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(269)^0100h	;volatile
	movlw	(03Fh)
	movwf	(??_eeprom_read+0)+0
	movf	(??_eeprom_read+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(396)^0180h,f	;volatile
	bsf	(3168/8)^0180h,(3168)&7
	clrc
	btfsc	(3168/8)^0180h,(3168)&7
	setc
	movlw	0
	skipnc
	movlw	1

	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movf	(268)^0100h,w	;volatile
	goto	l2642
	
l5947:	
	line	12
	
l2642:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_read
	__end_of_eeprom_read:
;; =============== function _eeprom_read ends ============

	signat	_eeprom_read,4217
	global	_eeprom_write
psect	text183,local,class=CODE,delta=2
global __ptext183
__ptext183:

;; *************** function _eeprom_write *****************
;; Defined at:
;;		line 7 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\eewrite.c"
;; Parameters:    Size  Location     Type
;;  addr            1    wreg     unsigned char 
;;  value           1    0[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  addr            1    2[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         1       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         3       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_eep_default
;;		_eeprom_write_block
;; This function uses a non-reentrant model
;;
psect	text183
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\eewrite.c"
	line	7
	global	__size_of_eeprom_write
	__size_of_eeprom_write	equ	__end_of_eeprom_write-_eeprom_write
	
_eeprom_write:	
	opt	stack 6
; Regs used in _eeprom_write: [wreg+status,2+status,0]
;eeprom_write@addr stored from wreg
	movwf	(eeprom_write@addr)
	line	8
	
l3563:	
	goto	l3564
	
l3565:	
	
l3564:	
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3169/8)^0180h,(3169)&7
	goto	u2401
	goto	u2400
u2401:
	goto	l3564
u2400:
	goto	l5913
	
l3566:	
	
l5913:	
	movf	(eeprom_write@addr),w
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(269)^0100h	;volatile
	movf	(eeprom_write@value),w
	movwf	(268)^0100h	;volatile
	
l5915:	
	movlw	(03Fh)
	movwf	(??_eeprom_write+0)+0
	movf	(??_eeprom_write+0)+0,w
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	andwf	(396)^0180h,f	;volatile
	
l5917:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(24/8),(24)&7
	
l5919:	
	btfss	(95/8),(95)&7
	goto	u2411
	goto	u2410
u2411:
	goto	l3567
u2410:
	
l5921:	
	bsf	(24/8),(24)&7
	
l3567:	
	bcf	(95/8),(95)&7
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	bsf	(3170/8)^0180h,(3170)&7
	
l5923:	
	movlw	(055h)
	movwf	(397)^0180h	;volatile
	movlw	(0AAh)
	movwf	(397)^0180h	;volatile
	
l5925:	
	bsf	(3169/8)^0180h,(3169)&7
	
l5927:	
	bcf	(3170/8)^0180h,(3170)&7
	
l5929:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(24/8),(24)&7
	goto	u2421
	goto	u2420
u2421:
	goto	l3570
u2420:
	
l5931:	
	bsf	(95/8),(95)&7
	goto	l3570
	
l3568:	
	goto	l3570
	
l3569:	
	line	10
;	Return value of _eeprom_write is never used
	
l3570:	
	return
	opt stack 0
GLOBAL	__end_of_eeprom_write
	__end_of_eeprom_write:
;; =============== function _eeprom_write ends ============

	signat	_eeprom_write,8313
psect	text184,local,class=CODE,delta=2
global __ptext184
__ptext184:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
