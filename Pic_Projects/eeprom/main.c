
#include "htc.h"
#include "typedefs.h"
#include "eeprom.h"
#define EEPROM_BLOCK_MAX_SIZE (16)
/*PIC16F887*/
//__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
//__CONFIG(BORV40);

/* Function prototypes*/


/* non writeable ram variable location*/
/* Volatile variable*/
const uint8_t settings[5] @ 0x008 = { 1, 5, 10, 50, 100 };
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main (void)
{
 /* structures*/
NAVY_PERSONEL new_guy1;
NAVY_PERSONEL new_guy1_copy;
STRUCT_UNION new_guy2;
NAVY_PERSONEL my_buffer1; 

/* variables*/
uint8_t i;
uint8_t base_addr;
uint8_t base_size;


 /* arrays*/
 uint8_t new_guy1_array[] = {'R','O','D','E','T','A','T','3','1','6','6','7','9','1','3','8'};
 uint8_t eeprom_read_ar[16];
 
 /*pointers*/
uint8_t x;
uint8_t * ptr1;
uint8_t * * ptr2;
uint8_t * * * ptr3;

  ptr1 = &x;
  ptr2 = &ptr1;
  ptr3 = &ptr2;

  *(*(*ptr3)) = 0x55;


NAVY_PERSONEL *ptr_new_guy1;

//ptr_new_guy1 = &new_guy1;         /* Add the address of new guy*/
//ptr_new_guy1->name[0] = 'T';
//ptr_new_guy1->rate = 2;//{'H','T');
//ptr_new_guy1->social_security_num = 2;//{'3','1','6','6','7','9','6','6','7'};

 new_guy1.name[0] = 'J';
 new_guy1.name[1] = 'A';
 new_guy1.name[2] = 'M';
 new_guy1.name[3] = 'E';
 new_guy1.name[4] = 'B';
 new_guy1.rate[0] = 'A';
 new_guy1.rate[1] = 'K';
 new_guy1.social_security_num[0] = '0';
 new_guy1.social_security_num[1] = '8';
 new_guy1.social_security_num[2] = '1';
 new_guy1.social_security_num[3] = '2';
 new_guy1.social_security_num[4] = '5';
 new_guy1.social_security_num[5] = '6';
 new_guy1.social_security_num[6] = '7';
 new_guy1.social_security_num[7] = '9';
 new_guy1.social_security_num[8] = '2';
 
/*copy contents of structure over*/
 new_guy1_copy = new_guy1;

 ptr_new_guy1 = &new_guy1;         /* Add the new guy*/
 ptr_new_guy1 ->name[0] = 'T';
 ptr_new_guy1 ->name[1] = 'O';
 ptr_new_guy1 ->name[2] = 'M';
 ptr_new_guy1 ->name[3] = 'S';
 
 ptr_new_guy1 ->rate[0] = 'P';
 ptr_new_guy1 ->rate[1] = 'M';

 ptr_new_guy1->social_security_num[0]= 2;


/*copy contents of structure over*/
 new_guy1_copy = new_guy1;


/* EXAMPLE OF THE STRUCTURE and UNION*/
new_guy2.struct_member1[0] = 'H';
new_guy2.struct_member1[1] = 'E';
new_guy2.struct_member1[2] = 'L';
new_guy2.struct_member1[3] = 'L';
new_guy2.struct_member1[4] = 'O';
new_guy2.UNION_PART.union_member2 = 0xBEEF;

/* eeprom writing and reading section */

 eeprom_write_block(0xE0, sizeof(new_guy1), &new_guy2.struct_member1[0]);

 //eep_write_add_new(&new_guy1_array[0]);                        /* this is a linear array*/
 //eep_write_add_new(&new_guy1.name[0]);                       /* this is a stucture*/

 eeprom_write_block(0xD0, sizeof(new_guy1), &new_guy1.name[0]); 
 //eeprom_write_block(0x60,16,&new_guy1.name[0]);
  

 eeprom_read_block1(0x50, 16, &my_buffer1.name[0]);
 /* This works but not very efficient*/
 //  base_addr = 0x60;
 //  base_size = 16;
 //for(i=0;i<=EEPROM_BLOCK_MAX_SIZE;i++)
 //{
 //   eeprom_read_ar[i] =*eeprom_read_block(base_addr,base_size);                 /* read something back from eeprom*/ 
 //   base_addr += 1;                                                                                    
 //   base_size -= 1;
 //}


 /* Initializations*/
  di(); 
  asm("nop");                                                 /* dis_able all interrupts*/
  eep_default();                                             /* Load in default from flash*/
  asm("nop");
  //while (HTS ==0)
  //{}                                            /* wait until clock stable*/ 

  while(1)
  {
    asm("nop");
  }
}





