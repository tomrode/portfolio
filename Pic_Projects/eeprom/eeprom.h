/*****************************************************************************/
/* FILE NAME:  eeprom.h                                                    */
/*                                                                           */
/* Description:  eeprom functionality.                                  */
/*****************************************************************************/

#include "htc.h"
#include "typedefs.h"
/* Structures*/
 typedef struct{
                     uint8_t name[5];                /* limit to 5 bytes*/
                     uint8_t rate[2];                /* limit to 2 bytes*/
                     uint8_t social_security_num[9]; /* 9 bytes*/
               }NAVY_PERSONEL; 

typedef struct
              {
                 uint16_t struct_member1[6];
                 uint8_t struct_member2;
                 union
                      {   
                         uint8_t  union_member1;        
                         uint16_t union_member2;
                      }UNION_PART;
              }STRUCT_UNION; 
 
/* Function prototypes*/
void eep_default(void);
void eep_write_add_new(uint8_t *new_guy);
void eeprom_write_block(unsigned char address, unsigned char size, unsigned char * buffer); 
unsigned char *eeprom_read_block(unsigned char address, unsigned char size);
void eeprom_read_block1(unsigned char address, unsigned char size, unsigned char * buffer);