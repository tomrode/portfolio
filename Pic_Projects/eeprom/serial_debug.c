
#include "htc.h"
#include "typedefs.h"
#include "serial_debug.h"
/***************************************************************************************************************
* serial_data_debug()
* 
* Intent: Used as a Serial marker for debugging purposes.
*  
* INPUT  -> 1 byte
* OUTPUT -> NONE
*
* CALLS TO:   NONE
* CALLS FROM: User defined 
/****************************************************************************************************************/

void serial_data_debug (uint8_t x)
{
 /* Local variables*/
  uint8_t i; 
  uint16_t test_val;
  uint16_t and_against_mask;
  uint16_t result;
  
  /* Initialization Section */
  
  TRISD = 0x00;              /* Make as TRIS as output or what every Data direction available.*/
  result = 0;                /* Clear out the result to known state.*/      
  and_against_mask = 0x8000; /* Preload with start bit at msb spot. */
  test_val =(0x8000 | x);    /* get the start bit and byte in question, put together in 16 bit form. */ 
  
 for(i=0; i<16; i++)
   {    
      RD0 = 0;                                 /* Output off*/
       
      result = (and_against_mask & test_val);  /* Do a bit wise logical test*/ 
      
      while(result)                            /* See if output is true*/
       {
         RD0 = 1;                              /* Make RD0 Turn on*/
         result = 0;                           /* get out of the while*/         
       } 
      
      /* move down the 16 bit value one spot for the next bit-wise logic test.*/  
      and_against_mask = (and_against_mask >> 1);
   } 
  
}
