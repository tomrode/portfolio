/**********************************************************/
/* Simple program to test if I can caomplie using a batch */
/* file                                                   */
/* 8 AUG 2012 using PIC16F887                             */ 
/*                                                        */
/**********************************************************/

#include <htc.h>
#include "func1.h"

void func1(void)
{
   static int test_func1_var;

   test_func1_var++;
  

   if(test_func1_var >=127)
    {
        PORTD = 0xF0;
        _delay(300000);         /*Amount of instruction cycles*/ 
        PORTD = 0x0F; 
        _delay(300000);         /*Amount of instruction cycles*/ 
   }
   else
    {
      PORTD = test_func1_var;
    }
}
