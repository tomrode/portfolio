/**********************************************************/
/* Simple program to test if I can caomplie using a batch */
/* file                                                   */
/* 8 AUG 2012 using PIC16F887                             */ 
/*                                                        */
/**********************************************************/
#include <htc.h>
#include "func1.h"
/**********************************************************/
/* Device Configuration
/**********************************************************/

          
/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & HS);
__CONFIG(BORV40);  //HS means resonator 8Mhz     
       
 
/**********************************************************/
/* PROTOTYPES
/**********************************************************/

void init(void);
//void func1(void);
void main(void);

/***********************************************************/
/* MAIN
/***********************************************************/
void main(void)
{
static int count;
init();         /* Turn on TRISD*/
count=0;
while(1)
  {
   if(count<10)
    {     
       PORTD ^= 0xFF;
      _delay(900000);         /*Amount of instruction cycles*/
       count++;
    }
   else
     {
        func1();
        // _delay(90000);         /*Amount of instruction cycles*/
     }   
  }
}


void init(void)
{
TRISD = 0x00;
PORTD = 0x00;
TRISB = 0x00;
PORTB = 0x00;
}

//void func1(void)
//{
//   static int test_func1_var;

//   test_func1_var++;
//   PORTD = test_func1_var;
//}