/*****************************************************************************/
/* FILE NAME:  func.h                                                        */
/*****************************************************************************/

#include "GenericTypeDefs.h"

#ifndef FUNC_H
#define FUNC_H

/* Global variables*/
extern int extern_var;
/* Function prototypes*/
void func(void);

#endif