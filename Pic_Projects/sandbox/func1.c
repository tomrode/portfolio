#include <p32xxxx.h>			// Required for SFR defs
#include "GenericTypeDefs.h"
#include "func1.h"

/* LOCAL */
/*function prototypes*/
int call0(void);
int call1(void);
int call2(void);
int call3(void);
int call4(void);
int call5(void);
int call6(void);
int call7(void);
int call8(void);
int call9(void);

 
/********************************************************************************************************
/*
/*        MY FIRST EXPOSURE TO THE MPLAB C32 COMPILIER 
/*
/********************************************************************************************************/
int call0(void) 
{ 
return call1();
}

int call1(void) 
{ 
return call2();
}

int call2(void) 
{ 
return call3();
}


int call3(void) 
{ 
return call4();
}

int call4(void) 
{ 
return call5();
}

int call5(void) 
{ 
return call6();
}

int call6(void) 
{ 
return call7();
}

int call7(void) 
{ 
return call8();
}

int call8(void) 
{ 
return call9();
}

int call9(void) 
{ 
return 0xDEAD;
}



/* EXTERNAL-GLOBAL in SCOPE variables*/
//int func1_var; /*declared in main*/


int add_func(int a, int b)
{
  long mult_c;
  func1_var = 33;
  mult_c = (char)a;
  ++func1_var;
  return (a+b);

};
int minus_func(int a, int b)
{
  return (a-b);
};
int mult_func(int a, int b)
{
  return (a*b);
};
int div_func(int a, int b)
{
  return (a/b);
};

void test(void)
{ 
 int c_test;
 c_test =((*func_array[0])(10,21));
 c_test =((*func_array[2])(10,21));
// c_test = (&add_func)(5,7);
}
/****************************************************************************/
/* FUNCTION  POINTER
/*
/****************************************************************************/
int another_ptr_func (int a, int b,int (*func_ptr)(int a, int b))
{
  int result;
  result = func_ptr(a,b);
  return  result;
}



/*----------------------------------------------------------------------------*/
/*Name : Vf040_ComputeJ1850CRC                                                */
/*Role : This function takes a buffer and its size, then calculates the       */
/*        SAE J1850 CRC  for the msg.                                         */
/*Interface : -  Arg1: A pointer to the message we're calculating the CRC for */
/*            -  Arg2: The length of the message being calculated             */
/*Pre-condition : -                                                           */
/*Constraints :                                                               */
/*Behavior :                                                                  */
/*  DO                                                                        */
/*    Calculates the CRC one byte at time using polynom 0x1D                  */
/*    (Representation normal)                                                 */
/*  OD                                                                        */
/*----------------------------------------------------------------------------*/
 INT8 Vf040_ComputeJ1850CRC(INT8 *msg_buf, INT8 nbytes)
{
  INT8 crc_reg=0xff,poly,byte_count,bit_count;
  INT8 *byte_point;
  INT8 bit_point;

  INT8 test_ptr;

  test_ptr = *msg_buf;
  msg_buf++; 
  test_ptr = *msg_buf;

  for (byte_count=0, byte_point=msg_buf; byte_count<nbytes; ++byte_count, ++byte_point)
  {
    for (bit_count=0, bit_point=0x80 ; bit_count<8; ++bit_count, bit_point>>=1)
    {
      if (bit_point & *byte_point)
      {/* case for new bit = 1 */
        if (crc_reg & 0x80)
        {
          /* define the polynomial */
          poly=1;
        }
        else
        {
          poly=0x1c;
        }
        crc_reg= ( (crc_reg << 1) | 1) ^ poly;
      }
      else
      {/* case for new bit = 0 */
        poly=0;
        if (crc_reg & 0x80)
        {
          poly=0x1d;
        }
        crc_reg= (crc_reg << 1) ^ poly;
      }
    }
  }
  return ~crc_reg;  // Return CRC
}