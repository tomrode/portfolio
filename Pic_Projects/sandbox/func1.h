#ifndef FUNC1_H   
#define FUNC1_H    


/********************************************************************/
/* Type definitions FUNCTION POINTER                                                */
/********************************************************************/

typedef int(*T_FUNCPTR_1)(int a, int b);

/********************************************************************/
/* Function Prototyes                                               */
/********************************************************************/
int add_func(int a, int b);
int minus_func(int a, int b);
int mult_func(int a, int b);
int div_func(int a, int b);

void test(void);
int another_ptr_func (int a,int b,int (*func_ptr)(int a, int b));
 INT8 Vf040_ComputeJ1850CRC(INT8 *msg_buf, INT8 nbytes);
/*********************************************************************/
/* CONSTANTS WHICH IS LOCAL ONLY 
/*********************************************************************/
const T_FUNCPTR_1 func_array[] =
{
   add_func,
   minus_func,
   mult_func,
   div_func
};

/* extenal vars*/
extern int func1_var;

#endif