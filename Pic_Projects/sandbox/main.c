#include <p32xxxx.h>			// Required for SFR defs
#include "GenericTypeDefs.h"
#include "MoreTypeDefs.h"
#include "extern.h"
//#include "func1.h"
//#include "specificTypeDefs.h"
const INT8 RightOutDisplay[] = {0x00,0x00,0x01,0x02,0x03};
const INT8 LeftOutDisplay[]  = {0x00,0x03,0x02,0x01,0x00};
/********************************************************************************************************
/*
/*        MY FIRST EXPOSURE TO THE MPLAB C32 COMPILIER 
/*        SANDBOX IS alot of nothing just exercising examples
/*
/********************************************************************************************************/

/* TYPE DEFS*/

typedef struct
            {
             INT8 mem1;
             INT8 mem2;
             struct {
                      char mem_bit0    :1;
                      char mem_bits1   :2;
                      char mem_bits2   :4;
                    }bit_field;         
             INT8 array[5];
            }TEST_STRUCT;

/* Constants Arrays*/
const INT block01[10];

INT block02[20];
INT return_proc;
/* Function prototypes*/
void main (void);
INT checksum(INT *data, INT length);
INT proc_block01(void);
INT8 batt_voltage(void);
void struct_ptr_pass(CANTxMessageBuffer *pass_buff);
void vSB_ApplyBitManipulations(void *pvData);
/* EXTERNAL-GLOBAL in SCOPE variables*/
int func1_var;
int extern_var;
INT test_switch;
/*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/

void main (void)
{
/* Structures */
TEST_STRUCT      see_work;
TEST_STRUCT      *ptr;
CANTxMessageBuffer *buffer;
CANTxMessageBuffer  Toms_message;
/* Variables */
float divide;
INT var;
INT count;
INT size_of;
INT8 j1850crc_example_ar[] = {0xBA,0xDB,0xEE,0xF0,'F'};
im_hwo_type im_type_test;
im_hwi_type im_input;
im_hwi_type MACRO_TEST;
int         INT_MACRO_TEST;
  int a;
  int b;
  int c;
  INT8 d;
  int e; 
 INT8 *nullPtr;
 INT16 *i_ptr; 
 INT16 *r_resultPtr;
 INT16 *c_ptr;
 INT16 *regularPtr;
 INT8 r[] = {0xDE,0xAD,0xBE,0xEF};  
 INT32 ulData = 0x00001234;
 void *pvSrc = (void *)&ulData;
 vSB_ApplyBitManipulations(pvSrc);

  struct
      {
         INT16 mem1;
         INT16 mem2;     
      }DeadBeefStruct;    
  static char  resultofr0; 
  static char  resultofr1;
  static char  resultofr2;
  static char  resultofr3; 
  static INT16 resultofr4; 
  
  d = (0xFF);
 // d &= (0x30);
  if(!(d & 0x01))
  { 
     asm("nop");
  }

 // if(!(d & 

  nullPtr = &j1850crc_example_ar[0];
  while(*nullPtr++);    /* find the end of the string*/  

  DeadBeefStruct.mem1 = 0xDEAD;
  DeadBeefStruct.mem2 = 0xBEEF;
  //c_ptr = (u_int08 *)&r[0];
  //i_ptr = (u_int16 *)c_ptr; /* Should work*/
  i_ptr = (INT16 *)&r[0];  /* violation */
  //resultofr0 = *i_ptr;
/*
  regularPtr = &r;
  resultofr0 = *regularPtr;
  regularPtr++;
  resultofr1 = *regularPtr;
  regularPtr++;
  resultofr2 = *regularPtr;
  regularPtr++;
  resultofr3 = *regularPtr;
*/
  resultofr0 = *i_ptr;
  i_ptr++;
  resultofr1 = *i_ptr;
  
  
  
  i_ptr = (INT16 *)&(DeadBeefStruct.mem1);
  resultofr4 = *i_ptr;
  i_ptr++;
  resultofr4 = *i_ptr;
  resultofr3 = (INT8)resultofr4;
 e = 2; 
 d &= (0xF0); 
 d ^= (0x08 >>  RightOutDisplay[e]); 
 e = 3;
 d &= (0xF0);
 d ^= (0x08  >> RightOutDisplay[e]);

 e = 2;
 d &= (0x0F);   /*Maintain integrity of right side now*/
 d ^= (0x80 >> LeftOutDisplay[e]);
 d = (0x63 & 0xF0);

  a=5;
  b=4;
  c=1;
extern_var = 1;
_nop();
func();

 func1_var = 44;
 c = add_func(a, b);

 var = call0();   /* 10 levels of calls takes 1.5 micro seconds*/

_nop();
#define TEN_LEVEL_CALL    (call0()); /* same thing in macro form*/
#define ANOTHER_MACRO_TEN_LEVEL_CALL     TEN_LEVEL_CALL


var = ANOTHER_MACRO_TEN_LEVEL_CALL;

 test();
 c = another_ptr_func (11,22,&add_func);
 c = Vf040_ComputeJ1850CRC(&j1850crc_example_ar[0],4);

 d = (( a == 4)?c:b);

#define SET_SOMETHING_MACRO(x)     (x = (( a == 5)?c:b))  
#define VF008_BatteryVoltage       (im_input.hwi_tipm_batt_volt)
//im_input.hwi_tipm_batt_volt = 0x33;
VF008_BatteryVoltage =  batt_voltage(); 
INT_MACRO_TEST = VF008_BatteryVoltage;
e = SET_SOMETHING_MACRO(e);

/* Set up to make portb B outputs*/
/* toggle port pin*/
AD1PCFG = 0xFFFF;    // Initialize AN pins as digital need this to enable portb
TRISB  = 0;

im_type_test.hwo_tk_lmp = 1;
im_type_test.hwo_vta_lamp = 1;
count = 0;
test_switch       = 1;


see_work.mem1      = 0xBA;
see_work.mem2      = 0xDE;
see_work.bit_field.mem_bits1 = 0x03;
see_work.array[1]  = 0x55;

buffer = &Toms_message;
buffer -> messageWord[0] = 0; /* Clear out the CAN message*/
buffer -> messageWord[1] = 0;
buffer -> messageWord[2] = 0;
buffer -> messageWord[3] = 0; 
buffer -> msgSID.SID = 0x515; /* CAN ID*/
buffer -> msgEID.DLC = 4;     /* 4 bytes of payload*/
buffer -> msgEID.SRR = 2;     /* SRR reg*/
buffer -> data[0]    = 0xDE;
buffer -> data[1]    = 0xAD;
buffer -> data[2]    = 0xBE;
buffer -> data[3]    = 0xEF;

LATB = 0x00000003;
//while(1)
//{ 
struct_ptr_pass(buffer);
//}
/* Its Like the pointer places values back into see_work*/ 
ptr = &see_work;
ptr -> mem1 = 0x11; 
ptr -> mem2 = 0x22;
ptr -> bit_field.mem_bit0 = 1;
ptr -> bit_field.mem_bits1 = 3;
ptr -> bit_field.mem_bits2 = 15;
ptr -> array[0] = 0xBA;
ptr -> array[1] = 0xDE;
ptr -> array[2] = 0xEF;
ptr -> array[3] = 1;
ptr -> array[4] = 2;


size_of = sizeof(see_work.array[1]);
while(1)
 {
  divide = 600/9.0;
   switch(test_switch)
    {
      case 1:
       var = 1;
       var = 99;
       var = 100;
       var =  batt_voltage();
       test_switch = 2;
      break;
   
      case 2:
       var = 2;
       test_switch = 3;
      break;

      case 3:
       var = 3;
       test_switch = 1;  /* loop forever*/
      break;

      default:
        break;
    } 

 }

}



void struct_ptr_pass(CANTxMessageBuffer *pass_buff)
{
  CANTxMessageBuffer StructPassResult; 

  StructPassResult = *pass_buff;
   _nop();
 
   LATB ^= 0x00000003;
  
}

/** Poly Space example implementation*/
void vSB_ApplyBitManipulations(void *pvData)
{
    INT GetpvData;
    
    GetpvData = *( INT32*)pvData; 
}

 
INT checksum(INT *data, INT length)
{
INT res;
INT i;

  res = 0;
  for(i = 0; i <=length; i++)
  {
    res += (INT)*data;
  }		

 return (res & 0x00ff); 	
}	



INT proc_block01(void)
{
INT temp;	
		
 temp = checksum(block01,10);
 return(temp);
}

INT8 batt_voltage(void)
{
 test_switch = 1;
 return 0x99;
}