#include "htc.h"
#include "typedefs.h"
#include "timer.h"
/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);

/* Function prototypes*/                               

uint32_t factorial(uint32_t n);
void dec_global(void);
    
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main (void)
{
/* variables for the main*/
uint32_t result_fac_main;
/* Enables the timer int */

GIE = 0;   /* Global int enable*/
PEIE = 1;  /* Peripheral anable*/
TMR1IE = 1;/* Timer1 int enable*/ 
TMR1ON = 1;
timer_set(TIMER_1,1);
ei();       /* enable the interrupts now*/
my_global=200;
  while(1)
  {
     di();
     result_fac_main = factorial(5);
     ei();
     asm("nop");//dec_global();
  }
}


void dec_global(void)
{
if (my_global > 0)
 {
   my_global--;
 }
 else
 {
 /* Do nothing*/
 }
}


uint32_t factorial(uint32_t n)
{
  uint32_t result;

  result = 1;

  while (n > 0)
  {
    result = result*n;
    n--;
  }

  return result;
}



