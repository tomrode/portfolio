
#include "typedefs.h"
#include "htc.h"



/* Variables*/
extern uint32_t my_global;  
enum sw_timers_e
{
TIMER_1,
TIMER_2,               // use for adc pot test
TIMER_MAX
};


/*Function Prototypes*/
uint16_t get_timer(uint8_t index);             // Which timer to use out of enum
//uint32_t factorial_recursive(uint32_t n);
void timer_set(uint8_t index, uint16_t value); 
void timer_isr(void);
void timer_init(void);
