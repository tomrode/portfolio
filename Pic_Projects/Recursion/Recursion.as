opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 10920"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 5 "C:\pic_projects\Recursion\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 5 "C:\pic_projects\Recursion\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 6 "C:\pic_projects\Recursion\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 6 "C:\pic_projects\Recursion\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_timer_set
	FNCALL	_main,_factorial
	FNCALL	_factorial,___lmul
	FNROOT	_main
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_my_global
	global	_timer_array
	global	_tmr1_isr_counter
	global	_toms_variable
	global	_T1CON
psect	text192,local,class=CODE,delta=2
global __ptext192
__ptext192:
_T1CON	set	16
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_PEIE
_PEIE	set	94
	global	_TMR1IF
_TMR1IF	set	96
	global	_TMR1ON
_TMR1ON	set	128
	global	_TMR1IE
_TMR1IE	set	1120
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"Recursion.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_tmr1_isr_counter:
       ds      1

_toms_variable:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_my_global:
       ds      4

_timer_array:
       ds      4

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
	clrf	((__pbssBANK0)+7)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	4
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x4
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x5
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	?___lmul
?___lmul:	; 4 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	global	___lmul@multiplier
___lmul@multiplier:	; 4 bytes @ 0x0
	ds	2
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	ds	1
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	ds	1
	global	___lmul@multiplicand
___lmul@multiplicand:	; 4 bytes @ 0x4
	ds	4
	global	??___lmul
??___lmul:	; 0 bytes @ 0x8
	ds	1
	global	___lmul@product
___lmul@product:	; 4 bytes @ 0x9
	ds	4
	global	?_factorial
?_factorial:	; 4 bytes @ 0xD
	global	factorial@n
factorial@n:	; 4 bytes @ 0xD
	ds	4
	global	??_factorial
??_factorial:	; 0 bytes @ 0x11
	ds	4
	global	factorial@result
factorial@result:	; 4 bytes @ 0x15
	ds	4
	global	??_main
??_main:	; 0 bytes @ 0x19
	global	main@result_fac_main
main@result_fac_main:	; 4 bytes @ 0x19
	ds	4
;;Data sizes: Strings 0, constant 0, data 0, bss 10, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      9      11
;; BANK0           80     29      37
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___lmul	unsigned long  size(1) Largest target is 0
;;
;; ?_factorial	unsigned long  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_factorial
;;   _factorial->___lmul
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 4     4      0     250
;;                                             25 BANK0      4     4      0
;;                          _timer_set
;;                          _factorial
;; ---------------------------------------------------------------------------------
;; (1) _factorial                                           12     8      4     183
;;                                             13 BANK0     12     8      4
;;                             ___lmul
;; ---------------------------------------------------------------------------------
;; (2) ___lmul                                              13     5      8      92
;;                                              0 BANK0     13     5      8
;; ---------------------------------------------------------------------------------
;; (1) _timer_set                                            6     4      2      66
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _interrupt_handler                                    4     4      0      90
;;                                              5 COMMON     4     4      0
;;                          _timer_isr
;; ---------------------------------------------------------------------------------
;; (4) _timer_isr                                            9     9      0      90
;;                                              0 COMMON     5     5      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _timer_set
;;   _factorial
;;     ___lmul
;;
;; _interrupt_handler (ROOT)
;;   _timer_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      9       B       1       78.6%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       6       2        0.0%
;;ABS                  0      0      30       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     1D      25       5       46.3%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      36      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 17 in file "C:\pic_projects\Recursion\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  result_fac_m    4   25[BANK0 ] unsigned long 
;; Return value:  Size  Location     Type
;;                  2  842[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_timer_set
;;		_factorial
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\Recursion\main.c"
	line	17
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 4
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	22
	
l4981:	
;main.c: 19: uint32_t result_fac_main;
;main.c: 22: GIE = 0;
	bcf	(95/8),(95)&7
	line	23
;main.c: 23: PEIE = 1;
	bsf	(94/8),(94)&7
	line	24
;main.c: 24: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	25
;main.c: 25: TMR1ON = 1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(128/8),(128)&7
	line	26
	
l4983:	
;main.c: 26: timer_set(TIMER_1,1);
	movlw	low(01h)
	movwf	(?_timer_set)
	movlw	high(01h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	27
	
l4985:	
;main.c: 27: (GIE = 1);
	bsf	(95/8),(95)&7
	line	28
	
l4987:	
;main.c: 28: my_global=200;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_my_global+3)
	movlw	0
	movwf	(_my_global+2)
	movlw	0
	movwf	(_my_global+1)
	movlw	0C8h
	movwf	(_my_global)

	goto	l4989
	line	29
;main.c: 29: while(1)
	
l843:	
	line	31
	
l4989:	
;main.c: 30: {
;main.c: 31: (GIE = 0);
	bcf	(95/8),(95)&7
	line	32
	
l4991:	
;main.c: 32: result_fac_main = factorial(5);
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_factorial+3)
	movlw	0
	movwf	(?_factorial+2)
	movlw	0
	movwf	(?_factorial+1)
	movlw	05h
	movwf	(?_factorial)

	fcall	_factorial
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(3+(?_factorial)),w
	movwf	(main@result_fac_main+3)
	movf	(2+(?_factorial)),w
	movwf	(main@result_fac_main+2)
	movf	(1+(?_factorial)),w
	movwf	(main@result_fac_main+1)
	movf	(0+(?_factorial)),w
	movwf	(main@result_fac_main)

	line	33
	
l4993:	
;main.c: 33: (GIE = 1);
	bsf	(95/8),(95)&7
	line	34
	
l4995:	
# 34 "C:\pic_projects\Recursion\main.c"
nop ;#
psect	maintext
	goto	l4989
	line	35
	
l844:	
	line	29
	goto	l4989
	
l845:	
	line	36
	
l846:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_factorial
psect	text193,local,class=CODE,delta=2
global __ptext193
__ptext193:

;; *************** function _factorial *****************
;; Defined at:
;;		line 53 in file "C:\pic_projects\Recursion\main.c"
;; Parameters:    Size  Location     Type
;;  n               4   13[BANK0 ] unsigned long 
;; Auto vars:     Size  Location     Type
;;  result          4   21[BANK0 ] unsigned long 
;; Return value:  Size  Location     Type
;;                  4   13[BANK0 ] unsigned long 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       4       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      12       0       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___lmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text193
	file	"C:\pic_projects\Recursion\main.c"
	line	53
	global	__size_of_factorial
	__size_of_factorial	equ	__end_of_factorial-_factorial
	
_factorial:	
	opt	stack 4
; Regs used in _factorial: [wreg+status,2+status,0+pclath+cstack]
	line	56
	
l4969:	
;main.c: 54: uint32_t result;
;main.c: 56: result = 1;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(factorial@result+3)
	movlw	0
	movwf	(factorial@result+2)
	movlw	0
	movwf	(factorial@result+1)
	movlw	01h
	movwf	(factorial@result)

	line	58
;main.c: 58: while (n > 0)
	goto	l4975
	
l855:	
	line	60
	
l4971:	
;main.c: 59: {
;main.c: 60: result = result*n;
	movf	(factorial@result+3),w
	movwf	(?___lmul+3)
	movf	(factorial@result+2),w
	movwf	(?___lmul+2)
	movf	(factorial@result+1),w
	movwf	(?___lmul+1)
	movf	(factorial@result),w
	movwf	(?___lmul)

	movf	(factorial@n+3),w
	movwf	3+(?___lmul)+04h
	movf	(factorial@n+2),w
	movwf	2+(?___lmul)+04h
	movf	(factorial@n+1),w
	movwf	1+(?___lmul)+04h
	movf	(factorial@n),w
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(3+(?___lmul)),w
	movwf	(factorial@result+3)
	movf	(2+(?___lmul)),w
	movwf	(factorial@result+2)
	movf	(1+(?___lmul)),w
	movwf	(factorial@result+1)
	movf	(0+(?___lmul)),w
	movwf	(factorial@result)

	line	61
	
l4973:	
;main.c: 61: n--;
	movlw	01h
	movwf	((??_factorial+0)+0)
	movlw	0
	movwf	((??_factorial+0)+0+1)
	movlw	0
	movwf	((??_factorial+0)+0+2)
	movlw	0
	movwf	((??_factorial+0)+0+3)
	movf	0+(??_factorial+0)+0,w
	subwf	(factorial@n),f
	movf	1+(??_factorial+0)+0,w
	skipc
	incfsz	1+(??_factorial+0)+0,w
	goto	u2535
	goto	u2536
u2535:
	subwf	(factorial@n+1),f
u2536:
	movf	2+(??_factorial+0)+0,w
	skipc
	incfsz	2+(??_factorial+0)+0,w
	goto	u2537
	goto	u2538
u2537:
	subwf	(factorial@n+2),f
u2538:
	movf	3+(??_factorial+0)+0,w
	skipc
	incfsz	3+(??_factorial+0)+0,w
	goto	u2539
	goto	u2530
u2539:
	subwf	(factorial@n+3),f
u2530:

	goto	l4975
	line	62
	
l854:	
	line	58
	
l4975:	
	movf	(factorial@n+3),w
	iorwf	(factorial@n+2),w
	iorwf	(factorial@n+1),w
	iorwf	(factorial@n),w
	skipz
	goto	u2541
	goto	u2540
u2541:
	goto	l4971
u2540:
	goto	l4977
	
l856:	
	line	64
	
l4977:	
;main.c: 62: }
;main.c: 64: return result;
	movf	(factorial@result+3),w
	movwf	(?_factorial+3)
	movf	(factorial@result+2),w
	movwf	(?_factorial+2)
	movf	(factorial@result+1),w
	movwf	(?_factorial+1)
	movf	(factorial@result),w
	movwf	(?_factorial)

	goto	l857
	
l4979:	
	line	65
	
l857:	
	return
	opt stack 0
GLOBAL	__end_of_factorial
	__end_of_factorial:
;; =============== function _factorial ends ============

	signat	_factorial,4220
	global	___lmul
psect	text194,local,class=CODE,delta=2
global __ptext194
__ptext194:

;; *************** function ___lmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\lmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      4    0[BANK0 ] unsigned long 
;;  multiplicand    4    4[BANK0 ] unsigned long 
;; Auto vars:     Size  Location     Type
;;  product         4    9[BANK0 ] unsigned long 
;; Return value:  Size  Location     Type
;;                  4    0[BANK0 ] unsigned long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       8       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0      13       0       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_factorial
;; This function uses a non-reentrant model
;;
psect	text194
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\lmul.c"
	line	3
	global	__size_of___lmul
	__size_of___lmul	equ	__end_of___lmul-___lmul
	
___lmul:	
	opt	stack 4
; Regs used in ___lmul: [wreg+status,2+status,0]
	line	4
	
l4955:	
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(___lmul@product+3)
	movlw	0
	movwf	(___lmul@product+2)
	movlw	0
	movwf	(___lmul@product+1)
	movlw	0
	movwf	(___lmul@product)

	goto	l4957
	line	6
	
l3624:	
	line	7
	
l4957:	
	btfss	(___lmul@multiplier),(0)&7
	goto	u2481
	goto	u2480
u2481:
	goto	l4961
u2480:
	line	8
	
l4959:	
	movf	(___lmul@multiplicand),w
	addwf	(___lmul@product),f
	movf	(___lmul@multiplicand+1),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2491
	addwf	(___lmul@product+1),f
u2491:
	movf	(___lmul@multiplicand+2),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2492
	addwf	(___lmul@product+2),f
u2492:
	movf	(___lmul@multiplicand+3),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2493
	addwf	(___lmul@product+3),f
u2493:

	goto	l4961
	
l3625:	
	line	9
	
l4961:	
	movlw	01h
	movwf	(??___lmul+0)+0
u2505:
	clrc
	rlf	(___lmul@multiplicand),f
	rlf	(___lmul@multiplicand+1),f
	rlf	(___lmul@multiplicand+2),f
	rlf	(___lmul@multiplicand+3),f
	decfsz	(??___lmul+0)+0
	goto	u2505
	line	10
	
l4963:	
	movlw	01h
u2515:
	clrc
	rrf	(___lmul@multiplier+3),f
	rrf	(___lmul@multiplier+2),f
	rrf	(___lmul@multiplier+1),f
	rrf	(___lmul@multiplier),f
	addlw	-1
	skipz
	goto	u2515

	line	11
	movf	(___lmul@multiplier+3),w
	iorwf	(___lmul@multiplier+2),w
	iorwf	(___lmul@multiplier+1),w
	iorwf	(___lmul@multiplier),w
	skipz
	goto	u2521
	goto	u2520
u2521:
	goto	l4957
u2520:
	goto	l4965
	
l3626:	
	line	12
	
l4965:	
	movf	(___lmul@product+3),w
	movwf	(?___lmul+3)
	movf	(___lmul@product+2),w
	movwf	(?___lmul+2)
	movf	(___lmul@product+1),w
	movwf	(?___lmul+1)
	movf	(___lmul@product),w
	movwf	(?___lmul)

	goto	l3627
	
l4967:	
	line	13
	
l3627:	
	return
	opt stack 0
GLOBAL	__end_of___lmul
	__end_of___lmul:
;; =============== function ___lmul ends ============

	signat	___lmul,8316
	global	_timer_set
psect	text195,local,class=CODE,delta=2
global __ptext195
__ptext195:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 18 in file "C:\pic_projects\Recursion\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text195
	file	"C:\pic_projects\Recursion\timer.c"
	line	18
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 5
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	20
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l4887:	
;timer.c: 19: uint16_t array_contents;
;timer.c: 20: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(timer_set@index),w
	skipnc
	goto	u2361
	goto	u2360
u2361:
	goto	l2541
u2360:
	line	22
	
l4889:	
;timer.c: 21: {
;timer.c: 22: (GIE = 0);
	bcf	(95/8),(95)&7
	line	23
	
l4891:	
;timer.c: 23: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	24
	
l4893:	
;timer.c: 24: (GIE = 1);
	bsf	(95/8),(95)&7
	line	25
;timer.c: 25: }
	goto	l2541
	line	26
	
l2539:	
	goto	l2541
	line	28
;timer.c: 26: else
;timer.c: 27: {
	
l2540:	
	line	29
	
l2541:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_interrupt_handler
psect	text196,local,class=CODE,delta=2
global __ptext196
__ptext196:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 6 in file "C:\pic_projects\Recursion\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_timer_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text196
	file	"C:\pic_projects\Recursion\interrupt.c"
	line	6
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 4
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	swapf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text196
	line	7
	
i1l4899:	
;interrupt.c: 7: timer_isr();
	fcall	_timer_isr
	line	8
	
i1l1696:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	swapf	(??_interrupt_handler+0)^0FFFFFF80h,w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_timer_isr
psect	text197,local,class=CODE,delta=2
global __ptext197
__ptext197:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 54 in file "C:\pic_projects\Recursion\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  result_fac_t    4    0        unsigned long 
;;  i               1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         5       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text197
	file	"C:\pic_projects\Recursion\timer.c"
	line	54
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 4
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	57
	
i1l4901:	
;timer.c: 55: uint8_t i;
;timer.c: 56: uint32_t result_fac_timer_isr;
;timer.c: 57: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u239_21
	goto	u239_20
u239_21:
	goto	i1l2555
u239_20:
	
i1l4903:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u240_21
	goto	u240_20
u240_21:
	goto	i1l2555
u240_20:
	line	60
	
i1l4905:	
;timer.c: 58: {
;timer.c: 60: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	61
	
i1l4907:	
;timer.c: 61: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	62
	
i1l4909:	
;timer.c: 62: TMR1L = 0x08;
	movlw	(08h)
	movwf	(14)	;volatile
	line	63
	
i1l4911:	
;timer.c: 63: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	64
	
i1l4913:	
;timer.c: 64: my_global--;
	movlw	01h
	movwf	((??_timer_isr+0)+0)
	movlw	0
	movwf	((??_timer_isr+0)+0+1)
	movlw	0
	movwf	((??_timer_isr+0)+0+2)
	movlw	0
	movwf	((??_timer_isr+0)+0+3)
	movf	0+(??_timer_isr+0)+0,w
	subwf	(_my_global),f
	movf	1+(??_timer_isr+0)+0,w
	skipc
	incfsz	1+(??_timer_isr+0)+0,w
	goto	u241_25
	goto	u241_26
u241_25:
	subwf	(_my_global+1),f
u241_26:
	movf	2+(??_timer_isr+0)+0,w
	skipc
	incfsz	2+(??_timer_isr+0)+0,w
	goto	u241_27
	goto	u241_28
u241_27:
	subwf	(_my_global+2),f
u241_28:
	movf	3+(??_timer_isr+0)+0,w
	skipc
	incfsz	3+(??_timer_isr+0)+0,w
	goto	u241_29
	goto	u241_20
u241_29:
	subwf	(_my_global+3),f
u241_20:

	line	66
	
i1l4915:	
;timer.c: 66: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	68
	
i1l4917:	
;timer.c: 68: for (i = 0; i < TIMER_MAX; i++)
	clrf	(timer_isr@i)
	
i1l4919:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u242_21
	goto	u242_20
u242_21:
	goto	i1l4923
u242_20:
	goto	i1l2555
	
i1l4921:	
	goto	i1l2555
	line	69
	
i1l2550:	
	line	70
	
i1l4923:	
;timer.c: 69: {
;timer.c: 70: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u243_21
	goto	u243_20
u243_21:
	goto	i1l4927
u243_20:
	line	72
	
i1l4925:	
;timer.c: 71: {
;timer.c: 72: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	73
;timer.c: 73: }
	goto	i1l4927
	line	74
	
i1l2552:	
	goto	i1l4927
	line	76
;timer.c: 74: else
;timer.c: 75: {
	
i1l2553:	
	line	68
	
i1l4927:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l4929:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u244_21
	goto	u244_20
u244_21:
	goto	i1l4923
u244_20:
	goto	i1l2555
	
i1l2551:	
	line	79
;timer.c: 76: }
;timer.c: 77: }
;timer.c: 79: }
	goto	i1l2555
	line	80
	
i1l2549:	
	goto	i1l2555
	line	82
;timer.c: 80: else
;timer.c: 81: {
	
i1l2554:	
	line	83
	
i1l2555:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
psect	text198,local,class=CODE,delta=2
global __ptext198
__ptext198:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
