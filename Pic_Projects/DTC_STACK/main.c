#include "htc.h"
#include "typedefs.h"
#define MAX_LENGTH (6)

/*PIC16F887*/
//__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
//__CONFIG(BORV40);

/*ENUMS*/
enum
   {
    NO_DTC,                              /* implied 0*/
    OPEN,
    SHORT,
    LOSS_COMM,
    INVALID_CONFIG                       /* implied 4*/
    }dtc_names;

/* CONSTANT ARRAY */
const uint8_t dtc_array_list[] = 
                          {
                           NO_DTC,        /* implied 0*/
                           OPEN,         
                           SHORT,
                           LOSS_COMM,
                           INVALID_CONFIG, /* implied 4*/
                           MAX_LENGTH
                          };
/* This array will have the most current dtc*/
uint8_t current_dtc_stack[MAX_LENGTH-1];


/* Function prototypes*/
void main(void);
void push_dtc_to_stack(uint8_t dtc_num);      
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/

void main(void)
{
   uint8_t reported_dtc;

while(1)
   {
     reported_dtc = OPEN;
     push_dtc_to_stack(reported_dtc);
     
     reported_dtc = SHORT;
     push_dtc_to_stack(reported_dtc);

     reported_dtc = INVALID_CONFIG;
     push_dtc_to_stack(reported_dtc);
  
     reported_dtc = OPEN;
     push_dtc_to_stack(reported_dtc);

     reported_dtc = LOSS_COMM;
     push_dtc_to_stack(reported_dtc);
     
     asm("nop");
   } 
}


void push_dtc_to_stack(uint8_t dtc_num)
{ 
/* local vars*/
static uint8_t i;
static uint8_t dtc_counter;
static uint8_t current_dtc_stack_copy[MAX_LENGTH-1];

 
/* Off the bat ckeck for non dtc numbers*/ 
  if(dtc_num >= MAX_LENGTH)
   {
       /* Maintian the stack as is*/
       asm("nop");
   }
  /* okay keep going for an actual dtc number at this point*/
  else 
    {
       /* Lets see something work now every iteration fill in array*/
        current_dtc_stack[dtc_counter] = dtc_num;
        current_dtc_stack_copy[dtc_counter] = current_dtc_stack[dtc_counter];   /* just a copy */ 
           
       /* Check for duplicate DTC's*/
       if(dtc_counter>=1)
        {
          if(current_dtc_stack_copy[dtc_counter-1] == dtc_num)
           {
              current_dtc_stack[dtc_counter] = 0;  
           }  
        }   
         if(dtc_counter>=2)
          {
            if(current_dtc_stack_copy[dtc_counter-2] == dtc_num)
             {
                current_dtc_stack[dtc_counter] = 0;  
             }
           } 
           if(dtc_counter>=3)
            {
             if(current_dtc_stack_copy[dtc_counter-3] == dtc_num)
              {
                current_dtc_stack[dtc_counter] = 0;  
              }
            } 
             if(dtc_counter>=4)
              {
               if(current_dtc_stack_copy[dtc_counter-4] == dtc_num)
                {
                  current_dtc_stack[dtc_counter] = 0;  
                }
               } 
                if(dtc_counter>=5)
              {
               if(current_dtc_stack_copy[dtc_counter-5] == dtc_num)
                {
                  current_dtc_stack[dtc_counter] = 0;  
                }
               } 
              
         dtc_counter++;           
    }
 
     
}  


