opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 10920"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
	FNCALL	_main,_push_dtc_to_stack
	FNROOT	_main
	global	_dtc_array_list
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\pic_projects\DTC_STACK\main.c"
	line	20
_dtc_array_list:
	retlw	0
	retlw	01h
	retlw	02h
	retlw	03h
	retlw	04h
	retlw	06h
	global	_dtc_array_list
	global	push_dtc_to_stack@current_dtc_stack_copy
	global	_current_dtc_stack
	global	_dtc_names
	global	_toms_variable
	global	push_dtc_to_stack@dtc_counter
	global	push_dtc_to_stack@i
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"DTC_STACK.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_current_dtc_stack:
       ds      5

_dtc_names:
       ds      1

_toms_variable:
       ds      1

push_dtc_to_stack@dtc_counter:
       ds      1

push_dtc_to_stack@i:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
push_dtc_to_stack@current_dtc_stack_copy:
       ds      5

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
	clrf	((__pbssCOMMON)+6)&07Fh
	clrf	((__pbssCOMMON)+7)&07Fh
	clrf	((__pbssCOMMON)+8)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_push_dtc_to_stack
?_push_dtc_to_stack:	; 0 bytes @ 0x0
	global	??_push_dtc_to_stack
??_push_dtc_to_stack:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	ds	1
	global	push_dtc_to_stack@dtc_num
push_dtc_to_stack@dtc_num:	; 1 bytes @ 0x1
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x2
	ds	1
	global	main@reported_dtc
main@reported_dtc:	; 1 bytes @ 0x3
	ds	1
;;Data sizes: Strings 0, constant 6, data 0, bss 14, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      4      13
;; BANK0           80      0       5
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_push_dtc_to_stack
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 2     2      0     269
;;                                              2 COMMON     2     2      0
;;                  _push_dtc_to_stack
;; ---------------------------------------------------------------------------------
;; (1) _push_dtc_to_stack                                    2     2      0     154
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _push_dtc_to_stack
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      4       D       1       92.9%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       1       2        0.0%
;;ABS                  0      0      12       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      0       5       5        6.3%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      13      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 41 in file "C:\pic_projects\DTC_STACK\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  reported_dtc    1    3[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_push_dtc_to_stack
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\DTC_STACK\main.c"
	line	41
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 7
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	44
;main.c: 42: uint8_t reported_dtc;
;main.c: 44: while(1)
	
l845:	
	line	46
	
l3176:	
;main.c: 45: {
;main.c: 46: reported_dtc = OPEN;
	clrf	(main@reported_dtc)
	bsf	status,0
	rlf	(main@reported_dtc),f
	line	47
	
l3178:	
;main.c: 47: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	49
	
l3180:	
;main.c: 49: reported_dtc = SHORT;
	movlw	(02h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@reported_dtc)
	line	50
	
l3182:	
;main.c: 50: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	52
	
l3184:	
;main.c: 52: reported_dtc = INVALID_CONFIG;
	movlw	(04h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@reported_dtc)
	line	53
	
l3186:	
;main.c: 53: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	55
	
l3188:	
;main.c: 55: reported_dtc = OPEN;
	clrf	(main@reported_dtc)
	bsf	status,0
	rlf	(main@reported_dtc),f
	line	56
;main.c: 56: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	58
	
l3190:	
;main.c: 58: reported_dtc = LOSS_COMM;
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@reported_dtc)
	line	59
	
l3192:	
;main.c: 59: push_dtc_to_stack(reported_dtc);
	movf	(main@reported_dtc),w
	fcall	_push_dtc_to_stack
	line	61
	
l3194:	
# 61 "C:\pic_projects\DTC_STACK\main.c"
nop ;#
psect	maintext
	goto	l845
	line	62
	
l846:	
	line	44
	goto	l845
	
l847:	
	line	63
	
l848:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_push_dtc_to_stack
psect	text75,local,class=CODE,delta=2
global __ptext75
__ptext75:

;; *************** function _push_dtc_to_stack *****************
;; Defined at:
;;		line 67 in file "C:\pic_projects\DTC_STACK\main.c"
;; Parameters:    Size  Location     Type
;;  dtc_num         1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  dtc_num         1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text75
	file	"C:\pic_projects\DTC_STACK\main.c"
	line	67
	global	__size_of_push_dtc_to_stack
	__size_of_push_dtc_to_stack	equ	__end_of_push_dtc_to_stack-_push_dtc_to_stack
	
_push_dtc_to_stack:	
	opt	stack 7
; Regs used in _push_dtc_to_stack: [wreg-fsr0h+status,2+status,0]
;push_dtc_to_stack@dtc_num stored from wreg
	line	75
	movwf	(push_dtc_to_stack@dtc_num)
	
l3138:	
;main.c: 69: static uint8_t i;
;main.c: 70: static uint8_t dtc_counter;
;main.c: 71: static uint8_t current_dtc_stack_copy[(6)-1];
;main.c: 75: if(dtc_num >= (6))
	movlw	(06h)
	subwf	(push_dtc_to_stack@dtc_num),w
	skipc
	goto	u2331
	goto	u2330
u2331:
	goto	l3142
u2330:
	line	78
	
l3140:	
# 78 "C:\pic_projects\DTC_STACK\main.c"
nop ;#
psect	text75
	line	79
;main.c: 79: }
	goto	l869
	line	81
	
l857:	
	line	84
	
l3142:	
;main.c: 81: else
;main.c: 82: {
;main.c: 84: current_dtc_stack[dtc_counter] = dtc_num;
	movf	(push_dtc_to_stack@dtc_num),w
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	movf	(??_push_dtc_to_stack+0)+0,w
	movwf	indf
	line	85
;main.c: 85: current_dtc_stack_copy[dtc_counter] = current_dtc_stack[dtc_counter];
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	movf	indf,w
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	push_dtc_to_stack@current_dtc_stack_copy&0ffh
	movwf	fsr0
	movf	(??_push_dtc_to_stack+0)+0,w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	line	88
	
l3144:	
;main.c: 88: if(dtc_counter>=1)
	movf	(push_dtc_to_stack@dtc_counter),w
	skipz
	goto	u2340
	goto	l3150
u2340:
	line	90
	
l3146:	
;main.c: 89: {
;main.c: 90: if(current_dtc_stack_copy[dtc_counter-1] == dtc_num)
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	0FFh
	addlw	push_dtc_to_stack@current_dtc_stack_copy&0ffh
	movwf	fsr0
	movf	indf,w
	xorwf	(push_dtc_to_stack@dtc_num),w
	skipz
	goto	u2351
	goto	u2350
u2351:
	goto	l3150
u2350:
	line	92
	
l3148:	
;main.c: 91: {
;main.c: 92: current_dtc_stack[dtc_counter] = 0;
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	clrf	indf
	goto	l3150
	line	93
	
l860:	
	goto	l3150
	line	94
	
l859:	
	line	95
	
l3150:	
;main.c: 93: }
;main.c: 94: }
;main.c: 95: if(dtc_counter>=2)
	movlw	(02h)
	subwf	(push_dtc_to_stack@dtc_counter),w
	skipc
	goto	u2361
	goto	u2360
u2361:
	goto	l3156
u2360:
	line	97
	
l3152:	
;main.c: 96: {
;main.c: 97: if(current_dtc_stack_copy[dtc_counter-2] == dtc_num)
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	0FEh
	addlw	push_dtc_to_stack@current_dtc_stack_copy&0ffh
	movwf	fsr0
	movf	indf,w
	xorwf	(push_dtc_to_stack@dtc_num),w
	skipz
	goto	u2371
	goto	u2370
u2371:
	goto	l3156
u2370:
	line	99
	
l3154:	
;main.c: 98: {
;main.c: 99: current_dtc_stack[dtc_counter] = 0;
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	clrf	indf
	goto	l3156
	line	100
	
l862:	
	goto	l3156
	line	101
	
l861:	
	line	102
	
l3156:	
;main.c: 100: }
;main.c: 101: }
;main.c: 102: if(dtc_counter>=3)
	movlw	(03h)
	subwf	(push_dtc_to_stack@dtc_counter),w
	skipc
	goto	u2381
	goto	u2380
u2381:
	goto	l3162
u2380:
	line	104
	
l3158:	
;main.c: 103: {
;main.c: 104: if(current_dtc_stack_copy[dtc_counter-3] == dtc_num)
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	0FDh
	addlw	push_dtc_to_stack@current_dtc_stack_copy&0ffh
	movwf	fsr0
	movf	indf,w
	xorwf	(push_dtc_to_stack@dtc_num),w
	skipz
	goto	u2391
	goto	u2390
u2391:
	goto	l3162
u2390:
	line	106
	
l3160:	
;main.c: 105: {
;main.c: 106: current_dtc_stack[dtc_counter] = 0;
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	clrf	indf
	goto	l3162
	line	107
	
l864:	
	goto	l3162
	line	108
	
l863:	
	line	109
	
l3162:	
;main.c: 107: }
;main.c: 108: }
;main.c: 109: if(dtc_counter>=4)
	movlw	(04h)
	subwf	(push_dtc_to_stack@dtc_counter),w
	skipc
	goto	u2401
	goto	u2400
u2401:
	goto	l3168
u2400:
	line	111
	
l3164:	
;main.c: 110: {
;main.c: 111: if(current_dtc_stack_copy[dtc_counter-4] == dtc_num)
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	0FCh
	addlw	push_dtc_to_stack@current_dtc_stack_copy&0ffh
	movwf	fsr0
	movf	indf,w
	xorwf	(push_dtc_to_stack@dtc_num),w
	skipz
	goto	u2411
	goto	u2410
u2411:
	goto	l3168
u2410:
	line	113
	
l3166:	
;main.c: 112: {
;main.c: 113: current_dtc_stack[dtc_counter] = 0;
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	clrf	indf
	goto	l3168
	line	114
	
l866:	
	goto	l3168
	line	115
	
l865:	
	line	116
	
l3168:	
;main.c: 114: }
;main.c: 115: }
;main.c: 116: if(dtc_counter>=5)
	movlw	(05h)
	subwf	(push_dtc_to_stack@dtc_counter),w
	skipc
	goto	u2421
	goto	u2420
u2421:
	goto	l3174
u2420:
	line	118
	
l3170:	
;main.c: 117: {
;main.c: 118: if(current_dtc_stack_copy[dtc_counter-5] == dtc_num)
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	0FBh
	addlw	push_dtc_to_stack@current_dtc_stack_copy&0ffh
	movwf	fsr0
	movf	indf,w
	xorwf	(push_dtc_to_stack@dtc_num),w
	skipz
	goto	u2431
	goto	u2430
u2431:
	goto	l3174
u2430:
	line	120
	
l3172:	
;main.c: 119: {
;main.c: 120: current_dtc_stack[dtc_counter] = 0;
	movf	(push_dtc_to_stack@dtc_counter),w
	addlw	_current_dtc_stack&0ffh
	movwf	fsr0
	clrf	indf
	goto	l3174
	line	121
	
l868:	
	goto	l3174
	line	122
	
l867:	
	line	124
	
l3174:	
;main.c: 121: }
;main.c: 122: }
;main.c: 124: dtc_counter++;
	movlw	(01h)
	movwf	(??_push_dtc_to_stack+0)+0
	movf	(??_push_dtc_to_stack+0)+0,w
	addwf	(push_dtc_to_stack@dtc_counter),f
	goto	l869
	line	125
	
l858:	
	line	128
	
l869:	
	return
	opt stack 0
GLOBAL	__end_of_push_dtc_to_stack
	__end_of_push_dtc_to_stack:
;; =============== function _push_dtc_to_stack ends ============

	signat	_push_dtc_to_stack,4216
psect	text76,local,class=CODE,delta=2
global __ptext76
__ptext76:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
