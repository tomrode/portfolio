
#include "htc.h"
#include "typedefs.h"
#include "timer.h"


/*Global variable*/
vuint8_t tmr1_isr_counter;      // volatile isr variable for timer 1 
uint16_t timer_array[TIMER_MAX]; 
GeneralFlagsSettingType FlagSet; 


/* Functions prototypes*/
void timer_isr(void);

/***************************************************************/
/* Timer1 main functions
/***************************************************************/

void timer_set(uint8_t index, uint16_t value)
{ 
 uint16_t array_contents;  
 extern_var = 45; 
 FlagSet.GeneralFlags.Flag2 = TRUE;                          
  if (index < TIMER_MAX)
     {
      di();                       // disable interrrupts 
      timer_array[index] = value;
      ei();                          
     }
   else
     {
     } 
} 


uint16_t get_timer(uint8_t index)
{
uint16_t result;

  if (index < TIMER_MAX)
  {
    di();
    result = timer_array[index];
    ei();
  }
  else
  {
    result = 0;
  }

  return result;

}



void timer_isr(void) 
{
uint8_t i;                        // index  
if((TMR1IE)&&(TMR1IF))
 {   
   
     TMR1IF=0;
     T1CON &= ~(0x01);
     TMR1L =   0x80;//0x08;              //0x08 for pic16f887A 0x89;THIS GIVES A 1.00910 ms roll over on scope for pic16f887
     TMR1H =   0xFF;
     T1CON |=  0x01;
       
    for (i = 0; i < TIMER_MAX; i++)
      {
        if (timer_array[i] != 0)
            {
            timer_array[i]--;
            }
         else
            {
            }
      }

}
 else
 {
 }
}

void timer_init(void)
{
uint8_t i;                        // index 
  for (i = 0; i < TIMER_MAX; i++)
  {
    timer_array[i]=0;
  }
}