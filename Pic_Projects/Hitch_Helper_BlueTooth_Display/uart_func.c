
#include <htc.h>
#include "uart_func.h"
#include "typedefs.h"

/* FUTURE USE TO ENCODE SPECIFIC BYTES WHILE MAINTAINING ENOUGH PAYLOAD CAPABILITY*/
#define SENSOR1_HI_DECODE   (0x20)
#define SENSOR2_HI_DECODE   (0x40)
#define SENSOR3_HI_DECODE   (0x80)
#define UART_INTERRUPT_DESIRED

     
    
/* Global variables to this module*/
static uint8_t uartrx_isr_flag;
static uint8_t uarttx_isr_flag;
#ifdef UART_TX_ENABLED
/*****************************************************************************************************/
/* function name: uart_tx()
/* DATE 5 APR 2013 
/* DESCRIPTION: This function will output 2 sensors (4bytes) ans possible alignment byte 
/*
/* INPUTS-> 6 bytes SENSOR1 Hi and Lo SENSOR2 Hi and Lo May need to put if Calibrated and aligned. 
/* OUTPUTS-> None
/*                         
/*****************************************************************************************************/
void uart_tx(uint8_t EepSen1HiValUart,uint8_t EepSen1LoValUart,uint8_t EepSen2HiValUart,uint8_t EepSen2LoValUart,uint8_t EepSen3HiValUart,uint8_t EepSen3LoValUart)
{
asm("nop");
asm("nop");
asm("nop");

/*******************************SEND SOME ACTUALL BYTES OUT HERE**************************************/ 
 di();                                        /* Nothing should interrupt putting this into a polling mode*/  
TXREG = EepSen1HiValUart;                     /* Send out hi byte with SENSOR 1 May need to encode this */             
    while (TRMT = 0)                          /* TMRT = transmit buffer Reg Empty flag*/                   
      {
       /* Wait here until transmittion is complete */
      }
     _delay(1000);                            /* 1000 around 330 micro seconds */
TXREG = EepSen1LoValUart;                     /* Send out lo byte with SENSOR 1 May need to encode this */              
    while (TRMT = 0)                          /* TMRT = transmit buffer Reg Empty flag*/                   
      {
       /* Wait here until transmittion is complete */
      }
     _delay(1000);                            /* 1000 around 330 micro seconds */
TXREG = EepSen2HiValUart;                     /* Send out hi byte with SENSOR 3 May need to encode this */             
     while (TRMT = 0)                         /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(1000);                            /* 1000 around 330 micro seconds */
TXREG = EepSen2LoValUart;                     /* Send out lo byte with SENSOR 3 May need to encode this */              
     while (TRMT = 0)                         /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(1000);                            /* 1000 around 330 micro seconds */ 
TXREG = EepSen3HiValUart;                     /* Send out hi byte with SENSOR 3 May need to encode this */             
     while (TRMT = 0)                         /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(1000);                            /* 1000 around 330 micro seconds */
TXREG = EepSen3LoValUart;                     /* Send out lo byte with SENSOR 3 May need to encode this */              
     while (TRMT = 0)                         /* TMRT = transmit buffer Reg Empty flag*/                   
     {
       /* Wait here until transmittion is complete */
     }
     _delay(1000);                            /* 1000 around 330 micro seconds */ 
     /* Maybe the sensor is aligned here byte it will be another input into this function*/
 ei();
}
#endif /* UART_TX_ENABLED*/
/*********************************************************************************************/
/* function name: uart_rx()
/* Date 4 DEC 2011
/* DESCRIPTION: 
/*
/* INPUTS  -> NONE
/* OUTPUTS -> 8-bit received read (in this case from CAPL)
/* Update history:
/* - 21 DEC 2011: Fixed the lock up added in a test for overrun if condition then 
/*                re-initialize the UART.
/* - 4 MAY 2013   Will use this with PIC16F887 Not sure if this overrun flag will give issue's
/*                will poll   
/*********************************************************************************************/
uint8_t uart_rx(void)
{
uint8_t rcv_byte;
while(RCIDL == TRUE)                            /* Wait until the Master sends out */
{}

  rcv_byte = RCREG;             /* init a receive */   

if (OERR ==1)
{
  CREN = 0;                      /* Shut off Receive */
  uart_init();                   /* try re-initialise*/ 
}
else
{
  while ((RCIF == 1) && (OERR != 1))
  {
     /* Wait until flag clears, There is a character in the FIFO */
  }
} 

return rcv_byte;  
}
#ifdef FILLRECEIVESENSORBUFF_ENABLED
/*********************************************************************************************
* FillRecieveSensorBuff()
* Description: This Function will incrementally get byte stream
*             and populate structure.
* INPUT:   NONE
* OUTPUT:  Pointer to UART_RECEIVE_SENSOR_STUCTURE type. Which will contain 6 bytes of 
*         sensor data.
*
* Notes:   10 June 2013 
*          Made a test to see if Structure passes correctly.     
*          1 JULY 2013
*          Put in filter to pass only good reads.       
*********************************************************************************************/
UART_RECEIVE_SENSOR_STUCTURE* FillRecieveSensorBuff(void)
{
//#define OUTPUT_TEST
 UART_RECEIVE_SENSOR_STUCTURE ReceivedSensorVal;
 UART_RECEIVE_SENSOR_STUCTURE ReceivedSensorValCopy;
 UART_RECEIVE_SENSOR_STUCTURE ReceivedSensorValLastGoodRead;
 uint8_t i;
 uint8_t Sen1HiMask;
 uint8_t Sen2HiMask;
 uint8_t Sen3HiMask;
 uint8_t temp;

  for(i=0; i<6; i++)
  {
 //  while(RCIDL == TRUE)                            /* Wait until the Master sends out */
 //   {}
   temp = uart_rx();
#ifdef OUTPUT_TEST
   if(i == 0)
   { 
     temp = 0x24;//uart_rx();                           /* Extract a byte from UART*/
   }
   if(i == 1)
   { 
     temp = 0x7A;//uart_rx();                           /* Extract a byte from UART*/
   }
   if(i == 2)
   { 
     temp = 0x46;//uart_rx();                           /* Extract a byte from UART*/
   }
   if(i == 3)
   { 
     temp = 0xE4;//uart_rx();                           /* Extract a byte from UART*/
   }
   if(i == 4)
   { 
     temp = 0x86;//uart_rx();                           /* Extract a byte from UART*/
   }
   if(i == 5)
   { 
     temp = 0x55;//uart_rx();                           /* Extract a byte from UART*/
   }
#endif 
  /* Get the Sensor 1 hi and lo byte and place into the receive structure*/
   Sen1HiMask = (temp & SENSOR1_HI_DECODE);
  
   if((Sen1HiMask == SENSOR1_HI_DECODE) && (i == 0))
   {   
     ReceivedSensorVal.Sen1HiValUart = (temp ^ SENSOR1_HI_DECODE);  /* Sen1 Hi byte Guarentee here*/
   }
   if(i==1)
   {
     ReceivedSensorVal.Sen1LoValUart = temp;
   } 
  /* Get the Sensor 2 hi and lo byte and place into the receive structure*/
   Sen2HiMask = (temp & SENSOR2_HI_DECODE);
   if((Sen2HiMask == SENSOR2_HI_DECODE) && (i == 2))
   {
       ReceivedSensorVal.Sen2HiValUart = (temp ^ SENSOR2_HI_DECODE);  /* Sen2 Hi byte Guarentee here*/
   }	
   if(i==3)
   {
      ReceivedSensorVal.Sen2LoValUart = temp;
   }
  /* Get the Sensor 3 hi and lo byte and place into the receive structure*/
   Sen3HiMask = (temp & SENSOR3_HI_DECODE);
   if((Sen3HiMask == SENSOR3_HI_DECODE) && (i == 4))
   {
     ReceivedSensorVal.Sen3HiValUart = (temp ^ SENSOR3_HI_DECODE);  /* Sen3 Hi byte Guarentee here*/
   }
   if(i==5)
   {
     ReceivedSensorVal.Sen3LoValUart = temp;
   }
 } /*END FOR LOOP*/
 ReceivedSensorValCopy = ReceivedSensorVal;                         /* Make a copy for the filter test*/
 if((ReceivedSensorValCopy.Sen1HiValUart <= 4) && (ReceivedSensorValCopy.Sen2HiValUart <= 6) && (ReceivedSensorValCopy.Sen3HiValUart <= 6))
 {  /* reading is considered good*/
	ReceivedSensorValLastGoodRead = ReceivedSensorValCopy;
	return &ReceivedSensorValCopy;
 }
 else
  {
    return &ReceivedSensorValLastGoodRead;
  }
}
#endif /*FILLRECEIVESENSORBUFF_ENABLED*/
#ifdef FILLRECEIVESENSORBUFF3BYTES_ENABLED
/*********************************************************************************************
* FillRecieveSensorBuff3Bytes()
* Description: This Function will incrementally get byte stream
*             and populate structure.
* INPUT:   NONE
* OUTPUT:  Pointer to UART_RECEIVE_SENSOR_STUCTURE type. Which will contain 3 bytes of 
*         sensor data.
*
* Notes:   10 June 2013 
*          Made a test to see if Structure passes correctly.     
*          1 JULY 2013
*          Put in filter to pass only good reads.  
*          6 JULY 2013
*          Will use same structure,which will automatically place zero in the SenxLoValUart
*          members.
*********************************************************************************************/
UART_RECEIVE_SENSOR_STUCTURE* FillRecieveSensorBuff3Bytes(void)
{
//#define OUTPUT_TEST_3BYTES
 UART_RECEIVE_SENSOR_STUCTURE ReceivedSensorVal;
 UART_RECEIVE_SENSOR_STUCTURE ReceivedSensorValCopy;
 UART_RECEIVE_SENSOR_STUCTURE ReceivedSensorValLastGoodRead;
 uint8_t i;
 uint8_t Sen1HiMask;
 uint8_t Sen2HiMask;
 uint8_t Sen3HiMask;
 uint8_t temp;

  for(i=0; i<3; i++)
  {
   temp = uart_rx();
#ifdef OUTPUT_TEST_3BYTES
   if(i == 0)
   { 
     temp = 0x24;//uart_rx();                           /* Extract a byte from UART*/
   }
   if(i == 1)
   { 
     temp = 0x46;//uart_rx();                           /* Extract a byte from UART*/
   }
   if(i == 2)
   { 
      temp = 0x86;//uart_rx();                           /* Extract a byte from UART*/
   }
#endif 
  /* Get the Sensor 1 hi and lo byte and place into the receive structure*/
   Sen1HiMask = (temp & SENSOR1_HI_DECODE);
  
   if((Sen1HiMask == SENSOR1_HI_DECODE) && (i == 0))
   {   
     ReceivedSensorVal.Sen1HiValUart = (temp ^ SENSOR1_HI_DECODE);  /* Sen1 Hi byte Guarentee here*/
   }
  /* Get the Sensor 2 hi and lo byte and place into the receive structure*/
   Sen2HiMask = (temp & SENSOR2_HI_DECODE);
   if((Sen2HiMask == SENSOR2_HI_DECODE) && (i == 1))
   {
       ReceivedSensorVal.Sen2HiValUart = (temp ^ SENSOR2_HI_DECODE);  /* Sen2 Hi byte Guarentee here*/
   }	
  /* Get the Sensor 3 hi and lo byte and place into the receive structure*/
   Sen3HiMask = (temp & SENSOR3_HI_DECODE);
   if((Sen3HiMask == SENSOR3_HI_DECODE) && (i == 2))
   {
     ReceivedSensorVal.Sen3HiValUart = (temp ^ SENSOR3_HI_DECODE);  /* Sen3 Hi byte Guarentee here*/
   }
 } /*END FOR LOOP*/
 ReceivedSensorValCopy = ReceivedSensorVal;                         /* Make a copy for the filter test*/
 if((ReceivedSensorValCopy.Sen1HiValUart <= 4) && (ReceivedSensorValCopy.Sen2HiValUart <= 6) && (ReceivedSensorValCopy.Sen3HiValUart <= 6))
 {  /* reading is considered good*/
	ReceivedSensorValLastGoodRead = ReceivedSensorValCopy;
	return &ReceivedSensorValCopy;
 }
 else
  {
    return &ReceivedSensorValLastGoodRead;
  }
}
#endif /*FILLRECEIVESENSORBUFF3BYTES_ENABLED*/
/*********************************************************************************************
* uart_init()
* Description: Initialize Universal asynchronous receive and transmitt module.
* 
* INPUTS: None
* OUTPUTS: None
* 
*         NOTES:
*            28 JUNED 2013
* Changed SPBGR timing, found the higher the baud rate the better results from the pulsewidthx
* return.
**********************************************************************************************/
void uart_init(void)
{
//RC6 44 � � � � TX/CK � � � �
//RC7 1 � � � � RX/DT
//TRISC    = 0x80;       /* Pin 26 RC7/RX/DT  PIN 25 Rc6/TX/DT 
SPBRG      = 0x06;//0x08;       /*08 made 18us or 55555 bps 19 made 52.2 us scope 19200 baud says 33 for 104 us Baud rate = FOSC/(16(X+1))  "51" at 8MHz at 9600 baud "25" at 8MHz at 19200 baud */ 
TXSTA      = 0x25;       /* |CSRC=0|TX9=0|TXEN=1|SYNC=0| -  |BRGH=1|TRMT=0|TX9D=0| */
RCSTA      = 0x90;       /* |SPEN=1(Serial port enabled)|RX9=0|SREN=0|CREN=1(RECEIVER ON)|ADDEN=0|FERR=0|OERR=0|RX9D=0|*/
}


#ifdef UART_INTERRUPT_DESIRED
void uartrx_isr(void)
{

  if ((RCIF==1)&&(RCIE==1))
  {
    RCIF=0;             // cleared by hardware
    uartrx_isr_flag=1;  // set the indication 
  }
  else 
  {
  }
}

 


void uarttx_isr(void)
{
  if ((TXIF)&&(TXIE))
  {
    
    TXIF=0;             // clear flag
    uarttx_isr_flag=1;  // set the indication 
     
  }
  else 
  {
  }
}
#endif //UART_INTERRUPT_DESIRED
