opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 7503"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 34 "F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 34 "F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 35 "F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 35 "F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_init_micro
	FNCALL	_main,_uart_init
	FNCALL	_main,_timer_init
	FNCALL	_main,_timer_set
	FNCALL	_main,_get_timer
	FNCALL	_main,_FillRecieveSensorBuff3Bytes
	FNCALL	_main,_OutLedBox
	FNCALL	_FillRecieveSensorBuff3Bytes,_uart_rx
	FNCALL	_uart_rx,_uart_init
	FNROOT	_main
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_uarttx_isr_flag
	global	_timer_array
	global	_FlagSet
	global	_extern_var
	global	_tmr1_isr_counter
	global	_uartrx_isr_flag
	global	_PORTA
psect	text382,local,class=CODE,delta=2
global __ptext382
__ptext382:
_PORTA	set	5
	global	_PORTB
_PORTB	set	6
	global	_PORTD
_PORTD	set	8
	global	_PORTE
_PORTE	set	9
	global	_RCREG
_RCREG	set	26
	global	_RCSTA
_RCSTA	set	24
	global	_T1CON
_T1CON	set	16
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_CREN
_CREN	set	196
	global	_GIE
_GIE	set	95
	global	_OERR
_OERR	set	193
	global	_PEIE
_PEIE	set	94
	global	_RCIF
_RCIF	set	101
	global	_TMR1IF
_TMR1IF	set	96
	global	_TXIF
_TXIF	set	100
	global	_OPTION
_OPTION	set	129
	global	_OSCCON
_OSCCON	set	143
	global	_SPBRG
_SPBRG	set	153
	global	_TRISA
_TRISA	set	133
	global	_TRISB
_TRISB	set	134
	global	_TRISD
_TRISD	set	136
	global	_TRISE
_TRISE	set	137
	global	_TXSTA
_TXSTA	set	152
	global	_RCIE
_RCIE	set	1125
	global	_TMR1IE
_TMR1IE	set	1120
	global	_TXIE
_TXIE	set	1124
	global	_ANSEL
_ANSEL	set	392
	global	_ANSELH
_ANSELH	set	393
	global	_RCIDL
_RCIDL	set	3134
	file	"Hitch_Helper_BlueTooth_Display.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_timer_array:
       ds      2

_FlagSet:
       ds      1

_extern_var:
       ds      1

_tmr1_isr_counter:
       ds      1

_uartrx_isr_flag:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_uarttx_isr_flag:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init_micro
?_init_micro:	; 0 bytes @ 0x0
	global	?_uart_init
?_uart_init:	; 0 bytes @ 0x0
	global	?_timer_init
?_timer_init:	; 0 bytes @ 0x0
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_FillRecieveSensorBuff3Bytes
?_FillRecieveSensorBuff3Bytes:	; 1 bytes @ 0x0
	global	?_uart_rx
?_uart_rx:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_init_micro
??_init_micro:	; 0 bytes @ 0x0
	global	??_uart_init
??_uart_init:	; 0 bytes @ 0x0
	global	??_timer_init
??_timer_init:	; 0 bytes @ 0x0
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	?_OutLedBox
?_OutLedBox:	; 0 bytes @ 0x0
	global	??_uart_rx
??_uart_rx:	; 0 bytes @ 0x0
	global	?_get_timer
?_get_timer:	; 2 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	global	OutLedBox@UartIn
OutLedBox@UartIn:	; 3 bytes @ 0x0
	ds	1
	global	timer_init@i
timer_init@i:	; 1 bytes @ 0x1
	global	uart_rx@rcv_byte
uart_rx@rcv_byte:	; 1 bytes @ 0x1
	ds	1
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	global	??_get_timer
??_get_timer:	; 0 bytes @ 0x2
	global	??_FillRecieveSensorBuff3Bytes
??_FillRecieveSensorBuff3Bytes:	; 0 bytes @ 0x2
	ds	1
	global	??_OutLedBox
??_OutLedBox:	; 0 bytes @ 0x3
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	global	get_timer@result
get_timer@result:	; 2 bytes @ 0x3
	global	FillRecieveSensorBuff3Bytes@ReceivedSensorValLastGoodRead
FillRecieveSensorBuff3Bytes@ReceivedSensorValLastGoodRead:	; 3 bytes @ 0x3
	ds	1
	global	OutLedBox@TestCompleteUartTransfer
OutLedBox@TestCompleteUartTransfer:	; 3 bytes @ 0x4
	ds	1
	global	get_timer@index
get_timer@index:	; 1 bytes @ 0x5
	ds	1
	global	FillRecieveSensorBuff3Bytes@Sen1HiMask
FillRecieveSensorBuff3Bytes@Sen1HiMask:	; 1 bytes @ 0x6
	ds	1
	global	FillRecieveSensorBuff3Bytes@Sen2HiMask
FillRecieveSensorBuff3Bytes@Sen2HiMask:	; 1 bytes @ 0x7
	ds	1
	global	FillRecieveSensorBuff3Bytes@Sen3HiMask
FillRecieveSensorBuff3Bytes@Sen3HiMask:	; 1 bytes @ 0x8
	ds	1
	global	FillRecieveSensorBuff3Bytes@ReceivedSensorVal
FillRecieveSensorBuff3Bytes@ReceivedSensorVal:	; 3 bytes @ 0x9
	ds	3
	global	FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy
FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy:	; 3 bytes @ 0xC
	ds	3
	global	FillRecieveSensorBuff3Bytes@i
FillRecieveSensorBuff3Bytes@i:	; 1 bytes @ 0xF
	ds	1
	global	FillRecieveSensorBuff3Bytes@temp
FillRecieveSensorBuff3Bytes@temp:	; 1 bytes @ 0x10
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x11
	ds	1
	global	main@CompleteUartTransfer
main@CompleteUartTransfer:	; 3 bytes @ 0x12
	ds	3
	global	main@INIT_STATE_MACHINE
main@INIT_STATE_MACHINE:	; 1 bytes @ 0x15
	ds	1
;;Data sizes: Strings 0, constant 0, data 0, bss 7, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80     22      23
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_FillRecieveSensorBuff3Bytes	PTR struct . size(1) Largest target is 3
;;		 -> FillRecieveSensorBuff3Bytes@ReceivedSensorValLastGoodRead(BANK0[3]), FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy(BANK0[3]), 
;;
;; ?_get_timer	unsigned short  size(1) Largest target is 0
;;
;; sp__FillRecieveSensorBuff3Bytes	PTR struct . size(1) Largest target is 3
;;		 -> FillRecieveSensorBuff3Bytes@ReceivedSensorValLastGoodRead(BANK0[3]), FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy(BANK0[3]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_FillRecieveSensorBuff3Bytes
;;   _FillRecieveSensorBuff3Bytes->_uart_rx
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 6     6      0    1871
;;                                             17 BANK0      5     5      0
;;                         _init_micro
;;                          _uart_init
;;                         _timer_init
;;                          _timer_set
;;                          _get_timer
;;        _FillRecieveSensorBuff3Bytes
;;                          _OutLedBox
;; ---------------------------------------------------------------------------------
;; (1) _FillRecieveSensorBuff3Bytes                         15    15      0     792
;;                                              2 BANK0     15    15      0
;;                            _uart_rx
;; ---------------------------------------------------------------------------------
;; (2) _uart_rx                                              2     2      0      34
;;                                              0 BANK0      2     2      0
;;                          _uart_init
;; ---------------------------------------------------------------------------------
;; (1) _OutLedBox                                            7     4      3     665
;;                                              0 BANK0      7     4      3
;; ---------------------------------------------------------------------------------
;; (1) _get_timer                                            6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (1) _timer_set                                            6     4      2      93
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _timer_init                                           2     2      0      99
;;                                              0 BANK0      2     2      0
;; ---------------------------------------------------------------------------------
;; (3) _uart_init                                            0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _init_micro                                           0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 3
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (4) _interrupt_handler                                    4     4      0      60
;;                                              2 COMMON     4     4      0
;;                          _timer_isr
;; ---------------------------------------------------------------------------------
;; (5) _timer_isr                                            2     2      0      60
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 5
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init_micro
;;   _uart_init
;;   _timer_init
;;   _timer_set
;;   _get_timer
;;   _FillRecieveSensorBuff3Bytes
;;     _uart_rx
;;       _uart_init
;;   _OutLedBox
;;
;; _interrupt_handler (ROOT)
;;   _timer_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       7       2        0.0%
;;ABS                  0      0      23       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     16      17       5       28.8%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      2A      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 61 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  CompleteUart    3   18[BANK0 ] struct .
;;  INIT_STATE_M    1   21[BANK0 ] unsigned char 
;;  INIT_BLINKS_    1    0        enum E844
;; Return value:  Size  Location     Type
;;                  2  852[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       4       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       5       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_init_micro
;;		_uart_init
;;		_timer_init
;;		_timer_set
;;		_get_timer
;;		_FillRecieveSensorBuff3Bytes
;;		_OutLedBox
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
	line	61
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 3
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	82
	
l5473:	
;main.c: 65: UART_RECEIVE_SENSOR_STUCTURE CompleteUartTransfer;
;main.c: 66: uint8_t INIT_STATE_MACHINE;
;main.c: 67: enum
;main.c: 68: {
;main.c: 69: LEFT_SIDE,
;main.c: 70: RIGHT_SIDE,
;main.c: 71: FWD_BCK,
;main.c: 72: OFF_DISPLAY_INIT,
;main.c: 73: EXIT_INIT
;main.c: 74: }INIT_BLINKS_STATES;
;main.c: 82: init_micro();
	fcall	_init_micro
	line	83
;main.c: 83: uart_init();
	fcall	_uart_init
	line	84
	
l5475:	
;main.c: 84: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	85
	
l5477:	
;main.c: 85: timer_init();
	fcall	_timer_init
	line	86
	
l5479:	
;main.c: 86: PEIE = 1;
	bsf	(94/8),(94)&7
	line	87
	
l5481:	
;main.c: 87: (GIE = 1);
	bsf	(95/8),(95)&7
	line	88
	
l5483:	
;main.c: 88: timer_set(DISPLAY_BLUETOOTH_TIMER, (2000));
	movlw	low(07D0h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(?_timer_set)
	movlw	high(07D0h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	89
	
l5485:	
;main.c: 89: INIT_STATE_MACHINE = LEFT_SIDE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@INIT_STATE_MACHINE)
	line	90
	
l5487:	
;main.c: 90: PORTB = 0, PORTD = 0, PORTE = 0;
	clrf	(6)	;volatile
	
l5489:	
	clrf	(8)	;volatile
	
l5491:	
	clrf	(9)	;volatile
	line	97
;main.c: 97: while(INIT_STATE_MACHINE != EXIT_INIT)
	goto	l5535
	
l854:	
	line	99
;main.c: 98: {
;main.c: 99: switch (INIT_STATE_MACHINE)
	goto	l5533
	line	101
;main.c: 100: {
;main.c: 101: case LEFT_SIDE:
	
l856:	
	line	102
	
l5493:	
;main.c: 102: if(get_timer(DISPLAY_BLUETOOTH_TIMER) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u3491
	goto	u3490
u3491:
	goto	l857
u3490:
	line	104
	
l5495:	
;main.c: 103: {
;main.c: 104: INIT_STATE_MACHINE = RIGHT_SIDE;
	clrf	(main@INIT_STATE_MACHINE)
	bsf	status,0
	rlf	(main@INIT_STATE_MACHINE),f
	line	105
	
l5497:	
;main.c: 105: timer_set(DISPLAY_BLUETOOTH_TIMER, (2000));
	movlw	low(07D0h)
	movwf	(?_timer_set)
	movlw	high(07D0h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	106
;main.c: 106: }
	goto	l5535
	line	107
	
l857:	
	line	109
;main.c: 107: else
;main.c: 108: {
;main.c: 109: INIT_STATE_MACHINE = LEFT_SIDE;
	clrf	(main@INIT_STATE_MACHINE)
	line	110
	
l5499:	
;main.c: 110: PORTD = 0xFC;
	movlw	(0FCh)
	movwf	(8)	;volatile
	goto	l5535
	line	111
	
l858:	
	line	112
;main.c: 111: }
;main.c: 112: break;
	goto	l5535
	line	114
;main.c: 114: case RIGHT_SIDE:
	
l860:	
	line	115
	
l5501:	
;main.c: 115: if(get_timer(DISPLAY_BLUETOOTH_TIMER) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u3501
	goto	u3500
u3501:
	goto	l5507
u3500:
	line	117
	
l5503:	
;main.c: 116: {
;main.c: 117: INIT_STATE_MACHINE = FWD_BCK;
	movlw	(02h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@INIT_STATE_MACHINE)
	line	118
	
l5505:	
;main.c: 118: timer_set(DISPLAY_BLUETOOTH_TIMER, (2000));
	movlw	low(07D0h)
	movwf	(?_timer_set)
	movlw	high(07D0h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	119
;main.c: 119: }
	goto	l5535
	line	120
	
l861:	
	line	122
	
l5507:	
;main.c: 120: else
;main.c: 121: {
;main.c: 122: INIT_STATE_MACHINE = RIGHT_SIDE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@INIT_STATE_MACHINE)
	bsf	status,0
	rlf	(main@INIT_STATE_MACHINE),f
	line	123
	
l5509:	
;main.c: 123: PORTB = 0x3C;
	movlw	(03Ch)
	movwf	(6)	;volatile
	line	124
	
l5511:	
;main.c: 124: PORTE = 0x03;
	movlw	(03h)
	movwf	(9)	;volatile
	goto	l5535
	line	125
	
l862:	
	line	126
;main.c: 125: }
;main.c: 126: break;
	goto	l5535
	line	128
;main.c: 128: case FWD_BCK:
	
l863:	
	line	129
	
l5513:	
;main.c: 129: if(get_timer(DISPLAY_BLUETOOTH_TIMER) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u3511
	goto	u3510
u3511:
	goto	l5519
u3510:
	line	131
	
l5515:	
;main.c: 130: {
;main.c: 131: INIT_STATE_MACHINE = OFF_DISPLAY_INIT;
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@INIT_STATE_MACHINE)
	line	132
	
l5517:	
;main.c: 132: timer_set(DISPLAY_BLUETOOTH_TIMER, (2000));
	movlw	low(07D0h)
	movwf	(?_timer_set)
	movlw	high(07D0h)
	movwf	((?_timer_set))+1
	movlw	(0)
	fcall	_timer_set
	line	133
;main.c: 133: }
	goto	l5535
	line	134
	
l864:	
	line	136
	
l5519:	
;main.c: 134: else
;main.c: 135: {
;main.c: 136: INIT_STATE_MACHINE = FWD_BCK;
	movlw	(02h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@INIT_STATE_MACHINE)
	line	137
;main.c: 137: PORTB = 0xFF;
	movlw	(0FFh)
	movwf	(6)	;volatile
	line	138
;main.c: 138: PORTD = 0xFF;
	movlw	(0FFh)
	movwf	(8)	;volatile
	line	139
;main.c: 139: PORTE = 0xFF;
	movlw	(0FFh)
	movwf	(9)	;volatile
	goto	l5535
	line	140
	
l865:	
	line	141
;main.c: 140: }
;main.c: 141: break;
	goto	l5535
	line	143
;main.c: 143: case OFF_DISPLAY_INIT:
	
l866:	
	line	144
	
l5521:	
;main.c: 144: if(get_timer(DISPLAY_BLUETOOTH_TIMER) == 0)
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	((1+(?_get_timer))),w
	iorwf	((0+(?_get_timer))),w
	skipz
	goto	u3521
	goto	u3520
u3521:
	goto	l5529
u3520:
	line	146
	
l5523:	
;main.c: 145: {
;main.c: 146: PORTB = 0;
	clrf	(6)	;volatile
	line	147
;main.c: 147: PORTD = 0;
	clrf	(8)	;volatile
	line	148
;main.c: 148: PORTE = 0;
	clrf	(9)	;volatile
	line	149
	
l5525:	
;main.c: 149: INIT_STATE_MACHINE = EXIT_INIT;
	movlw	(04h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@INIT_STATE_MACHINE)
	line	153
	
l5527:	
;main.c: 153: (GIE = 0);
	bcf	(95/8),(95)&7
	line	154
;main.c: 154: }
	goto	l5535
	line	155
	
l867:	
	line	157
	
l5529:	
;main.c: 155: else
;main.c: 156: {
;main.c: 157: INIT_STATE_MACHINE = OFF_DISPLAY_INIT;
	movlw	(03h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@INIT_STATE_MACHINE)
	goto	l5535
	line	158
	
l868:	
	line	159
;main.c: 158: }
;main.c: 159: break;
	goto	l5535
	line	161
;main.c: 161: default:
	
l869:	
	line	162
;main.c: 162: break;
	goto	l5535
	line	163
	
l5531:	
;main.c: 163: }
	goto	l5535
	line	99
	
l855:	
	
l5533:	
	movf	(main@INIT_STATE_MACHINE),w
	; Switch size 1, requested type "space"
; Number of cases is 4, Range of values is 0 to 3
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           13     7 (average)
; direct_byte           20     8 (fixed)
; jumptable            260     6 (fixed)
; rangetable             8     6 (fixed)
; spacedrange           14     9 (fixed)
; locatedrange           4     3 (fixed)
;	Chosen strategy is simple_byte

	opt asmopt_off
	xorlw	0^0	; case 0
	skipnz
	goto	l5493
	xorlw	1^0	; case 1
	skipnz
	goto	l5501
	xorlw	2^1	; case 2
	skipnz
	goto	l5513
	xorlw	3^2	; case 3
	skipnz
	goto	l5521
	goto	l5535
	opt asmopt_on

	line	163
	
l859:	
	goto	l5535
	line	164
	
l853:	
	line	97
	
l5535:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@INIT_STATE_MACHINE),w
	xorlw	04h
	skipz
	goto	u3531
	goto	u3530
u3531:
	goto	l5533
u3530:
	goto	l5537
	
l870:	
	goto	l5537
	line	166
;main.c: 164: }
;main.c: 166: while(1)
	
l871:	
	line	168
	
l5537:	
;main.c: 167: {
;main.c: 168: CompleteUartTransfer = *FillRecieveSensorBuff3Bytes();
	fcall	_FillRecieveSensorBuff3Bytes
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@CompleteUartTransfer)
	incf	fsr0,f
	movf	indf,w
	movwf	(main@CompleteUartTransfer+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(main@CompleteUartTransfer+2)
	line	169
	
l5539:	
;main.c: 169: OutLedBox(CompleteUartTransfer);
	movf	(main@CompleteUartTransfer),w
	movwf	(?_OutLedBox)
	movf	(main@CompleteUartTransfer+1),w
	movwf	(?_OutLedBox+1)
	movf	(main@CompleteUartTransfer+2),w
	movwf	(?_OutLedBox+2)
	fcall	_OutLedBox
	line	170
	
l5541:	
# 170 "F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
nop ;#
psect	maintext
	goto	l5537
	line	171
	
l872:	
	line	166
	goto	l5537
	
l873:	
	line	172
	
l874:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_FillRecieveSensorBuff3Bytes
psect	text383,local,class=CODE,delta=2
global __ptext383
__ptext383:

;; *************** function _FillRecieveSensorBuff3Bytes *****************
;; Defined at:
;;		line 230 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\uart_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  ReceivedSens    3   12[BANK0 ] struct .
;;  ReceivedSens    3    9[BANK0 ] struct .
;;  ReceivedSens    3    3[BANK0 ] struct .
;;  temp            1   16[BANK0 ] unsigned char 
;;  i               1   15[BANK0 ] unsigned char 
;;  Sen3HiMask      1    8[BANK0 ] unsigned char 
;;  Sen2HiMask      1    7[BANK0 ] unsigned char 
;;  Sen1HiMask      1    6[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      PTR struct .
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      14       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0      15       0       0       0
;;Total ram usage:       15 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_uart_rx
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text383
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\uart_func.c"
	line	230
	global	__size_of_FillRecieveSensorBuff3Bytes
	__size_of_FillRecieveSensorBuff3Bytes	equ	__end_of_FillRecieveSensorBuff3Bytes-_FillRecieveSensorBuff3Bytes
	
_FillRecieveSensorBuff3Bytes:	
	opt	stack 3
; Regs used in _FillRecieveSensorBuff3Bytes: [wreg+status,2+status,0+pclath+cstack]
	line	241
	
l5425:	
;uart_func.c: 232: UART_RECEIVE_SENSOR_STUCTURE ReceivedSensorVal;
;uart_func.c: 233: UART_RECEIVE_SENSOR_STUCTURE ReceivedSensorValCopy;
;uart_func.c: 234: UART_RECEIVE_SENSOR_STUCTURE ReceivedSensorValLastGoodRead;
;uart_func.c: 235: uint8_t i;
;uart_func.c: 236: uint8_t Sen1HiMask;
;uart_func.c: 237: uint8_t Sen2HiMask;
;uart_func.c: 238: uint8_t Sen3HiMask;
;uart_func.c: 239: uint8_t temp;
;uart_func.c: 241: for(i=0; i<3; i++)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(FillRecieveSensorBuff3Bytes@i)
	
l5427:	
	movlw	(03h)
	subwf	(FillRecieveSensorBuff3Bytes@i),w
	skipc
	goto	u3381
	goto	u3380
u3381:
	goto	l5431
u3380:
	goto	l5457
	
l5429:	
	goto	l5457
	line	242
	
l2617:	
	line	243
	
l5431:	
;uart_func.c: 242: {
;uart_func.c: 243: temp = uart_rx();
	fcall	_uart_rx
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_FillRecieveSensorBuff3Bytes+0)+0
	movf	(??_FillRecieveSensorBuff3Bytes+0)+0,w
	movwf	(FillRecieveSensorBuff3Bytes@temp)
	line	259
	
l5433:	
;uart_func.c: 259: Sen1HiMask = (temp & (0x20));
	movf	(FillRecieveSensorBuff3Bytes@temp),w
	andlw	020h
	movwf	(??_FillRecieveSensorBuff3Bytes+0)+0
	movf	(??_FillRecieveSensorBuff3Bytes+0)+0,w
	movwf	(FillRecieveSensorBuff3Bytes@Sen1HiMask)
	line	261
	
l5435:	
;uart_func.c: 261: if((Sen1HiMask == (0x20)) && (i == 0))
	movf	(FillRecieveSensorBuff3Bytes@Sen1HiMask),w
	xorlw	020h
	skipz
	goto	u3391
	goto	u3390
u3391:
	goto	l2619
u3390:
	
l5437:	
	movf	(FillRecieveSensorBuff3Bytes@i),f
	skipz
	goto	u3401
	goto	u3400
u3401:
	goto	l2619
u3400:
	line	263
	
l5439:	
;uart_func.c: 262: {
;uart_func.c: 263: ReceivedSensorVal.Sen1HiValUart = (temp ^ (0x20));
	movf	(FillRecieveSensorBuff3Bytes@temp),w
	xorlw	020h
	movwf	(??_FillRecieveSensorBuff3Bytes+0)+0
	movf	(??_FillRecieveSensorBuff3Bytes+0)+0,w
	movwf	(FillRecieveSensorBuff3Bytes@ReceivedSensorVal)
	line	264
	
l2619:	
	line	266
;uart_func.c: 264: }
;uart_func.c: 266: Sen2HiMask = (temp & (0x40));
	movf	(FillRecieveSensorBuff3Bytes@temp),w
	andlw	040h
	movwf	(??_FillRecieveSensorBuff3Bytes+0)+0
	movf	(??_FillRecieveSensorBuff3Bytes+0)+0,w
	movwf	(FillRecieveSensorBuff3Bytes@Sen2HiMask)
	line	267
	
l5441:	
;uart_func.c: 267: if((Sen2HiMask == (0x40)) && (i == 1))
	movf	(FillRecieveSensorBuff3Bytes@Sen2HiMask),w
	xorlw	040h
	skipz
	goto	u3411
	goto	u3410
u3411:
	goto	l2620
u3410:
	
l5443:	
	movf	(FillRecieveSensorBuff3Bytes@i),w
	xorlw	01h
	skipz
	goto	u3421
	goto	u3420
u3421:
	goto	l2620
u3420:
	line	269
	
l5445:	
;uart_func.c: 268: {
;uart_func.c: 269: ReceivedSensorVal.Sen2HiValUart = (temp ^ (0x40));
	movf	(FillRecieveSensorBuff3Bytes@temp),w
	xorlw	040h
	movwf	(??_FillRecieveSensorBuff3Bytes+0)+0
	movf	(??_FillRecieveSensorBuff3Bytes+0)+0,w
	movwf	0+(FillRecieveSensorBuff3Bytes@ReceivedSensorVal)+01h
	line	270
	
l2620:	
	line	272
;uart_func.c: 270: }
;uart_func.c: 272: Sen3HiMask = (temp & (0x80));
	movf	(FillRecieveSensorBuff3Bytes@temp),w
	andlw	080h
	movwf	(??_FillRecieveSensorBuff3Bytes+0)+0
	movf	(??_FillRecieveSensorBuff3Bytes+0)+0,w
	movwf	(FillRecieveSensorBuff3Bytes@Sen3HiMask)
	line	273
	
l5447:	
;uart_func.c: 273: if((Sen3HiMask == (0x80)) && (i == 2))
	movf	(FillRecieveSensorBuff3Bytes@Sen3HiMask),w
	xorlw	080h
	skipz
	goto	u3431
	goto	u3430
u3431:
	goto	l5453
u3430:
	
l5449:	
	movf	(FillRecieveSensorBuff3Bytes@i),w
	xorlw	02h
	skipz
	goto	u3441
	goto	u3440
u3441:
	goto	l5453
u3440:
	line	275
	
l5451:	
;uart_func.c: 274: {
;uart_func.c: 275: ReceivedSensorVal.Sen3HiValUart = (temp ^ (0x80));
	movf	(FillRecieveSensorBuff3Bytes@temp),w
	xorlw	080h
	movwf	(??_FillRecieveSensorBuff3Bytes+0)+0
	movf	(??_FillRecieveSensorBuff3Bytes+0)+0,w
	movwf	0+(FillRecieveSensorBuff3Bytes@ReceivedSensorVal)+02h
	goto	l5453
	line	276
	
l2621:	
	line	241
	
l5453:	
	movlw	(01h)
	movwf	(??_FillRecieveSensorBuff3Bytes+0)+0
	movf	(??_FillRecieveSensorBuff3Bytes+0)+0,w
	addwf	(FillRecieveSensorBuff3Bytes@i),f
	
l5455:	
	movlw	(03h)
	subwf	(FillRecieveSensorBuff3Bytes@i),w
	skipc
	goto	u3451
	goto	u3450
u3451:
	goto	l5431
u3450:
	goto	l5457
	
l2618:	
	line	278
	
l5457:	
;uart_func.c: 276: }
;uart_func.c: 277: }
;uart_func.c: 278: ReceivedSensorValCopy = ReceivedSensorVal;
	movf	(FillRecieveSensorBuff3Bytes@ReceivedSensorVal),w
	movwf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy)
	movf	(FillRecieveSensorBuff3Bytes@ReceivedSensorVal+1),w
	movwf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy+1)
	movf	(FillRecieveSensorBuff3Bytes@ReceivedSensorVal+2),w
	movwf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy+2)
	line	279
;uart_func.c: 279: if((ReceivedSensorValCopy.Sen1HiValUart <= 4) && (ReceivedSensorValCopy.Sen2HiValUart <= 6) && (ReceivedSensorValCopy.Sen3HiValUart <= 6))
	movlw	(05h)
	subwf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy),w
	skipnc
	goto	u3461
	goto	u3460
u3461:
	goto	l5469
u3460:
	
l5459:	
	movlw	(07h)
	subwf	0+(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy)+01h,w
	skipnc
	goto	u3471
	goto	u3470
u3471:
	goto	l5469
u3470:
	
l5461:	
	movlw	(07h)
	subwf	0+(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy)+02h,w
	skipnc
	goto	u3481
	goto	u3480
u3481:
	goto	l5469
u3480:
	line	281
	
l5463:	
;uart_func.c: 280: {
;uart_func.c: 281: ReceivedSensorValLastGoodRead = ReceivedSensorValCopy;
	movf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy),w
	movwf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValLastGoodRead)
	movf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy+1),w
	movwf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValLastGoodRead+1)
	movf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy+2),w
	movwf	(FillRecieveSensorBuff3Bytes@ReceivedSensorValLastGoodRead+2)
	line	282
;uart_func.c: 282: return &ReceivedSensorValCopy;
	movlw	(FillRecieveSensorBuff3Bytes@ReceivedSensorValCopy)&0ffh
	goto	l2623
	
l5465:	
	goto	l2623
	line	283
	
l5467:	
;uart_func.c: 283: }
	goto	l2623
	line	284
	
l2622:	
	line	286
	
l5469:	
;uart_func.c: 284: else
;uart_func.c: 285: {
;uart_func.c: 286: return &ReceivedSensorValLastGoodRead;
	movlw	(FillRecieveSensorBuff3Bytes@ReceivedSensorValLastGoodRead)&0ffh
	goto	l2623
	
l5471:	
	goto	l2623
	line	287
	
l2624:	
	line	288
	
l2623:	
	return
	opt stack 0
GLOBAL	__end_of_FillRecieveSensorBuff3Bytes
	__end_of_FillRecieveSensorBuff3Bytes:
;; =============== function _FillRecieveSensorBuff3Bytes ends ============

	signat	_FillRecieveSensorBuff3Bytes,89
	global	_uart_rx
psect	text384,local,class=CODE,delta=2
global __ptext384
__ptext384:

;; *************** function _uart_rx *****************
;; Defined at:
;;		line 89 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\uart_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  rcv_byte        1    1[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_uart_init
;; This function is called by:
;;		_FillRecieveSensorBuff3Bytes
;; This function uses a non-reentrant model
;;
psect	text384
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\uart_func.c"
	line	89
	global	__size_of_uart_rx
	__size_of_uart_rx	equ	__end_of_uart_rx-_uart_rx
	
_uart_rx:	
	opt	stack 3
; Regs used in _uart_rx: [wreg+status,2+status,0+pclath+cstack]
	line	91
	
l5409:	
;uart_func.c: 90: uint8_t rcv_byte;
;uart_func.c: 91: while(RCIDL == TRUE)
	goto	l2604
	
l2605:	
	line	92
;uart_func.c: 92: {}
	
l2604:	
	line	91
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	btfsc	(3134/8)^0180h,(3134)&7
	goto	u3341
	goto	u3340
u3341:
	goto	l2604
u3340:
	goto	l5411
	
l2606:	
	line	94
	
l5411:	
;uart_func.c: 94: rcv_byte = RCREG;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(26),w	;volatile
	movwf	(??_uart_rx+0)+0
	movf	(??_uart_rx+0)+0,w
	movwf	(uart_rx@rcv_byte)
	line	96
	
l5413:	
;uart_func.c: 96: if (OERR ==1)
	btfss	(193/8),(193)&7
	goto	u3351
	goto	u3350
u3351:
	goto	l2609
u3350:
	line	98
	
l5415:	
;uart_func.c: 97: {
;uart_func.c: 98: CREN = 0;
	bcf	(196/8),(196)&7
	line	99
	
l5417:	
;uart_func.c: 99: uart_init();
	fcall	_uart_init
	line	100
;uart_func.c: 100: }
	goto	l5421
	line	101
	
l2607:	
	line	103
;uart_func.c: 101: else
;uart_func.c: 102: {
;uart_func.c: 103: while ((RCIF == 1) && (OERR != 1))
	goto	l2609
	
l2610:	
	line	106
;uart_func.c: 104: {
	
l2609:	
	line	103
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(101/8),(101)&7
	goto	u3361
	goto	u3360
u3361:
	goto	l5421
u3360:
	
l5419:	
	btfss	(193/8),(193)&7
	goto	u3371
	goto	u3370
u3371:
	goto	l2609
u3370:
	goto	l5421
	
l2612:	
	goto	l5421
	
l2613:	
	goto	l5421
	line	107
	
l2608:	
	line	109
	
l5421:	
;uart_func.c: 106: }
;uart_func.c: 107: }
;uart_func.c: 109: return rcv_byte;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(uart_rx@rcv_byte),w
	goto	l2614
	
l5423:	
	line	110
	
l2614:	
	return
	opt stack 0
GLOBAL	__end_of_uart_rx
	__end_of_uart_rx:
;; =============== function _uart_rx ends ============

	signat	_uart_rx,89
	global	_OutLedBox
psect	text385,local,class=CODE,delta=2
global __ptext385
__ptext385:

;; *************** function _OutLedBox *****************
;; Defined at:
;;		line 205 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
;; Parameters:    Size  Location     Type
;;  UartIn          3    0[BANK0 ] struct .
;; Auto vars:     Size  Location     Type
;;  TestComplete    3    4[BANK0 ] struct .
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       3       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       7       0       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text385
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
	line	205
	global	__size_of_OutLedBox
	__size_of_OutLedBox	equ	__end_of_OutLedBox-_OutLedBox
	
_OutLedBox:	
	opt	stack 5
; Regs used in _OutLedBox: [wreg+status,2+status,0]
	line	207
	
l5311:	
;main.c: 206: UART_RECEIVE_SENSOR_STUCTURE TestCompleteUartTransfer;
;main.c: 207: TestCompleteUartTransfer = UartIn;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(OutLedBox@UartIn),w
	movwf	(OutLedBox@TestCompleteUartTransfer)
	movf	(OutLedBox@UartIn+1),w
	movwf	(OutLedBox@TestCompleteUartTransfer+1)
	movf	(OutLedBox@UartIn+2),w
	movwf	(OutLedBox@TestCompleteUartTransfer+2)
	line	209
	
l5313:	
;main.c: 209: PORTB = 0;
	clrf	(6)	;volatile
	line	210
	
l5315:	
;main.c: 210: PORTD = 0;
	clrf	(8)	;volatile
	line	211
	
l5317:	
;main.c: 211: PORTE = 0;
	clrf	(9)	;volatile
	line	212
	
l5319:	
;main.c: 212: if(UartIn.Sen3HiValUart > 6)
	movlw	(07h)
	subwf	0+(OutLedBox@UartIn)+02h,w
	skipc
	goto	u3151
	goto	u3150
u3151:
	goto	l5323
u3150:
	line	214
	
l5321:	
;main.c: 213: {
;main.c: 214: PORTD &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	215
;main.c: 215: PORTD ^= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l5323
	line	216
	
l877:	
	line	217
	
l5323:	
;main.c: 216: }
;main.c: 217: if(UartIn.Sen3HiValUart == 6)
	movf	0+(OutLedBox@UartIn)+02h,w
	xorlw	06h
	skipz
	goto	u3161
	goto	u3160
u3161:
	goto	l5327
u3160:
	line	219
	
l5325:	
;main.c: 218: {
;main.c: 219: PORTD &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	220
;main.c: 220: PORTD ^= 0x80;
	movlw	(080h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l5327
	line	221
	
l878:	
	line	222
	
l5327:	
;main.c: 221: }
;main.c: 222: if(UartIn.Sen3HiValUart == 5)
	movf	0+(OutLedBox@UartIn)+02h,w
	xorlw	05h
	skipz
	goto	u3171
	goto	u3170
u3171:
	goto	l5331
u3170:
	line	224
	
l5329:	
;main.c: 223: {
;main.c: 224: PORTD &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	225
;main.c: 225: PORTD ^= 0x40;
	movlw	(040h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l5331
	line	226
	
l879:	
	line	227
	
l5331:	
;main.c: 226: }
;main.c: 227: if(UartIn.Sen3HiValUart == 4)
	movf	0+(OutLedBox@UartIn)+02h,w
	xorlw	04h
	skipz
	goto	u3181
	goto	u3180
u3181:
	goto	l5335
u3180:
	line	229
	
l5333:	
;main.c: 228: {
;main.c: 229: PORTD &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	230
;main.c: 230: PORTD ^= 0x20;
	movlw	(020h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l5335
	line	231
	
l880:	
	line	232
	
l5335:	
;main.c: 231: }
;main.c: 232: if(UartIn.Sen3HiValUart == 3)
	movf	0+(OutLedBox@UartIn)+02h,w
	xorlw	03h
	skipz
	goto	u3191
	goto	u3190
u3191:
	goto	l5339
u3190:
	line	234
	
l5337:	
;main.c: 233: {
;main.c: 234: PORTD &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	235
;main.c: 235: PORTD ^= 0x10;
	movlw	(010h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l5339
	line	236
	
l881:	
	line	237
	
l5339:	
;main.c: 236: }
;main.c: 237: if(UartIn.Sen3HiValUart == 2)
	movf	0+(OutLedBox@UartIn)+02h,w
	xorlw	02h
	skipz
	goto	u3201
	goto	u3200
u3201:
	goto	l5343
u3200:
	line	239
	
l5341:	
;main.c: 238: {
;main.c: 239: PORTD &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	240
;main.c: 240: PORTD ^= 0x08;
	movlw	(08h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l5343
	line	241
	
l882:	
	line	242
	
l5343:	
;main.c: 241: }
;main.c: 242: if(UartIn.Sen3HiValUart <= 1)
	movlw	(02h)
	subwf	0+(OutLedBox@UartIn)+02h,w
	skipnc
	goto	u3211
	goto	u3210
u3211:
	goto	l5347
u3210:
	line	244
	
l5345:	
;main.c: 243: {
;main.c: 244: PORTD &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	245
;main.c: 245: PORTD ^= 0x04;
	movlw	(04h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l5347
	line	246
	
l883:	
	line	249
	
l5347:	
;main.c: 246: }
;main.c: 249: if(UartIn.Sen2HiValUart > 6)
	movlw	(07h)
	subwf	0+(OutLedBox@UartIn)+01h,w
	skipc
	goto	u3221
	goto	u3220
u3221:
	goto	l5353
u3220:
	line	251
	
l5349:	
;main.c: 250: {
;main.c: 251: PORTB &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	252
;main.c: 252: PORTB ^= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	line	253
	
l5351:	
;main.c: 253: PORTE = 0x03;
	movlw	(03h)
	movwf	(9)	;volatile
	goto	l5353
	line	254
	
l884:	
	line	255
	
l5353:	
;main.c: 254: }
;main.c: 255: if(UartIn.Sen2HiValUart == 6)
	movf	0+(OutLedBox@UartIn)+01h,w
	xorlw	06h
	skipz
	goto	u3231
	goto	u3230
u3231:
	goto	l5359
u3230:
	line	257
	
l5355:	
;main.c: 256: {
;main.c: 257: PORTB &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	258
;main.c: 258: PORTB ^= 0x80;
	movlw	(080h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	line	259
	
l5357:	
;main.c: 259: PORTE = 0x01;
	movlw	(01h)
	movwf	(9)	;volatile
	goto	l5359
	line	260
	
l885:	
	line	261
	
l5359:	
;main.c: 260: }
;main.c: 261: if(UartIn.Sen2HiValUart == 5)
	movf	0+(OutLedBox@UartIn)+01h,w
	xorlw	05h
	skipz
	goto	u3241
	goto	u3240
u3241:
	goto	l5365
u3240:
	line	263
	
l5361:	
;main.c: 262: {
;main.c: 263: PORTB &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	264
;main.c: 264: PORTB ^= 0x40;
	movlw	(040h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	line	265
	
l5363:	
;main.c: 265: PORTE = 0x02;
	movlw	(02h)
	movwf	(9)	;volatile
	goto	l5365
	line	266
	
l886:	
	line	267
	
l5365:	
;main.c: 266: }
;main.c: 267: if(UartIn.Sen2HiValUart == 4)
	movf	0+(OutLedBox@UartIn)+01h,w
	xorlw	04h
	skipz
	goto	u3251
	goto	u3250
u3251:
	goto	l5371
u3250:
	line	269
	
l5367:	
;main.c: 268: {
;main.c: 269: PORTB &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	270
;main.c: 270: PORTB ^= 0x20;
	movlw	(020h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	line	271
	
l5369:	
;main.c: 271: PORTE = 0x00;
	clrf	(9)	;volatile
	goto	l5371
	line	272
	
l887:	
	line	273
	
l5371:	
;main.c: 272: }
;main.c: 273: if(UartIn.Sen2HiValUart == 3)
	movf	0+(OutLedBox@UartIn)+01h,w
	xorlw	03h
	skipz
	goto	u3261
	goto	u3260
u3261:
	goto	l5377
u3260:
	line	275
	
l5373:	
;main.c: 274: {
;main.c: 275: PORTB &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	276
;main.c: 276: PORTB ^= 0x10;
	movlw	(010h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	line	277
	
l5375:	
;main.c: 277: PORTE = 0x00;
	clrf	(9)	;volatile
	goto	l5377
	line	278
	
l888:	
	line	279
	
l5377:	
;main.c: 278: }
;main.c: 279: if(UartIn.Sen2HiValUart == 2)
	movf	0+(OutLedBox@UartIn)+01h,w
	xorlw	02h
	skipz
	goto	u3271
	goto	u3270
u3271:
	goto	l5383
u3270:
	line	281
	
l5379:	
;main.c: 280: {
;main.c: 281: PORTB &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	282
;main.c: 282: PORTB ^= 0x08;
	movlw	(08h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	line	283
	
l5381:	
;main.c: 283: PORTE = 0x00;
	clrf	(9)	;volatile
	goto	l5383
	line	284
	
l889:	
	line	285
	
l5383:	
;main.c: 284: }
;main.c: 285: if(UartIn.Sen2HiValUart <= 1)
	movlw	(02h)
	subwf	0+(OutLedBox@UartIn)+01h,w
	skipnc
	goto	u3281
	goto	u3280
u3281:
	goto	l5389
u3280:
	line	287
	
l5385:	
;main.c: 286: {
;main.c: 287: PORTB &= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	288
;main.c: 288: PORTB ^= 0x04;
	movlw	(04h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	line	289
	
l5387:	
;main.c: 289: PORTE = 0x00;
	clrf	(9)	;volatile
	goto	l5389
	line	290
	
l890:	
	line	292
	
l5389:	
;main.c: 290: }
;main.c: 292: if(UartIn.Sen1HiValUart > 4)
	movlw	(05h)
	subwf	(OutLedBox@UartIn),w
	skipc
	goto	u3291
	goto	u3290
u3291:
	goto	l5393
u3290:
	line	294
	
l5391:	
;main.c: 293: {
;main.c: 294: PORTB &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	295
;main.c: 295: PORTD &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	296
;main.c: 296: PORTB ^= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	line	297
;main.c: 297: PORTD ^= 0x03;
	movlw	(03h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l5393
	line	298
	
l891:	
	line	299
	
l5393:	
;main.c: 298: }
;main.c: 299: if(UartIn.Sen1HiValUart == 4)
	movf	(OutLedBox@UartIn),w
	xorlw	04h
	skipz
	goto	u3301
	goto	u3300
u3301:
	goto	l5397
u3300:
	line	301
	
l5395:	
;main.c: 300: {
;main.c: 301: PORTB &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	302
;main.c: 302: PORTD &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	303
;main.c: 303: PORTB ^= 0x01;
	movlw	(01h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	goto	l5397
	line	304
	
l892:	
	line	305
	
l5397:	
;main.c: 304: }
;main.c: 305: if(UartIn.Sen1HiValUart == 3)
	movf	(OutLedBox@UartIn),w
	xorlw	03h
	skipz
	goto	u3311
	goto	u3310
u3311:
	goto	l5401
u3310:
	line	307
	
l5399:	
;main.c: 306: {
;main.c: 307: PORTB &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	308
;main.c: 308: PORTD &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	309
;main.c: 309: PORTB ^= 0x02;
	movlw	(02h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(6),f	;volatile
	goto	l5401
	line	310
	
l893:	
	line	311
	
l5401:	
;main.c: 310: }
;main.c: 311: if(UartIn.Sen1HiValUart == 2)
	movf	(OutLedBox@UartIn),w
	xorlw	02h
	skipz
	goto	u3321
	goto	u3320
u3321:
	goto	l5405
u3320:
	line	313
	
l5403:	
;main.c: 312: {
;main.c: 313: PORTD &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	314
;main.c: 314: PORTB &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	315
;main.c: 315: PORTD ^= 0x01;
	movlw	(01h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l5405
	line	316
	
l894:	
	line	317
	
l5405:	
;main.c: 316: }
;main.c: 317: if(UartIn.Sen1HiValUart <= 1)
	movlw	(02h)
	subwf	(OutLedBox@UartIn),w
	skipnc
	goto	u3331
	goto	u3330
u3331:
	goto	l896
u3330:
	line	319
	
l5407:	
;main.c: 318: {
;main.c: 319: PORTD &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(8),f	;volatile
	line	320
;main.c: 320: PORTB &= 0xFC;
	movlw	(0FCh)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	andwf	(6),f	;volatile
	line	321
;main.c: 321: PORTD ^= 0x02;
	movlw	(02h)
	movwf	(??_OutLedBox+0)+0
	movf	(??_OutLedBox+0)+0,w
	xorwf	(8),f	;volatile
	goto	l896
	line	322
	
l895:	
	line	323
	
l896:	
	return
	opt stack 0
GLOBAL	__end_of_OutLedBox
	__end_of_OutLedBox:
;; =============== function _OutLedBox ends ============

	signat	_OutLedBox,4216
	global	_get_timer
psect	text386,local,class=CODE,delta=2
global __ptext386
__ptext386:

;; *************** function _get_timer *****************
;; Defined at:
;;		line 38 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  index           1    5[BANK0 ] unsigned char 
;;  result          2    3[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text386
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\timer.c"
	line	38
	global	__size_of_get_timer
	__size_of_get_timer	equ	__end_of_get_timer-_get_timer
	
_get_timer:	
	opt	stack 5
; Regs used in _get_timer: [wreg-fsr0h+status,2+status,0]
;get_timer@index stored from wreg
	line	41
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_timer@index)
	
l5297:	
;timer.c: 39: uint16_t result;
;timer.c: 41: if (index < TIMER_MAX)
	movf	(get_timer@index),f
	skipz
	goto	u3141
	goto	u3140
u3141:
	goto	l5305
u3140:
	line	43
	
l5299:	
;timer.c: 42: {
;timer.c: 43: (GIE = 0);
	bcf	(95/8),(95)&7
	line	44
	
l5301:	
;timer.c: 44: result = timer_array[index];
	movf	(get_timer@index),w
	movwf	(??_get_timer+0)+0
	addwf	(??_get_timer+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	indf,w
	movwf	(get_timer@result)
	incf	fsr0,f
	movf	indf,w
	movwf	(get_timer@result+1)
	line	45
	
l5303:	
;timer.c: 45: (GIE = 1);
	bsf	(95/8),(95)&7
	line	46
;timer.c: 46: }
	goto	l5307
	line	47
	
l1747:	
	line	49
	
l5305:	
;timer.c: 47: else
;timer.c: 48: {
;timer.c: 49: result = 0;
	clrf	(get_timer@result)
	clrf	(get_timer@result+1)
	goto	l5307
	line	50
	
l1748:	
	line	52
	
l5307:	
;timer.c: 50: }
;timer.c: 52: return result;
	movf	(get_timer@result+1),w
	clrf	(?_get_timer+1)
	addwf	(?_get_timer+1)
	movf	(get_timer@result),w
	clrf	(?_get_timer)
	addwf	(?_get_timer)

	goto	l1749
	
l5309:	
	line	54
	
l1749:	
	return
	opt stack 0
GLOBAL	__end_of_get_timer
	__end_of_get_timer:
;; =============== function _get_timer ends ============

	signat	_get_timer,4218
	global	_timer_set
psect	text387,local,class=CODE,delta=2
global __ptext387
__ptext387:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 21 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text387
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\timer.c"
	line	21
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 5
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	23
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l5285:	
;timer.c: 22: uint16_t array_contents;
;timer.c: 23: extern_var = 45;
	movlw	(02Dh)
	movwf	(??_timer_set+0)+0
	movf	(??_timer_set+0)+0,w
	movwf	(_extern_var)
	line	24
	
l5287:	
;timer.c: 24: FlagSet.GeneralFlags.Flag2 = TRUE;
	bsf	(_FlagSet),1
	line	25
	
l5289:	
;timer.c: 25: if (index < TIMER_MAX)
	movf	(timer_set@index),f
	skipz
	goto	u3131
	goto	u3130
u3131:
	goto	l1744
u3130:
	line	27
	
l5291:	
;timer.c: 26: {
;timer.c: 27: (GIE = 0);
	bcf	(95/8),(95)&7
	line	28
	
l5293:	
;timer.c: 28: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	29
	
l5295:	
;timer.c: 29: (GIE = 1);
	bsf	(95/8),(95)&7
	line	30
;timer.c: 30: }
	goto	l1744
	line	31
	
l1742:	
	goto	l1744
	line	33
;timer.c: 31: else
;timer.c: 32: {
	
l1743:	
	line	34
	
l1744:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_timer_init
psect	text388,local,class=CODE,delta=2
global __ptext388
__ptext388:

;; *************** function _timer_init *****************
;; Defined at:
;;		line 88 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text388
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\timer.c"
	line	88
	global	__size_of_timer_init
	__size_of_timer_init	equ	__end_of_timer_init-_timer_init
	
_timer_init:	
	opt	stack 5
; Regs used in _timer_init: [wreg-fsr0h+status,2+status,0]
	line	90
	
l5273:	
;timer.c: 89: uint8_t i;
;timer.c: 90: for (i = 0; i < TIMER_MAX; i++)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(timer_init@i)
	
l5275:	
	movf	(timer_init@i),w
	skipz
	goto	u3110
	goto	l5279
u3110:
	goto	l1763
	
l5277:	
	goto	l1763
	line	91
	
l1761:	
	line	92
	
l5279:	
;timer.c: 91: {
;timer.c: 92: timer_array[i]=0;
	movf	(timer_init@i),w
	movwf	(??_timer_init+0)+0
	addwf	(??_timer_init+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(0)
	movwf	indf
	incf	fsr0,f
	movlw	high(0)
	movwf	indf
	line	90
	
l5281:	
	movlw	(01h)
	movwf	(??_timer_init+0)+0
	movf	(??_timer_init+0)+0,w
	addwf	(timer_init@i),f
	
l5283:	
	movf	(timer_init@i),w
	skipz
	goto	u3120
	goto	l5279
u3120:
	goto	l1763
	
l1762:	
	line	94
	
l1763:	
	return
	opt stack 0
GLOBAL	__end_of_timer_init
	__end_of_timer_init:
;; =============== function _timer_init ends ============

	signat	_timer_init,88
	global	_uart_init
psect	text389,local,class=CODE,delta=2
global __ptext389
__ptext389:

;; *************** function _uart_init *****************
;; Defined at:
;;		line 303 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\uart_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;;		_uart_rx
;; This function uses a non-reentrant model
;;
psect	text389
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\uart_func.c"
	line	303
	global	__size_of_uart_init
	__size_of_uart_init	equ	__end_of_uart_init-_uart_init
	
_uart_init:	
	opt	stack 3
; Regs used in _uart_init: [wreg]
	line	307
	
l5271:	
;uart_func.c: 307: SPBRG = 0x06;
	movlw	(06h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(153)^080h	;volatile
	line	308
;uart_func.c: 308: TXSTA = 0x25;
	movlw	(025h)
	movwf	(152)^080h	;volatile
	line	309
;uart_func.c: 309: RCSTA = 0x90;
	movlw	(090h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(24)	;volatile
	line	310
	
l2627:	
	return
	opt stack 0
GLOBAL	__end_of_uart_init
	__end_of_uart_init:
;; =============== function _uart_init ends ============

	signat	_uart_init,88
	global	_init_micro
psect	text390,local,class=CODE,delta=2
global __ptext390
__ptext390:

;; *************** function _init_micro *****************
;; Defined at:
;;		line 332 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text390
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\main.c"
	line	332
	global	__size_of_init_micro
	__size_of_init_micro	equ	__end_of_init_micro-_init_micro
	
_init_micro:	
	opt	stack 5
; Regs used in _init_micro: [wreg+status,2]
	line	333
	
l5251:	
;main.c: 333: FlagSet.ClearAllGeneralFlagBits = 0;
	clrf	(_FlagSet)
	line	334
	
l5253:	
;main.c: 334: OSCCON = 0x71;
	movlw	(071h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(143)^080h	;volatile
	line	336
;main.c: 336: OPTION = 0x08;
	movlw	(08h)
	movwf	(129)^080h	;volatile
	line	337
	
l5255:	
;main.c: 337: ANSEL = 0x00;
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	clrf	(392)^0180h	;volatile
	line	338
	
l5257:	
;main.c: 338: ANSELH = 0x00;
	clrf	(393)^0180h	;volatile
	line	339
	
l5259:	
;main.c: 339: TRISA = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(133)^080h	;volatile
	line	340
	
l5261:	
;main.c: 340: PORTA = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(5)	;volatile
	line	341
	
l5263:	
;main.c: 341: TRISB = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(134)^080h	;volatile
	line	343
	
l5265:	
;main.c: 343: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	344
	
l5267:	
;main.c: 344: TRISE = 0x00;
	clrf	(137)^080h	;volatile
	line	345
	
l5269:	
;main.c: 345: T1CON = 0x31;
	movlw	(031h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(16)	;volatile
	line	346
	
l899:	
	return
	opt stack 0
GLOBAL	__end_of_init_micro
	__end_of_init_micro:
;; =============== function _init_micro ends ============

	signat	_init_micro,88
	global	_interrupt_handler
psect	text391,local,class=CODE,delta=2
global __ptext391
__ptext391:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 6 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_timer_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text391
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\interrupt.c"
	line	6
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 3
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	swapf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text391
	line	8
	
i1l4049:	
;interrupt.c: 8: timer_isr();
	fcall	_timer_isr
	line	9
	
i1l3474:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	swapf	(??_interrupt_handler+0)^0FFFFFF80h,w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_timer_isr
psect	text392,local,class=CODE,delta=2
global __ptext392
__ptext392:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 59 in file "F:\pic_projects\Hitch_Helper_BlueTooth_Display\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text392
	file	"F:\pic_projects\Hitch_Helper_BlueTooth_Display\timer.c"
	line	59
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 3
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	61
	
i1l4005:	
;timer.c: 60: uint8_t i;
;timer.c: 61: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u24_21
	goto	u24_20
u24_21:
	goto	i1l1758
u24_20:
	
i1l4007:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u25_21
	goto	u25_20
u25_21:
	goto	i1l1758
u25_20:
	line	64
	
i1l4009:	
;timer.c: 62: {
;timer.c: 64: TMR1IF=0;
	bcf	(96/8),(96)&7
	line	65
	
i1l4011:	
;timer.c: 65: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	66
	
i1l4013:	
;timer.c: 66: TMR1L = 0x80;
	movlw	(080h)
	movwf	(14)	;volatile
	line	67
	
i1l4015:	
;timer.c: 67: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	68
	
i1l4017:	
;timer.c: 68: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	70
	
i1l4019:	
;timer.c: 70: for (i = 0; i < TIMER_MAX; i++)
	clrf	(timer_isr@i)
	
i1l4021:	
	movf	(timer_isr@i),w
	skipz
	goto	u26_20
	goto	i1l4025
u26_20:
	goto	i1l1758
	
i1l4023:	
	goto	i1l1758
	line	71
	
i1l1753:	
	line	72
	
i1l4025:	
;timer.c: 71: {
;timer.c: 72: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u27_21
	goto	u27_20
u27_21:
	goto	i1l4029
u27_20:
	line	74
	
i1l4027:	
;timer.c: 73: {
;timer.c: 74: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	75
;timer.c: 75: }
	goto	i1l4029
	line	76
	
i1l1755:	
	goto	i1l4029
	line	78
;timer.c: 76: else
;timer.c: 77: {
	
i1l1756:	
	line	70
	
i1l4029:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l4031:	
	movf	(timer_isr@i),w
	skipz
	goto	u28_20
	goto	i1l4025
u28_20:
	goto	i1l1758
	
i1l1754:	
	line	81
;timer.c: 78: }
;timer.c: 79: }
;timer.c: 81: }
	goto	i1l1758
	line	82
	
i1l1752:	
	goto	i1l1758
	line	84
;timer.c: 82: else
;timer.c: 83: {
	
i1l1757:	
	line	85
	
i1l1758:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
psect	text393,local,class=CODE,delta=2
global __ptext393
__ptext393:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
