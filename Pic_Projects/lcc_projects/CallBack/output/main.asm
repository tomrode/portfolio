	.file	"c:\lcc\include\stdio.h"
_$M0:
	.file	"c:\lcc_projects\callback\main.c"
	.text
	.file	"c:\lcc\include\safelib.h"
_$M1:
	.file	"c:\lcc\include\stdio.h"
_$M2:
	.file	"c:\lcc\include\stddef.h"
_$M3:
	.file	"c:\lcc\include\stdlib.h"
_$M4:
	.file	"c:\lcc_projects\callback\main.c"
_$M5:
	.text
;    1 #include "stdio.h"
;    2 #include "stdlib.h"
;    3 
;    4 #define FUNCTION_POINTER_NO_ARRAY
;    5 
;    6 #ifdef FUNCTION_POINTER_NO_ARRAY
;    7 /********************************************************************/
;    8 /* Function prototypes                                              */
;    9 /********************************************************************/
;   10 float Plus (float a, float b);
;   11 float Minus (float a, float b);
;   12 float Multiply(float a, float b);
;   13 float Divide  (float a, float b);
;   14 void Switch_With_Function_Pointer(float a, float b, float (*pt2Func)(float, float ));
;   15 void main (void);
;   16 
;   17 
;   18 
;   19 /********************************************************************/
;   20 /* Function definitions                                             */
;   21 /********************************************************************/
;   22 
;   23 
;   24 void main(void)
	.type	_main,function
_main:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ecx
	movl	$1,%ecx
_$8:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$8
;   25 {
	.line	25
;   26   float result1;
;   27 
;   28   Switch_With_Function_Pointer(7, 5, &Multiply);      /*pointer function to desired artihmetic operation*/
	.line	28
	pushl	$_Multiply
	flds	_$5
	subl	$4,%esp
	fstps	(%esp)
	flds	_$6
	subl	$4,%esp
	fstps	(%esp)
	call	_Switch_With_Function_Pointer
	addl	$12,%esp
;   29 
;   30   Switch_With_Function_Pointer(12, 7, &Plus);      /*pointer function to desired artihmetic operation*/
	.line	30
	pushl	$_Plus
	flds	_$6
	subl	$4,%esp
	fstps	(%esp)
	flds	_$7
	subl	$4,%esp
	fstps	(%esp)
	call	_Switch_With_Function_Pointer
	addl	$12,%esp
_$4:
;   31 }
	.line	31
	leave
	ret
_$9:
	.size	_main,_$9-_main
	.globl	_main
;   32 
;   33 
;   34 float Plus (float a, float b) { return a+b; }
	.type	_Plus,function
_Plus:
	pushl	%ebp
	movl	%esp,%ebp
	.line	34
	flds	12(%ebp)
	fadds	8(%ebp)
_$10:
	leave
	ret
_$11:
	.size	_Plus,_$11-_Plus
	.globl	_Plus
;   35 float Minus   (float a, float b) { return a-b; }
	.type	_Minus,function
_Minus:
	pushl	%ebp
	movl	%esp,%ebp
	.line	35
	flds	8(%ebp)
	fsubs	12(%ebp)
_$12:
	leave
	ret
_$13:
	.size	_Minus,_$13-_Minus
	.globl	_Minus
;   36 float Multiply(float a, float b) { return a*b; }
	.type	_Multiply,function
_Multiply:
	pushl	%ebp
	movl	%esp,%ebp
	.line	36
	flds	12(%ebp)
	fmuls	8(%ebp)
_$14:
	leave
	ret
_$15:
	.size	_Multiply,_$15-_Multiply
	.globl	_Multiply
;   37 float Divide  (float a, float b) { return a/b; }
	.type	_Divide,function
_Divide:
	pushl	%ebp
	movl	%esp,%ebp
	.line	37
	flds	8(%ebp)
	fdivs	12(%ebp)
_$16:
	leave
	ret
_$17:
	.size	_Divide,_$17-_Divide
	.globl	_Divide
;   38 // Solution with a function pointer - <pt2Func> is a function pointer and points to
;   39 // a function which takes two floats and returns a float. The function pointer
;   40 // "specifies" which operation shall be executed.
;   41 void Switch_With_Function_Pointer(float a, float b, float (*pt2Func)(float, float))
	.type	_Switch_With_Function_Pointer,function
_Switch_With_Function_Pointer:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$12,%esp
	movl	$3,%ecx
_$21:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$21
;   42 {
	.line	42
;   43   float result, var;
;   44   result = pt2Func(a, b);  // call using function pointer
	.line	44
	flds	12(%ebp)
	subl	$4,%esp
	fstps	(%esp)
	flds	8(%ebp)
	subl	$4,%esp
	fstps	(%esp)
	call	*16(%ebp)
	addl	$8,%esp
	fstps	-12(%ebp)
	flds	-12(%ebp)
	fstps	-4(%ebp)
;   45   var = 2;
	.line	45
	flds	_$19
	fstps	-8(%ebp)
_$18:
;   46 
;   47 }
	.line	47
	leave
	ret
_$22:
	.size	_Switch_With_Function_Pointer,_$22-_Switch_With_Function_Pointer
	.globl	_Switch_With_Function_Pointer
	.data
	.align	2
_$19:
; float 2
	.long	0x40000000
	.align	2
_$7:
; float 12
	.long	0x41400000
	.align	2
_$6:
; float 7
	.long	0x40e00000
	.align	2
_$5:
; float 5
	.long	0x40a00000
