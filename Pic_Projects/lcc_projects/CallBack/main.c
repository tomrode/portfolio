#include "stdio.h"
#include "stdlib.h"

#define FUNCTION_POINTER_NO_ARRAY

#ifdef FUNCTION_POINTER_NO_ARRAY
/********************************************************************/
/* Function prototypes                                              */
/********************************************************************/
float Plus (float a, float b);
float Minus (float a, float b);
float Multiply(float a, float b);
float Divide  (float a, float b);
void Switch_With_Function_Pointer(float a, float b, float (*pt2Func)(float, float ));
void main (void);



/********************************************************************/
/* Function definitions                                             */
/********************************************************************/


void main(void)
{
  float result1;

  Switch_With_Function_Pointer(7, 5, &Multiply);      /*pointer function to desired artihmetic operation*/

  Switch_With_Function_Pointer(12, 7, &Plus);      /*pointer function to desired artihmetic operation*/
}


float Plus (float a, float b) { return a+b; }
float Minus   (float a, float b) { return a-b; }
float Multiply(float a, float b) { return a*b; }
float Divide  (float a, float b) { return a/b; }
// Solution with a function pointer - <pt2Func> is a function pointer and points to
// a function which takes two floats and returns a float. The function pointer
// "specifies" which operation shall be executed.
void Switch_With_Function_Pointer(float a, float b, float (*pt2Func)(float, float))
{
  float result, var;
  result = pt2Func(a, b);  // call using function pointer
  var = 2;

}

#endif

#ifdef FUNCTION_POINTER_ARRAY

/********************************************************************/
/* Function prototypes                                              */
/********************************************************************/

float increment(float x);
float decrement(float x);
float square(float x);
float reciprocal(float x);
void main(void);

/********************************************************************/
/* Type definitions                                                 */
/********************************************************************/

typedef float (*T_FUNCPTR_1)(float);


/********************************************************************/

/* Constant Definitions                                             */
/********************************************************************/

const T_FUNCPTR_1 func_array[] =
{
   increment,
   decrement,
   square,
   reciprocal
};

/********************************************************************/
/* Function definitions                                             */
/********************************************************************/

void main(void)
{
  float a;
  float b;

  a = 5;
  b = (*func_array[0])(a);
  printf("%f + 1 = %f \n",a,b);

  a = 66;
  b = (*func_array[1])(a);
  printf("%f - 1 = %f \n",a,b);

  a = 3;
  b = (*func_array[2])(a);
  printf("%f ^ 2 = %f \n",a,b);

  a = 10;
  b = (*func_array[3])(a);
  printf("1 / %f = %f \n",a,b);
}

float increment(float x)
{
  float y;

  y = x + 1;

  return y;
}

float decrement(float x)
{
  float y;

  y = x - 1;

  return y;
}

float square(float x)
{
  float y;

  y = x*x;

  return y;
}

float reciprocal(float x)
{
  float y;

  y = 1/x;

  return y;
}
#endif

#ifdef CALL_BACK


/* Function prototypes */
void PrintTwoNumbers(int (*numberSourced)(void));
int overNineThousand(void);
int meaningOfLife(void);
void main(void);




/* Here we call PrintTwoNumbers() with three different callbacks. */
void main(void)

{

    printf( "PrintTwoNumbers(rand) is:");//,PrintTwoNumbers(rand));
    PrintTwoNumbers(rand);
    PrintTwoNumbers(overNineThousand);
    PrintTwoNumbers(meaningOfLife);

}

/* The calling function takes a single callback as a parameter. */
void PrintTwoNumbers(int (*numberSourced)(void))
{
    printf("%d and %d\n", numberSourced(), numberSourced());
}

/* A possible callback */
int overNineThousand(void)
{
    return (rand() % 1000) + 9001;
}


/* Another possible callback. */
int meaningOfLife(void)
{
    return 42;
}


#endif


