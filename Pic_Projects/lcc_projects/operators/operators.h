/*********************************************************/
/* Constants                                             */
/*********************************************************/
#define NUMBER1    19
#define NUMBER2    4
#define NUMBER3    360
#define NUMBER4    1234
#define NUMBER5    5678
/*********************************************************/
/* Function Prototypes                                   */
/*********************************************************/

int add_num(int a, int b);
int right_shifted(int a);
unsigned int xor_num(int a,int b);
float divide_num(float a, float b);
long modulus_num(long c,long d);



