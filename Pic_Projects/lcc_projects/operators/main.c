  /*                                      Author  T.Rode                              Date 22 MAY 2011
   Examples of different operators and other subjects
  */
/*******************************************************/
/*Preprocessor operations                              */
/*******************************************************/
#include "operators.h"
#include "func2.h"
#include "stdio.h"
#include "math.h"

/*******************************************************/
/*Structure template                                   */
/*******************************************************/
typedef struct complex //typedef is a keyword comlex is a tag
      {
      float real;
      float imag;
      }COMPLEX;
/*******************************************************/
/*Function call from math.h                            */
/*******************************************************/
double sin(double x);    // sine function






void main(void)

{
/********************************************************/
/* Variable Definition                                  */
/********************************************************/
long         num1 = NUMBER1;
long         num2 = NUMBER2;
double       num3 = NUMBER3;
int          num4 = NUMBER4; //a
int          num5 = NUMBER5; //b
int          add;
int          shifted;
int          xored;
float        divided;
double       sin_num;
int          test1_num;
int          somevar = 42;
int          *ptr_to_somevar;
int           mod_num;

int CASE_STATE_MACHINE;
int case_test;


CASE_STATE_MACHINE=0;

switch(CASE_STATE_MACHINE)
      {
      case 0:
       printf("CASE_0\n");
       break;

      case 1:
       printf("CASE_1\n");
       break;

      default:
       break;
      }



//struct   complex vector;   // Declare vector as a complex type varible
COMPLEX vector;              // or do this when using typedef





/*********************************************************/
/*Function calls                                         */
/*********************************************************/
add = add_num(num1,num2);                         //Adding function
shifted = right_shifted(num1);                    //Right shifting function
xored = xor_num(num1,num2);                       //Exculsive or function
divided = divide_num(num1,num2);                  //Divide function
sin_num =  sin(num3);                             //Sine of a number in radians
test1_num = test_num(num4,num5);                   //test for higher number
mod_num = modulus_num(num1,num2);              //modulus operator example
/**********************************************************/
/*Pointer example                                         */
/**********************************************************/
ptr_to_somevar = &(somevar);
/**********************************************************/
/*Outputs via printf                                      */
/**********************************************************/







printf("number 1:%d\n",num1);
printf("number 2:%d\n",num2);
printf("number 4:%d\n",num4);
printf("number 5:%d\n",num5);
printf("\n");                                       //space holder
printf("The two numbers added together      : %d\n",add);
printf("Number1 right shifted               : %d\n",shifted);
printf("The number exclusive ored           : %d\n",xored);
printf("The number Divided of num1 and num2 : %5.2f\n",divided);
printf("The modulus only of num1 and num 2  : %d\n",mod_num);
printf("The sine in Rads of 90 degrees      : %8.8f\n",sin_num);
printf("The tested number returned is       : %d\n",test1_num);
printf("THe value of pointer                : %d\n",*ptr_to_somevar);
printf("Type in real\n");
scanf("%f", &vector.real);
printf("Type in imaginary\n");
scanf("%f", &vector.imag);
printf("The real part %.2f imaginary part: %.2f\n",vector.real, vector.imag);  //example of using a structure
}

