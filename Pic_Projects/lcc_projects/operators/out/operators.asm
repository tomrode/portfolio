	.file	"c:\lcc_projects\operators\operators.h"
_$M0:
	.file	"c:\lcc_projects\operators\operators.c"
	.text
	.file	"c:\lcc_projects\operators\func2.h"
_$M1:
	.file	"c:\lcc_projects\operators\operators.c"
_$M2:
	.text
;    1 #include "operators.h"
;    2 #include "func2.h"
;    3 //#include "stdio.h"
;    4 /*********************************************************/
;    5 /* Macro Definitions                                     */
;    6 /*********************************************************/
;    7 
;    8 
;    9 /*********************************************************/
;   10 /* Variable Definitions                                  */
;   11 /*********************************************************/
;   12 
;   13 /*********************************************************/
;   14 /* add_num()                                             */
;   15 /*                                                       */
;   16 /* Description:  This function returns a sumation        */
;   17 /*               of two integer types                    */
;   18 /*                                                       */
;   19 /*                                                       */
;   20 /* Inputs:       2 integers                              */
;   21 /*                                                       */
;   22 /* Outputs:      result 1 integer                        */
;   23 /*********************************************************/
;   24 int add_num(int a, int b)
	.type	_add_num,function
_add_num:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ecx
	movl	$1,%ecx
_$2:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$2
	pushl	%edi
;   25  {
	.line	25
;   26  int added_num;
;   27  added_num = a+b;
	.line	27
	movl	8(%ebp),%edi
	addl	12(%ebp),%edi
	movl	%edi,-4(%ebp)
;   28 
;   29  return added_num;
	.line	29
	movl	-4(%ebp),%eax
_$1:
;   30  }
	.line	30
	popl	%edi
	leave
	ret
_$3:
	.size	_add_num,_$3-_add_num
	.globl	_add_num
;   31 
;   32 /*********************************************************/
;   33 /* right_shifted()                                       */
;   34 /*                                                       */
;   35 /* Description:  This function returns a right shifted   */
;   36 /*               integer                                 */
;   37 /*                                                       */
;   38 /*                                                       */
;   39 /* Inputs:       1 integers                              */
;   40 /*                                                       */
;   41 /* Outputs:      result 1  integer right shifted         */
;   42 /*********************************************************/
;   43 int right_shifted(int a)
	.type	_right_shifted,function
_right_shifted:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ecx
	movl	$1,%ecx
_$5:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$5
	pushl	%edi
;   44  {
	.line	44
;   45  int shifted_num;
;   46  shifted_num = a>>1; // right shift by 1
	.line	46
	movl	8(%ebp),%edi
	sarl	$1,%edi
	movl	%edi,-4(%ebp)
;   47 
;   48  return shifted_num;
	.line	48
	movl	-4(%ebp),%eax
_$4:
;   49  }
	.line	49
	popl	%edi
	leave
	ret
_$6:
	.size	_right_shifted,_$6-_right_shifted
	.globl	_right_shifted
;   50 /*********************************************************/
;   51 /* xor_num()                                             */
;   52 /*                                                       */
;   53 /* Description: This function will return the result     */
;   54 /*              of two integers xor'ed via truth table   */
;   55 /*              A|B|Y                                    */
;   56 /*              0|0|0                                    */
;   57 /*              0|1|1                                    */
;   58 /*              1|0|1                                    */
;   59 /*              1|1|0                                    */
;   60 /*                                                       */
;   61 /* Inputs:      2 integers                               */
;   62 /*                                                       */
;   63 /* Outputs:     Result 1 xor'ed unsigned integer         */
;   64 /*********************************************************/
;   65 unsigned int xor_num(int a,int b)
	.type	_xor_num,function
_xor_num:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ecx
	movl	$1,%ecx
_$9:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$9
	pushl	%edi
;   66  {
	.line	66
;   67  int xored_num;
;   68  xored_num = a^=b;  //Exclusive ored two numbers
	.line	68
	movl	8(%ebp),%edi
	xorl	12(%ebp),%edi
	movl	%edi,8(%ebp)
	movl	%edi,-4(%ebp)
;   69 
;   70  return xored_num;
	.line	70
	movl	-4(%ebp),%eax
_$7:
;   71  }
	.line	71
	popl	%edi
	leave
	ret
_$10:
	.size	_xor_num,_$10-_xor_num
	.globl	_xor_num
;   72 
;   73 
;   74 /*********************************************************/
;   75 /* divide_num()                                          */
;   76 /*                                                       */
;   77 /* Description: This function will return the result     */
;   78 /*              of two integers divided with modulus     */
;   79 /*              (remainder)                              */
;   80 /*                                                       */
;   81 /* Inputs:      2 integers                               */
;   82 /*                                                       */
;   83 /* Outputs:     Result 1 float type                      */
;   84 /*********************************************************/
;   85 float divide_num(float a, float b)
	.type	_divide_num,function
_divide_num:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ecx
	pushl	%eax
	movl	$2,%ecx
_$15:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$15
;   86  {
	.line	86
;   87  float divided_num;       // quotient
;   88  signed int modulus;      // remainder
;   89 
;   90 if(b>0)
	.line	90
	fldz
	fcomps	12(%ebp)
	fnstsw %ax
	sahf
	jae	_$12
;   91   {
;   92    divided_num = a/b;     // a=dividend, b=divisor
	.line	92
	flds	8(%ebp)
	fdivs	12(%ebp)
	fstps	-4(%ebp)
;   93    return divided_num;
	.line	93
	flds	-4(%ebp)
	jmp	_$11
_$12:
;   94   }
;   95 else
;   96   printf("divisor zero\n");
	.line	96
	pushl	$_$14
	call	_printf
	addl	$4,%esp
;   97   return 0;
	.line	97
	fldz
_$11:
;   98  }
	.line	98
	leave
	ret
_$16:
	.size	_divide_num,_$16-_divide_num
	.globl	_divide_num
;   99 
;  100 /*********************************************************/
;  101 /* modulus_num()                                          */
;  102 /*                                                       */
;  103 /* Description: This function will return the result     */
;  104 /*              of two integers divided with it modulus   */
;  105 /*              (remainder)                              */
;  106 /*                                                       */
;  107 /* Inputs:      2 integers                               */
;  108 /*                                                       */
;  109 /* Outputs:     Result 1 float type                      */
;  110 /*********************************************************/
;  111 
;  112 long modulus_num(long c, long d)
	.type	_modulus_num,function
_modulus_num:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ecx
	movl	$1,%ecx
_$18:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$18
;  113 {
	.line	113
;  114 int mod_num;
;  115 mod_num = c % d;
	.line	115
	movl	8(%ebp),%eax
	movl	12(%ebp),%ecx
	cdq
	idivl	%ecx
	movl	%edx,-4(%ebp)
;  116 return mod_num;
	.line	116
	movl	-4(%ebp),%eax
_$17:
;  117 }
	.line	117
	leave
	ret
_$19:
	.size	_modulus_num,_$19-_modulus_num
	.globl	_modulus_num
	.extern	_printf
	.data
_$14:
; "divisor zero\n\x0"
	.byte	100,105,118,105,115,111,114,32,122,101,114,111,10,0
