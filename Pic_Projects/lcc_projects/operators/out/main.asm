	.file	"c:\lcc_projects\operators\operators.h"
_$M0:
	.file	"c:\lcc_projects\operators\main.c"
	.text
	.file	"c:\lcc_projects\operators\func2.h"
_$M1:
	.file	"c:\lcc\include\stdio.h"
_$M2:
	.file	"c:\lcc\include\safelib.h"
_$M3:
	.file	"c:\lcc\include\stdio.h"
_$M4:
	.file	"c:\lcc\include\math.h"
_$M5:
	.file	"c:\lcc\include\tgmath.h"
_$M6:
	.file	"c:\lcc\include\math.h"
_$M7:
	.file	"c:\lcc_projects\operators\main.c"
_$M8:
	.text
;    1   /*                                      Author  T.Rode                              Date 22 MAY 2011
;    2    Examples of different operators and other subjects
;    3   */
;    4 /*******************************************************/
;    5 /*Preprocessor operations                              */
;    6 /*******************************************************/
;    7 #include "operators.h"
;    8 #include "func2.h"
;    9 #include "stdio.h"
;   10 #include "math.h"
;   11 
;   12 /*******************************************************/
;   13 /*Structure template                                   */
;   14 /*******************************************************/
;   15 typedef struct complex //typedef is a keyword comlex is a tag
;   16       {
;   17       float real;
;   18       float imag;
;   19       }COMPLEX;
;   20 /*******************************************************/
;   21 /*Function call from math.h                            */
;   22 /*******************************************************/
;   23 double sin(double x);    // sine function
;   24 
;   25 
;   26 
;   27 
;   28 
;   29 
;   30 void main(void)
	.type	_main,function
_main:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$96,%esp
	movl	$24,%ecx
_$37:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$37
	pushl	%edi
;   31 
;   32 {
	.line	32
;   33 /********************************************************/
;   34 /* Variable Definition                                  */
;   35 /********************************************************/
;   36 long         num1 = NUMBER1;
	.line	36
	movl	$19,-4(%ebp)
;   37 long         num2 = NUMBER2;
	.line	37
	movl	$4,-8(%ebp)
;   38 double       num3 = NUMBER3;
	.line	38
	fldl	_$2
	fstpl	-40(%ebp)
;   39 int          num4 = NUMBER4; //a
	.line	39
	movl	$1234,-20(%ebp)
;   40 int          num5 = NUMBER5; //b
	.line	40
	movl	$5678,-24(%ebp)
;   41 int          add;
;   42 int          shifted;
;   43 int          xored;
;   44 float        divided;
;   45 double       sin_num;
;   46 int          test1_num;
;   47 int          somevar = 42;
	.line	47
	movl	$42,-72(%ebp)
;   48 int          *ptr_to_somevar;
;   49 int           mod_num;
;   50 
;   51 int CASE_STATE_MACHINE;
;   52 int case_test;
;   53 
;   54 
;   55 CASE_STATE_MACHINE=0;
	.line	55
	movl	$0,-28(%ebp)
;   56 
;   57 switch(CASE_STATE_MACHINE)
	.line	57
	movl	-28(%ebp),%edi
	cmpl	$0,%edi
	je	_$5
	cmpl	$1,%edi
	je	_$7
	jmp	_$4
_$5:
;   58       {
;   59       case 0:
;   60        printf("CASE_0\n");
	.line	60
	pushl	$_$6
	call	_printf
	addl	$4,%esp
;   61        break;
	.line	61
	jmp	_$4
_$7:
;   62 
;   63       case 1:
;   64        printf("CASE_1\n");
	.line	64
	pushl	$_$8
	call	_printf
	addl	$4,%esp
;   65        break;
;   66 
;   67       default:
;   68        break;
	.line	68
_$4:
;   69       }
;   70 
;   71 
;   72 
;   73 //struct   complex vector;   // Declare vector as a complex type varible
;   74 COMPLEX vector;              // or do this when using typedef
;   75 
;   76 
;   77 
;   78 
;   79 
;   80 /*********************************************************/
;   81 /*Function calls                                         */
;   82 /*********************************************************/
;   83 add = add_num(num1,num2);                         //Adding function
	.line	83
	pushl	-8(%ebp)
	pushl	-4(%ebp)
	call	_add_num
	addl	$8,%esp
	movl	%eax,-44(%ebp)
;   84 shifted = right_shifted(num1);                    //Right shifting function
	.line	84
	pushl	-4(%ebp)
	call	_right_shifted
	addl	$4,%esp
	movl	%eax,-48(%ebp)
;   85 xored = xor_num(num1,num2);                       //Exculsive or function
	.line	85
	pushl	-8(%ebp)
	pushl	-4(%ebp)
	call	_xor_num
	addl	$8,%esp
	movl	%eax,-52(%ebp)
;   86 divided = divide_num(num1,num2);                  //Divide function
	.line	86
	push	-8(%ebp)
	fildl	(%esp)
	addl	$4,%esp
	subl	$4,%esp
	fstps	(%esp)
	push	-4(%ebp)
	fildl	(%esp)
	addl	$4,%esp
	subl	$4,%esp
	fstps	(%esp)
	call	_divide_num
	addl	$8,%esp
	fstps	-88(%ebp)
	flds	-88(%ebp)
	fstps	-56(%ebp)
;   87 sin_num =  sin(num3);                             //Sine of a number in radians
	.line	87
	fldl	-40(%ebp)
	subl	$8,%esp
	fstpl	(%esp)
	call	_sin
	addl	$8,%esp
	fstpl	-96(%ebp)
	fldl	-96(%ebp)
	fstpl	-64(%ebp)
;   88 test1_num = test_num(num4,num5);                   //test for higher number
	.line	88
	pushl	-24(%ebp)
	pushl	-20(%ebp)
	call	_test_num
	addl	$8,%esp
	movl	%eax,-68(%ebp)
;   89 mod_num = modulus_num(num1,num2);              //modulus operator example
	.line	89
	pushl	-8(%ebp)
	pushl	-4(%ebp)
	call	_modulus_num
	addl	$8,%esp
	movl	%eax,-80(%ebp)
;   90 /**********************************************************/
;   91 /*Pointer example                                         */
;   92 /**********************************************************/
;   93 ptr_to_somevar = &(somevar);
	.line	93
	leal	-72(%ebp),%edi
	movl	%edi,-76(%ebp)
;   94 /**********************************************************/
;   95 /*Outputs via printf                                      */
;   96 /**********************************************************/
;   97 
;   98 
;   99 
;  100 
;  101 
;  102 
;  103 
;  104 printf("number 1:%d\n",num1);
	.line	104
	pushl	-4(%ebp)
	pushl	$_$9
	call	_printf
	addl	$8,%esp
;  105 printf("number 2:%d\n",num2);
	.line	105
	pushl	-8(%ebp)
	pushl	$_$10
	call	_printf
	addl	$8,%esp
;  106 printf("number 4:%d\n",num4);
	.line	106
	pushl	-20(%ebp)
	pushl	$_$11
	call	_printf
	addl	$8,%esp
;  107 printf("number 5:%d\n",num5);
	.line	107
	pushl	-24(%ebp)
	pushl	$_$12
	call	_printf
	addl	$8,%esp
;  108 printf("\n");                                       //space holder
	.line	108
	pushl	$_$13
	call	_printf
	addl	$4,%esp
;  109 printf("The two numbers added together      : %d\n",add);
	.line	109
	pushl	-44(%ebp)
	pushl	$_$14
	call	_printf
	addl	$8,%esp
;  110 printf("Number1 right shifted               : %d\n",shifted);
	.line	110
	pushl	-48(%ebp)
	pushl	$_$15
	call	_printf
	addl	$8,%esp
;  111 printf("The number exclusive ored           : %d\n",xored);
	.line	111
	pushl	-52(%ebp)
	pushl	$_$16
	call	_printf
	addl	$8,%esp
;  112 printf("The number Divided of num1 and num2 : %5.2f\n",divided);
	.line	112
	flds	-56(%ebp)
	subl	$8,%esp
	fstpl	(%esp)
	pushl	$_$17
	call	_printf
	addl	$12,%esp
;  113 printf("The modulus only of num1 and num 2  : %d\n",mod_num);
	.line	113
	pushl	-80(%ebp)
	pushl	$_$18
	call	_printf
	addl	$8,%esp
;  114 printf("The sine in Rads of 90 degrees      : %8.8f\n",sin_num);
	.line	114
	fldl	-64(%ebp)
	subl	$8,%esp
	fstpl	(%esp)
	pushl	$_$19
	call	_printf
	addl	$12,%esp
;  115 printf("The tested number returned is       : %d\n",test1_num);
	.line	115
	pushl	-68(%ebp)
	pushl	$_$20
	call	_printf
	addl	$8,%esp
;  116 printf("THe value of pointer                : %d\n",*ptr_to_somevar);
	.line	116
	movl	-76(%ebp),%edi
	pushl	(,%edi)
	pushl	$_$21
	call	_printf
	addl	$8,%esp
;  117 printf("Type in real\n");
	.line	117
	pushl	$_$22
	call	_printf
	addl	$4,%esp
;  118 scanf("%f", &vector.real);
	.line	118
	leal	-16(%ebp),%edi
	pushl	%edi
	pushl	$_$23
	call	_scanf
	addl	$8,%esp
;  119 printf("Type in imaginary\n");
	.line	119
	pushl	$_$24
	call	_printf
	addl	$4,%esp
;  120 scanf("%f", &vector.imag);
	.line	120
	leal	-12(%ebp),%edi
	pushl	%edi
	pushl	$_$23
	call	_scanf
	addl	$8,%esp
;  121 printf("The real part %.2f imaginary part: %.2f\n",vector.real, vector.imag);  //example of using a structure
	.line	121
	flds	-12(%ebp)
	subl	$8,%esp
	fstpl	(%esp)
	flds	-16(%ebp)
	subl	$8,%esp
	fstpl	(%esp)
	pushl	$_$26
	call	_printf
	addl	$20,%esp
_$1:
;  122 }
	.line	122
	popl	%edi
	leave
	ret
_$38:
	.size	_main,_$38-_main
	.globl	_main
	.extern	_sin
	.extern	_scanf
	.extern	_printf
	.extern	_test_num
	.extern	_modulus_num
	.extern	_divide_num
	.extern	_xor_num
	.extern	_right_shifted
	.extern	_add_num
	.data
_$26:
; "The real part %.2f imaginary part: %.2f\n\x0"
	.byte	84,104,101,32,114,101,97,108,32,112,97,114,116,32,37,46
	.byte	50,102,32,105,109,97,103,105,110,97,114,121,32,112,97
	.byte	114,116,58,32,37,46,50,102,10,0
_$24:
; "Type in imaginary\n\x0"
	.byte	84,121,112,101,32,105,110,32,105,109,97,103,105,110,97,114
	.byte	121,10,0
_$23:
; "%f\x0"
	.byte	37,102,0
_$22:
; "Type in real\n\x0"
	.byte	84,121,112,101,32,105,110,32,114,101,97,108,10,0
_$21:
; "THe value of pointer                : %d\n\x0"
	.byte	84,72,101,32,118,97,108,117,101,32,111,102,32,112,111,105
	.byte	110,116,101,114,32,32,32,32,32,32,32,32,32,32,32
	.byte	32,32,32,32,32,58,32,37,100,10,0
_$20:
; "The tested number returned is       : %d\n\x0"
	.byte	84,104,101,32,116,101,115,116,101,100,32,110,117,109,98,101
	.byte	114,32,114,101,116,117,114,110,101,100,32,105,115,32,32
	.byte	32,32,32,32,32,58,32,37,100,10,0
_$19:
; "The sine in Rads of 90 degrees      : %8.8f\n\x0"
	.byte	84,104,101,32,115,105,110,101,32,105,110,32,82,97,100,115
	.byte	32,111,102,32,57,48,32,100,101,103,114,101,101,115,32
	.byte	32,32,32,32,32,58,32,37,56,46,56,102,10,0
_$18:
; "The modulus only of num1 and num 2  : %d\n\x0"
	.byte	84,104,101,32,109,111,100,117,108,117,115,32,111,110,108,121
	.byte	32,111,102,32,110,117,109,49,32,97,110,100,32,110,117
	.byte	109,32,50,32,32,58,32,37,100,10,0
_$17:
; "The number Divided of num1 and num2 : %5.2f\n\x0"
	.byte	84,104,101,32,110,117,109,98,101,114,32,68,105,118,105,100
	.byte	101,100,32,111,102,32,110,117,109,49,32,97,110,100,32
	.byte	110,117,109,50,32,58,32,37,53,46,50,102,10,0
_$16:
; "The number exclusive ored           : %d\n\x0"
	.byte	84,104,101,32,110,117,109,98,101,114,32,101,120,99,108,117
	.byte	115,105,118,101,32,111,114,101,100,32,32,32,32,32,32
	.byte	32,32,32,32,32,58,32,37,100,10,0
_$15:
; "Number1 right shifted               : %d\n\x0"
	.byte	78,117,109,98,101,114,49,32,114,105,103,104,116,32,115,104
	.byte	105,102,116,101,100,32,32,32,32,32,32,32,32,32,32
	.byte	32,32,32,32,32,58,32,37,100,10,0
_$14:
; "The two numbers added together      : %d\n\x0"
	.byte	84,104,101,32,116,119,111,32,110,117,109,98,101,114,115,32
	.byte	97,100,100,101,100,32,116,111,103,101,116,104,101,114,32
	.byte	32,32,32,32,32,58,32,37,100,10,0
_$13:
; "\n\x0"
	.byte	10,0
_$12:
; "number 5:%d\n\x0"
	.byte	110,117,109,98,101,114,32,53,58,37,100,10,0
_$11:
; "number 4:%d\n\x0"
	.byte	110,117,109,98,101,114,32,52,58,37,100,10,0
_$10:
; "number 2:%d\n\x0"
	.byte	110,117,109,98,101,114,32,50,58,37,100,10,0
_$9:
; "number 1:%d\n\x0"
	.byte	110,117,109,98,101,114,32,49,58,37,100,10,0
_$8:
; "CASE_1\n\x0"
	.byte	67,65,83,69,95,49,10,0
_$6:
; "CASE_0\n\x0"
	.byte	67,65,83,69,95,48,10,0
	.align	4
_$2:
; double 360
	.long	0x0,0x40768000
	.extern	_sin
