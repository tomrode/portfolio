	.file	"c:\lcc_projects\operators\operators.h"
_$M0:
	.file	"c:\lcc_projects\operators\func2.c"
	.text
	.file	"c:\lcc_projects\operators\func2.h"
_$M1:
	.file	"c:\lcc_projects\operators\func2.c"
_$M2:
	.text
;    1 #include "operators.h"
;    2 #include "func2.h"
;    3 //#include "stdio.h"
;    4 
;    5 /*********************************************************/
;    6 /* Macro Definitions                                     */
;    7 /*********************************************************/
;    8 
;    9 
;   10 /*********************************************************/
;   11 /* Variable Definitions                                  */
;   12 /*********************************************************/
;   13 
;   14 /*********************************************************/
;   15 /* test_num()                                            */
;   16 /*                                                       */
;   17 /* Description:  This function returns a flag for a test */
;   18 /*               made with two integer types for which is*/
;   19 /*               larger                                  */
;   20 /*                                                       */
;   21 /* Inputs:       2 integers of different values          */
;   22 /*                                                       */
;   23 /* Outputs:      result 1 integer of larger value        */
;   24 /*********************************************************/
;   25 int test_num(int a, int b)
	.type	_test_num,function
_test_num:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$12,%esp
	movl	$3,%ecx
_$6:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$6
	pushl	%edi
;   26  {
	.line	26
;   27  int tested_num;
;   28  int num_4 = a;
	.line	28
	movl	8(%ebp),%edi
	movl	%edi,-4(%ebp)
;   29  int num_5 = b;
	.line	29
	movl	12(%ebp),%edi
	movl	%edi,-8(%ebp)
;   30 
;   31 
;   32 
;   33  if(num_4 < num_5)
	.line	33
	movl	-8(%ebp),%edi
	cmpl	%edi,-4(%ebp)
	jge	_$2
;   34     {
;   35      tested_num=2;
	.line	35
	movl	$2,-12(%ebp)
	jmp	_$3
_$2:
;   36     }
;   37   else if(num_4 > num_5)
	.line	37
	movl	-8(%ebp),%edi
	cmpl	%edi,-4(%ebp)
	jle	_$4
;   38     {
;   39      tested_num=1;
	.line	39
	movl	$1,-12(%ebp)
_$4:
;   40     }
;   41     else
;   42     {
	.line	42
_$5:
;   43     }
;   44  return tested_num;
	.line	44
_$3:
	movl	-12(%ebp),%eax
_$1:
;   45  }
	.line	45
	popl	%edi
	leave
	ret
_$7:
	.size	_test_num,_$7-_test_num
	.globl	_test_num
