#include "operators.h"
#include "func2.h"
//#include "stdio.h"
/*********************************************************/
/* Macro Definitions                                     */
/*********************************************************/


/*********************************************************/
/* Variable Definitions                                  */
/*********************************************************/

/*********************************************************/
/* add_num()                                             */
/*                                                       */
/* Description:  This function returns a sumation        */
/*               of two integer types                    */
/*                                                       */
/*                                                       */
/* Inputs:       2 integers                              */
/*                                                       */
/* Outputs:      result 1 integer                        */
/*********************************************************/
int add_num(int a, int b)
 {
 int added_num;
 added_num = a+b;

 return added_num;
 }

/*********************************************************/
/* right_shifted()                                       */
/*                                                       */
/* Description:  This function returns a right shifted   */
/*               integer                                 */
/*                                                       */
/*                                                       */
/* Inputs:       1 integers                              */
/*                                                       */
/* Outputs:      result 1  integer right shifted         */
/*********************************************************/
int right_shifted(int a)
 {
 int shifted_num;
 shifted_num = a>>1; // right shift by 1

 return shifted_num;
 }
/*********************************************************/
/* xor_num()                                             */
/*                                                       */
/* Description: This function will return the result     */
/*              of two integers xor'ed via truth table   */
/*              A|B|Y                                    */
/*              0|0|0                                    */
/*              0|1|1                                    */
/*              1|0|1                                    */
/*              1|1|0                                    */
/*                                                       */
/* Inputs:      2 integers                               */
/*                                                       */
/* Outputs:     Result 1 xor'ed unsigned integer         */
/*********************************************************/
unsigned int xor_num(int a,int b)
 {
 int xored_num;
 xored_num = a^=b;  //Exclusive ored two numbers

 return xored_num;
 }


/*********************************************************/
/* divide_num()                                          */
/*                                                       */
/* Description: This function will return the result     */
/*              of two integers divided with modulus     */
/*              (remainder)                              */
/*                                                       */
/* Inputs:      2 integers                               */
/*                                                       */
/* Outputs:     Result 1 float type                      */
/*********************************************************/
float divide_num(float a, float b)
 {
 float divided_num;       // quotient
 signed int modulus;      // remainder

if(b>0)
  {
   divided_num = a/b;     // a=dividend, b=divisor
   return divided_num;
  }
else
  printf("divisor zero\n");
  return 0;
 }

/*********************************************************/
/* modulus_num()                                          */
/*                                                       */
/* Description: This function will return the result     */
/*              of two integers divided with it modulus   */
/*              (remainder)                              */
/*                                                       */
/* Inputs:      2 integers                               */
/*                                                       */
/* Outputs:     Result 1 float type                      */
/*********************************************************/

long modulus_num(long c, long d)
{
int mod_num;
mod_num = c % d;
return mod_num;
}
