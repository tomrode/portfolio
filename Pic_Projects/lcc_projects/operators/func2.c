#include "operators.h"
#include "func2.h"
//#include "stdio.h"

/*********************************************************/
/* Macro Definitions                                     */
/*********************************************************/


/*********************************************************/
/* Variable Definitions                                  */
/*********************************************************/

/*********************************************************/
/* test_num()                                            */
/*                                                       */
/* Description:  This function returns a flag for a test */
/*               made with two integer types for which is*/
/*               larger                                  */
/*                                                       */
/* Inputs:       2 integers of different values          */
/*                                                       */
/* Outputs:      result 1 integer of larger value        */
/*********************************************************/
int test_num(int a, int b)
 {
 int tested_num;
 int num_4 = a;
 int num_5 = b;



 if(num_4 < num_5)
    {
     tested_num=2;
    }
  else if(num_4 > num_5)
    {
     tested_num=1;
    }
    else
    {
    }
 return tested_num;
 }



