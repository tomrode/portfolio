#include "stdio.h"
#include "typedefs.h"



#define  KD    ((10/100)*2)

/*******************************************************************************/
/*           Type definitions                                                  */
/*******************************************************************************/

typedef struct
{
uint8_t real;
uint8_t imaginary;
}COMPLEX;


/*******************************************************************************/
/*           Function Prototypes                                               */
/*******************************************************************************/
void main(void);
void pointer_example(void);
void which_complex_number(COMPLEX *system_variables);

/*******************************************************************************/
/*           Constant Definitions                                              */
/*******************************************************************************/
const uint8_t global_ar[] = "HELLOWORLDILOVEPIE";

/*******************************************************************************/
/*           Global variables                                                  */
/*******************************************************************************/


union flg_type {
unsigned char flg_byte; /* to hold 8 flag bits in byte for bytewise manipulation */
struct {
        unsigned flag0:1;
        unsigned flag1:1;
        unsigned flag2:2;
        unsigned flag3:1;
        unsigned flag4:1;
        unsigned flag5:1;
        unsigned flag6:1;
        unsigned flag7:1;
        }flag_bits;
};


void main(void)
{
/* Local Type main definitions */
COMPLEX *num1;
COMPLEX *num2;
COMPLEX *num3;
COMPLEX *num4;
COMPLEX  number1;
uint8_t test;

union flg_type flags;



test = 77;
test = KD +1 ;
num1 = &number1;
num1 -> real       = 0x22;
num1 -> imaginary  = 0xAD;
//printf("%d",number1.real);
//printf("%d",number1.imaginary);

//while (1)
// {
    //which_complex_number(num1);
    pointer_example();
// }
}

/*******************************************************************************/
/* pointer_example()                                                           */
/*                                                                             */
/* Description: This function pointes to variable named "test" which appears   */
/*              backwards and the pointer points to first byte and the pointer */
/*              writes and increment by 1 byte.                                */
/* INPUTS: NONE                                                                */
/* OUPUTS: NONE                                                                */
/*******************************************************************************/
void pointer_example(void)
{
/* Local Variables*/
uint32_t test;
uint8_t *ptr;
uint8_t *ptr2;
uint8_t pie_ar[100];
uint8_t i;

uint8_t flag1;
uint8_t flag2;
uint8_t flag3;
uint8_t current_actual_position_c[4];

flag1=0;
flag2=1;
flag3=0;

if (flag1==1)
  {
   i=1;
  }
else if(flag2 == 1)
   {
   i=2;
   }
else if (flag3 == 1)
   {
   i=3;
   }
 else
   {
    /* Do nothing*/
   }
ptr = &global_ar[0];




for (i=0;i<100;i++)
{

  pie_ar[i] = *ptr;
  ptr++;
   /* Stop this evolution if not ascii A-Z Capital or a spaces in between*/
  if((pie_ar[i] < 0x41) || (pie_ar[i] > 0x5A))
    {
      i = 100;
    }
   else
     {
     }


}

test=0xBADBEEF1;
ptr = &test;      /* Allocate a pointee, make the pointer point to the address of variable test*/


ptr2=ptr;         /* Pointer assignment sets ptr2 to point to ptr's pointee*/

*ptr2=13;
*ptr = 0xba;      /* Dereference the pointer, like stick in a value*/
ptr++;
*ptr = 0xdb;
ptr++;
*ptr = 0xee;
ptr++;
*ptr = 0xf1;
ptr++;

}



void which_complex_number(COMPLEX *system_variables)
{
COMPLEX incomming_number;          /* Make this a type COMPLEX*/

uint8_t incomming_realpart;
uint8_t incomming_imaginarypart;


incomming_number= *system_variables;

incomming_realpart = incomming_number.real;
printf("%x\n",incomming_number.real);
incomming_imaginarypart = incomming_number.imaginary;
printf("%x\n",incomming_number.imaginary);

printf("END\n");

}
