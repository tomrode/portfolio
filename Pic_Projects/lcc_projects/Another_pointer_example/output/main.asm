	.file	"c:\lcc\include\stdio.h"
_$M0:
	.file	"c:\lcc_projects\another_pointer_example\main.c"
	.text
	.file	"c:\lcc\include\safelib.h"
_$M1:
	.file	"c:\lcc\include\stdio.h"
_$M2:
	.file	"c:\lcc_projects\another_pointer_example\typedefs.h"
_$M3:
	.file	"c:\lcc_projects\another_pointer_example\main.c"
_$M4:
	.data
	.globl	_global_ar
	.type	_global_ar,object
_global_ar:
; "HELLOWORLDILOVEPIE\x0"
	.byte	72,69,76,76,79,87,79,82,76,68,73,76,79,86,69,80
	.byte	73,69,0
	.text
;    1 #include "stdio.h"
;    2 #include "typedefs.h"
;    3 
;    4 
;    5 
;    6 #define  KD    ((10/100)*2)
;    7 
;    8 /*******************************************************************************/
;    9 /*           Type definitions                                                  */
;   10 /*******************************************************************************/
;   11 
;   12 typedef struct
;   13 {
;   14 uint8_t real;
;   15 uint8_t imaginary;
;   16 }COMPLEX;
;   17 
;   18 
;   19 /*******************************************************************************/
;   20 /*           Function Prototypes                                               */
;   21 /*******************************************************************************/
;   22 void main(void);
;   23 void pointer_example(void);
;   24 void which_complex_number(COMPLEX *system_variables);
;   25 
;   26 /*******************************************************************************/
;   27 /*           Constant Definitions                                              */
;   28 /*******************************************************************************/
;   29 const uint8_t global_ar[] = "HELLOWORLDILOVEPIE";
;   30 
;   31 /*******************************************************************************/
;   32 /*           Global variables                                                  */
;   33 /*******************************************************************************/
;   34 
;   35 
;   36 union flg_type {
;   37 unsigned char flg_byte; /* to hold 8 flag bits in byte for bytewise manipulation */
;   38 struct {
;   39         unsigned flag0:1;
;   40         unsigned flag1:1;
;   41         unsigned flag2:2;
;   42         unsigned flag3:1;
;   43         unsigned flag4:1;
;   44         unsigned flag5:1;
;   45         unsigned flag6:1;
;   46         unsigned flag7:1;
;   47         }flag_bits;
;   48 };
;   49 
;   50 
;   51 void main(void)
	.type	_main,function
_main:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$28,%esp
	movl	$7,%ecx
_$5:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$5
	pushl	%edi
;   52 {
	.line	52
;   53 /* Local Type main definitions */
;   54 COMPLEX *num1;
;   55 COMPLEX *num2;
;   56 COMPLEX *num3;
;   57 COMPLEX *num4;
;   58 COMPLEX  number1;
;   59 uint8_t test;
;   60 
;   61 union flg_type flags;
;   62 
;   63 
;   64 
;   65 test = 77;
	.line	65
	movb	$77,-5(%ebp)
;   66 test = KD +1 ;
	.line	66
	movb	$1,-5(%ebp)
;   67 num1 = &number1;
	.line	67
	leal	-9(%ebp),%edi
	movl	%edi,-4(%ebp)
;   68 num1 -> real       = 0x22;
	.line	68
	movl	-4(%ebp),%edi
	movb	$34,(,%edi)
;   69 num1 -> imaginary  = 0xAD;
	.line	69
	movl	-4(%ebp),%edi
	movb	$173,1(%edi)
;   70 //printf("%d",number1.real);
;   71 //printf("%d",number1.imaginary);
;   72 
;   73 //while (1)
;   74 // {
;   75     //which_complex_number(num1);
;   76     pointer_example();
	.line	76
	call	_pointer_example
_$4:
;   77 // }
;   78 }
	.line	78
	popl	%edi
	leave
	ret
_$6:
	.size	_main,_$6-_main
	.globl	_main
;   79 
;   80 /*******************************************************************************/
;   81 /* pointer_example()                                                           */
;   82 /*                                                                             */
;   83 /* Description: This function pointes to variable named "test" which appears   */
;   84 /*              backwards and the pointer points to first byte and the pointer */
;   85 /*              writes and increment by 1 byte.                                */
;   86 /* INPUTS: NONE                                                                */
;   87 /* OUPUTS: NONE                                                                */
;   88 /*******************************************************************************/
;   89 void pointer_example(void)
	.type	_pointer_example,function
_pointer_example:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$124,%esp
	movl	$31,%ecx
_$22:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$22
	pushl	%ebx
	pushl	%edi
;   90 {
	.line	90
;   91 /* Local Variables*/
;   92 uint32_t test;
;   93 uint8_t *ptr;
;   94 uint8_t *ptr2;
;   95 uint8_t pie_ar[100];
;   96 uint8_t i;
;   97 
;   98 uint8_t flag1;
;   99 uint8_t flag2;
;  100 uint8_t flag3;
;  101 uint8_t current_actual_position_c[4];
;  102 
;  103 flag1=0;
	.line	103
	movb	$0,-117(%ebp)
;  104 flag2=1;
	.line	104
	movb	$1,-118(%ebp)
;  105 flag3=0;
	.line	105
	movb	$0,-119(%ebp)
;  106 
;  107 if (flag1==1)
	.line	107
	cmpb	$1,-117(%ebp)
	jne	_$8
;  108   {
;  109    i=1;
	.line	109
	movb	$1,-1(%ebp)
	jmp	_$9
_$8:
;  110   }
;  111 else if(flag2 == 1)
	.line	111
	cmpb	$1,-118(%ebp)
	jne	_$10
;  112    {
;  113    i=2;
	.line	113
	movb	$2,-1(%ebp)
	jmp	_$11
_$10:
;  114    }
;  115 else if (flag3 == 1)
	.line	115
	cmpb	$1,-119(%ebp)
	jne	_$12
;  116    {
;  117    i=3;
	.line	117
	movb	$3,-1(%ebp)
_$12:
;  118    }
;  119  else
;  120    {
	.line	120
_$13:
;  121     /* Do nothing*/
;  122    }
;  123 ptr = &global_ar[0];
	.line	123
_$11:
_$9:
	leal	_global_ar,%edi
	movl	%edi,-8(%ebp)
;  124 
;  125 
;  126 
;  127 
;  128 for (i=0;i<100;i++)
	.line	128
	movb	$0,-1(%ebp)
	jmp	_$17
_$14:
;  129 {
;  130 
;  131   pie_ar[i] = *ptr;
	.line	131
	movzbl	-1(%ebp),%ebx
	movl	-8(%ebp),%edi
	movb	(,%edi),%dl
	movb	%dl,-108(%ebp,%ebx)
;  132   ptr++;
	.line	132
	addl	$1,-8(%ebp)
;  133    /* Stop this evolution if not ascii A-Z Capital or a spaces in between*/
;  134   if((pie_ar[i] < 0x41) || (pie_ar[i] > 0x5A))
	.line	134
	movzbl	-1(%ebp),%ebx
	movzbl	-108(%ebp,%ebx),%ebx
	cmpl	$65,%ebx
	jl	_$20
	cmpl	$90,%ebx
	jle	_$18
_$20:
;  135     {
;  136       i = 100;
	.line	136
	movb	$100,-1(%ebp)
_$18:
;  137     }
;  138    else
;  139      {
	.line	139
_$19:
;  140      }
;  141 
;  142 
;  143 }
	.line	143
_$15:
	.line	128
	addb	$1,-1(%ebp)
_$17:
	movzbl	-1(%ebp),%ebx
	cmpl	$100,%ebx
	jl	_$14
;  144 
;  145 test=0xBADBEEF1;
	.line	145
	movl	$0xbadbeef1,-112(%ebp)
;  146 ptr = &test;      /* Allocate a pointee, make the pointer point to the address of variable test*/
	.line	146
	leal	-112(%ebp),%edi
	movl	%edi,-8(%ebp)
;  147 
;  148 
;  149 ptr2=ptr;         /* Pointer assignment sets ptr2 to point to ptr's pointee*/
	.line	149
	movl	-8(%ebp),%edi
	movl	%edi,-116(%ebp)
;  150 
;  151 *ptr2=13;
	.line	151
	movl	-116(%ebp),%edi
	movb	$13,(,%edi)
;  152 *ptr = 0xba;      /* Dereference the pointer, like stick in a value*/
	.line	152
	movl	-8(%ebp),%edi
	movb	$186,(,%edi)
;  153 ptr++;
	.line	153
	addl	$1,-8(%ebp)
;  154 *ptr = 0xdb;
	.line	154
	movl	-8(%ebp),%edi
	movb	$219,(,%edi)
;  155 ptr++;
	.line	155
	addl	$1,-8(%ebp)
;  156 *ptr = 0xee;
	.line	156
	movl	-8(%ebp),%edi
	movb	$238,(,%edi)
;  157 ptr++;
	.line	157
	addl	$1,-8(%ebp)
;  158 *ptr = 0xf1;
	.line	158
	movl	-8(%ebp),%edi
	movb	$241,(,%edi)
;  159 ptr++;
	.line	159
	addl	$1,-8(%ebp)
_$7:
;  160 
;  161 }
	.line	161
	popl	%edi
	popl	%ebx
	leave
	ret
_$23:
	.size	_pointer_example,_$23-_pointer_example
	.globl	_pointer_example
;  162 
;  163 
;  164 
;  165 void which_complex_number(COMPLEX *system_variables)
	.type	_which_complex_number,function
_which_complex_number:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ecx
	pushl	%eax
	movl	$2,%ecx
_$29:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$29
	pushl	%ebx
	pushl	%esi
	pushl	%edi
;  166 {
	.line	166
;  167 COMPLEX incomming_number;          /* Make this a type COMPLEX*/
;  168 
;  169 uint8_t incomming_realpart;
;  170 uint8_t incomming_imaginarypart;
;  171 
;  172 
;  173 incomming_number= *system_variables;
	.line	173
	leal	-4(%ebp),%edi
	movl	8(%ebp),%esi
	leal	(,%esi),%esi
	movl	$2,%ecx
	rep
	movsb
;  174 
;  175 incomming_realpart = incomming_number.real;
	.line	175
	movb	-4(%ebp),%bl
	movb	%bl,-5(%ebp)
;  176 printf("%x\n",incomming_number.real);
	.line	176
	movzbl	-4(%ebp),%ebx
	pushl	%ebx
	pushl	$_$25
	call	_printf
	addl	$8,%esp
;  177 incomming_imaginarypart = incomming_number.imaginary;
	.line	177
	movb	-3(%ebp),%bl
	movb	%bl,-6(%ebp)
;  178 printf("%x\n",incomming_number.imaginary);
	.line	178
	movzbl	-3(%ebp),%ebx
	pushl	%ebx
	pushl	$_$25
	call	_printf
	addl	$8,%esp
;  179 
;  180 printf("END\n");
	.line	180
	pushl	$_$28
	call	_printf
	addl	$4,%esp
_$24:
;  181 
;  182 }
	.line	182
	popl	%edi
	popl	%esi
	popl	%ebx
	leave
	ret
_$30:
	.size	_which_complex_number,_$30-_which_complex_number
	.globl	_which_complex_number
	.extern	_printf
	.data
_$28:
; "END\n\x0"
	.byte	69,78,68,10,0
_$25:
; "%x\n\x0"
	.byte	37,120,10,0
