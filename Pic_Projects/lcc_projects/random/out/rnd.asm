	.file	"c:\lcc\include\sys\timeb.h"
_$M0:
	.file	"c:\lcc_projects\random\rnd.c"
	.text
	.file	"c:\lcc_projects\random\rnd.h"
_$M1:
	.file	"c:\lcc_projects\random\rnd.c"
_$M2:
	.data
	.type	_rnd_init_flag,object
_rnd_init_flag:
	.byte	0
	.text
;    1 #include <sys\timeb.h>
;    2 #include "rnd.h"
;    3 
;    4 /*********************************************************/
;    5 /* Macro Definitions                                     */
;    6 /*********************************************************/
;    7 
;    8 #define rnd_get_poly_bit(bit)  ((rnd_poly >> bit) & 1)
;    9 
;   10 /*********************************************************/
;   11 /* Variable Definitions                                  */
;   12 /*********************************************************/
;   13 
;   14 static int rnd_poly;
;   15 static unsigned char rnd_init_flag = 0;
;   16 
;   17 /*********************************************************/
;   18 /* rnd_get()                                             */
;   19 /*                                                       */
;   20 /* Description:  This function returns a pseudo-random   */
;   21 /*               number between 0 and (n - 1).  The      */
;   22 /*               algorithm is based on a CRC32-IEEE802.3 */
;   23 /*               polynomial.                             */
;   24 /*                                                       */
;   25 /*               NOTE:  Modulus values (n) less than or  */
;   26 /*               equal to 1 will always return 0.        */
;   27 /*                                                       */
;   28 /* Inputs:       n (random number modulus)               */
;   29 /*                                                       */
;   30 /* Outputs:      result (pseudo-random number)           */
;   31 /*********************************************************/
;   32 
;   33 int rnd_get(int n)
	.type	_rnd_get,function
_rnd_get:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$20,%esp
	movl	$5,%ecx
_$10:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$10
	pushl	%ebx
	pushl	%esi
	pushl	%edi
;   34 {
	.line	34
;   35   int result;
;   36   int temp;
;   37   struct timeb timedata;
;   38 
;   39   if (rnd_init_flag == 0)
	.line	39
	cmpb	$0,_rnd_init_flag
	jne	_$2
;   40   {
;   41     _ftime(&timedata);
	.line	41
	leal	-20(%ebp),%edi
	pushl	%edi
	call	__ftime
	addl	$4,%esp
;   42     rnd_poly = timedata.time;
	.line	42
	movl	-20(%ebp),%edi
	movl	%edi,_rnd_poly
;   43     rnd_init_flag = 1;
	.line	43
	movb	$1,_rnd_init_flag
_$2:
;   44   }
;   45 
;   46   temp = rnd_get_poly_bit(0) ^
	.line	46
	movl	_rnd_poly,%edi
	movl	_rnd_poly,%esi
	andl	$1,%esi
	movl	%edi,%ebx
	sarl	$1,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$3,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$4,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$5,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$7,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$8,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$10,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$11,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$12,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$16,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$22,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	movl	%edi,%ebx
	sarl	$23,%ebx
	andl	$1,%ebx
	xorl	%ebx,%esi
	sarl	$26,%edi
	andl	$1,%edi
	xorl	%edi,%esi
	movl	%esi,-8(%ebp)
;   47          rnd_get_poly_bit(1) ^
;   48          rnd_get_poly_bit(3) ^
;   49          rnd_get_poly_bit(4) ^
;   50          rnd_get_poly_bit(5) ^
;   51          rnd_get_poly_bit(7) ^
;   52          rnd_get_poly_bit(8) ^
;   53          rnd_get_poly_bit(10) ^
;   54          rnd_get_poly_bit(11) ^
;   55          rnd_get_poly_bit(12) ^
;   56          rnd_get_poly_bit(16) ^
;   57          rnd_get_poly_bit(22) ^
;   58          rnd_get_poly_bit(23) ^
;   59          rnd_get_poly_bit(26);
;   60 
;   61   rnd_poly = (rnd_poly << 1) | temp;
	.line	61
	movl	_rnd_poly,%edi
	sall	$1,%edi
	orl	-8(%ebp),%edi
	movl	%edi,_rnd_poly
;   62   rnd_poly = (int)((unsigned int)(rnd_poly << 1) >> 1);
	.line	62
	movl	_rnd_poly,%edi
	sall	$1,%edi
	shrl	$1,%edi
	movl	%edi,_rnd_poly
;   63 
;   64   if (n <= 1)
	.line	64
	cmpl	$1,8(%ebp)
	jg	_$4
;   65   {
;   66     result = 0;
	.line	66
	movl	$0,-4(%ebp)
	jmp	_$5
_$4:
;   67   }
;   68   else
;   69   {
	.line	69
;   70     result = rnd_poly % n;
	.line	70
	movl	_rnd_poly,%eax
	movl	8(%ebp),%ecx
	cdq
	idivl	%ecx
	movl	%edx,-4(%ebp)
_$5:
;   71   }
;   72 
;   73   return result;
	.line	73
	movl	-4(%ebp),%eax
_$1:
;   74 }
	.line	74
	popl	%edi
	popl	%esi
	popl	%ebx
	leave
	ret
_$11:
	.size	_rnd_get,_$11-_rnd_get
	.globl	_rnd_get
	.bss
	.align	2
	.type	_rnd_poly,object
	.lcomm	_rnd_poly,4
	.extern	__ftime
