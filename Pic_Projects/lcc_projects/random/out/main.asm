	.file	"c:\lcc\include\stdio.h"
_$M0:
	.file	"c:\lcc_projects\random\main.c"
	.text
	.file	"c:\lcc\include\safelib.h"
_$M1:
	.file	"c:\lcc\include\stdio.h"
_$M2:
	.file	"c:\lcc_projects\random\rnd.h"
_$M3:
	.file	"c:\lcc_projects\random\main.c"
_$M4:
	.text
;    1   /*                                      Author J.Pietrasik, T.Rode                              Date 17 FEB 2011
;    2   ** This program will scan an interger and also generate an interger from a function call rnd_get();
;    3   ** It will compare and determine if its scaned interger is too high or too low and if its correct it will output the amount of guesses
;    4   ** Also will promt user if they want to quit anytime.
;    5   **
;    6   */
;    7 #include "stdio.h"
;    8 #include "rnd.h"
;    9 #define TRUE            1
;   10 #define FALSE           0
;   11 
;   12 
;   13 int main(void);
;   14 //int store_count(int x);
;   15 
;   16 
;   17 int main(void)
	.type	_main,function
_main:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$32,%esp
	movl	$8,%ecx
_$42:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$42
	pushl	%esi
	pushl	%edi
;   18 {
	.line	18
;   19   int n_returned;                                          // returned value
;   20   int *ptr_n_returned;
;   21   int number;
;   22   int *ptr_number;
;   23   int high_low_flag;
;   24   int guessing_counter=0;
	.line	24
	movl	$0,-16(%ebp)
;   25   int quit_flag;
;   26   int quit=0;
	.line	26
	movl	$0,-24(%ebp)
;   27 
;   28 
;   29  printf("Put in a number from 1 to 99 or 0 to quit\n");
	.line	29
	pushl	$_$2
	call	_printf
	addl	$4,%esp
;   30  scanf("%i",&number);
	.line	30
	leal	-12(%ebp),%edi
	pushl	%edi
	pushl	$_$3
	call	_scanf
	addl	$8,%esp
;   31 
;   32 
;   33  guessing_counter++;
	.line	33
	incl	-16(%ebp)
;   34  ptr_number = &number;
	.line	34
	leal	-12(%ebp),%edi
	movl	%edi,-4(%ebp)
;   35  printf("Here is the scanned number %d\n",number);
	.line	35
	pushl	-12(%ebp)
	pushl	$_$4
	call	_printf
	addl	$8,%esp
;   36  n_returned = rnd_get(100);                               //this is the range
	.line	36
	pushl	$100
	call	_rnd_get
	addl	$4,%esp
	movl	%eax,-8(%ebp)
;   37 
;   38  // quit or not logic//
;   39 
;   40 if(*ptr_number==quit)
	.line	40
	movl	-4(%ebp),%edi
	movl	-24(%ebp),%esi
	cmpl	%esi,(,%edi)
	jne	_$5
;   41     {
;   42      quit_flag=TRUE;
	.line	42
	movl	$1,-20(%ebp)
	jmp	_$6
_$5:
;   43     }
;   44         else
;   45           {
	.line	45
;   46            quit_flag=FALSE;
	.line	46
	movl	$0,-20(%ebp)
_$6:
;   47           }
;   48 
;   49 if(quit_flag==TRUE)
	.line	49
	cmpl	$1,-20(%ebp)
	jne	_$7
;   50 {
;   51     printf("Quit was decided!\n");
	.line	51
	pushl	$_$9
	call	_printf
	addl	$4,%esp
;   52     return 0;
	.line	52
	movl	$0,%eax
	jmp	_$1
_$7:
;   53 }
;   54 
;   55 // Ok quit not wanted the logic will proceed//
;   56 else if(quit_flag==FALSE) //(*ptr_quit!=q_pressed)
	.line	56
	cmpl	$0,-20(%ebp)
	jne	_$10
	jmp	_$13
_$12:
;   57  {
;   58     while((n_returned>*ptr_number)||(n_returned<*ptr_number))
;   59   {                                    // dont need
;   60       if((n_returned>*ptr_number)&&(n_returned!=*ptr_number))      // number guessed is too low
	.line	60
	movl	-8(%ebp),%edi
	movl	-4(%ebp),%esi
	movl	(,%esi),%esi
	cmpl	%esi,%edi
	jle	_$15
	cmpl	%esi,%edi
	je	_$15
;   61       {
;   62        guessing_counter++;
	.line	62
	incl	-16(%ebp)
;   63        printf("Guessed number is too LOW Guess again:\n");
	.line	63
	pushl	$_$17
	call	_printf
	addl	$4,%esp
;   64        printf("\n");
	.line	64
	pushl	$_$18
	call	_printf
	addl	$4,%esp
;   65 
;   66        printf("Put number from 1 to 99 or 0 to quit\n");
	.line	66
	pushl	$_$19
	call	_printf
	addl	$4,%esp
;   67        scanf("%i",&number);
	.line	67
	leal	-12(%ebp),%edi
	pushl	%edi
	pushl	$_$3
	call	_scanf
	addl	$8,%esp
;   68        ptr_number = &number;
	.line	68
	leal	-12(%ebp),%edi
	movl	%edi,-4(%ebp)
;   69        printf("Here is the scanned number %d\n",number);
	.line	69
	pushl	-12(%ebp)
	pushl	$_$4
	call	_printf
	addl	$8,%esp
;   70 
;   71                      //If quit is desired
;   72        if(*ptr_number==quit)
	.line	72
	movl	-4(%ebp),%edi
	movl	-24(%ebp),%esi
	cmpl	%esi,(,%edi)
	jne	_$20
;   73        {
;   74          quit_flag=TRUE;
	.line	74
	movl	$1,-20(%ebp)
_$20:
;   75        }
;   76 
;   77        if(quit_flag==TRUE)
	.line	77
	cmpl	$1,-20(%ebp)
	jne	_$16
;   78        {
;   79          printf("Quit was decided!\n");
	.line	79
	pushl	$_$9
	call	_printf
	addl	$4,%esp
;   80          return 0;
	.line	80
	movl	$0,%eax
	jmp	_$1
;   81        }
;   82         else
;   83           {
	.line	83
;   84           }
;   85 
;   86         }
	.line	86
_$15:
;   87         else
;   88         {
	.line	88
_$16:
;   89         }
;   90 
;   91        if((n_returned<*ptr_number)&&(n_returned!=*ptr_number))             // number guessed to high
	.line	91
	movl	-8(%ebp),%edi
	movl	-4(%ebp),%esi
	movl	(,%esi),%esi
	cmpl	%esi,%edi
	jge	_$24
	cmpl	%esi,%edi
	je	_$24
;   92        {
;   93         guessing_counter++;
	.line	93
	incl	-16(%ebp)
;   94         printf("Guessed number is too HIGH Guess again\n");
	.line	94
	pushl	$_$26
	call	_printf
	addl	$4,%esp
;   95         printf("\n");
	.line	95
	pushl	$_$18
	call	_printf
	addl	$4,%esp
;   96 
;   97         printf("Put in a number from 1 to 99 or 0 to quit\n");
	.line	97
	pushl	$_$2
	call	_printf
	addl	$4,%esp
;   98         scanf("%i",&number);
	.line	98
	leal	-12(%ebp),%edi
	pushl	%edi
	pushl	$_$3
	call	_scanf
	addl	$8,%esp
;   99         ptr_number = &number;
	.line	99
	leal	-12(%ebp),%edi
	movl	%edi,-4(%ebp)
;  100         printf("Here is the scanned number %d\n",number);
	.line	100
	pushl	-12(%ebp)
	pushl	$_$4
	call	_printf
	addl	$8,%esp
;  101 
;  102                       //If quit is desired
;  103          if(*ptr_number==quit)
	.line	103
	movl	-4(%ebp),%edi
	movl	-24(%ebp),%esi
	cmpl	%esi,(,%edi)
	jne	_$27
;  104          {
;  105           quit_flag=TRUE;
	.line	105
	movl	$1,-20(%ebp)
_$27:
;  106          }
;  107             if(quit_flag==TRUE)
	.line	107
	cmpl	$1,-20(%ebp)
	jne	_$25
;  108             {
;  109              printf("Quit was decided!\n");
	.line	109
	pushl	$_$9
	call	_printf
	addl	$4,%esp
;  110              return 0;
	.line	110
	movl	$0,%eax
	jmp	_$1
;  111             }
;  112              else
;  113                {
	.line	113
;  114                }
;  115         }
	.line	115
_$24:
;  116         else
;  117           {
	.line	117
_$25:
;  118           }
;  119 
;  120       }
	.line	120
_$13:
	.line	58
	movl	-8(%ebp),%edi
	movl	-4(%ebp),%esi
	movl	(,%esi),%esi
	cmpl	%esi,%edi
	jg	_$12
	cmpl	%esi,%edi
	jl	_$12
;  121 
;  122        if(n_returned==*ptr_number)                     // number guessed right
	.line	122
	movl	-4(%ebp),%edi
	movl	(,%edi),%edi
	cmpl	%edi,-8(%ebp)
	jne	_$31
;  123        {
;  124         printf("YOU ARE RIGHT\n");
	.line	124
	pushl	$_$33
	call	_printf
	addl	$4,%esp
_$31:
;  125        }
;  126        else
;  127          {
	.line	127
_$32:
;  128          }
;  129 
;  130         printf("the amount of guesses %d\n",guessing_counter);
	.line	130
	pushl	-16(%ebp)
	pushl	$_$34
	call	_printf
	addl	$8,%esp
;  131         main();
	.line	131
	call	_main
_$10:
;  132   }
;  133 
;  134 }
	.line	134
	movl	$0,%eax
_$1:
	popl	%edi
	popl	%esi
	leave
	ret
_$43:
	.size	_main,_$43-_main
	.globl	_main
	.extern	_rnd_get
	.extern	_scanf
	.extern	_printf
	.data
_$34:
; "the amount of guesses %d\n\x0"
	.byte	116,104,101,32,97,109,111,117,110,116,32,111,102,32,103,117
	.byte	101,115,115,101,115,32,37,100,10,0
_$33:
; "YOU ARE RIGHT\n\x0"
	.byte	89,79,85,32,65,82,69,32,82,73,71,72,84,10,0
_$26:
; "Guessed number is too HIGH Guess again\n\x0"
	.byte	71,117,101,115,115,101,100,32,110,117,109,98,101,114,32,105
	.byte	115,32,116,111,111,32,72,73,71,72,32,71,117,101,115
	.byte	115,32,97,103,97,105,110,10,0
_$19:
; "Put number from 1 to 99 or 0 to quit\n\x0"
	.byte	80,117,116,32,110,117,109,98,101,114,32,102,114,111,109,32
	.byte	49,32,116,111,32,57,57,32,111,114,32,48,32,116,111
	.byte	32,113,117,105,116,10,0
_$18:
; "\n\x0"
	.byte	10,0
_$17:
; "Guessed number is too LOW Guess again:\n\x0"
	.byte	71,117,101,115,115,101,100,32,110,117,109,98,101,114,32,105
	.byte	115,32,116,111,111,32,76,79,87,32,71,117,101,115,115
	.byte	32,97,103,97,105,110,58,10,0
_$9:
; "Quit was decided!\n\x0"
	.byte	81,117,105,116,32,119,97,115,32,100,101,99,105,100,101,100
	.byte	33,10,0
_$4:
; "Here is the scanned number %d\n\x0"
	.byte	72,101,114,101,32,105,115,32,116,104,101,32,115,99,97,110
	.byte	110,101,100,32,110,117,109,98,101,114,32,37,100,10,0
_$3:
; "%i\x0"
	.byte	37,105,0
_$2:
; "Put in a number from 1 to 99 or 0 to quit\n\x0"
	.byte	80,117,116,32,105,110,32,97,32,110,117,109,98,101,114,32
	.byte	102,114,111,109,32,49,32,116,111,32,57,57,32,111,114
	.byte	32,48,32,116,111,32,113,117,105,116,10,0
