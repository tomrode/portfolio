#include <sys\timeb.h>
#include "rnd.h"

/*********************************************************/
/* Macro Definitions                                     */
/*********************************************************/

#define rnd_get_poly_bit(bit)  ((rnd_poly >> bit) & 1)

/*********************************************************/
/* Variable Definitions                                  */
/*********************************************************/

static int rnd_poly;
static unsigned char rnd_init_flag = 0;

/*********************************************************/
/* rnd_get()                                             */
/*                                                       */
/* Description:  This function returns a pseudo-random   */
/*               number between 0 and (n - 1).  The      */
/*               algorithm is based on a CRC32-IEEE802.3 */
/*               polynomial.                             */
/*                                                       */
/*               NOTE:  Modulus values (n) less than or  */
/*               equal to 1 will always return 0.        */
/*                                                       */
/* Inputs:       n (random number modulus)               */
/*                                                       */
/* Outputs:      result (pseudo-random number)           */
/*********************************************************/

int rnd_get(int n)
{
  int result;
  int temp;
  struct timeb timedata;

  if (rnd_init_flag == 0)
  {
    _ftime(&timedata);
    rnd_poly = timedata.time;
    rnd_init_flag = 1;
  }

  temp = rnd_get_poly_bit(0) ^
         rnd_get_poly_bit(1) ^
         rnd_get_poly_bit(3) ^
         rnd_get_poly_bit(4) ^
         rnd_get_poly_bit(5) ^
         rnd_get_poly_bit(7) ^
         rnd_get_poly_bit(8) ^
         rnd_get_poly_bit(10) ^
         rnd_get_poly_bit(11) ^
         rnd_get_poly_bit(12) ^
         rnd_get_poly_bit(16) ^
         rnd_get_poly_bit(22) ^
         rnd_get_poly_bit(23) ^
         rnd_get_poly_bit(26);

  rnd_poly = (rnd_poly << 1) | temp;
  rnd_poly = (int)((unsigned int)(rnd_poly << 1) >> 1);

  if (n <= 1)
  {
    result = 0;
  }
  else
  {
    result = rnd_poly % n;
  }

  return result;
}
