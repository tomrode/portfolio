/* 23 FEB 2012 I am just excersizing this skill */

#include "stdio.h"
#include "typedefs.h"
#include "list.h"

#define MAX_STACK_COUNT (10)

/********************************************************************/
/* Type definitions                                                 */
/********************************************************************/
typedef struct
{
RODE_ATTRIBUTES whom_picked;
uint8_t stack_counter;
}WHOMPICKED_STACK;

struct
{
uint16_t flag1    :1;
uint16_t flag2    :1;
uint16_t flag3    :1;
uint16_t flag4    :1;
uint16_t flag5    :4;
uint16_t flag6    :8;
}TEST_FLAGS;



/********************************************************************/
/* Function prototypes                                              */
/********************************************************************/
void question_func(void);
void whompicked_stack(RODE_ATTRIBUTES *the_person_picked);
const ARRAY[] = {12,22,33,44};

int main(void);


/********************************************************************/
/* Function definitions                                             */
/********************************************************************/

/******************************MAIN LOOP HERE************************************************************/
int main(void)
{

int i;
int count=0;
int max_seats;
max_seats =4;

for(i=0;i<max_seats;i++)
  {
   count = 50 - ARRAY[i];
  }




var=99;

question_func();

return 0;
}



/*****************************FUNCTIONS BELOW************************************************************/

void whompicked_stack(RODE_ATTRIBUTES *the_person_picked)
{

//static WHOMPICKED_STACK count;


RODE_ATTRIBUTES complete_person;

WHOMPICKED_STACK *person_picked_ptr;
static WHOMPICKED_STACK current_person;
static WHOMPICKED_STACK overall_stack_ar[MAX_STACK_COUNT];

/* values from the pointer */
uint8_t get_gender;
uint8_t get_age;
uint8_t get_hair_color;
uint8_t i;
complete_person = *the_person_picked;                       /* the data from the pointer*/


get_gender = complete_person.gender;
get_age = complete_person.age;
get_hair_color = complete_person.hair_color;


person_picked_ptr = &current_person;
person_picked_ptr -> whom_picked.gender      = get_gender;    /* load in last pointer contents*/
person_picked_ptr -> whom_picked.age         = get_age;
person_picked_ptr -> whom_picked.hair_color  = get_hair_color;

if (current_person.stack_counter < MAX_STACK_COUNT)
  {
  overall_stack_ar[current_person.stack_counter]=current_person;

  printf("IN FUNCTION WHOMPICKED gender: has %d\n", overall_stack_ar[current_person.stack_counter].whom_picked.gender);
  printf("IN FUNCTION WHOMPICKED age: has %d\n", overall_stack_ar[current_person.stack_counter].whom_picked.age);
  printf("IN FUNCTION WHOMPICKED hair_color: has %d\n", overall_stack_ar[current_person.stack_counter].whom_picked.hair_color);
  printf("IN FUNCTION WHOMPICKED count: has %d\n", overall_stack_ar[current_person.stack_counter].stack_counter);
  current_person.stack_counter++;
  }
 else
 {
  printf("current_person.stack_counter > MAX_STACK_COUNT\n" );
  printf("\n");
  for (i=0;i<MAX_STACK_COUNT;i++)
   {
     printf("STACK gender:      %d\n", overall_stack_ar[i].whom_picked.gender);
     printf("STACK age:         %d\n", overall_stack_ar[i].whom_picked.age);
     printf("STACK hair_color:  %d\n", overall_stack_ar[i].whom_picked.hair_color);
     printf("STACK count:       %d\n", overall_stack_ar[i].stack_counter);
     printf("\n");

  }

 }
//return &current_person;
}





void question_func(void)
{
/* Varibles for function*/
uint8_t  pick_num;
uint8_t *picked_num;
uint8_t try_again;

/* TEST VARIABLE*/
uint8_t gender;
uint8_t age;


/* Structures*/
RODE_ATTRIBUTES person_picked;
WHOMPICKED_STACK stack_count;



printf(" Who do you want to know about in the Rode family?\n");
printf("\n");
printf(" THIS WILL RECORD 10 ENTRIES AND DISPLAY AT THE END\n");
printf("\n");
printf(" Tom = 0, Allison = 1, Henry = 2 ?");
printf("\n");

scanf("%d",&pick_num);

picked_num = &pick_num;
printf("\n");
printf("The number that was entered is %d:", *picked_num);
printf("\n");
printf("\n");
printf("\n");
printf("\n");
/*person_picked = *which_person(*picked_num);*/

printf("\n");

printf("HERE ARE THE THREE ATTRIBUTES FOR THE PERSON PICKED\n");
printf("\n");
printf("RODE_ATTRIBUTES: gender;     FEMALE = 0, MALE = 1           \n");
printf("RODE_ATTRIBUTES: age;        0-255                          \n");
printf("RODE_ATTRIBUTES: hair_color; BLACK=0,BROWN=1,DIRTY_BLONDE=2 \n");
printf("\n");
printf("\n");

if (*picked_num == TOM)
 {
   printf("YOU PICKED:     TOM\n");
 }
else if (*picked_num == ALLISON)
    {
      printf("YOU PICKED:     ALLISON\n");
    }
else if (*picked_num == HENRY)
    {
      printf("YOU PICKED:     HENRY\n");
    }
else
 {
 /* do nothing*/
 }
person_picked = *which_person(*picked_num);     /* This variables has RODE_ATTRIBUTES now inside */

printf("  Here are there attributes\n");
printf("  gender: %d  age: %d  hair_color: %d\n", person_picked.gender,person_picked.age,person_picked.hair_color);
printf("\n");
printf("\n");

//stack_count = *whompicked_stack(); /* OLD WAY*/
whompicked_stack(&person_picked);
//printf(" HERE IS THE COUNT FROM stack_count: %d gender: %d  age: %d hair color: %d\n ", stack_count.stack_counter);//,stack_count.gender,stack_count.age,stack_count.hair_color);
printf("\n");
printf("\n");

printf("  Do you want to try again? 0 = NO, 1 = YES\n");
scanf("%d",&try_again);

 if (try_again == YES)
    {
    question_func();
    }
  else
   {

   }

}
