#include "list.h"
#include "typedefs.h"


uint8_t var = 99;

RODE_ATTRIBUTES *which_person(uint8_t whom_picked)
{

RODE_ATTRIBUTES *tom;
RODE_ATTRIBUTES *allison;
RODE_ATTRIBUTES *henry;
RODE_ATTRIBUTES  person_picked;



if(whom_picked == TOM)
 {
   tom = &person_picked;
   tom     -> gender     = MALE;
   tom     -> age        = 37;
   tom     -> hair_color = DIRTY_BLONDE;
 }
 else if(whom_picked == ALLISON)
 {
   allison = &person_picked;
   allison     -> gender     = FEMALE;
   allison     -> age        = 33;
   allison     -> hair_color = BLACK;
 }
 else if(whom_picked == HENRY)
 {
   henry = &person_picked;
   henry     -> gender     = MALE;
   henry     -> age        = 3;
   henry     -> hair_color = BROWN;
 }
else
{
 /* nothing*/
}

return &person_picked;
}
