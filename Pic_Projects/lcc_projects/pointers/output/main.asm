	.file	"c:\lcc\include\stdio.h"
_$M0:
	.file	"c:\lcc_projects\pointers\main.c"
	.text
	.file	"c:\lcc\include\safelib.h"
_$M1:
	.file	"c:\lcc\include\stdio.h"
_$M2:
	.file	"c:\lcc_projects\pointers\typedefs.h"
_$M3:
	.file	"c:\lcc_projects\pointers\list.h"
_$M4:
	.file	"c:\lcc_projects\pointers\main.c"
_$M5:
	.data
	.globl	_ARRAY
	.align	2
	.type	_ARRAY,object
_ARRAY:
	.long	12
	.long	22
	.long	33
	.long	44
	.text
; i --> %edi
; max_seats --> %esi
; count --> %ebx
;    1 /* 23 FEB 2012 I am just excersizing this skill */
;    2 
;    3 #include "stdio.h"
;    4 #include "typedefs.h"
;    5 #include "list.h"
;    6 
;    7 #define MAX_STACK_COUNT (10)
;    8 
;    9 /********************************************************************/
;   10 /* Type definitions                                                 */
;   11 /********************************************************************/
;   12 typedef struct
;   13 {
;   14 RODE_ATTRIBUTES whom_picked;
;   15 uint8_t stack_counter;
;   16 }WHOMPICKED_STACK;
;   17 
;   18 struct
;   19 {
;   20 uint16_t flag1    :1;
;   21 uint16_t flag2    :1;
;   22 uint16_t flag3    :1;
;   23 uint16_t flag4    :1;
;   24 uint16_t flag5    :4;
;   25 uint16_t flag6    :8;
;   26 }TEST_FLAGS;
;   27 
;   28 
;   29 
;   30 /********************************************************************/
;   31 /* Function prototypes                                              */
;   32 /********************************************************************/
;   33 void question_func(void);
;   34 void whompicked_stack(RODE_ATTRIBUTES *the_person_picked);
;   35 const ARRAY[] = {12,22,33,44};
;   36 
;   37 int main(void);
;   38 
;   39 
;   40 /********************************************************************/
;   41 /* Function definitions                                             */
;   42 /********************************************************************/
;   43 
;   44 /******************************MAIN LOOP HERE************************************************************/
;   45 int main(void)
	.type	_main,function
_main:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ebx
	pushl	%esi
	pushl	%edi
;   46 {
	.line	46
;   47 
;   48 int i;
;   49 int count=0;
	.line	49
	xor	%ebx,%ebx
;   50 int max_seats;
;   51 max_seats =4;
	.line	51
	movl	$4,%esi
;   52 
;   53 for(i=0;i<max_seats;i++)
	.line	53
	xor	%edi,%edi
	jmp	_$9
_$6:
;   54   {
;   55    count = 50 - ARRAY[i];
	.line	55
	movl	$50,%edx
	movl	%edx,%ebx
	subl	_ARRAY(,%edi,4),%ebx
	.line	53
	incl	%edi
_$9:
	cmpl	%esi,%edi
	jl	_$6
;   56   }
;   57 
;   58 
;   59 
;   60 
;   61 var=99;
	.line	61
	movb	$99,_var
;   62 
;   63 question_func();
	.line	63
	call	_question_func
;   64 
;   65 return 0;
	.line	65
	xor	%eax,%eax
;   66 }
	.line	66
	popl	%edi
	popl	%esi
	popl	%ebx
	leave
	ret
_$10:
	.size	_main,_$10-_main
	.globl	_main
	.bss
	.type	_$S1,object
	.lcomm	_$S1,4
	.type	_$S2,object
	.lcomm	_$S2,40
	.text
; person_picked_ptr --> %edi
;   67 
;   68 
;   69 
;   70 /*****************************FUNCTIONS BELOW************************************************************/
;   71 
;   72 void whompicked_stack(RODE_ATTRIBUTES *the_person_picked)
	.type	_whompicked_stack,function
_whompicked_stack:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ecx
	pushl	%eax
	pushl	%ebx
	pushl	%edi
;   73 {
	.line	73
;   74 
;   75 //static WHOMPICKED_STACK count;
;   76 
;   77 
;   78 RODE_ATTRIBUTES complete_person;
;   79 
;   80 WHOMPICKED_STACK *person_picked_ptr;
;   81 static WHOMPICKED_STACK current_person;
;   82 static WHOMPICKED_STACK overall_stack_ar[MAX_STACK_COUNT];
;   83 
;   84 /* values from the pointer */
;   85 uint8_t get_gender;
;   86 uint8_t get_age;
;   87 uint8_t get_hair_color;
;   88 uint8_t i;
;   89 complete_person = *the_person_picked;                       /* the data from the pointer*/
	.line	89
	movl	8(%ebp),%eax
	movl	(,%eax),%eax
	movl	%eax,-5(%ebp)
;   90 
;   91 
;   92 get_gender = complete_person.gender;
	.line	92
	movb	-5(%ebp),%al
	movb	%al,-6(%ebp)
;   93 get_age = complete_person.age;
	.line	93
	movb	-4(%ebp),%al
	movb	%al,-7(%ebp)
;   94 get_hair_color = complete_person.hair_color;
	.line	94
	movb	-3(%ebp),%al
	movb	%al,-8(%ebp)
;   95 
;   96 
;   97 person_picked_ptr = &current_person;
	.line	97
	leal	_$S1,%edi
;   98 person_picked_ptr -> whom_picked.gender      = get_gender;    /* load in last pointer contents*/
	.line	98
	movb	-6(%ebp),%al
	movb	%al,(,%edi)
;   99 person_picked_ptr -> whom_picked.age         = get_age;
	.line	99
	movb	-7(%ebp),%al
	movb	%al,1(%edi)
;  100 person_picked_ptr -> whom_picked.hair_color  = get_hair_color;
	.line	100
	movb	-8(%ebp),%al
	movb	%al,2(%edi)
;  101 
;  102 if (current_person.stack_counter < MAX_STACK_COUNT)
	.line	102
	movb	_$S1+3,%al
	cmpb	$10,%al
	jae	_$14
;  103   {
;  104   overall_stack_ar[current_person.stack_counter]=current_person;
	.line	104
	movzbl	_$S1+3,%ebx
	movl	_$S1,%edi
	movl	%edi,_$S2(,%ebx,4)
;  105 
;  106   printf("IN FUNCTION WHOMPICKED gender: has %d\n", overall_stack_ar[current_person.stack_counter].whom_picked.gender);
	.line	106
	movzbl	_$S1+3,%ebx
	movzbl	_$S2(,%ebx,4),%ebx
	pushl	%ebx
	pushl	$_$18
	call	_printf
;  107   printf("IN FUNCTION WHOMPICKED age: has %d\n", overall_stack_ar[current_person.stack_counter].whom_picked.age);
	.line	107
	movzbl	_$S1+3,%ebx
	movzbl	_$S2+1(,%ebx,4),%ebx
	pushl	%ebx
	pushl	$_$20
	call	_printf
;  108   printf("IN FUNCTION WHOMPICKED hair_color: has %d\n", overall_stack_ar[current_person.stack_counter].whom_picked.hair_color);
	.line	108
	movzbl	_$S1+3,%ebx
	movzbl	_$S2+2(,%ebx,4),%ebx
	pushl	%ebx
	pushl	$_$23
	call	_printf
;  109   printf("IN FUNCTION WHOMPICKED count: has %d\n", overall_stack_ar[current_person.stack_counter].stack_counter);
	.line	109
	movzbl	_$S1+3,%ebx
	movzbl	_$S2+3(,%ebx,4),%ebx
	pushl	%ebx
	pushl	$_$26
	call	_printf
	addl	$32,%esp
;  110   current_person.stack_counter++;
	.line	110
	addb	$1,_$S1+3
	jmp	_$15
_$14:
;  111   }
;  112  else
;  113  {
	.line	113
;  114   printf("current_person.stack_counter > MAX_STACK_COUNT\n" );
	.line	114
	pushl	$_$30
	call	_printf
;  115   printf("\n");
	.line	115
	pushl	$_$31
	call	_printf
	addl	$8,%esp
;  116   for (i=0;i<MAX_STACK_COUNT;i++)
	.line	116
	movb	$0,-1(%ebp)
	jmp	_$35
_$32:
;  117    {
;  118      printf("STACK gender:      %d\n", overall_stack_ar[i].whom_picked.gender);
	.line	118
	movzbl	-1(%ebp),%ebx
	movzbl	_$S2(,%ebx,4),%ebx
	pushl	%ebx
	pushl	$_$36
	call	_printf
;  119      printf("STACK age:         %d\n", overall_stack_ar[i].whom_picked.age);
	.line	119
	movzbl	-1(%ebp),%ebx
	movzbl	_$S2+1(,%ebx,4),%ebx
	pushl	%ebx
	pushl	$_$37
	call	_printf
;  120      printf("STACK hair_color:  %d\n", overall_stack_ar[i].whom_picked.hair_color);
	.line	120
	movzbl	-1(%ebp),%ebx
	movzbl	_$S2+2(,%ebx,4),%ebx
	pushl	%ebx
	pushl	$_$39
	call	_printf
;  121      printf("STACK count:       %d\n", overall_stack_ar[i].stack_counter);
	.line	121
	movzbl	-1(%ebp),%ebx
	movzbl	_$S2+3(,%ebx,4),%ebx
	pushl	%ebx
	pushl	$_$41
	call	_printf
;  122      printf("\n");
	.line	122
	pushl	$_$31
	call	_printf
	addl	$36,%esp
	.line	116
	addb	$1,-1(%ebp)
_$35:
	movzbl	-1(%ebp),%eax
	cmpb	$10,%al
	jb	_$32
;  123 
;  124   }
;  125 
;  126  }
	.line	126
_$15:
;  127 //return &current_person;
;  128 }
	.line	128
	popl	%edi
	popl	%ebx
	leave
	ret
_$43:
	.size	_whompicked_stack,_$43-_whompicked_stack
	.globl	_whompicked_stack
; picked_num --> %edi
;  129 
;  130 
;  131 
;  132 
;  133 
;  134 void question_func(void)
	.type	_question_func,function
_question_func:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$12,%esp
	pushl	%esi
	pushl	%edi
;  135 {
	.line	135
;  136 /* Varibles for function*/
;  137 uint8_t  pick_num;
;  138 uint8_t *picked_num;
;  139 uint8_t try_again;
;  140 
;  141 /* TEST VARIABLE*/
;  142 uint8_t gender;
;  143 uint8_t age;
;  144 
;  145 
;  146 /* Structures*/
;  147 RODE_ATTRIBUTES person_picked;
;  148 WHOMPICKED_STACK stack_count;
;  149 
;  150 
;  151 
;  152 printf(" Who do you want to know about in the Rode family?\n");
	.line	152
	pushl	$_$45
	call	_printf
;  153 printf("\n");
	.line	153
	pushl	$_$31
	call	_printf
;  154 printf(" THIS WILL RECORD 10 ENTRIES AND DISPLAY AT THE END\n");
	.line	154
	pushl	$_$46
	call	_printf
;  155 printf("\n");
	.line	155
	pushl	$_$31
	call	_printf
;  156 printf(" Tom = 0, Allison = 1, Henry = 2 ?");
	.line	156
	pushl	$_$47
	call	_printf
;  157 printf("\n");
	.line	157
	pushl	$_$31
	call	_printf
;  158 
;  159 scanf("%d",&pick_num);
	.line	159
	leal	-5(%ebp),%eax
	pushl	%eax
	pushl	$_$48
	call	_scanf
;  160 
;  161 picked_num = &pick_num;
	.line	161
	leal	-5(%ebp),%edi
;  162 printf("\n");
	.line	162
	pushl	$_$31
	call	_printf
;  163 printf("The number that was entered is %d:", *picked_num);
	.line	163
	movzbl	(,%edi),%eax
	pushl	%eax
	pushl	$_$49
	call	_printf
;  164 printf("\n");
	.line	164
	pushl	$_$31
	call	_printf
;  165 printf("\n");
	.line	165
	pushl	$_$31
	call	_printf
;  166 printf("\n");
	.line	166
	pushl	$_$31
	call	_printf
;  167 printf("\n");
	.line	167
	pushl	$_$31
	call	_printf
;  168 /*person_picked = *which_person(*picked_num);*/
;  169 
;  170 printf("\n");
	.line	170
	pushl	$_$31
	call	_printf
;  171 
;  172 printf("HERE ARE THE THREE ATTRIBUTES FOR THE PERSON PICKED\n");
	.line	172
	pushl	$_$50
	call	_printf
;  173 printf("\n");
	.line	173
	pushl	$_$31
	call	_printf
;  174 printf("RODE_ATTRIBUTES: gender;     FEMALE = 0, MALE = 1           \n");
	.line	174
	pushl	$_$51
	call	_printf
;  175 printf("RODE_ATTRIBUTES: age;        0-255                          \n");
	.line	175
	pushl	$_$52
	call	_printf
;  176 printf("RODE_ATTRIBUTES: hair_color; BLACK=0,BROWN=1,DIRTY_BLONDE=2 \n");
	.line	176
	pushl	$_$53
	call	_printf
;  177 printf("\n");
	.line	177
	pushl	$_$31
	call	_printf
;  178 printf("\n");
	.line	178
	pushl	$_$31
	call	_printf
	addl	$92,%esp
;  179 
;  180 if (*picked_num == TOM)
	.line	180
	cmpb	$0,(,%edi)
	jne	_$54
;  181  {
;  182    printf("YOU PICKED:     TOM\n");
	.line	182
	pushl	$_$56
	call	_printf
	popl	%ecx
	jmp	_$55
_$54:
;  183  }
;  184 else if (*picked_num == ALLISON)
	.line	184
	cmpb	$1,(,%edi)
	jne	_$57
;  185     {
;  186       printf("YOU PICKED:     ALLISON\n");
	.line	186
	pushl	$_$59
	call	_printf
	popl	%ecx
	jmp	_$55
_$57:
;  187     }
;  188 else if (*picked_num == HENRY)
	.line	188
	cmpb	$2,(,%edi)
	jne	_$55
;  189     {
;  190       printf("YOU PICKED:     HENRY\n");
	.line	190
	pushl	$_$62
	call	_printf
	popl	%ecx
;  191     }
;  192 else
;  193  {
	.line	193
;  194  /* do nothing*/
;  195  }
;  196 person_picked = *which_person(*picked_num);     /* This variables has RODE_ATTRIBUTES now inside */
	.line	196
_$55:
	movzbl	(,%edi),%eax
	pushl	%eax
	call	_which_person
	movl	(,%eax),%esi
	movl	%esi,-4(%ebp)
;  197 
;  198 printf("  Here are there attributes\n");
	.line	198
	pushl	$_$63
	call	_printf
;  199 printf("  gender: %d  age: %d  hair_color: %d\n", person_picked.gender,person_picked.age,person_picked.hair_color);
	.line	199
	movzbl	-2(%ebp),%eax
	pushl	%eax
	movzbl	-3(%ebp),%eax
	pushl	%eax
	movzbl	-4(%ebp),%eax
	pushl	%eax
	pushl	$_$64
	call	_printf
;  200 printf("\n");
	.line	200
	pushl	$_$31
	call	_printf
;  201 printf("\n");
	.line	201
	pushl	$_$31
	call	_printf
;  202 
;  203 //stack_count = *whompicked_stack(); /* OLD WAY*/
;  204 whompicked_stack(&person_picked);
	.line	204
	leal	-4(%ebp),%eax
	pushl	%eax
	call	_whompicked_stack
;  205 //printf(" HERE IS THE COUNT FROM stack_count: %d gender: %d  age: %d hair color: %d\n ", stack_count.stack_counter);//,stack_count.gender,stack_count.age,stack_count.hair_color);
;  206 printf("\n");
	.line	206
	pushl	$_$31
	call	_printf
;  207 printf("\n");
	.line	207
	pushl	$_$31
	call	_printf
;  208 
;  209 printf("  Do you want to try again? 0 = NO, 1 = YES\n");
	.line	209
	pushl	$_$67
	call	_printf
;  210 scanf("%d",&try_again);
	.line	210
	leal	-6(%ebp),%eax
	pushl	%eax
	pushl	$_$48
	call	_scanf
	addl	$56,%esp
;  211 
;  212  if (try_again == YES)
	.line	212
	cmpb	$1,-6(%ebp)
	jne	_$68
;  213     {
;  214     question_func();
	.line	214
	call	_question_func
_$68:
;  215     }
;  216   else
;  217    {
	.line	217
;  218 
;  219    }
;  220 
;  221 }
	.line	221
	popl	%edi
	popl	%esi
	leave
	ret
_$71:
	.size	_question_func,_$71-_question_func
	.globl	_question_func
	.bss
	.globl	_TEST_FLAGS
	.align	2
	.type	_TEST_FLAGS,object
	.comm	_TEST_FLAGS,4
	.extern	_which_person
	.extern	_var
	.extern	_scanf
	.extern	_printf
	.data
_$67:
; "  Do you want to try again? 0 = NO, 1 = YES\n\x0"
	.byte	32,32,68,111,32,121,111,117,32,119,97,110,116,32,116,111
	.byte	32,116,114,121,32,97,103,97,105,110,63,32,48,32,61
	.byte	32,78,79,44,32,49,32,61,32,89,69,83,10,0
_$64:
; "  gender: %d  age: %d  hair_color: %d\n\x0"
	.byte	32,32,103,101,110,100,101,114,58,32,37,100,32,32,97,103
	.byte	101,58,32,37,100,32,32,104,97,105,114,95,99,111,108
	.byte	111,114,58,32,37,100,10,0
_$63:
; "  Here are there attributes\n\x0"
	.byte	32,32,72,101,114,101,32,97,114,101,32,116,104,101,114,101
	.byte	32,97,116,116,114,105,98,117,116,101,115,10,0
_$62:
; "YOU PICKED:     HENRY\n\x0"
	.byte	89,79,85,32,80,73,67,75,69,68,58,32,32,32,32,32
	.byte	72,69,78,82,89,10,0
_$59:
; "YOU PICKED:     ALLISON\n\x0"
	.byte	89,79,85,32,80,73,67,75,69,68,58,32,32,32,32,32
	.byte	65,76,76,73,83,79,78,10,0
_$56:
; "YOU PICKED:     TOM\n\x0"
	.byte	89,79,85,32,80,73,67,75,69,68,58,32,32,32,32,32
	.byte	84,79,77,10,0
_$53:
; "RODE_ATTRIBUTES: hair_color; BLACK=0,BROWN=1,DIRTY_BLONDE=2 \n\x0"
	.byte	82,79,68,69,95,65,84,84,82,73,66,85,84,69,83,58
	.byte	32,104,97,105,114,95,99,111,108,111,114,59,32,66,76
	.byte	65,67,75,61,48,44,66,82,79,87,78,61,49,44,68
	.byte	73,82,84,89,95,66,76,79,78,68,69,61,50,32,10
	.byte	0
_$52:
; "RODE_ATTRIBUTES: age;        0-255                          \n\x0"
	.byte	82,79,68,69,95,65,84,84,82,73,66,85,84,69,83,58
	.byte	32,97,103,101,59,32,32,32,32,32,32,32,32,48,45
	.byte	50,53,53,32,32,32,32,32,32,32,32,32,32,32,32
	.byte	32,32,32,32,32,32,32,32,32,32,32,32,32,32,10
	.byte	0
_$51:
; "RODE_ATTRIBUTES: gender;     FEMALE = 0, MALE = 1           \n\x0"
	.byte	82,79,68,69,95,65,84,84,82,73,66,85,84,69,83,58
	.byte	32,103,101,110,100,101,114,59,32,32,32,32,32,70,69
	.byte	77,65,76,69,32,61,32,48,44,32,77,65,76,69,32
	.byte	61,32,49,32,32,32,32,32,32,32,32,32,32,32,10
	.byte	0
_$50:
; "HERE ARE THE THREE ATTRIBUTES FOR THE PERSON PICKED\n\x0"
	.byte	72,69,82,69,32,65,82,69,32,84,72,69,32,84,72,82
	.byte	69,69,32,65,84,84,82,73,66,85,84,69,83,32,70
	.byte	79,82,32,84,72,69,32,80,69,82,83,79,78,32,80
	.byte	73,67,75,69,68,10,0
_$49:
; "The number that was entered is %d:\x0"
	.byte	84,104,101,32,110,117,109,98,101,114,32,116,104,97,116,32
	.byte	119,97,115,32,101,110,116,101,114,101,100,32,105,115,32
	.byte	37,100,58,0
_$48:
; "%d\x0"
	.byte	37,100,0
_$47:
; " Tom = 0, Allison = 1, Henry = 2 ?\x0"
	.byte	32,84,111,109,32,61,32,48,44,32,65,108,108,105,115,111
	.byte	110,32,61,32,49,44,32,72,101,110,114,121,32,61,32
	.byte	50,32,63,0
_$46:
; " THIS WILL RECORD 10 ENTRIES AND DISPLAY AT THE END\n\x0"
	.byte	32,84,72,73,83,32,87,73,76,76,32,82,69,67,79,82
	.byte	68,32,49,48,32,69,78,84,82,73,69,83,32,65,78
	.byte	68,32,68,73,83,80,76,65,89,32,65,84,32,84,72
	.byte	69,32,69,78,68,10,0
_$45:
; " Who do you want to know about in the Rode family?\n\x0"
	.byte	32,87,104,111,32,100,111,32,121,111,117,32,119,97,110,116
	.byte	32,116,111,32,107,110,111,119,32,97,98,111,117,116,32
	.byte	105,110,32,116,104,101,32,82,111,100,101,32,102,97,109
	.byte	105,108,121,63,10,0
_$41:
; "STACK count:       %d\n\x0"
	.byte	83,84,65,67,75,32,99,111,117,110,116,58,32,32,32,32
	.byte	32,32,32,37,100,10,0
_$39:
; "STACK hair_color:  %d\n\x0"
	.byte	83,84,65,67,75,32,104,97,105,114,95,99,111,108,111,114
	.byte	58,32,32,37,100,10,0
_$37:
; "STACK age:         %d\n\x0"
	.byte	83,84,65,67,75,32,97,103,101,58,32,32,32,32,32,32
	.byte	32,32,32,37,100,10,0
_$36:
; "STACK gender:      %d\n\x0"
	.byte	83,84,65,67,75,32,103,101,110,100,101,114,58,32,32,32
	.byte	32,32,32,37,100,10,0
_$31:
; "\n\x0"
	.byte	10,0
_$30:
; "current_person.stack_counter > MAX_STACK_COUNT\n\x0"
	.byte	99,117,114,114,101,110,116,95,112,101,114,115,111,110,46,115
	.byte	116,97,99,107,95,99,111,117,110,116,101,114,32,62,32
	.byte	77,65,88,95,83,84,65,67,75,95,67,79,85,78,84
	.byte	10,0
_$26:
; "IN FUNCTION WHOMPICKED count: has %d\n\x0"
	.byte	73,78,32,70,85,78,67,84,73,79,78,32,87,72,79,77
	.byte	80,73,67,75,69,68,32,99,111,117,110,116,58,32,104
	.byte	97,115,32,37,100,10,0
_$23:
; "IN FUNCTION WHOMPICKED hair_color: has %d\n\x0"
	.byte	73,78,32,70,85,78,67,84,73,79,78,32,87,72,79,77
	.byte	80,73,67,75,69,68,32,104,97,105,114,95,99,111,108
	.byte	111,114,58,32,104,97,115,32,37,100,10,0
_$20:
; "IN FUNCTION WHOMPICKED age: has %d\n\x0"
	.byte	73,78,32,70,85,78,67,84,73,79,78,32,87,72,79,77
	.byte	80,73,67,75,69,68,32,97,103,101,58,32,104,97,115
	.byte	32,37,100,10,0
_$18:
; "IN FUNCTION WHOMPICKED gender: has %d\n\x0"
	.byte	73,78,32,70,85,78,67,84,73,79,78,32,87,72,79,77
	.byte	80,73,67,75,69,68,32,103,101,110,100,101,114,58,32
	.byte	104,97,115,32,37,100,10,0
