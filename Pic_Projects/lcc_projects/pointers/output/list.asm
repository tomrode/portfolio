	.file	"c:\lcc_projects\pointers\typedefs.h"
_$M0:
	.file	"c:\lcc_projects\pointers\list.c"
	.text
	.file	"c:\lcc_projects\pointers\list.h"
_$M1:
	.file	"c:\lcc_projects\pointers\list.c"
_$M2:
	.data
	.globl	_var
	.type	_var,object
_var:
	.byte	99
	.text
; tom --> %edi
; tom --> %edi
;    1 #include "list.h"
;    2 #include "typedefs.h"
;    3 
;    4 
;    5 uint8_t var = 99;
;    6 
;    7 RODE_ATTRIBUTES *which_person(uint8_t whom_picked)
	.type	_which_person,function
_which_person:
	pushl	%ebp
	movl	%esp,%ebp
	pushl	%ecx
	pushl	%eax
;    8 {
	.line	8
;    9 
;   10 RODE_ATTRIBUTES *tom;
;   11 RODE_ATTRIBUTES *allison;
;   12 RODE_ATTRIBUTES *henry;
;   13 RODE_ATTRIBUTES  person_picked;
;   14 
;   15 
;   16 
;   17 if(whom_picked == TOM)
	.line	17
	cmpb	$0,8(%ebp)
	jne	_$4
;   18  {
;   19    tom = &person_picked;
	.line	19
	leal	-4(%ebp),%ecx
;   20    tom     -> gender     = MALE;
	.line	20
	movb	$1,(,%ecx)
;   21    tom     -> age        = 37;
	.line	21
	movb	$37,1(%ecx)
;   22    tom     -> hair_color = DIRTY_BLONDE;
	.line	22
	movb	$2,2(%ecx)
	jmp	_$5
_$4:
;   23  }
;   24  else if(whom_picked == ALLISON)
	.line	24
	cmpb	$1,8(%ebp)
	jne	_$6
;   25  {
;   26    allison = &person_picked;
	.line	26
	leal	-4(%ebp),%ecx
;   27    allison     -> gender     = FEMALE;
	.line	27
	movb	$0,(,%ecx)
;   28    allison     -> age        = 33;
	.line	28
	movb	$33,1(%ecx)
;   29    allison     -> hair_color = BLACK;
	.line	29
	movb	$0,2(%ecx)
	jmp	_$5
_$6:
;   30  }
;   31  else if(whom_picked == HENRY)
	.line	31
	cmpb	$2,8(%ebp)
	jne	_$5
;   32  {
;   33    henry = &person_picked;
	.line	33
	leal	-4(%ebp),%eax
	movl	%eax,-8(%ebp)
;   34    henry     -> gender     = MALE;
	.line	34
	movb	$1,(,%eax)
;   35    henry     -> age        = 3;
	.line	35
	movb	$3,1(%eax)
;   36    henry     -> hair_color = BROWN;
	.line	36
	movb	$1,2(%eax)
;   37  }
;   38 else
;   39 {
	.line	39
;   40  /* nothing*/
;   41 }
;   42 
;   43 return &person_picked;
	.line	43
_$5:
	leal	-4(%ebp),%eax
;   44 }
	.line	44
	leave
	ret
_$10:
	.size	_which_person,_$10-_which_person
	.globl	_which_person
