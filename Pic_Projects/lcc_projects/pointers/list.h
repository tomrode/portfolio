#include "typedefs.h"
#ifndef LIST_H
#define LIST_H

typedef struct
{
uint8_t gender;
uint8_t age;
uint8_t hair_color;
}RODE_ATTRIBUTES;



extern uint8_t var;
enum sex
{
FEMALE,
MALE
};

enum hair_color
{
BLACK,         /* implied 0*/
BROWN,
DIRTY_BLONDE
};


enum whom_picked
{
TOM,         /* implied 0*/
ALLISON,
HENRY
};

enum boolean
{
NO,
YES
};

/* Function prototypes */



RODE_ATTRIBUTES* which_person(uint8_t whom_picked);


#endif
