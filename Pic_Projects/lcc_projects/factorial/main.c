#include "stdio.h"

/* function prototypes*/
int factorial_func(int fact_number);

void main(void)
{
int fact_num;
int return_fact_num;

printf("Scan in a number to be used as a factiorial\n");
scanf("%d", &fact_num);
return_fact_num = factorial_func(fact_num);
printf("Here is the factiorial %d\n", return_fact_num);

}

int factorial_func(int fact_number)
{

int x = fact_number;
int i;

if (x <= 0)
 {
  x=1;
 }
else
 {
 for(i=1; i<fact_number; i++)
  {
   x *= (fact_number-i)*1;
  }
 }
return x;
}
