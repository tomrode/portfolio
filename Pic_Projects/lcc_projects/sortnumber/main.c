  /*                                      Author T.Rode                              Date 24 FEB 2011
  ** This program will scan in 5 interger from a user prompt it will sort them in ascending order
  */
#include "stdio.h"
#include "func1.h"
#define MAX_LENGTH   5
#define n_PRESSED   'n'
#define y_PRESSED   'y'
#define TRUE         1
#define FALSE        0
int main(void);


int main(void)
{
int number1;
int number2;
int number3;
int number4;
int number5;
int i;
int flag;
int quit_flag=0;
char quit_wish[1];
char *ptr_quit_wish;
char temp;
char sort_array[MAX_LENGTH];


////// This is if you don't want to quit

do
 {
  printf("This program will sort 5 numbers and put them in ascending order\n");
  printf("  \n");
  printf("Enter number 1 : ");
  scanf("%i",&number1);
  printf("  \n");
  printf("Enter number 2 : ");
  scanf("%i",&number2);
  printf("  \n");
  printf("Enter number 3 : ");
  scanf("%i",&number3);
  printf("  \n");
  printf("Enter number 4 : ");
  scanf("%i",&number4);
  printf("  \n");
  printf("Enter number 5 : ");
  scanf("%i",&number5);


  sort_array[0]=number1;
  sort_array[1]=number2;
  sort_array[2]=number3;
  sort_array[3]=number4;
  sort_array[4]=number5;
/////////////////////////////////////////// THE FUNCTION

int firstelement(int *number1, int n=5);  // n = number of elements

        flag=1;
        while(flag==1)                            // I want to say count flag
         {
         flag=0;
         for(i=0; i<MAX_LENGTH-1; i++)
          {
           if(sort_array[i]>sort_array[i+1])                           // This will swap the elements so it will make the order ascend
             {
             temp = sort_array[i+1];
             sort_array[i+1] = sort_array[i];
             sort_array[i] = temp;
             flag=1;
             }
          }
         }
////////////////////////////////////////////
  number1=sort_array[0];
  number2=sort_array[1];
  number3=sort_array[2];
  number4=sort_array[3];
  number5=sort_array[4];

  printf("\n");
  printf(" The result : %3d\n",number1);
  printf("\n");
  printf(" The result : %3d\n",number2);
  printf("\n");
  printf(" The result : %3d\n",number3);
  printf("\n");
  printf(" The result : %3d\n",number4);
  printf("\n");
  printf(" The result : %3d\n",number5);
  printf(" Do you wish to quit? Press 'y' for yes or 'n' for no\n");
  scanf("%s",&quit_wish);
  ptr_quit_wish = &quit_wish;              //&quit_wish;

if(quit_wish[0]==y_PRESSED)                // Good example of using the strinr element 0 and not a pointer
 {
  quit_flag=FALSE;
  printf("y pressed\n");
  return 0;
 }
 else if(*ptr_quit_wish==n_PRESSED)
  {
  quit_flag=TRUE;
  printf("n pressed\n");
  }
 }
while (quit_flag==TRUE);
}
