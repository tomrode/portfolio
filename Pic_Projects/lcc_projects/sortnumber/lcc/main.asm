	.file	"c:\lcc\include\stdio.h"
_$M0:
	.file	"c:\lcc_projects\sortnumber\main.c"
	.text
	.file	"c:\lcc\include\safelib.h"
_$M1:
	.file	"c:\lcc\include\stdio.h"
_$M2:
	.file	"c:\lcc_projects\sortnumber\func1.h"
_$M3:
	.file	"c:\lcc_projects\sortnumber\main.c"
_$M4:
	.text
;    1   /*                                      Author T.Rode                              Date 24 FEB 2011
;    2   ** This program will scan in 5 interger from a user prompt it will sort them in ascending order
;    3   */
;    4 #include "stdio.h"
;    5 #include "func1.h"
;    6 #define MAX_LENGTH   5
;    7 #define n_PRESSED   'n'
;    8 #define y_PRESSED   'y'
;    9 #define TRUE         1
;   10 #define FALSE        0
;   11 int main(void);
;   12 
;   13 
;   14 int main(void)
	.type	_main,function
_main:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$48,%esp
	movl	$12,%ecx
_$45:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$45
	pushl	%ebx
	pushl	%edi
;   15 {
	.line	15
;   16 int number1;
;   17 int number2;
;   18 int number3;
;   19 int number4;
;   20 int number5;
;   21 int i;
;   22 int flag;
;   23 int quit_flag=0;
	.line	23
	movl	$0,-44(%ebp)
_$2:
;   24 char quit_wish[1];
;   25 char *ptr_quit_wish;
;   26 char temp;
;   27 char sort_array[MAX_LENGTH];
;   28 
;   29 
;   30 ////// This is if you don't want to quit
;   31 
;   32 do
;   33  {
;   34   printf("This program will sort 5 numbers and put them in ascending order\n");
	.line	34
	pushl	$_$5
	call	_printf
	addl	$4,%esp
;   35   printf("  \n");
	.line	35
	pushl	$_$6
	call	_printf
	addl	$4,%esp
;   36   printf("Enter number 1 : ");
	.line	36
	pushl	$_$7
	call	_printf
	addl	$4,%esp
;   37   scanf("%i",&number1);
	.line	37
	leal	-20(%ebp),%edi
	pushl	%edi
	pushl	$_$8
	call	_scanf
	addl	$8,%esp
;   38   printf("  \n");
	.line	38
	pushl	$_$6
	call	_printf
	addl	$4,%esp
;   39   printf("Enter number 2 : ");
	.line	39
	pushl	$_$9
	call	_printf
	addl	$4,%esp
;   40   scanf("%i",&number2);
	.line	40
	leal	-24(%ebp),%edi
	pushl	%edi
	pushl	$_$8
	call	_scanf
	addl	$8,%esp
;   41   printf("  \n");
	.line	41
	pushl	$_$6
	call	_printf
	addl	$4,%esp
;   42   printf("Enter number 3 : ");
	.line	42
	pushl	$_$10
	call	_printf
	addl	$4,%esp
;   43   scanf("%i",&number3);
	.line	43
	leal	-28(%ebp),%edi
	pushl	%edi
	pushl	$_$8
	call	_scanf
	addl	$8,%esp
;   44   printf("  \n");
	.line	44
	pushl	$_$6
	call	_printf
	addl	$4,%esp
;   45   printf("Enter number 4 : ");
	.line	45
	pushl	$_$11
	call	_printf
	addl	$4,%esp
;   46   scanf("%i",&number4);
	.line	46
	leal	-32(%ebp),%edi
	pushl	%edi
	pushl	$_$8
	call	_scanf
	addl	$8,%esp
;   47   printf("  \n");
	.line	47
	pushl	$_$6
	call	_printf
	addl	$4,%esp
;   48   printf("Enter number 5 : ");
	.line	48
	pushl	$_$12
	call	_printf
	addl	$4,%esp
;   49   scanf("%i",&number5);
	.line	49
	leal	-36(%ebp),%edi
	pushl	%edi
	pushl	$_$8
	call	_scanf
	addl	$8,%esp
;   50 
;   51 
;   52   sort_array[0]=number1;
	.line	52
	movl	-20(%ebp),%edi
	movl	%di,%bx
	movb	%bl,-9(%ebp)
;   53   sort_array[1]=number2;
	.line	53
	movl	-24(%ebp),%edi
	movl	%di,%bx
	movb	%bl,-8(%ebp)
;   54   sort_array[2]=number3;
	.line	54
	movl	-28(%ebp),%edi
	movl	%di,%bx
	movb	%bl,-7(%ebp)
;   55   sort_array[3]=number4;
	.line	55
	movl	-32(%ebp),%edi
	movl	%di,%bx
	movb	%bl,-6(%ebp)
;   56   sort_array[4]=number5;
	.line	56
	movl	-36(%ebp),%edi
	movl	%di,%bx
	movb	%bl,-5(%ebp)
;   57 /////////////////////////////////////////// THE FUNCTION
;   58 
;   59 int firstelement(int *number1, int n=5);  // n = number of elements
;   60 
;   61         flag=1;
	.line	61
	movl	$1,-16(%ebp)
	jmp	_$18
_$17:
;   62         while(flag==1)                            // I want to say count flag
;   63          {
;   64          flag=0;
	.line	64
	movl	$0,-16(%ebp)
;   65          for(i=0; i<MAX_LENGTH-1; i++)
	.line	65
	movl	$0,-4(%ebp)
_$20:
;   66           {
;   67            if(sort_array[i]>sort_array[i+1])                           // This will swap the elements so it will make the order ascend
	.line	67
	movl	-4(%ebp),%edi
	movb	-9(%ebp,%edi),%bl
	cmpb	-8(%ebp,%edi),%bl
	jle	_$24
;   68              {
;   69              temp = sort_array[i+1];
	.line	69
	movl	-4(%ebp),%edi
	movb	-8(%ebp,%edi),%bl
	movb	%bl,-10(%ebp)
;   70              sort_array[i+1] = sort_array[i];
	.line	70
	movl	-4(%ebp),%edi
	movb	-9(%ebp,%edi),%bl
	movb	%bl,-8(%ebp,%edi)
;   71              sort_array[i] = temp;
	.line	71
	movl	-4(%ebp),%edi
	movb	-10(%ebp),%bl
	movb	%bl,-9(%ebp,%edi)
;   72              flag=1;
	.line	72
	movl	$1,-16(%ebp)
_$24:
;   73              }
;   74           }
	.line	74
_$21:
	.line	65
	incl	-4(%ebp)
	cmpl	$4,-4(%ebp)
	jl	_$20
;   75          }
	.line	75
_$18:
	.line	62
	cmpl	$1,-16(%ebp)
	je	_$17
;   76 ////////////////////////////////////////////
;   77   number1=sort_array[0];
	.line	77
	movsbl	-9(%ebp),%ebx
	movl	%ebx,-20(%ebp)
;   78   number2=sort_array[1];
	.line	78
	movsbl	-8(%ebp),%ebx
	movl	%ebx,-24(%ebp)
;   79   number3=sort_array[2];
	.line	79
	movsbl	-7(%ebp),%ebx
	movl	%ebx,-28(%ebp)
;   80   number4=sort_array[3];
	.line	80
	movsbl	-6(%ebp),%ebx
	movl	%ebx,-32(%ebp)
;   81   number5=sort_array[4];
	.line	81
	movsbl	-5(%ebp),%ebx
	movl	%ebx,-36(%ebp)
;   82 
;   83   printf("\n");
	.line	83
	pushl	$_$33
	call	_printf
	addl	$4,%esp
;   84   printf(" The result : %3d\n",number1);
	.line	84
	pushl	-20(%ebp)
	pushl	$_$34
	call	_printf
	addl	$8,%esp
;   85   printf("\n");
	.line	85
	pushl	$_$33
	call	_printf
	addl	$4,%esp
;   86   printf(" The result : %3d\n",number2);
	.line	86
	pushl	-24(%ebp)
	pushl	$_$34
	call	_printf
	addl	$8,%esp
;   87   printf("\n");
	.line	87
	pushl	$_$33
	call	_printf
	addl	$4,%esp
;   88   printf(" The result : %3d\n",number3);
	.line	88
	pushl	-28(%ebp)
	pushl	$_$34
	call	_printf
	addl	$8,%esp
;   89   printf("\n");
	.line	89
	pushl	$_$33
	call	_printf
	addl	$4,%esp
;   90   printf(" The result : %3d\n",number4);
	.line	90
	pushl	-32(%ebp)
	pushl	$_$34
	call	_printf
	addl	$8,%esp
;   91   printf("\n");
	.line	91
	pushl	$_$33
	call	_printf
	addl	$4,%esp
;   92   printf(" The result : %3d\n",number5);
	.line	92
	pushl	-36(%ebp)
	pushl	$_$34
	call	_printf
	addl	$8,%esp
;   93   printf(" Do you wish to quit? Press 'y' for yes or 'n' for no\n");
	.line	93
	pushl	$_$35
	call	_printf
	addl	$4,%esp
;   94   scanf("%s",&quit_wish);
	.line	94
	leal	-37(%ebp),%edi
	pushl	%edi
	pushl	$_$36
	call	_scanf
	addl	$8,%esp
;   95   ptr_quit_wish = &quit_wish;              //&quit_wish;
	.line	95
	leal	-37(%ebp),%edi
	movl	%edi,-48(%ebp)
;   96 
;   97 if(quit_wish[0]==y_PRESSED)                // Good example of using the strinr element 0 and not a pointer
	.line	97
	cmpb	$121,-37(%ebp)
	jne	_$37
;   98  {
;   99   quit_flag=FALSE;
	.line	99
	movl	$0,-44(%ebp)
;  100   printf("y pressed\n");
	.line	100
	pushl	$_$39
	call	_printf
	addl	$4,%esp
;  101   return 0;
	.line	101
	movl	$0,%eax
	jmp	_$1
_$37:
;  102  }
;  103  else if(*ptr_quit_wish==n_PRESSED)
	.line	103
	movl	-48(%ebp),%edi
	cmpb	$110,(,%edi)
	jne	_$40
;  104   {
;  105   quit_flag=TRUE;
	.line	105
	movl	$1,-44(%ebp)
;  106   printf("n pressed\n");
	.line	106
	pushl	$_$42
	call	_printf
	addl	$4,%esp
_$40:
;  107   }
;  108  }
	.line	108
_$3:
;  109 while (quit_flag==TRUE);
	.line	109
	cmpl	$1,-44(%ebp)
	je	_$2
	movl	$0,%eax
_$1:
;  110 }
	.line	110
	popl	%edi
	popl	%ebx
	leave
	ret
_$46:
	.size	_main,_$46-_main
	.globl	_main
	.extern	_scanf
	.extern	_printf
	.data
_$42:
; "n pressed\n\x0"
	.byte	110,32,112,114,101,115,115,101,100,10,0
_$39:
; "y pressed\n\x0"
	.byte	121,32,112,114,101,115,115,101,100,10,0
_$36:
; "%s\x0"
	.byte	37,115,0
_$35:
; " Do you wish to quit? Press 'y' for yes or 'n' for no\n\x0"
	.byte	32,68,111,32,121,111,117,32,119,105,115,104,32,116,111,32
	.byte	113,117,105,116,63,32,80,114,101,115,115,32,39,121,39
	.byte	32,102,111,114,32,121,101,115,32,111,114,32,39,110,39
	.byte	32,102,111,114,32,110,111,10,0
_$34:
; " The result : %3d\n\x0"
	.byte	32,84,104,101,32,114,101,115,117,108,116,32,58,32,37,51
	.byte	100,10,0
_$33:
; "\n\x0"
	.byte	10,0
_$12:
; "Enter number 5 : \x0"
	.byte	69,110,116,101,114,32,110,117,109,98,101,114,32,53,32,58
	.byte	32,0
_$11:
; "Enter number 4 : \x0"
	.byte	69,110,116,101,114,32,110,117,109,98,101,114,32,52,32,58
	.byte	32,0
_$10:
; "Enter number 3 : \x0"
	.byte	69,110,116,101,114,32,110,117,109,98,101,114,32,51,32,58
	.byte	32,0
_$9:
; "Enter number 2 : \x0"
	.byte	69,110,116,101,114,32,110,117,109,98,101,114,32,50,32,58
	.byte	32,0
_$8:
; "%i\x0"
	.byte	37,105,0
_$7:
; "Enter number 1 : \x0"
	.byte	69,110,116,101,114,32,110,117,109,98,101,114,32,49,32,58
	.byte	32,0
_$6:
; "  \n\x0"
	.byte	32,32,10,0
_$5:
; "This program will sort 5 numbers and put them in ascending order\n\x0"
	.byte	84,104,105,115,32,112,114,111,103,114,97,109,32,119,105,108
	.byte	108,32,115,111,114,116,32,53,32,110,117,109,98,101,114
	.byte	115,32,97,110,100,32,112,117,116,32,116,104,101,109,32
	.byte	105,110,32,97,115,99,101,110,100,105,110,103,32,111,114
	.byte	100,101,114,10,0
