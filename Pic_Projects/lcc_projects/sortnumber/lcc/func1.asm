	.file	"c:\lcc_projects\sortnumber\func1.h"
_$M0:
	.file	"c:\lcc_projects\sortnumber\func1.c"
	.text
	.file	"c:\lcc_projects\sortnumber\func1.c"
_$M1:
	.text
;    1                                      // Author T.Rode                              Date 26 FEB 2011
;    2 
;    3 #include "func1.h"
;    4 
;    5 int firstelement(int *a, int n);
;    6 
;    7 int firstelement(int *a, int n)
	.type	_firstelement,function
_firstelement:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$20,%esp
	movl	$5,%ecx
_$18:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$18
	pushl	%ebx
	pushl	%edi
;    8 {
	.line	8
;    9 int result;
;   10 int i;
;   11 int flag;
;   12 char temp;
;   13 char sort_array[MAX_LENGTH];
;   14 if(n<=0)
	.line	14
	cmpl	$0,12(%ebp)
	jg	_$2
;   15   {
;   16   result=0;
	.line	16
	movl	$0,-20(%ebp)
	jmp	_$3
_$2:
;   17   }
;   18  else
;   19    {
	.line	19
;   20    result=a[0];
	.line	20
	movl	8(%ebp),%edi
	movl	(,%edi),%edi
	movl	%edi,-20(%ebp)
;   21         flag=1;
	.line	21
	movl	$1,-16(%ebp)
	jmp	_$5
_$4:
;   22         while(flag==1)                            // I want to say count flag
;   23          {
;   24          flag=0;
	.line	24
	movl	$0,-16(%ebp)
;   25          for(i=0; i<MAX_LENGTH-1; i++)
	.line	25
	movl	$0,-4(%ebp)
_$7:
;   26           {
;   27            if(sort_array[i]>sort_array[i+1])                           // This will swap the elements so it will make the order ascend
	.line	27
	movl	-4(%ebp),%edi
	movb	-9(%ebp,%edi),%bl
	cmpb	-8(%ebp,%edi),%bl
	jle	_$11
;   28              {
;   29              temp = sort_array[i+1];
	.line	29
	movl	-4(%ebp),%edi
	movb	-8(%ebp,%edi),%bl
	movb	%bl,-10(%ebp)
;   30              sort_array[i+1] = sort_array[i];
	.line	30
	movl	-4(%ebp),%edi
	movb	-9(%ebp,%edi),%bl
	movb	%bl,-8(%ebp,%edi)
;   31              sort_array[i] = temp;
	.line	31
	movl	-4(%ebp),%edi
	movb	-10(%ebp),%bl
	movb	%bl,-9(%ebp,%edi)
;   32              flag=1;
	.line	32
	movl	$1,-16(%ebp)
_$11:
;   33              }
;   34           }
	.line	34
_$8:
	.line	25
	incl	-4(%ebp)
	cmpl	$4,-4(%ebp)
	jl	_$7
;   35          }
	.line	35
_$5:
	.line	22
	cmpl	$1,-16(%ebp)
	je	_$4
;   36 
;   37 
;   38 
;   39 
;   40 
;   41 
;   42    }
	.line	42
_$3:
;   43 return result;
	.line	43
	movl	-20(%ebp),%eax
_$1:
;   44 }
	.line	44
	popl	%edi
	popl	%ebx
	leave
	ret
_$19:
	.size	_firstelement,_$19-_firstelement
	.globl	_firstelement
