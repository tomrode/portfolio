/*********************************************************************************************************************************************************
**                                                              Date Jan 2011, Author T.Rode
** Description:   This little program simply prompts you to take in a string in the scanf() function, with a maximum length displayed,
**                It also displays the and date. This can be terminated with character 'q' and this execute 10 times due to #define ASK_TIMES 10
**                Only one array was needed to perform this not two from before reverse_string[MAX_LENGTH]
**
**********************************************************************************************************************************************************/
#include<time.h>
#include<stdio.h>
#include<string.h>
#define MAX_LENGTH     10
#define ASK_TIMES      10                                                         // Run reverse string this many times
#define q_PRESSED      'q'                                                 //A=0x41,esc=0x1b lower case q=0x71
#define TRUE           1
#define FALSE          0
void main(void)
{

  char string[MAX_LENGTH];
  char temp;
  char *ptr_quit;
  int rerun;
  int i=0;
  int j=MAX_LENGTH-1;
  int q_pressed=q_PRESSED;
  int quit_flag;


for (rerun=0;rerun<ASK_TIMES;rerun++)                                                               // Initialize rerun and execute it ASK_TIMES times.

 {
     time_t ltime;
	 time(&ltime);
     printf("                     The Date and Time mow is %s\n",ctime(&ltime));                           // Capture and display the current time.


  printf("\n");
  printf(" Enter the string to be reversed no bigger than (%d) characters:\n", MAX_LENGTH);
  printf(" Or Enter 'q' to quit:\n");
  printf("\n");
  scanf("%s",string);                                                                                //10s to limit the string input length
  printf("  String Count Initially = %d, here is the string = %s\n",strlen(string),string);
  ptr_quit=&string[0];

 // printf("  Temp Count Initially = %d, here is the temp string = %s\n",strlen(temp),temp);
    printf("                                                            \n");

if(*ptr_quit==q_pressed)
	{
	 quit_flag=TRUE;
    }
   if ((*ptr_quit==q_pressed)&&(strlen(string)<2))
       {
		quit_flag=TRUE;
	   }
		else
		  {
           quit_flag=FALSE;
		  }
                                                                                                   // This is the entry point for executing if quit is wanted or not
if(quit_flag==TRUE)
{
	printf("q was pressed and ready to quit\n");
	rerun=ASK_TIMES;
}

else if(quit_flag==FALSE) //(*ptr_quit!=q_pressed)
 {
  if(strlen(string)<MAX_LENGTH)
    {
       string[MAX_LENGTH-1]=0x00;
    if(strlen(string)==0x00)
       {
        printf("This is null");
        printf("\nprint something else\n");
       }
     else
     {
      while((string[i]!=0)&&(i<=MAX_LENGTH))
           {
           i++;

           }
           printf(" This is how many characters in string: %d\n",strlen(string));
           printf(" This is the input string: %s\n",string);
           printf("                                                            \n");
             j = strlen(string) - 1;
             for(i=0;i<=j;i++)        //i<=j;i++)                                           // for(j=strlen(string);j>=1;j--)//for(j=strlen(string)-1;j>=0;j--)
                {
                 temp=string[i];      // c=b
                 string[i]=string[j]; // b=a
                 string[j]=temp;      // a=c
                 j--;
                 }

                printf(" Here is string that was entered in reverse: %s\n",string);
				printf("                                              \n");
           }
    }
       else
          {
          printf("Too Big\n");
          printf("MAX_LENGTH is:(%d)\n",MAX_LENGTH);
          }
   }
 }


}
