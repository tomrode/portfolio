	.file	"c:\lcc\include\time.h"
_$M0:
	.file	"c:\lcc_projects\revstring\main.c"
	.text
	.file	"c:\lcc\include\safelib.h"
_$M1:
	.file	"c:\lcc\include\time.h"
_$M2:
	.file	"c:\lcc\include\stdio.h"
_$M3:
	.file	"c:\lcc\include\string.h"
_$M4:
	.file	"c:\lcc_projects\revstring\main.c"
_$M5:
	.text
;    1 /*********************************************************************************************************************************************************
;    2 **                                                              Date Jan 2011, Author T.Rode
;    3 ** Description:   This little program simply prompts you to take in a string in the scanf() function, with a maximum length displayed,
;    4 **                It also displays the and date. This can be terminated with character 'q' and this execute 10 times due to #define ASK_TIMES 10
;    5 **                Only one array was needed to perform this not two from before reverse_string[MAX_LENGTH]
;    6 **
;    7 **********************************************************************************************************************************************************/
;    8 #include<time.h>
;    9 #include<stdio.h>
;   10 #include<string.h>
;   11 #define MAX_LENGTH     10
;   12 #define ASK_TIMES      10                                                         // Run reverse string this many times
;   13 #define q_PRESSED      'q'                                                 //A=0x41,esc=0x1b lower case q=0x71
;   14 #define TRUE           1
;   15 #define FALSE          0
;   16 void main(void)
	.type	_main,function
_main:
	pushl	%ebp
	movl	%esp,%ebp
	subl	$44,%esp
	movl	$11,%ecx
_$51:
	decl	%ecx
	movl	$0xfffa5a5a,(%esp,%ecx,4)
	jne	_$51
	pushl	%ebx
	pushl	%esi
	pushl	%edi
;   17 {
	.line	17
;   18 
;   19   char string[MAX_LENGTH];
;   20   char temp;
;   21   char *ptr_quit;
;   22   int rerun;
;   23   int i=0;
	.line	23
	movl	$0,-16(%ebp)
;   24   int j=MAX_LENGTH-1;
	.line	24
	movl	$9,-28(%ebp)
;   25   int q_pressed=q_PRESSED;
	.line	25
	movl	$113,-36(%ebp)
;   26   int quit_flag;
;   27 
;   28 
;   29 for (rerun=0;rerun<ASK_TIMES;rerun++)                                                               // Initialize rerun and execute it ASK_TIMES times.
	.line	29
	movl	$0,-32(%ebp)
_$2:
;   30 
;   31  {
;   32      time_t ltime;
;   33 	 time(&ltime);
	.line	33
	leal	-44(%ebp),%edi
	pushl	%edi
	call	_time
	addl	$4,%esp
;   34      printf("                     The Date and Time mow is %s\n",ctime(&ltime));                           // Capture and display the current time.
	.line	34
	leal	-44(%ebp),%edi
	pushl	%edi
	call	_ctime
	addl	$4,%esp
	movl	%eax,%edi
	pushl	%edi
	pushl	$_$6
	call	_printf
	addl	$8,%esp
;   35 
;   36 
;   37   printf("\n");
	.line	37
	pushl	$_$7
	call	_printf
	addl	$4,%esp
;   38   printf(" Enter the string to be reversed no bigger than (%d) characters:\n", MAX_LENGTH);
	.line	38
	pushl	$10
	pushl	$_$8
	call	_printf
	addl	$8,%esp
;   39   printf(" Or Enter 'q' to quit:\n");
	.line	39
	pushl	$_$9
	call	_printf
	addl	$4,%esp
;   40   printf("\n");
	.line	40
	pushl	$_$7
	call	_printf
	addl	$4,%esp
;   41   scanf("%s",string);                                                                                //10s to limit the string input length
	.line	41
	leal	-10(%ebp),%edi
	pushl	%edi
	pushl	$_$10
	call	_scanf
	addl	$8,%esp
;   42   printf("  String Count Initially = %d, here is the string = %s\n",strlen(string),string);
	.line	42
	leal	-10(%ebp),%edi
	pushl	%edi
	call	_strlen
	addl	$4,%esp
	movl	%eax,%edi
	leal	-10(%ebp),%esi
	pushl	%esi
	pushl	%edi
	pushl	$_$11
	call	_printf
	addl	$12,%esp
;   43   ptr_quit=&string[0];
	.line	43
	leal	-10(%ebp),%edi
	movl	%edi,-20(%ebp)
;   44 
;   45  // printf("  Temp Count Initially = %d, here is the temp string = %s\n",strlen(temp),temp);
;   46     printf("                                                            \n");
	.line	46
	pushl	$_$12
	call	_printf
	addl	$4,%esp
;   47 
;   48 if(*ptr_quit==q_pressed)
	.line	48
	movl	-20(%ebp),%edi
	movsbl	(,%edi),%ebx
	cmpl	-36(%ebp),%ebx
	jne	_$13
;   49 	{
;   50 	 quit_flag=TRUE;
	.line	50
	movl	$1,-24(%ebp)
_$13:
;   51     }
;   52    if ((*ptr_quit==q_pressed)&&(strlen(string)<2))
	.line	52
	movl	-20(%ebp),%edi
	movsbl	(,%edi),%ebx
	cmpl	-36(%ebp),%ebx
	jne	_$15
	leal	-10(%ebp),%edi
	pushl	%edi
	call	_strlen
	addl	$4,%esp
	cmpl	$2,%eax
	jae	_$15
;   53        {
;   54 		quit_flag=TRUE;
	.line	54
	movl	$1,-24(%ebp)
	jmp	_$16
_$15:
;   55 	   }
;   56 		else
;   57 		  {
	.line	57
;   58            quit_flag=FALSE;
	.line	58
	movl	$0,-24(%ebp)
_$16:
;   59 		  }
;   60                                                                                                    // This is the entry point for executing if quit is wanted or not
;   61 if(quit_flag==TRUE)
	.line	61
	cmpl	$1,-24(%ebp)
	jne	_$17
;   62 {
;   63 	printf("q was pressed and ready to quit\n");
	.line	63
	pushl	$_$19
	call	_printf
	addl	$4,%esp
;   64 	rerun=ASK_TIMES;
	.line	64
	movl	$10,-32(%ebp)
	jmp	_$18
_$17:
;   65 }
;   66 
;   67 else if(quit_flag==FALSE) //(*ptr_quit!=q_pressed)
	.line	67
	cmpl	$0,-24(%ebp)
	jne	_$20
;   68  {
;   69   if(strlen(string)<MAX_LENGTH)
	.line	69
	leal	-10(%ebp),%edi
	pushl	%edi
	call	_strlen
	addl	$4,%esp
	cmpl	$10,%eax
	jae	_$22
;   70     {
;   71        string[MAX_LENGTH-1]=0x00;
	.line	71
	movb	$0,-1(%ebp)
;   72     if(strlen(string)==0x00)
	.line	72
	leal	-10(%ebp),%edi
	pushl	%edi
	call	_strlen
	addl	$4,%esp
	cmpl	$0,%eax
	jne	_$30
;   73        {
;   74         printf("This is null");
	.line	74
	pushl	$_$27
	call	_printf
	addl	$4,%esp
;   75         printf("\nprint something else\n");
	.line	75
	pushl	$_$28
	call	_printf
	addl	$4,%esp
	jmp	_$23
;   76        }
;   77      else
;   78      {
	.line	78
_$29:
;   79       while((string[i]!=0)&&(i<=MAX_LENGTH))
;   80            {
;   81            i++;
	.line	81
	incl	-16(%ebp)
_$30:
	.line	79
	movl	-16(%ebp),%edi
	cmpb	$0,-10(%ebp,%edi)
	je	_$32
	cmpl	$10,%edi
	jle	_$29
_$32:
;   82 
;   83            }
;   84            printf(" This is how many characters in string: %d\n",strlen(string));
	.line	84
	leal	-10(%ebp),%edi
	pushl	%edi
	call	_strlen
	addl	$4,%esp
	movl	%eax,%edi
	pushl	%edi
	pushl	$_$33
	call	_printf
	addl	$8,%esp
;   85            printf(" This is the input string: %s\n",string);
	.line	85
	leal	-10(%ebp),%edi
	pushl	%edi
	pushl	$_$34
	call	_printf
	addl	$8,%esp
;   86            printf("                                                            \n");
	.line	86
	pushl	$_$12
	call	_printf
	addl	$4,%esp
;   87              j = strlen(string) - 1;
	.line	87
	leal	-10(%ebp),%edi
	pushl	%edi
	call	_strlen
	addl	$4,%esp
	movl	%eax,%edi
	subl	$1,%edi
	movl	%edi,-28(%ebp)
;   88              for(i=0;i<=j;i++)        //i<=j;i++)                                           // for(j=strlen(string);j>=1;j--)//for(j=strlen(string)-1;j>=0;j--)
	.line	88
	movl	$0,-16(%ebp)
	jmp	_$38
_$35:
;   89                 {
;   90                  temp=string[i];      // c=b
	.line	90
	movl	-16(%ebp),%edi
	movb	-10(%ebp,%edi),%bl
	movb	%bl,-37(%ebp)
;   91                  string[i]=string[j]; // b=a
	.line	91
	movl	-16(%ebp),%edi
	movl	-28(%ebp),%esi
	movb	-10(%ebp,%esi),%bl
	movb	%bl,-10(%ebp,%edi)
;   92                  string[j]=temp;      // a=c
	.line	92
	movl	-28(%ebp),%edi
	movb	-37(%ebp),%bl
	movb	%bl,-10(%ebp,%edi)
;   93                  j--;
	.line	93
	decl	-28(%ebp)
_$36:
	.line	88
	incl	-16(%ebp)
_$38:
	movl	-28(%ebp),%edi
	cmpl	%edi,-16(%ebp)
	jle	_$35
;   94                  }
;   95 
;   96                 printf(" Here is string that was entered in reverse: %s\n",string);
	.line	96
	leal	-10(%ebp),%edi
	pushl	%edi
	pushl	$_$39
	call	_printf
	addl	$8,%esp
;   97 				printf("                                              \n");
	.line	97
	pushl	$_$40
	call	_printf
	addl	$4,%esp
;   98            }
;   99     }
	.line	99
	jmp	_$23
_$22:
;  100        else
;  101           {
	.line	101
;  102           printf("Too Big\n");
	.line	102
	pushl	$_$41
	call	_printf
	addl	$4,%esp
;  103           printf("MAX_LENGTH is:(%d)\n",MAX_LENGTH);
	.line	103
	pushl	$10
	pushl	$_$42
	call	_printf
	addl	$8,%esp
_$23:
;  104           }
;  105    }
	.line	105
_$20:
;  106  }
	.line	106
_$18:
_$3:
	.line	29
	incl	-32(%ebp)
	cmpl	$10,-32(%ebp)
	jl	_$2
;  107 
;  108 
;  109 }
	.line	109
_$1:
	popl	%edi
	popl	%esi
	popl	%ebx
	leave
	ret
_$52:
	.size	_main,_$52-_main
	.globl	_main
	.extern	_strlen
	.extern	_scanf
	.extern	_printf
	.extern	_ctime
	.extern	_time
	.data
_$42:
; "MAX_LENGTH is:(%d)\n\x0"
	.byte	77,65,88,95,76,69,78,71,84,72,32,105,115,58,40,37
	.byte	100,41,10,0
_$41:
; "Too Big\n\x0"
	.byte	84,111,111,32,66,105,103,10,0
_$40:
; "                                              \n\x0"
	.byte	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.byte	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.byte	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.byte	10,0
_$39:
; " Here is string that was entered in reverse: %s\n\x0"
	.byte	32,72,101,114,101,32,105,115,32,115,116,114,105,110,103,32
	.byte	116,104,97,116,32,119,97,115,32,101,110,116,101,114,101
	.byte	100,32,105,110,32,114,101,118,101,114,115,101,58,32,37
	.byte	115,10,0
_$34:
; " This is the input string: %s\n\x0"
	.byte	32,84,104,105,115,32,105,115,32,116,104,101,32,105,110,112
	.byte	117,116,32,115,116,114,105,110,103,58,32,37,115,10,0
_$33:
; " This is how many characters in string: %d\n\x0"
	.byte	32,84,104,105,115,32,105,115,32,104,111,119,32,109,97,110
	.byte	121,32,99,104,97,114,97,99,116,101,114,115,32,105,110
	.byte	32,115,116,114,105,110,103,58,32,37,100,10,0
_$28:
; "\nprint something else\n\x0"
	.byte	10,112,114,105,110,116,32,115,111,109,101,116,104,105,110,103
	.byte	32,101,108,115,101,10,0
_$27:
; "This is null\x0"
	.byte	84,104,105,115,32,105,115,32,110,117,108,108,0
_$19:
; "q was pressed and ready to quit\n\x0"
	.byte	113,32,119,97,115,32,112,114,101,115,115,101,100,32,97,110
	.byte	100,32,114,101,97,100,121,32,116,111,32,113,117,105,116
	.byte	10,0
_$12:
; "                                                            \n\x0"
	.byte	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.byte	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.byte	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.byte	32,32,32,32,32,32,32,32,32,32,32,32,32,32,10
	.byte	0
_$11:
; "  String Count Initially = %d, here is the string = %s\n\x0"
	.byte	32,32,83,116,114,105,110,103,32,67,111,117,110,116,32,73
	.byte	110,105,116,105,97,108,108,121,32,61,32,37,100,44,32
	.byte	104,101,114,101,32,105,115,32,116,104,101,32,115,116,114
	.byte	105,110,103,32,61,32,37,115,10,0
_$10:
; "%s\x0"
	.byte	37,115,0
_$9:
; " Or Enter 'q' to quit:\n\x0"
	.byte	32,79,114,32,69,110,116,101,114,32,39,113,39,32,116,111
	.byte	32,113,117,105,116,58,10,0
_$8:
; " Enter the string to be reversed no bigger than (%d) characters:\n\x0"
	.byte	32,69,110,116,101,114,32,116,104,101,32,115,116,114,105,110
	.byte	103,32,116,111,32,98,101,32,114,101,118,101,114,115,101
	.byte	100,32,110,111,32,98,105,103,103,101,114,32,116,104,97
	.byte	110,32,40,37,100,41,32,99,104,97,114,97,99,116,101
	.byte	114,115,58,10,0
_$7:
; "\n\x0"
	.byte	10,0
_$6:
; "                     The Date and Time mow is %s\n\x0"
	.byte	32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
	.byte	32,32,32,32,32,84,104,101,32,68,97,116,101,32,97
	.byte	110,100,32,84,105,109,101,32,109,111,119,32,105,115,32
	.byte	37,115,10,0
