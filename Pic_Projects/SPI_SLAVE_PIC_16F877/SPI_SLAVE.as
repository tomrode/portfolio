opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 19 "C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 19 "C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FFF & 0x3FFD ;#
# 20 "C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 20 "C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_init
	FNCALL	_main,_spi_init
	FNCALL	_main,_full_step_state_machine
	FNCALL	_full_step_state_machine,_linear_motor_state_machine
	FNCALL	_full_step_state_machine,_get_timer
	FNCALL	_full_step_state_machine,_timer_set
	FNROOT	_main
	FNCALL	_interrupt_handler,_timer_isr
	FNCALL	_interrupt_handler,_spi_isr
	FNCALL	intlevel1,_interrupt_handler
	global	intlevel1
	FNROOT	intlevel1
	global	_motor_speed_array
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\spi_func.c"
	line	5
_motor_speed_array:
	retlw	0Ah
	retlw	0

	retlw	0Bh
	retlw	0

	retlw	0Ch
	retlw	0

	retlw	0Dh
	retlw	0

	retlw	0Fh
	retlw	0

	retlw	011h
	retlw	0

	retlw	013h
	retlw	0

	retlw	017h
	retlw	0

	retlw	01Ch
	retlw	0

	retlw	024h
	retlw	0

	retlw	032h
	retlw	0

	retlw	053h
	retlw	0

	retlw	0FAh
	retlw	0

	retlw	0A1h
	retlw	01h

	retlw	071h
	retlw	02h

	retlw	088h
	retlw	013h

	global	_halfstep_look_up_array
psect	strings
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\stepper_state_machine_half_full_time_potchange.c"
	line	33
_halfstep_look_up_array:
	retlw	01h
	retlw	03h
	retlw	02h
	retlw	06h
	retlw	04h
	retlw	0Ch
	retlw	08h
	retlw	09h
	global	_look_up_array
psect	strings
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\stepper_state_machine_half_full_time_potchange.c"
	line	26
_look_up_array:
	retlw	03h
	retlw	06h
	retlw	0Ch
	retlw	09h
	global	_motor_speed_array
	global	_halfstep_look_up_array
	global	_look_up_array
	global	_timer_array
	global	full_step_state_machine@adc_result_state_machine
	global	_E_HALFSTEP_FUNC
	global	_step_func_output
	global	_step_func_temp
	global	full_step_state_machine@index
	global	full_step_state_machine@step_func_state
	global	_E_STEP_FUNC
	global	_spi_isr_flag
	global	_tmr1_isr_counter
	global	_toms_variable
	global	half_step_state_machine@half_index
	global	half_step_state_machine@halfstep_func_state
	global	_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_watch_dog_count:
       ds      2

	global	full_step_state_machine@current_pot_val
full_step_state_machine@current_pot_val:
       ds      2

	global	full_step_state_machine@old_pot_val
full_step_state_machine@old_pot_val:
       ds      2

	global	_INTCON
_INTCON	set	11
	global	_PORTD
_PORTD	set	8
	global	_SSPBUF
_SSPBUF	set	19
	global	_SSPCON
_SSPCON	set	20
	global	_T1CON
_T1CON	set	16
	global	_TMR1H
_TMR1H	set	15
	global	_TMR1L
_TMR1L	set	14
	global	_GIE
_GIE	set	95
	global	_PEIE
_PEIE	set	94
	global	_RA5
_RA5	set	45
	global	_RD4
_RD4	set	68
	global	_RD7
_RD7	set	71
	global	_RE0
_RE0	set	72
	global	_SSPIF
_SSPIF	set	99
	global	_SSPOV
_SSPOV	set	166
	global	_TMR1IF
_TMR1IF	set	96
	global	_TO
_TO	set	28
	global	_OPTION
_OPTION	set	129
	global	_OSCCON
_OSCCON	set	143
	global	_PIE1
_PIE1	set	140
	global	_SSPSTAT
_SSPSTAT	set	148
	global	_TRISA
_TRISA	set	133
	global	_TRISC
_TRISC	set	135
	global	_TRISD
_TRISD	set	136
	global	_TRISE
_TRISE	set	137
	global	_HTS
_HTS	set	1146
	global	_SSPIE
_SSPIE	set	1123
	global	_TMR1IE
_TMR1IE	set	1120
	global	_ANSEL
_ANSEL	set	392
	file	"SPI_SLAVE.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_E_STEP_FUNC:
       ds      1

_spi_isr_flag:
       ds      1

_tmr1_isr_counter:
       ds      1

_toms_variable:
       ds      1

half_step_state_machine@half_index:
       ds      1

half_step_state_machine@halfstep_func_state:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_timer_array:
       ds      4

full_step_state_machine@adc_result_state_machine:
       ds      2

_E_HALFSTEP_FUNC:
       ds      1

_step_func_output:
       ds      1

_step_func_temp:
       ds      1

full_step_state_machine@index:
       ds      1

full_step_state_machine@step_func_state:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
	clrf	((__pbssCOMMON)+2)&07Fh
	clrf	((__pbssCOMMON)+3)&07Fh
	clrf	((__pbssCOMMON)+4)&07Fh
	clrf	((__pbssCOMMON)+5)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
	clrf	((__pbssBANK0)+5)&07Fh
	clrf	((__pbssBANK0)+6)&07Fh
	clrf	((__pbssBANK0)+7)&07Fh
	clrf	((__pbssBANK0)+8)&07Fh
	clrf	((__pbssBANK0)+9)&07Fh
	clrf	((__pbssBANK0)+10)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init
?_init:	; 0 bytes @ 0x0
	global	?_spi_init
?_spi_init:	; 0 bytes @ 0x0
	global	?_full_step_state_machine
?_full_step_state_machine:	; 0 bytes @ 0x0
	global	?_timer_isr
?_timer_isr:	; 0 bytes @ 0x0
	global	??_timer_isr
??_timer_isr:	; 0 bytes @ 0x0
	global	?_spi_isr
?_spi_isr:	; 0 bytes @ 0x0
	global	??_spi_isr
??_spi_isr:	; 0 bytes @ 0x0
	global	?_interrupt_handler
?_interrupt_handler:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	ds	1
	global	timer_isr@i
timer_isr@i:	; 1 bytes @ 0x1
	ds	1
	global	??_interrupt_handler
??_interrupt_handler:	; 0 bytes @ 0x2
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_init
??_init:	; 0 bytes @ 0x0
	global	??_spi_init
??_spi_init:	; 0 bytes @ 0x0
	global	?_timer_set
?_timer_set:	; 0 bytes @ 0x0
	global	?_linear_motor_state_machine
?_linear_motor_state_machine:	; 2 bytes @ 0x0
	global	?_get_timer
?_get_timer:	; 2 bytes @ 0x0
	global	timer_set@value
timer_set@value:	; 2 bytes @ 0x0
	ds	2
	global	??_linear_motor_state_machine
??_linear_motor_state_machine:	; 0 bytes @ 0x2
	global	??_timer_set
??_timer_set:	; 0 bytes @ 0x2
	global	??_get_timer
??_get_timer:	; 0 bytes @ 0x2
	ds	1
	global	timer_set@index
timer_set@index:	; 1 bytes @ 0x3
	global	linear_motor_state_machine@index
linear_motor_state_machine@index:	; 1 bytes @ 0x3
	global	get_timer@result
get_timer@result:	; 2 bytes @ 0x3
	ds	2
	global	get_timer@index
get_timer@index:	; 1 bytes @ 0x5
	ds	1
	global	??_full_step_state_machine
??_full_step_state_machine:	; 0 bytes @ 0x6
	ds	1
	global	full_step_state_machine@pot_changed_flag
full_step_state_machine@pot_changed_flag:	; 1 bytes @ 0x7
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0x8
;;Data sizes: Strings 0, constant 44, data 0, bss 17, persistent 6 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6      12
;; BANK0           80      8      25
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_linear_motor_state_machine	unsigned short  size(1) Largest target is 0
;;
;; ?_get_timer	unsigned short  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in COMMON
;;
;;   _interrupt_handler->_timer_isr
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_full_step_state_machine
;;   _full_step_state_machine->_get_timer
;;
;; Critical Paths under _interrupt_handler in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _interrupt_handler in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 2     2      0     294
;;                               _init
;;                           _spi_init
;;            _full_step_state_machine
;; ---------------------------------------------------------------------------------
;; (1) _full_step_state_machine                              2     2      0     294
;;                                              6 BANK0      2     2      0
;;         _linear_motor_state_machine
;;                          _get_timer
;;                          _timer_set
;; ---------------------------------------------------------------------------------
;; (2) _linear_motor_state_machine                           4     2      2      34
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (2) _get_timer                                            6     4      2      99
;;                                              0 BANK0      6     4      2
;; ---------------------------------------------------------------------------------
;; (2) _timer_set                                            6     4      2      93
;;                                              0 BANK0      4     2      2
;; ---------------------------------------------------------------------------------
;; (1) _spi_init                                             0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _init                                                 0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (3) _interrupt_handler                                    4     4      0      90
;;                                              2 COMMON     4     4      0
;;                          _timer_isr
;;                            _spi_isr
;; ---------------------------------------------------------------------------------
;; (4) _spi_isr                                              1     1      0       0
;;                                              0 COMMON     1     1      0
;; ---------------------------------------------------------------------------------
;; (4) _timer_isr                                            2     2      0      90
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 4
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init
;;   _spi_init
;;   _full_step_state_machine
;;     _linear_motor_state_machine
;;     _get_timer
;;     _timer_set
;;
;; _interrupt_handler (ROOT)
;;   _timer_isr
;;   _spi_isr
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      6       C       1       85.7%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       6       2        0.0%
;;ABS                  0      0      25       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      8      19       5       31.3%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      2B      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 37 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  initial_in      1    0        unsigned char 
;;  send_back_ou    1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  2  844[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_init
;;		_spi_init
;;		_full_step_state_machine
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
	line	37
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 4
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	41
	
l6210:	
;main.c: 41: watch_dog_count=0;
	movlw	low(0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_watch_dog_count)
	movlw	high(0)
	movwf	((_watch_dog_count))+1
	line	48
	
l6212:	
;main.c: 42: uint8_t send_back_out;
;main.c: 43: uint8_t initial_in;
;main.c: 48: init();
	fcall	_init
	line	49
	
l6214:	
;main.c: 49: spi_init();
	fcall	_spi_init
	line	50
	
l6216:	
;main.c: 50: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	51
	
l6218:	
;main.c: 51: SSPIE = 1;
	bsf	(1123/8)^080h,(1123)&7
	line	52
	
l6220:	
;main.c: 52: PEIE = 1;
	bsf	(94/8),(94)&7
	line	53
	
l6222:	
;main.c: 53: (GIE = 1);
	bsf	(95/8),(95)&7
	line	54
	
l6224:	
;main.c: 54: full_step_state_machine();
	fcall	_full_step_state_machine
	line	60
;main.c: 60: while (HTS==0)
	goto	l845
	
l846:	
	line	62
;main.c: 61: {
	
l845:	
	line	60
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1146/8)^080h,(1146)&7
	goto	u3021
	goto	u3020
u3021:
	goto	l845
u3020:
	goto	l848
	
l847:	
	line	63
;main.c: 62: }
;main.c: 63: while(1)
	
l848:	
	line	65
;main.c: 64: {
;main.c: 65: if (TO==1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u3031
	goto	u3030
u3031:
	goto	l849
u3030:
	goto	l6230
	line	67
	
l6226:	
;main.c: 66: {
;main.c: 67: while (RA5==0)
	goto	l6230
	
l851:	
	line	69
	
l6228:	
;main.c: 68: {
;main.c: 69: PORTD = SSPBUF;
	movf	(19),w	;volatile
	movwf	(8)	;volatile
	goto	l6230
	line	71
	
l850:	
	line	67
	
l6230:	
	btfss	(45/8),(45)&7
	goto	u3041
	goto	u3040
u3041:
	goto	l6228
u3040:
	
l852:	
	line	72
# 72 "C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
clrwdt ;#
psect	maintext
	line	73
;main.c: 73: }
	goto	l848
	line	75
	
l849:	
	line	77
# 77 "C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
clrwdt ;#
psect	maintext
	goto	l848
	line	78
	
l853:	
	goto	l848
	line	79
	
l854:	
	line	63
	goto	l848
	
l855:	
	line	80
	
l856:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_full_step_state_machine
psect	text386,local,class=CODE,delta=2
global __ptext386
__ptext386:

;; *************** function _full_step_state_machine *****************
;; Defined at:
;;		line 79 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\stepper_state_machine_half_full_time_potchange.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  pot_changed_    1    7[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       2       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_linear_motor_state_machine
;;		_get_timer
;;		_timer_set
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text386
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\stepper_state_machine_half_full_time_potchange.c"
	line	79
	global	__size_of_full_step_state_machine
	__size_of_full_step_state_machine	equ	__end_of_full_step_state_machine-_full_step_state_machine
	
_full_step_state_machine:	
	opt	stack 4
; Regs used in _full_step_state_machine: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	87
	
l6132:	
;stepper_state_machine_half_full_time_potchange.c: 80: static uint8_t step_func_state = E_STEP_FUNC_DEFAULT;
;stepper_state_machine_half_full_time_potchange.c: 81: static uint8_t index;
;stepper_state_machine_half_full_time_potchange.c: 82: static uint16_t adc_result_state_machine;
;stepper_state_machine_half_full_time_potchange.c: 83: static uint16_t old_pot_val;
;stepper_state_machine_half_full_time_potchange.c: 84: static uint16_t current_pot_val;
;stepper_state_machine_half_full_time_potchange.c: 85: uint8_t pot_changed_flag;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(full_step_state_machine@current_pot_val+1),w
	clrf	(full_step_state_machine@old_pot_val+1)
	addwf	(full_step_state_machine@old_pot_val+1)
	movf	(full_step_state_machine@current_pot_val),w
	clrf	(full_step_state_machine@old_pot_val)
	addwf	(full_step_state_machine@old_pot_val)

	line	88
	
l6134:	
;stepper_state_machine_half_full_time_potchange.c: 88: current_pot_val = linear_motor_state_machine();
	fcall	_linear_motor_state_machine
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_linear_motor_state_machine)),w
	clrf	(full_step_state_machine@current_pot_val+1)
	addwf	(full_step_state_machine@current_pot_val+1)
	movf	(0+(?_linear_motor_state_machine)),w
	clrf	(full_step_state_machine@current_pot_val)
	addwf	(full_step_state_machine@current_pot_val)

	line	90
	
l6136:	
;stepper_state_machine_half_full_time_potchange.c: 90: if(current_pot_val == old_pot_val)
	movf	(full_step_state_machine@old_pot_val+1),w
	xorwf	(full_step_state_machine@current_pot_val+1),w
	skipz
	goto	u2915
	movf	(full_step_state_machine@old_pot_val),w
	xorwf	(full_step_state_machine@current_pot_val),w
u2915:

	skipz
	goto	u2911
	goto	u2910
u2911:
	goto	l6140
u2910:
	line	92
	
l6138:	
;stepper_state_machine_half_full_time_potchange.c: 91: {
;stepper_state_machine_half_full_time_potchange.c: 92: pot_changed_flag = FALSE;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(full_step_state_machine@pot_changed_flag)
	line	93
;stepper_state_machine_half_full_time_potchange.c: 93: }
	goto	l6142
	line	94
	
l3427:	
	line	96
	
l6140:	
;stepper_state_machine_half_full_time_potchange.c: 94: else
;stepper_state_machine_half_full_time_potchange.c: 95: {
;stepper_state_machine_half_full_time_potchange.c: 96: pot_changed_flag = TRUE;
	clrf	(full_step_state_machine@pot_changed_flag)
	bsf	status,0
	rlf	(full_step_state_machine@pot_changed_flag),f
	goto	l6142
	line	97
	
l3428:	
	line	98
	
l6142:	
;stepper_state_machine_half_full_time_potchange.c: 97: }
;stepper_state_machine_half_full_time_potchange.c: 98: adc_result_state_machine = current_pot_val;
	movf	(full_step_state_machine@current_pot_val+1),w
	clrf	(full_step_state_machine@adc_result_state_machine+1)
	addwf	(full_step_state_machine@adc_result_state_machine+1)
	movf	(full_step_state_machine@current_pot_val),w
	clrf	(full_step_state_machine@adc_result_state_machine)
	addwf	(full_step_state_machine@adc_result_state_machine)

	line	100
;stepper_state_machine_half_full_time_potchange.c: 100: switch (step_func_state)
	goto	l6206
	line	103
;stepper_state_machine_half_full_time_potchange.c: 101: {
;stepper_state_machine_half_full_time_potchange.c: 103: case E_STEP_FUNC_FORWARD:
	
l3430:	
	line	105
	
l6144:	
;stepper_state_machine_half_full_time_potchange.c: 105: if((get_timer(TIMER_1) == 0) || (pot_changed_flag == TRUE))
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_get_timer)),w
	iorwf	(0+(?_get_timer)),w
	skipnz
	goto	u2921
	goto	u2920
u2921:
	goto	l6148
u2920:
	
l6146:	
	movf	(full_step_state_machine@pot_changed_flag),w
	xorlw	01h
	skipz
	goto	u2931
	goto	u2930
u2931:
	goto	l6208
u2930:
	goto	l6148
	
l3433:	
	line	107
	
l6148:	
;stepper_state_machine_half_full_time_potchange.c: 106: {
;stepper_state_machine_half_full_time_potchange.c: 107: step_func_temp = look_up_array[index];
	movf	(full_step_state_machine@index),w
	addlw	low((_look_up_array-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_temp)
	line	108
	
l6150:	
;stepper_state_machine_half_full_time_potchange.c: 108: step_func_output = step_func_temp;
	movf	(_step_func_temp),w
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_output)
	line	109
	
l6152:	
;stepper_state_machine_half_full_time_potchange.c: 109: timer_set(TIMER_1,adc_result_state_machine);
	movf	(full_step_state_machine@adc_result_state_machine+1),w
	clrf	(?_timer_set+1)
	addwf	(?_timer_set+1)
	movf	(full_step_state_machine@adc_result_state_machine),w
	clrf	(?_timer_set)
	addwf	(?_timer_set)

	movlw	(0)
	fcall	_timer_set
	line	110
	
l6154:	
;stepper_state_machine_half_full_time_potchange.c: 110: index++;
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	addwf	(full_step_state_machine@index),f
	line	111
	
l6156:	
;stepper_state_machine_half_full_time_potchange.c: 111: step_func_state = E_STEP_FUNC_FORWARD;
	clrf	(full_step_state_machine@step_func_state)
	bsf	status,0
	rlf	(full_step_state_machine@step_func_state),f
	line	112
	
l6158:	
;stepper_state_machine_half_full_time_potchange.c: 112: if (RE0 == 1)
	btfss	(72/8),(72)&7
	goto	u2941
	goto	u2940
u2941:
	goto	l3434
u2940:
	line	114
	
l6160:	
;stepper_state_machine_half_full_time_potchange.c: 113: {
;stepper_state_machine_half_full_time_potchange.c: 114: step_func_state = E_STEP_FUNC_REVERSE;
	movlw	(02h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(full_step_state_machine@step_func_state)
	line	115
	
l3434:	
	line	116
;stepper_state_machine_half_full_time_potchange.c: 115: }
;stepper_state_machine_half_full_time_potchange.c: 116: if (index > 3)
	movlw	(04h)
	subwf	(full_step_state_machine@index),w
	skipc
	goto	u2951
	goto	u2950
u2951:
	goto	l6208
u2950:
	line	118
	
l6162:	
;stepper_state_machine_half_full_time_potchange.c: 117: {
;stepper_state_machine_half_full_time_potchange.c: 118: index = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(full_step_state_machine@index)
	line	119
	
l6164:	
;stepper_state_machine_half_full_time_potchange.c: 119: step_func_temp = look_up_array[index];
	movf	(full_step_state_machine@index),w
	addlw	low((_look_up_array-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_temp)
	line	120
;stepper_state_machine_half_full_time_potchange.c: 120: }
	goto	l6208
	line	121
	
l3435:	
	goto	l6208
	line	124
;stepper_state_machine_half_full_time_potchange.c: 121: else
;stepper_state_machine_half_full_time_potchange.c: 122: {
	
l3436:	
	line	125
;stepper_state_machine_half_full_time_potchange.c: 124: }
;stepper_state_machine_half_full_time_potchange.c: 125: }
	goto	l6208
	line	126
	
l3431:	
	goto	l6208
	line	129
;stepper_state_machine_half_full_time_potchange.c: 126: else
;stepper_state_machine_half_full_time_potchange.c: 127: {
	
l3437:	
	line	131
;stepper_state_machine_half_full_time_potchange.c: 129: }
;stepper_state_machine_half_full_time_potchange.c: 131: break;
	goto	l6208
	line	133
;stepper_state_machine_half_full_time_potchange.c: 133: case E_STEP_FUNC_REVERSE:
	
l3439:	
	line	135
	
l6166:	
;stepper_state_machine_half_full_time_potchange.c: 135: if((get_timer(TIMER_1) == 0) || (pot_changed_flag == TRUE))
	movlw	(0)
	fcall	_get_timer
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_get_timer)),w
	iorwf	(0+(?_get_timer)),w
	skipnz
	goto	u2961
	goto	u2960
u2961:
	goto	l6170
u2960:
	
l6168:	
	movf	(full_step_state_machine@pot_changed_flag),w
	xorlw	01h
	skipz
	goto	u2971
	goto	u2970
u2971:
	goto	l6208
u2970:
	goto	l6170
	
l3442:	
	line	137
	
l6170:	
;stepper_state_machine_half_full_time_potchange.c: 136: {
;stepper_state_machine_half_full_time_potchange.c: 137: step_func_temp = look_up_array[index];
	movf	(full_step_state_machine@index),w
	addlw	low((_look_up_array-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_temp)
	line	138
	
l6172:	
;stepper_state_machine_half_full_time_potchange.c: 138: step_func_output = step_func_temp;
	movf	(_step_func_temp),w
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_output)
	line	139
	
l6174:	
;stepper_state_machine_half_full_time_potchange.c: 139: timer_set(TIMER_1, adc_result_state_machine);
	movf	(full_step_state_machine@adc_result_state_machine+1),w
	clrf	(?_timer_set+1)
	addwf	(?_timer_set+1)
	movf	(full_step_state_machine@adc_result_state_machine),w
	clrf	(?_timer_set)
	addwf	(?_timer_set)

	movlw	(0)
	fcall	_timer_set
	line	140
	
l6176:	
;stepper_state_machine_half_full_time_potchange.c: 140: step_func_state = E_STEP_FUNC_REVERSE;
	movlw	(02h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(full_step_state_machine@step_func_state)
	line	141
	
l6178:	
;stepper_state_machine_half_full_time_potchange.c: 141: if (RE0 == 0)
	btfsc	(72/8),(72)&7
	goto	u2981
	goto	u2980
u2981:
	goto	l6182
u2980:
	line	143
	
l6180:	
;stepper_state_machine_half_full_time_potchange.c: 142: {
;stepper_state_machine_half_full_time_potchange.c: 143: step_func_state = E_STEP_FUNC_FORWARD;
	clrf	(full_step_state_machine@step_func_state)
	bsf	status,0
	rlf	(full_step_state_machine@step_func_state),f
	goto	l6182
	line	144
	
l3443:	
	line	145
	
l6182:	
;stepper_state_machine_half_full_time_potchange.c: 144: }
;stepper_state_machine_half_full_time_potchange.c: 145: if (index > 0)
	movf	(full_step_state_machine@index),w
	skipz
	goto	u2990
	goto	l6186
u2990:
	line	147
	
l6184:	
;stepper_state_machine_half_full_time_potchange.c: 146: {
;stepper_state_machine_half_full_time_potchange.c: 147: index--;
	movlw	low(01h)
	subwf	(full_step_state_machine@index),f
	line	148
;stepper_state_machine_half_full_time_potchange.c: 148: }
	goto	l6208
	line	149
	
l3444:	
	
l6186:	
;stepper_state_machine_half_full_time_potchange.c: 149: else if (index == 0)
	movf	(full_step_state_machine@index),f
	skipz
	goto	u3001
	goto	u3000
u3001:
	goto	l6208
u3000:
	line	151
	
l6188:	
;stepper_state_machine_half_full_time_potchange.c: 150: {
;stepper_state_machine_half_full_time_potchange.c: 151: index = 3;
	movlw	(03h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(full_step_state_machine@index)
	line	152
;stepper_state_machine_half_full_time_potchange.c: 152: }
	goto	l6208
	line	153
	
l3446:	
	goto	l6208
	line	156
;stepper_state_machine_half_full_time_potchange.c: 153: else
;stepper_state_machine_half_full_time_potchange.c: 154: {
	
l3447:	
	goto	l6208
	
l3445:	
	line	157
;stepper_state_machine_half_full_time_potchange.c: 156: }
;stepper_state_machine_half_full_time_potchange.c: 157: }
	goto	l6208
	line	158
	
l3440:	
	goto	l6208
	line	161
;stepper_state_machine_half_full_time_potchange.c: 158: else
;stepper_state_machine_half_full_time_potchange.c: 159: {
	
l3448:	
	line	163
;stepper_state_machine_half_full_time_potchange.c: 161: }
;stepper_state_machine_half_full_time_potchange.c: 163: break;
	goto	l6208
	line	165
;stepper_state_machine_half_full_time_potchange.c: 165: default:
	
l3449:	
	line	166
	
l6190:	
;stepper_state_machine_half_full_time_potchange.c: 166: index = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(full_step_state_machine@index)
	line	167
	
l6192:	
;stepper_state_machine_half_full_time_potchange.c: 167: step_func_output = 0xF0;
	movlw	(0F0h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(_step_func_output)
	line	168
	
l6194:	
;stepper_state_machine_half_full_time_potchange.c: 168: adc_result_state_machine = linear_motor_state_machine();
	fcall	_linear_motor_state_machine
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_linear_motor_state_machine)),w
	clrf	(full_step_state_machine@adc_result_state_machine+1)
	addwf	(full_step_state_machine@adc_result_state_machine+1)
	movf	(0+(?_linear_motor_state_machine)),w
	clrf	(full_step_state_machine@adc_result_state_machine)
	addwf	(full_step_state_machine@adc_result_state_machine)

	line	170
	
l6196:	
;stepper_state_machine_half_full_time_potchange.c: 170: timer_set(TIMER_1,adc_result_state_machine);
	movf	(full_step_state_machine@adc_result_state_machine+1),w
	clrf	(?_timer_set+1)
	addwf	(?_timer_set+1)
	movf	(full_step_state_machine@adc_result_state_machine),w
	clrf	(?_timer_set)
	addwf	(?_timer_set)

	movlw	(0)
	fcall	_timer_set
	line	171
	
l6198:	
;stepper_state_machine_half_full_time_potchange.c: 171: if (RE0 == 0)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(72/8),(72)&7
	goto	u3011
	goto	u3010
u3011:
	goto	l6202
u3010:
	line	173
	
l6200:	
;stepper_state_machine_half_full_time_potchange.c: 172: {
;stepper_state_machine_half_full_time_potchange.c: 173: step_func_state = E_STEP_FUNC_FORWARD;
	clrf	(full_step_state_machine@step_func_state)
	bsf	status,0
	rlf	(full_step_state_machine@step_func_state),f
	line	174
;stepper_state_machine_half_full_time_potchange.c: 174: }
	goto	l6208
	line	175
	
l3450:	
	line	177
	
l6202:	
;stepper_state_machine_half_full_time_potchange.c: 175: else
;stepper_state_machine_half_full_time_potchange.c: 176: {
;stepper_state_machine_half_full_time_potchange.c: 177: step_func_state = E_STEP_FUNC_REVERSE;
	movlw	(02h)
	movwf	(??_full_step_state_machine+0)+0
	movf	(??_full_step_state_machine+0)+0,w
	movwf	(full_step_state_machine@step_func_state)
	goto	l6208
	line	178
	
l3451:	
	line	179
;stepper_state_machine_half_full_time_potchange.c: 178: }
;stepper_state_machine_half_full_time_potchange.c: 179: break;
	goto	l6208
	line	180
	
l6204:	
;stepper_state_machine_half_full_time_potchange.c: 180: }
	goto	l6208
	line	100
	
l3429:	
	
l6206:	
	movf	(full_step_state_machine@step_func_state),w
	; Switch size 1, requested type "space"
; Number of cases is 2, Range of values is 1 to 2
; switch strategies available:
; Name         Bytes Cycles
; simple_byte     7     4 (average)
; direct_byte    28    22 (fixed)
;	Chosen strategy is simple_byte

	xorlw	1^0	; case 1
	skipnz
	goto	l6144
	xorlw	2^1	; case 2
	skipnz
	goto	l6166
	goto	l6190

	line	180
	
l3438:	
	line	183
	
l6208:	
;stepper_state_machine_half_full_time_potchange.c: 183: PORTD = step_func_output;
	movf	(_step_func_output),w
	movwf	(8)	;volatile
	line	184
	
l3452:	
	return
	opt stack 0
GLOBAL	__end_of_full_step_state_machine
	__end_of_full_step_state_machine:
;; =============== function _full_step_state_machine ends ============

	signat	_full_step_state_machine,88
	global	_linear_motor_state_machine
psect	text387,local,class=CODE,delta=2
global __ptext387
__ptext387:

;; *************** function _linear_motor_state_machine *****************
;; Defined at:
;;		line 38 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\spi_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_full_step_state_machine
;; This function uses a non-reentrant model
;;
psect	text387
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\spi_func.c"
	line	38
	global	__size_of_linear_motor_state_machine
	__size_of_linear_motor_state_machine	equ	__end_of_linear_motor_state_machine-_linear_motor_state_machine
	
_linear_motor_state_machine:	
	opt	stack 4
; Regs used in _linear_motor_state_machine: [wreg-fsr0h+status,2+status,0+pclath]
	line	41
	
l6054:	
;spi_func.c: 39: uint8_t index;
;spi_func.c: 41: if (spi_isr_flag==1)
	movf	(_spi_isr_flag),w	;volatile
	xorlw	01h
	skipz
	goto	u2821
	goto	u2820
u2821:
	goto	l6066
u2820:
	line	43
	
l6056:	
;spi_func.c: 42: {
;spi_func.c: 43: RD7=1;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bsf	(71/8),(71)&7
	line	44
;spi_func.c: 44: (GIE = 0);
	bcf	(95/8),(95)&7
	line	45
	
l6058:	
;spi_func.c: 45: spi_isr_flag=0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(_spi_isr_flag)	;volatile
	line	46
	
l6060:	
;spi_func.c: 46: index = SSPBUF & 0x0F;
	movf	(19),w
	andlw	0Fh
	movwf	(??_linear_motor_state_machine+0)+0
	movf	(??_linear_motor_state_machine+0)+0,w
	movwf	(linear_motor_state_machine@index)
	line	47
	
l6062:	
;spi_func.c: 47: (GIE = 1);
	bsf	(95/8),(95)&7
	line	48
	
l6064:	
;spi_func.c: 48: RD7=0;
	bcf	(71/8),(71)&7
	line	49
;spi_func.c: 49: }
	goto	l6066
	line	50
	
l4319:	
	goto	l6066
	line	52
;spi_func.c: 50: else
;spi_func.c: 51: {
	
l4320:	
	line	54
	
l6066:	
;spi_func.c: 52: }
;spi_func.c: 54: return motor_speed_array[index];
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(linear_motor_state_machine@index),w
	movwf	(??_linear_motor_state_machine+0)+0
	addwf	(??_linear_motor_state_machine+0)+0,w
	addlw	low((_motor_speed_array-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(?_linear_motor_state_machine)
	fcall	stringdir
	movwf	(?_linear_motor_state_machine+1)
	goto	l4321
	
l6068:	
	line	55
	
l4321:	
	return
	opt stack 0
GLOBAL	__end_of_linear_motor_state_machine
	__end_of_linear_motor_state_machine:
;; =============== function _linear_motor_state_machine ends ============

	signat	_linear_motor_state_machine,90
	global	_get_timer
psect	text388,local,class=CODE,delta=2
global __ptext388
__ptext388:

;; *************** function _get_timer *****************
;; Defined at:
;;		line 33 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  index           1    5[BANK0 ] unsigned char 
;;  result          2    3[BANK0 ] unsigned short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       3       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       6       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_full_step_state_machine
;;		_half_step_state_machine
;; This function uses a non-reentrant model
;;
psect	text388
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\timer.c"
	line	33
	global	__size_of_get_timer
	__size_of_get_timer	equ	__end_of_get_timer-_get_timer
	
_get_timer:	
	opt	stack 4
; Regs used in _get_timer: [wreg-fsr0h+status,2+status,0]
;get_timer@index stored from wreg
	line	36
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(get_timer@index)
	
l6040:	
;timer.c: 34: uint16_t result;
;timer.c: 36: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(get_timer@index),w
	skipnc
	goto	u2811
	goto	u2810
u2811:
	goto	l6048
u2810:
	line	38
	
l6042:	
;timer.c: 37: {
;timer.c: 38: (GIE = 0);
	bcf	(95/8),(95)&7
	line	39
	
l6044:	
;timer.c: 39: result = timer_array[index];
	movf	(get_timer@index),w
	movwf	(??_get_timer+0)+0
	addwf	(??_get_timer+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(get_timer@result)
	incf	fsr0,f
	movf	indf,w
	movwf	(get_timer@result+1)
	line	40
	
l6046:	
;timer.c: 40: (GIE = 1);
	bsf	(95/8),(95)&7
	line	41
;timer.c: 41: }
	goto	l6050
	line	42
	
l2546:	
	line	44
	
l6048:	
;timer.c: 42: else
;timer.c: 43: {
;timer.c: 44: result = 0;
	movlw	low(0)
	movwf	(get_timer@result)
	movlw	high(0)
	movwf	((get_timer@result))+1
	goto	l6050
	line	45
	
l2547:	
	line	47
	
l6050:	
;timer.c: 45: }
;timer.c: 47: return result;
	movf	(get_timer@result+1),w
	clrf	(?_get_timer+1)
	addwf	(?_get_timer+1)
	movf	(get_timer@result),w
	clrf	(?_get_timer)
	addwf	(?_get_timer)

	goto	l2548
	
l6052:	
	line	49
	
l2548:	
	return
	opt stack 0
GLOBAL	__end_of_get_timer
	__end_of_get_timer:
;; =============== function _get_timer ends ============

	signat	_get_timer,4218
	global	_timer_set
psect	text389,local,class=CODE,delta=2
global __ptext389
__ptext389:

;; *************** function _timer_set *****************
;; Defined at:
;;		line 18 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\timer.c"
;; Parameters:    Size  Location     Type
;;  index           1    wreg     unsigned char 
;;  value           2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  index           1    3[BANK0 ] unsigned char 
;;  array_conten    2    0        unsigned short 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0       1       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0       4       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_full_step_state_machine
;;		_half_step_state_machine
;; This function uses a non-reentrant model
;;
psect	text389
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\timer.c"
	line	18
	global	__size_of_timer_set
	__size_of_timer_set	equ	__end_of_timer_set-_timer_set
	
_timer_set:	
	opt	stack 4
; Regs used in _timer_set: [wreg-fsr0h+status,2+status,0]
;timer_set@index stored from wreg
	line	20
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(timer_set@index)
	
l6032:	
;timer.c: 19: uint16_t array_contents;
;timer.c: 20: if (index < TIMER_MAX)
	movlw	(02h)
	subwf	(timer_set@index),w
	skipnc
	goto	u2801
	goto	u2800
u2801:
	goto	l2543
u2800:
	line	22
	
l6034:	
;timer.c: 21: {
;timer.c: 22: (GIE = 0);
	bcf	(95/8),(95)&7
	line	23
	
l6036:	
;timer.c: 23: timer_array[index] = value;
	movf	(timer_set@index),w
	movwf	(??_timer_set+0)+0
	addwf	(??_timer_set+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movf	(timer_set@value),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(timer_set@value+1),w
	movwf	indf
	line	24
	
l6038:	
;timer.c: 24: (GIE = 1);
	bsf	(95/8),(95)&7
	line	25
;timer.c: 25: }
	goto	l2543
	line	26
	
l2541:	
	goto	l2543
	line	28
;timer.c: 26: else
;timer.c: 27: {
	
l2542:	
	line	29
	
l2543:	
	return
	opt stack 0
GLOBAL	__end_of_timer_set
	__end_of_timer_set:
;; =============== function _timer_set ends ============

	signat	_timer_set,8312
	global	_spi_init
psect	text390,local,class=CODE,delta=2
global __ptext390
__ptext390:

;; *************** function _spi_init *****************
;; Defined at:
;;		line 72 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\spi_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text390
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\spi_func.c"
	line	72
	global	__size_of_spi_init
	__size_of_spi_init	equ	__end_of_spi_init-_spi_init
	
_spi_init:	
	opt	stack 5
; Regs used in _spi_init: [wreg+status,2]
	line	73
	
l6028:	
;spi_func.c: 73: SSPSTAT = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(148)^080h	;volatile
	line	74
	
l6030:	
;spi_func.c: 74: SSPCON = 0x24;
	movlw	(024h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(20)	;volatile
	line	75
	
l4329:	
	return
	opt stack 0
GLOBAL	__end_of_spi_init
	__end_of_spi_init:
;; =============== function _spi_init ends ============

	signat	_spi_init,88
	global	_init
psect	text391,local,class=CODE,delta=2
global __ptext391
__ptext391:

;; *************** function _init *****************
;; Defined at:
;;		line 84 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text391
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\main.c"
	line	84
	global	__size_of_init
	__size_of_init	equ	__end_of_init-_init
	
_init:	
	opt	stack 5
; Regs used in _init: [wreg+status,2]
	line	85
	
l6014:	
;main.c: 85: OPTION = 0x80;
	movlw	(080h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(129)^080h	;volatile
	line	86
;main.c: 86: OSCCON = 0x61;
	movlw	(061h)
	movwf	(143)^080h	;volatile
	line	87
;main.c: 87: TRISA = 0x20;
	movlw	(020h)
	movwf	(133)^080h	;volatile
	line	88
	
l6016:	
;main.c: 88: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	89
	
l6018:	
;main.c: 89: PORTD = 0x00;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(8)	;volatile
	line	90
;main.c: 90: TRISC = 0x18;
	movlw	(018h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(135)^080h	;volatile
	line	91
;main.c: 91: TRISE = 0x01;
	movlw	(01h)
	movwf	(137)^080h	;volatile
	line	92
	
l6020:	
;main.c: 92: ANSEL = 0x00;
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	clrf	(392)^0180h	;volatile
	line	93
	
l6022:	
;main.c: 93: PIE1 = 0x09;
	movlw	(09h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(140)^080h	;volatile
	line	94
	
l6024:	
;main.c: 94: INTCON = 0x40;
	movlw	(040h)
	movwf	(11)	;volatile
	line	95
	
l6026:	
;main.c: 95: T1CON = 0x35;
	movlw	(035h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(16)	;volatile
	line	96
	
l859:	
	return
	opt stack 0
GLOBAL	__end_of_init
	__end_of_init:
;; =============== function _init ends ============

	signat	_init,88
	global	_interrupt_handler
psect	text392,local,class=CODE,delta=2
global __ptext392
__ptext392:

;; *************** function _interrupt_handler *****************
;; Defined at:
;;		line 6 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\interrupt.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          4       0       0       0       0
;;      Totals:         4       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_timer_isr
;;		_spi_isr
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text392
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\interrupt.c"
	line	6
	global	__size_of_interrupt_handler
	__size_of_interrupt_handler	equ	__end_of_interrupt_handler-_interrupt_handler
	
_interrupt_handler:	
	opt	stack 4
; Regs used in _interrupt_handler: [wreg-fsr0h+status,2+status,0+pclath+cstack]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_interrupt_handler+0)
	movf	fsr0,w
	movwf	(??_interrupt_handler+1)
	movf	pclath,w
	movwf	(??_interrupt_handler+2)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_interrupt_handler+3)
	ljmp	_interrupt_handler
psect	text392
	line	7
	
i1l5770:	
;interrupt.c: 7: timer_isr();
	fcall	_timer_isr
	line	8
	
i1l5772:	
;interrupt.c: 8: spi_isr();
	fcall	_spi_isr
	line	9
	
i1l1700:	
	movf	(??_interrupt_handler+3),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	btemp+1
	movf	(??_interrupt_handler+2),w
	movwf	pclath
	movf	(??_interrupt_handler+1),w
	movwf	fsr0
	movf	(??_interrupt_handler+0),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_interrupt_handler
	__end_of_interrupt_handler:
;; =============== function _interrupt_handler ends ============

	signat	_interrupt_handler,88
	global	_spi_isr
psect	text393,local,class=CODE,delta=2
global __ptext393
__ptext393:

;; *************** function _spi_isr *****************
;; Defined at:
;;		line 59 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\spi_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text393
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\spi_func.c"
	line	59
	global	__size_of_spi_isr
	__size_of_spi_isr	equ	__end_of_spi_isr-_spi_isr
	
_spi_isr:	
	opt	stack 4
; Regs used in _spi_isr: [wreg]
	line	60
	
i1l4746:	
;spi_func.c: 60: if ((SSPIF==1)&&(SSPIE==1))
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(99/8),(99)&7
	goto	u6_21
	goto	u6_20
u6_21:
	goto	i1l4325
u6_20:
	
i1l4748:	
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1123/8)^080h,(1123)&7
	goto	u7_21
	goto	u7_20
u7_21:
	goto	i1l4325
u7_20:
	line	62
	
i1l4750:	
;spi_func.c: 61: {
;spi_func.c: 62: SSPIF=0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(99/8),(99)&7
	line	63
	
i1l4752:	
;spi_func.c: 63: spi_isr_flag=1;
	movlw	(01h)
	movwf	(??_spi_isr+0)+0
	movf	(??_spi_isr+0)+0,w
	movwf	(_spi_isr_flag)	;volatile
	line	64
;spi_func.c: 64: }
	goto	i1l4325
	line	65
	
i1l4324:	
	line	67
;spi_func.c: 65: else
;spi_func.c: 66: {
	
i1l4325:	
	line	68
;spi_func.c: 67: }
;spi_func.c: 68: SSPOV = 0;
	bcf	status, 5	;RP0=0, select bank0
	bcf	(166/8),(166)&7
	line	69
	
i1l4326:	
	return
	opt stack 0
GLOBAL	__end_of_spi_isr
	__end_of_spi_isr:
;; =============== function _spi_isr ends ============

	signat	_spi_isr,88
	global	_timer_isr
psect	text394,local,class=CODE,delta=2
global __ptext394
__ptext394:

;; *************** function _timer_isr *****************
;; Defined at:
;;		line 54 in file "C:\pic_projects\SPI_SLAVE_PIC_16F877\timer.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_interrupt_handler
;; This function uses a non-reentrant model
;;
psect	text394
	file	"C:\pic_projects\SPI_SLAVE_PIC_16F877\timer.c"
	line	54
	global	__size_of_timer_isr
	__size_of_timer_isr	equ	__end_of_timer_isr-_timer_isr
	
_timer_isr:	
	opt	stack 4
; Regs used in _timer_isr: [wreg-fsr0h+status,2+status,0]
	line	56
	
i1l5774:	
;timer.c: 55: uint8_t i;
;timer.c: 56: if((TMR1IE==1)&&(TMR1IF==1))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u248_21
	goto	u248_20
u248_21:
	goto	i1l2557
u248_20:
	
i1l5776:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u249_21
	goto	u249_20
u249_21:
	goto	i1l2557
u249_20:
	line	58
	
i1l5778:	
;timer.c: 57: {
;timer.c: 58: RD4 = 1;
	bsf	(68/8),(68)&7
	line	59
;timer.c: 59: TMR1IF = 0;
	bcf	(96/8),(96)&7
	line	60
	
i1l5780:	
;timer.c: 60: T1CON &= ~(0x01);
	movlw	(0FEh)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	andwf	(16),f	;volatile
	line	61
	
i1l5782:	
;timer.c: 61: TMR1L = 0x89;
	movlw	(089h)
	movwf	(14)	;volatile
	line	62
	
i1l5784:	
;timer.c: 62: TMR1H = 0xFF;
	movlw	(0FFh)
	movwf	(15)	;volatile
	line	63
	
i1l5786:	
;timer.c: 63: T1CON |= 0x01;
	bsf	(16)+(0/8),(0)&7	;volatile
	line	64
	
i1l5788:	
;timer.c: 64: RD4 = 0;
	bcf	(68/8),(68)&7
	line	65
;timer.c: 65: for (i = 0; i < TIMER_MAX; i++)
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(timer_isr@i)
	
i1l5790:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u250_21
	goto	u250_20
u250_21:
	goto	i1l5794
u250_20:
	goto	i1l2557
	
i1l5792:	
	goto	i1l2557
	line	66
	
i1l2552:	
	line	67
	
i1l5794:	
;timer.c: 66: {
;timer.c: 67: if (timer_array[i] != 0)
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	incf	fsr0,f
	iorwf	indf,w
	skipnz
	goto	u251_21
	goto	u251_20
u251_21:
	goto	i1l5798
u251_20:
	line	69
	
i1l5796:	
;timer.c: 68: {
;timer.c: 69: timer_array[i]--;
	movf	(timer_isr@i),w
	movwf	(??_timer_isr+0)+0
	addwf	(??_timer_isr+0)+0,w
	addlw	_timer_array&0ffh
	movwf	fsr0
	movlw	low(01h)
	subwf	indf,f
	incfsz	fsr0,f
	movlw	high(01h)
	skipc
	decf	indf,f
	subwf	indf,f
	decf	fsr0,f
	line	70
;timer.c: 70: }
	goto	i1l5798
	line	71
	
i1l2554:	
	goto	i1l5798
	line	73
;timer.c: 71: else
;timer.c: 72: {
	
i1l2555:	
	line	65
	
i1l5798:	
	movlw	(01h)
	movwf	(??_timer_isr+0)+0
	movf	(??_timer_isr+0)+0,w
	addwf	(timer_isr@i),f
	
i1l5800:	
	movlw	(02h)
	subwf	(timer_isr@i),w
	skipc
	goto	u252_21
	goto	u252_20
u252_21:
	goto	i1l5794
u252_20:
	goto	i1l2557
	
i1l2553:	
	line	76
;timer.c: 73: }
;timer.c: 74: }
;timer.c: 76: }
	goto	i1l2557
	line	77
	
i1l2551:	
	goto	i1l2557
	line	80
;timer.c: 77: else
;timer.c: 78: {
	
i1l2556:	
	line	81
	
i1l2557:	
	return
	opt stack 0
GLOBAL	__end_of_timer_isr
	__end_of_timer_isr:
;; =============== function _timer_isr ends ============

	signat	_timer_isr,88
psect	text395,local,class=CODE,delta=2
global __ptext395
__ptext395:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
