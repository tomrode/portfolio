#include "htc.h"
#include "typedefs.h"


/*Global Variables*/
extern  vuint8_t spi_isr_flag;


/*Function prototypes*/

void spi_isr(void);
uint16_t linear_motor_state_machine(void);
void spi_init(void);