/**************************************************************/          
/* 10 OCT 2011 using PIC16F877                                */ 
/* Setting up for SPI SLAVE,                                  */
/* 3 Nov 2011                                                 */ 
/* Master fixed with Shorter SS time 23 micro second          */
/* no more lock up.                                           */
/*                                                            */
/**************************************************************/
#include "htc.h"
#include "typedefs.h"
#include "timer.h"
#include "stepper_state_machine.h"
#include "spi_func.h"
/**********************************************************/
/* Device Configuration
/**********************************************************/
        
/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTEN & INTCLK);
__CONFIG(BORV40);       
      
       
 
/**********************************************************/
/* Function Prototypes 
/**********************************************************/

void init(void);     // SFR inits

/***********************************************************/
/* Variable Definition
/***********************************************************/
/*unsigned int watch_dog_count=0;*/

persistent int watch_dog_count;                    /* will remain with a reset  */
int main(void)
{
/************************************************************/
/* LOCAL MAIN VARIABLES                                     */
/************************************************************/
watch_dog_count=0;                    
uint8_t send_back_out;                              /*recieve a message to test*/
uint8_t initial_in;

/***********************************************************/
/* INITIALIZATIONS                                         */
/***********************************************************/
init();                                             /* Initialize SFR.*/
spi_init();                                         /* Initialize all spi.*/
TMR1IE = 1;                                         /* Timer 1 interrupt enable.*/
SSPIE = 1;                                          /* spi intrrupt enable.*/
PEIE = 1;                                           /* enable all peripheral interrupts.*/
ei();                                               /* enable all interrupts.*/
full_step_state_machine();                          /* set the state machine to default initially.*/

/************************************************************/
/*  MAIN LOOP OVERALL                                       */
/************************************************************/

while (HTS==0)
  {
  }                                                 /* wait until clock stable */
while(1)                   
 { 
  if (TO==1)                                       /*STATUS reg, bit 4 1=After power up,CLRWDT or sleep instruction 0=A WDT time-out occured */
     {                               
     while (RA5==0)                                /* the SS chip select pin see if its low*/
     {
     PORTD = SSPBUF;      
     //full_step_state_machine();                     /* set the state machine to default initially*/
     }     
     CLRWDT();                                      /* clear if WD timed out*/
     }
     
   else
      {
      CLRWDT();                                     /* Clear watchdog timer.*/
      } 
  }
 } 


 void init(void)
{
OPTION     = 0x80;                                 /*|PSA=Prescale to WDT|most prescale rate|*/
OSCCON     = 0x61;                                 /* 4 Mhz clock*/
TRISA      = 0x20;                                 /* Setup for SS pin as set TRISA pin 5 set hook a weak ground to pin RA5*/
TRISD      = 0x00;                                 /* Tris directions: 1=input, 0=output*/
PORTD      = 0x00;                                 /* port D all off */
TRISC      = 0x18;                                 /* |bit 7|bit 6|bit 5 SDO=0|bit 4 SS=1|bit 3 SCK slave mode =1|bit 2=0| bit 1=0 | bit 0=0 |   */
TRISE      = 0x01;                                 /* MAKE PORTE RE1 an input Pushbutton*/
ANSEL      = 0x00;                                 /* Make this a digital pin*/
PIE1       = 0x09;                                 /* TMR1IE = enabled and SSPIE = enabled INTERRUPTS*/
INTCON     = 0x40;                                 /* PEIE = enabled*/
T1CON      = 0x35;                                 /* T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled*/
}


