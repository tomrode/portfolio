#include "typedefs.h"
#include "htc.h"
#include "timer.h"
#include "spi_func.h
void interrupt interrupt_handler(void)
{
 timer_isr();
 spi_isr();
}
