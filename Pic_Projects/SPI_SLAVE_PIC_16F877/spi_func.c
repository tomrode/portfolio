#include "htc.h"
#include "spi_func.h"
#include "timer.h"

const uint16_t motor_speed_array[] =  
{                           /*      FROM EXCELL        */
  0x000A,                   /* index 0  speed 125 RPM  */
  0x000B,                   /* index 1  speed 115 RPM  */
  0x000C,                   /* index 2  speed 105 RPM  */
  0x000D,                   /* index 3  speed 95  RPM  */
  0x000F,                   /* index 4  speed 85  RPM  */
  0x0011,                   /* index 5  speed 75  RPM  */
  0x0013,                   /* index 6  speed 65  RPM  */                
  0x0017,                   /* index 7  speed 55  RPM  */
  0x001C,                   /* index 8  speed 45  RPM  */
  0x0024,                   /* index 9  speed 35  RPM  */
  0x0032,                   /* index 10 speed 25  RPM  */ 
  0x0053,                   /* index 11 speed 15  RPM  */
  0x00FA,                   /* index 12 speed 05  RPM  */
  0x01A1,                   /* index 13 speed 03  RPM  */
  0x0271,                   /* index 14 speed 02  RPM  */
  0x1388                    /* index 15 turn very slow */
};

vuint8_t spi_isr_flag = 0;  /* made this flag volatile*/

//////////////////////////////////////////////////////////////////////////////////////
/*    linear_motor_state_machine()
/*    Description: This function will return a 16 bit value from motor_speed_array[]
/*                Implemented a jitter test filter for varing adc counts. It will not pass 
/*                indexed value if count greater than 5.    
/*    INPUTS:     None
/*    OUTPUTS:    value from array for time 0-15.
/*
/////////////////////////////////////////////////////////////////////////////////////*/    

uint16_t linear_motor_state_machine(void)
{
uint8_t index;

 if (spi_isr_flag==1) 
 {
   RD7=1;                   /* Test for interrupt.*/    
   di();
   spi_isr_flag=0;          /* Clear out this SW flag initially.*/
   index = SSPBUF & 0x0F;   /* Mask off the buffer */
   ei();
   RD7=0; 
 }
 else
 {
 }

return motor_speed_array[index];              
}


void spi_isr(void)
{ 
  if ((SSPIF==1)&&(SSPIE==1))
  {
    SSPIF=0;                    /* Clear the hardware int flag*/  
    spi_isr_flag=1;             /* Set the internal SW flag*/ 
  }
  else 
  {
  }
  SSPOV = 0;                   /*Force clear it*/
}

void spi_init(void)
{
SSPSTAT    = 0x00;             /* |SMP=cleared in slave mode|CKE = Xmit occurs Idle to active|everything else I2C stuff.*/
SSPCON     = 0x24;             /* |WCOL = 0|SSPOV = 0 slave mode only|SSPEN = 1 enable SCK,SD),SDI,SS as serial pins |CKP|SSPM3=0|SSPM2=1|SSPM1=0|SSPM0=0|*/
}
