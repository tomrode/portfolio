#include "htc.h"
#include "typedefs.h"

/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);
/* STRUCTURES */
 typedef struct
        {
        uint8_t c1;
        uint8_t c2;
        uint8_t c3;
        uint8_t c4;
        uint8_t c5;
        uint8_t c6;
        uint8_t c7;
        uint8_t c8;
        uint8_t c9;
        }COMPLEX; 
/* Function prototypes*/
void sfreg(void);                                   // Initializr  the hard ware
int FindMax (int *arr, int size);
/* non writeable ram variable location*/
persistent int persistent_watch_dog_count;

/* Volatile variable*/
vuint8_t tmr1_counter;      // volatile isr variable for timer 1

      
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main (void)
{
 
  /* variables for the main*/
  uint8_t  watchdog;
  COMPLEX vector;
  int arr[3] = {0x0801,0x0002,0x0003};
  int Max;
  Max = FindMax(arr, 3);
  
/***********************************************************************/
/*          INITIALIZATIONS
/***********************************************************************/
  sfreg();                                            // initialize sfr's   
  TMR1IE = 1;                                         // Timer 1 interrupt enable
  PEIE = 1;                                           // enable all peripheral interrupts
  ei();                                               // enable all interrupts
  
/*************************************************************************/
/*           CODE WILL REMAIN IN while loop if no watch dog reset
/*************************************************************************/ 
 while (HTS == 0)
  {}                                                 // wait until clock stable 

  while(1)
  {
    if(TO == 1)                                       // No watchdog reset proceed with adc read      
    {
     asm("nop");
     vector.c1 = 0x12;
     vector.c2 = 0x22;
     vector.c3 = 0x33;
     vector.c4 = 0x44;
     vector.c5 = 0x55;
     vector.c6 = 0x66;
     vector.c7 = 0x77;
     vector.c8 = 0x88;
     vector.c9 = 0x99;
     watchdog = TO;
     CLRWDT();
     }
    else                                            // a watchdog reset happend update this count
    { 
      watchdog = TO;
      PORTD = persistent_watch_dog_count++;         // Keep a un-accessed RAM count of wdt 
      CLRWDT();
    }
  }
}

void sfreg(void)
{
WDTCON = 0x17;       // WDTPS3=1|WDTPS2=0|WDTPS1=1|WDTPS0=1|SWDTEN=1 5 seconds before watch dog expires
OSCCON = 0x61;       // 4 Mhz clock
OPTION = 0x00;       // |PSA=Prescale to WDT|most prescale rate|
TRISE  = 0x01;       // MAKE PORTE RE1 an input
TRISD  = 0x00;       // Port D as outputs
T1CON  = 0x35;       // T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled
ANSEL  &=0x00;       // Make a digital I/O
}

int FindMax (int *arr, int size)
{
  int *p = 0;
  int i;

  p = arr[0];
   /** - Defect: Null pointer dereference*/
   
  for(i=0; i < size; i++)
  {
    if(arr[i] > (*p) )
    {
      p = arr[i];
    }
  }
  return p;
}



