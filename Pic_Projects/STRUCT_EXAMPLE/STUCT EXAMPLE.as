opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 5 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 5 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 6 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 6 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_FindMax
	FNCALL	_main,_sfreg
	FNROOT	_main
	global	main@F810
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
	line	39

;initializer for main@F810
	retlw	01h
	retlw	08h

	retlw	02h
	retlw	0

	retlw	03h
	retlw	0

	global	_tmr1_counter
	global	_toms_variable
	global	_persistent_watch_dog_count
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_persistent_watch_dog_count:
       ds      2

	global	_PORTD
_PORTD	set	8
	global	_T1CON
_T1CON	set	16
	global	_GIE
_GIE	set	95
	global	_PEIE
_PEIE	set	94
	global	_TO
_TO	set	28
	global	_OPTION
_OPTION	set	129
	global	_OSCCON
_OSCCON	set	143
	global	_TRISD
_TRISD	set	136
	global	_TRISE
_TRISE	set	137
	global	_HTS
_HTS	set	1146
	global	_TMR1IE
_TMR1IE	set	1120
	global	_WDTCON
_WDTCON	set	261
	global	_ANSEL
_ANSEL	set	392
	file	"STUCT EXAMPLE.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_tmr1_counter:
       ds      1

_toms_variable:
       ds      1

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
main@F810:
       ds      6

; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
; Initialize objects allocated to BANK0
	global __pidataBANK0
psect cinit,class=CODE,delta=2
	fcall	__pidataBANK0+0		;fetch initializer
	movwf	__pdataBANK0+0&07fh		
	fcall	__pidataBANK0+1		;fetch initializer
	movwf	__pdataBANK0+1&07fh		
	fcall	__pidataBANK0+2		;fetch initializer
	movwf	__pdataBANK0+2&07fh		
	fcall	__pidataBANK0+3		;fetch initializer
	movwf	__pdataBANK0+3&07fh		
	fcall	__pidataBANK0+4		;fetch initializer
	movwf	__pdataBANK0+4&07fh		
	fcall	__pidataBANK0+5		;fetch initializer
	movwf	__pdataBANK0+5&07fh		
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_sfreg
?_sfreg:	; 0 bytes @ 0x0
	global	??_sfreg
??_sfreg:	; 0 bytes @ 0x0
	global	?_FindMax
?_FindMax:	; 2 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	global	FindMax@size
FindMax@size:	; 2 bytes @ 0x0
	ds	2
	global	??_FindMax
??_FindMax:	; 0 bytes @ 0x2
	ds	7
	global	FindMax@p
FindMax@p:	; 2 bytes @ 0x9
	ds	2
	global	FindMax@arr
FindMax@arr:	; 1 bytes @ 0xB
	ds	1
	global	FindMax@i
FindMax@i:	; 2 bytes @ 0xC
	ds	2
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	??_main
??_main:	; 0 bytes @ 0x0
	ds	1
	global	main@Max
main@Max:	; 2 bytes @ 0x1
	ds	2
	global	main@arr
main@arr:	; 6 bytes @ 0x3
	ds	6
	global	main@watchdog
main@watchdog:	; 1 bytes @ 0x9
	ds	1
	global	main@vector
main@vector:	; 9 bytes @ 0xA
	ds	9
;;Data sizes: Strings 0, constant 0, data 6, bss 2, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     14      14
;; BANK0           80     19      29
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?_FindMax	int  size(2) Largest target is 512
;;		 -> RAM(DATA[512]), NULL(NULL[0]), 
;;
;; FindMax@arr	PTR int  size(1) Largest target is 6
;;		 -> main@arr(BANK0[6]), 
;;
;; FindMax@p	PTR int  size(2) Largest target is 512
;;		 -> RAM(DATA[512]), NULL(NULL[0]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_FindMax
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                19    19      0     425
;;                                              0 BANK0     19    19      0
;;                            _FindMax
;;                              _sfreg
;; ---------------------------------------------------------------------------------
;; (1) _sfreg                                                0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _FindMax                                             14    12      2     203
;;                                              0 COMMON    14    12      2
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _FindMax
;;   _sfreg
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      2C      12        0.0%
;;ABS                  0      0      2B       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       1       2        0.0%
;;BANK0               50     13      1D       5       36.3%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      E       E       1      100.0%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 34 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  vector          9   10[BANK0 ] struct .
;;  arr             6    3[BANK0 ] int [3]
;;  Max             2    1[BANK0 ] int 
;;  watchdog        1    9[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2  844[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      18       0       0       0
;;      Temps:          0       1       0       0       0
;;      Totals:         0      19       0       0       0
;;Total ram usage:       19 bytes
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_FindMax
;;		_sfreg
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
	line	34
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 7
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	39
	
l2102:	
;main.c: 37: uint8_t watchdog;
;main.c: 38: COMPLEX vector;
;main.c: 39: int arr[3] = {0x0801,0x0002,0x0003};
	movlw	(main@arr)&0ffh
	movwf	fsr0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@F810),w
	bcf	status, 7	;select IRP bank0
	movwf	indf
	incf	fsr0,f
	movf	(main@F810)+1,w
	movwf	indf
	incf	fsr0,f
	movf	(main@F810)+2,w
	movwf	indf
	incf	fsr0,f
	movf	(main@F810)+3,w
	movwf	indf
	incf	fsr0,f
	movf	(main@F810)+4,w
	movwf	indf
	incf	fsr0,f
	movf	(main@F810)+5,w
	movwf	indf
	line	41
	
l2104:	
;main.c: 40: int Max;
;main.c: 41: Max = FindMax(arr, 3);
	movlw	low(03h)
	movwf	(?_FindMax)
	movlw	high(03h)
	movwf	((?_FindMax))+1
	movlw	(main@arr)&0ffh
	fcall	_FindMax
	movf	(1+(?_FindMax)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@Max+1)
	addwf	(main@Max+1)
	movf	(0+(?_FindMax)),w
	clrf	(main@Max)
	addwf	(main@Max)

	line	46
	
l2106:	
;main.c: 46: sfreg();
	fcall	_sfreg
	line	47
	
l2108:	
;main.c: 47: TMR1IE = 1;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	bsf	(1120/8)^080h,(1120)&7
	line	48
	
l2110:	
;main.c: 48: PEIE = 1;
	bsf	(94/8),(94)&7
	line	49
	
l2112:	
;main.c: 49: (GIE = 1);
	bsf	(95/8),(95)&7
	line	54
;main.c: 54: while (HTS == 0)
	goto	l847
	
l848:	
	line	55
;main.c: 55: {}
	
l847:	
	line	54
	btfss	(1146/8)^080h,(1146)&7
	goto	u2201
	goto	u2200
u2201:
	goto	l847
u2200:
	goto	l850
	
l849:	
	line	57
;main.c: 57: while(1)
	
l850:	
	line	59
;main.c: 58: {
;main.c: 59: if(TO == 1)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(28/8),(28)&7
	goto	u2211
	goto	u2210
u2211:
	goto	l2120
u2210:
	line	61
	
l2114:	
# 61 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
nop ;#
psect	maintext
	line	62
	
l2116:	
;main.c: 62: vector.c1 = 0x12;
	movlw	(012h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@vector)
	line	63
;main.c: 63: vector.c2 = 0x22;
	movlw	(022h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+01h
	line	64
;main.c: 64: vector.c3 = 0x33;
	movlw	(033h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+02h
	line	65
;main.c: 65: vector.c4 = 0x44;
	movlw	(044h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+03h
	line	66
;main.c: 66: vector.c5 = 0x55;
	movlw	(055h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+04h
	line	67
;main.c: 67: vector.c6 = 0x66;
	movlw	(066h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+05h
	line	68
;main.c: 68: vector.c7 = 0x77;
	movlw	(077h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+06h
	line	69
;main.c: 69: vector.c8 = 0x88;
	movlw	(088h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+07h
	line	70
;main.c: 70: vector.c9 = 0x99;
	movlw	(099h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@vector)+08h
	line	71
;main.c: 71: watchdog = TO;
	movlw	0
	btfsc	(28/8),(28)&7
	movlw	1
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@watchdog)
	line	72
	
l2118:	
# 72 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
clrwdt ;#
psect	maintext
	line	73
;main.c: 73: }
	goto	l850
	line	74
	
l851:	
	line	76
	
l2120:	
;main.c: 74: else
;main.c: 75: {
;main.c: 76: watchdog = TO;
	movlw	0
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfsc	(28/8),(28)&7
	movlw	1
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@watchdog)
	line	77
;main.c: 77: PORTD = persistent_watch_dog_count++;
	movf	(_persistent_watch_dog_count),w
	movwf	(8)	;volatile
	movlw	low(01h)
	addwf	(_persistent_watch_dog_count),f
	skipnc
	incf	(_persistent_watch_dog_count+1),f
	movlw	high(01h)
	addwf	(_persistent_watch_dog_count+1),f
	line	78
	
l2122:	
# 78 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
clrwdt ;#
psect	maintext
	goto	l850
	line	79
	
l852:	
	goto	l850
	line	80
	
l853:	
	line	57
	goto	l850
	
l854:	
	line	81
	
l855:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_sfreg
psect	text109,local,class=CODE,delta=2
global __ptext109
__ptext109:

;; *************** function _sfreg *****************
;; Defined at:
;;		line 84 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text109
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
	line	84
	global	__size_of_sfreg
	__size_of_sfreg	equ	__end_of_sfreg-_sfreg
	
_sfreg:	
	opt	stack 7
; Regs used in _sfreg: [wreg+status,2]
	line	85
	
l2092:	
;main.c: 85: WDTCON = 0x17;
	movlw	(017h)
	bcf	status, 5	;RP0=0, select bank2
	bsf	status, 6	;RP1=1, select bank2
	movwf	(261)^0100h	;volatile
	line	86
;main.c: 86: OSCCON = 0x61;
	movlw	(061h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(143)^080h	;volatile
	line	87
	
l2094:	
;main.c: 87: OPTION = 0x00;
	clrf	(129)^080h	;volatile
	line	88
	
l2096:	
;main.c: 88: TRISE = 0x01;
	movlw	(01h)
	movwf	(137)^080h	;volatile
	line	89
;main.c: 89: TRISD = 0x00;
	clrf	(136)^080h	;volatile
	line	90
	
l2098:	
;main.c: 90: T1CON = 0x35;
	movlw	(035h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(16)	;volatile
	line	91
	
l2100:	
;main.c: 91: ANSEL &=0x00;
	bsf	status, 5	;RP0=1, select bank3
	bsf	status, 6	;RP1=1, select bank3
	clrf	(392)^0180h	;volatile
	line	92
	
l858:	
	return
	opt stack 0
GLOBAL	__end_of_sfreg
	__end_of_sfreg:
;; =============== function _sfreg ends ============

	signat	_sfreg,88
	global	_FindMax
psect	text110,local,class=CODE,delta=2
global __ptext110
__ptext110:

;; *************** function _FindMax *****************
;; Defined at:
;;		line 95 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
;; Parameters:    Size  Location     Type
;;  arr             1    wreg     PTR int 
;;		 -> main@arr(6), 
;;  size            2    0[COMMON] int 
;; Auto vars:     Size  Location     Type
;;  arr             1   11[COMMON] PTR int 
;;		 -> main@arr(6), 
;;  i               2   12[COMMON] int 
;;  p               2    9[COMMON] PTR int 
;;		 -> RAM(512), NULL(0), 
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         2       0       0       0       0
;;      Locals:         5       0       0       0       0
;;      Temps:          7       0       0       0       0
;;      Totals:        14       0       0       0       0
;;Total ram usage:       14 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text110
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\STRUCT_EXAMPLE\main.c"
	line	95
	global	__size_of_FindMax
	__size_of_FindMax	equ	__end_of_FindMax-_FindMax
	
_FindMax:	
	opt	stack 7
; Regs used in _FindMax: [wreg-fsr0h+status,2+status,0]
;FindMax@arr stored from wreg
	movwf	(FindMax@arr)
	line	96
	
l2074:	
;main.c: 96: int *p = 0;
	movlw	(0&0ffh)
	movwf	(FindMax@p)
	movlw	(0x0/2)
	movwf	(FindMax@p+1)
	line	99
	
l2076:	
;main.c: 97: int i;
;main.c: 99: p = arr[0];
	movf	(FindMax@arr),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(FindMax@p)
	incf	fsr0,f
	movf	indf,w
	movwf	(FindMax@p+1)
	line	102
	
l2078:	
;main.c: 102: for(i=0; i < size; i++)
	movlw	low(0)
	movwf	(FindMax@i)
	movlw	high(0)
	movwf	((FindMax@i))+1
	goto	l2086
	line	103
	
l862:	
	line	104
	
l2080:	
;main.c: 103: {
;main.c: 104: if(arr[i] > (*p) )
	movf	(FindMax@i),w
	movwf	(??_FindMax+0)+0
	addwf	(??_FindMax+0)+0,w
	addwf	(FindMax@arr),w
	movwf	(??_FindMax+1)+0
	movf	0+(??_FindMax+1)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(??_FindMax+2)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_FindMax+2)+0+1
	movf	(FindMax@p),w
	movwf	fsr0
	bsf	status,7
	btfss	(FindMax@p+1),0
	bcf	status,7
	movf	indf,w
	movwf	(??_FindMax+4)+0+0
	incf	fsr0,f
	movf	indf,w
	movwf	(??_FindMax+4)+0+1
	movf	1+(??_FindMax+4)+0,w
	xorlw	80h
	movwf	(??_FindMax+6)+0
	movf	1+(??_FindMax+2)+0,w
	xorlw	80h
	subwf	(??_FindMax+6)+0,w
	skipz
	goto	u2185
	movf	0+(??_FindMax+2)+0,w
	subwf	0+(??_FindMax+4)+0,w
u2185:

	skipnc
	goto	u2181
	goto	u2180
u2181:
	goto	l2084
u2180:
	line	106
	
l2082:	
;main.c: 105: {
;main.c: 106: p = arr[i];
	movf	(FindMax@i),w
	movwf	(??_FindMax+0)+0
	addwf	(??_FindMax+0)+0,w
	addwf	(FindMax@arr),w
	movwf	(??_FindMax+1)+0
	movf	0+(??_FindMax+1)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(FindMax@p)
	incf	fsr0,f
	movf	indf,w
	movwf	(FindMax@p+1)
	goto	l2084
	line	107
	
l863:	
	line	102
	
l2084:	
	movlw	low(01h)
	addwf	(FindMax@i),f
	skipnc
	incf	(FindMax@i+1),f
	movlw	high(01h)
	addwf	(FindMax@i+1),f
	goto	l2086
	
l861:	
	
l2086:	
	movf	(FindMax@i+1),w
	xorlw	80h
	movwf	(??_FindMax+0)+0
	movf	(FindMax@size+1),w
	xorlw	80h
	subwf	(??_FindMax+0)+0,w
	skipz
	goto	u2195
	movf	(FindMax@size),w
	subwf	(FindMax@i),w
u2195:

	skipc
	goto	u2191
	goto	u2190
u2191:
	goto	l2080
u2190:
	goto	l2088
	
l864:	
	line	109
	
l2088:	
;main.c: 107: }
;main.c: 108: }
;main.c: 109: return p;
	movf	(FindMax@p+1),w
	clrf	(?_FindMax+1)
	addwf	(?_FindMax+1)
	movf	(FindMax@p),w
	clrf	(?_FindMax)
	addwf	(?_FindMax)

	goto	l865
	
l2090:	
	line	110
	
l865:	
	return
	opt stack 0
GLOBAL	__end_of_FindMax
	__end_of_FindMax:
;; =============== function _FindMax ends ============

	signat	_FindMax,8314
psect	text111,local,class=CODE,delta=2
global __ptext111
__ptext111:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
