opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F877A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 10 "C:\pic_projects\HITECH_EXAMPLE\test_main.c"
	psect config,class=CONFIG,delta=2 ;#
# 10 "C:\pic_projects\HITECH_EXAMPLE\test_main.c"
	dw 0x3FFD & 0x3FFB & 0x3FFF & 0x3FBF & 0x3FFF & 0x3FFF & 0x37FF & 0x3FFF & 0x3FFF ;#
	FNCALL	_main,_init
	FNROOT	_main
	FNCALL	intlevel1,_my_isr
	global	intlevel1
	FNROOT	intlevel1
	global	_counter
	global	_toms_count
	global	_INTCON
psect	text72,local,class=CODE,delta=2
global __ptext72
__ptext72:
_INTCON	set	11
	global	_PORTB
_PORTB	set	6
	global	_T1CON
_T1CON	set	16
	global	_GIE
_GIE	set	95
	global	_TMR1IF
_TMR1IF	set	96
	global	_OPTION
_OPTION	set	129
	global	_PIE1
_PIE1	set	140
	global	_TRISB
_TRISB	set	134
	global	_TMR1IE
_TMR1IE	set	1120
	file	"HITECH.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_counter:
       ds      1

_toms_count:
       ds      1

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_init
?_init:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?_my_isr
?_my_isr:	; 0 bytes @ 0x0
	global	??_my_isr
??_my_isr:	; 0 bytes @ 0x0
	ds	5
	global	??_init
??_init:	; 0 bytes @ 0x5
	global	??_main
??_main:	; 0 bytes @ 0x5
	ds	1
;;Data sizes: Strings 0, constant 0, data 0, bss 2, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6       8
;; BANK0           80      0       0
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _my_isr in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _my_isr in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _my_isr in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _my_isr in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.
;;
;; Critical Paths under _my_isr in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 1     1      0       0
;;                                              5 COMMON     1     1      0
;;                               _init
;; ---------------------------------------------------------------------------------
;; (1) _init                                                 0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 1
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (2) _my_isr                                               5     5      0       0
;;                                              0 COMMON     5     5      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _init
;;
;; _my_isr (ROOT)
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      6       8       1       57.1%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       1       2        0.0%
;;ABS                  0      0       8       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0       9      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 32 in file "C:\pic_projects\HITECH_EXAMPLE\test_main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_init
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\pic_projects\HITECH_EXAMPLE\test_main.c"
	line	32
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 6
; Regs used in _main: [wreg+status,2+status,0+pclath+cstack]
	line	33
	
l2299:	
;test_main.c: 33: counter = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(_counter)	;volatile
	line	34
;test_main.c: 34: toms_count=0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(_toms_count)
	line	35
	
l2301:	
;test_main.c: 35: init();
	fcall	_init
	line	36
	
l2303:	
;test_main.c: 36: (GIE = 1);
	bsf	(95/8),(95)&7
	goto	l2305
	line	38
;test_main.c: 38: while (1)
	
l566:	
	line	40
	
l2305:	
;test_main.c: 39: {
;test_main.c: 40: PORTB = counter;
	movf	(_counter),w	;volatile
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(6)	;volatile
	line	44
	
l2307:	
;test_main.c: 44: if (counter >= 3)
	movlw	(03h)
	subwf	(_counter),w	;volatile
	skipc
	goto	u2161
	goto	u2160
u2161:
	goto	l2305
u2160:
	line	47
	
l2309:	
;test_main.c: 45: {
;test_main.c: 47: toms_count++;
	movlw	(01h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	addwf	(_toms_count),f
	goto	l2305
	line	48
	
l567:	
	goto	l2305
	line	49
	
l568:	
	line	38
	goto	l2305
	
l569:	
	line	52
	
l570:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_init
psect	text73,local,class=CODE,delta=2
global __ptext73
__ptext73:

;; *************** function _init *****************
;; Defined at:
;;		line 16 in file "C:\pic_projects\HITECH_EXAMPLE\test_main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text73
	file	"C:\pic_projects\HITECH_EXAMPLE\test_main.c"
	line	16
	global	__size_of_init
	__size_of_init	equ	__end_of_init-_init
	
_init:	
	opt	stack 6
; Regs used in _init: [wreg+status,2]
	line	17
	
l1501:	
;test_main.c: 17: PIE1 = 0x01;
	movlw	(01h)
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	movwf	(140)^080h	;volatile
	line	18
;test_main.c: 18: INTCON = 0x40;
	movlw	(040h)
	movwf	(11)	;volatile
	line	19
;test_main.c: 19: OPTION = 0x80;
	movlw	(080h)
	movwf	(129)^080h	;volatile
	line	20
;test_main.c: 20: T1CON = 0x35;
	movlw	(035h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(16)	;volatile
	line	21
	
l1503:	
;test_main.c: 21: TRISB = 0x00;
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(134)^080h	;volatile
	line	23
	
l563:	
	return
	opt stack 0
GLOBAL	__end_of_init
	__end_of_init:
;; =============== function _init ends ============

	signat	_init,88
	global	_my_isr
psect	text74,local,class=CODE,delta=2
global __ptext74
__ptext74:

;; *************** function _my_isr *****************
;; Defined at:
;;		line 12 in file "C:\pic_projects\HITECH_EXAMPLE\isr_func.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          5       0       0       0       0
;;      Totals:         5       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	text74
	file	"C:\pic_projects\HITECH_EXAMPLE\isr_func.c"
	line	12
	global	__size_of_my_isr
	__size_of_my_isr	equ	__end_of_my_isr-_my_isr
	
_my_isr:	
	opt	stack 6
; Regs used in _my_isr: [wreg+status,2+status,0]
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
global interrupt_function
interrupt_function:
	global saved_w
	saved_w	set	btemp+0
	movwf	saved_w
	movf	status,w
	movwf	(??_my_isr+1)
	movf	fsr0,w
	movwf	(??_my_isr+2)
	movf	pclath,w
	movwf	(??_my_isr+3)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	btemp+1,w
	movwf	(??_my_isr+4)
	ljmp	_my_isr
psect	text74
	line	13
	
i1l1505:	
;isr_func.c: 13: if((TMR1IE)&&(TMR1IF))
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	btfss	(1120/8)^080h,(1120)&7
	goto	u1_21
	goto	u1_20
u1_21:
	goto	i1l1132
u1_20:
	
i1l1507:	
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	btfss	(96/8),(96)&7
	goto	u2_21
	goto	u2_20
u2_21:
	goto	i1l1132
u2_20:
	line	15
	
i1l1509:	
;isr_func.c: 14: {
;isr_func.c: 15: counter++;
	movlw	(01h)
	movwf	(??_my_isr+0)+0
	movf	(??_my_isr+0)+0,w
	addwf	(_counter),f	;volatile
	line	16
	
i1l1511:	
;isr_func.c: 16: TMR1IF=0;
	bcf	(96/8),(96)&7
	goto	i1l1132
	line	17
	
i1l1131:	
	line	18
	
i1l1132:	
	movf	(??_my_isr+4),w
	bcf	status, 5	;RP0=0, select bank0
	movwf	btemp+1
	movf	(??_my_isr+3),w
	movwf	pclath
	movf	(??_my_isr+2),w
	movwf	fsr0
	movf	(??_my_isr+1),w
	movwf	status
	swapf	saved_w,f
	swapf	saved_w,w
	retfie
	opt stack 0
GLOBAL	__end_of_my_isr
	__end_of_my_isr:
;; =============== function _my_isr ends ============

	signat	_my_isr,88
psect	text75,local,class=CODE,delta=2
global __ptext75
__ptext75:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
