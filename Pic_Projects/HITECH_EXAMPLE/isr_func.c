#include <htc.h>

/***********************************************************/
/* Variable Definition
/***********************************************************/
extern volatile char main_counter;
extern volatile char counter_flag;
extern volatile int masked_count_rd0;
extern volatile int masked_count_rd1;
extern volatile int masked_count_rd2;
extern volatile int masked_count_rd3;
extern volatile int masked_count_rd4;
extern volatile int masked_count_rd5;
extern volatile int masked_count_rd6;
extern volatile int masked_count_rd7;
/***********************************************************/
/* isr function
/***********************************************************/ 
void interrupt my_isr(void)
{
 if((TMR1IE)&&(TMR1IF))
    {
     INTCON = 0x00;  //Globaly and peripheraly disable all interruputs
     main_counter++;
     TMR1IF=0;
     counter_flag=1;
     TMR1L = 0x00;
     masked_count_rd0 = (main_counter&0x01);  //mask out bit 0
     masked_count_rd1 = (main_counter&0x02);  //mask out bit 1 
     masked_count_rd2 = (main_counter&0x04);  //mask out bit 2 
     masked_count_rd3 = (main_counter&0x08);  //mask out bit 3
     masked_count_rd4 = (main_counter&0x10);  //mask out bit 4
     masked_count_rd5 = (main_counter&0x20);  //mask out bit 5
     masked_count_rd6 = (main_counter&0x40);  //mask out bit 6
     masked_count_rd7 = (main_counter&0x80);  //mask out bit 7 
     INTCON = 0x40;  //Re-enable peripheral interrupts
     }
}
      