/**********************************************************/
/* Sample program from HI-TECH quick start .pdf           */
/* 1 Jun 2011 using PIC16F877A                            */ 
/* blink PORT B and I put in my own variable              */
/**********************************************************/
#include <htc.h>
/**********************************************************/
/* Device Configuration
/**********************************************************/
       /*PIC16F887*/
//__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
//__CONFIG(BORV40);
      /* PIC16F877A*/
       __CONFIG(XT & WDTDIS & PWRTDIS & BORDIS & LVPEN & WRTEN &
       DEBUGEN & DUNPROT & UNPROTECT & HS);
      
       
 
/**********************************************************/
/* Initialize SFR 
/**********************************************************/
int off_switch(void);
void ee_write(void);
void init(void)

{
//ANSEL   = 0x00;
TRISE   = 0xFF; 
//OSCCON  = 0x61;       // Internal 4 Mhz clock                     
PIE1    = 0x01;       // TMR1IE = enabled 
INTCON  = 0x40;       // PEIE = enabled
OPTION  = 0x80;       // PORTB pull-ups disabled
T1CON   = 0x35;       // T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled
TRISD   = 0x00;       // port directions: 1=input, 0=output
//TMR1L   = 0xE0;
//TMR1H   = 0xFF;
}
/***********************************************************/
/* Variable Definition
/***********************************************************/
volatile char counter;       
unsigned char toms_count;
unsigned int eeprom_low  = 0x0005;
unsigned int eeprom_mid  = 0x0004;
unsigned int eeprom_hi   = 0x0003;
unsigned int ee_copy_low = 0x0025;
unsigned int ee_copy_mid = 0x0024;
unsigned int ee_copy_hi  = 0x0023;
unsigned int ee_copy_low_write;
unsigned int flash_adr   = 0x00A0;
unsigned int slow_count; 
int main_while = 1;

int main(void)
{
TRISD = 0x00;
PORTD = 0xFF;
/*
init();
//PORTD = 0;
counter = 0;
toms_count=0;
slow_count = 0;
ee_copy_low_write=eeprom_read(eeprom_low);   
eeprom_write(ee_copy_low,ee_copy_low_write);  // make a copy initially

//while (HTS == 0)                            // wait until clock stable HTS set   |    |IRCF2|IRCF1|IRCF0|OSTS|HTS|LTS|SCS| 
  //    {}
while (RE0 == 0)                            // wait until pushed
      {}
while (RE0 == 1)                            // wait until released
      {}
ei();                                       // enable interrupts
 while (main_while == 1)
    {
    PORTD = slow_count;
  
   if (counter == 1)                        // .5 second of count
      {
      counter=0;                            // reinitaize the TMR1 count
      slow_count++;  
      toms_count++;
      off_switch();                         // RE0 push button
      ee_write();
      TMR1H  = 0xF0;
       }
     }*/   
}

void ee_write(void)
{
 if (toms_count<=0x55)
  {
   eeprom_write(eeprom_low,toms_count);    // eeprom write
  }
 else if (toms_count >= 0x56)
  {
  eeprom_write(eeprom_mid,toms_count);
  }  
  else {}

 
}

int off_switch(void)
{
 if (RE0 == 1)
    {
     di();                                  // disable interrupts 
     while (RE0 == 1)                       // wait until pushed
      {
       main_while = 0;
      }
    } 
  else 
    {}
return 0;
}