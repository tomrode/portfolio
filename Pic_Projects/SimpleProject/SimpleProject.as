opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 7 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD & 0x3FBF ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 8 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	dw 0x3FFF ;#
	FNCALL	_main,_gfGPLD_1_OperatorAlarmSelInit
	FNCALL	_main,_TestCRC
	FNCALL	_main,_gADCAverageRead
	FNCALL	_gADCAverageRead,___lwdiv
	FNROOT	_main
	global	mainold@F1055
psect	idataBANK0,class=CODE,space=0,delta=2
global __pidataBANK0
__pidataBANK0:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	line	499

;initializer for mainold@F1055
	retlw	0AAh
	retlw	0BBh
	global	_AlarmTimes
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	line	116
_AlarmTimes:
	retlw	0
	retlw	01h
	retlw	0
	retlw	0
	retlw	0
	retlw	01h
	retlw	0
	retlw	0
	retlw	01h
	retlw	01h
	retlw	01h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	01h
	retlw	02h
	retlw	02h
	retlw	05h
	retlw	05h
	retlw	0
	retlw	0
	retlw	023h
	retlw	01h
	retlw	03h
	retlw	03h
	retlw	05h
	retlw	05h
	retlw	0
	retlw	0
	retlw	064h
	retlw	01h
	retlw	04h
	retlw	01h
	retlw	032h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	05h
	retlw	01h
	retlw	05h
	retlw	05h
	retlw	05h
	retlw	05h
	retlw	0
	retlw	0
	retlw	06h
	retlw	01h
	retlw	06h
	retlw	013h
	retlw	019h
	retlw	0C8h
	retlw	0
	retlw	01h
	retlw	07h
	retlw	01h
	retlw	06h
	retlw	013h
	retlw	019h
	retlw	032h
	retlw	0
	retlw	01h
	retlw	08h
	retlw	01h
	retlw	0Ch
	retlw	0Ch
	retlw	0Ch
	retlw	0Ch
	retlw	0
	retlw	01h
	retlw	09h
	retlw	01h
	retlw	064h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0Ah
	retlw	01h
	retlw	0C8h
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	retlw	0
	global	_MyConstAr
psect	strings
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	line	53
_MyConstAr:
	retlw	011h
	retlw	022h
	retlw	033h
	global	_AlarmTimes
	global	_MyConstAr
	global	_gKey_Switches
	global	mainold@ubOldValue
	global	mainold@uwColumnval
	global	_HwInput
	global	_UwRowColStat
	global	_UwRowColStatMSK
	global	_UwRowColpar
	global	_x
	global	example@CutVal
	global	gADCAverageRead@uwAggregateCnt
	global	_NewKeySw
	global	_toms_variable
	global	_ubUseAlarmLock
	global	_ubUseAlarmLockInit
	global	gADCAverageRead@ubDivideByCnt
	global	gADCAverageRead@ubResult
	global	gfGPLD_1_OperatorAlarmSel@fAlarmTypeTwoSame
	global	gfGPLD_1_OperatorAlarmSel@fCycleRepeatPar
	global	gfGPLD_1_OperatorAlarmSel@ubAlarmStMch
	global	gfGPLD_1_OperatorAlarmSel@ubAlarmTypeLast
	global	gfGPLD_1_OperatorAlarmSel@ubAlarmTypePar
	global	gfGPLD_1_OperatorAlarmSel@ubBuzzerState
	global	gfGPLD_1_OperatorAlarmSel@ubNumOfCyclesPar
	global	gfGPLD_1_OperatorAlarmSel@ubOffTime1Par
	global	gfGPLD_1_OperatorAlarmSel@ubOffTime2Par
	global	gfGPLD_1_OperatorAlarmSel@ubOffTimeCnt
	global	gfGPLD_1_OperatorAlarmSel@ubOnTime1Par
	global	gfGPLD_1_OperatorAlarmSel@ubOnTime2Par
	global	gfGPLD_1_OperatorAlarmSel@ubPauseCnt
	global	gfGPLD_1_OperatorAlarmSel@ubPauseTimePar
	global	gfGPLD_1_OperatorAlarmSel@ubRepeatCnt
	global	gfGPLD_1_OperatorAlarmSel@ubUseAlarmLock
	global	mainold@ubPot2Transmit
	global	gfGPLD_1_OperatorAlarmSel@ubAlarmTypeCurrent
	global	gfGPLD_1_OperatorAlarmSel@ubOnTimeCnt
	global	mainold@ubRowSwTransition
	global	_Test
	global	_MyConstArRcv
	global	_geMot_OT_State
	global	TestCRC@uwTest
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
TestCRC@uwTest:
       ds      2

	file	"SimpleProject.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
gfGPLD_1_OperatorAlarmSel@ubAlarmTypeCurrent:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubOnTimeCnt:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_gKey_Switches:
       ds      14

mainold@ubOldValue:
       ds      6

mainold@uwColumnval:
       ds      4

_HwInput:
       ds      2

_UwRowColStat:
       ds      2

_UwRowColStatMSK:
       ds      2

_UwRowColpar:
       ds      2

_x:
       ds      2

example@CutVal:
       ds      2

gADCAverageRead@uwAggregateCnt:
       ds      2

_NewKeySw:
       ds      1

_toms_variable:
       ds      1

_ubUseAlarmLock:
       ds      1

_ubUseAlarmLockInit:
       ds      1

gADCAverageRead@ubDivideByCnt:
       ds      1

gADCAverageRead@ubResult:
       ds      1

gfGPLD_1_OperatorAlarmSel@fAlarmTypeTwoSame:
       ds      1

gfGPLD_1_OperatorAlarmSel@fCycleRepeatPar:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubAlarmStMch:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubAlarmTypeLast:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubAlarmTypePar:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubBuzzerState:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubNumOfCyclesPar:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubOffTime1Par:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubOffTime2Par:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubOffTimeCnt:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubOnTime1Par:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubOnTime2Par:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubPauseCnt:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubPauseTimePar:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubRepeatCnt:
       ds      1

gfGPLD_1_OperatorAlarmSel@ubUseAlarmLock:
       ds      1

mainold@ubPot2Transmit:
       ds      1

_MyConstArRcv:
       ds      3

_geMot_OT_State:
       ds      1

psect	dataBANK0,class=BANK0,space=1
global __pdataBANK0
__pdataBANK0:
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	line	499
mainold@F1055:
       ds      2

psect	bssBANK1,class=BANK1,space=1
global __pbssBANK1
__pbssBANK1:
mainold@ubRowSwTransition:
       ds      6

_Test:
       ds      4

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR containing the base address, and
;	W with the last address+1
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf		;clear RAM location pointed to by FSR
	incf	fsr,f		;increment pointer
	xorwf	fsr,w		;XOR with final address
	btfsc	status,2	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
	xorwf	fsr,w		;XOR again to restore value
	goto	clrloop		;do the next byte

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	bcf	status, 7	;select IRP bank0
	movlw	low(__pbssBANK0)
	movwf	fsr
	movlw	low((__pbssBANK0)+041h)
	fcall	clear_ram
; Clear objects allocated to BANK1
psect cinit,class=CODE,delta=2
	movlw	low(__pbssBANK1)
	movwf	fsr
	movlw	low((__pbssBANK1)+0Ah)
	fcall	clear_ram
; Initialize objects allocated to BANK0
	global __pidataBANK0
psect cinit,class=CODE,delta=2
	fcall	__pidataBANK0+0		;fetch initializer
	movwf	__pdataBANK0+0&07fh		
	fcall	__pidataBANK0+1		;fetch initializer
	movwf	__pdataBANK0+1&07fh		
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_gfGPLD_1_OperatorAlarmSelInit
?_gfGPLD_1_OperatorAlarmSelInit:	; 0 bytes @ 0x0
	global	??_gfGPLD_1_OperatorAlarmSelInit
??_gfGPLD_1_OperatorAlarmSelInit:	; 0 bytes @ 0x0
	global	?_TestCRC
?_TestCRC:	; 0 bytes @ 0x0
	global	?_gADCAverageRead
?_gADCAverageRead:	; 1 bytes @ 0x0
	global	?_main
?_main:	; 2 bytes @ 0x0
	global	?___lwdiv
?___lwdiv:	; 2 bytes @ 0x0
	global	TestCRC@ptr
TestCRC@ptr:	; 2 bytes @ 0x0
	global	___lwdiv@divisor
___lwdiv@divisor:	; 2 bytes @ 0x0
	ds	2
	global	??_TestCRC
??_TestCRC:	; 0 bytes @ 0x2
	global	___lwdiv@dividend
___lwdiv@dividend:	; 2 bytes @ 0x2
	ds	2
	global	??___lwdiv
??___lwdiv:	; 0 bytes @ 0x4
	ds	1
	global	___lwdiv@quotient
___lwdiv@quotient:	; 2 bytes @ 0x5
	ds	2
	global	___lwdiv@counter
___lwdiv@counter:	; 1 bytes @ 0x7
	ds	1
	global	??_gADCAverageRead
??_gADCAverageRead:	; 0 bytes @ 0x8
	ds	2
	global	gADCAverageRead@ADCIn
gADCAverageRead@ADCIn:	; 1 bytes @ 0xA
	ds	1
	global	??_main
??_main:	; 0 bytes @ 0xB
	ds	1
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	main@SoundID
main@SoundID:	; 1 bytes @ 0x0
	ds	1
	global	main@ulValPtr
main@ulValPtr:	; 4 bytes @ 0x1
	ds	4
	global	main@ulValue
main@ulValue:	; 4 bytes @ 0x5
	ds	4
	global	main@TestADC
main@TestADC:	; 1 bytes @ 0x9
	ds	1
	global	main@pulAddrPtr
main@pulAddrPtr:	; 1 bytes @ 0xA
	ds	1
;;Data sizes: Strings 0, constant 91, data 2, bss 77, persistent 2 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     12      14
;; BANK0           80     11      80
;; BANK1           80      0      10
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___lwdiv	unsigned int  size(1) Largest target is 0
;;
;; sp__memcpy	PTR void  size(1) Largest target is 3
;;		 -> MyConstArRcv(BANK0[3]), 
;;
;; TestCRC@ptr	PTR const unsigned short  size(2) Largest target is 512
;;		 -> RAM(DATA[512]), 
;;
;; main@pulAddrPtr	PTR unsigned long  size(1) Largest target is 4
;;		 -> main@ulValue(BANK0[4]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_gADCAverageRead
;;   _gADCAverageRead->___lwdiv
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 1, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                13    13      0     300
;;                                             11 COMMON     1     1      0
;;                                              0 BANK0     11    11      0
;;      _gfGPLD_1_OperatorAlarmSelInit
;;                            _TestCRC
;;                    _gADCAverageRead
;; ---------------------------------------------------------------------------------
;; (1) _gADCAverageRead                                      3     3      0     184
;;                                              8 COMMON     3     3      0
;;                            ___lwdiv
;; ---------------------------------------------------------------------------------
;; (2) ___lwdiv                                              8     4      4     162
;;                                              0 COMMON     8     4      4
;; ---------------------------------------------------------------------------------
;; (1) _TestCRC                                              2     0      2      22
;;                                              0 COMMON     2     0      2
;; ---------------------------------------------------------------------------------
;; (1) _gfGPLD_1_OperatorAlarmSelInit                        0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _gfGPLD_1_OperatorAlarmSelInit
;;   _TestCRC
;;   _gADCAverageRead
;;     ___lwdiv
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BANK3               60      0       0       9        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;BANK2               60      0       0      11        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITSFR2              0      0       0       5        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;BANK1               50      0       A       7       12.5%
;;BITBANK1            50      0       0       6        0.0%
;;CODE                 0      0       0       0        0.0%
;;DATA                 0      0      6A      12        0.0%
;;ABS                  0      0      68       3        0.0%
;;NULL                 0      0       0       0        0.0%
;;STACK                0      0       2       2        0.0%
;;BANK0               50      B      50       5      100.0%
;;BITBANK0            50      0       0       4        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;COMMON               E      C       E       1      100.0%
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 160 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  ulValue         4    5[BANK0 ] unsigned long 
;;  ulValPtr        4    1[BANK0 ] unsigned long 
;;  pulAddrPtr      1   10[BANK0 ] PTR unsigned long 
;;		 -> main@ulValue(4), 
;;  TestADC         1    9[BANK0 ] unsigned char 
;;  SoundID         1    0[BANK0 ] unsigned char 
;;  OutputEn        1    0        unsigned char 
;; Return value:  Size  Location     Type
;;                  2  876[COMMON] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      11       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         1      11       0       0       0
;;Total ram usage:       12 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_gfGPLD_1_OperatorAlarmSelInit
;;		_TestCRC
;;		_gADCAverageRead
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	line	160
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 6
; Regs used in _main: [wreg-fsr0h+status,2+status,0+pclath+cstack]
	line	161
	
l2885:	
;main.c: 161: uint8_t SoundID = 2;
	movlw	(02h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@SoundID)
	line	164
	
l2887:	
;main.c: 162: uint8_t OutputEn;
;main.c: 163: uint8_t TestADC;
;main.c: 164: gfGPLD_1_OperatorAlarmSelInit();
	fcall	_gfGPLD_1_OperatorAlarmSelInit
	line	166
	
l2889:	
;main.c: 165: uint32_t *pulAddrPtr;
;main.c: 166: uint32_t ulValue = 0xFEEDFACE;
	movlw	0FEh
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@ulValue+3)
	movlw	0EDh
	movwf	(main@ulValue+2)
	movlw	0FAh
	movwf	(main@ulValue+1)
	movlw	0CEh
	movwf	(main@ulValue)

	line	169
	
l2891:	
;main.c: 167: uint32_t ulValPtr;
;main.c: 169: pulAddrPtr = &ulValue;
	movlw	(main@ulValue)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@pulAddrPtr)
	line	170
	
l2893:	
;main.c: 170: ulValPtr = *pulAddrPtr;
	movf	(main@pulAddrPtr),w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(main@ulValPtr)
	incf	fsr0,f
	movf	indf,w
	movwf	(main@ulValPtr+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(main@ulValPtr+2)
	incf	fsr0,f
	movf	indf,w
	movwf	(main@ulValPtr+3)
	line	172
	
l2895:	
;main.c: 172: TestCRC((uint16_t *) ulValPtr);
	movf	(main@ulValPtr+1),w
	clrf	(?_TestCRC+1)
	addwf	(?_TestCRC+1)
	movf	(main@ulValPtr),w
	clrf	(?_TestCRC)
	addwf	(?_TestCRC)

	fcall	_TestCRC
	goto	l2897
	line	175
;main.c: 175: while(1)
	
l877:	
	line	178
	
l2897:	
# 178 "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
nop ;#
psect	maintext
	line	179
	
l2899:	
;main.c: 179: TestADC = gADCAverageRead(TestADC);
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(main@TestADC),w
	fcall	_gADCAverageRead
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(main@TestADC)
	goto	l2897
	line	181
	
l878:	
	line	175
	goto	l2897
	
l879:	
	line	182
	
l880:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,90
	global	_gADCAverageRead
psect	text143,local,class=CODE,delta=2
global __ptext143
__ptext143:

;; *************** function _gADCAverageRead *****************
;; Defined at:
;;		line 209 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
;; Parameters:    Size  Location     Type
;;  ADCIn           1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  ADCIn           1   10[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         1       0       0       0       0
;;      Temps:          2       0       0       0       0
;;      Totals:         3       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___lwdiv
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text143
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	line	209
	global	__size_of_gADCAverageRead
	__size_of_gADCAverageRead	equ	__end_of_gADCAverageRead-_gADCAverageRead
	
_gADCAverageRead:	
	opt	stack 6
; Regs used in _gADCAverageRead: [wreg+status,2+status,0+pclath+cstack]
;gADCAverageRead@ADCIn stored from wreg
	line	214
	movwf	(gADCAverageRead@ADCIn)
	
l2867:	
;main.c: 210: static uint16_t uwAggregateCnt;
;main.c: 211: static uint8_t ubDivideByCnt;
;main.c: 212: static uint8_t ubResult;
;main.c: 214: if ( ubDivideByCnt < 6 )
	movlw	(06h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	subwf	(gADCAverageRead@ubDivideByCnt),w
	skipnc
	goto	u2991
	goto	u2990
u2991:
	goto	l2873
u2990:
	line	217
	
l2869:	
;main.c: 215: {
;main.c: 217: uwAggregateCnt += ADCIn;
	movf	(gADCAverageRead@ADCIn),w
	movwf	(??_gADCAverageRead+0)+0
	clrf	(??_gADCAverageRead+0)+0+1
	movf	0+(??_gADCAverageRead+0)+0,w
	addwf	(gADCAverageRead@uwAggregateCnt),f
	skipnc
	incf	(gADCAverageRead@uwAggregateCnt+1),f
	movf	1+(??_gADCAverageRead+0)+0,w
	addwf	(gADCAverageRead@uwAggregateCnt+1),f
	line	220
	
l2871:	
;main.c: 220: ubDivideByCnt++;
	movlw	(01h)
	movwf	(??_gADCAverageRead+0)+0
	movf	(??_gADCAverageRead+0)+0,w
	addwf	(gADCAverageRead@ubDivideByCnt),f
	goto	l2873
	line	224
	
l894:	
	line	225
	
l2873:	
;main.c: 224: }
;main.c: 225: if( 6 == ubDivideByCnt )
	movf	(gADCAverageRead@ubDivideByCnt),w
	xorlw	06h
	skipz
	goto	u3001
	goto	u3000
u3001:
	goto	l2881
u3000:
	line	228
	
l2875:	
;main.c: 226: {
;main.c: 228: ubResult = (uwAggregateCnt / 6);
	movlw	low(06h)
	movwf	(?___lwdiv)
	movlw	high(06h)
	movwf	((?___lwdiv))+1
	movf	(gADCAverageRead@uwAggregateCnt+1),w
	clrf	1+(?___lwdiv)+02h
	addwf	1+(?___lwdiv)+02h
	movf	(gADCAverageRead@uwAggregateCnt),w
	clrf	0+(?___lwdiv)+02h
	addwf	0+(?___lwdiv)+02h

	fcall	___lwdiv
	movf	(0+(?___lwdiv)),w
	movwf	(??_gADCAverageRead+0)+0
	movf	(??_gADCAverageRead+0)+0,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(gADCAverageRead@ubResult)
	line	231
	
l2877:	
;main.c: 231: uwAggregateCnt = 0;
	movlw	low(0)
	movwf	(gADCAverageRead@uwAggregateCnt)
	movlw	high(0)
	movwf	((gADCAverageRead@uwAggregateCnt))+1
	line	234
	
l2879:	
;main.c: 234: ubDivideByCnt = 0;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	movwf	(gADCAverageRead@ubDivideByCnt)
	goto	l2881
	line	235
	
l895:	
	line	236
	
l2881:	
;main.c: 235: }
;main.c: 236: return(ubResult);
	movf	(gADCAverageRead@ubResult),w
	goto	l896
	
l2883:	
	line	237
	
l896:	
	return
	opt stack 0
GLOBAL	__end_of_gADCAverageRead
	__end_of_gADCAverageRead:
;; =============== function _gADCAverageRead ends ============

	signat	_gADCAverageRead,4217
	global	___lwdiv
psect	text144,local,class=CODE,delta=2
global __ptext144
__ptext144:

;; *************** function ___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[COMMON] unsigned int 
;;  dividend        2    2[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    5[COMMON] unsigned int 
;;  counter         1    7[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         4       0       0       0       0
;;      Locals:         3       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:         8       0       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_gADCAverageRead
;; This function uses a non-reentrant model
;;
psect	text144
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.80\sources\lwdiv.c"
	line	5
	global	__size_of___lwdiv
	__size_of___lwdiv	equ	__end_of___lwdiv-___lwdiv
	
___lwdiv:	
	opt	stack 6
; Regs used in ___lwdiv: [wreg+status,2+status,0]
	line	9
	
l2843:	
	movlw	low(0)
	movwf	(___lwdiv@quotient)
	movlw	high(0)
	movwf	((___lwdiv@quotient))+1
	line	10
	movf	(___lwdiv@divisor+1),w
	iorwf	(___lwdiv@divisor),w
	skipnz
	goto	u2921
	goto	u2920
u2921:
	goto	l2863
u2920:
	line	11
	
l2845:	
	clrf	(___lwdiv@counter)
	bsf	status,0
	rlf	(___lwdiv@counter),f
	line	12
	goto	l2851
	
l1043:	
	line	13
	
l2847:	
	movlw	01h
	
u2935:
	clrc
	rlf	(___lwdiv@divisor),f
	rlf	(___lwdiv@divisor+1),f
	addlw	-1
	skipz
	goto	u2935
	line	14
	
l2849:	
	movlw	(01h)
	movwf	(??___lwdiv+0)+0
	movf	(??___lwdiv+0)+0,w
	addwf	(___lwdiv@counter),f
	goto	l2851
	line	15
	
l1042:	
	line	12
	
l2851:	
	btfss	(___lwdiv@divisor+1),(15)&7
	goto	u2941
	goto	u2940
u2941:
	goto	l2847
u2940:
	goto	l2853
	
l1044:	
	goto	l2853
	line	16
	
l1045:	
	line	17
	
l2853:	
	movlw	01h
	
u2955:
	clrc
	rlf	(___lwdiv@quotient),f
	rlf	(___lwdiv@quotient+1),f
	addlw	-1
	skipz
	goto	u2955
	line	18
	movf	(___lwdiv@divisor+1),w
	subwf	(___lwdiv@dividend+1),w
	skipz
	goto	u2965
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),w
u2965:
	skipc
	goto	u2961
	goto	u2960
u2961:
	goto	l2859
u2960:
	line	19
	
l2855:	
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),f
	movf	(___lwdiv@divisor+1),w
	skipc
	decf	(___lwdiv@dividend+1),f
	subwf	(___lwdiv@dividend+1),f
	line	20
	
l2857:	
	bsf	(___lwdiv@quotient)+(0/8),(0)&7
	goto	l2859
	line	21
	
l1046:	
	line	22
	
l2859:	
	movlw	01h
	
u2975:
	clrc
	rrf	(___lwdiv@divisor+1),f
	rrf	(___lwdiv@divisor),f
	addlw	-1
	skipz
	goto	u2975
	line	23
	
l2861:	
	movlw	low(01h)
	subwf	(___lwdiv@counter),f
	btfss	status,2
	goto	u2981
	goto	u2980
u2981:
	goto	l2853
u2980:
	goto	l2863
	
l1047:	
	goto	l2863
	line	24
	
l1041:	
	line	25
	
l2863:	
	movf	(___lwdiv@quotient+1),w
	clrf	(?___lwdiv+1)
	addwf	(?___lwdiv+1)
	movf	(___lwdiv@quotient),w
	clrf	(?___lwdiv)
	addwf	(?___lwdiv)

	goto	l1048
	
l2865:	
	line	26
	
l1048:	
	return
	opt stack 0
GLOBAL	__end_of___lwdiv
	__end_of___lwdiv:
;; =============== function ___lwdiv ends ============

	signat	___lwdiv,8314
	global	_TestCRC
psect	text145,local,class=CODE,delta=2
global __ptext145
__ptext145:

;; *************** function _TestCRC *****************
;; Defined at:
;;		line 185 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
;; Parameters:    Size  Location     Type
;;  ptr             2    0[COMMON] PTR const unsigned short
;;		 -> RAM(512), 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         2       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         2       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text145
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	line	185
	global	__size_of_TestCRC
	__size_of_TestCRC	equ	__end_of_TestCRC-_TestCRC
	
_TestCRC:	
	opt	stack 7
; Regs used in _TestCRC: [wreg+status,2+status,0]
	line	188
	
l2553:	
;main.c: 186: static uint16_t uwTest;
;main.c: 188: uwTest = ptr;
	movf	(TestCRC@ptr+1),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(TestCRC@uwTest+1)
	addwf	(TestCRC@uwTest+1)
	movf	(TestCRC@ptr),w
	clrf	(TestCRC@uwTest)
	addwf	(TestCRC@uwTest)

	line	189
	
l885:	
	return
	opt stack 0
GLOBAL	__end_of_TestCRC
	__end_of_TestCRC:
;; =============== function _TestCRC ends ============

	signat	_TestCRC,4216
	global	_gfGPLD_1_OperatorAlarmSelInit
psect	text146,local,class=CODE,delta=2
global __ptext146
__ptext146:

;; *************** function _gfGPLD_1_OperatorAlarmSelInit *****************
;; Defined at:
;;		line 256 in file "C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text146
	file	"C:\Users\t0028919\Desktop\Flash_Drive_Backup\Removable Disk\pic_projects\SimpleProject\Src\main.c"
	line	256
	global	__size_of_gfGPLD_1_OperatorAlarmSelInit
	__size_of_gfGPLD_1_OperatorAlarmSelInit	equ	__end_of_gfGPLD_1_OperatorAlarmSelInit-_gfGPLD_1_OperatorAlarmSelInit
	
_gfGPLD_1_OperatorAlarmSelInit:	
	opt	stack 7
; Regs used in _gfGPLD_1_OperatorAlarmSelInit: [wreg+status,2+status,0]
	line	260
	
l2551:	
;main.c: 260: ubUseAlarmLockInit = UNLOCK_INIT_STATE;
	clrc
	movlw	0
	btfsc	status,0
	movlw	1
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(_ubUseAlarmLockInit)
	line	262
	
l899:	
	return
	opt stack 0
GLOBAL	__end_of_gfGPLD_1_OperatorAlarmSelInit
	__end_of_gfGPLD_1_OperatorAlarmSelInit:
;; =============== function _gfGPLD_1_OperatorAlarmSelInit ends ============

	signat	_gfGPLD_1_OperatorAlarmSelInit,88
psect	text147,local,class=CODE,delta=2
global __ptext147
__ptext147:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
