#include <string.h>
#include <stdio.h>
#include "htc.h"
#include "typedefs.h"

/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK & CP);//HS code protected);
__CONFIG(BORV40);
/* STRUCTURES */

#define mSHIFT3(x)    ((uint16_t)((x) >> 3))  

#define mROW(x)       ((uint16_t)((x) & 0x0007))
#define mCOLUMN(x)    ((uint16_t)((x) & 0x00F8) >> 3)   
#define mSHIFT(x)     ((uint16_t)((x) & 0x0100)  >>  8)    
#define mALTERNATE(x) ((uint16_t)((x) & 0x0200)  >>  9)
#define mFUNCTION(x)  ((uint16_t)((x) & 0x0400)  >>  10)    
#define mLANGUAGE(x)  ((uint16_t)((x) & 0xF000)  >>  12)    

#define mSETROW(x,y)       ((uint16_t)((x) & 0xFFF8) | (uint8_t)(y) )
#define mSETCOLUMN(x,y)    ((uint16_t)((x) & 0xFF07) | ((uint8_t)(y) << 3) )
#define mSETSHIFT(x,y)     ((uint16_t)((x) & 0xFEFF) | ((uint8_t)(y) << 8) )
#define mSETALTERNATE(x,y) ((uint16_t)((x) & 0xFDFF) | ((uint8_t)(y) << 9) )
#define mSETFUNCTION(x,y)  ((uint16_t)((x) & 0xFBFF) | ((uint8_t)(y) << 10) )
#define mSETLANGUAGE(x,y)  ((uint16_t)((x) & 0x0FFF) | ((uint8_t)(y) << 12) )

/*!
  \defgroup nmtstatecodes NMT state codes
  @{
*/
#define BOOTUP               0 /*!< Signal initial bootup. */
#define STOPPED              4 /*!< Signal stopped state. */
#define OPERATIONAL          5 /*!< Signal operational state. */
#define PRE_OPERATIONAL    127 /*!< Signal pre-operational state. */



/** @def   CO_SCM_RX_PDO_1_MASKBIT
 *  @brief Defines the maskbit associated with the first receive SCM Slave PDO
 */
#define CO_SCM_RX_PDO_1_MASKBIT 0x00000001

/** @def   CO_SCM_RX_PDO_2_MASKBIT
 *  @brief Defines the maskbit associated with the second receive SCM Slave PDO
 */
#define CO_SCM_RX_PDO_2_MASKBIT 0x00000002

#define USR_CLEAR_FOUND_BY_SCANNER    (uint8_t)(~0x02u)
#define USR_CLEAR_NODE_FOUND_ONCE     (uint8_t)(~0x40u)
#define SOIL_SEN_ADC_AVERAGE_DIVISOR 6 
#define  PDO_t_Idx   uint8_t     /* max 255 byte object data */




typedef struct Key_Switches
{
	uint16_t ubHistory[3];
	uint16_t ubPointer;
	uint16_t Switches;
    uint32_t ulFpgamask;
} Key_Switches_t;



const uint8_t MyConstAr[3] = {0x11, 0x22,0x33};
static uint32_t Test;

    
    /*Alarm Timer data  */           
    //#define RESET_ALARM              0u 
    //#define CONTINOUS_ALARM          1u 
    //#define BEEP_BEEP_ALARM          2u 
    //#define BEEP3_ALARM              3u 
    //#define BEEP_ONCE_ALARM          4u 
    //#define BEEP2_ONCE_ALARM         5u 
    //#define BEEP_STATUS_ALARM        6u 
    //#define BEEP_ALERT_ALARM         7u
    //#define BEEP_SLOWDOWN_ALARM      8u 
    //#define ONE_SEC_ALARM            9u 
   //#define TWO_SEC_ALARM           10u 
typedef enum
{
   RESET_ALARM            =  0u,
   CONTINOUS_ALARM        =  1u,  
   BEEP_BEEP_ALARM        =  2u, 
   BEEP3_ALARM            =  3u, 
   BEEP_ONCE_ALARM        =  4u,
   BEEP2_ONCE_ALARM       =  5u,
   BEEP_STATUS_ALARM      =  6u, 
   BEEP_ALERT_ALARM       =  7u, 
   BEEP_SLOWDOWN_ALARM    =  8u,
   ONE_SEC_ALARM          =  9u, 
   TWO_SEC_ALARM          = 10u
}


    typedef struct status
    {
        uint8_t ubAlarmType;       // Control id
        uint8_t ubNumOfCycles;     // Number of cycles between on & off
        uint8_t ubOnTime1;         // Alarm on time length 1
        uint8_t ubOffTime1;        // Alarm off time length 1
        uint8_t ubOnTime2;         // Alarm on time length 2
        uint8_t ubOffTime2;        // Alarm off time length 2
        uint8_t ubPauseTime;       // Pause time if repeating pattern
        uint8_t fCycleRepeat;     // Repeat Cycle
    }ALARM_DEFINITIONS;

    typedef enum ext_alrm_states
    {
        INIT_STATE     = 0,
        T1_STATE       = 1,
        T2_STATE       = 2,
        PAUSE_STATE    = 3,
        REPEAT_STATE   = 4 
    }ext_alrm_states_t; 

    typedef enum lock_statemachine
    {   
        UNLOCK_INIT_STATE       = 0,
        LOCK_INIT_STATE         = 1,  
        CURRENTLY_RUNNING_STATE = 2,
        DONE_STATE              = 3,
        ALLOW_STATE_MACHINE     = 4,  
        DISALLOW_STATE_MACHINE  = 5  
    }lock_statemachine_t; 
   
    const ALARM_DEFINITIONS AlarmTimes[]=
    {   //Alarm Type          Cycle  T1on   T1off  T2on  T2off  TPause  Repeat
        {RESET_ALARM,         1,     0,     0,     0,     1,     0,     0},
        {CONTINOUS_ALARM,     1,     1,     0,     0,     0,     0,     1},
        {BEEP_BEEP_ALARM,     2,     5,     5,     0,     0,    35,     1},
        {BEEP3_ALARM,         3,     5,     5,     0,     0,   100,     1},
        {BEEP_ONCE_ALARM,     1,    50,     0,     0,     0,     0,     0},
        {BEEP2_ONCE_ALARM,    1,     5,     5,     5,     5,     0,     0},
        {BEEP_STATUS_ALARM,   1,     6,    19,    25,   200,     0,     1},
        {BEEP_ALERT_ALARM,    1,     6,    19,    25,    50,     0,     1},
        {BEEP_SLOWDOWN_ALARM, 1,    12,    12,    12,    12,     0,     1},
        {ONE_SEC_ALARM,       1,   100,     0,     0,     0,     0,     0},
        {TWO_SEC_ALARM,       1,   200,     0,     0,     0,     0,     0}
    };

/*!
  This structure holds the actual settings for the module.
*/
typedef struct _vModInfo {
  uint8_t bBoardAdr;    /*!< module's address 1..127      */
  uint8_t  bBaudRate;    /*!< module's baudrate code       */
  uint8_t  bModuleState; /*!< module's state               */
  uint8_t  bCommState;   /*!< module's communication state */
}
vModInfo;

    
/* Function prototypes*/
int mainold (void);                                   // Initializr  the hard ware
uint8_t gfGPLD_1_OperatorAlarmSel(const ALARM_DEFINITIONS *pAlarmObj );
void gfGPLD_1_OperatorAlarmSelInit(void);
void example(uint32_t Val);
void vTemperatureStateMachine(temperature_states_t *eState);
uint8_t gADCAverageRead(uint8_t ADCIn);
void TestCRC(const uint16_t *ptr);
uint32_t gulOS_CalcUnusedStackSize(void *pvStackStart, uint32_t ulStackSize);
void TestConst(void);
void initModuleInfo(void);
/* non writeable ram variable location*/
/* Volatile variable*/

/* Local scope variable*/
Key_Switches_t gKey_Switches; 
uint8_t NewKeySw;
uint16_t HwInput; 
uint16_t UwRowColStat, UwRowColStatMSK, UwRowColpar;
uint8_t MyConstArRcv[3];
uint16_t x = 0;   
temperature_states_t geMot_OT_State;


vModInfo ModuleInfo;
static lock_statemachine_t ubUseAlarmLock; 
static lock_statemachine_t ubUseAlarmLockInit; 

 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/

int main (void)
{
uint8_t SoundID = 2;  // IMM input
uint8_t OutputEn;
uint8_t TestADC;
uint8_t InputVal;
gfGPLD_1_OperatorAlarmSelInit();
uint32_t *pulAddrPtr; 
uint32_t ulValue = 0xFEEDFACE;
uint32_t ulValPtr;
uint32_t Return;
uint8_t Test;

pulAddrPtr = &ulValue;
ulValPtr = *pulAddrPtr;

void *tx_thread_stack_start;

TestCRC((uint16_t *) ulValPtr); //should truncate off upper 16 bit

Test = ~ 0x55;
//Test = ~(Test);
asm("nop");

    /**- This is like an implied time 10 ms in this case */ 
    while(1)
    {  
       //OutputEn = gfGPLD_1_OperatorAlarmSel(&AlarmTimes[SoundID] ); 
      
       TestADC = gADCAverageRead(TestADC);

       // Lets see what Chris function does
       InputVal = 0xEF;
       Test = 0xEF;

       //tx_thread_stack_start = &InputVal;
       Return = gulOS_CalcUnusedStackSize(&Test, 10);

       asm("nop");

       initModuleInfo();
       TestConst();
    }
}                                 

/**
 * @fn      uint32_t gulOS_CalcUnusedStackSize(void *pvStackStart, uint32_t ulStackSize)
 *
 * @brief   This function finds the number of unused bytes in a thread stack
 *
 * @param   pvStackStart = Pointer to the thread stack
 * @param   ulStackSize = Size in bytes of the thread stack
 *
 * @return  Number of unused bytes in the thread stack
 *
 * @author  Chris Graunke
 *
 * @note    N/A
 *
 */
uint32_t gulOS_CalcUnusedStackSize(void *pvStackStart, uint32_t ulStackSize)
{
    uint32_t ulUnusedBytes = 0u;
    uint8_t *pubStackBytes = (uint8_t *)pvStackStart;
    uint8_t StackStartReturned = 0u;


    /** ###Functional overview: */

    /** - Step through stack until the unused pattern byte is not found */
    while ( pubStackBytes[ulUnusedBytes] == (uint8_t)0xEF )
    {
        /** - If the byte is unused increment the counter */
        ulUnusedBytes++;

        /** - Stop if end of stack reached */
        if ( ulUnusedBytes >= ulStackSize )
        {
            break;
        }
    }
    
    /** - Test to see if pointer is returning as expected */
    StackStartReturned = *pubStackBytes;

    /** - Return result to caller */
    return StackStartReturned;//ulUnusedBytes;
}

void TestCRC(const uint16_t *ptr)
{
   static uint16_t uwTest;
   
   uwTest = ptr; //It did truncate this off
}

void initModuleInfo(void)
{
   ModuleInfo.bCommState = OPERATIONAL;
}
  //const uint8_t kfNetworkOperational = ( ModuleInfo.bCommState == OPERATIONAL );
const uint8_t SetVal = 5;
 uint8_t ArLEN = 10;
void TestConst(void)
{
    //const uint8_t ArLEN = 10;
    uint8_t Ar[10] = {0,0,0,0,0,0,0,0,0,0};
   //const uint8_t kfNetworkOperational = (SetVal == 5);//( ModuleInfo.bCommState == OPERATIONAL );

   //kfNetworkOperational = 11;
}




/**
   @fn      gADCAverageRead()

   @brief   Average the Soil sensor readings.

   @param   ADC Channel

   @return  ubTemp = 8 bit ADC averaged read

   @note    TBD.

   @author  Tom Rode

   @date    07/06/2016

*/
uint8_t gADCAverageRead(uint8_t ADCIn)
{
static uint16_t uwAggregateCnt;
static uint8_t  ubDivideByCnt;
static uint8_t  ubResult;




    if ( ubDivideByCnt < SOIL_SEN_ADC_AVERAGE_DIVISOR )
    {
        /** - Get a Summing of Current ADC in */
        uwAggregateCnt += ADCIn;

        /** - Increment the counter */
        ubDivideByCnt++;

        /** - This should be what ever was the last result */ 
        ubResult = ubResult;
    }
    if( SOIL_SEN_ADC_AVERAGE_DIVISOR == ubDivideByCnt )
    {
        /** - Make the average calculation */
        ubResult = (uwAggregateCnt / SOIL_SEN_ADC_AVERAGE_DIVISOR);

        /** - Clear out the running total */ 
        uwAggregateCnt = 0;

        /** - Zero the counter */
        ubDivideByCnt = 0;
    }
return(ubResult);
}



/**
 * @fn      void gfGPLD_1_OperatorAlarmSelInit ( void )
 *
 * @brief   Handles the Timing Sequence for Piezo buzzer
 *  
 * @param   N/A
 *  
 * @return  N/A
 *
 * @author  Tom Rode
 *
 * @note    Set the initialization on start up allowing the IMM to run Statemachine initially 
 *  
 */
void gfGPLD_1_OperatorAlarmSelInit (void)
{
    /** ###Functional overview: */

    /** - Allow for first IMM alarm typr */
    ubUseAlarmLockInit = UNLOCK_INIT_STATE; 

}


/**
 * @fn      uint8_t gfGPLD_1_OperatorAlarmSel (const ALARM_DEFINITIONS *pObj )
 *
 * @brief   Handles the Timing Sequence for Piezo buzzer
 *  
 * @param   *pAlarmObj = Truck parameter for a specific tone sequence 
 *  
 * @return  Buzzer driver to be enabled/disabled
 *
 * @author  Tom Rode
 *
 * @note    N/A 
 *  
 */
uint8_t gfGPLD_1_OperatorAlarmSel (const ALARM_DEFINITIONS *pAlarmObj )
{
    /** - Varaibles from ALARM_DEFINITIONS */ 
    static uint8_t ubAlarmTypePar;       // Control id is SoundI
    static uint8_t ubNumOfCyclesPar;     // Number of cycles between on & off
    static uint8_t ubOnTime1Par;         // Alarm on time length 1
    static uint8_t ubOffTime1Par;        // Alarm off time length 1
    static uint8_t ubOnTime2Par;         // Alarm on time length 2
    static uint8_t ubOffTime2Par;        // Alarm off time length 2
    static uint8_t ubPauseTimePar;       // Pause time if repeating pattern
    static uint8_t fCycleRepeatPar;      // Repeat Cycle

    /** - IMM alrm states */
    static uint8_t ubAlarmTypeCurrent;   // The current Control SoundID
    static uint8_t ubAlarmTypeLast;      // The last Control SoundID
    static uint8_t fAlarmTypeTwoSame;    // Two of the same States 
    static uint8_t ubUseAlarmLock;            

    /** - All of the counters */
    static uint8_t ubOffTimeCnt;
    static uint8_t ubOnTimeCnt;
    static uint8_t ubRepeatCnt;
    static uint8_t ubPauseCnt;
 
    /** - State Machines and Output driver */
    static uint8_t ubAlarmStMch;          // state machine                  
    static uint8_t ubBuzzerState;         // This will be the output

     /** ###Functional overview: */

    /** - Current state IMM placed */
    ubAlarmTypeCurrent = pAlarmObj->ubAlarmType; 

    /** - IMM cannot change the Alarm type until previous cycle complete */
    if ( ubAlarmTypeLast != ubAlarmTypeCurrent )    
    {
       /** - Unlike alarms from IMM received */
       //fAlarmTypeTwoSame = FALSE;   
    }
    else if (ubAlarmTypeLast == ubAlarmTypeCurrent )
    {
       /** - Two of the same Alarm typpes from IMM received */
       //fAlarmTypeTwoSame = TRUE; 
   
       /** - A complete cyle occured or the initialization */
       if ( (DONE_STATE == ubUseAlarmLock) || (UNLOCK_INIT_STATE == ubUseAlarmLockInit))
       {
           /** - Initiialization happened lock it out */
           ubUseAlarmLockInit = LOCK_INIT_STATE;
  
           /** - State machine complete */
           ubUseAlarmLock = ALLOW_STATE_MACHINE;

           /** - Set state machine to beginning */
           ubAlarmStMch = INIT_STATE;

       }
    }
    if ( ALLOW_STATE_MACHINE == ubUseAlarmLock ) 
    {
        /**  - Alarm state Machine */
        switch ( ubAlarmStMch )
        {

            case INIT_STATE: 

                /** - De-reference the pointer for paramter for a given alarm pattern and populate working variables*/ 
                //ubAlarmTypePar   = pAlarmObj->ubAlarmType;
                ubNumOfCyclesPar = pAlarmObj->ubNumOfCycles;
                ubOnTime1Par     = pAlarmObj->ubOnTime1;
                ubOffTime1Par    = pAlarmObj->ubOffTime1;    
                ubOnTime2Par     = pAlarmObj->ubOnTime2; 
                ubOffTime2Par    = pAlarmObj->ubOffTime2;
                ubPauseTimePar   = pAlarmObj->ubPauseTime;
                fCycleRepeatPar  = pAlarmObj->fCycleRepeat;
          
                /** - Use this through the endng of the state machine*/
                //fUseAlarmLock = CURRENTLY_RUNNING_STATE;  
            
                /** - reset the on time count */
                ubOnTimeCnt = 0u; 

                /** - reset the off time count */
                ubOffTimeCnt = 0u; 

                /** - reset the repeat count */
                ubRepeatCnt = 0u;

                /** - reset the pause count */
                ubPauseCnt = 0u;

                /** - Reset the statemachine for T1 */
                ubAlarmStMch = T1_STATE;
 
                break;

            case T1_STATE:
                /** - Look at T1on from incomming parameter*/
                if ( ubOnTimeCnt < ubOnTime1Par )
                {
                    /** - Set the output accordingly */
                    ubBuzzerState = TRUE;  
 
                    /** - Increment the on timer count */
                    ubOnTimeCnt++;

                } 
                /** - Look at T1off from incomming parameter*/  
                else if ( (ubOnTimeCnt >= ubOnTime1Par) && ( ubOffTimeCnt < ubOffTime1Par) ) 
                {
                    /** - Set the output accordingly */
                    ubBuzzerState = FALSE; 

                    /** - Increment the off timer count */
                    ubOffTimeCnt++;
                }
                else
                { 
                    /** - Reset the on timer count */
                    ubOnTimeCnt = 0u;

                    /** - Reset the off timer count */
                    ubOffTimeCnt = 0u; 

                    /** - Transition on to T2 state */
                    ubAlarmStMch = T2_STATE;
                }  
                break;

            case T2_STATE:
                /** - Look at T2on from incomming parameter*/
                if ( ubOnTimeCnt < ubOnTime2Par )
                {
                    /** - Set the output accordingly */
                    ubBuzzerState = TRUE;  
 
                    /** - Increment the on timer count */
                    ubOnTimeCnt++;

                } 
                /** - Look at T2off from incomming parameter*/  
                else if ( (ubOnTimeCnt >= ubOnTime2Par) && ( ubOffTimeCnt < ubOffTime2Par) ) 
                {
                    /** - Set the output accordingly */
                    ubBuzzerState = FALSE; 

                    /** - Increment the off timer count */
                    ubOffTimeCnt++;
                }
                else
                { 
                    /** - Reset the on timer count */
                    ubOnTimeCnt = 0u;

                    /** - Reset the off timer count */
                    ubOffTimeCnt = 0u; 

                    /** - Transition on to Repaeat chech state */
                    ubAlarmStMch = REPEAT_STATE;
                }   
                break;

            case REPEAT_STATE:
                /** - Look at repeat count from incomming parameter*/
                if ( ubRepeatCnt < (ubNumOfCyclesPar - 1u) )
                {
                    /** - Increment the repeat count */
                    ubRepeatCnt++;

                    /** - Remain in this state */
                    ubAlarmStMch = T1_STATE;  
                } 
                else 
                {
                    /** - Reset the repeat count */
                    ubRepeatCnt = 0u;

                    /** - Transition on to Pause state */
                    ubAlarmStMch = PAUSE_STATE; 
                } 
                break;

            case PAUSE_STATE:
               /** - Look at the Pause count from incomming parameter */
               if ( ubPauseCnt < ubPauseTimePar )
               {
                   /** - Increment the pause count */
                   ubPauseCnt++; 

                   /** - Remain in the Pause state */
                   ubAlarmStMch = PAUSE_STATE;
               } 
               else
               {
                   /** - transition to the Init state */
                   ubAlarmStMch = INIT_STATE;  

                   /** - Allow a new alarm state from IMM */
                   ubUseAlarmLock = DONE_STATE;   
               } 
               break;       
        }
    }        
    /** - Record the last IMM alarm state before exiting function */
    ubAlarmTypeLast = ubAlarmTypeCurrent; 

    /** - Return Driver output state */
    return(ubBuzzerState);
}



int mainold (void)
{
uint8_t UbRetVal;
uint8_t UbExitVal;
static uint32_t uwColumnval;
uint8_t ubMask = 0x0001;
uint8_t UbShiftIndex;
unsigned int place = 0xCAFE;
uint8_t pubInst[2] = {0xAA, 0xBB};
uint8_t pubPtr;
/** used for key lo to hi*/
static uint8_t ubRowSwTransition[6u];
static uint8_t ubOldValue[6u];
uint8_t Row;
uint8_t ubKeySel;
uint16_t uwSwitches[6];
uint8_t CheckTransition;

uint16_t Test2 = 0x02;
uint32_t TestTruncation = 0x12345678;
char  *ptr;
ptr  = (char *)0;
 
uint8_t b_flags;
static uint8_t ubPot2Transmit;
uint8_t NegNum8bit = (PDO_t_Idx)(-1);
uint32_t NegNum32bit = sizeof( Key_Switches_t);
uint16_t uwDitherInhibit = 0x00f7;
uint16_t uwChan;
uint8_t fChanDitherInhibit;

uint8_t val = 254;

uint8_t i;
uint8_t ubOnesOut=0;
uint8_t ubTensCnt=0;
uint8_t ubTensAmt=0;
uint8_t ubTensOut=0;
uint8_t ubHundCnt=0;
uint8_t ubHundAmt=0;
uint8_t ubHundOut=0;

    for(i=0; i<val; i++)
    {
        if(ubTensCnt >= 10)
        {
            /** Tally up the amount of tens*/
            ubTensAmt++;

            /** Reset tens count*/
            ubTensCnt=1;
        }
        else
        {
            /** increment tens count*/
            ubTensCnt++;
        } 

        if(ubHundCnt >= 100)
        {
            /** Tally up the amount of Hundred*/
            ubHundAmt++;

            /** Reset hundred count*/
            ubHundCnt=1;
        }
        else
        {
           /** increment hundred count*/
           ubHundCnt++;
        } 

         /** Count Correction for output*/
         ubHundOut =  ubHundAmt; 
         ubTensOut = (ubHundAmt * 10);
         ubTensOut = (ubTensAmt - ubTensOut);
         ubOnesOut = (val - ((ubHundAmt * 100) + (ubTensOut * 10)));
        /** - ASCII Encoded */
         ubHundOut += 0x30;
         ubTensOut += 0x30;
         ubOnesOut += 0x30;

    }

fChanDitherInhibit = (uwDitherInhibit & (1 << uwChan));

fChanDitherInhibit = (fChanDitherInhibit >> uwChan); 

/** geMot_OT_State Should change after pointer asignment*/ 
geMot_OT_State = TEMP_STATE_OK;
vTemperatureStateMachine(&geMot_OT_State);

Test2 = (20 % 8);

Key_Switches_t *pOut;
Key_Switches_t exptr;

Test |= CO_SCM_RX_PDO_1_MASKBIT;
Test |= CO_SCM_RX_PDO_2_MASKBIT;
Test |= 0x00000100;
pOut = &exptr;
pOut->ulFpgamask = 0x33331234;

pOut->ulFpgamask &= (0x00001234);


pubPtr = pubInst[1];

pubInst[0] = 0xCC;
if( ptr == 0)
{
  asm("nop");
}


if(sizeof( Key_Switches_t) > (PDO_t_Idx)(-1))
{
  asm("nop");
}



//memset(&MyConstArRcv[0],&MyConstAr[0],3);
memcpy(MyConstArRcv,MyConstAr,sizeof(MyConstAr)); 


b_flags  &= /*(uint8_t)*/(USR_CLEAR_FOUND_BY_SCANNER & USR_CLEAR_NODE_FOUND_ONCE);
Test = ( 0xF2 ^ Test2);


example(TestTruncation);

 /* see if ColRow in limits */
 UwRowColStatMSK = 0xE686; 
 /* current COLUMN stat*/
 UwRowColStat = mCOLUMN( UwRowColStatMSK );
 /* current ROW stat*/
 UwRowColStat = mROW( UwRowColStatMSK );
 /* current FUNCTION stat*/
 UwRowColStat = mFUNCTION( UwRowColStatMSK );
 /* current ALTERNATE stat*/
 UwRowColStat = mALTERNATE( UwRowColStatMSK );
 /* current ALTERNATE stat*/
 UwRowColStat = mLANGUAGE( UwRowColStatMSK );
 /* set the ROW */
 UwRowColStat = mSETROW(UwRowColStat, 7);
 /* set column */
 UwRowColStat = mSETCOLUMN(UwRowColStat, 26);
 /* set shift */
 UwRowColStat = mSETSHIFT(UwRowColStat, 0);
 /* set alt */
 UwRowColStat = mSETALTERNATE(UwRowColStat, 1);
 /* set function */
 UwRowColStat = mSETFUNCTION(UwRowColStat, 1);
/* set function */
 UwRowColStat = mSETLANGUAGE(UwRowColStat, 9);
 /* parameter is 11 */
 UwRowColpar = 0x0B;

while(1)
{
    //if ( (ubPot2Transmit++ & 0x01) == TRUE )
    if ( ++ubPot2Transmit & 0x01 )
    {
        b_flags++;
    }
           
}

while(1)
{

     /** used for key hi to lo*/
     /** ###Functional overview: */

     // PSEUDO CODE 
     /** - Place the switch value into respective Row element */
     asm("nop");
     ubRowSwTransition[Row] = CheckTransition;
      
     /** - See if there is a value */
     if ( ubRowSwTransition[Row] > 0u ) 
     {
         /** - Determine what the value is and store it for next iteration*/ 
         ubOldValue[Row] = ubRowSwTransition[Row];

         /** - Output zero */
         ubKeySel = 0; 
     }
     /** Hi to Lo transition occured in a particular Row, see if de-bounced masked value low transition occured */ 
     else if( ( ubRowSwTransition[Row] != ubOldValue[Row] ) && ( 0u == ( uwSwitches[Row] & 1u << ( ubOldValue[Row] - 1))) ) 
     {
         /** - Output switch on falling edge */ 
         ubKeySel = ubOldValue[Row];  

         /** - Clear out the buffer */  
         ubOldValue[Row] = 0u;          
     }
     else
     {
         /** - Output zero */
         ubKeySel = 0;
     }
     asm("nop");

}
  while(1)
  {   
    asm("nop");
    asm("nop");
    asm("nop");
    asm("nop");
    UbExitVal = 22; // column number of switches

    for(UbShiftIndex = 1; UbShiftIndex <= UbExitVal; UbShiftIndex++)
    {
          
         if( 0 == uwColumnval)
         {
             /** - There is no Column switch hit */
             UbRetVal = 0u;
             
              /** - Make to exit loop */
             UbExitVal = 0;
         }
         /** - Run through mask to determine column key hit */
         else if( uwColumnval == ubMask)
         {
             /** - Return index which correlates to column key hit stop for loop*/
             UbRetVal = UbShiftIndex;
  
             /** - Make to exit loop */
             UbExitVal = 0;
         } 
         /** - Right shift one until value and mask equal */
         else
         {
             uwColumnval = uwColumnval >> 1;
      
         }
    }
    asm("nop");
    asm("nop");
    asm("nop");
    asm("nop");


  }

  while(1)
  {
   gKey_Switches.ubHistory[gKey_Switches.ubPointer] = HwInput;

   /*Is 1 ms? */

    /** - Capture three same state readings */
   if ((gKey_Switches.ubHistory[0] == gKey_Switches.ubHistory[1]) &&
   	   (gKey_Switches.ubHistory[1] == gKey_Switches.ubHistory[2]))
   	{
	   gKey_Switches.Switches = gKey_Switches.ubHistory[0];
   	}

    gKey_Switches.ubPointer++;
   	if (gKey_Switches.ubPointer > 2)
   	{
   		gKey_Switches.ubPointer = 0;
   	}
    asm("nop");
  }
}
/********************************************************
*@fn vTemperatureStateMachine(temperature_states_t *eState)
*
*********************************************************/
void vTemperatureStateMachine(temperature_states_t *eState)
{
    /** Change the pointer value*/
    *eState = TEMP_STATE_CUTBACK;

}



void example(uint32_t Val)
{
 static uint16_t CutVal;

 //CutVal = (uint16_t) Val; 
 CutVal = (uint16_t)((Val >> 16) & 0x00FF);

}