/************************************************************************************************
 *  File:       Alarm.h
 *
 *  Purpose:    Defines the Alarm manager settings
 *
 *  Project:    C584
 *
 *  Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     David J. Obringer
 *
 *  Revision History:
 *              Written 6/17/2004
 *
 ************************************************************************************************
*/
#include "main.h"
#ifndef alarm_registration
    /******************************************************************
      *                                                               *
      *  G l o b a l   S c o p e   S e c t i o n                      *
      *                                                               *
      *****************************************************************/
    
    // Defines
    #define ALARM_SERVICE_UPDATE_MS 10
    #define STARTUP_ALARM_TIMEOUT   500/ALARM_SERVICE_UPDATE_MS
    
    /* Alarm Control id's in order listed in Alarm_data[], High Priority to lowest bit */
    #define AID_IDLE_ALARM           0 
    #define POWER_ON                 1  
    #define AID_FAULT_INDICATION     2  
    #define AID_MISTAKE_ICON         3  
    #define PM_DUE                   4  
    #define AID_SRO_MESSAGE          5  
    #define BATTERY_RESTRANT_MSG     6  
    #define TOW_MODE                 7  
    #define AID_500MSEC_BEEP         8  
    #define AID_STEER_ALERT          9  
    #define AID_STEER_WARNING       10  
    #define AID_ONESEC_BEEP         11  
    #define AID_TWOSEC_BEEP         12  
    #define AID_GUIDE_SLOWDOWN      13  
    #define AID_MANUAL_STEER        14  
    #define NUM_OF_ALARM_IDS        15
    #define ALARM_AID_ARRAY_SIZE (((NUM_OF_ALARM_IDS-1)/(BITS_PER_BYTE))+1)
    #define AID_VOID                NUM_OF_ALARM_IDS
                                     
    /*Alarm Timer data  */           
    #define RESET_ALARM              0 
    #define CONTINOUS_ALARM          1 
    #define BEEP_BEEP_ALARM          2 
    #define BEEP3_ALARM              3 
    #define BEEP_ONCE_ALARM          4 
    #define BEEP2_ONCE_ALARM         5 
    #define BEEP_STATUS_ALARM        6 
    #define BEEP_ALERT_ALARM         7 
    #define BEEP_SLOWDOWN_ALARM      8 
    #define ONE_SEC_ALARM            9 
    #define TWO_SEC_ALARM           10 
    
    #define ALARM_REQUEST_IDLE      0x00
    #define ALARM_REQUEST_PENDING   0x01
    #define ALARM_REQUEST_MADE      0x02

    #define ALARM_DRV_OFF   0
    #define ALARM_DRV_ON    (!ALARM_DRV_OFF) 
    #define CCU6_TRAP_STATE_BIT     0x0800
    
    /* Types */
    typedef struct alm_data
    {
        ubyte ubControlId;
        ubyte ubAlarmTypeRef;
    }
    ALARM_DATA_TYPE;
    
    typedef struct status
    {
        ubyte ubAlarmType;       // Control id
        ubyte ubNumOfCycles;     // Number of cycles between on & off
        ubyte ubOnTime1;         // Alarm on time length 1
        ubyte ubOffTime1;        // Alarm off time length 1
        ubyte ubOnTime2;         // Alarm on time length 2
        ubyte ubOffTime2;        // Alarm off time length 2
        ubyte ubPauseTime;       // Pause time if repeating pattern
        boolean fCycleRepeat;    // Repeat Cycle
    }ALARM_DEFINITIONS;

    typedef enum
    {
        ST_EXT_ALARM_INIT       =  0,
        ST_EXT_ALARM_OFF        =  1,
        ST_EXT_ALARM_ON         =  2,
        ST_EXT_ALARM_FAULT      =  3,
        ST_EXT_ALARM_LOG_FAULT  =  4,
        ST_EXT_ALARM_LATCH_OFF  =  5,
    } EXT_ALARM_STATES;

#endif
#ifdef ALARM_MODULE
     #ifndef alarm_registration_local
     #define alarm_registration_local 1
     /*******************************************************************************
      *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Definitions
    #define gvSetAcm2DispAlmBit()   (P0H_P7=1)
    #define gvClrAcm2DispAlmBit()   (P0H_P7=0)
    /*External Display Bits*/
    #define vResetAlarmDriver()     (P1L_P2=0)
    #define gfExtAlarmOn()          (fToBoolean(fExtAlarmDrvOn == ALARM_DRV_ON))
    #define gfExtAlarmFault()       (fToBoolean(CCU6_IS & CCU6_TRAP_STATE_BIT))
               //Time base = 10 mSec
    const ALARM_DEFINITIONS AlarmTimes[]=
    {   //Alarm Type       Cycle  T1on   T1off  T2on  T2off  TPause  Repeat
        {RESET_ALARM,         1,     0,     0,     0,     1,     0,     0},
        {CONTINOUS_ALARM,     1,     1,     0,     0,     0,     0,     1},
        {BEEP_BEEP_ALARM,     2,     5,     5,     0,     0,    35,     1},
        {BEEP3_ALARM,         3,     5,     5,     0,     0,   100,     1},
        {BEEP_ONCE_ALARM,     1,    50,     0,     0,     0,     0,     0},
        {BEEP2_ONCE_ALARM,    1,     5,     5,     5,     5,     0,     0},
        {BEEP_STATUS_ALARM,   1,     6,    19,    25,   200,     0,     1},
        {BEEP_ALERT_ALARM,    1,     6,    19,    25,    50,     0,     1},
        {BEEP_SLOWDOWN_ALARM, 1,    12,    12,    12,    12,     0,     1},
        {ONE_SEC_ALARM,       1,   100,     0,     0,     0,     0,     0},
        {TWO_SEC_ALARM,       1,   200,     0,     0,     0,     0,     0}
    };
    
    const ALARM_DATA_TYPE AlarmCtrlData[]=
    {           //Prioritized
        {AID_IDLE_ALARM,            RESET_ALARM},      //Never will occur Mask is = 0x0000
        {POWER_ON,                  CONTINOUS_ALARM},
        {AID_FAULT_INDICATION,      BEEP_ONCE_ALARM},
        {AID_MISTAKE_ICON,          BEEP_ONCE_ALARM},
        {PM_DUE,                    CONTINOUS_ALARM},
        {AID_SRO_MESSAGE,           BEEP_BEEP_ALARM},
        {BATTERY_RESTRANT_MSG,      BEEP_BEEP_ALARM},
        {TOW_MODE,                  BEEP_BEEP_ALARM},
        {AID_500MSEC_BEEP,          BEEP_ONCE_ALARM},
        {AID_STEER_ALERT,           BEEP_ALERT_ALARM},
        {AID_STEER_WARNING,         CONTINOUS_ALARM},
        {AID_ONESEC_BEEP,           ONE_SEC_ALARM},
        {AID_TWOSEC_BEEP,           TWO_SEC_ALARM},
        {AID_GUIDE_SLOWDOWN,        BEEP_SLOWDOWN_ALARM},
        {AID_MANUAL_STEER,          BEEP_STATUS_ALARM}
    };
    
    // Function prototypes
    void vAlarmRequest(ubyte ubControl_Id, boolean fRequest);
    void vUpdateAlarm(void);
    void vInitAlarmControl(void);
    ubyte ubGetAlarmAID(void);
    void vExtAlarmService(void);
    void vAlarmOut(boolean fAlarmDrvReq);
    
    // Global Variables
    ALARM_DEFINITIONS far gAlarmData;
    uword far guwAlarmReqMask;
    uword far guwAlarmActiveMask;
    uword far uwAlarmStartupTimer = STARTUP_ALARM_TIMEOUT;
    ubyte far ubAlarmAIDStat[ALARM_AID_ARRAY_SIZE];
    ubyte far ubAlarmLastPass = 0;
    ubyte far ubAlarmServState = ST_EXT_ALARM_INIT;
    boolean fExtAlarmDrvOn = ALARM_DRV_OFF;

    #endif
#else
    #ifndef alarm_registration
    #define alarm_registration 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Externaly visable function prototypes
    extern void vAlarmRequest(ubyte ubControl_Id, boolean fRequest);
    extern void vUpdateAlarm(void);
    extern void vInitAlarmControl(void);
    extern void vServiceStartupAlarm(void);
    extern void vExtAlarmService(void);
    
    // Externally visable global variables

    #endif
#endif


