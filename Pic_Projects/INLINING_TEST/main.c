#include "htc.h"

/********************************************************************************************************
/*
/*  TRYING THE INLINE PRAGMA
/*
/********************************************************************************************************/
/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);
/* STRUCTURES */
/*NONE*/
/* Constants Arrays*/
char block01[10];
char block02[20];
int return_proc;
/* Function prototypes*/
void main (void);
int checksum(char *data, int length);
int proc_block01(void);


int checksum(char *data, int length)
{
int res;
int i;

  res = 0;
  for(i = 0; i <=length; i++)
  {
    res += (int)*data;
  }		

 return (res & 0x00ff); 	
}	

#pragma inline(checksum)

int proc_block01(void)
{
int temp;	
		
 temp = checksum(block01,10);
 return(temp);
}


/*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/

void main (void)
{
//int return_proc;
int test_switch;
int var;
int count;
count = 0;
test_switch = 1;
#pragma switch direct     /* all switch transitions are the same time*/
while(1)
 {
   switch(test_switch)
    {
      case 1:

        //++count;
       #asm
           nop
           MOVLW 0x1
           ADDWF 0x70,F 
           nop
       #endasm

       var = 1;
       var = 99;
       var = 100;
       var = 1;
       test_switch = 2;
      break;
   
      case 2:
       var = 2;
       test_switch = 3;
      break;

      case 3:
       var = 3;
       test_switch = 1;  /* loop forever*/
      break;

      default:
        break;
    } 

 }
//return_proc = proc_block01();	
}