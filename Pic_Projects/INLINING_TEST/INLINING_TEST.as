opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 6738"

opt pagewidth 120

	opt lm

	processor	16F887
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 9 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 9 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
	dw 0x1FFF & 0x3FFF & 0x3FFF & 0x3BFF & 0x3EFF & 0x3FFF & 0x3FFF & 0x3FFF & 0x3FEF & 0x3FF7 & 0x3FFD ;#
# 10 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 10 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
	dw 0x3FFF ;#
	FNROOT	_main
	global	_block02
	global	_return_proc
	global	_block01
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:
	file	"INLINING_TEST.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_return_proc:
       ds      2

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_block02:
       ds      20

_block01:
       ds      10

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR containing the base address, and
;	W with the last address+1
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf		;clear RAM location pointed to by FSR
	incf	fsr,f		;increment pointer
	xorwf	fsr,w		;XOR with final address
	btfsc	status,2	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
	xorwf	fsr,w		;XOR again to restore value
	goto	clrloop		;do the next byte

; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	bcf	status, 7	;select IRP bank0
	movlw	low(__pbssBANK0)
	movwf	fsr
	movlw	low((__pbssBANK0)+01Eh)
	fcall	clear_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	??_main
??_main:	; 0 bytes @ 0x0
	global	main@count
main@count:	; 2 bytes @ 0x0
	ds	2
	global	main@var
main@var:	; 2 bytes @ 0x2
	ds	2
	global	main@test_switch
main@test_switch:	; 2 bytes @ 0x4
	ds	2
;;Data sizes: Strings 0, constant 0, data 0, bss 32, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14      6       8
;; BANK0           80      0      30
;; BANK1           80      0       0
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:



;;
;; Critical Paths under _main in COMMON
;;
;;   None.
;;
;; Critical Paths under _main in BANK0
;;
;;   None.
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 6     6      0      15
;;                                              0 COMMON     6     6      0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 0
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      6       8       1       57.1%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       0       2        0.0%
;;ABS                  0      0      26       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50      0      1E       5       37.5%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       0       7        0.0%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      26      12        0.0%

	global	_main
psect	maintext

;; *************** function _main *****************
;; Defined at:
;;		line 53 in file "d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  test_switch     2    4[COMMON] int 
;;  var             2    2[COMMON] int 
;;  count           2    0[COMMON] int 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         6       0       0       0       0
;;      Temps:          0       0       0       0       0
;;      Totals:         6       0       0       0       0
;;Total ram usage:        6 bytes
;; This function calls:
;;		Nothing
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
	line	53
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 8
; Regs used in _main: [wreg-fsr0h+status,2+status,0]
	line	58
	
l1248:	
;main.c: 55: int test_switch;
;main.c: 56: int var;
;main.c: 57: int count;
;main.c: 58: count = 0;
	movlw	low(0)
	movwf	(main@count)
	movlw	high(0)
	movwf	((main@count))+1
	line	59
;main.c: 59: test_switch = 1;
	movlw	low(01h)
	movwf	(main@test_switch)
	movlw	high(01h)
	movwf	((main@test_switch))+1
	goto	l1258
	line	61
;main.c: 61: while(1)
	
l850:	
	line	63
;main.c: 62: {
;main.c: 63: switch(test_switch)
	goto	l1258
	line	65
;main.c: 64: {
;main.c: 65: case 1:
	
l852:	
	line	69
# 69 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
           nop ;#
	line	70
# 70 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
           MOVLW 0x1 ;#
	line	71
# 71 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
           ADDWF 0x70,F ;#
	line	72
# 72 "d:\users\f53368a\Desktop\Various Documents\pic_projects\INLINING_TEST\main.c"
           nop ;#
psect	maintext
	line	75
	
l1250:	
;main.c: 75: var = 1;
	movlw	low(01h)
	movwf	(main@var)
	movlw	high(01h)
	movwf	((main@var))+1
	line	76
;main.c: 76: var = 99;
	movlw	low(063h)
	movwf	(main@var)
	movlw	high(063h)
	movwf	((main@var))+1
	line	77
;main.c: 77: var = 100;
	movlw	low(064h)
	movwf	(main@var)
	movlw	high(064h)
	movwf	((main@var))+1
	line	78
;main.c: 78: var = 1;
	movlw	low(01h)
	movwf	(main@var)
	movlw	high(01h)
	movwf	((main@var))+1
	line	79
;main.c: 79: test_switch = 2;
	movlw	low(02h)
	movwf	(main@test_switch)
	movlw	high(02h)
	movwf	((main@test_switch))+1
	line	80
;main.c: 80: break;
	goto	l1258
	line	82
;main.c: 82: case 2:
	
l854:	
	line	83
	
l1252:	
;main.c: 83: var = 2;
	movlw	low(02h)
	movwf	(main@var)
	movlw	high(02h)
	movwf	((main@var))+1
	line	84
;main.c: 84: test_switch = 3;
	movlw	low(03h)
	movwf	(main@test_switch)
	movlw	high(03h)
	movwf	((main@test_switch))+1
	line	85
;main.c: 85: break;
	goto	l1258
	line	87
;main.c: 87: case 3:
	
l855:	
	line	88
	
l1254:	
;main.c: 88: var = 3;
	movlw	low(03h)
	movwf	(main@var)
	movlw	high(03h)
	movwf	((main@var))+1
	line	89
;main.c: 89: test_switch = 1;
	movlw	low(01h)
	movwf	(main@test_switch)
	movlw	high(01h)
	movwf	((main@test_switch))+1
	line	90
;main.c: 90: break;
	goto	l1258
	line	92
;main.c: 92: default:
	
l856:	
	line	93
;main.c: 93: break;
	goto	l1258
	line	94
	
l1256:	
;main.c: 94: }
	goto	l1258
	line	63
	
l851:	
	
l1258:	
	; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 1 sub-switches
; Switch size 1, requested type "direct"
; Number of cases is 1, Range of values is 0 to 0
; switch strategies available:
; Name         Bytes Cycles
; simple_byte     4     3 (average)
; direct_byte    22    19 (fixed)
;	Chosen strategy is direct_byte

	movf (main@test_switch+1),w
	movwf fsr
	movlw	1
	subwf	fsr,w
skipnc
goto l1258
movlw high(S2082)
movwf pclath
	clrc
	rlf fsr,w
	addwf fsr,w
	addlw low(S2082)
	movwf pc
psect	swtext1,local,class=CONST,delta=2
global __pswtext1
__pswtext1:
S2082:
	ljmp	l2080
psect	maintext
	
l2080:	
; Switch size 1, requested type "direct"
; Number of cases is 3, Range of values is 1 to 3
; switch strategies available:
; Name         Bytes Cycles
; simple_byte    10     6 (average)
; direct_byte    31    22 (fixed)
;	Chosen strategy is direct_byte

	movf (main@test_switch),w
	addlw	-1
	skipc
goto l1258
	movwf fsr
	movlw	3
	subwf	fsr,w
skipnc
goto l1258
movlw high(S2084)
movwf pclath
	clrc
	rlf fsr,w
	addwf fsr,w
	addlw low(S2084)
	movwf pc
psect	swtext2,local,class=CONST,delta=2
global __pswtext2
__pswtext2:
S2084:
	ljmp	l852
	ljmp	l1252
	ljmp	l1254
psect	maintext

	line	94
	
l853:	
	goto	l1258
	line	96
	
l857:	
	line	61
	goto	l1258
	
l858:	
	line	98
	
l859:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
psect	maintext
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
