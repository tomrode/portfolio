opt subtitle "HI-TECH Software Omniscient Code Generator (Lite mode) build 9453"

opt pagewidth 120

	opt lm

	processor	16F877A
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
pc	equ	2
pcl	equ	2
status	equ	3
fsr	equ	4
fsr0	equ	4
c	equ	1
z	equ	0
pclath	equ	10
# 37 "C:\Users\Tom\Documents\pic_projects\PID\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 37 "C:\Users\Tom\Documents\pic_projects\PID\main.c"
	dw 0xFFFB ;#
	FNCALL	_main,_PIDcal
	FNCALL	_PIDcal,___lmul
	FNROOT	_main
	global	_actual_position_ar
psect	strings,class=STRING,delta=2
global __pstrings
__pstrings:
;	global	stringdir,stringtab,__stringbase
stringtab:
;	String table - string pointers are 1 byte each
stringcode:stringdir:
movlw high(stringdir)
movwf pclath
movf fsr,w
incf fsr
	addwf pc
__stringbase:
	retlw	0
psect	strings
	file	"C:\Users\Tom\Documents\pic_projects\PID\main.c"
	line	31
_actual_position_ar:
	retlw	0
	retlw	0

	retlw	053h
	retlw	0

	retlw	0
	retlw	0

	retlw	042h
	retlw	0

	retlw	042h
	retlw	0

	retlw	042h
	retlw	0

	retlw	042h
	retlw	0

	retlw	043h
	retlw	0

	retlw	043h
	retlw	0

	retlw	043h
	retlw	0

	retlw	043h
	retlw	0

	retlw	043h
	retlw	0

	retlw	043h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	044h
	retlw	0

	retlw	0
	retlw	0

	global	_actual_position_ar
	global	PIDcal@i_term
	global	_FLAGS_STATE
	global	PIDcal@pre_error
	global	_CARRY
_CARRY	set	24
	global	_GIE
_GIE	set	95
	global	_EEADR
_EEADR	set	269
	global	_EEDATA
_EEDATA	set	268
	global	_EECON1
_EECON1	set	396
	global	_EECON2
_EECON2	set	397
	global	_RD
_RD	set	3168
	global	_WR
_WR	set	3169
	global	_WREN
_WREN	set	3170
	file	"PID.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
PIDcal@i_term:
       ds      4

_FLAGS_STATE:
       ds      1

psect	bssBANK1,class=BANK1,space=1
global __pbssBANK1
__pbssBANK1:
PIDcal@pre_error:
       ds      2

; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	clrf	((__pbssBANK0)+0)&07Fh
	clrf	((__pbssBANK0)+1)&07Fh
	clrf	((__pbssBANK0)+2)&07Fh
	clrf	((__pbssBANK0)+3)&07Fh
	clrf	((__pbssBANK0)+4)&07Fh
; Clear objects allocated to BANK1
psect cinit,class=CODE,delta=2
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	((__pbssBANK1)+0)&07Fh
	clrf	((__pbssBANK1)+1)&07Fh
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
clrf status
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?___lmul
?___lmul:	; 4 bytes @ 0x0
	global	___lmul@multiplier
___lmul@multiplier:	; 4 bytes @ 0x0
	ds	4
	global	___lmul@multiplicand
___lmul@multiplicand:	; 4 bytes @ 0x4
	ds	4
	global	??___lmul
??___lmul:	; 0 bytes @ 0x8
	ds	1
	global	___lmul@product
___lmul@product:	; 4 bytes @ 0x9
	ds	4
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	?_PIDcal
?_PIDcal:	; 2 bytes @ 0x0
	global	PIDcal@actual_position
PIDcal@actual_position:	; 1 bytes @ 0x0
	ds	2
	global	??_PIDcal
??_PIDcal:	; 0 bytes @ 0x2
	ds	8
	global	PIDcal@p_term
PIDcal@p_term:	; 4 bytes @ 0xA
	ds	4
	global	PIDcal@d_term
PIDcal@d_term:	; 4 bytes @ 0xE
	ds	4
	global	PIDcal@setpoint
PIDcal@setpoint:	; 1 bytes @ 0x12
	ds	1
	global	PIDcal@output
PIDcal@output:	; 4 bytes @ 0x13
	ds	4
	global	PIDcal@error
PIDcal@error:	; 2 bytes @ 0x17
	ds	2
	global	??_main
??_main:	; 0 bytes @ 0x19
	ds	4
	global	main@COPY_VIN_TEST
main@COPY_VIN_TEST:	; 16 bytes @ 0x1D
	ds	16
	global	main@max
main@max:	; 2 bytes @ 0x2D
	ds	2
	global	main@union32
main@union32:	; 4 bytes @ 0x2F
	ds	4
	global	main@result
main@result:	; 2 bytes @ 0x33
	ds	2
	global	main@test_actual_position_ar
main@test_actual_position_ar:	; 1 bytes @ 0x35
	ds	1
	global	main@ptr
main@ptr:	; 1 bytes @ 0x36
	ds	1
	global	main@setpoint
main@setpoint:	; 1 bytes @ 0x37
	ds	1
	global	main@actual_position
main@actual_position:	; 1 bytes @ 0x38
	ds	1
	global	main@i
main@i:	; 1 bytes @ 0x39
	ds	1
	global	main@TEST_VIN
main@TEST_VIN:	; 16 bytes @ 0x3A
	ds	16
;;Data sizes: Strings 0, constant 52, data 0, bss 7, persistent 0 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     13      13
;; BANK0           80     74      79
;; BANK1           80      0       2
;; BANK3           96      0       0
;; BANK2           96      0       0

;;
;; Pointer list with targets:

;; ?___lmul	unsigned long  size(1) Largest target is 0
;;
;; ?_PIDcal	unsigned short  size(1) Largest target is 2
;;		 -> PIDcal@error(BANK0[2]), 
;;
;; main@ptr	PTR struct . size(1) Largest target is 16
;;		 -> main@TEST_VIN(BANK0[16]), 
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _PIDcal->___lmul
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_PIDcal
;;
;; Critical Paths under _main in BANK1
;;
;;   None.
;;
;; Critical Paths under _main in BANK3
;;
;;   None.
;;
;; Critical Paths under _main in BANK2
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 4, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                52    52      0     790
;;                                             25 BANK0     49    49      0
;;                             _PIDcal
;; ---------------------------------------------------------------------------------
;; (1) _PIDcal                                              25    23      2     340
;;                                              0 BANK0     25    23      2
;;                             ___lmul
;; ---------------------------------------------------------------------------------
;; (2) ___lmul                                              13     5      8      92
;;                                              0 COMMON    13     5      8
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _PIDcal
;;     ___lmul
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BITCOMMON            E      0       0       0        0.0%
;;EEDATA             100      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;COMMON               E      D       D       1       92.9%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;STACK                0      0       2       2        0.0%
;;ABS                  0      0      5E       3        0.0%
;;BITBANK0            50      0       0       4        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;BANK0               50     4A      4F       5       98.8%
;;BITSFR2              0      0       0       5        0.0%
;;SFR2                 0      0       0       5        0.0%
;;BITBANK1            50      0       0       6        0.0%
;;BANK1               50      0       2       7        2.5%
;;BITBANK3            60      0       0       8        0.0%
;;BANK3               60      0       0       9        0.0%
;;BITBANK2            60      0       0      10        0.0%
;;BANK2               60      0       0      11        0.0%
;;DATA                 0      0      60      12        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 44 in file "C:\Users\Tom\Documents\pic_projects\PID\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  TEST_VIN       16   58[BANK0 ] struct .
;;  COPY_VIN_TES   16   29[BANK0 ] struct .
;;  union32         4   47[BANK0 ] unsigned long 
;;  result          2   51[BANK0 ] unsigned short 
;;  max             2   45[BANK0 ] short 
;;  union16         2    0        unsigned short 
;;  i               1   57[BANK0 ] unsigned char 
;;  actual_posit    1   56[BANK0 ] unsigned char 
;;  setpoint        1   55[BANK0 ] unsigned char 
;;  ptr             1   54[BANK0 ] PTR struct .
;;		 -> main@TEST_VIN(16), 
;;  test_actual_    1   53[BANK0 ] unsigned char 
;;  union8          1    0        unsigned char 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       0       0       0       0
;;      Locals:         0      45       0       0       0
;;      Temps:          0       4       0       0       0
;;      Totals:         0      49       0       0       0
;;Total ram usage:       49 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_PIDcal
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\Users\Tom\Documents\pic_projects\PID\main.c"
	line	44
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 6
; Regs used in _main: [wreg-fsr0h+status,2+status,0+btemp+1+pclath+cstack]
	line	60
	
l2881:	
;main.c: 46: VIN TEST_VIN;
;main.c: 47: VIN COPY_VIN_TEST;
;main.c: 48: VIN *ptr;
;main.c: 50: sint16_t max;
;main.c: 51: uint8_t setpoint;
;main.c: 52: uint8_t actual_position;
;main.c: 53: uint16_t result;
;main.c: 54: uint8_t union8;
;main.c: 55: uint8_t i;
;main.c: 56: uint16_t union16;
;main.c: 57: uint32_t union32;
;main.c: 58: uint8_t test_actual_position_ar;
;main.c: 60: FLAGS_STATE.flag1 = FALSE;
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	bcf	(_FLAGS_STATE),0
	line	61
;main.c: 61: FLAGS_STATE.flag2 = FALSE;
	bcf	(_FLAGS_STATE),1
	line	62
;main.c: 62: FLAGS_STATE.flag3 = FALSE;
	bcf	(_FLAGS_STATE),2
	line	63
;main.c: 63: FLAGS_STATE.flag4 = FALSE;
	bcf	(_FLAGS_STATE),3
	line	64
;main.c: 64: FLAGS_STATE.flag5 = FALSE;
	bcf	(_FLAGS_STATE),4
	line	65
;main.c: 65: FLAGS_STATE.flag6 = FALSE;
	bcf	(_FLAGS_STATE),5
	line	66
;main.c: 66: FLAGS_STATE.flag7 = FALSE;
	bcf	(_FLAGS_STATE),6
	line	67
;main.c: 67: FLAGS_STATE.flag8 = FALSE;
	bcf	(_FLAGS_STATE),7
	line	69
;main.c: 69: FLAGS_STATE.flag1 = TRUE;
	bsf	(_FLAGS_STATE),0
	line	70
;main.c: 70: FLAGS_STATE.flag2 = TRUE;
	bsf	(_FLAGS_STATE),1
	line	71
;main.c: 71: FLAGS_STATE.flag3 = TRUE;
	bsf	(_FLAGS_STATE),2
	line	72
;main.c: 72: FLAGS_STATE.flag4 = TRUE;
	bsf	(_FLAGS_STATE),3
	line	73
;main.c: 73: FLAGS_STATE.flag5 = TRUE;
	bsf	(_FLAGS_STATE),4
	line	74
;main.c: 74: FLAGS_STATE.flag6 = TRUE;
	bsf	(_FLAGS_STATE),5
	line	75
;main.c: 75: FLAGS_STATE.flag7 = TRUE;
	bsf	(_FLAGS_STATE),6
	line	76
;main.c: 76: FLAGS_STATE.flag8 = TRUE;
	bsf	(_FLAGS_STATE),7
	line	79
	
l2883:	
;main.c: 79: TEST_VIN.lo=95;
	movlw	(05Fh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@TEST_VIN)
	line	80
	
l2885:	
;main.c: 80: TEST_VIN.VIN_FLAGS_STATE.vin_flag7=TRUE;
	clrf	0+(main@TEST_VIN)+0Ah
	bsf	status,0
	rlf	0+(main@TEST_VIN)+0Ah,f
	line	81
	
l2887:	
;main.c: 81: TEST_VIN.VIN_FLAGS_STATE.vin_flag2=TRUE;
	clrf	0+(main@TEST_VIN)+05h
	bsf	status,0
	rlf	0+(main@TEST_VIN)+05h,f
	line	82
;main.c: 82: TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin1=0xE1;
	movlw	(0E1h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	0+(main@TEST_VIN)+0Ch
	line	83
;main.c: 83: TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2=0x6945;
	movlw	low(06945h)
	movwf	0+(main@TEST_VIN)+0Ch
	movlw	high(06945h)
	movwf	(0+(main@TEST_VIN)+0Ch)+1
	line	84
;main.c: 84: TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin3=0x333675678;
	movlw	033h
	movwf	3+(main@TEST_VIN)+0Ch
	movlw	067h
	movwf	2+(main@TEST_VIN)+0Ch
	movlw	056h
	movwf	1+(main@TEST_VIN)+0Ch
	movlw	078h
	movwf	0+(main@TEST_VIN)+0Ch

	line	85
;main.c: 85: TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2=0xBAE1;
	movlw	low(0BAE1h)
	movwf	0+(main@TEST_VIN)+0Ch
	movlw	high(0BAE1h)
	movwf	(0+(main@TEST_VIN)+0Ch)+1
	line	86
;main.c: 86: union32 = TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2;
	movf	0+(main@TEST_VIN)+0Ch,w
	movwf	(main@union32)
	movf	1+(main@TEST_VIN)+0Ch,w
	movwf	((main@union32))+1
	clrf	2+((main@union32))
	clrf	3+((main@union32))
	goto	l2889
	line	87
;main.c: 87: do
	
l691:	
	line	89
	
l2889:	
;main.c: 88: {
;main.c: 89: union32 = TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin3;
	movlw	(0Ch)
	addlw	main@TEST_VIN&0ffh
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	movf	indf,w
	movwf	(main@union32)
	incf	fsr0,f
	movf	indf,w
	movwf	(main@union32+1)
	incf	fsr0,f
	movf	indf,w
	movwf	(main@union32+2)
	incf	fsr0,f
	movf	indf,w
	movwf	(main@union32+3)
	line	90
	
l2891:	
;main.c: 90: TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2++;
	movlw	low(01h)
	addwf	0+(main@TEST_VIN)+0Ch,f
	skipnc
	incf	1+(main@TEST_VIN)+0Ch,f
	movlw	high(01h)
	addwf	1+(main@TEST_VIN)+0Ch,f
	line	92
	
l2893:	
;main.c: 91: }
;main.c: 92: while(TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2 <= 0xBAEF);
	movlw	high(0BAF0h)
	subwf	1+(main@TEST_VIN)+0Ch,w
	movlw	low(0BAF0h)
	skipnz
	subwf	0+(main@TEST_VIN)+0Ch,w
	skipc
	goto	u2451
	goto	u2450
u2451:
	goto	l2889
u2450:
	goto	l2895
	
l692:	
	line	94
	
l2895:	
;main.c: 94: ptr = &TEST_VIN;
	movlw	(main@TEST_VIN)&0ffh
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@ptr)
	line	95
	
l2897:	
;main.c: 95: ptr -> lo = 79;
	movlw	(04Fh)
	movwf	(??_main+0)+0
	movf	(main@ptr),w
	movwf	fsr0
	movf	(??_main+0)+0,w
	movwf	indf
	line	97
	
l2899:	
;main.c: 97: COPY_VIN_TEST = TEST_VIN;
	movlw	(main@COPY_VIN_TEST)&0ffh
	movwf	fsr0
	movlw	low(main@TEST_VIN)
	movwf	(??_main+0)+0
	movf	fsr0,w
	movwf	((??_main+0)+0+1)
	movlw	16
	movwf	((??_main+0)+0+2)
u2460:
	movf	(??_main+0)+0,w
	movwf	fsr0
	bcf	status, 7	;select IRP bank0
	
	movf	indf,w
	movwf	((??_main+0)+0+3)
	incf	(??_main+0)+0,f
	movf	((??_main+0)+0+1),w
	movwf	fsr0
	
	movf	((??_main+0)+0+3),w
	movwf	indf
	incf	((??_main+0)+0+1),f
	decfsz	((??_main+0)+0+2),f
	goto	u2460
	goto	l2901
	line	98
;main.c: 98: while(1)
	
l693:	
	line	100
	
l2901:	
;main.c: 99: {
;main.c: 100: max = (100*(65536));
	clrf	(main@max)
	clrf	(main@max+1)
	line	101
	
l2903:	
;main.c: 101: setpoint = 0xA0;
	movlw	(0A0h)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@setpoint)
	line	102
	
l2905:	
;main.c: 102: actual_position = 0x9D;
	movlw	(09Dh)
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(main@actual_position)
	line	104
	
l2907:	
;main.c: 104: result=PIDcal(setpoint,actual_position);
	movf	(main@actual_position),w
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	movwf	(?_PIDcal)
	movf	(main@setpoint),w
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_PIDcal)),w
	clrf	(main@result+1)
	addwf	(main@result+1)
	movf	(0+(?_PIDcal)),w
	clrf	(main@result)
	addwf	(main@result)

	line	105
	
l2909:	
# 105 "C:\Users\Tom\Documents\pic_projects\PID\main.c"
nop ;#
psect	maintext
	line	106
	
l2911:	
;main.c: 106: for (i=0;i<=30;i++)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	clrf	(main@i)
	
l2913:	
	movlw	(01Fh)
	subwf	(main@i),w
	skipc
	goto	u2471
	goto	u2470
u2471:
	goto	l2917
u2470:
	goto	l2901
	
l2915:	
	goto	l2901
	line	107
	
l694:	
	line	108
	
l2917:	
;main.c: 107: {
;main.c: 108: test_actual_position_ar = actual_position_ar[i];
	movf	(main@i),w
	movwf	(??_main+0)+0
	addwf	(??_main+0)+0,w
	addlw	low((_actual_position_ar-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(main@test_actual_position_ar)
	line	109
	
l2919:	
;main.c: 109: result=PIDcal(0x44,actual_position_ar[i]);
	movf	(main@i),w
	movwf	(??_main+0)+0
	addwf	(??_main+0)+0,w
	addlw	low((_actual_position_ar-__stringbase))
	movwf	fsr0
	fcall	stringdir
	movwf	(??_main+1)+0
	movf	(??_main+1)+0,w
	movwf	(?_PIDcal)
	movlw	(044h)
	fcall	_PIDcal
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(1+(?_PIDcal)),w
	clrf	(main@result+1)
	addwf	(main@result+1)
	movf	(0+(?_PIDcal)),w
	clrf	(main@result)
	addwf	(main@result)

	line	110
	
l2921:	
# 110 "C:\Users\Tom\Documents\pic_projects\PID\main.c"
nop ;#
psect	maintext
	line	106
	
l2923:	
	movlw	(01h)
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_main+0)+0
	movf	(??_main+0)+0,w
	addwf	(main@i),f
	
l2925:	
	movlw	(01Fh)
	subwf	(main@i),w
	skipc
	goto	u2481
	goto	u2480
u2481:
	goto	l2917
u2480:
	goto	l2901
	
l695:	
	goto	l2901
	line	112
	
l696:	
	line	98
	goto	l2901
	
l697:	
	line	113
	
l698:	
	global	start
	ljmp	start
	opt stack 0
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_PIDcal
psect	text119,local,class=CODE,delta=2
global __ptext119
__ptext119:

;; *************** function _PIDcal *****************
;; Defined at:
;;		line 128 in file "C:\Users\Tom\Documents\pic_projects\PID\main.c"
;; Parameters:    Size  Location     Type
;;  setpoint        1    wreg     unsigned char 
;;  actual_posit    1    0[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  setpoint        1   18[BANK0 ] unsigned char 
;;  output          4   19[BANK0 ] long 
;;  d_term          4   14[BANK0 ] long 
;;  p_term          4   10[BANK0 ] long 
;;  error           2   23[BANK0 ] short 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned short 
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         0       2       0       0       0
;;      Locals:         0      15       0       0       0
;;      Temps:          0       8       0       0       0
;;      Totals:         0      25       0       0       0
;;Total ram usage:       25 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___lmul
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text119
	file	"C:\Users\Tom\Documents\pic_projects\PID\main.c"
	line	128
	global	__size_of_PIDcal
	__size_of_PIDcal	equ	__end_of_PIDcal-_PIDcal
	
_PIDcal:	
	opt	stack 6
; Regs used in _PIDcal: [wreg+status,2+status,0+btemp+1+pclath+cstack]
;PIDcal@setpoint stored from wreg
	line	138
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(PIDcal@setpoint)
	
l2849:	
;main.c: 129: static sint16_t pre_error = 0;
;main.c: 130: sint16_t error;
;main.c: 131: sint32_t output;
;main.c: 133: sint32_t p_term;
;main.c: 134: static sint32_t i_term = 0;
;main.c: 135: sint32_t d_term;
;main.c: 138: (GIE = 0);
	bcf	(95/8),(95)&7
	line	139
	
l2851:	
;main.c: 139: error = (setpoint - actual_position);
	movf	(PIDcal@actual_position),w
	movwf	(??_PIDcal+0)+0
	clrf	(??_PIDcal+0)+0+1
	comf	(??_PIDcal+0)+0,f
	comf	(??_PIDcal+0)+1,f
	incf	(??_PIDcal+0)+0,f
	skipnz
	incf	(??_PIDcal+0)+1,f
	movf	(PIDcal@setpoint),w
	addwf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@error)
	movf	1+(??_PIDcal+0)+0,w
	skipnc
	incf	1+(??_PIDcal+0)+0,w
	movwf	((PIDcal@error))+1
	line	142
	
l2853:	
;main.c: 142: p_term = (sint32_t)((40*(65536)))*(sint32_t)(error);
	movf	(PIDcal@error),w
	movwf	(?___lmul)
	movf	(PIDcal@error+1),w
	movwf	(?___lmul+1)
	movlw	0
	btfsc	(?___lmul+1),7
	movlw	255
	movwf	(?___lmul+2)
	movwf	(?___lmul+3)
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	028h
	movwf	2+(?___lmul)+04h
	movlw	0
	movwf	1+(?___lmul)+04h
	movlw	0
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	movf	(3+(?___lmul)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(PIDcal@p_term+3)
	movf	(2+(?___lmul)),w
	movwf	(PIDcal@p_term+2)
	movf	(1+(?___lmul)),w
	movwf	(PIDcal@p_term+1)
	movf	(0+(?___lmul)),w
	movwf	(PIDcal@p_term)

	line	145
	
l2855:	
;main.c: 145: i_term = i_term + (sint32_t)((5.00*(65536))*(0.02))*(sint32_t)(error);
	movf	(PIDcal@error),w
	movwf	(?___lmul)
	movf	(PIDcal@error+1),w
	movwf	(?___lmul+1)
	movlw	0
	btfsc	(?___lmul+1),7
	movlw	255
	movwf	(?___lmul+2)
	movwf	(?___lmul+3)
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	0
	movwf	2+(?___lmul)+04h
	movlw	019h
	movwf	1+(?___lmul)+04h
	movlw	099h
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	movf	(0+(?___lmul)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	addwf	(PIDcal@i_term),w
	movwf	((??_PIDcal+0)+0+0)
	movlw	0
	skipnc
	movlw	1
	addwf	(1+(?___lmul)),w
	clrf	((??_PIDcal+0)+0+2)
	skipnc
	incf	((??_PIDcal+0)+0+2),f
	addwf	(PIDcal@i_term+1),w
	movwf	((??_PIDcal+0)+0+1)
	skipnc
	incf	((??_PIDcal+0)+0+2),f
	movf	(2+(?___lmul)),w
	addwf	((??_PIDcal+0)+0+2),w
	clrf	((??_PIDcal+0)+0+3)
	skipnc
	incf	((??_PIDcal+0)+0+3),f
	addwf	(PIDcal@i_term+2),w
	movwf	((??_PIDcal+0)+0+2)
	skipnc
	incf	((??_PIDcal+0)+0+3),f
	movf	(3+(?___lmul)),w
	addwf	((??_PIDcal+0)+0+3),w
	addwf	(PIDcal@i_term+3),w
	movwf	((??_PIDcal+0)+0+3)
	movf	3+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term+3)
	movf	2+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term+2)
	movf	1+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term+1)
	movf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@i_term)

	line	147
	
l2857:	
;main.c: 147: if (i_term > (100*(65536)))
	movf	(PIDcal@i_term+3),w
	xorlw	80h
	movwf	btemp+1
	movlw	0
	xorlw	80h
	subwf	btemp+1,w
	
	skipz
	goto	u2393
	movlw	064h
	subwf	(PIDcal@i_term+2),w
	skipz
	goto	u2393
	movlw	0
	subwf	(PIDcal@i_term+1),w
	skipz
	goto	u2393
	movlw	01h
	subwf	(PIDcal@i_term),w
u2393:
	skipc
	goto	u2391
	goto	u2390
u2391:
	goto	l705
u2390:
	line	149
	
l2859:	
;main.c: 148: {
;main.c: 149: i_term = (100*(65536));
	movlw	0
	movwf	(PIDcal@i_term+3)
	movlw	064h
	movwf	(PIDcal@i_term+2)
	movlw	0
	movwf	(PIDcal@i_term+1)
	movlw	0
	movwf	(PIDcal@i_term)

	line	150
;main.c: 150: }
	goto	l2863
	line	151
	
l705:	
;main.c: 151: else if (i_term < (0))
	btfss	(PIDcal@i_term+3),7
	goto	u2401
	goto	u2400
u2401:
	goto	l2863
u2400:
	line	153
	
l2861:	
;main.c: 152: {
;main.c: 153: i_term = (0);
	movlw	0
	movwf	(PIDcal@i_term+3)
	movlw	0
	movwf	(PIDcal@i_term+2)
	movlw	0
	movwf	(PIDcal@i_term+1)
	movlw	0
	movwf	(PIDcal@i_term)

	goto	l2863
	line	154
	
l707:	
	goto	l2863
	line	157
	
l706:	
	
l2863:	
;main.c: 154: }
;main.c: 157: d_term = (sint32_t)((0.10*(65536))/(0.02))*(sint32_t)(error - pre_error);
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	comf	(PIDcal@pre_error)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(??_PIDcal+0)+0
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	comf	(PIDcal@pre_error+1)^080h,w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	((??_PIDcal+0)+0+1)
	incf	(??_PIDcal+0)+0,f
	skipnz
	incf	((??_PIDcal+0)+0+1),f
	movf	(PIDcal@error),w
	addwf	0+(??_PIDcal+0)+0,w
	movwf	(?___lmul)
	movf	(PIDcal@error+1),w
	skipnc
	incf	(PIDcal@error+1),w
	addwf	1+(??_PIDcal+0)+0,w
	movwf	1+(?___lmul)
	clrf	(?___lmul)+2
	btfsc	(?___lmul)+1,7
	decf	2+(?___lmul),f
	movf	(?___lmul)+2,w
	movwf	3+(?___lmul)
	movlw	0
	movwf	3+(?___lmul)+04h
	movlw	05h
	movwf	2+(?___lmul)+04h
	movlw	0
	movwf	1+(?___lmul)+04h
	movlw	0
	movwf	0+(?___lmul)+04h

	fcall	___lmul
	movf	(3+(?___lmul)),w
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movwf	(PIDcal@d_term+3)
	movf	(2+(?___lmul)),w
	movwf	(PIDcal@d_term+2)
	movf	(1+(?___lmul)),w
	movwf	(PIDcal@d_term+1)
	movf	(0+(?___lmul)),w
	movwf	(PIDcal@d_term)

	line	160
	
l2865:	
;main.c: 160: output = p_term + i_term + d_term;
	movf	(PIDcal@d_term),w
	movwf	(??_PIDcal+0)+0
	movf	(PIDcal@d_term+1),w
	movwf	((??_PIDcal+0)+0+1)
	movf	(PIDcal@d_term+2),w
	movwf	((??_PIDcal+0)+0+2)
	movf	(PIDcal@d_term+3),w
	movwf	((??_PIDcal+0)+0+3)
	movf	(PIDcal@p_term),w
	addwf	(PIDcal@i_term),w
	movwf	((??_PIDcal+4)+0+0)
	movlw	0
	skipnc
	movlw	1
	addwf	(PIDcal@p_term+1),w
	clrf	((??_PIDcal+4)+0+2)
	skipnc
	incf	((??_PIDcal+4)+0+2),f
	addwf	(PIDcal@i_term+1),w
	movwf	((??_PIDcal+4)+0+1)
	skipnc
	incf	((??_PIDcal+4)+0+2),f
	movf	(PIDcal@p_term+2),w
	addwf	((??_PIDcal+4)+0+2),w
	clrf	((??_PIDcal+4)+0+3)
	skipnc
	incf	((??_PIDcal+4)+0+3),f
	addwf	(PIDcal@i_term+2),w
	movwf	((??_PIDcal+4)+0+2)
	skipnc
	incf	((??_PIDcal+4)+0+3),f
	movf	(PIDcal@p_term+3),w
	addwf	((??_PIDcal+4)+0+3),w
	addwf	(PIDcal@i_term+3),w
	movwf	((??_PIDcal+4)+0+3)
	movf	0+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+0,f
	movf	1+(??_PIDcal+4)+0,w
	skipnc
	incfsz	1+(??_PIDcal+4)+0,w
	goto	u2410
	goto	u2411
u2410:
	addwf	(??_PIDcal+0)+1,f
u2411:
	movf	2+(??_PIDcal+4)+0,w
	skipnc
	incfsz	2+(??_PIDcal+4)+0,w
	goto	u2412
	goto	u2413
u2412:
	addwf	(??_PIDcal+0)+2,f
u2413:
	movf	3+(??_PIDcal+4)+0,w
	skipnc
	incf	3+(??_PIDcal+4)+0,w
	addwf	(??_PIDcal+0)+3,f
	movf	3+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+3)
	movf	2+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+2)
	movf	1+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output+1)
	movf	0+(??_PIDcal+0)+0,w
	movwf	(PIDcal@output)

	line	162
	
l2867:	
;main.c: 162: if (output > (100*(65536)))
	movf	(PIDcal@output+3),w
	xorlw	80h
	movwf	btemp+1
	movlw	0
	xorlw	80h
	subwf	btemp+1,w
	
	skipz
	goto	u2423
	movlw	064h
	subwf	(PIDcal@output+2),w
	skipz
	goto	u2423
	movlw	0
	subwf	(PIDcal@output+1),w
	skipz
	goto	u2423
	movlw	01h
	subwf	(PIDcal@output),w
u2423:
	skipc
	goto	u2421
	goto	u2420
u2421:
	goto	l708
u2420:
	line	164
	
l2869:	
;main.c: 163: {
;main.c: 164: output = (100*(65536));
	movlw	0
	movwf	(PIDcal@output+3)
	movlw	064h
	movwf	(PIDcal@output+2)
	movlw	0
	movwf	(PIDcal@output+1)
	movlw	0
	movwf	(PIDcal@output)

	line	165
;main.c: 165: }
	goto	l2873
	line	166
	
l708:	
;main.c: 166: else if (output < (0))
	btfss	(PIDcal@output+3),7
	goto	u2431
	goto	u2430
u2431:
	goto	l2873
u2430:
	line	168
	
l2871:	
;main.c: 167: {
;main.c: 168: output = (0);
	movlw	0
	movwf	(PIDcal@output+3)
	movlw	0
	movwf	(PIDcal@output+2)
	movlw	0
	movwf	(PIDcal@output+1)
	movlw	0
	movwf	(PIDcal@output)

	goto	l2873
	line	169
	
l710:	
	goto	l2873
	line	172
	
l709:	
	
l2873:	
;main.c: 169: }
;main.c: 172: pre_error = error;
	movf	(PIDcal@error+1),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(PIDcal@pre_error+1)^080h
	addwf	(PIDcal@pre_error+1)^080h
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(PIDcal@error),w
	bsf	status, 5	;RP0=1, select bank1
	bcf	status, 6	;RP1=0, select bank1
	clrf	(PIDcal@pre_error)^080h
	addwf	(PIDcal@pre_error)^080h

	line	179
	
l2875:	
;main.c: 179: (GIE = 1);
	bsf	(95/8),(95)&7
	line	180
	
l2877:	
;main.c: 180: return ((uint8_t)(output >> 16));
	bcf	status, 5	;RP0=0, select bank0
	bcf	status, 6	;RP1=0, select bank0
	movf	(PIDcal@output),w
	movwf	(??_PIDcal+0)+0
	movf	(PIDcal@output+1),w
	movwf	((??_PIDcal+0)+0+1)
	movf	(PIDcal@output+2),w
	movwf	((??_PIDcal+0)+0+2)
	movf	(PIDcal@output+3),w
	movwf	((??_PIDcal+0)+0+3)
	movlw	010h
	movwf	(??_PIDcal+4)+0
u2445:
	rlf	(??_PIDcal+0)+3,w
	rrf	(??_PIDcal+0)+3,f
	rrf	(??_PIDcal+0)+2,f
	rrf	(??_PIDcal+0)+1,f
	rrf	(??_PIDcal+0)+0,f
u2440:
	decfsz	(??_PIDcal+4)+0,f
	goto	u2445
	movf	0+(??_PIDcal+0)+0,w
	movwf	(??_PIDcal+5)+0
	clrf	(??_PIDcal+5)+0+1
	movf	0+(??_PIDcal+5)+0,w
	movwf	(?_PIDcal)
	movf	1+(??_PIDcal+5)+0,w
	movwf	(?_PIDcal+1)
	goto	l711
	
l2879:	
	line	181
	
l711:	
	return
	opt stack 0
GLOBAL	__end_of_PIDcal
	__end_of_PIDcal:
;; =============== function _PIDcal ends ============

	signat	_PIDcal,8314
	global	___lmul
psect	text120,local,class=CODE,delta=2
global __ptext120
__ptext120:

;; *************** function ___lmul *****************
;; Defined at:
;;		line 3 in file "C:\Program Files (x86)\HI-TECH Software\PICC\9.82\sources\lmul.c"
;; Parameters:    Size  Location     Type
;;  multiplier      4    0[COMMON] unsigned long 
;;  multiplicand    4    4[COMMON] unsigned long 
;; Auto vars:     Size  Location     Type
;;  product         4    9[COMMON] unsigned long 
;; Return value:  Size  Location     Type
;;                  4    0[COMMON] unsigned long 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK3   BANK2
;;      Params:         8       0       0       0       0
;;      Locals:         4       0       0       0       0
;;      Temps:          1       0       0       0       0
;;      Totals:        13       0       0       0       0
;;Total ram usage:       13 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_PIDcal
;; This function uses a non-reentrant model
;;
psect	text120
	file	"C:\Program Files (x86)\HI-TECH Software\PICC\9.82\sources\lmul.c"
	line	3
	global	__size_of___lmul
	__size_of___lmul	equ	__end_of___lmul-___lmul
	
___lmul:	
	opt	stack 6
; Regs used in ___lmul: [wreg+status,2+status,0]
	line	4
	
l2835:	
	movlw	0
	movwf	(___lmul@product+3)
	movlw	0
	movwf	(___lmul@product+2)
	movlw	0
	movwf	(___lmul@product+1)
	movlw	0
	movwf	(___lmul@product)

	goto	l2837
	line	6
	
l1474:	
	line	7
	
l2837:	
	btfss	(___lmul@multiplier),(0)&7
	goto	u2341
	goto	u2340
u2341:
	goto	l2841
u2340:
	line	8
	
l2839:	
	movf	(___lmul@multiplicand),w
	addwf	(___lmul@product),f
	movf	(___lmul@multiplicand+1),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2351
	addwf	(___lmul@product+1),f
u2351:
	movf	(___lmul@multiplicand+2),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2352
	addwf	(___lmul@product+2),f
u2352:
	movf	(___lmul@multiplicand+3),w
	clrz
	skipnc
	addlw	1
	skipnz
	goto	u2353
	addwf	(___lmul@product+3),f
u2353:

	goto	l2841
	
l1475:	
	line	9
	
l2841:	
	movlw	01h
	movwf	(??___lmul+0)+0
u2365:
	clrc
	rlf	(___lmul@multiplicand),f
	rlf	(___lmul@multiplicand+1),f
	rlf	(___lmul@multiplicand+2),f
	rlf	(___lmul@multiplicand+3),f
	decfsz	(??___lmul+0)+0
	goto	u2365
	line	10
	
l2843:	
	movlw	01h
u2375:
	clrc
	rrf	(___lmul@multiplier+3),f
	rrf	(___lmul@multiplier+2),f
	rrf	(___lmul@multiplier+1),f
	rrf	(___lmul@multiplier),f
	addlw	-1
	skipz
	goto	u2375

	line	11
	movf	(___lmul@multiplier+3),w
	iorwf	(___lmul@multiplier+2),w
	iorwf	(___lmul@multiplier+1),w
	iorwf	(___lmul@multiplier),w
	skipz
	goto	u2381
	goto	u2380
u2381:
	goto	l2837
u2380:
	goto	l2845
	
l1476:	
	line	12
	
l2845:	
	movf	(___lmul@product+3),w
	movwf	(?___lmul+3)
	movf	(___lmul@product+2),w
	movwf	(?___lmul+2)
	movf	(___lmul@product+1),w
	movwf	(?___lmul+1)
	movf	(___lmul@product),w
	movwf	(?___lmul)

	goto	l1477
	
l2847:	
	line	13
	
l1477:	
	return
	opt stack 0
GLOBAL	__end_of___lmul
	__end_of___lmul:
;; =============== function ___lmul ends ============

	signat	___lmul,8316
psect	text121,local,class=CODE,delta=2
global __ptext121
__ptext121:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
