#include "htc.h"
#include "typedefs.h"
#include "float.h"
//Define parameter
//#define  FLOATING_POINT_PID   
#define  FIXED_POINT_PID

/* All these #defines for floating point*/
#ifdef FLOATING_POINT_PID
#define epsilon 0.01
#define dt 0.01             //100ms loop time
#define MAX  100            //was 4 For Current Saturation
#define MIN -100
#define Kp  30.0             /*This made a huge differnence was this 0.1*/
#define Kd  0.01
#define Ki  0.05             //0.005
#endif

/* All these #defines for fixed point*/
#ifdef FIXED_POINT_PID
#define SCALE_FACTOR   (65536)              /* 1.000 in S16.16 format */
#define MAX            (100*SCALE_FACTOR)
#define MIN            (0)
#define KP             (40*SCALE_FACTOR)
#define KI             (5.00*SCALE_FACTOR)
#define KD             (0.10*SCALE_FACTOR)
#define DT             (0.02)              /* was .01*/

#endif
/*CONSTANTS ARRAYS*/
const actual_position_ar[] = {0x00,0x53,0x00,0x42,0x42,0x42,0x42,0x43,0x43,0x43,0x43,0x43,0x43,0x44,0x44,0x44,0x44,0x44,0x44,0x44,0x44,0x44,0x44,0x44,0x44,0x00};




     
__CONFIG(WDTE_OFF);
/* Function Prototypes*/
//float PIDcal(float setpoint,float actual_position);
uint16_t PIDcal(uint8_t setpoint,uint8_t actual_position);
float abs(float a);

void main(void)
{
/*Structues*/
VIN TEST_VIN;
VIN COPY_VIN_TEST;
VIN *ptr;
/*local vatiables*/
sint16_t max;
uint8_t setpoint;
uint8_t actual_position;
uint16_t result;
uint8_t union8;
uint8_t i;
uint16_t union16;
uint32_t union32;
uint8_t test_actual_position_ar;
/* program*/
FLAGS_STATE.flag1 = FALSE;
FLAGS_STATE.flag2 = FALSE;
FLAGS_STATE.flag3 = FALSE;
FLAGS_STATE.flag4 = FALSE;
FLAGS_STATE.flag5 = FALSE;
FLAGS_STATE.flag6 = FALSE;
FLAGS_STATE.flag7 = FALSE;
FLAGS_STATE.flag8 = FALSE;

FLAGS_STATE.flag1 = TRUE;
FLAGS_STATE.flag2 = TRUE;
FLAGS_STATE.flag3 = TRUE;
FLAGS_STATE.flag4 = TRUE;
FLAGS_STATE.flag5 = TRUE;
FLAGS_STATE.flag6 = TRUE;
FLAGS_STATE.flag7 = TRUE;
FLAGS_STATE.flag8 = TRUE;


TEST_VIN.lo=95;
TEST_VIN.VIN_FLAGS_STATE.vin_flag7=TRUE;
TEST_VIN.VIN_FLAGS_STATE.vin_flag2=TRUE;
TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin1=0xE1;
TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2=0x6945;
TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin3=0x333675678;
TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2=0xBAE1;
union32 = TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2;
do
 {
   union32 = TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin3;
   TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2++;
 } 
 while(TEST_VIN.VIN_FLAGS_STATE.TOMS.tom_vin2 <= 0xBAEF);

ptr = &TEST_VIN;
ptr -> lo = 79;                                 /* Dereference and stick in this value*/
 
COPY_VIN_TEST = TEST_VIN;                   /* copy the contents of this type over*/
while(1)
{
max = MAX;
setpoint = 0xA0;
actual_position = 0x9D;
 
result=PIDcal(setpoint,actual_position);
asm("nop");                                 /* =31*/
for (i=0;i<=30;i++)
  {
     test_actual_position_ar = actual_position_ar[i]; 
     result=PIDcal(0x44,actual_position_ar[i]);
     asm("nop");   
  } /* end FOR LOOP*/
 }
}
#ifdef FIXED_POINT_PID
/*****************************************************************************/
/* PIDcal():                                                                 */
/*                                                                           */
/* Description:  This function implements a PID controller with overflow     */
/*               and underflow protection.                                   */
/*                                                                           */
/* Inputs:       setpoint -> Input setpoint value                            */
/*               actual_position -> Input feedback value                     */
/*                                                                           */
/* Outputs:      output -> PWM output (0x00 = 0%, 0xFF = 100%)               */
/*****************************************************************************/

uint16_t PIDcal(uint8_t setpoint,uint8_t actual_position)
{
  static sint16_t pre_error = 0;
  sint16_t error;
  sint32_t output;

  sint32_t p_term;
  static sint32_t i_term = 0;
  sint32_t d_term;
   
  /* Calculate the error. */
  di();
  error = (setpoint - actual_position);

  /* Calculate the proportional term. */
  p_term = (sint32_t)(KP)*(sint32_t)(error);

  /* Calculate the integral term and saturate. */
  i_term = i_term + (sint32_t)(KI*DT)*(sint32_t)(error);

  if (i_term > MAX)
  {
    i_term = MAX;
  }
  else if (i_term < MIN)
  {
    i_term = MIN;
  }

  /* Calculate the derivative term. */
  d_term = (sint32_t)(KD/DT)*(sint32_t)(error - pre_error);

  /* Calculate the output and saturate. */ 
  output = p_term + i_term + d_term;
  
  if (output > MAX)
  {
    output = MAX;
  }
  else if (output < MIN)
  {
    output = MIN;
  }

  /* Update the error history. */
  pre_error = error;
 // output = ((uint16_t)(output >> 8)*2.55);
  //if (output > 0x8000)
  //   {
  //     output = 0x8000;   /* saturate for Autosar pwm*/
  //   } 

  ei();
return ((uint8_t)(output >> 16));
}

#endif








#ifdef FLOATING_POINT_PID
float PIDcal(float setpoint,float actual_position)
{
	static float pre_error = 0;
	static float integral = 0;
	float error;
	float derivative;
	float output;

	//Caculate P,I,D
	error = setpoint - actual_position;

	//In case of error too small then stop intergration
	if(abs(error) > epsilon)
	{
		integral = integral + error*dt;
	}
	derivative = (error - pre_error)/dt;
	output = Kp*error + Ki*integral + Kd*derivative;

	//Saturation Filter
	if(output > MAX)
	{
		output = MAX;
	}
	else if(output < MIN)
	{
		output = MIN;
	}
        //Update error
        pre_error = error;

 return output;
}
#endif

float abs(float a)
{
if (a < 0)
 {  
   return -a;
 }
 else
 {
  return a;
 }
}
