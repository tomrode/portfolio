/*****************************************************************************/
/* FILE NAME:  typedefs.h                                                    */
/*                                                                           */
/* Description:  Standard type definitions.                                  */
/*****************************************************************************/

#ifndef TYPEDEFS_H
#define TYPEDEFS_H

enum
{
FALSE,                       // implied to be zero
TRUE,                        // implied to be one
DEFAULT
};

typedef signed char sint8_t;
typedef unsigned char uint8_t;
typedef volatile signed char vsint8_t;
typedef volatile unsigned char vuint8_t;

typedef signed short sint16_t;
typedef unsigned short uint16_t;
typedef volatile signed short vsint16_t;
typedef volatile unsigned short vuint16_t;

typedef signed long sint32_t;
typedef unsigned long uint32_t;
typedef volatile signed long vsint32_t;
typedef volatile unsigned long vuint32_t;


typedef struct
        {
        uint8_t lo;
        uint8_t mid;
        uint8_t hi[2];
         struct
              {
              uint8_t vin_flag1;
              uint8_t vin_flag2;
              uint8_t vin_flag3;
              uint8_t vin_flag4;
              uint8_t vin_flag5;
              uint8_t vin_flag6;
              uint8_t vin_flag7;
              uint8_t vin_flag8;
              union
                   {
                    uint8_t  tom_vin1;
                    uint16_t tom_vin2;
                    uint32_t tom_vin3;
                   }TOMS; 
              }VIN_FLAGS_STATE; 
          }VIN;
struct
    {
     uint8_t flag1 :1;
     uint8_t flag2 :1;
     uint8_t flag3 :1;
     uint8_t flag4 :1;
     uint8_t flag5 :1;
     uint8_t flag6 :1;
     uint8_t flag7 :1;
     uint8_t flag8 :1;
    }FLAGS_STATE; 

#endif


