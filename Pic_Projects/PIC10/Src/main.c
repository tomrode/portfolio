#include "htc.h"
#include "typedefs.h"

/*PIC10f200*/
//__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
//__CONFIG(CP_OFF & MCLRE_OFF & WDTE_OFF);
/* STRUCTURES */

#define mSHIFT3(x)    ((uint16_t)((x) >> 3))  

#define mROW(x)       ((uint16_t)((x) & 0x0007))
#define mCOLUMN(x)    ((uint16_t)((x) & 0x00F8) >> 3)   
#define mSHIFT(x)     ((uint16_t)((x) & 0x0100)  >>  8)    
#define mALTERNATE(x) ((uint16_t)((x) & 0x0200)  >>  9)
#define mFUNCTION(x)  ((uint16_t)((x) & 0x0400)  >>  10)    
#define mLANGUAGE(x)  ((uint16_t)((x) & 0xF000)  >>  12)    

#define mSETROW(x,y)       ((uint16_t)((x) & 0xFFF8) | (uint8_t)(y) )
#define mSETCOLUMN(x,y)    ((uint16_t)((x) & 0xFF07) | ((uint8_t)(y) << 3) )
#define mSETSHIFT(x,y)     ((uint16_t)((x) & 0xFEFF) | ((uint8_t)(y) << 8) )
#define mSETALTERNATE(x,y) ((uint16_t)((x) & 0xFDFF) | ((uint8_t)(y) << 9) )
#define mSETFUNCTION(x,y)  ((uint16_t)((x) & 0xFBFF) | ((uint8_t)(y) << 10) )
#define mSETLANGUAGE(x,y)  ((uint16_t)((x) & 0x0FFF) | ((uint8_t)(y) << 12) )
typedef struct Key_Switches
{
	uint16_t ubHistory[3];
	uint16_t ubPointer;
	uint16_t Switches;
} Key_Switches_t;
/* Function prototypes*/
int main (void);                                   // Initializr  the hard ware

/* non writeable ram variable location*/


/* Volatile variable*/

/* Local scope variable*/

 
 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main(void)
{

}

