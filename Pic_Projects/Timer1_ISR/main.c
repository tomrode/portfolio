
/**********************************************************************************************************
* Date   29 APR 2013             TEST THE ANALOG SWITCH SPDT FOR HH     
* Description: I am going to use RC5 to address this switch 
*
***********************************************************************************************************/
#include "htc.h"
#include "typedefs.h"
#include "timer.h"

/*PIC16F887*/
__CONFIG(DEBUGEN & LVPEN & FCMEN & IESODIS & BORXSLP & DUNPROTECT & UNPROTECT & MCLREN & PWRTEN & WDTDIS & INTCLK);//HS);
__CONFIG(BORV40);
/* STRUCTURES */
 
/* Function prototypes*/
int main (void);
void sfreg(void);                                   // Initializr  the hard ware

/* non writeable ram variable location*/
/* bit fields*/
struct 
     {
       uint8_t lo    : 1;
       uint8_t dummy : 6;
       uint8_t hi    : 1;
     }foo;
union
    {
   struct  
        { 
          uint8_t b7 : 1;
          uint8_t b6 : 1;
          uint8_t b5 : 1;
          uint8_t b4 : 1;
          uint8_t b3 : 1;
          uint8_t b2 : 1;
          uint8_t b1 : 1;
          uint8_t b0 : 1;
   
          //STUCTURE this_mem;
        }which_mem;
     uint8_t clear;
    }flags;
/* Volatile variable*/


/*arrays*/

 /*********************************************************************/
/*        THE MAIN LOOP                                              */         
/*********************************************************************/
int main (void)
{
 /* variables for the main*/
UNION  test_union;
foo.hi = 1;
flags.which_mem.b3 = 1;
flags.which_mem.b7 = 1;
flags.clear = 0x00;
/***********************************************************************/
/*          INITIALIZATIONS
/***********************************************************************/
  sfreg();                                            // initialize sfr's   
  di();                                               // disable all interrupts
  PEIE = 1;                                           /* enable all peripheral interrupts*/ 
  TMR1IE = 1;
  timer_set(TIMER_1, 100);//1000);
  
/*************************************************************************/
/*           CODE WILL REMAIN IN while loop if no watch dog reset
/*************************************************************************/ 
// while (HTS == 0)
//  {}                                                 // wait until clock stable 
 test_union.which_mem.mem1 = 55;
 test_union.which_mem.mem2 = 14;
 test_union.clear           = 0;
 RC5                        = 1;
 
   while(1)
   {    
      if(get_timer(TIMER_1) == 0)
      {
          timer_set(TIMER_1, 30); 
          PORTD ^= 0xFF;
         
      }
   } 
}

void sfreg(void)
{
//WDTCON = 0x17;       // WDTPS3=1|WDTPS2=0|WDTPS1=1|WDTPS0=1|SWDTEN=1 5 seconds before watch dog expires
OSCCON = 0x61;       // 4 Mhz clock
OPTION = 0x00;       // |PSA=Prescale to WDT|most prescale rate|
TRISE  = 0x01;       // MAKE PORTE RE1 an input
TRISC5  = 0;          // Make RC 5 output
TRISC2  = 1;
TRISD  = 0x00;       // Port D as outputs
PORTD  = 0x55; 
T1CON  = 0x35;       // T1CKPS1,T1CKPS0 = 1:8 prescaler,T1SYNC = do not synchronise external clock input ,TMR1ON = enabled
ANSEL  &=0x00;       // Make a digital I/O
}

