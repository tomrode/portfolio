/*****************************************************************************/
/* FILE NAME:  func.h                                                        */
/*****************************************************************************/

#include "typedefs.h"

#ifndef FUNC_H
#define FUNC_H

/* Function prototypes*/
void func(void);

#endif