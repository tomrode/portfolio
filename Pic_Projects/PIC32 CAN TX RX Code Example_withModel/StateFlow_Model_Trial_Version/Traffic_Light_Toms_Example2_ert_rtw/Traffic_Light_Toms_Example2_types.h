/*
 * File: Traffic_Light_Toms_Example2_types.h
 *
 * Code generated for Simulink model 'Traffic_Light_Toms_Example2'.
 *
 * Model version                  : 1.17
 * Simulink Coder version         : 8.4 (R2013a) 13-Feb-2013
 * TLC version                    : 8.4 (Jan 19 2013)
 * C/C++ source code generated on : Fri Jan 10 12:49:51 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Traffic_Light_Toms_Example2_types_h_
#define RTW_HEADER_Traffic_Light_Toms_Example2_types_h_

/* Forward declaration for rtModel */
typedef struct tag_RTM_Traffic_Light_Toms_Ex_T RT_MODEL_Traffic_Light_Toms_E_T;

#endif                                 /* RTW_HEADER_Traffic_Light_Toms_Example2_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
