/**************************************************************
*  File ccppar.h
*  generated at Wed Dec 04 09:34:27 2013
*             Toolversion:   427
*               Bussystem:   CAN
*
*  generated out of CANdb:   D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\CCAN.dbc
*                            D:\rtc_wa_KL15\BCM_NET_DIAG\CUSW\TOOLS\CANGEN\BCAN.dbc

*            Manufacturer:   Fiat
*                for node:   BCM
*   Generation parameters:   Target system = FR60
*                            Compiler      = Fujitsu / Softune
*
* License information:       
*   -    Serialnumber:       CBD0800214
*   - Date of license:       19.9.2008
*
***************************************************************
Software is licensed for:    
Magneti Marelli Sistemi Elettronici S.p.A.
Fiat / SLP2 / MB91460P / Fujitsu Softune V60L01 / MB91F467
**************************************************************/

#ifndef __CCPPAR_H__
#define __CCPPAR_H__

#define V21

#define C_ENABLE_CCP

#define CCP_CAN_CHANNEL 0
#include "can_inc.h"

/* include generated header file */
#include "can_msg.h"

#define CCP_ROM V_MEMROM2

#define CCP_BYTE    unsigned char
#define CCP_WORD    unsigned short
#define CCP_DWORD   unsigned long
#define CCP_BYTEPTR unsigned char*

#define CCP_DISABLE_INTERRUPT CanInterruptDisable()
#define CCP_ENABLE_INTERRUPT CanInterruptRestore()

#define CCP_STATION_ID "BCM"

#define CCP_STATION_ADDR 0x4


#define CCP_DTO_ID 0x9e3d4000
#define CCP_TX_HANDLE NETC_TX_DTO_BCM
#define CCP_TX_DATA_PTR DTO_BCM._c
#define CCP_CRO_ID 0x9e394000
#define CCP_RX_DATA_PTR CRO_BCM._c

#define CCP_DAQ
#define CCP_MAX_ODT 7
#define CCP_MAX_DAQ 1


#define CCP_CHECKSUM
#define CCP_CHECKSUM_TYPE CCP_WORD



#endif
