/**********************************************************************
* � 2009 Microchip Technology Inc.
*
* FileName:        main.c
* Dependencies:    Header (.h) files if applicable, see below
* Processor:       PIC32
* Compiler:        MPLAB� C32 
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip's
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP'S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
************************************************************************/

#include <p32xxxx.h>
#include <peripheral\system.h>

#include "GenericTypeDefs.h"
#include "h\CANFunctions.h"
#include "h\Explorer16.h"
#include "h\statemachine.h"
#include "h\can_msg.h"
#include "h\Traffic_Light_Toms_example2.h"
/* This code example runs on a Explorer 16 board
 * with a 8Mhz crystal and the ECAN LIN Pictail
 * plus board. The CAN1 and CAN2 connecters on the
 * ECAN LIN Pictail plus board are connected to 
 * each other to form a 2 node CAN bus.*/

/* The code example shows how to transmit and receive
 * standard ID messages using the PIC32 CAN modules and
 * the PIC32 Peripheral Library. In this case, CAN1 
 * sends a Standard ID message to CAN1 and CAN2 replies
 * back with a Standard ID message.
 *
 * A timer is setup for 0.1 millisecond. This is used
 * to obtain a 1 second tick. Every 1 second, CAN1 will
 * send a message to CAN2 to toggle LED5. CAN2 will
 * then send a message to CAN1 to toggle LED6. Hence 
 * both LEDs will toggle every second.*/

 /* Configuration Bit settings
 * SYSCLK = 80 MHz (((8MHz Crystal/ FPLLIDIV) * FPLLMUL) / FPLLODIV)
 * PBCLK = 80 MHz
 * Primary Osc w/PLL (XT+,HS+,EC+PLL)
 * WDT OFF
 * Other options are don't care */


#pragma config FPLLMUL = MUL_20, FPLLIDIV = DIV_2, FPLLODIV = DIV_1, FWDTEN = OFF
#pragma config POSCMOD = HS, FNOSC = PRIPLL, FPBDIV = DIV_1 


#define SYS_FREQ        (80000000L)
#define TOGGLE_INIT_PORTD       (16)
/* Each CAN modulec uses 2 Channels (Channel 0
 * and Channel 1). Each channel is configured
 * to be 8 messages deep. Channel 0 is configured
 * for Transmit and Channel 1 is configured for
 * receive. CAN module configuration code
 * is in CANFunctions.c */ 

/* Note the size of each Channel area.
 * It is 2 (Channels) * 8 (Messages Buffers) 
 * 16 (bytes/per message buffer) bytes. Each 
 * CAN module should have its own message 
 * area. */

BYTE CAN1MessageFifoArea[2 * 8 * 16];
BYTE CAN2MessageFifoArea[2 * 8 * 16];
BOOL IgnRunFlag;                             /* this is an external variable*/
/* Global data*/


/* Function Prototypes*/
int main(void);
void Toggle_Portd(void); 


int main(void)
{
     /* Local main variables*/
    _c_CBC_PT2_msgType CBC_PT2TxMessagemain;
    static  ExtU_Traffic_Light_Toms_Examp_T ModelClock;
    static unsigned int value;
    /* Configure system for maximum performance
     * and enable multi vector interrupts. */
  
    SYSTEMConfig(SYS_FREQ, SYS_CFG_WAIT_STATES | SYS_CFG_PCACHE);
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();
    
  /* Intialize  Explorer16 LED ports,
   * CAN module and start the timer. Make
   * all analog ports digital. */
    
  AD1PCFGSET = 0xFFFFFFFF;
    
  //Explorer16Init(); /* Function is defined in Explorer16.c    */
  CAN1Init();       /* Function is defined in CANFunctions.c  */
  CAN2Init();       /* Function is defined in CANFunctions.c  */
  Timer1Init();     /* Function is defined in TimerFunctions.c  */
    /* I added this in to toggle 3 leds on pic32 board visual indication before loop*/ 
  TRISD = 0xF0;          /* makes as outputs*/
  PORTSetPinsDigitalIn(IOPORT_D,BIT_5 | BIT_6| BIT_13);


  Toggle_Portd();
 
  while(1)
  {
    /* In this loop, we wait till we have
     * 1 second tick. When a 1 second 
     * tick is obtained, CAN1 will send a
     * message to CAN2. When CAN2 receives
     * this message it toggles LED5. It then
     * sends a message to CAN1. When CAN1 
         * receives this message it toggles 
     * LED6. */

        if(_1000ms_task() == TRUE)
         {
           
         }    
        if(_500ms_task() == TRUE)
         {  
            ModelClock.ClockIn ^= 0x01;
            Traffic_Light_Toms_Example2_U.ClockIn = ModelClock.ClockIn; 
            DiagReqStateMachine();
            CAN2RxMsgProcess(); 
         }
        if(_250ms_task() == TRUE)
         { 
         
         }
        if(_100ms_task() == TRUE)
         {
             IgnRunFlag = TRUE;
             CAN2TxSendLEDMsg();    /* Send out ignition in run*/        
         }
         if(_50ms_task() == TRUE)
         {
          
         }
         if(_25ms_task() == TRUE)
         {
            
         }
         if(_10ms_task() == TRUE)
         {
          value = (PORTReadBits(IOPORT_D,BIT_6 | BIT_7|BIT_13));
            if (value == 0x00002080)                             /* Mask value for SW1 RD6*/
           {
               PORTSetBits(IOPORT_D,(BIT_0|BIT_1|BIT_2)); 
               Traffic_Light_Toms_Example2_U.EnableIn = TRUE;
           }
        
            Traffic_Light_Toms_Example2_step();    /* Goto the model*/
            if(Traffic_Light_Toms_Example2_Y.redOut == TRUE)
            {
              PORTSetBits(IOPORT_D,BIT_0); 
              PORTClearBits(IOPORT_D,BIT_1 | BIT_2); 
            }
            else if (Traffic_Light_Toms_Example2_Y.yellowOut == TRUE)
            {
              PORTSetBits(IOPORT_D,BIT_1); 
              PORTClearBits(IOPORT_D,BIT_0 | BIT_2); 
            }
             else if (Traffic_Light_Toms_Example2_Y.greenOut == TRUE)
            {
              PORTSetBits(IOPORT_D,BIT_2); 
              PORTClearBits(IOPORT_D,BIT_0 | BIT_1); 
            }
            else
             {} 
         
         }
         if(_5ms_task() == TRUE)
         { 
          /* A trial to see if this will send out */
         /* BYTE 0*/
         CBC_PT2TxMessagemain.CBC_PT2ACC_DLY_ACTSts = (Traffic_Light_Toms_Example2_Y.redOut);
         CBC_PT2TxMessagemain.CBC_PT2RemStActvSts   = (Traffic_Light_Toms_Example2_Y.yellowOut);
         CBC_PT2TxMessagemain.CBC_PT2StTypSts       = (Traffic_Light_Toms_Example2_Y.greenOut);
         CBC_PT2TxMessagemain.CBC_PT2CmdIgnSts      = 0x03;
         /* BYTE 1*/
         CBC_PT2TxMessagemain.CBC_PT2RemSt_InhibitSts = 0x07;
         CBC_PT2TxMessagemain.CBC_PT2KeyInIgnSts      = 0x02;
         /* BYTE 2*/
         CBC_PT2TxMessagemain.CBC_PT2FOBSearchRequest = (Traffic_Light_Toms_Example2_Y.WalkLedOut);
         CBC_PT2TxMessagemain.CBC_PT2StartRelayBCMSts =(Traffic_Light_Toms_Example2_Y.DoNotWalkLedOut);
         CBC_PT2TxMessagemain.CBC_PT2StartRelayBCMFault =(Traffic_Light_Toms_Example2_Y.WalkCautionLedOut);
         CBC_PT2TxMessagemain.CBC_PT2PanicModeActive  = TRUE;
         /* BYTE 3*/
         CBC_PT2TxMessagemain.CBC_PT2MessageCounter = 0x0A;
         /* BYTE 4*/
         CBC_PT2TxMessagemain.CBC_PT2CRC = 0xAB;
         /* pass via pointer to Can controller*/          
         CAN2TxSendCBC_PT2(&CBC_PT2TxMessagemain);      
            /* Function is defined in CANFunctions.c. 
               Tested the time for call found A: Port toggle around 30ns and
               B: 576 ns for the LATD  to toggle with CANTxSend SID's. EID's add 50ns more*/  
         }
    /* CAN2RxMsgProcess will check if CAN2
     * has received a message from CAN1 and
     * will toggle LED5. It will send a message
     * to CAN1 to toggle LED6. */
          /* CANxRxMsgProcess() reads take 225ns */
    //CAN2RxMsgProcess();

    /* CAN1RxMsgProcess() will check if CAN1
     * has received a message from CAN2 and
     * will toggle LED6. */

    //CAN1RxMsgProcess();
  }

    
}
/************************************************************************
/* Toggle_Portd()
/*                 This function will toggle the 3 LEDS on the 
/*                 USB Starter Kit II (Part Number: DM320003-2) 
/*                 upto TOGGLE_INIT_PORTD times then resume.              
/*  
/* INPUTS: NONE
/* OUTPUT: NONE
/*************************************************************************/
void Toggle_Portd(void)
{
    static INT8 Portd_Toggle;
    INT8 StateMachine;
    /* Example to use port D from port B*/ 
    //AD1PCFG = 0xFFFF;                // Initialize AN pins as digital
    //TRISD  = 0;                        // initialize PORTD as output
    Portd_Toggle = 0x00;               // make count zero
    _nop();
     StateMachine = 0;                   
    do
     {
        if(_100ms_task() == TRUE)
          {   
            switch (StateMachine)
             {
               case 0:
                Portd_Toggle++;
                LATD = Portd_Toggle;
                if(Portd_Toggle == 7)
                 {
                   StateMachine = 1; 
                 }     
               break;
               case 1:
                Portd_Toggle--;
                LATD = Portd_Toggle;
                if(Portd_Toggle == 0)
                 {
                    Portd_Toggle = TOGGLE_INIT_PORTD + 1; /* Get out here*/
                 } 
                break;
                 default:
                  break;
             }     
           } 
     }while(Portd_Toggle <= TOGGLE_INIT_PORTD);  /* 16 on/off transistions*/
   LATD   = 0x0000;                   // Ensure portD off.
   _nop();
}
