/*
 * File: Traffic_Light_Toms_Example.h
 *
 * Code generated for Simulink model 'Traffic_Light_Toms_Example'.
 *
 * Model version                  : 1.14
 * Simulink Coder version         : 8.4 (R2013a) 13-Feb-2013
 * TLC version                    : 8.4 (Jan 19 2013)
 * C/C++ source code generated on : Mon Jan 06 09:30:50 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Microchip->PIC18
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Traffic_Light_Toms_Example_h_
#define RTW_HEADER_Traffic_Light_Toms_Example_h_
#ifndef Traffic_Light_Toms_Example_COMMON_INCLUDES_
# define Traffic_Light_Toms_Example_COMMON_INCLUDES_
#include <string.h>
#include "rtwtypes.h"
#include "rt_zcfcn.h"
#endif                                 /* Traffic_Light_Toms_Example_COMMON_INCLUDES_ */

#include "Traffic_Light_Toms_Example_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Block signals (auto storage) */
typedef struct {
  int8_T Tick;                         /* '<Root>/Traffic_Light' */
} B_Traffic_Light_Toms_Example_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T cnt2;                         /* '<Root>/Cross_Walk_Indication' */
  real_T cnt1;                         /* '<Root>/Cross_Walk_Indication' */
  uint32_T presentTicks;               /* '<Root>/Traffic_Light' */
  uint32_T elapsedTicks;               /* '<Root>/Traffic_Light' */
  uint32_T previousTicks;              /* '<Root>/Traffic_Light' */
  uint32_T presentTicks_c;             /* '<Root>/Cross_Walk_Indication' */
  uint32_T elapsedTicks_h;             /* '<Root>/Cross_Walk_Indication' */
  uint32_T previousTicks_m;            /* '<Root>/Cross_Walk_Indication' */
  uint8_T is_active_c1_Traffic_Light_Toms;/* '<Root>/Traffic_Light' */
  uint8_T is_c1_Traffic_Light_Toms_Exampl;/* '<Root>/Traffic_Light' */
  uint8_T temporalCounter_i1;          /* '<Root>/Traffic_Light' */
  uint8_T is_active_c2_Traffic_Light_Toms;/* '<Root>/Cross_Walk_Indication' */
  uint8_T is_c2_Traffic_Light_Toms_Exampl;/* '<Root>/Cross_Walk_Indication' */
  uint8_T is_WALK_CAUTION_YELLOW;      /* '<Root>/Cross_Walk_Indication' */
  uint8_T is_active_cnt1;              /* '<Root>/Cross_Walk_Indication' */
  uint8_T is_active_cnt2;              /* '<Root>/Cross_Walk_Indication' */
  uint8_T temporalCounter_i1_c;        /* '<Root>/Cross_Walk_Indication' */
} DW_Traffic_Light_Toms_Example_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState Traffic_Light_Trig_ZCE;   /* '<Root>/Traffic_Light' */
} PrevZCX_Traffic_Light_Toms_Ex_T;

/* External inputs (root inport signals with auto storage) */
typedef struct {
  real_T ClockIn;                      /* '<Root>/ClockIn' */
} ExtU_Traffic_Light_Toms_Examp_T;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  boolean_T redOut;                    /* '<Root>/redOut' */
  boolean_T greenOut;                  /* '<Root>/greenOut' */
  boolean_T yellowOut;                 /* '<Root>/yellowOut' */
  boolean_T WalkLedOut;                /* '<Root>/WalkLedOut' */
  boolean_T DoNotWalkLedOut;           /* '<Root>/DoNotWalkLedOut' */
  boolean_T WalkCautionLedOut;         /* '<Root>/WalkCautionLedOut' */
} ExtY_Traffic_Light_Toms_Examp_T;

/* Real-time Model Data Structure */
struct tag_RTM_Traffic_Light_Toms_Ex_T {
  const char_T * volatile errorStatus;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    uint16_T clockTick0;
  } Timing;
};

/* Block signals (auto storage) */
extern B_Traffic_Light_Toms_Example_T Traffic_Light_Toms_Example_B;

/* Block states (auto storage) */
extern DW_Traffic_Light_Toms_Example_T Traffic_Light_Toms_Example_DW;

/* External inputs (root inport signals with auto storage) */
extern ExtU_Traffic_Light_Toms_Examp_T Traffic_Light_Toms_Example_U;

/* External outputs (root outports fed by signals with auto storage) */
extern ExtY_Traffic_Light_Toms_Examp_T Traffic_Light_Toms_Example_Y;

/* Model entry point functions */
extern void Traffic_Light_Toms_Example_initialize(void);
extern void Traffic_Light_Toms_Example_step(void);
extern void Traffic_Light_Toms_Example_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Traffic_Light_Toms_E_T *const Traffic_Light_Toms_Example_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'Traffic_Light_Toms_Example'
 * '<S1>'   : 'Traffic_Light_Toms_Example/Cross_Walk_Indication'
 * '<S2>'   : 'Traffic_Light_Toms_Example/Traffic_Light'
 */
#endif                                 /* RTW_HEADER_Traffic_Light_Toms_Example_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
