function bio=Traffic_Light_Toms_Examplebio
bio = [];
bio(1).blkName='Cross_Walk_Indication/p1';
bio(1).sigName='WALK_LED';
bio(1).portIdx=0;
bio(1).dim=[1,1];
bio(1).sigWidth=1;
bio(1).sigAddress='&Traffic_Light_Toms_Example_B.WALK_LED';
bio(1).ndims=2;
bio(1).size=[];

bio(getlenBIO) = bio(1);

bio(2).blkName='Cross_Walk_Indication/p2';
bio(2).sigName='DO_NOT_WALK_LED';
bio(2).portIdx=1;
bio(2).dim=[1,1];
bio(2).sigWidth=1;
bio(2).sigAddress='&Traffic_Light_Toms_Example_B.DO_NOT_WALK_LED';
bio(2).ndims=2;
bio(2).size=[];


bio(3).blkName='Cross_Walk_Indication/p3';
bio(3).sigName='WALK_CAUTION_LED';
bio(3).portIdx=2;
bio(3).dim=[1,1];
bio(3).sigWidth=1;
bio(3).sigAddress='&Traffic_Light_Toms_Example_B.WALK_CAUTION_LED';
bio(3).ndims=2;
bio(3).size=[];


bio(4).blkName='Traffic_Light/p1';
bio(4).sigName='red';
bio(4).portIdx=0;
bio(4).dim=[1,1];
bio(4).sigWidth=1;
bio(4).sigAddress='&Traffic_Light_Toms_Example_B.red';
bio(4).ndims=2;
bio(4).size=[];


bio(5).blkName='Traffic_Light/p2';
bio(5).sigName='green';
bio(5).portIdx=1;
bio(5).dim=[1,1];
bio(5).sigWidth=1;
bio(5).sigAddress='&Traffic_Light_Toms_Example_B.green';
bio(5).ndims=2;
bio(5).size=[];


bio(6).blkName='Traffic_Light/p3';
bio(6).sigName='yellow';
bio(6).portIdx=2;
bio(6).dim=[1,1];
bio(6).sigWidth=1;
bio(6).sigAddress='&Traffic_Light_Toms_Example_B.yellow';
bio(6).ndims=2;
bio(6).size=[];


bio(7).blkName='Pulse Generator';
bio(7).sigName='';
bio(7).portIdx=0;
bio(7).dim=[1,1];
bio(7).sigWidth=1;
bio(7).sigAddress='&Traffic_Light_Toms_Example_B.PulseGenerator';
bio(7).ndims=2;
bio(7).size=[];


function len = getlenBIO
len = 7;

