/*
 * Traffic_Light_Toms_Example.h
 *
 * Code generation for model "Traffic_Light_Toms_Example".
 *
 * Model version              : 1.14
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Mon Jul  2 09:43:27 2018
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Traffic_Light_Toms_Example_h_
#define RTW_HEADER_Traffic_Light_Toms_Example_h_
#include <float.h>
#include <string.h>
#include <stddef.h>
#ifndef Traffic_Light_Toms_Example_COMMON_INCLUDES_
# define Traffic_Light_Toms_Example_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#endif                                 /* Traffic_Light_Toms_Example_COMMON_INCLUDES_ */

#include "Traffic_Light_Toms_Example_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_zcfcn.h"
#include "rt_nonfinite.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm)         ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  uint8_T red;                         /* '<Root>/Traffic_Light' */
  uint8_T green;                       /* '<Root>/Traffic_Light' */
  uint8_T yellow;                      /* '<Root>/Traffic_Light' */
  boolean_T WALK_LED;                  /* '<Root>/Cross_Walk_Indication' */
  boolean_T DO_NOT_WALK_LED;           /* '<Root>/Cross_Walk_Indication' */
  boolean_T WALK_CAUTION_LED;          /* '<Root>/Cross_Walk_Indication' */
} B_Traffic_Light_Toms_Example_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T cnt2;                         /* '<Root>/Cross_Walk_Indication' */
  real_T cnt1;                         /* '<Root>/Cross_Walk_Indication' */
  struct {
    void *LoggedData[3];
  } Traffic_light_PWORK;               /* '<Root>/Traffic_light' */

  struct {
    void *LoggedData[3];
  } Walk_indication_PWORK;             /* '<Root>/Walk_indication' */

  int32_T clockTickCounter;            /* '<Root>/Pulse Generator' */
  uint32_T presentTicks;               /* '<Root>/Traffic_Light' */
  uint32_T elapsedTicks;               /* '<Root>/Traffic_Light' */
  uint32_T previousTicks;              /* '<Root>/Traffic_Light' */
  uint8_T is_active_c1_Traffic_Light_Toms;/* '<Root>/Traffic_Light' */
  uint8_T is_c1_Traffic_Light_Toms_Exampl;/* '<Root>/Traffic_Light' */
  uint8_T temporalCounter_i1;          /* '<Root>/Traffic_Light' */
  uint8_T is_active_c2_Traffic_Light_Toms;/* '<Root>/Cross_Walk_Indication' */
  uint8_T is_c2_Traffic_Light_Toms_Exampl;/* '<Root>/Cross_Walk_Indication' */
  uint8_T is_WALK_CAUTION_YELLOW;      /* '<Root>/Cross_Walk_Indication' */
  uint8_T temporalCounter_i1_k;        /* '<Root>/Cross_Walk_Indication' */
} DW_Traffic_Light_Toms_Example_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState Traffic_Light_Trig_ZCE;   /* '<Root>/Traffic_Light' */
} PrevZCX_Traffic_Light_Toms_Ex_T;

/* Parameters (auto storage) */
struct P_Traffic_Light_Toms_Example_T_ {
  real_T PulseGenerator_Amp;           /* Expression: 1
                                        * Referenced by: '<Root>/Pulse Generator'
                                        */
  real_T PulseGenerator_Period;        /* Computed Parameter: PulseGenerator_Period
                                        * Referenced by: '<Root>/Pulse Generator'
                                        */
  real_T PulseGenerator_Duty;          /* Computed Parameter: PulseGenerator_Duty
                                        * Referenced by: '<Root>/Pulse Generator'
                                        */
  real_T PulseGenerator_PhaseDelay;    /* Expression: 0
                                        * Referenced by: '<Root>/Pulse Generator'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_Traffic_Light_Toms_Ex_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_Traffic_Light_Toms_Example_T Traffic_Light_Toms_Example_P;

/* Block signals (auto storage) */
extern B_Traffic_Light_Toms_Example_T Traffic_Light_Toms_Example_B;

/* Block states (auto storage) */
extern DW_Traffic_Light_Toms_Example_T Traffic_Light_Toms_Example_DW;

/* Model entry point functions */
extern void Traffic_Light_Toms_Example_initialize(void);
extern void Traffic_Light_Toms_Example_step(void);
extern void Traffic_Light_Toms_Example_terminate(void);

/* Real-time Model object */
extern RT_MODEL_Traffic_Light_Toms_E_T *const Traffic_Light_Toms_Example_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'Traffic_Light_Toms_Example'
 * '<S1>'   : 'Traffic_Light_Toms_Example/Cross_Walk_Indication'
 * '<S2>'   : 'Traffic_Light_Toms_Example/Traffic_Light'
 */
#endif                                 /* RTW_HEADER_Traffic_Light_Toms_Example_h_ */
