/*
 * Traffic_Light_Toms_Example_types.h
 *
 * Code generation for model "Traffic_Light_Toms_Example".
 *
 * Model version              : 1.14
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Mon Jul  2 09:43:27 2018
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Traffic_Light_Toms_Example_types_h_
#define RTW_HEADER_Traffic_Light_Toms_Example_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_Traffic_Light_Toms_Example_T_ P_Traffic_Light_Toms_Example_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_Traffic_Light_Toms_Ex_T RT_MODEL_Traffic_Light_Toms_E_T;

#endif                                 /* RTW_HEADER_Traffic_Light_Toms_Example_types_h_ */
