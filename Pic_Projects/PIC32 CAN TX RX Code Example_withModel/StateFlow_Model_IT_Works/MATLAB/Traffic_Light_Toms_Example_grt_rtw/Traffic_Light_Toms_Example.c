/*
 * Traffic_Light_Toms_Example.c
 *
 * Code generation for model "Traffic_Light_Toms_Example".
 *
 * Model version              : 1.14
 * Simulink Coder version : 8.13 (R2017b) 24-Jul-2017
 * C source code generated on : Mon Jul  2 09:43:27 2018
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "Traffic_Light_Toms_Example.h"
#include "Traffic_Light_Toms_Example_private.h"

/* Named constants for Chart: '<Root>/Cross_Walk_Indication' */
#define Traffic_Li_IN_DO_NOT_WALK_GREEN ((uint8_T)2U)
#define Traffic_Ligh_IN_NO_ACTIVE_CHILD ((uint8_T)0U)
#define Traffic_Light_To_IN_CAUTION_OFF ((uint8_T)1U)
#define Traffic_Light_Tom_IN_CAUTION_ON ((uint8_T)2U)
#define Traffic_Light_Toms_Exa_IN_COUNT ((uint8_T)1U)
#define Traffic_Light_Toms__IN_WALK_RED ((uint8_T)4U)
#define Traffic__IN_WALK_CAUTION_YELLOW ((uint8_T)3U)

/* Named constants for Chart: '<Root>/Traffic_Light' */
#define Traffic_Light_Toms_E_IN_Caution ((uint8_T)1U)
#define Traffic_Light_Toms_Exam_IN_Stop ((uint8_T)3U)
#define Traffic_Light_Toms_Exampl_IN_Go ((uint8_T)2U)

/* Block signals (auto storage) */
B_Traffic_Light_Toms_Example_T Traffic_Light_Toms_Example_B;

/* Block states (auto storage) */
DW_Traffic_Light_Toms_Example_T Traffic_Light_Toms_Example_DW;

/* Previous zero-crossings (trigger) states */
PrevZCX_Traffic_Light_Toms_Ex_T Traffic_Light_Toms_Exam_PrevZCX;

/* Real-time model */
RT_MODEL_Traffic_Light_Toms_E_T Traffic_Light_Toms_Example_M_;
RT_MODEL_Traffic_Light_Toms_E_T *const Traffic_Light_Toms_Example_M =
  &Traffic_Light_Toms_Example_M_;

/* Model step function */
void Traffic_Light_Toms_Example_step(void)
{
  real_T rtb_PulseGenerator;
  ZCEventType zcEvent;
  boolean_T guard1 = false;

  /* DiscretePulseGenerator: '<Root>/Pulse Generator' */
  rtb_PulseGenerator = (Traffic_Light_Toms_Example_DW.clockTickCounter <
                        Traffic_Light_Toms_Example_P.PulseGenerator_Duty) &&
    (Traffic_Light_Toms_Example_DW.clockTickCounter >= 0) ?
    Traffic_Light_Toms_Example_P.PulseGenerator_Amp : 0.0;
  if (Traffic_Light_Toms_Example_DW.clockTickCounter >=
      Traffic_Light_Toms_Example_P.PulseGenerator_Period - 1.0) {
    Traffic_Light_Toms_Example_DW.clockTickCounter = 0;
  } else {
    Traffic_Light_Toms_Example_DW.clockTickCounter++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator' */

  /* Chart: '<Root>/Traffic_Light' incorporates:
   *  TriggerPort: '<S2>/Tick'
   */
  zcEvent = rt_ZCFcn(RISING_ZERO_CROSSING,
                     &Traffic_Light_Toms_Exam_PrevZCX.Traffic_Light_Trig_ZCE,
                     (rtb_PulseGenerator));
  if (zcEvent != NO_ZCEVENT) {
    Traffic_Light_Toms_Example_DW.presentTicks =
      Traffic_Light_Toms_Example_M->Timing.clockTick0;
    Traffic_Light_Toms_Example_DW.elapsedTicks =
      Traffic_Light_Toms_Example_DW.presentTicks -
      Traffic_Light_Toms_Example_DW.previousTicks;
    Traffic_Light_Toms_Example_DW.previousTicks =
      Traffic_Light_Toms_Example_DW.presentTicks;
    if (Traffic_Light_Toms_Example_DW.temporalCounter_i1 +
        Traffic_Light_Toms_Example_DW.elapsedTicks <= 15U) {
      Traffic_Light_Toms_Example_DW.temporalCounter_i1 = (uint8_T)
        (Traffic_Light_Toms_Example_DW.temporalCounter_i1 +
         Traffic_Light_Toms_Example_DW.elapsedTicks);
    } else {
      Traffic_Light_Toms_Example_DW.temporalCounter_i1 = 15U;
    }

    /* Gateway: Traffic_Light */
    /* Event: '<S2>:8' */
    /* During: Traffic_Light */
    if (Traffic_Light_Toms_Example_DW.is_active_c1_Traffic_Light_Toms == 0U) {
      /* Entry: Traffic_Light */
      Traffic_Light_Toms_Example_DW.is_active_c1_Traffic_Light_Toms = 1U;

      /* Entry Internal: Traffic_Light */
      /* Transition: '<S2>:18' */
      Traffic_Light_Toms_Example_DW.is_c1_Traffic_Light_Toms_Exampl =
        Traffic_Light_Toms_Exam_IN_Stop;
      Traffic_Light_Toms_Example_DW.temporalCounter_i1 = 0U;

      /* Entry 'Stop': '<S2>:4' */
      Traffic_Light_Toms_Example_B.red = 1U;
    } else {
      switch (Traffic_Light_Toms_Example_DW.is_c1_Traffic_Light_Toms_Exampl) {
       case Traffic_Light_Toms_E_IN_Caution:
        /* During 'Caution': '<S2>:6' */
        if (Traffic_Light_Toms_Example_DW.temporalCounter_i1 >= 8U) {
          /* Transition: '<S2>:11' */
          /* Exit 'Caution': '<S2>:6' */
          Traffic_Light_Toms_Example_B.yellow = 0U;
          Traffic_Light_Toms_Example_DW.is_c1_Traffic_Light_Toms_Exampl =
            Traffic_Light_Toms_Exam_IN_Stop;
          Traffic_Light_Toms_Example_DW.temporalCounter_i1 = 0U;

          /* Entry 'Stop': '<S2>:4' */
          Traffic_Light_Toms_Example_B.red = 1U;
        }
        break;

       case Traffic_Light_Toms_Exampl_IN_Go:
        /* During 'Go': '<S2>:5' */
        if (Traffic_Light_Toms_Example_DW.temporalCounter_i1 >= 10U) {
          /* Transition: '<S2>:10' */
          /* Exit 'Go': '<S2>:5' */
          Traffic_Light_Toms_Example_B.green = 0U;
          Traffic_Light_Toms_Example_DW.is_c1_Traffic_Light_Toms_Exampl =
            Traffic_Light_Toms_E_IN_Caution;
          Traffic_Light_Toms_Example_DW.temporalCounter_i1 = 0U;

          /* Entry 'Caution': '<S2>:6' */
          Traffic_Light_Toms_Example_B.yellow = 1U;
        }
        break;

       default:
        /* During 'Stop': '<S2>:4' */
        if (Traffic_Light_Toms_Example_DW.temporalCounter_i1 >= 10U) {
          /* Transition: '<S2>:9' */
          /* Exit 'Stop': '<S2>:4' */
          Traffic_Light_Toms_Example_B.red = 0U;
          Traffic_Light_Toms_Example_DW.is_c1_Traffic_Light_Toms_Exampl =
            Traffic_Light_Toms_Exampl_IN_Go;
          Traffic_Light_Toms_Example_DW.temporalCounter_i1 = 0U;

          /* Entry 'Go': '<S2>:5' */
          Traffic_Light_Toms_Example_B.green = 1U;
        }
        break;
      }
    }
  }

  /* Chart: '<Root>/Cross_Walk_Indication' */
  if (Traffic_Light_Toms_Example_DW.temporalCounter_i1_k < 3U) {
    Traffic_Light_Toms_Example_DW.temporalCounter_i1_k++;
  }

  /* Gateway: Cross_Walk_Indication */
  /* During: Cross_Walk_Indication */
  if (Traffic_Light_Toms_Example_DW.is_active_c2_Traffic_Light_Toms == 0U) {
    /* Entry: Cross_Walk_Indication */
    Traffic_Light_Toms_Example_DW.is_active_c2_Traffic_Light_Toms = 1U;

    /* Entry Internal: Cross_Walk_Indication */
    /* Transition: '<S1>:27' */
    Traffic_Light_Toms_Example_DW.is_c2_Traffic_Light_Toms_Exampl =
      Traffic_Light_Toms__IN_WALK_RED;

    /* Entry 'WALK_RED': '<S1>:1' */
    Traffic_Light_Toms_Example_B.WALK_CAUTION_LED = false;
    Traffic_Light_Toms_Example_B.WALK_LED = true;
  } else {
    guard1 = false;
    switch (Traffic_Light_Toms_Example_DW.is_c2_Traffic_Light_Toms_Exampl) {
     case Traffic_Light_Toms_Exa_IN_COUNT:
      /* During 'COUNT': '<S1>:36' */
      /* Transition: '<S1>:40' */
      /* Exit Internal 'COUNT': '<S1>:36' */
      /* Exit 'COUNT': '<S1>:36' */
      Traffic_Light_Toms_Example_DW.is_c2_Traffic_Light_Toms_Exampl =
        Traffic_Light_Toms__IN_WALK_RED;

      /* Entry 'WALK_RED': '<S1>:1' */
      Traffic_Light_Toms_Example_B.WALK_CAUTION_LED = false;
      Traffic_Light_Toms_Example_B.WALK_LED = true;
      break;

     case Traffic_Li_IN_DO_NOT_WALK_GREEN:
      /* During 'DO_NOT_WALK_GREEN': '<S1>:3' */
      if (Traffic_Light_Toms_Example_B.yellow == 1) {
        /* Transition: '<S1>:5' */
        Traffic_Light_Toms_Example_DW.is_c2_Traffic_Light_Toms_Exampl =
          Traffic__IN_WALK_CAUTION_YELLOW;

        /* Entry 'WALK_CAUTION_YELLOW': '<S1>:2' */
        Traffic_Light_Toms_Example_B.DO_NOT_WALK_LED = false;

        /* Entry Internal 'WALK_CAUTION_YELLOW': '<S1>:2' */
        /* Transition: '<S1>:32' */
        Traffic_Light_Toms_Example_DW.is_WALK_CAUTION_YELLOW =
          Traffic_Light_Tom_IN_CAUTION_ON;
        Traffic_Light_Toms_Example_DW.temporalCounter_i1_k = 0U;

        /* Entry 'CAUTION_ON': '<S1>:28' */
        Traffic_Light_Toms_Example_B.WALK_CAUTION_LED = true;
      }
      break;

     case Traffic__IN_WALK_CAUTION_YELLOW:
      /* During 'WALK_CAUTION_YELLOW': '<S1>:2' */
      if (Traffic_Light_Toms_Example_B.red == 1) {
        /* Transition: '<S1>:6' */
        if ((Traffic_Light_Toms_Example_DW.cnt1 <= 3.0) &&
            (Traffic_Light_Toms_Example_DW.cnt2 <= 3.0)) {
          /* Transition: '<S1>:55' */
          /* Exit Internal 'WALK_CAUTION_YELLOW': '<S1>:2' */
          Traffic_Light_Toms_Example_DW.is_WALK_CAUTION_YELLOW =
            Traffic_Ligh_IN_NO_ACTIVE_CHILD;
          Traffic_Light_Toms_Example_DW.is_c2_Traffic_Light_Toms_Exampl =
            Traffic_Light_Toms_Exa_IN_COUNT;

          /* Entry 'COUNT': '<S1>:36' */
          /* Entry Internal 'COUNT': '<S1>:36' */
          /* Entry 'cnt1': '<S1>:37' */
          Traffic_Light_Toms_Example_DW.cnt1++;

          /* Entry 'cnt2': '<S1>:38' */
          Traffic_Light_Toms_Example_DW.cnt2++;
        } else if ((Traffic_Light_Toms_Example_DW.cnt1 > 3.0) &&
                   (Traffic_Light_Toms_Example_DW.cnt2 > 3.0)) {
          /* Transition: '<S1>:56' */
          /* Exit Internal 'WALK_CAUTION_YELLOW': '<S1>:2' */
          Traffic_Light_Toms_Example_DW.is_WALK_CAUTION_YELLOW =
            Traffic_Ligh_IN_NO_ACTIVE_CHILD;
          Traffic_Light_Toms_Example_DW.is_c2_Traffic_Light_Toms_Exampl =
            Traffic_Light_Toms__IN_WALK_RED;

          /* Entry 'WALK_RED': '<S1>:1' */
          Traffic_Light_Toms_Example_B.WALK_CAUTION_LED = false;
          Traffic_Light_Toms_Example_B.WALK_LED = true;
        } else {
          guard1 = true;
        }
      } else {
        guard1 = true;
      }
      break;

     default:
      /* During 'WALK_RED': '<S1>:1' */
      if (Traffic_Light_Toms_Example_B.green == 1) {
        /* Transition: '<S1>:7' */
        Traffic_Light_Toms_Example_DW.is_c2_Traffic_Light_Toms_Exampl =
          Traffic_Li_IN_DO_NOT_WALK_GREEN;

        /* Entry 'DO_NOT_WALK_GREEN': '<S1>:3' */
        Traffic_Light_Toms_Example_B.WALK_LED = false;
        Traffic_Light_Toms_Example_B.DO_NOT_WALK_LED = true;
      }
      break;
    }

    if (guard1) {
      if (Traffic_Light_Toms_Example_DW.is_WALK_CAUTION_YELLOW ==
          Traffic_Light_To_IN_CAUTION_OFF) {
        /* During 'CAUTION_OFF': '<S1>:29' */
        if (Traffic_Light_Toms_Example_DW.temporalCounter_i1_k >= 1U) {
          /* Transition: '<S1>:31' */
          Traffic_Light_Toms_Example_DW.is_WALK_CAUTION_YELLOW =
            Traffic_Light_Tom_IN_CAUTION_ON;
          Traffic_Light_Toms_Example_DW.temporalCounter_i1_k = 0U;

          /* Entry 'CAUTION_ON': '<S1>:28' */
          Traffic_Light_Toms_Example_B.WALK_CAUTION_LED = true;
        }
      } else {
        /* During 'CAUTION_ON': '<S1>:28' */
        if (Traffic_Light_Toms_Example_DW.temporalCounter_i1_k >= 1U) {
          /* Transition: '<S1>:30' */
          Traffic_Light_Toms_Example_DW.is_WALK_CAUTION_YELLOW =
            Traffic_Light_To_IN_CAUTION_OFF;
          Traffic_Light_Toms_Example_DW.temporalCounter_i1_k = 0U;

          /* Entry 'CAUTION_OFF': '<S1>:29' */
          Traffic_Light_Toms_Example_B.WALK_CAUTION_LED = false;
        }
      }
    }
  }

  /* End of Chart: '<Root>/Cross_Walk_Indication' */

  /* Matfile logging */
  rt_UpdateTXYLogVars(Traffic_Light_Toms_Example_M->rtwLogInfo,
                      (&Traffic_Light_Toms_Example_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.5s, 0.0s] */
    if ((rtmGetTFinal(Traffic_Light_Toms_Example_M)!=-1) &&
        !((rtmGetTFinal(Traffic_Light_Toms_Example_M)-
           Traffic_Light_Toms_Example_M->Timing.taskTime0) >
          Traffic_Light_Toms_Example_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(Traffic_Light_Toms_Example_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++Traffic_Light_Toms_Example_M->Timing.clockTick0)) {
    ++Traffic_Light_Toms_Example_M->Timing.clockTickH0;
  }

  Traffic_Light_Toms_Example_M->Timing.taskTime0 =
    Traffic_Light_Toms_Example_M->Timing.clockTick0 *
    Traffic_Light_Toms_Example_M->Timing.stepSize0 +
    Traffic_Light_Toms_Example_M->Timing.clockTickH0 *
    Traffic_Light_Toms_Example_M->Timing.stepSize0 * 4294967296.0;
}

/* Model initialize function */
void Traffic_Light_Toms_Example_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)Traffic_Light_Toms_Example_M, 0,
                sizeof(RT_MODEL_Traffic_Light_Toms_E_T));
  rtmSetTFinal(Traffic_Light_Toms_Example_M, 100.0);
  Traffic_Light_Toms_Example_M->Timing.stepSize0 = 0.5;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    Traffic_Light_Toms_Example_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(Traffic_Light_Toms_Example_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(Traffic_Light_Toms_Example_M->rtwLogInfo, (NULL));
    rtliSetLogT(Traffic_Light_Toms_Example_M->rtwLogInfo, "tout");
    rtliSetLogX(Traffic_Light_Toms_Example_M->rtwLogInfo, "");
    rtliSetLogXFinal(Traffic_Light_Toms_Example_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(Traffic_Light_Toms_Example_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(Traffic_Light_Toms_Example_M->rtwLogInfo, 0);
    rtliSetLogMaxRows(Traffic_Light_Toms_Example_M->rtwLogInfo, 1000);
    rtliSetLogDecimation(Traffic_Light_Toms_Example_M->rtwLogInfo, 1);
    rtliSetLogY(Traffic_Light_Toms_Example_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(Traffic_Light_Toms_Example_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(Traffic_Light_Toms_Example_M->rtwLogInfo, (NULL));
  }

  /* block I/O */
  (void) memset(((void *) &Traffic_Light_Toms_Example_B), 0,
                sizeof(B_Traffic_Light_Toms_Example_T));

  /* states (dwork) */
  (void) memset((void *)&Traffic_Light_Toms_Example_DW, 0,
                sizeof(DW_Traffic_Light_Toms_Example_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(Traffic_Light_Toms_Example_M->rtwLogInfo, 0.0,
    rtmGetTFinal(Traffic_Light_Toms_Example_M),
    Traffic_Light_Toms_Example_M->Timing.stepSize0, (&rtmGetErrorStatus
    (Traffic_Light_Toms_Example_M)));

  /* Start for DiscretePulseGenerator: '<Root>/Pulse Generator' */
  Traffic_Light_Toms_Example_DW.clockTickCounter = 0;
  Traffic_Light_Toms_Exam_PrevZCX.Traffic_Light_Trig_ZCE = UNINITIALIZED_ZCSIG;

  /* SystemInitialize for Chart: '<Root>/Traffic_Light' */
  Traffic_Light_Toms_Example_DW.temporalCounter_i1 = 0U;
  Traffic_Light_Toms_Example_DW.is_active_c1_Traffic_Light_Toms = 0U;
  Traffic_Light_Toms_Example_DW.is_c1_Traffic_Light_Toms_Exampl =
    Traffic_Ligh_IN_NO_ACTIVE_CHILD;
  Traffic_Light_Toms_Example_B.red = 0U;
  Traffic_Light_Toms_Example_B.green = 0U;
  Traffic_Light_Toms_Example_B.yellow = 0U;
  Traffic_Light_Toms_Example_DW.presentTicks = 0U;
  Traffic_Light_Toms_Example_DW.elapsedTicks = 0U;
  Traffic_Light_Toms_Example_DW.previousTicks = 0U;

  /* SystemInitialize for Chart: '<Root>/Cross_Walk_Indication' */
  Traffic_Light_Toms_Example_DW.is_WALK_CAUTION_YELLOW =
    Traffic_Ligh_IN_NO_ACTIVE_CHILD;
  Traffic_Light_Toms_Example_DW.temporalCounter_i1_k = 0U;
  Traffic_Light_Toms_Example_DW.is_active_c2_Traffic_Light_Toms = 0U;
  Traffic_Light_Toms_Example_DW.is_c2_Traffic_Light_Toms_Exampl =
    Traffic_Ligh_IN_NO_ACTIVE_CHILD;
  Traffic_Light_Toms_Example_DW.cnt2 = 0.0;
  Traffic_Light_Toms_Example_DW.cnt1 = 0.0;
  Traffic_Light_Toms_Example_B.WALK_LED = false;
  Traffic_Light_Toms_Example_B.DO_NOT_WALK_LED = false;
  Traffic_Light_Toms_Example_B.WALK_CAUTION_LED = false;

  /* Enable for Chart: '<Root>/Traffic_Light' */
  Traffic_Light_Toms_Example_DW.presentTicks =
    Traffic_Light_Toms_Example_M->Timing.clockTick0;
  Traffic_Light_Toms_Example_DW.previousTicks =
    Traffic_Light_Toms_Example_DW.presentTicks;
}

/* Model terminate function */
void Traffic_Light_Toms_Example_terminate(void)
{
  /* (no terminate code required) */
}
