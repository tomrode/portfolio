/*
 * File: Traffic_Light_Toms_Example_types.h
 *
 * Code generated for Simulink model 'Traffic_Light_Toms_Example'.
 *
 * Model version                  : 1.14
 * Simulink Coder version         : 8.4 (R2013a) 13-Feb-2013
 * TLC version                    : 8.4 (Jan 19 2013)
 * C/C++ source code generated on : Thu Jan 02 12:20:46 2014
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: Microchip->PIC18
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Traffic_Light_Toms_Example_types_h_
#define RTW_HEADER_Traffic_Light_Toms_Example_types_h_
#include "rtwtypes.h"
#ifndef SS_LONG
#define SS_LONG                        14
#endif

#ifndef SS_ULONG
#define SS_ULONG                       15
#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM_Traffic_Light_Toms_Ex_T RT_MODEL_Traffic_Light_Toms_E_T;

#endif                                 /* RTW_HEADER_Traffic_Light_Toms_Example_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
