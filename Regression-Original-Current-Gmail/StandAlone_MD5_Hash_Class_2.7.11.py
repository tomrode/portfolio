import os, sys
import hashlib


class MD5Hash(object):

  # Constructor
  def __init__(self, C_fileToHash, HashValfile, CurrentHash, SavedHash):
    # instance variables
    self.C_fileToHash = C_fileToHash
    self.HashValfile = HashValfile
    self.CurrentHash = CurrentHash
    self.SavedHash = SavedHash

  # Accesser Methods (getters) and Mutator Method (setters)      
  def WriteHash(self):    
    f = open(self.HashValfile, "w")
    fhash = self.CurrentHash
    #print("This is the Saved Hash:", str(fhash))
    f.write(fhash)
    f.close()

  def ReadHash(self):
    f = open(self.HashValfile, 'r')
    self.SavedHash = f.read()
    #print("This is what was Saved Hash:", str(self.SavedHash))
    f.close()
    return self.SavedHash

  def MD5_Hash_Calc(self):
    BLOCKSIZE = 65536
    hasher = hashlib.md5()
    with open(self.C_fileToHash, 'rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    self.CurrentHash = hasher.hexdigest()
    #print("This is the Current Hash:", str(self.CurrentHash))
    return self.CurrentHash


def MD5HashDriver():

  # This list will be appended if a hash is different and returned to StartRegression.py
  Testname = []

  # List of changed files
  changed_files_list = []

  # List of missing .C files
  Missing_files_list = []

  # MD5 txt file suffix
  MD5HASHtxt = '_MD5Hash.txt'

  # Set base path
  CROWN_SOURCE_BASE = 'C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/'
 
  # Folder location for all Hashes
  md5_output_path =   'C:/vcast-buildsystem-orig/Regression-Original-Scripts/MD5Hashes/'

  # Folder where Gmail message text file saved at
  GmailMessageToMD5HASH = 'C:/vcast-buildsystem-orig/Regression-Original-Current-Gmail/'


  # All overall regressions lists
  all_regressions = [
                      #AECP 
                      [
                         #'C' file            path where the file resides                           batch file to add if Hash mis-match
                         ['aecp_main',        (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/aecp/'),    'AECP_MAIN_INTEGRATION.bat'],
                         ['imm_interface',    (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'     ),    'AECP_MAIN_INTEGRATION.bat'],
                         ['immcomm_driver',   (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/comm/'),    'AECP_MAIN_INTEGRATION.bat']
                      ],
                      #BECP 
                      [                         
                         ['becp_main',        (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/becp/'),    'BECP_MAIN_INTEGRATION.bat'],
                         ['supv_interface',   (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),         'BECP_MAIN_INTEGRATION.bat']                      
                      ],
                      #EVENT_MANAGER
                      [                          
                         ['EventManager',     (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),        'EVENT_MANAGER_INTEGRATION.bat'],
                         ['NvMemory',         (CROWN_SOURCE_BASE + 'VCM_APP/src/nvstorage/fram/'), 'EVENT_MANAGER_INTEGRATION.bat'],
                         ['OS_Interface',     (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),        'EVENT_MANAGER_INTEGRATION.bat'],
                         ['XcpUserFlash',     (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/xcp/usr/'),'EVENT_MANAGER_INTEGRATION.bat'],
                         ['crc',              (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),      'EVENT_MANAGER_INTEGRATION.bat'],
                         ['debug_support',    (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),      'EVENT_MANAGER_INTEGRATION.bat'],
                         ['imm_interface',    (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'     ),   'EVENT_MANAGER_INTEGRATION.bat'],
                         ['immcomm_driver',   (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/comm/'),   'EVENT_MANAGER_INTEGRATION.bat'],
                         ['instrument',       (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),      'EVENT_MANAGER_INTEGRATION.bat'],
                         ['io_thread',        (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/io/'),     'EVENT_MANAGER_INTEGRATION.bat'],
                         ['param',            (CROWN_SOURCE_BASE + 'VCM_APP/src/parameters/'),     'EVENT_MANAGER_INTEGRATION.bat'],
                         ['pd_data_test',     (CROWN_SOURCE_BASE + 'VCM_APP/src/nvstorage/'),      'EVENT_MANAGER_INTEGRATION.bat'],
                         ['watchdog',         (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),        'EVENT_MANAGER_INTEGRATION.bat']
                      ],
                      #SWITCHBOARD
                      [
                         #'C' file            path where the file resides                          batch file to add if Hash mis-match
                         ['ag_common',       (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen_API/'),     'SWITCH_BOARD_INTEGRATION.bat'],
                         ['ag_input_router', (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen_API/'),     'SWITCH_BOARD_INTEGRATION.bat'],
                         ['ag_output_router',(CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen_API/'),     'SWITCH_BOARD_INTEGRATION.bat'],
                         ['param',           (CROWN_SOURCE_BASE + 'VCM_APP/src/parameters/'),      'SWITCH_BOARD_INTEGRATION.bat'],
                         ['switchboard',     (CROWN_SOURCE_BASE + 'CommonSource/switch_board/'),   'SWITCH_BOARD_INTEGRATION.bat'] 
                      ],
                      #EXTERNAL_ALARM
                      [                         
                         ['external_alarm', (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/io/'),       'EXTERNAL_ALARM_INTEGRATION.bat']                    
                      ],
                      #GYRO_DRIVER
                      [                         
                         ['gyro_driver',    (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/gyro/'),     'GYRO_DRIVER_INTEGRATION.bat']                    
                      ],
                      #FEATURES_INTERFACE
                      [                         
                         ['features_interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/interface/'),    'FEATURES_INTERFACE_INTEGRATION.bat']                    
                      ],
                      #SUPV_INTERFACE
                      [                         
                         ['supv_interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),          'SUPV_INTERFACE_INTEGRATION.bat'],
                         ['features_interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/interface/'),    'SUPV_INTERFACE_INTEGRATION.bat'] 
                      ],
                      #IMM_INTERFACE
                      [                         
                         ['imm_interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),           'IMM_INTERFACE_INTEGRATION.bat']                    
                      ],
                      #OS_INTERFACE
                      [                         
                         ['OS_Interface', (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),            'OS_INTERFACE_INTEGRATION.bat']                    
                      ],
                      #STEERING
                      [                          
                         ['AssistCmd',            (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/AssistCmd/'),                   'STEERING_INTEGRATION.bat'],
                         ['Cmd_Pos_Integrator',   (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/Cmd_Pos_Integrator/'),          'STEERING_INTEGRATION.bat'],
                         ['HmsSim_Steering',      (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/HmsSim_Steering/'),             'STEERING_INTEGRATION.bat'],
                         ['HmsSim_SteeringBuild', (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/HmsSim_SteeringBuild_ert_rtw/'),'STEERING_INTEGRATION.bat'],
                         ['PosCmd',               (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/PosCmd/'),                      'STEERING_INTEGRATION.bat'],
                         ['PosCmd_data',          (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/PosCmd/'),                      'STEERING_INTEGRATION.bat'],
                         ['SpdCmd',               (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/SpdCmd/'),                      'STEERING_INTEGRATION.bat'],
                         ['SteeringSIM',          (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/SteeringSIM/'),                 'STEERING_INTEGRATION.bat'],
                         ['SteeringSIMbuild',     (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/SteeringSIMbuild_ert_rtw/'),    'STEERING_INTEGRATION.bat'],
                         ['StrMstrApp',           (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrMstrApp/'),                  'STEERING_INTEGRATION.bat'],
                         ['StrMstrSup',           (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrMstrSup/'),                  'STEERING_INTEGRATION.bat'],
                         ['StrMtrSpdLmt',         (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrMtrSpdLmt/'),                'STEERING_INTEGRATION.bat'],
                         ['StrPosHndlSpdSense',   (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrPosHndlSpdSense/'),          'STEERING_INTEGRATION.bat'],
                         ['StrPosWhAngLimiter',   (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrPosWhAngLimiter/'),          'STEERING_INTEGRATION.bat'],
                         ['StrPos_SahNmbns',      (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrPos_SahNmbns/'),             'STEERING_INTEGRATION.bat'],
                         ['StrPos_SetPntRateLmtr',(CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/StrPos_SetPntRateLmtr/'),       'STEERING_INTEGRATION.bat'],
                         ['TfdCurrentSpProfile',  (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TfdCurrentSpProfile/'),         'STEERING_INTEGRATION.bat'],
                         ['TfdOpMode',            (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TfdOpMode/'),                   'STEERING_INTEGRATION.bat'],
                         ['TfdOvrMode',           (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TfdOvrMode/'),                  'STEERING_INTEGRATION.bat'],
                         ['Til2DuScaling',        (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/Til2DuScaling/'),               'STEERING_INTEGRATION.bat'],
                         ['TrxSpd_StrMtrSpdLmt',  (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TrxSpd_StrMtrSpdLmt/'),         'STEERING_INTEGRATION.bat'],
                         ['WhAngErr_StrMtrSpdLmt',(CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/WhAngErr_StrMtrSpdLmt/'),       'STEERING_INTEGRATION.bat'],
                         ['WhAng_StrMtrSpdLmt',   (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/WhAng_StrMtrSpdLmt/'),          'STEERING_INTEGRATION.bat']
                      ],
                      #IP_INTERFACE
                      [                         
                         ['IP_Interface',         (CROWN_SOURCE_BASE + 'VCM_APP/src/general/'),                                   'IP_INTERFACE_INTEGRATION.bat']                    
                      ],
                      #MANIFEST_MANAGER
                      [                         
                         ['Manifest_mgr',         (CROWN_SOURCE_BASE + 'VCM_APP/src/interface/'),                                 'MANIFEST_MANAGER_INTEGRATION.bat']                    
                      ],
                      #TRACTION
                      [                         
                         ['GetSpeed',              (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/GetSpeed/'),                   'TRACTION_INTEGRATION.bat'],
                         ['OnTrac',                (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/OnTrac/'),                     'TRACTION_INTEGRATION.bat'],
                         ['PIControlLoop',         (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/PIControlLoop/'),              'TRACTION_INTEGRATION.bat'],
                         ['ParameterDiagnostics',  (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/ParameterDiagnostics/'),       'TRACTION_INTEGRATION.bat'],
                         ['ThrottleSpdCmd',        (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/ThrottleSpdCmd/'),             'TRACTION_INTEGRATION.bat'],
                         ['TorqueLimiter',         (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TorqueLimiter/'),              'TRACTION_INTEGRATION.bat'],
                         ['TractionSIM',           (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TractionSIM/'),                'TRACTION_INTEGRATION.bat'],
                         ['TractionSIMbuild',      (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/TractionSIMbuild_ert_rtw/'),   'TRACTION_INTEGRATION.bat'],
                         ['Traction_State_Machine',(CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/Traction_State_Machine/'),     'TRACTION_INTEGRATION.bat'],
                         ['accel_decel_slew',      (CROWN_SOURCE_BASE + 'VCM_APP/src/Autogen/SIMu1/accel_decel_slew/'),           'TRACTION_INTEGRATION.bat']             
                      ],
                      #FRAM_DRIVER                      
                      [                         
                         ['fram_driver',           (CROWN_SOURCE_BASE + 'VCM_APP/src/drivers/fram/'),                             'FRAM_DRIVER_INTEGRATION.bat'],
                         ['NvMemory',              (CROWN_SOURCE_BASE + 'VCM_APP/src/nvstorage/fram/'),                           'FRAM_DRIVER_INTEGRATION.bat']
                      ],
                      #UTILITIES                      
                      [                         
                         ['app_info',              (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['crc',                   (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['debug_support',         (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['instrument',            (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['stopwatch',             (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat'],
                         ['util',                  (CROWN_SOURCE_BASE + 'VCM_APP/src/utilities/'),                                'UTILITIES_INTEGRATION_GEN2.bat']
                      ]

                  ]

     
  while(1): 
    selection = str(input("Type '1' to calculate and save hash, '2' to see if saved hash and current has match, '3' Email hash calculate: "))

    # Writing Hash for email message ititially only needed once in theory
    if selection == '3':
        
        # assingment of the class to this variable with correspond file and output MD5 .txt file
        regression = MD5Hash((GmailMessageToMD5HASH + 'GmailMessage.txt'), (md5_output_path + 'GmailMessageNowHashed' + MD5HASHtxt) , None, None) 
        regression.MD5_Hash_Calc()
        regression.WriteHash()


    # Hash Writing only needed once in theory 
    if selection == '1':
       
        i = 0
        for element in all_regressions:          
            j = 0
            for line in all_regressions[i]:
                
                # assingment of the class to this variable with correspond 'C' file and output MD5 .txt file
                regression = MD5Hash((all_regressions[i][j][1] + all_regressions[i][j][0] + '.c'), (md5_output_path + all_regressions[i][j][0] + MD5HASHtxt) , None, None)                    
                regression.MD5_Hash_Calc()
                regression.WriteHash()
                j = j + 1
            i = i + 1
                                                
    # Hash Reading this will be mostly used                               
    if selection == '2':
        CurrentBatRun = None
        LastBatRun = None        
        fFirstrun = True
        fChangedfileWrite = False
        fMissingCfileWrite = False
        fTestnameToRunWrite = False
        i = 0
        
        for element in all_regressions:          
            j = 0
            for line in all_regressions[i]:
                # assingment of the class to this variable with correspond 'C' file and output MD5 .txt file
                try:
                    regression = MD5Hash((all_regressions[i][j][1] + all_regressions[i][j][0] + '.c'), (md5_output_path + all_regressions[i][j][0] + MD5HASHtxt) , None, None)
            
                    # calculate and read saved Hashes 
                    CurrentHash = regression.MD5_Hash_Calc()
                    SavedHash = regression.ReadHash()
                except:
                    # Allow missing .C files list to be written
                    print("This file missing", all_regressions[i][j][0])
                    fMissingCfileWrite = True
                    
                    # append the list of missing .C files and a new line escape sequence 
                    Missing_files_list.append(all_regressions[i][j][0])
                    Missing_files_list.append('\n')
                     

                # is the current and saved hash value the same the remove the test from the overall list?
                if CurrentHash == SavedHash:
                    print("Current and saved Hashes match", all_regressions[i][j][0])
                               
                else:
                    # Allow what files hashes have changed list to write 
                    print("Current and saved Hashes Do Not match in:", all_regressions[i][j][0])
                    fChangedfileWrite = True 
                   
                    # Append and place to a new line escape sequence 
                    changed_files_list.append(all_regressions[i][j][0])
                    changed_files_list.append('\n')
                    
                    if fFirstrun == True:
                        # initialy append this list with first value 
                        fFirstrun = False
                        
                        # load initially the value from first C file line from this list add new line escape sequence and allow what test to run list to be written
                        fTestnameToRunWrite = True
                        LastBatRun = all_regressions[i][j][2]
                        Testname.append(LastBatRun)
                        Testname.append('\n')
                        
                    else:
                        CurrentBatRun = all_regressions[i][j][2]                        
                        if CurrentBatRun != LastBatRun:
                          
                            #if there is a difference then append Testname list add new line escape sequence                            
                            Testname.append(CurrentBatRun)
                            Testname.append('\n')
                            
                        # update the last bat file for next run through      
                        LastBatRun = CurrentBatRun
                        
                j = j + 1                
            i = i + 1
            
        
        # Store fault data in a text files if flagged to 
        if  fChangedfileWrite == True:  
            f = open('changed_files_list.txt', "w")
            f.writelines(changed_files_list)
            f.close()

        if fTestnameToRunWrite == True: 
            f = open('Testname.txt', "w")
            f.writelines(Testname)
            f.close()

        if fMissingCfileWrite == True:
            f = open('Missing_files_list.txt', "w")
            f.writelines(Missing_files_list)
            f.close()
                                           
        print("\n")       
        print("List of source files with non-matching hashes",changed_files_list)
        print("List of in-correct hashes, these Regressions to be run", Testname)
        print("List of missing .C files",Missing_files_list)
        print("\n")   

      
if __name__ == '__main__':
  MD5HashDriver()
 
