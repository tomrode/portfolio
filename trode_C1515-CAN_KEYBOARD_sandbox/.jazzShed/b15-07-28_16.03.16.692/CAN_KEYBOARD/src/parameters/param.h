 /************************************************************************************************
 *  File:       parm.h
 *
 *  Purpose:    Defines the settable parameters for the truck
 *
 *  Project:    C1515
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey, Tom Rode
 *
 *  Revision History:
 *              Written 11/29/2005
 *              Revised 01/29/2015 
 *
 ************************************************************************************************
*/
#include "DSP2803x_Device.h"
#include "types.h"
#include "IQmathLib.h"
#include "utf_8.h"

#ifndef param_registration

    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/

#define NUM_OF_SV_PWMS 2

    
typedef struct
{
    uint32_t ulEnglishTables[4u][55u];                           /**< Four sub tables per langauge, all key switch Tables are maxed out at 5 rows and 11 columns = 55 4 byte utf8 characters*/
    uint32_t ulGermanTables[4u][55u];                            /**< Four sub tables per langauge, all key switch Tables are maxed out at 5 rows and 11 columns = 55 4 byte utf8 characters*/
    uint32_t ulSpanishTables[4u][55u];                           /**< Four sub tables per langauge, all key switch Tables are maxed out at 5 rows and 11 columns = 55 4 byte utf8 characters*/      
    uint32_t ulLang4Tables[4u][55u];                             /**< Four sub tables per langauge, all key switch Tables are maxed out at 5 rows and 11 columns = 55 4 byte utf8 characters*/
	uint32_t ulLang5Tables[4u][55u];                             /**< Four sub tables per langauge, all key switch Tables are maxed out at 5 rows and 11 columns = 55 4 byte utf8 characters*/  
	uint32_t ulLang6Tables[4u][55u];                             /**< Four sub tables per langauge, all key switch Tables are maxed out at 5 rows and 11 columns = 55 4 byte utf8 characters*/  
	uint32_t ulLang7Tables[4u][55u];                             /**< Four sub tables per langauge, all key switch Tables are maxed out at 5 rows and 11 columns = 55 4 byte utf8 characters*/  
	uint32_t ulLang8Tables[4u][55u];                             /**< Four sub tables per langauge, all key switch Tables are maxed out at 5 rows and 11 columns = 55 4 byte utf8 characters*/  
	uint32_t ulLang9Tables[4u][55u];                             /**< Four sub tables per langauge, all key switch Tables are maxed out at 5 rows and 11 columns = 55 4 byte utf8 characters*/  
}multi_dimension_ar_t;
      
 /** @struct  Keyboard_Switch_Hardware_Check 
 *  @brief   Hardware switch ckeck parameter in sector H in FLASH.
 * 
 *  @typedef Keyboard_Switch_Hardware_Check_t
 *  @brief   Switch check for Hardware
 */     
typedef struct Keyboard_Switch_Hardware_Check
{
	uint32_t ulHardwareSwCheckTable[55u];                       /**< Hardware switch check table */            
}Keyboard_Switch_Hardware_Check_t;

/** @struct  keyboard driver 
 *  @brief   Driver settings of this parameter in sector H in FLASH.
 * 
 *  @typedef keyboard_timing_t
 *  @brief   keyboard timing type definition
 */
typedef struct
{
     /*
    @@ ELEMENT = uwFaultMatureTime
    @@ STRUCTURE = keyboard_timing_t
    @@ DATA_TYPE = UWORD
    @@ DESCRIPTION = "Stuck or shorted key fault maturation time"
    @@ END
    */ 
    uint16_t                      uwFaultMatureTime;              /**< Fault maturation time if key stuck or shorted */    

    /*
    @@ ELEMENT = ubRows
    @@ STRUCTURE = keyboard_driver_t
    @@ DATA_TYPE = UBYTE
    @@ DESCRIPTION = "Number of Hardware Row Switches"
    @@ END
    */ 
    uint8_t                      ubRows;                           /**< The amount of key board switch rows */   

    /*
    @@ ELEMENT = ubColumns
    @@ STRUCTURE = keyboard_driver_t
    @@ DATA_TYPE = UBYTE
    @@ DESCRIPTION = "Number of Hardware column Switches"
    @@ END
    */ 
    uint8_t                      ubColumns;                          /**< The amount of column keys */   

     /*
    @@ ELEMENT = ubCharOnPressed
    @@ STRUCTURE = keyboard_driver_t
    @@ DATA_TYPE = UBYTE
    @@ DESCRIPTION = "Output the character on button pressed or released "
    @@ END
    */ 
    uint8_t                      ubCharOnPressed;                    /**< Output character on pressed or released */    

      /*
    @@ ELEMENT = ubLangTablesCnt
    @@ STRUCTURE = keyboard_driver_t
    @@ DATA_TYPE = UBYTE
    @@ DESCRIPTION = " This is the amount of language table being used "
    @@ END
    */ 
    uint8_t                      ubLangTablesCnt;                    /**< This is the amount of language tables sets, this Variable used for protection of in case 0x276 message is wrong */   

     /*
    @@ ELEMENT = ubDSP28035
    @@ STRUCTURE = keyboard_driver_t
    @@ DATA_TYPE = UBYTE
    @@ DESCRIPTION = " This is used for Hardware Abstraction "
    @@ END
    */ 
    uint8_t                      ubDSP28035;                    /**< This parameter is for hardware abstarction using 28035DSP micro.  */
}keyboard_driver_t;  

/** @struct  language tables
 *  @brief   UTF-8 characters of the parameter sector H in FLASH.
 *
 *  @typedef language_tables_t
 *  @brief   language_tables type definition
 */
typedef struct
{               
    multi_dimension_ar_t               LangTableAr;                           /**< Language arrays */ 
	Keyboard_Switch_Hardware_Check_t   HwSwChk;                               /**< Hardware switch check */
    keyboard_driver_t                  Kt;                                    /**< Hardware key parameters */ 
}param_type_t;



#endif

#ifdef PARAM_MODULE

    #ifndef param_registration_local
    #define param_registration_local 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/

    #endif


#else
    #ifndef param_registration
    #define param_registration 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/

    extern const param_type_t gParams;


    #endif
#endif

