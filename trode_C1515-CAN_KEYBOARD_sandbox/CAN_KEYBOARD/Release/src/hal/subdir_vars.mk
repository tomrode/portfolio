################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/hal/Adc.c \
../src/hal/Gpio.c \
../src/hal/IO_interface.c \
../src/hal/SPI.c \
../src/hal/ramtron.c 

OBJS += \
./Adc.obj \
./Gpio.obj \
./IO_interface.obj \
./SPI.obj \
./ramtron.obj 

C_DEPS += \
./src/hal/Adc.pp \
./src/hal/Gpio.pp \
./src/hal/IO_interface.pp \
./src/hal/SPI.pp \
./src/hal/ramtron.pp 

C_DEPS__QUOTED += \
"src\hal\Adc.pp" \
"src\hal\Gpio.pp" \
"src\hal\IO_interface.pp" \
"src\hal\SPI.pp" \
"src\hal\ramtron.pp" 

OBJS__QUOTED += \
"Adc.obj" \
"Gpio.obj" \
"IO_interface.obj" \
"SPI.obj" \
"ramtron.obj" 

C_SRCS__QUOTED += \
"../src/hal/Adc.c" \
"../src/hal/Gpio.c" \
"../src/hal/IO_interface.c" \
"../src/hal/SPI.c" \
"../src/hal/ramtron.c" 


