################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/CodeStartBranch.asm \
../src/ParamFlag.asm \
../src/SetDBGIER.asm 

C_SRCS += \
../src/PieCtrl.c \
../src/Scheduler.c \
../src/SysCtrl.c 

OBJS += \
./CodeStartBranch.obj \
./ParamFlag.obj \
./PieCtrl.obj \
./Scheduler.obj \
./SetDBGIER.obj \
./SysCtrl.obj 

ASM_DEPS += \
./src/CodeStartBranch.pp \
./src/ParamFlag.pp \
./src/SetDBGIER.pp 

C_DEPS += \
./src/PieCtrl.pp \
./src/Scheduler.pp \
./src/SysCtrl.pp 

C_DEPS__QUOTED += \
"src\PieCtrl.pp" \
"src\Scheduler.pp" \
"src\SysCtrl.pp" 

OBJS__QUOTED += \
"CodeStartBranch.obj" \
"ParamFlag.obj" \
"PieCtrl.obj" \
"Scheduler.obj" \
"SetDBGIER.obj" \
"SysCtrl.obj" 

ASM_DEPS__QUOTED += \
"src\CodeStartBranch.pp" \
"src\ParamFlag.pp" \
"src\SetDBGIER.pp" 

ASM_SRCS__QUOTED += \
"../src/CodeStartBranch.asm" \
"../src/ParamFlag.asm" \
"../src/SetDBGIER.asm" 

C_SRCS__QUOTED += \
"../src/PieCtrl.c" \
"../src/Scheduler.c" \
"../src/SysCtrl.c" 


