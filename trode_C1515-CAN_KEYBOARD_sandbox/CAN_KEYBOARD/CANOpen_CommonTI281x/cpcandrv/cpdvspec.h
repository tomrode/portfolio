/*--------------------------------------------------------------------
       CPDVSPEC.H
  --------------------------------------------------------------------
       Copyright (C) 1995-1997 Vector Informatik GmbH, Stuttgart

       Programmer:    Dipl.Phys. J. Kl�ser (kl),
		      Vector Informatik GmbH, Stuttgart

       Function:      CAN driver for CANopen tools
       Modulversion:
       1.0  15.11.95: Creation
       1.1  01.04.96: new function cpDriverName
       1.2  17.04.96: new function cpGetIniSectionName
       1.3  10.03.97: new drivers
       1.4  19.06.97: additional error codes
  --------------------------------------------------------------------*/
#ifndef __CPDVSPEC_H
#define __CPDVSPEC_H

#ifdef __cplusplus
 extern "C" {
#endif


#ifndef __DRVSPEC_H__
typedef enum {
	DV_DBB196    = 0,			/* Daimler-Benz-Board FullCAN */
	DV_DBB196B   = 1,			/* Daimler-Benz-Board BasicCAN */
	DV_CANIB     = 2,			/* Bosch CANIB */
	DV_DEMO      = 3,			/* Demo Treiber, Dummy ohne Board */
	DV_CANAC     = 4,			/* Softing CAN-Application-Controller */
	DV_EMUANA    = 5,			/* Softing Emulator/Analyzer */
	DV_CANAC2    = 6,			/* Softing CAN-Application-Controller 2 */
	DV_CANAC2X   = 7,			/* Softing CAN-AC 2 with i527*/
	DV_CPC       = 8,			/* CPC-PP from EMS W�nsche */
	DV_INDIGO    = 9,			/* Silicon Graphics Indigo2 */
	DV_CANCARD   = 10,		/* CANcard PCMCIA 11 Bit */
	DV_CANCARDX  = 11,		/* CANcard PCMCIA 29 Bit */
	DV_CANAC2B   = 12,		/* Softing CANAC2-527 but only 11 Bit */
	DV_VAN462    = 13,		/* NSI VAN card */
	DV_VANDEMO   = 14,		/* NSI VAN Demo */
   DV_PCANDONGLE = 15,     /* Peak CAN-Dongle */
   DV_VCANDONGLE = 16,     /* Vector CAN-Dongle */
   DV_VCANCARD   = 17,     /* Vector CAN-Card */
   DV_VIRTUAL    = 18,     /* Virtueller CAN-Treiber */
   DV_LEUKHARDT_SAT_CARD = 0x100+0, /* Sektionsname:  LEUKHARDT_SAT_CARD */
   DV_LEUKHARDT_PC_ISA_CAN = 0x100+1, /* Sektionsname: LEUKHARDT_PC_ISA_CAN */
} VCpDriver;
#else
typedef dvDriver_t VCpDriver;
#endif

typedef struct {
	short int		swVersion;		/* driver software version */
	short int		fwVersion;		/* firmware version */
	short int 	hwVersion;		/* hardware version */
} VCpVersion;

typedef unsigned long VCpHandle;

typedef struct {
	unsigned long	id;
	unsigned short rtr;
	unsigned short dlc;
	unsigned char	data[8];
} VCpMessage;

#ifndef __DRVSPEC_H__
typedef enum {
  ERR_INI_TIMEOUT    = 0,             /* timeout during board initialization */
  ERR_RX_QUEUE_EMPTY = 1,             /* no events in the rx queue */
  ERR_NO_EVENT       = ERR_RX_QUEUE_EMPTY,
  ERR_TX_QUEUE_FULL  = 2,             /* tx queue full, tx request refused */
  ERR_UNKNOWN_CONTR  = 3,             /* unknown Controller-Nr. */
  ERR_OP_TIMEOUT     = 4,             /* timeout during command */
  ERR_DPR_OVERFLOW   = 5,             /* DPRAM-Overflow */
  ERR_WRONG_COMMAND  = 6,             /* not allowed event in dvPutCommand */
  ERR_WRONG_CARD     = 8,             /* driver detected another hardware (see CANIB) */

  ERR_WRONG_CHIP_TYPE = 9,            /* parameter error in dvMeasureInit */
  ERR_NO_CAN_2        = 10,           /* parameter error in dvMeasureInit and dvPutCommand */
  ERR_NOT_IMPLEMENTED = 11,           /* not (yet) implemented function in this version of driver */

  ERR_NO_IMP_ACC      = 12,           /* 82526: no access to imp */
  ERR_NO_TRANSFER     = 14,           /*   "  : last msg wasn't transferred */

  ERR_INIT_TWICE      = 15,           /* driver already initialized */

  ERR_SEND_ID        = 100,           /* unknown send id (FullCAN only) */
  ERR_RX_QUEUE_OVR   = 101,           /* rx queue overrun */
  ERR_BUSOFF         = 102,           /* chip state busoff */
  ERR_ERROR_PASSIVE  = 103,           /* chip state error passive */
  ERR_ERROR_ACTIVE   = 104,           /* chip state error active */
  ERR_RX_REG_OVR     = 105,           /* rx register overrrun (BasicCan only) */

  ERR_CHIP_NO_ANSWER = 106,           /* at bootup the firmware couldn't access the controller */
  ERR_ADR_NOT_VALID  = 107,           /* no valid dpram address in dvBoardInit */

  ERR_NO_INT_REC     = 108,           /* no interrupt from CANIB received */
  ERR_MODULNR        = 109,           /* wrong modulnumber                */
  ERR_POINTER        = 110,           /* wrong pointer to source buffer   */
  ERR_ADDRESS        = 111,           /* address > CANIB_SRAM_SIZE        */
  ERR_SIZE           = 112,           /* address + size > CANIB_SRAM_SIZE */
  ERR_WRONG_DEVICE   = 113,           /* CAN-Nr. <> 0 && CAN-Nr. <> 1     */
  ERR_MAX_ENTRY_SIZE   = 114,         /* FIFO-Entry > 16 Byte             */

  /* codes 200..455 are reserved for CANIB-Driver */

  ERR_CAN	     = 216,           /*          */
  ERR_TIMEOUT_CPU_ACC= 217,           /* Enable CPU not successful */
  ERR_TRANSFER_STAT  = 218,           /* Set of TR-Status Bit not successful */
  ERR_CAN0_NOHALT    = 219,           /* Halt  command not successful */
  ERR_CAN1_NOHALT    = 220,           /* Halt  command not successful */
  ERR_CAN0_NORESET   = 221,           /* Reset command not successful */
  ERR_CAN1_NORESET   = 222,           /* Reset command not successful */

  ERR_FIFO_TIME_OUT  = 232,           /* Timeout waiting for Data from FIFO */

  ERR_UNKNOWN_COMMAND  = 248,         /* Unknown command */
  ERR_UNKNOWN_FUNCTION = 249,         /* Unknown function */
  ERR_WRONG_FORMAT     = 250,         /* Wrong parameter format */
  ERR_WRONG_PARAMETER  = 251,         /* Wrong parameter */
  ERR_UNEXP_OK_MESS    = 252,         /* OK-message while waiting for data */

  ERR_UNKNOWN_ERROR    = 455,          /* Unknown answer from CANIB */

  ERR_WRONG_REGISTER   = 500,          /* wrong registration */
  ERR_FW_VERSION       = 501,          /* invalid firmware version */
  ERR_FILE_IO          = 502,          /* file not found, not opened etc. */

  ERR_BUSLOAD_EVENT    = 999,          /* pseudo error for busload */

  /* codes 1000-2999 are reserved for CANIB-Driver */
  ERR_WRONG_OBJECTID    = 3000,        /* id out of range for basic can or
                                          not set for object buffer */
  ERR_NO_MEM            = 3001,        /* no mem with dvMalloc */
  ERR_ALREADY_LOCKED   = 3010,         /* driver is locked by another application */
  ERR_INVALID_HANDLE   = 3011,         /* invalid access handle */
  ERR_DRV_NOTSUPPORTED = 3012,         /* driver not supported */
  ERR_CALL_CARDINST    = 3013          /* cardinst not called */
} VCpError;
#else
 typedef dvErrCode_t VCpError;
#endif



#ifdef __BORLANDC__
 #define EXPORT2 _export FAR
 #define EXPORT1
EXPORT1 VCpDriver EXPORT2 pascal cpDriverType(void);
EXPORT1 const char FAR * EXPORT2 pascal cpDriverName(void);
EXPORT1 int EXPORT2 pascal cpGetVersion(VCpVersion FAR *version);
EXPORT1 int EXPORT2 pascal cpBoardInit(void);
EXPORT1 int EXPORT2 pascal cpShutDown(void);
EXPORT1 VCpHandle EXPORT2 pascal cpLock(void);
EXPORT1 void EXPORT2 pascal cpUnlock(VCpHandle handle);
EXPORT1 int EXPORT2 pascal cpSetAcceptanceFilter(VCpHandle handle,
      unsigned int code, unsigned int mask,
      unsigned long codeXtd, unsigned long maskXtd);
EXPORT1 int EXPORT2 pascal cpMeasureInit(VCpHandle handle);
EXPORT1 int EXPORT2 pascal cpMeasureStart(VCpHandle handle);
EXPORT1 int EXPORT2 pascal cpMeasureStop(VCpHandle handle);
EXPORT1 int EXPORT2 pascal cpSendMessage( VCpHandle handle, VCpMessage FAR *msg);
EXPORT1 int EXPORT2 pascal cpGetMessage(VCpHandle handle, VCpMessage FAR *msg);
EXPORT1 VCpError EXPORT2 pascal cpError(VCpHandle handle);
EXPORT1 const char* EXPORT2 pascal cpGetIniSectionName(void);
#endif

#ifdef _MSC_VER // Parameter List N O T turned
typedef  VCpDriver (pascal *cpDriverType)(void);
typedef  const char FAR *   (pascal *cpDriverName)(void);
typedef  int   (pascal *cpGetVersion)(VCpVersion FAR *version);
typedef  int   (pascal *cpBoardInit)(void);
typedef  int   (pascal *cpShutDown)(void);
typedef  VCpHandle   (pascal *cpLock)(void);
typedef  void   (pascal *cpUnlock)(VCpHandle handle);
typedef  int   (pascal *cpSetAcceptanceFilter)(VCpHandle handle,
                                                unsigned int code,
                                                unsigned int mask,
                                                unsigned long codeXtd,
                                                unsigned long maskXtd);
typedef  int   (pascal *cpMeasureInit)(VCpHandle handle);
typedef  int   (pascal *cpMeasureStart)(VCpHandle handle);
typedef  int   (pascal *cpMeasureStop)(VCpHandle handle);
typedef  int   (pascal *cpSendMessage)(VCpHandle handle, VCpMessage FAR *msg);
typedef  int   (pascal *cpGetMessage)(VCpHandle handle, VCpMessage FAR *msg);
typedef  VCpError   (pascal *cpError)(VCpHandle handle);
typedef  const char*   (pascal *cpGetIniSectionName)(void);
#endif


#ifdef __cplusplus
 }
#endif




#endif

