/*--------------------------------------------------------------------
       BASICOM.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Basic protocol stack routines
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>

#include "portab.h"
#include "cos_main.h"
#include "vassert.h"
#include "vdbgprnt.h"
#include "target.h"
#include "cancntrl.h"
#include "timerdef.h"
#include "objdict.h"
#include "sdoserv.h"
#include "emcyhndl.h"
#include "profil.h"
#include "buffer.h"
#include "usrclbck.h"
#include "pdodef.h"

#if NMT_MASTER_ENABLE == 1
# include "nmtms.h"
#endif /* NMT_MASTER_ENABLE == 1 */

#if CLIENT_ENABLE == 1
# include "sdoclnt.h"
#endif /* CLIENT_ENABLE == 1 */

#if ENABLE_LAYER_SERVICES == 1
# if ENABLE_LSS_MASTER == 1
#  include "lss_mst.h"
# else
#  include "lss_slv.h"
# endif /* ENABLE_LSS_MASTER == 1 */
#endif /* ENABLE_LAYER_SERVICES == 1 */

#if STORE_PARAMETERS_SUPP == 1
# include "nonvolst.h"
#endif /* STORE_PARAMETERS_SUPP == 1 */

#if DELAYED_SDO_ENABLE == 1
# include "sdodlyac.h"
#endif /* DELAYED_SDO_ENABLE == 1 */

#if PROFILE_DS401 == 1
# include "ds401.h"
#endif /* PROFILE_DS401 == 1 */

#if STATUS_LEDS_USED == 1
# include "co_led.h"
#endif /* STATUS_LEDS_USED == 1 */


/*--------------------------------------------------------------------*/
/*  local definitions                                                 */
/*--------------------------------------------------------------------*/
#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
# if SDO_SERVER2_DEFAULT_ENABLE == 1
#  define SDO_SERVER2_DEFAULT_CONF 1
# else
#  define SDO_SERVER2_DEFAULT_CONF 0
# endif /* SDO_SERVER2_DEFAULT_ENABLE == 1 */
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */

int gswCAN_Controller_OverrunCount = 0;
int gswCAN_Stack_OverrunCount = 0;
unsigned long gulCAN_Overrun_Zone = 0;
int gswCAN_Stack_Buffer_Number = 0;
unsigned long gulCAN_Stack_Buffer_ID = 0;

/*--------------------------------------------------------------------*/
/*  external functions                                                */
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
#if EXAMPLE_OBJECTS_USED == 1
extern BYTE gDomainBuffer[EXAMPLE_BUFFER_SIZE];
#endif /* EXAMPLE_OBJECTS_USED == 1 */
extern vEmcyManage XDATA EmcyTable; /* EMCY information */

/* externals from buf_____.c */
extern FAR CAN_BUF XDATA tCanMsgBuffer[MAX_CAN_BUFFER]; /* CAN message buffer */

/* external from SDOCLNT.C */
extern VSdoAttrib ClientSdo;

/* external from SDOSERV.C */
extern VSdoAttrib XDATA ServerSdo[NUM_SDO_SERVERS];

/* externals from objvars.h */
#if OBJECT_1029_USED == 1
/* location of error behavior (Index 1029h) */
extern BYTE abErrorBehavior[];
#endif /* OBJECT_1029_USED == 1 */

#if QUEUED_MODE == 1
# if SIGNAL_EMCYMSG == 1
/* limits to signal messages in EMCY range */
extern CONST VSignalIdArea CODE SignalEmcy[1];
# endif /* SIGNAL_EMCYMSG == 1 */

# if SIGNAL_BOOTUPMSG == 1
/* limits to signal messages in error control range */
extern CONST VSignalIdArea CODE SignalBootUp[1];
# endif /* SIGNAL_BOOTUPMSG == 1 */
#endif /* QUEUED_MODE == 1 */

#if MAX_RX_PDOS > 0
extern RPDOparameter XDATA mRPDO[MAX_RX_PDOS];
extern CONST VRPDOComParData CODE mRPDOAttr[MAX_RX_PDOS];
extern CONST MAPPING CODE atRxPdoDefMap[MAX_RX_PDOS][MAX_MAPPED_OBJECTS];
#endif /* MAX_RX_PDOS > 0 */

#if MAX_TX_PDOS > 0
extern TPDOparameter XDATA mTPDO[MAX_TX_PDOS];
extern CONST VTPDOComParData CODE mTPDOAttr[MAX_TX_PDOS];
extern CONST MAPPING CODE atTxPdoDefMap[MAX_TX_PDOS][MAX_MAPPED_OBJECTS];
#endif /* MAX_TX_PDOS > 0 */


/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/
/*!
  \brief module information.

  We hold all the communication relevant information here.
*/
vModInfo XDATA ModuleInfo = {
  MODULE_NODE_ID,
  MODULE_BAUDRATE,
  LOCAL_INIT,
  BOOTUP
};

/*!
  \brief Inhibit time values for the emergency message.

  For every baudrate there exists one entry in this table.
  The value gives the inhibit time in units of 100 us.
*/
CONST WORD CODE awCEmcyInhibit[SELECTABLE_BITTIMES] = {
  0, /* e.g. 1200,  10K bit/s */
  0, /* e.g.  600,  20K bit/s */
  0, /* e.g.  240,  50K bit/s */
  0, /* e.g.  120, 100K bit/s */
  0, /* e.g.   96, 125K bit/s */
  0, /* e.g.   48, 250K bit/s */
  0, /* e.g.   24, 500K bit/s */
  0, /* e.g.   16, 800K bit/s */
  0  /* e.g.   12,   1M bit/s */
};

BYTE XDATA bOvErrorReg; /*!< error register       */

#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
DWORD XDATA lLifeTime;                  /*!< node guarding life time   */
BOOLEAN DATA fMasterGuard = OFF;        /*!< master guarding on/off    */
BOOLEAN DATA fGuardingFailure = FALSE;  /*!< master guarding alarm     */
BOOLEAN DATA fToggleBit;                /*!< node guarding toggle bit  */
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */

#if ENABLE_NMT_ERROR_CTRL != 0
BOOLEAN DATA fNodeGuarding;             /*!< switch node guarding on/off */
BOOLEAN DATA fTxGuardRequest = FALSE;   /*!< transmit guard request    */
#endif /* ENABLE_NMT_ERROR_CTRL != 0 */

#if NMT_MASTER_ENABLE == 1
BOOLEAN DATA fTxNmtRequest = FALSE;     /*!< transmit NMT request         */
#endif /* NMT_MASTER_ENABLE == 1 */

#if NMT_GUARDING_ENABLE == 1
BOOLEAN DATA fTxNmtGuardRequest = FALSE;/*!< transmit node guard request  */
#endif /* NMT_GUARDING_ENABLE == 1 */

#if SYNC_PRODUCER == 1
BOOLEAN DATA fTxSyncRequest = FALSE;    /*!< transmit SYNC request        */
#endif /* SYNC_PRODUCER == 1 */

BOOLEAN DATA fTxSdoRequest  = FALSE;    /*!< SDO transmit indicator       */
#if ENABLE_EMCYMSG == 1
BOOLEAN DATA fTxEmcyRequest = FALSE;    /*!< EMCY transmit indicator      */
#endif /* ENABLE_EMCYMSG == 1 */

# if (ENABLE_LAYER_SERVICES == 1) && (ENABLE_LSS_MASTER == 0)
BOOLEAN DATA fTxLssRequest = FALSE;     /*!< LSS transmit indicator       */
# endif /* (ENABLE_LAYER_SERVICES == 1) && (ENABLE_LSS_MASTER == 0) */

BOOLEAN DATA fEmcyOverrun;              /*!< EMCY overrun flag            */
BOOLEAN DATA fEmcyIdCollision;          /*!< EMCY ID collision flag       */
#if ENABLE_NMT_ERROR_CTRL > 1
BOOLEAN DATA fHeartbeatPro;             /*!< producer heartbeat event     */
#endif /* ENABLE_NMT_ERROR_CTRL > 1 */

#if (SYNC_USED == 1) || (TIME_STAMP_USED == 1)
QBYTE XDATA lHighResTime;           /*!< high resolution time stamp       */
#endif /* (SYNC_USED == 1) || (TIME_STAMP_USED == 1) */

STATIC BYTE XDATA bLastCanStatus; /*!< last sampled CAN status */
BYTE XDATA bExtEmcy;       /*!< additional pre-defined EMCY situations */
BYTE XDATA bNewExtEmcy;    /*!< new additional pre-defined EMCY situations */

#if TIME_STAMP_USED == 1
TIME_OF_DAY XDATA tTimeOfDay;     /*!< time of day (time stamp object)    */
#endif /* TIME_STAMP_USED == 1 */
#if HEARTBEAT_CONSUMER == 1
BOOLEAN DATA fHeartbeatConErr;          /*!< consumer heartbeat event     */
CON_HEART XDATA atHeartbeatCon[NUM_OF_HB_CONSUMERS]; /*!< consumer heartbeat information */
#endif /* HEARTBEAT_CONSUMER == 1 */

/* pointer to data from delayed SDO transfer */
#if DELAYED_SDO_ENABLE == 1
BYTE * pbDelayedSdoData[NUM_SDO_SERVERS];
#endif /* DELAYED_SDO_ENABLE == 1 */

VComParaArea XDATA gStkGlobDat; /*!< Storage block for non-volatile data. */


/*--------------------------------------------------------------------*/
/*  function definitions                                              */
/*--------------------------------------------------------------------*/

#if QUEUED_MODE == 1
#else
/*!
  \brief Transmit oustanding messages.

  If we are not in #QUEUED_MODE we have only single buffers
  for every message. So sometimes a message is in the buffer
  but not sent yet.
  This function fires the transmission.
*/
STATIC void sSlvChkPndTxReq(void)
{
  static BYTE DATA i = 0;     /* buffer counter */

#if ENABLE_EMCYMSG == 1
  /* emergency message is handled in gCheckEmcyTransmit() */
  if (fTxEmcyRequest == TRUE) {
    if (gCan_SendBuf(EMCY_TX) == NO_ERR) {
      /* message transmitted */
      fTxEmcyRequest = FALSE;
    } /* if */
    return;
  } /* if */
#endif /* ENABLE_EMCYMSG == 1 */

# if MAX_TX_PDOS > 0
  /* transmit PDOs */
  for (; i < MAX_TX_PDOS; i++) {
    if (mTPDO[i].bTransRequest == TRUE) {
      if (gCan_SendBuf((BYTE)(PDO_TX + i)) == NO_ERR) {
        /* message transmitted */
        mTPDO[i].bTransRequest = FALSE;
      } /* if */
      return;
    } /* if */
  } /* for */

  i = 0; /* reset counter */
# endif /* MAX_TX_PDOS > 0 */

#if ENABLE_NMT_ERROR_CTRL != 0
  /* node guarding message */
  if (fTxGuardRequest == TRUE) {
    if (gCan_SendBuf(ERRCTRL_TX) == NO_ERR) {
      /* message transmitted */
      fTxGuardRequest = FALSE;
    } /* if */
    return;
  } /* if */
#endif /* ENABLE_NMT_ERROR_CTRL != 0 */

  /* transmit SDO */
  if (fTxSdoRequest == TRUE) {
    if (gCan_SendBuf(SDO_TX) == NO_ERR) {
      /* message transmitted */
      fTxSdoRequest = FALSE;
    } /* if */
    return;
  } /* if */

# if NMT_GUARDING_ENABLE == 1
  /* node guarding request */
  if (fTxNmtGuardRequest == TRUE) {
    if (gCan_SendBuf(NMT_GUARD_TX) == NO_ERR) {
      /* message transmitted */
      fTxNmtGuardRequest = FALSE;
    } /* if */
    return;
  } /* if */
# endif /* NMT_GUARDING_ENABLE == 1 */

# if SYNC_PRODUCER == 1
  /* SYNC request */
  if (fTxSyncRequest == TRUE) {
    if (gCan_SendBuf(SYNC_TX) == NO_ERR) {
      /* message transmitted */
      fTxSyncRequest = FALSE;
    } /* if */
    return;
  } /* if */
# endif /* SYNC_PRODUCER == 1 */

# if NMT_MASTER_ENABLE == 1
  /* NMT service request */
  if (fTxNmtRequest == TRUE) {
    if (gCan_SendBuf(NMT_ZERO_TX) == NO_ERR) {
      /* message transmitted */
      fTxNmtRequest = FALSE;
    } /* if */
    return;
  } /* if */
# endif /* NMT_MASTER_ENABLE == 1 */

# if (ENABLE_LAYER_SERVICES == 1) && (ENABLE_LSS_MASTER == 0)
  /* LSS service request */
  if (fTxLssRequest == TRUE) {
    if (gCan_SendBuf(LSS_TX) == NO_ERR) {
      /* message transmitted */
      fTxLssRequest = FALSE;
    } /* if */
    return;
  } /* if */
# endif /* (ENABLE_LAYER_SERVICES == 1) && (ENABLE_LSS_MASTER == 0) */
}
#endif /* QUEUED_MODE == 1 */


/*!
  \brief Set internal variables to default values.

  Internally used variables are resetted here.
*/
STATIC void sSlvVarInit(void)
{
  BYTE XDATA i;                  /* counter                              */
  BYTE XDATA k;                  /* counter                              */

  /* clear all transmit and receive buffer parameters */
  for (i = 0; i < MAX_CAN_BUFFER; i++) {
    tCanMsgBuffer[i].bSta = BUF_EMPTY;
    tCanMsgBuffer[i].tMsg.Dlc = 0;
    tCanMsgBuffer[i].tMsg.Rtr = 0;
    tCanMsgBuffer[i].tMsg.Ext = 0;
    tCanMsgBuffer[i].tMsg.TxOk = 0;
    tCanMsgBuffer[i].tMsg.RxOk = 0;
    for (k = 0; k < 8; k++) {
      MESSAGE_DB(i, k) = 0;
    } /* for */
  } /* for */

  /* EMCY management table */
#if ENABLE_EMCYMSG == 1
  gStkGlobDat.mEmcyInhReloadTime.wc  = awCEmcyInhibit[ModuleInfo.bBaudRate];
  EmcyTable.wbEmcyInhTime.wc         = awCEmcyInhibit[ModuleInfo.bBaudRate];
#endif /* ENABLE_EMCYMSG == 1 */
  gResetPredefErrors();

#if SYNC_USED == 1
  lHighResTime.dw = 0;                 /* clear high resolution time stamp     */
# if SYNC_PRODUCER == 1
  gNmt_SyncStop();                     /* disable SYNC producer                */
  gStkGlobDat.lComCyclePeriod.dw = 0;
# endif /* SYNC_PRODUCER == 1 */
#endif /* SYNC_USED == 1 */

#if TIME_STAMP_USED == 1
  tTimeOfDay.ms.dw = 0;         /* clear time of day                    */
  tTimeOfDay.days.wc = 0;
#endif /* TIME_STAMP_USED == 1 */

#if HEARTBEAT_CONSUMER == 1
  for (i = 0; i < NUM_OF_HB_CONSUMERS; i++) {
    atHeartbeatCon[i].bRun = OFF;
  } /* for */
  fHeartbeatConErr = OFF;       /* clear consumer heartbeat event       */
#endif /* HEARTBEAT_CONSUMER == 1 */

#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
    ServerSdo[0].bChannelEnabled = TRUE; /* the first channel is enabled by default */
    ServerSdo[0].bRxIdEnabled = TRUE;    /* Rx ID is enabled */
    ServerSdo[0].bTxIdEnabled = TRUE;    /* Tx ID is enabled */
    ServerSdo[0].bNodeIdClient = ModuleInfo.bBoardAdr; /* client of this server */
  for (i = 1; i < NUM_SDO_SERVERS; i++) {
    /* configure the second channel */
    ServerSdo[i].bChannelEnabled = SDO_SERVER2_DEFAULT_CONF;
    ServerSdo[i].bRxIdEnabled = SDO_SERVER2_DEFAULT_CONF;
    ServerSdo[i].bTxIdEnabled = SDO_SERVER2_DEFAULT_CONF;
    ServerSdo[i].bNodeIdClient = ModuleInfo.bBoardAdr;
  } /* for */
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */

#if OBJECT_1029_USED == 1
  /* object 1029 subindex 1 (communication error): default value 0 */
  abErrorBehavior[0] = ERR_PREOPERATIONAL;
  /* default value for invalid error class: 1 (no state change) */
  abErrorBehavior[NUM_ERROR_CLASSES] = ERR_NO_CHANGE;
  /* set subindices 2..NUM_ERROR_CLASSES to default */
# if NUM_ERROR_CLASSES > 1
#  if PROFILE_DS401 == 1
  /* "PRE-OPERATIONAL" for I/O profile DS-401 */
  MemSet(&abErrorBehavior[1], ERR_PREOPERATIONAL, (NUM_ERROR_CLASSES - 1));
#  else
  /* "NO CHANGE" generally */
  MemSet(&abErrorBehavior[1], ERR_NO_CHANGE, (NUM_ERROR_CLASSES - 1));
#  endif /* PROFILE_DS401 == 1 */
# endif /* NUM_ERROR_CLASSES > 1 */
#endif /* OBJECT_1029_USED == 1 */

#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
  fToggleBit       = OFF;       /* reset guarding toggle bit            */
  fMasterGuard     = OFF;       /* master guarding off                  */
  fGuardingFailure = FALSE;     /* clear master guarding alarm          */
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */

#if ENABLE_NMT_ERROR_CTRL > 1
  fNodeGuarding    = ON;        /* node guarding enabled     */
  fHeartbeatPro    = OFF;       /* clear producer heartbeat event       */
#endif /* ENABLE_NMT_ERROR_CTRL > 1 */

  fEmcyOverrun = FALSE;         /* clear overrun flag */
  bLastCanStatus = 0;           /* no CAN problems */
  bExtEmcy = 0;                 /* no EMCY relevant problems (copy) */
  bNewExtEmcy = 0;              /* no EMCY relevant problems */
} /* sSlvVarInit */


/*!
  \brief Set the used identifiers and the initial PDO behaviour.

  The identifiers for all CANopen related objects are set.
  additionally the PDO transmission behaviour is initialized the first time.
*/
void gSlvSetIdent(void)
{
#if (QUEUED_MODE == 1) && (ID_FILTER > 0)
# if (SIGNAL_EMCYMSG == 1) || (SIGNAL_BOOTUPMSG == 1)
  WORD wTmpId;
# endif /* (SIGNAL_EMCYMSG == 1) || (SIGNAL_BOOTUPMSG == 1) */
#endif /* (QUEUED_MODE == 1) && (ID_FILTER > 0) */

  /* initialise the mapping structure */
  InitMapping();
  /* initialise the communication parameters */
  InitComPars();

  /* set CANopen predefined identifier */
#if MAX_RX_PDOS > 0
  gSlvSetRpdoIdent();
#endif /* MAX_RX_PDOS > 0 */

#if MAX_TX_PDOS > 0
  gSlvSetTpdoIdent();
#endif /* MAX_TX_PDOS > 0 */

#if NMT_MASTER_ENABLE == 1
  /* COB ID 0 NMT service (receive) */
  gCB_BufSetId(NMT_ZERO_RX, NMT_ZERO_ID, 2, RX_DIR);
  /* COB ID 0 NMT service (transmit) */
  gCB_BufSetId(NMT_ZERO_TX, NMT_ZERO_ID, 2, TX_DIR);
  gCB_BufEnable(ON, NMT_ZERO_TX);
#else
  /* COB ID 0 NMT service (receive) */
  gCB_BufSetId(NMT_ZERO_RX, NMT_ZERO_ID, 2, RX_DIR);
  gCB_BufEnable(ON, NMT_ZERO_RX);
#endif /* NMT_MASTER_ENABLE == 1 */

#if SYNC_USED == 1
  /* COB ID SYNC message */
  gCB_BufSetId(SYNC_RX, SYNC_ID, 0, RX_DIR);
  gCB_BufEnable(ON, SYNC_RX);
# if SYNC_PRODUCER == 1
  /* SYNC producer message information */
  tCanMsgBuffer[SYNC_TX].tMsg.qbId.dw = SYNC_ID;
  tCanMsgBuffer[SYNC_TX].tMsg.Dlc = 0;
  tCanMsgBuffer[SYNC_TX].tMsg.Rtr = 0;
# endif /* SYNC_PRODUCER == 1 */
#endif /* SYNC_USED == 1 */

  /* COB ID receive SDO */
  gCB_BufSetId(SDO_RX, SDORX_ID + ModuleInfo.bBoardAdr, 8, RX_DIR);
  gCB_BufEnable(ON, SDO_RX);

#if TIME_STAMP_USED == 1
  /* COB ID time stamp message */
  gCB_BufSetId(TIME_RX, (0x80000000UL | TIME_ID), 6, RX_DIR);
  gCB_BufEnable(ON, TIME_RX);
#endif /* TIME_STAMP_USED == 1 */

#if ENABLE_EMCYMSG == 1
  /* COB ID emergency message */
  gCB_BufSetId(EMCY_TX, EMCY_ID + ModuleInfo.bBoardAdr, 8, TX_DIR);
  gCB_BufEnable(ON, EMCY_TX);
#endif /* ENABLE_EMCYMSG == 1 */

  /* COB ID transmit SDO */
  gCB_BufSetId(SDO_TX, SDOTX_ID + ModuleInfo.bBoardAdr, 8, TX_DIR);
  gCB_BufEnable(ON, SDO_TX);

#if ENABLE_NMT_ERROR_CTRL != 0
  /* COB ID node guarding */
  gCB_BufSetId(ERRCTRL_TX, ERRCTRL_ID + ModuleInfo.bBoardAdr, 1, TX_DIR);
  gCB_BufEnable(ON, ERRCTRL_TX);
#endif /* ENABLE_NMT_ERROR_CTRL != 0 */

#if CLIENT_ENABLE == 1
  /* receive part of client SDO */
  gCB_BufSetId(CLSDO_RX, SDOTX_ID, 8, RX_DIR);
  gCB_BufEnable(ON, CLSDO_RX);

  /* transmit part of client SDO */
  gCB_BufSetId(CLSDO_TX, SDORX_ID, 8, TX_DIR);
  gCB_BufEnable(ON, CLSDO_TX);

  gSdoC_ClearInfo();
#endif /* CLIENT_ENABLE == 1*/

#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
  /* COB ID second transmit SDO */
  gCB_BufSetId(SDO_TX + 1, SDOTX_ID + SDO_SERVER2, 8, TX_DIR);
  gCB_BufEnable(SDO_SERVER2_DEFAULT_CONF, SDO_TX + 1); /* set bootup configuration */

  /* COB ID second receive SDO */
  gCB_BufSetId(SDO_RX + 1, SDORX_ID + SDO_SERVER2, 8, RX_DIR);
  gCB_BufEnable(SDO_SERVER2_DEFAULT_CONF, SDO_RX + 1); /* set bootup configuration */
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */

#if ENABLE_LAYER_SERVICES == 1
# if (EXEC_LAYER_SERVICES_ALWAYS == 1) && (ENABLE_LSS_MASTER == 0)
  /* receive layer services */
  gCB_BufSetId(LSS_RX, LSS_MASTER_ID, 8, RX_DIR);
  gCB_BufEnable(ON, LSS_RX);

  /* transmit layer services */
  gCB_BufSetId(LSS_TX, LSS_SLAVE_ID, 8, TX_DIR);
  gCB_BufEnable(ON, LSS_TX);
# endif  /* (EXEC_LAYER_SERVICES_ALWAYS == 1) && (ENABLE_LSS_MASTER == 0) */
# if ENABLE_LSS_MASTER == 1
  /* receive layer services */
  gCB_BufSetId(LSS_RX, LSS_SLAVE_ID, 8, RX_DIR);
  gCB_BufEnable(ON, LSS_RX);

  /* transmit layer services */
  gCB_BufSetId(LSS_TX, LSS_MASTER_ID, 8, TX_DIR);
  gCB_BufEnable(ON, LSS_TX);
# endif /* ENABLE_LSS_MASTER == 1 */  
#endif /* ENABLE_LAYER_SERVICES == 1 */

  /* set the buffer numbers for our first SDO */
  gSdoS_SetServerCanBuf(0, SDO_TX, SDO_RX);

#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
  /* set the buffer numbers for our second SDO */
  gSdoS_SetServerCanBuf(1, SDO_TX + 1, SDO_RX + 1);
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */

#if (QUEUED_MODE == 1) && (ID_FILTER > 0)
# if SIGNAL_EMCYMSG == 1
  /* set emergency message COB-IDs according to configured range */
  for (wTmpId = (WORD)SignalEmcy[0].qbIdLowLimit.dw;
       wTmpId <= (WORD)SignalEmcy[0].qbIdHighLimit.dw;
       wTmpId++) {
    if (wTmpId != (EMCY_ID + ModuleInfo.bBoardAdr)) {
      gCB_ConfigMsg(wTmpId, 0);
    }
  } /* for */
# endif /* SIGNAL_EMCYMSG == 1 */

# if SIGNAL_BOOTUPMSG == 1
  /* set BootUp message COB-IDs according to configured range */
  for (wTmpId = (WORD)SignalBootUp[0].qbIdLowLimit.dw;
       wTmpId <= (WORD)SignalBootUp[0].qbIdHighLimit.dw;
       wTmpId++) {
    if (wTmpId != (ERRCTRL_ID + ModuleInfo.bBoardAdr)) {
      gCB_ConfigMsg(wTmpId, 0);
    }
  } /* for */
# endif /* SIGNAL_BOOTUPMSG == 1 */
#endif /* (QUEUED_MODE == 1) && (ID_FILTER > 0) */
} /* gSlvSetIdent */



/*!
  \brief Evaluate the internal variables for EMCY conditions.

  The stack provides already five pre-defined EMCY events.
  For more information see section \ref emcypage
*/
void gEvalEmcy(void)
{
#if OBJECT_1029_USED == 1
  BYTE bTmpErrClass = 0xFF; /* default: invalid error class */
#endif /* OBJECT_1029_USED == 1 */
  BYTE bTmpCanStatus = gCan_GetCanStatus();

  if (bTmpCanStatus != bLastCanStatus) {
    /* there is something new to report */
    /* warning state reached for CAN controller */
    if ((bTmpCanStatus ^ bLastCanStatus) & CAN_STS_WARNING) {
      if (bTmpCanStatus & CAN_STS_WARNING) {
        gSetEmcyInternal(ECY_STATE_MON, ON);
#if STATUS_LEDS_USED == 1
        /* set ERROR LED: warning limit reached */
        gLED_SetLedState(LED_WARNING, ON);
#endif /* STATUS_LEDS_USED == 1 */

#if SIGNAL_STACK_EVENT == 1
        gSlvStackEventCallback(CAN_INFO, CAN_WARNING);
#endif /* SIGNAL_STACK_EVENT == 1 */
      } /* if */
      else {
        gSetEmcyInternal(ECY_STATE_MON, OFF);
#if STATUS_LEDS_USED == 1
        /* reset ERROR LED: no warning limit, not off bus */
        gLED_SetLedState(LED_BOFF, OFF);
        gLED_SetLedState(LED_WARNING, OFF);         
#endif /* STATUS_LEDS_USED == 1 */

#if SIGNAL_STACK_EVENT == 1
        gSlvStackEventCallback(CAN_INFO, CAN_ACTIVE);
#endif /* SIGNAL_STACK_EVENT == 1 */
      } /* else */
    } /* if */

    /* bus off */
    if ((bTmpCanStatus ^ bLastCanStatus) & CAN_STS_BUS_OFF) {
      if (bTmpCanStatus & CAN_STS_BUS_OFF) {
#if STATUS_LEDS_USED == 1
        /* set ERROR LED: BUS OFF */
        gLED_SetLedState(LED_BOFF, ON);
#endif /* STATUS_LEDS_USED == 1 */

#if SIGNAL_STACK_EVENT == 1
          gSlvStackEventCallback(CAN_INFO, BUS_OFF);
#endif /* SIGNAL_STACK_EVENT == 1 */
      } /* if */
      else {
        gSetEmcyInternal(ECY_BOFF_MON, ON);
#if STATUS_LEDS_USED == 1
        /* reset ERROR LED: no warning limit, not off bus */
        gLED_SetLedState(LED_BOFF, OFF);
        gLED_SetLedState(LED_WARNING, OFF);         
#endif /* STATUS_LEDS_USED == 1 */
      } /* else */
    } /* if */

    /* message lost */
    if ((bTmpCanStatus ^ bLastCanStatus) & CAN_STS_OVERRUN) {
      if (bTmpCanStatus & CAN_STS_OVERRUN) {
        gSetEmcyInternal(ECY_OVR_MON, ON);
        gswCAN_Controller_OverrunCount++;
#if SIGNAL_STACK_EVENT == 1
        gSlvStackEventCallback(CAN_INFO, CAN_OVERRUN);
#endif /* SIGNAL_STACK_EVENT == 1 */
      } /* if */
      else {
        gSetEmcyInternal(ECY_OVR_MON, OFF);
      } /* else */
    } /* if */
    bLastCanStatus = bTmpCanStatus; /* update */
  } /* if */

  if (bNewExtEmcy != bExtEmcy) {
    /* there is something new to report */
    /* received PDO smaller than mapping */
    if ((bNewExtEmcy ^ bExtEmcy) & MAP_EXCEED) {
      if (bNewExtEmcy & MAP_EXCEED) {
        if (gSetEmcyInternal(ECY_LEN_EXC_PROT, ON)) {
          /* force automatic reset */
          bExtEmcy |= MAP_EXCEED;
          bNewExtEmcy &= (BYTE)(~MAP_EXCEED);
        } /* if */
      } /* if */
      else {
        if (gSetEmcyInternal(ECY_LEN_EXC_PROT, OFF)) {
          bExtEmcy &= (BYTE)(~MAP_EXCEED);
        } /* if */
      } /* else */
    } /* if */
    /* received PDO exceeds mapping */
    if ((bNewExtEmcy ^ bExtEmcy) & PDO_EXCEED) {
      if (bNewExtEmcy & PDO_EXCEED) {
        if (gSetEmcyInternal(ECY_LEN_PROT, ON)) {
          /* force automatic reset */
          bExtEmcy |= PDO_EXCEED;
          bNewExtEmcy &= (BYTE)(~PDO_EXCEED);
        } /* if */
      } /* if */
      else {
        if (gSetEmcyInternal(ECY_LEN_PROT, OFF)) {
          bExtEmcy &= (BYTE)(~PDO_EXCEED);
        } /* if */
      } /* else */
    } /* if */
    /* error control failed */
    if ((bNewExtEmcy ^ bExtEmcy) & ERR_CTRL_FAIL) {
      if (bNewExtEmcy & ERR_CTRL_FAIL) {
#if STATUS_LEDS_USED == 1
        /* set ERROR LED: error control event */
        /* the application has to care for resetting this state!!! */
        gLED_SetLedState(LED_ERRCTRL, ON);
#endif /* STATUS_LEDS_USED == 1 */
        if (gSetEmcyInternal(ECY_ERR_MON, ON)) {
          /* force automatic reset */
          bExtEmcy |= ERR_CTRL_FAIL;
          bNewExtEmcy &= (BYTE)(~ERR_CTRL_FAIL);
        } /* if */
      } /* if */
      else {
        if (gSetEmcyInternal(ECY_ERR_MON, OFF)) {
          bExtEmcy &= (BYTE)(~ERR_CTRL_FAIL);
        } /* if */
      } /* else */
    } /* if */
    /* overrun detected */
    if ((bNewExtEmcy ^ bExtEmcy) & MSG_OVERRUN) {
      if (bNewExtEmcy & MSG_OVERRUN) {
        if (gSetEmcyInternal(ECY_OVR_MON, ON)) {
          gswCAN_Stack_OverrunCount++;
          /* force automatic reset */
          bExtEmcy |= MSG_OVERRUN;
          bNewExtEmcy &= (BYTE)(~MSG_OVERRUN);
        } /* if */
      } /* if */
      else {
        if (gSetEmcyInternal(ECY_OVR_MON, OFF)) {
          bExtEmcy &= (BYTE)(~MSG_OVERRUN);
        } /* if */
      } /* else */
    } /* if */
    /* COB-ID collison detected */
    if ((bNewExtEmcy ^ bExtEmcy) & COB_COLLISION) {
      if (bNewExtEmcy & COB_COLLISION) {
        if (gSetEmcyInternal(ECY_COB_COLL, ON)) {
          /* force automatic reset */
          bExtEmcy |= COB_COLLISION;
          bNewExtEmcy &= (BYTE)(~COB_COLLISION);
        } /* if */
      } /* if */
      else {
        if (gSetEmcyInternal(ECY_COB_COLL, OFF)) {
          bExtEmcy &= (BYTE)(~COB_COLLISION);
        } /* if */
      } /* else */
    } /* if */
  } /* if */

  /* check emergency state for error behaviour */
  /* check for errors */
  if (EmcyTable.bErrReg & EREG_GENERIC) {
    /* generic error set */
    if (EmcyTable.bErrReg & EREG_CURRENT) {
      /* current error */
    } /* if */
    else if (EmcyTable.bErrReg & EREG_VOLTAGE) {
      /* voltage error */
    } /* else if */
    else if (EmcyTable.bErrReg & EREG_TEMP) {
      /* temperature error */
    } /* else if */
    else if (EmcyTable.bErrReg & EREG_COMM) {
      /* communication error */
#if OBJECT_1029_USED == 1
      /* behaviour according object 1029h subindex 1: communication error */
      bTmpErrClass = 1;
#else
# if ON_COMMERROR_GO_PREOPERATIONAL == 1
      /* OPERATIONAL state? */
      if (ModuleInfo.bCommState == OPERATIONAL) {
        /* BUS OFF, Node Guarding or Heartbeat event? */
        if ((bTmpCanStatus & CAN_STS_BUS_OFF) ||
            (bExtEmcy & ERR_CTRL_FAIL))  {
          /* enter pre-operational state */
          gGoPreOperational();
        } /* if */
      } /* if */
# endif /* ON_COMMERROR_GO_PREOPERATIONAL == 1 */
#endif /* OBJECT_1029_USED == 1 */
    } /* else if */
    else if (EmcyTable.bErrReg & EREG_DEV_PROF) {
      /* device profile specific error   */
#if (OBJECT_1029_USED == 1) && (GENERIC_PROFILE_USED == 1)
      /* get profile specific error class */
      bTmpErrClass = PROFILE_CHECK_ERRORS();
#endif /* (OBJECT_1029_USED == 1) && (GENERIC_PROFILE_USED == 1) */
    } /* else if */
    else if (EmcyTable.bErrReg & EREG_MANUFACT) {
      /* manufacturer specific error */
    } /* else if */
    else {
      /* EREG_RESERVED */
    } /* else */

#if OBJECT_1029_USED == 1
    /* perform appropriate error reaction */
    if ((bTmpErrClass < 1) || (bTmpErrClass > NUM_ERROR_CLASSES)) {
      /* invalid resp. not implemented error class: no state change */
      bTmpErrClass = NUM_ERROR_CLASSES + 1;
    } /* if */
    switch (abErrorBehavior[bTmpErrClass - 1]) {
      case ERR_PREOPERATIONAL:
        /* switch to PREOPERATIONAL if current state is OPERATIONAL */
        if (ModuleInfo.bCommState == OPERATIONAL) {
          /* enter pre-operational state */
          gGoPreOperational();
        } /* if */
      break;

      case ERR_NO_CHANGE:
        /* no state change */
      break;

      case ERR_STOPPED:
        /* switch to STOPPED state */
        if (ModuleInfo.bCommState != STOPPED) {
          gGoStopped();
        } /* if */
      break;

      default:
        /* reserved value */
      break;
    } /* switch */
#endif /* OBJECT_1029_USED == 1 */
  } /* if */
} /* gEvalEmcy */


/*!
  \brief Basic receive message handler.

  This function gets the next available message from the buffer and
  evaluates it.
  \warning There is only one message evaluated during one call.

  The function incorporates the CANopen related state machine.
*/
void CanCom(void)
{
  BYTE DATA i;                  /* counter                              */
  BYTE XDATA bBNr;              /* buffer number                        */
#if (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0)
  BYTE XDATA bPdoNr;            /* index PDO parameter table            */
#endif /* (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0) */

  /* CAN message available ? */
  if ((bBNr = gCB_TestInMsg()) != NO_MESSAGE) {
#if MAX_RX_PDOS > 0
    if ((bBNr >= PDO_RX) && (bBNr < (PDO_RX + MAX_RX_PDOS))) {
      /* get PDO parameter index */
      bPdoNr = gSlvGetPDOParIndex(bBNr);

      if ((ModuleInfo.bCommState == OPERATIONAL)
        && mRPDO[bPdoNr].bMode) {
        /* synchronous ? */
        if (RPDO_PARAM[bPdoNr].mTT < 241) {
          tCanMsgBuffer[bBNr].bSta = BUF_FULL;
        } /* if */
        else { /* asynchronous                 */
          /* default/variable mapping */
          ANALYZE_PDO(RPDO_MAPPING[bPdoNr], bBNr);
          /* clear buffer */
          tCanMsgBuffer[bBNr].bSta = BUF_EMPTY;
        } /* else */
      } /* if */
      else {
        /* clear buffer */
        tCanMsgBuffer[bBNr].bSta = BUF_EMPTY;
      } /* else */
      bBNr = NO_MESSAGE; /* already handled */
    } /* if */
#endif /* MAX_RX_PDOS > 0 */
#if (MAX_RX_PDOS > 0) && (MAX_TX_PDOS > 0)
    else
#endif /* (MAX_RX_PDOS > 0) && (MAX_TX_PDOS > 0) */
#if MAX_TX_PDOS > 0
    if ((bBNr >= PDO_TX) && (bBNr < (PDO_TX + MAX_TX_PDOS))) {
      /* get PDO parameter index */
      bPdoNr = gSlvGetPDOParIndex(bBNr);

      if ((ModuleInfo.bCommState == OPERATIONAL)
        && mTPDO[bPdoNr].bMode
        && tCanMsgBuffer[bBNr].tMsg.Rtr == 1) {
        /* transmit PDO remote request */
        if (TPDO_PARAM[bPdoNr].mTT == 252) {
          /* transmission type 252 */
          gSlvSendPDO(bPdoNr, bBNr);
        } /* if */
        else {
          /* other transmission type */
          gSlvSendPDO(bPdoNr, bBNr);
        } /* else */
      } /* if */
      else {
        /* clear buffer */
        tCanMsgBuffer[bBNr].bSta = BUF_EMPTY;
      } /* else */
      bBNr = NO_MESSAGE; /* already handled */
    } /* else if */
#endif /* MAX_TX_PDOS > 0 */

    /* new CAN message available */
    switch (bBNr) {

    case NO_MESSAGE:
      break; /* leave */

    case NMT_ZERO_RX:           /* NMT services                      */
      switch (tCanMsgBuffer[NMT_ZERO_RX].tMsg.bDb[0]) {
        case NMT_CS_START_NODE: /* NMT start remote node                */
          if (ModuleInfo.bCommState != OPERATIONAL) {
            gGoOperational();
          } /* if */
          break;

        case NMT_CS_STOP_NODE:  /* NMT stop remote node                 */
          if (ModuleInfo.bCommState != STOPPED) {
            gGoStopped();
          } /* if */
          break;

        case NMT_CS_ENTER_PREOP:/* NMT enter pre-operational state      */
          if (ModuleInfo.bCommState != PRE_OPERATIONAL) {
            gGoPreOperational();
          } /* if */
          break;

        case NMT_CS_RESET_NODE: /* NMT reset node                       */
          gGoResetNode();
          break;

        case NMT_CS_RESET_COM:  /* NMT reset communication              */
          gGoResetComm();
          break;

        default:
          /* ignore other commands */
          VDEBUG_RPTE0("NMT0: Unknown command code received!\n");
          break;
      } /* switch */

      /* clear buffer */
      tCanMsgBuffer[NMT_ZERO_RX].bSta = BUF_EMPTY;
      break;

#if SYNC_USED == 1
    case SYNC_RX:             /* SYNC message                         */
      if (ModuleInfo.bCommState == OPERATIONAL) {
# if SIGNAL_SYNC == 1
        SyncEventRcv();
# endif /* SIGNAL_SYNC == 1 */

# if MAX_RX_PDOS > 0
        /* evaluate RPDOs */
        for (i = 0; i < MAX_RX_PDOS; i++) {
          if (mRPDO[i].bMode) {
            /* RX PDO enabled */
            if (RPDO_PARAM[i].mTT == 0) {
              /* transmission type 0 */
              if (tCanMsgBuffer[(BYTE)(PDO_RX + i)].bSta == BUF_FULL) {
                /* new output values available */
                /* default/variable mapping */
                ANALYZE_PDO(RPDO_MAPPING[i], (BYTE)(PDO_RX + i));
                /* clear buffer */
                tCanMsgBuffer[(BYTE)(PDO_RX + i)].bSta = BUF_EMPTY;
              } /* if */
            } /* if */
            else {
              if (RPDO_PARAM[i].mTT < 241) {
                /* transmission type 1 <= tt < 241 */
                /* increment receive PDO SYNC counter */
                mRPDO[i].bSyncCounter++;

                if (tCanMsgBuffer[(BYTE)(PDO_RX + i)].bSta == BUF_FULL) {
                  /* default/variable mapping */
                  ANALYZE_PDO(RPDO_MAPPING[i], (BYTE)(PDO_RX + i));
                  /* clear buffer */
                  tCanMsgBuffer[(BYTE)(PDO_RX + i)].bSta = BUF_EMPTY;
                  /* reset receive PDO SYNC counter */
                  mRPDO[i].bSyncCounter = 0;
                } /* if */
              } /* if */
              else {
                if (mRPDO[i].bSyncCounter >= RPDO_PARAM[i].mTT) {
                  /* SYNC message without new values  */
                  /* perhaps there is something to do */
                  /* reset receive PDO SYNC counter */
                  mRPDO[i].bSyncCounter = 0;
                } /* if */
              } /* else */
            } /* else */
          } /* if PDO enabled */
        } /* for MAX_RX_PDOS */
# endif /* MAX_RX_PDOS > 0 */

# if MAX_TX_PDOS > 0
        /* evaluate TPDOs */
        for (i = 0; i < MAX_TX_PDOS; i++) {
          if (mTPDO[i].bMode) {
            /* TX PDO enabled */
            if (TPDO_PARAM[i].mTT == 0) {
              /* transmission type 0 */
              if (mTPDO[i].bNewValue == TRUE) {
                /* new values available */
                /* send message */
                gSlvSendPDO(i, (BYTE)(PDO_TX + i));
              } /* if */
            } /* if */
            else {
              if (TPDO_PARAM[i].mTT < 241) {
                /* transmission type 1 <= tt < 241 */
                /* increment transmit PDO SYNC counter */
                mTPDO[i].bSyncCounter++;

                if (mTPDO[i].bSyncCounter >= TPDO_PARAM[i].mTT) {
                  /* TX PDO in sync mode */
                  /* send message */
                  gSlvSendPDO(i, (BYTE)(PDO_TX + i));
                  /* reset transmit PDO SYNC counter */
                  mTPDO[i].bSyncCounter = 0;
                } /* if */
              } /* if */
              else {
                /* transmission type 252 */
                if (TPDO_PARAM[i].mTT == 252) {
                  if (CREATE_PDO(TPDO_MAPPING[i], (BYTE)(PDO_TX + i)) == TRUE) {
                    mTPDO[i].bNewValue = TRUE;
                  } /* if */
                } /* if */
              } /* else */
            } /* else transmission type 0 */
          } /* if TPDO enabled */
        } /* for */
# endif /* MAX_TX_PDOS > 0 */
      } /* if OPERATIONAL */

      /* clear buffer */
      tCanMsgBuffer[SYNC_RX].bSta = BUF_EMPTY;
      break;
#endif /* SYNC_USED == 1 */


    case SDO_RX:              /* server SDO request */
      if (ModuleInfo.bCommState != STOPPED) {
        gSdoS_HandleSdo(0);
      } /* if */
      /* clear buffer */
      tCanMsgBuffer[bBNr].bSta = BUF_EMPTY;
      break;

#if MULTI_SDO_SERVER == 1
# if NUM_SDO_SERVERS == 2
    case SDO_RX + 1:          /* second server's SDO request */
      /* set index */
      if (ModuleInfo.bCommState != STOPPED) {
        for (i = 1; i < NUM_SDO_SERVERS; i++) {
          if (ServerSdo[i].bRxId == bBNr) {
            if (ServerSdo[i].bChannelEnabled == TRUE) {
              gSdoS_HandleSdo(i);
            } /* if */
          } /* if */
        } /* for */
      } /* if */

      /* clear buffer */
      tCanMsgBuffer[bBNr].bSta = BUF_EMPTY;
      break;
# endif /* NUM_SDO_SERVERS == 2 */
#endif /* MULTI_SDO_SERVER == 1 */

#if CLIENT_ENABLE == 1
    case CLSDO_RX:            /* client SDO answer */
      if (ModuleInfo.bCommState != STOPPED) {
        if ((ClientSdo.bLastState != ST_IDLE)
          && (ClientSdo.bRxId == bBNr)) {
          gSdoC_HandleSdo();
          /* It is a client message */
        } /* if */
      } /* if */

      /* clear buffer */
      tCanMsgBuffer[bBNr].bSta = BUF_EMPTY;
      break;
#endif /* CLIENT_ENABLE == 1 */

#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
    case ERRCTRL_TX:        /* node guarding                        */
      if (tCanMsgBuffer[ERRCTRL_TX].tMsg.Rtr) {
        /* manipulate the guarding data + toggle */
        tCanMsgBuffer[ERRCTRL_TX].tMsg.bDb[0] = NewGuardData();
        tCanMsgBuffer[ERRCTRL_TX].tMsg.Rtr = 0; /* reset RTR-Tag */

        /* try to send message */
        tCanMsgBuffer[ERRCTRL_TX].bSta = BUF_COMPLETE;
        if (gCan_SendBuf(ERRCTRL_TX) == BUSY_ERR) {
          /* controller busy, try again later */
          fTxGuardRequest = TRUE;
        } /* if */
        else {
          /* message transmitted */
          fTxGuardRequest = FALSE;
# if FULLCAN_BUFFER_MODE == 1
# else
          tCanMsgBuffer[ERRCTRL_TX].bSta = BUF_EMPTY;
# endif /* FULLCAN_BUFFER_MODE == 1 */
        } /* else */

        if ((gStkGlobDat.mGuardTime > 0) && (gStkGlobDat.mLifeTimeFactor > 0)) {
          fMasterGuard = OFF;           /* master guarding inactiv      */
          lLifeTime = (DWORD)gStkGlobDat.mGuardTime
          * (DWORD)gStkGlobDat.mLifeTimeFactor;   /* set life time        */
          fGuardingFailure = FALSE;     /* reset master guarding alarm  */
          fMasterGuard = ON;            /* master guarding activ        */
        } /* if */
        else {
          fGuardingFailure = FALSE;     /* reset master guarding alarm  */
          fMasterGuard = OFF;           /* master guarding inactiv      */
        } /* else */
      } /* if */
      else {
# if FULLCAN_BUFFER_MODE == 1
# else
        tCanMsgBuffer[ERRCTRL_TX].bSta = BUF_EMPTY;
# endif /* FULLCAN_BUFFER_MODE == 1 */
        /* COB id collision detected */
        bNewExtEmcy |= COB_COLLISION;
      } /* else */
      break;
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */


#if TIME_STAMP_USED == 1
    case TIME_RX:               /* time stamp message           */
      if (ModuleInfo.bCommState != STOPPED) {
        /* set new time of day */
        tTimeOfDay.ms.fb.b0 = tCanMsgBuffer[TIME_RX].tMsg.bDb[0];
        tTimeOfDay.ms.fb.b1 = tCanMsgBuffer[TIME_RX].tMsg.bDb[1];
        tTimeOfDay.ms.fb.b2 = tCanMsgBuffer[TIME_RX].tMsg.bDb[2];
        tTimeOfDay.ms.fb.b3 = tCanMsgBuffer[TIME_RX].tMsg.bDb[3];
        tTimeOfDay.days.bc.lb = tCanMsgBuffer[TIME_RX].tMsg.bDb[4];
        tTimeOfDay.days.bc.mb = tCanMsgBuffer[TIME_RX].tMsg.bDb[5];
        /* check time stamp callback function */
# if (SIGNAL_STACK_EVENT == 1) && (EXEC_USER_TIME_STAMP_CALLBACKS == 1)
        /* branch to callback function */
        gSlvStackEventCallback(TIME_STP, 0);
# endif /* (SIGNAL_STACK_EVENT == 1) && (EXEC_USER_TIME_STAMP_CALLBACKS == 1) */
      } /* if */

      /* clear buffer */
      tCanMsgBuffer[TIME_RX].bSta = BUF_EMPTY;
      break;
#endif /* TIME_STAMP_USED == 1 */

#if (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1))
    case LSS_RX:            /* layer setting service (LSS) */
      gLayerServices();
      /* clear buffer */
      tCanMsgBuffer[LSS_RX].bSta = BUF_EMPTY;
      break;
#endif /* (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1))*/

    default:
#if HEARTBEAT_CONSUMER == 1
    /* consumer heartbeat message */
    if ((bBNr >= ERRCTRL_RX) && (bBNr < (ERRCTRL_RX + NUM_OF_HB_CONSUMERS))) {
      i = (BYTE)(bBNr - ERRCTRL_RX);
      if (atHeartbeatCon[i].bRun != OFF) {  /* heartbeat enabled ?      */
        /* set new status */
        atHeartbeatCon[i].bRun = OFF;
        atHeartbeatCon[i].bStatus = tCanMsgBuffer[bBNr].tMsg.bDb[0];
        /* reset timer */
        atHeartbeatCon[i].wTime = gStkGlobDat.atHeartbeatCon[i].wReloadTime.wc;
        atHeartbeatCon[i].bRun = ON;
      } /* if */
      /* clear buffer */
      tCanMsgBuffer[bBNr].bSta = BUF_EMPTY;
      break;
    } /* if */
#endif /* HEARTBEAT_CONSUMER == 1 */

#if NMT_GUARDING_ENABLE == 1
    /* node guarding response */
    if ((bBNr >= ERRCTRL_RX) && (bBNr < (ERRCTRL_RX + GUARDING_RX_BUFFER))) {
      /* call guarding handler */
      gNdm_HandleGuarding(bBNr);
      /* clear buffer */
      tCanMsgBuffer[bBNr].bSta = BUF_EMPTY;
      break;
    } /* if */
#endif /* NMT_GUARDING_ENABLE == 1 */

      /* ignore other commands */
      VDEBUG_RPTE0("No handler for message!\n");
      break;
    } /* switch */
  } /* known message available */

#if DELAYED_SDO_ENABLE == 1
  /* check for outstanding SDO server reply (blocked by delayed data acquisition) */
  if (ModuleInfo.bCommState != STOPPED) {
    for (i = 0; i < NUM_SDO_SERVERS; i++) {
      if (ServerSdo[i].bDelayedState != DELAY_IDLE) {
        /* check for external data transfer finished */
        if (gSdoS_DelayedTransferStatus(i, &pbDelayedSdoData[i]) == DELAY_COMPLETE) {
          /* delayed SDO data access terminated */
          gSdoS_FinishDelayedTransfer(i, &pbDelayedSdoData[i]);
        } /* if */
      } /* if */
    } /* for */
  } /* if */
#endif /* DELAYED_SDO_ENABLE == 1 */
#if QUEUED_MODE == 1
#else
  /* new try to transmit 'busy' messages */
  sSlvChkPndTxReq();
#endif /* QUEUED_MODE == 1 */

#if ENABLE_NMT_ERROR_CTRL > 1
  /* check producer heartbeat */
  if (fHeartbeatPro) {
    fHeartbeatPro = OFF;        /* clear producer heartbeat event       */
    tCanMsgBuffer[ERRCTRL_TX].tMsg.bDb[0] = ModuleInfo.bCommState;

    /* try to send message */
    tCanMsgBuffer[ERRCTRL_TX].bSta = BUF_COMPLETE;
    if (gCan_SendBuf(ERRCTRL_TX) == BUSY_ERR) {
      /* controller busy, try again later */
      fTxGuardRequest = TRUE;
    } /* if */
    else {
      /* message transmitted */
      fTxGuardRequest = FALSE;
    } /* else */
  } /* if */
#endif /* ENABLE_NMT_ERROR_CTRL > 1 */
} /* CanCom() */


/*!
  \brief Execute the CANopen relevant message handling.

  This function evaluates the CAN communication if the slave is in the state
  #STACK_ENABLED.
  Additionally the guarding is evaluated.
*/
void CANopenStack(void)
{
  if (ModuleInfo.bModuleState == STACK_ENABLED) {
    CanCom();
    if (ModuleInfo.bCommState == OPERATIONAL) {
      Process(); /* now the process data can be evaluated by our application */
    } /* if */
#if GENERIC_PROFILE_USED == 1
    else if (ModuleInfo.bCommState == PRE_OPERATIONAL) {
      /* evaluation in PRE_OPERATIONAL state is processed */
      ProcessPreoperational();
    } /* else if */
#endif /* GENERIC_PROFILE_USED == 1 */

    /* CAN controller off bus ? HANDLING !!!! */

    /* message overrun */
    if ((fEmcyOverrun == TRUE) && !(bExtEmcy & MSG_OVERRUN)) {
      /* overrun detected */
      bNewExtEmcy |= MSG_OVERRUN;
      /* reset overrun flag */
      fEmcyOverrun = FALSE;
    } /* if */

#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
    /* node guarding */
    if (fGuardingFailure == TRUE) {
      /* guarding failure detected */
      bNewExtEmcy |= ERR_CTRL_FAIL;
      /* reset master guarding alarm */
      fGuardingFailure = FALSE;

      /* check missing guard request callback function */
# if (SIGNAL_GUARDFAILURE == 1) && (SIGNAL_STACK_EVENT == 1)
      /* branch to callback function */
      gSlvStackEventCallback(GD_EVT, 0);
# endif /* (SIGNAL_GUARDFAILURE == 1) && (SIGNAL_STACK_EVENT == 1) */
    } /* if */
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */

#if HEARTBEAT_CONSUMER == 1
    /* consumer heartbeat */
    if (fHeartbeatConErr == ON) {
      /* heartbeat failure detected */
      bNewExtEmcy |= ERR_CTRL_FAIL;
      /* clear consumer heartbeat event */
      fHeartbeatConErr = OFF;
    } /* if */
#endif /* HEARTBEAT_CONSUMER == 1 */
  } /* if */
#if ENABLE_LAYER_SERVICES == 1
  else {
    gHandleLssMsg();
  } /* else */
#endif /* ENABLE_LAYER_SERVICES == 1 */
}


/*!
  \brief Execute state machine.

  This function executes the state machine of the CANopen slave.
  The state machine knows the following states
  - #LOCAL_INIT
  - #PREPARE_STARTUP
  - #WAIT_FOR_STARTUP
  - #STACK_ENABLED
  - #STACK_STOPPED
  - #STACK_FREEZED
  - #STACK_WAKEUP
*/
void StateMachine(void)
{
  STATIC BYTE BootUpTx;

  switch (ModuleInfo.bModuleState) {
    case LOCAL_INIT:
      /* prepare chip selects and all the other hardware related stuff */
      VDEBUG_RPT0("State LOCAL_INIT reached\n");
      ModulInit(); /* module initialisation  */

#if ENABLE_LAYER_SERVICES == 1
      ModuleInfo.bModuleState = PREPARE_LSS; /* next state */
#else
      ModuleInfo.bModuleState = PREPARE_STARTUP; /* next state */
#endif /* ENABLE_LAYER_SERVICES == 1 */

#if EXAMPLE_OBJECTS_USED == 1
      MemSet(&gDomainBuffer[0], 0x00, sizeof(gDomainBuffer));
#endif /* EXAMPLE_OBJECTS_USED == 1 */
      MemSet(&gStkGlobDat, 0x00, sizeof(gStkGlobDat)); /* reset storable parameters area */
      gStkGlobDat.mBoardAdr = 0xFF; /* invalidate address */
      gStkGlobDat.mBaudRate = 0xFF; /* invalidate baud rate */
#if STORE_PARAMETERS_SUPP == 1
      gNvInit(); /* init block length for storage blocks */
      gLoadConfiguration();
#endif /* STORE_PARAMETERS_SUPP == 1 */
      gCB_Init();       /* create buffer area                   */
      gCan_Init();      /* prepare CAN controller               */

# if STATUS_LEDS_USED == 1
      gLED_Init();
# endif /* STATUS_LEDS_USED == 1 */  

      break;

#if ENABLE_LAYER_SERVICES == 1
    case PREPARE_LSS: {
      BYTE bTmp;

      VDEBUG_RPT0("State PREPARE_LSS reached\n");
      Tim_InitTimer(BASIC_TIME); /* initiate timer */
      Tim_StartTimer();
      
      bTmp = ReadBoardAdr(); /* get modul ID */
      if (((bTmp > 0) && (bTmp < 128))
# if STORE_PARAMETERS_SUPP == 1
       /* The stored parameters in nonvolatile memory always have priority.
          If the node id has been changed it must be stored before executing the switch mode operation service. */
       || (bTmp == LSS_INVALID_NODE_ID) 
# endif /* STORE_PARAMETERS_SUPP == 1 */
      ) {
        ModuleInfo.bBoardAdr = bTmp;
      } /* if */
     
      bTmp = ReadBaudRate(); /* get baud rate */
      if (bTmp < SELECTABLE_BITTIMES) {
        ModuleInfo.bBaudRate = bTmp;   /* store baudrate selection */
      } /* if */

      sSlvVarInit();    /* set variables to defaults            */

# if FULLCAN_BUFFER_MODE == 1
      VDEBUG_RPT2("%s %d \n", "CiA bit timing table index:", BR_1M - ModuleInfo.bBaudRate);
      gCan_CntrlInit(ModuleInfo.bBaudRate); /* initiate CAN controller */
      gInitLss();
# else
      gInitLss();
      VDEBUG_RPT2("%s %d \n", "CiA bit timing table index:", BR_1M - ModuleInfo.bBaudRate);
      gCan_CntrlInit(ModuleInfo.bBaudRate); /* initiate CAN controller */
# endif /* FULLCAN_BUFFER_MODE == 1 */
      VDEBUG_RPT0("LSS Init State reached \n");            
      ModuleInfo.bModuleState = EXECUTE_LSS; /* next state */
# if SIGNAL_STACK_EVENT == 1
      gSlvStackEventCallback(STACK_STATE, EXECUTE_LSS);
# endif /* SIGNAL_STACK_EVENT == 1 */

      break;
    } /* case PREPARE_LSS: */

    case EXECUTE_LSS:
      ModuleInfo.bModuleState = gLssStateMachine(); /* next state */
      break;
#endif /* ENABLE_LAYER_SERVICES == 1 */

    case PREPARE_STARTUP: {
#if ENABLE_LAYER_SERVICES == 0
      BYTE bTmp;

      VDEBUG_RPT0("State PREPARE_STARTUP reached\n");
      Tim_InitTimer(BASIC_TIME); /* initiate timer */
      Tim_StartTimer();

      sSlvVarInit();    /* set variables to defaults            */

      bTmp = ReadBoardAdr(); /* get modul ID */
      if ((bTmp > 0) && (bTmp < 128)) {
        ModuleInfo.bBoardAdr = bTmp;
      } /* if */
      
      bTmp = ReadBaudRate(); /* get baud rate */
      if (bTmp < SELECTABLE_BITTIMES) {
        ModuleInfo.bBaudRate = bTmp;   /* store baudrate selection */
      } /* if */

#endif /* ENABLE_LAYER_SERVICES == 0 */

#if FULLCAN_BUFFER_MODE == 1
# if ENABLE_LAYER_SERVICES == 0
      VDEBUG_RPT2("%s %d \n", "CiA bit timing table index:", BR_1M - ModuleInfo.bBaudRate);
      gCan_CntrlInit(ModuleInfo.bBaudRate); /* initiate CAN controller */
# endif /* ENABLE_LAYER_SERVICES == 0 */
      gSlvSetIdent();   /* set identifiers to defaults          */
# if STORE_PARAMETERS_SUPP == 1
      gApplyConfiguration();
# endif /* STORE_PARAMETERS_SUPP == 1 */
#else
      gSlvSetIdent();   /* set identifiers to defaults          */
# if STORE_PARAMETERS_SUPP == 1
      gApplyConfiguration();
#  endif /* STORE_PARAMETERS_SUPP == 1 */
# if ENABLE_LAYER_SERVICES == 0
      VDEBUG_RPT2("%s %d \n", "CiA bit timing table index:", BR_1M - ModuleInfo.bBaudRate);
      gCan_CntrlInit(ModuleInfo.bBaudRate); /* initiate CAN controller */
# endif /* ENABLE_LAYER_SERVICES == 0 */
#endif /* FULLCAN_BUFFER_MODE == 1 */

#if NMT_GUARDING_ENABLE
      gNdm_Init();
#endif /* NMT_GUARDING_ENABLE */

#if GENERIC_PROFILE_USED == 1
      PROFILE_INIT();
#endif /* GENERIC_PROFILE_USED == 1 */

      /* send bootup message */
#if ENABLE_NMT_ERROR_CTRL != 0
      BootUpTx = ERRCTRL_TX;
#else
      BootUpTx = SDO_TX;
      /* set ID and DLC for error control message */
      gCB_BufSetId(BootUpTx, ERRCTRL_ID + ModuleInfo.bBoardAdr, 1, TX_DIR);
# if FULLCAN_BUFFER_MODE == 1
      gCB_BufEnable(ON, BootUpTx);
# endif /* FULLCAN_BUFFER_MODE == 1 */
#endif /* ENABLE_NMT_ERROR_CTRL != 0 */

      if (Version3BootUp() == TRUE) {
#if ENABLE_EMCYMSG == 1
        BootUpTx = EMCY_TX;
#endif /* ENABLE_EMCYMSG == 1 */
        /* set ID and DLC for old style BootUp message */
        gCB_BufSetId(BootUpTx, EMCY_ID + ModuleInfo.bBoardAdr, 0, TX_DIR);
#if FULLCAN_BUFFER_MODE == 1
        gCB_BufEnable(ON, BootUpTx);
#endif /* FULLCAN_BUFFER_MODE == 1 */
      } /* if */

      tCanMsgBuffer[BootUpTx].bSta = BUF_COMPLETE;
      if (gCan_SendBuf(BootUpTx) == NO_ERR) {
        ModuleInfo.bModuleState = WAIT_FOR_STARTUP; /* next state */
        VDEBUG_RPT0("State WAIT_FOR_STARTUP reached\n");
#if SIGNAL_STACK_EVENT == 1
        gSlvStackEventCallback(STACK_STATE, WAIT_FOR_STARTUP);
#endif /* SIGNAL_STACK_EVENT == 1 */
      } /* if */
      else {
        ModuleInfo.bModuleState = STACK_STOPPED; /* next state */
      } /* else */
      break;
    } /* case PREPARE_STARTUP: */

    case WAIT_FOR_STARTUP:
      if (gCan_TxOccurred() == TRUE) {
        if (BootUpTx == SDO_TX) {
          /* restore ID and DLC for SDO TX message */
          gCB_BufSetId(SDO_TX, SDOTX_ID + ModuleInfo.bBoardAdr, 8, TX_DIR);
#if FULLCAN_BUFFER_MODE == 1
          gCB_BufEnable(ON, SDO_TX);
#endif /* FULLCAN_BUFFER_MODE == 1 */
        } /* if */
#if ENABLE_NMT_ERROR_CTRL != 0
        else if (BootUpTx == ERRCTRL_TX) {
          /* restore ID and DLC for error control message */
          gCB_BufSetId(ERRCTRL_TX, ERRCTRL_ID + ModuleInfo.bBoardAdr, 1, TX_DIR);
# if FULLCAN_BUFFER_MODE == 1
          gCB_BufEnable(ON, ERRCTRL_TX);
# endif /* FULLCAN_BUFFER_MODE == 1 */
        } /* if */
#endif /* ENABLE_NMT_ERROR_CTRL != 0 */
#if ENABLE_EMCYMSG == 1
        else if (BootUpTx == EMCY_TX) {
          /* restore ID and DLC for EMCY message */
          gCB_BufSetId(EMCY_TX, EMCY_ID + ModuleInfo.bBoardAdr, 8, TX_DIR);
# if FULLCAN_BUFFER_MODE == 1
          gCB_BufEnable(ON, EMCY_TX);
# endif /* FULLCAN_BUFFER_MODE == 1 */
        } /* if */
#endif /* ENABLE_EMCYMSG == 1 */

        ModuleInfo.bModuleState = STACK_ENABLED; /* next state */        
        VDEBUG_RPT0("State STACK_ENABLED reached\n");

#if CHECK_CONSISTENCY == 1
        if (!gCheckODOrdering()) {
          gSetEmcyInternal(0x6261, ON);
          gCheckEmcyTransmit();
        } /* if */
# if (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0)
        gVerifyMapping();
# endif /* (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0) */
#endif /* CHECK_CONSISTENCY == 1 */

#if SIGNAL_STACK_EVENT == 1
        gSlvStackEventCallback(STACK_STATE, STACK_ENABLED);
#endif /* SIGNAL_STACK_EVENT == 1 */
#if STARTUP_AUTONOMOUSLY == 1
        gGoOperational();
#else
        gGoPreOperational();
#endif /* STARTUP_AUTONOMOUSLY == 1 */

#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
        /* install remote frame handling */
        gCan_PrepareRemoteMessage(&tCanMsgBuffer[ERRCTRL_TX].tMsg);
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */

#if ENABLE_NMT_ERROR_CTRL > 1
        gStkGlobDat.tHeartbeatPro.wReloadTime.wc = START_HEARTBEAT_TIME;
        gStkGlobDat.tHeartbeatPro.wTime = START_HEARTBEAT_TIME;
        /* switch between heartbeat and nodeguarding */
        fNodeGuarding = (BOOLEAN)(gStkGlobDat.tHeartbeatPro.wReloadTime.wc ? OFF : ON);
#endif /* ENABLE_NMT_ERROR_CTRL > 1 */
      } /* if */
      break;

    case STACK_ENABLED:
      break;

#if (ENABLE_LAYER_SERVICES == 1) && (LSS_IMPLEMENTATION_LVL_2 == 1)
    case STACK_DEFERRED:
      gLssActivateBitTiming();
      break;
#endif /* ENABLE_LAYER_SERVICES == 1 */

    case STACK_STOPPED:
      Tim_StopTimer();
      break;

    case STACK_FREEZED:
      break;

    case STACK_WAKEUP:
      gCan_WakeUpOccurred();
      ModuleInfo.bModuleState = STACK_ENABLED; /* next state */
      break;

    default:
      break;
  } /* switch */
}


#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
/*!
  \brief Feed the new data item for RTR based guarding.

  This routine is fired on handling the guarding remote frame.
  It simply returns the next actual guard data item.

  \return New guard message contents (state and toggle bit).
*/
BYTE NewGuardData(void) {
  /* check toggle bit */
  if (fToggleBit == ON) {
    /* invert toggle bit */
    fToggleBit = OFF;
    /* set new message */
    return((BYTE)(0x80 | ModuleInfo.bCommState));
  } /* if */
  else {
    /* invert toggle bit */
    fToggleBit = ON;
    /* set new message */
    return((BYTE)(0x7F & ModuleInfo.bCommState));
  } /* else */
}
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */


/*!
  \brief Get the actual module status.

  Return the actual module status for further evaluation.

  \return actual module state
*/
BYTE GetModulStatus(void) {
  return ModuleInfo.bCommState;
}  


#if HEARTBEAT_CONSUMER == 1
/*!
  \brief Set a device for heartbeat checking.

  The heartbeat mechanism for a device is established through
  cyclically receiving a message by a heartbeat producer. If
  the heartbeat cycle fails for the heartbeat producer the
  application will be informed about that event.

  \param bNode - device node number
  \param wTime - heartbeat time

  \retval TRUE - device will be checked
  \retval FALSE - number of hearbeat buffers exceeded
*/
BYTE gSlvSetErrCtrlRxNode(BYTE bNode, WORD wTime)
{
  BYTE i;                       /* counter                              */

  /* check node number range */
  if ((bNode < 1) || (bNode > 127)) {
    return FALSE;
  } /* if */

  /* check already assigned slots */
  for (i = 0; i < NUM_OF_HB_CONSUMERS; i++) {
    if (bNode == gStkGlobDat.atHeartbeatCon[i].bNodeID) {   /* buffer found         */
      if (wTime == 0) {                         /* clear buffer         */
        atHeartbeatCon[i].bRun = OFF;
        atHeartbeatCon[i].bStatus = 0;
        atHeartbeatCon[i].wTime = 0;
        gStkGlobDat.atHeartbeatCon[i].wReloadTime.wc = 0;
        gStkGlobDat.atHeartbeatCon[i].bNodeID = 0;
        gCB_BufEnable(OFF, (BYTE)(ERRCTRL_RX + i)); /* disable COB_ID   */
      } /* if */
      else {                                    /* set new time value   */
        atHeartbeatCon[i].bRun = OFF;
        atHeartbeatCon[i].bStatus = 0;
        atHeartbeatCon[i].wTime = wTime;
        gStkGlobDat.atHeartbeatCon[i].wReloadTime.wc = wTime;
        atHeartbeatCon[i].bRun = FIRST_HB;
      } /* else */
      return TRUE;
    } /* if */
  } /* for */

  /* look for an empty slot */
  for (i = 0; i < NUM_OF_HB_CONSUMERS; i++) {
    if (gStkGlobDat.atHeartbeatCon[i].bNodeID == 0) {       /* empty buffer found   */
      if (wTime != 0) {                         /* enable heartbeat msg */
        gStkGlobDat.atHeartbeatCon[i].bNodeID = bNode;
        atHeartbeatCon[i].bStatus = 0;
        atHeartbeatCon[i].wTime = wTime;
        gStkGlobDat.atHeartbeatCon[i].wReloadTime.wc = wTime;
        atHeartbeatCon[i].bRun = FIRST_HB;
        /* set COB-ID */
        gCB_BufSetId((BYTE)(ERRCTRL_RX + i), ERRCTRL_ID + bNode, 1, RX_DIR);
        gCB_BufEnable(ON, (BYTE)(ERRCTRL_RX + i)); /* enable COB-ID     */
      } /* if */
      return TRUE;
    } /* if */
  } /* for */
  return FALSE;
}
#endif /* HEARTBEAT_CONSUMER == 1 */


#if STORE_PARAMETERS_SUPP == 1
/*!
  \brief Pre-store function for communication parameters

  This function is called if the communication parameters are about to be
  stored.
  The following actions take place:
  - get actual baud rate and node-ID
  - get actual PDO identifiers and enable information
  - get SYNC message
  - get EMCY message and EMCY inhibit time
  - get SDO server data
*/
void gStoreComPar(void)
{
  BYTE i;  

  gStkGlobDat.mBoardAdr = ModuleInfo.bBoardAdr;
  gStkGlobDat.mBaudRate = ModuleInfo.bBaudRate;

  /* RPDO mapping and parameters */
# if MAX_RX_PDOS > 0
  /* default parameter for receive PDOs */
  for (i = 0; i < MAX_RX_PDOS; i++) {
#  if RPDO_PAR_READONLY == 0
    RPDO_PARAM[i].mCOB.fb.b0 = tCanMsgBuffer[(BYTE)(PDO_RX + i)].tMsg.qbId.fb.b0;
    RPDO_PARAM[i].mCOB.fb.b1 = tCanMsgBuffer[(BYTE)(PDO_RX + i)].tMsg.qbId.fb.b1;
    RPDO_PARAM[i].mCOB.fb.b2 = tCanMsgBuffer[(BYTE)(PDO_RX + i)].tMsg.qbId.fb.b2;
    RPDO_PARAM[i].mCOB.fb.b3 = tCanMsgBuffer[(BYTE)(PDO_RX + i)].tMsg.qbId.fb.b3;
    /* store enable/disable information */
    if (!mRPDO[i].bMode) {
      RPDO_PARAM[i].mCOB.fb.b3 |= PDO_INVALID;
    } /* if */
#  endif /* RPDO_PAR_READONLY == 0 */
  } /* for */
# endif /* MAX_RX_PDOS > 0 */

  /* TPDO mapping and parameters */
# if MAX_TX_PDOS > 0
  /* default parameter for transmit PDOs */
  for (i = 0; i < MAX_TX_PDOS; i++) {
#  if TPDO_PAR_READONLY == 0
    TPDO_PARAM[i].mCOB.fb.b0 = tCanMsgBuffer[(BYTE)(PDO_TX + i)].tMsg.qbId.fb.b0;
    TPDO_PARAM[i].mCOB.fb.b1 = tCanMsgBuffer[(BYTE)(PDO_TX + i)].tMsg.qbId.fb.b1;
    TPDO_PARAM[i].mCOB.fb.b2 = tCanMsgBuffer[(BYTE)(PDO_TX + i)].tMsg.qbId.fb.b2;
    TPDO_PARAM[i].mCOB.fb.b3 = tCanMsgBuffer[(BYTE)(PDO_TX + i)].tMsg.qbId.fb.b3;
    /* store RTR valid information */
    if (!mTPDO[i].bRemoteAllowed) {
      TPDO_PARAM[i].mCOB.fb.b3 |= PDO_NO_RTR;
    } /* if */
    /* store enable/disable information */
    if (!mTPDO[i].bMode) {
      TPDO_PARAM[i].mCOB.fb.b3 |= PDO_INVALID;
    } /* if */
#  endif /* RPDO_PAR_READONLY == 0 */
  } /* for */
# endif /* MAX_RX_PDOS > 0 */

# if SYNC_USED == 1
  gStkGlobDat.mSyncId.dw = tCanMsgBuffer[SYNC_RX].tMsg.qbId.dw;
# endif /* SYNC_USED == 1 */

# if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
  for (i = 1; i < NUM_SDO_SERVERS; i++) {
    gStkGlobDat.mSSdoParameter[i - 1].mCobRx =
      tCanMsgBuffer[SDO_RX + i].tMsg.qbId.dw;
    /* store enable/disable information */
    if (!ServerSdo[i].bRxIdEnabled) {
      gStkGlobDat.mSSdoParameter[i - 1].mCobRx |= 0x80000000UL;
    } /* if */
    gStkGlobDat.mSSdoParameter[i - 1].mCobTx =
      tCanMsgBuffer[SDO_TX + i].tMsg.qbId.dw;
    /* store enable/disable information */
    if (!ServerSdo[i].bTxIdEnabled) {
      gStkGlobDat.mSSdoParameter[i - 1].mCobTx |= 0x80000000UL;
    } /* if */
    gStkGlobDat.mSSdoParameter[i - 1].mNodeId = ServerSdo[i].bNodeIdClient;
  } /* for */
# endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)*/
}


/*!
  \brief apply function for communication parameters

  This function is called if the communication parameters are about to be
  applied during start up.
  The following actions take place:
  - set actual PDO identifiers and enable information
  - set SYNC message
  - set EMCY message and EMCY inhibit time
  - set SDO server data
*/
BYTE gApplyComPar(void)
{
  BYTE rc = TRUE;
  BYTE i;  
  
# if (MAX_RX_PDOS > 0) && (RPDO_PAR_READONLY == 0)
  /* default parameter for receive PDOs */
  gSlvSetRpdoIdent();
# endif /* (MAX_RX_PDOS > 0) && (RPDO_PAR_READONLY == 0) */

# if (MAX_TX_PDOS > 0) && (TPDO_PAR_READONLY == 0)
  /* default parameter for transmit PDOs */
  gSlvSetTpdoIdent();
# endif /* (MAX_TX_PDOS > 0) && (TPDO_PAR_READONLY == 0) */

# if SYNC_USED == 1
  tCanMsgBuffer[SYNC_RX].tMsg.qbId.dw = gStkGlobDat.mSyncId.dw;
  gCB_BufEnable(OFF, SYNC_RX);
  /* COB ID SYNC message */
  gCB_BufSetId(SYNC_RX, gStkGlobDat.mSyncId.dw, 0, RX_DIR);
  gCB_BufEnable(ON, SYNC_RX);
#  if SYNC_PRODUCER == 1
  gCB_BufEnable(OFF, SYNC_TX);
  /* COB ID SYNC message */
  gCB_BufSetId(SYNC_TX, gStkGlobDat.mSyncId.dw, 0, TX_DIR);
  gCB_BufEnable(ON, SYNC_TX);
#  endif /* SYNC_PRODUCER == 1 */
# endif /* SYNC_USED == 1 */

# if ENABLE_EMCYMSG == 1
  gStkGlobDat.mEmcyInhReloadTime.wc  = awCEmcyInhibit[ModuleInfo.bBaudRate];
  EmcyTable.wbEmcyInhTime.wc         = gStkGlobDat.mEmcyInhReloadTime.wc;
# endif /* ENABLE_EMCYMSG == 1 */

# if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
  for (i = 1; i < NUM_SDO_SERVERS; i++) {
    gCB_BufEnable(OFF, (BYTE)(SDO_RX + i));
    tCanMsgBuffer[SDO_RX + i].tMsg.qbId.dw
      = gStkGlobDat.mSSdoParameter[i - 1].mCobRx & 0x7FFFFFFFUL;
    if (!(gStkGlobDat.mSSdoParameter[i - 1].mCobRx & 0x80000000UL)) {
      /* enable SDO */
      ServerSdo[i].bRxIdEnabled = TRUE;
      gCB_BufSetId((BYTE)(SDO_RX + i), tCanMsgBuffer[SDO_RX + i].tMsg.qbId.dw, 8, RX_DIR);
      gCB_BufEnable(ON, (BYTE)(SDO_RX + i));
    } /* if */
    else {
      /* disable SDO */
      ServerSdo[i].bRxIdEnabled = FALSE;
    } /* else */
    gCB_BufEnable(OFF, (BYTE)(SDO_TX + i));
    tCanMsgBuffer[SDO_TX + i].tMsg.qbId.dw
      = gStkGlobDat.mSSdoParameter[i - 1].mCobTx & 0x7FFFFFFFUL;
    if (!(gStkGlobDat.mSSdoParameter[i - 1].mCobTx & 0x80000000UL)) {
      /* enable SDO */
      ServerSdo[i].bTxIdEnabled = TRUE;
      gCB_BufSetId((BYTE)(SDO_TX + i), tCanMsgBuffer[SDO_TX + i].tMsg.qbId.dw, 8, TX_DIR);
      gCB_BufEnable(ON, (BYTE)(SDO_TX + i));
    } /* if */
    else {
      /* disable SDO */
      ServerSdo[i].bRxIdEnabled = FALSE;
    } /* else */
    /* restore enable/disable information */
    if ((ServerSdo[i].bRxIdEnabled == TRUE)
        && (ServerSdo[i].bTxIdEnabled == TRUE)) {
      ServerSdo[i].bChannelEnabled = TRUE;
    } /* if */
    else {
      ServerSdo[i].bChannelEnabled = FALSE;
    } /* else */
    ServerSdo[i].bNodeIdClient = gStkGlobDat.mSSdoParameter[i - 1].mNodeId;
  } /* for */
# endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */
  return rc;
} /* sApplyComPar */
#endif /* STORE_PARAMETERS_SUPP == 1 */



/*!
  \brief Request transition to OPERATIONAL.
*/
void gGoOperational(void)
{
  BYTE XDATA i;

  VDEBUG_RPT0("OPERATIONAL command received.\n");

  /* enable emergency message */
#if ENABLE_EMCYMSG == 1
  gCB_BufEnable(ON, EMCY_TX);
#endif /* ENABLE_EMCYMSG == 1 */

#if MAX_TX_PDOS > 0
  for (i = 0; i < MAX_TX_PDOS; i++) {
    /* enable transmit PDO in lower level buffer */
    if (mTPDO[i].bMode == 1) {
      gCB_BufEnable(ON, (BYTE)(PDO_TX + i));
      if (TPDO_PARAM[i].mTT == 255) {
        mTPDO[i].bNewValue = TRUE;
      } /* if */
      if (TPDO_PARAM[i].mTT == 254) {
        mTPDO[i].bNewValue = FALSE;
      } /* if */
    } /* if */
# if SYNC_USED == 1
    mTPDO[i].bSyncCounter = 0;
# endif /* SYNC_USED == 1 */
  } /* for */
#endif /* MAX_TX_PDOS > 0 */

#if SYNC_USED == 1
# if MAX_RX_PDOS > 0
  /* reset SYNC counter */
  for (i = 0; i < MAX_RX_PDOS; i++) {
    mRPDO[i].bSyncCounter = 0;
  } /* for */
# endif /* MAX_RX_PDOS > 0 */
#endif /* SYNC_USED == 1 */

  ModuleInfo.bCommState = OPERATIONAL;

#if STATUS_LEDS_USED == 1 
  /* set RUN LED: OPERATIONAL state */
  gLED_SetLedState(LED_OPERATIONAL, ON);
#endif /* STATUS_LEDS_USED == 1 */

#if (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1)
  /* branch to callback function */
  gSlvStackEventCallback(COMM_STATE, OPERATIONAL);
#endif /* (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1) */

#if REFRESH_ERRCTRL_TX_BUFFER == 1
  /* refresh remote buffer with actual commmunication state */
  gCan_PrepareRemoteMessage(&tCanMsgBuffer[ERRCTRL_TX].tMsg);
#endif /* REFRESH_ERRCTRL_TX_BUFFER */
} /* gGoOperational */



/*!
  \brief Request transition to PREOPERATIONAL.
*/
void gGoPreOperational(void)
{
#if MAX_TX_PDOS > 0
   BYTE XDATA i;
#endif /* MAX_TX_PDOS > 0 */

   VDEBUG_RPT0("PRE_OPERATIONAL command received.\n");

#if ENABLE_EMCYMSG == 1
  /* enable emergency message */
  gCB_BufEnable(ON, EMCY_TX);
#endif /* ENABLE_EMCYMSG == 1 */

#if MAX_TX_PDOS > 0
  for (i = 0; i < MAX_TX_PDOS; i++) {
    /* disable transmit PDO in lower level buffer */
    if (mTPDO[i].bMode == 1) {
      gCB_BufEnable(OFF, (BYTE)(PDO_TX + i));
    } /* if */
  } /* for */
  /* reset transmit requests */
  gSlvRstPDOTxReq();      /* transmit PDOs                    */
#endif /* MAX_TX_PDOS > 0 */

  ModuleInfo.bCommState = PRE_OPERATIONAL;

#if STATUS_LEDS_USED == 1 
  /* set RUN LED: PRE-OPERATIONAL state */
  gLED_SetLedState(LED_PREOP, ON);
#endif /* STATUS_LEDS_USED == 1 */

#if (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1)
  /* branch to callback function */
  gSlvStackEventCallback(COMM_STATE, PRE_OPERATIONAL);
#endif /* (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1) */

#if REFRESH_ERRCTRL_TX_BUFFER == 1
  /* refresh remote buffer with actual commmunication state */
  gCan_PrepareRemoteMessage(&tCanMsgBuffer[ERRCTRL_TX].tMsg);
#endif /* REFRESH_ERRCTRL_TX_BUFFER */
} /* gGoPreOperational */



/*!
  \brief Request transition to STOPPED.
*/
void gGoStopped(void)
{
#if MAX_TX_PDOS > 0
  BYTE XDATA i;
#endif /* MAX_TX_PDOS > 0 */

  VDEBUG_RPT0("STOP command received.\n");
  /* reset transmit requests */
  fTxSdoRequest = FALSE;  /* transmit SDO                     */

#if ENABLE_EMCYMSG == 1
  fTxEmcyRequest = FALSE; /* emergency message                */
  /* disable emergency message */
  gCB_BufEnable(OFF, EMCY_TX);
#endif /* ENABLE_EMCYMSG == 1 */

#if MAX_TX_PDOS > 0
  for (i = 0; i < MAX_TX_PDOS; i++) {
    /* disable transmit PDO in lower level buffer */
    if (mTPDO[i].bMode == 1) {
      gCB_BufEnable(OFF, (BYTE)(PDO_TX + i));
    } /* if */
  } /* for */
  /* reset transmit requests */
  gSlvRstPDOTxReq();      /* transmit PDOs                    */
#endif /* MAX_TX_PDOS > 0 */

  ModuleInfo.bCommState = STOPPED;

#if STATUS_LEDS_USED == 1 
  /* set RUN LED: STOPPED state */
  gLED_SetLedState(LED_STOPPED, ON);
#endif /* STATUS_LEDS_USED == 1 */

#if (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1)
  /* branch to callback function */
  gSlvStackEventCallback(COMM_STATE, STOPPED);
#endif /* (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1) */

#if REFRESH_ERRCTRL_TX_BUFFER == 1
  /* refresh remote buffer with actual commmunication state */
  gCan_PrepareRemoteMessage(&tCanMsgBuffer[ERRCTRL_TX].tMsg);
#endif /* REFRESH_ERRCTRL_TX_BUFFER */
} /* gGoStopped */



/*!
  \brief Request transition to RESET COMMUNICATION
*/
void gGoResetComm(void)
{
  VDEBUG_RPT0("RESET communication command received.\n");
#if ENABLE_LAYER_SERVICES == 1
  ModuleInfo.bModuleState = PREPARE_LSS;
#else
  ModuleInfo.bModuleState = PREPARE_STARTUP;
#endif /* ENABLE_LAYER_SERVICES == 1*/
  ModuleInfo.bCommState = BOOTUP;
  Tim_StopTimer();

#if STATUS_LEDS_USED == 1 
  /* set RUN LED: RESET state */
  gLED_SetLedState(LED_RESET, ON);
#endif /* STATUS_LEDS_USED == 1 */

#if (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1)
  /* branch to callback function */
  gSlvStackEventCallback(COMM_STATE, BOOTUP);
#endif /* (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1) */
} /* gGoResetComm */



/*!
  \brief Request transition to RESET NODE.
*/
void gGoResetNode(void)
{
  VDEBUG_RPT0("RESET node command received.\n");
  ModuleInfo.bModuleState = LOCAL_INIT;
  ModuleInfo.bCommState = BOOTUP;
  Tim_StopTimer();

#if STATUS_LEDS_USED == 1
  /* set RUN LED: RESET state */
  gLED_SetLedState(LED_RESET, ON);
#endif /* STATUS_LEDS_USED == 1 */
#if (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1)
  /* branch to callback function */
  gSlvStackEventCallback(COMM_STATE, BOOTUP);
#endif /* (SIGNAL_STACK_EVENT == 1) && (SIGNAL_STATE_CHANGE == 1) */

  ModulReset();  
} /* gGoResetAll */
 

#if ENABLE_LAYER_SERVICES == 1
/*!
  Store CANopen Node-ID in nonvolatile memory.

  \param id - CANopen Node-ID [1..127]
  \return CANopen Node-ID or failure (0)
*/
BYTE gStoreNodeID(BYTE id)
{
  if ((id > 0) && (id < 128)) {
    gStkGlobDat.mBoardAdr = id;
    return 0;
  } /* if */
  else {
    return 1;
  } /* else */
} /* gStoreNodeID */


/*!
  Store CAN baudrate in nonvolatile memory.

  \param br - CAN baudrate table index
  \return CAN baudrate table index or failure (0)
*/
BYTE gStoreBaudrate(BYTE br)
{
  if (br < SELECTABLE_BITTIMES) {
    gStkGlobDat.mBaudRate = br;
    return 0;
  } /* if */
  else {
    return 1;
  } /* else */
} /* gStoreBaudrate */
#endif /* ENABLE_LAYER_SERVICES == 1 */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Basic protocol stack routines.
  \par File name basicom.c
  \version 6
  \date 18.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

