/*--------------------------------------------------------------------
       COS_MAIN.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: CANopen slave main function
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>

#ifdef WIN32
# define STRICT
# include <stdio.h>
# include <conio.h>
# include <windows.h>
#endif /* WIN32 */

#include "portab.h"
#include "cos_main.h"
#include "vassert.h"
#include "vdbgprnt.h"
#include "target.h"
#include "cancntrl.h"
#include "timerdef.h"
#include "objdict.h"
#include "sdoserv.h"
#include "emcyhndl.h"
#include "profil.h"
#include "buffer.h"
#include "usrclbck.h"

#if NMT_MASTER_ENABLE == 1
# include "nmtms.h"
#endif /* NMT_MASTER_ENABLE == 1 */

#if CLIENT_ENABLE == 1
# include "sdoclnt.h"
#endif /* CLIENT_ENABLE == 1 */

#if ENABLE_LAYER_SERVICES == 1
# if ENABLE_LSS_MASTER == 1
#  include "lss_mst.h"
# else
#  include "lss_slv.h"
# endif /* ENABLE_LSS_MASTER == 1 */
#endif /* ENABLE_LAYER_SERVICES == 1 */

#if STORE_PARAMETERS_SUPP == 1
# include "nonvolst.h"
#endif /* STORE_PARAMETERS_SUPP == 1 */

#if DELAYED_SDO_ENABLE == 1
# include "sdodlyac.h"
#endif /* DELAYED_SDO_ENABLE == 1 */

#if PROFILE_DS401 == 1
# include "ds401.h"
#endif /* PROFILE_DS401 == 1 */

#if STATUS_LEDS_USED == 1
# include "co_led.h"
#endif /* STATUS_LEDS_USED == 1 */


/*--------------------------------------------------------------------*/
/*  external functions                                                */
/*--------------------------------------------------------------------*/
#ifdef WIN32
extern void gCan_IntHandler(void);
extern int gCan_Free(void);
extern void gCan_FreeHardware(void);
#endif /* WIN32 */
extern void StateMachine(void);
extern void CANopenStack(void);
extern void CheckEventTPdos(void);


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern vModInfo XDATA ModuleInfo;       /* module information */
extern BYTE XDATA bNewExtEmcy;          /* new additional pre-defined EMCY situations */


/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/
#if INFINITE_LOOP == 1
# ifdef WIN32
#  if CLIENT_ENABLE == 1
#   if EXAMPLE_OBJECTS_USED == 1
BYTE ExampleBuffer[EXAMPLE_BUFFER_SIZE];
#   endif
#  endif
# endif /* WIN32 */
#endif /* INFINITE_LOOP == 1 */


/*--------------------------------------------------------------------*/
/*  function definitions                                              */
/*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  main                                                              */
/*--------------------------------------------------------------------*/
#if INFINITE_LOOP == 1
/*!
  \brief Main loop.

  This function is compiled if
  #INFINITE_LOOP
  is defined in the adaptation header file. Add your specific routines for
  your environment.
*/
void main(void)
{
# ifdef WIN32
#  if CLIENT_ENABLE == 1
  WORD i;
#  endif /* CLIENT_ENABLE == 1 */
#  if EMCY_MAN_SPEC == 1
  BYTE bEmcyManSpecTest = 0;
  BYTE abManSpec[5] = { 0xAF, 0xFE, 0x01, 0x23, 0x45 };
#  endif /* EMCY_MAN_SPEC == 1 */
  BYTE bEmcyTest = 0;
  MSG msg;
  VDEBUG_SETUP;
# endif /* WIN32 */

# ifdef WIN32
#  if CLIENT_ENABLE == 1  
  for (i = 0; i < sizeof(ExampleBuffer); i++) {
    ExampleBuffer[i] = (BYTE)i;
  } /* for */
#  endif /* CLIENT_ENABLE == 1 */
# endif  /* WIN32 */
  /*
  Fill in preparation stuff.
  */
  PrepareEnvironment();

  while (TRUE)            /* M A I N  L O O P                     */
  {
    /*
    Fill in some cyclic stuff.
    */
# ifdef WIN32
    Sleep(1);
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
      if (msg.message == WM_QUIT) {
        PostQuitMessage(msg.wParam);
      } /* if */
      else {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      } /* else */
    } /* if */
    if(kbhit()) {
      char c = (char)getchar();
      switch (c) {
        case 'q': /* quit */
        case 'Q':
          (void)gCan_Free();
          gCan_FreeHardware();
          exit(0);
          break;
#  if CLIENT_ENABLE == 1
#   define RMT_CLIENT 4
        case 'u':
        case 'U':
          gSdoC_Enable(RMT_CLIENT); /* contact module with id RMT_CLIENT */
          gSdoC_PrepTransfer(EXAMPLE_OBJECT2, 0x0, sizeof(ExampleBuffer));
          gSdoC_Upload(ExampleBuffer);
          break;
        case 'd':
        case 'D':
          gSdoC_Enable(RMT_CLIENT); /* contact module with id RMT_CLIENT */
          gSdoC_PrepTransfer(EXAMPLE_OBJECT2, 0x0, sizeof(ExampleBuffer) - 5);
          gSdoC_Download(ExampleBuffer);
          break;
#   if SDO_BLOCK_ALLOWED == 1
        case 'w':
        case 'W':
          gSdoC_Enable(RMT_CLIENT); /* contact module with id RMT_CLIENT */
          gSdoC_PrepTransfer(EXAMPLE_OBJECT2, 0x0, sizeof(ExampleBuffer) - 5);
          gSdoC_BlkDownload(ExampleBuffer);
          break;
        case 'r':
        case 'R':
          gSdoC_Enable(RMT_CLIENT); /* contact module with id RMT_CLIENT */
          gSdoC_PrepTransfer(EXAMPLE_OBJECT2, 0x0, sizeof(ExampleBuffer));
          gSdoC_BlkUpload(ExampleBuffer);
          break;
#   endif /* SDO_BLOCK_ALLOWED == 1 */
#  endif /* CLIENT_ENABLE == 1 */
#  if ENABLE_EMCYMSG == 1
        case 'e':
        case 'E':
          bEmcyTest = (BYTE)~bEmcyTest;
          break;
#   if EMCY_MAN_SPEC == 1
        case 'f':
        case 'F':
          if (bEmcyManSpecTest == 0) {
            gSetEmcy(0xF001, ON, abManSpec);
            bEmcyManSpecTest = 1;
          } /* if */
          else {
            gSetEmcy(0xF001, OFF, abManSpec);
            bEmcyManSpecTest = 0;
          } /* else */
          break;
#   endif /* EMCY_MAN_SPEC == 1 */
#  endif /* ENABLE_EMCYMSG == 1 */
#  if ENABLE_LAYER_SERVICES == 1
        case 'l': /* execute LSS */
        case 'L':
          ModuleInfo.bModuleState = PREPARE_LSS; /* next state */
          break;
#  endif /* ENABLE_LAYER_SERVICES == 1 */
        default:
          break;
      } /* switch */
    } /* if */
    gCan_IntHandler();
    if (bEmcyTest != 0) {
      /* guarding/heartbeat failure detected */
      bNewExtEmcy |= ERR_CTRL_FAIL;
    } /* if */
    else {
      bNewExtEmcy &= ~ERR_CTRL_FAIL;
    } /* else */
# endif /* WIN32 */

    StateMachine();       /* control module's state machine */
    CANopenStack();       /* call the can handling stack */
# if ENABLE_EMCYMSG == 1
    gCheckEmcyTransmit(); /* handle emergency messages */
# endif /* ENABLE_EMCYMSG == 1 */
    TimerHandler();       /* handle timed functions */
    CheckEventTPdos();    /* handle event controlled transmit PDOs */
    gEvalEmcy();          /* evaluate new emcy errors */
  }
}
#endif /* INFINITE_LOOP == 1 */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief CANopen slave main function
  \par File name cos_main.c
  \version 78
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

