/*--------------------------------------------------------------------
       PDOEXEC.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Analyze and create PDOs
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>

#include "portab.h"
#include "cos_main.h"
#include "vassert.h"
#include "vdbgprnt.h"
#include "target.h"
#include "cancntrl.h"
#include "objdict.h"
#include "sdoserv.h"
#include "profil.h"
#include "buffer.h"
#include "usrclbck.h"
#include "pdodef.h"

#if PROFILE_DS401 == 1
# include "ds401.h"
#endif /* PROFILE_DS401 == 1 */


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern vModInfo XDATA ModuleInfo;       /* module information */
extern BYTE XDATA bNewExtEmcy;          /* new additional pre-defined EMCY situations */

/* externals from buf_____.c */
extern FAR CAN_BUF XDATA tCanMsgBuffer[MAX_CAN_BUFFER]; /* CAN message buffer */

#if MAX_RX_PDOS > 0
extern CONST VRPDOComParData CODE mRPDOAttr[MAX_RX_PDOS]; /*!< default parameters */
extern CONST MAPPING CODE atRxPdoDefMap[MAX_RX_PDOS][MAX_MAPPED_OBJECTS];
#endif /* MAX_RX_PDOS > 0 */

#if MAX_TX_PDOS > 0
extern CONST VTPDOComParData CODE mTPDOAttr[MAX_TX_PDOS]; /*!< default parameters */
extern CONST MAPPING CODE atTxPdoDefMap[MAX_TX_PDOS][MAX_MAPPED_OBJECTS];
#endif /* MAX_TX_PDOS > 0 */

extern VComParaArea XDATA gStkGlobDat; /* Storage block for non-volatile data. */


/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/
#if MAX_RX_PDOS > 0
RPDOparameter XDATA mRPDO[MAX_RX_PDOS];  /*!< receive PDO parameter   */
#endif /* MAX_RX_PDOS > 0 */

#if MAX_TX_PDOS > 0
TPDOparameter XDATA mTPDO[MAX_TX_PDOS];  /*!< transmit PDO parameter  */
#endif /* MAX_TX_PDOS > 0 */


/*--------------------------------------------------------------------*/
/*  function definitions                                              */
/*--------------------------------------------------------------------*/

/*!
  \brief Transmit a PDO.

  This call transmits a single PDO. The inhibit time is evaluated.
  \param bPdoNr    - number of PDO
  \param bBufferNr - used buffer number
*/
#if MAX_TX_PDOS > 0
void gSlvSendPDO(BYTE bPdoNr, BYTE bBufferNr)
{
# if (GENERIC_PROFILE_USED == 1) && (PROFILE_PDO_TX_HANDLING == 1)
  BYTE DATA bMappedItem; /* actual mapped item */
  WORD DATA wLocIndex;   /* 16 bit index for object dictionary entry */
# endif /* (GENERIC_PROFILE_USED == 1) && (PROFILE_PDO_TX_HANDLING == 1) */

  /* check inhibit time */
  if (mTPDO[bPdoNr].wInhibitTime != 0) {
    return;
  } /* if */

  /* buffer completed */
  tCanMsgBuffer[bBufferNr].bSta = BUF_COMPLETE;
  tCanMsgBuffer[bBufferNr].tMsg.Rtr = 0; /* reset RTR flag */

  /* reset input state indicator */
  mTPDO[bPdoNr].bNewValue = FALSE;

  /* set inhibit timer */
  mTPDO[bPdoNr].wInhibitTime = TPDO_PARAM[bPdoNr].wInhibitReload;

  /* reload event timer */
  mTPDO[bPdoNr].wEventTimer = TPDO_PARAM[bPdoNr].wEventReload;

  /* try to send message */
  if (gCan_SendBuf(bBufferNr) == BUSY_ERR) {
    /* controller busy, try again later */
    mTPDO[bPdoNr].bTransRequest = TRUE;
  } /* if */
  else {
    /* message transmitted */
    mTPDO[bPdoNr].bTransRequest = FALSE;
# if (GENERIC_PROFILE_USED == 1) && (PROFILE_PDO_TX_HANDLING == 1)
    /* additional handling for objects sent by PDO */
    /* check all entries */
    for (bMappedItem = 0; 
         bMappedItem < TPDO_MAPPING[bPdoNr][0].bObjLength; bMappedItem++) {
      /* generate Index */
      wLocIndex = 
        (WORD) (TPDO_MAPPING[bPdoNr][bMappedItem + 1].bIndexLsb | 
               (((WORD)TPDO_MAPPING[bPdoNr][bMappedItem + 1].bIndexMsb) << 8));

      /* branch to object handling routine */
      PROFILE_PDO_TX(wLocIndex, TPDO_MAPPING[bPdoNr][bMappedItem + 1].bSubIndex);
    } /* for */
# endif /* (GENERIC_PROFILE_USED == 1) && (PROFILE_PDO_TX_HANDLING == 1) */
  } /* else */
}
#endif /* MAX_TX_PDOS > 0 */


/*!
  \brief Reset PDO transmit requests.

  This function clears all pending PDO transmit requests.
*/
#if MAX_TX_PDOS > 0
void gSlvRstPDOTxReq(void)
{
  BYTE DATA i;                  /* counter                              */

  /* reset PDO transmit request */
  for (i = 0; i < MAX_TX_PDOS; i++) {
    mTPDO[i].bTransRequest = FALSE;
  }
}
#endif /* MAX_TX_PDOS > 0 */


#if (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0)
/*!
  \brief Convert the PDO buffer number to the internal PDO table index.

  The buffer number and the position in the PDO table are
  not the same. So we need a function to convert these values.
  \param bBufferNr - used buffer number
  \return index in PDO table
*/
BYTE gSlvGetPDOParIndex(BYTE bBufferNr)
{
  BYTE i; /* counter */

#if MAX_RX_PDOS > 0
  for (i = 0; i < MAX_RX_PDOS; i++) {
    if (bBufferNr == (PDO_RX + i)) {
      return i;
    } /* if */
  } /* for */
#endif /* MAX_RX_PDOS > 0 */

#if MAX_TX_PDOS > 0
  for (i = 0; i < MAX_TX_PDOS; i++) {
    if (bBufferNr == (PDO_TX + i)) {
      return i;
    } /* if */
  } /* for */
#endif /* MAX_TX_PDOS > 0 */

  return 0; /* fail safe but unrecognised */
} /* gSlvGetPDOParIndex */
#endif /* (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0) */


#if MAX_RX_PDOS > 0
/*!
  \brief Set RPDO identifier
*/
void gSlvSetRpdoIdent(void)
{
  BYTE XDATA i;                  /* counter                              */
  BYTE XDATA dlc;                /* DLC                                  */
  DWORD XDATA ident;             /* identifier                           */

  /* default parameter for receive PDOs */
  for (i = 0; i < MAX_RX_PDOS; i++) {
    /* count message bytes */
    /* default/variable mapping */
    dlc = (BYTE)(gSlvGetMapLen(RPDO_MAPPING[i], 0) >> 3);
    /* mask out control bits */
    ident = (RPDO_PARAM[i].mCOB.dw
# if ADD_NODE_ID_TO_PDO == 1
      + ModuleInfo.bBoardAdr
# endif /* ADD_NODE_ID_TO_PDO == 1 */
      ) & 0x1FFFFFFFUL;
    gCB_BufSetId((BYTE)(PDO_RX + i), ident, dlc, RX_DIR);
    if (RPDO_PARAM[i].mCOB.fb.b3 & PDO_INVALID) {
      /* disable PDO */
      mRPDO[i].bMode = 0;
      gCB_BufEnable(OFF, (BYTE)(PDO_RX + i));
    } /* if */
    else {
      /* enable PDO */
      mRPDO[i].bMode = 1;
      gCB_BufEnable(ON, (BYTE)(PDO_RX + i));
    } /* else */
    /* assigned CAN message buffers */
#if SYNC_USED == 1
    mRPDO[i].bSyncCounter = 0;   /* reset SYNC counter */
#endif /* SYNC_USED == 1 */
  } /* for */
} /* gSlvSetRpdoIdent */ 
#endif /* MAX_RX_PDOS > 0 */


#if MAX_TX_PDOS > 0
/*!
  \brief Set TPDO identifier
*/
void gSlvSetTpdoIdent(void)
{
  BYTE XDATA i;                  /* counter                              */
  BYTE XDATA dlc;                /* DLC                                  */
  DWORD XDATA ident;             /* identifier                           */

  /* default parameter for transmit PDOs */
  for (i = 0; i < MAX_TX_PDOS; i++) {
    /* count message bytes */
    /* default/variable mapping */
    dlc = (BYTE)(gSlvGetMapLen(TPDO_MAPPING[i], 0) >> 3);
    /* mask out control bits */
    ident = (TPDO_PARAM[i].mCOB.dw
#if ADD_NODE_ID_TO_PDO == 1
      + ModuleInfo.bBoardAdr
#endif /* ADD_NODE_ID_TO_PDO == 1 */
      ) & 0x1FFFFFFFUL;
    gCB_BufSetId((BYTE)(PDO_TX + i), ident, dlc, TX_DIR);
    /* assigned CAN message buffers */
# if SYNC_USED == 1
    mTPDO[i].bSyncCounter = 0;   /* reset SYNC counter           */
# endif /* SYNC_USED == 1 */
    mTPDO[i].wEventTimer = 0;    /* event timer                  */
    mTPDO[i].wInhibitTime = 0;   /* inhibit timer                */
# if MULTIPLE_RTR_USED == 1
    if (TPDO_PARAM[i].mCOB.fb.b3 & PDO_NO_RTR) {
      mTPDO[i].bRemoteAllowed = 0; /* no RTR allowed             */
    } /* if */
    else {
      mTPDO[i].bRemoteAllowed = 1; /* RTR allowed                */
    } /* else */
# else
    mTPDO[i].bRemoteAllowed = 0; /* remote request not allowed   */
# endif /* MULTIPLE_RTR_USED == 1 */
    if (TPDO_PARAM[i].mCOB.fb.b3 & PDO_INVALID) {
      mTPDO[i].bMode = 0;          /* disable PDO                */
    } /* if */
    else {
      mTPDO[i].bMode = 1;        /* enable PDO                   */
    } /* else */
  } /* for */
} /* gSlvSetTpdoIdent */
#endif /* MAX_TX_PDOS > 0 */


/*!
  \brief Analyse the mapping of a PDO.

  This function analyses the mapping of a PDO for a given message.
  This function is called for variable AND fixed mapping,
  because of the similar mapping structures in both cases.
  If you plan to support only fixed mapping and if you have
  the feeling that this function is too flexible it is recommended
  to rewrite it according to your fixed mapping adjustments.
  This function signals EMCY conditions if the mapping of
  the PDO and the length of the received message are not equal.

  \param pUsedMap - pointer to mapping entry
  \param bBufferNr - buffer location where PDO is stored
*/
#if MAX_RX_PDOS > 0
# if (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1)
void gSlvAnalyzePDO(BYTE bBufferNr)
# else
void gSlvAnalyzePDO(CONST MAPPING PDO_MAPPING_PTR * pUsedMap, BYTE bBufferNr)
# endif /* (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1) */
{
  BYTE DATA bPosition;          /* actual byte position               */

# if (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1)
  /* copy 8 bytes directly from the message to the output buffer */
  for (bPosition = 0; bPosition < 8; bPosition++) {
    *OUTPUT_DATA(((bBufferNr - PDO_RX) << 3) + bPosition) =
      MESSAGE_DB(bBufferNr, bPosition);
  } /* for */
# else
  BYTE DATA bMappedItem;        /* actual mapped item                 */
#  if GENERIC_PROFILE_USED == 1
  WORD wLocIndex; /* 16 bit index for object dictionary entry */
#  endif /* GENERIC_PROFILE_USED == 1 */

  if (pUsedMap == 0) {
    return;
  } /* if */

  /*
  Handle incorrect PDO length now.
  Due to misconfiguration it can happen that the received PDO
  does not fit into the mapping.
  1. mapping > message dlc
  The PDO must be rejected.
  2. mapping < message dlc
  The PDO could be evaluated for the valid bytes.
  In both cases an EMCY message will be generated.
  */
  /* get length in bits */
  bPosition = gSlvGetMapLen(pUsedMap, 0);

  /* for comparison multiply dlc with 8 */
  if (bPosition > (BYTE)(tCanMsgBuffer[bBufferNr].tMsg.Dlc << 3)) {
    /* received PDO smaller than mapping */
    bNewExtEmcy |= MAP_EXCEED;
    /* received PDO is not processed */
    return;
  } /* if */

  /* recycle variable bPosition - reset byte position */
  bPosition = 0;

  /* check all entries */
  for (bMappedItem = 0; bMappedItem < pUsedMap[0].bObjLength; bMappedItem++) {
#  if GENERIC_PROFILE_USED == 1
    /* generate index */
    wLocIndex = (WORD) (pUsedMap[bMappedItem + 1].bIndexLsb | 
                       (((WORD)pUsedMap[bMappedItem + 1].bIndexMsb) << 8));
#  endif /* GENERIC_PROFILE_USED == 1 */

    /* check object length */
    switch (pUsedMap[bMappedItem + 1].bObjLength) {
      case OBJ_LEN8:
        /* copy DATA */
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          LOCK_APPLICATION_WDATA;
          pUsedMap[bMappedItem + 1].pData[0] = MESSAGE_DB(bBufferNr, bPosition++);
          UNLOCK_APPLICATION_WDATA;

#  if GENERIC_PROFILE_USED == 1
          /* call receive PDO data evaluation function */
          PROFILE_PDO_ANALYZE(wLocIndex, pUsedMap[bMappedItem + 1].bSubIndex);
#  endif /* GENERIC_PROFILE_USED == 1 */
        } /* if */
        else {
          bPosition += 1; /* next object */
        } /* else */
        break;

      case OBJ_LEN16:
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          LOCK_APPLICATION_WDATA;
          /* copy DATA */
          (*((WBYTE *)pUsedMap[bMappedItem + 1].pData)).bc.lb /* low byte */
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((WBYTE*)pUsedMap[bMappedItem + 1].pData)).bc.mb /* high byte */
          = MESSAGE_DB(bBufferNr, bPosition++);
          UNLOCK_APPLICATION_WDATA;

#  if GENERIC_PROFILE_USED == 1
          /* call receive PDO data evaluation function */
          PROFILE_PDO_ANALYZE(wLocIndex, pUsedMap[bMappedItem + 1].bSubIndex);
#  endif /* GENERIC_PROFILE_USED == 1 */
        } /* if */
        else {
          bPosition += 2; /* next object */
        } /* else */
        break;

      case OBJ_LEN24:
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          LOCK_APPLICATION_WDATA;
          /* copy DATA */
          (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b0 /* low byte */
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b1 /* mid byte */
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b2 /* high byte */
          = MESSAGE_DB(bBufferNr, bPosition++);
          UNLOCK_APPLICATION_WDATA;

#  if GENERIC_PROFILE_USED == 1
          /* call receive PDO data evaluation function */
          PROFILE_PDO_ANALYZE(wLocIndex, pUsedMap[bMappedItem + 1].bSubIndex);
#  endif /* GENERIC_PROFILE_USED == 1 */
        } /* if */
        else {
          bPosition += 3; /* next object */
        } /* else */
        break;

      case OBJ_LEN32:
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          LOCK_APPLICATION_WDATA;
          /* copy DATA */
          (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b0 /* low byte low word */
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b1 /* high byte low word */
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b2 /* low byte high word */
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b3 /* high byte high word */
          = MESSAGE_DB(bBufferNr, bPosition++);
          UNLOCK_APPLICATION_WDATA;

#  if GENERIC_PROFILE_USED == 1
          /* call receive PDO data evaluation function */
          PROFILE_PDO_ANALYZE(wLocIndex, pUsedMap[bMappedItem + 1].bSubIndex);
#  endif /* GENERIC_PROFILE_USED == 1 */
        } /* if */
        else {
          bPosition += 4; /* next object */
        } /* else */
        break;

#  if OBJECTS_64BIT_USED == 1
      case OBJ_LEN64:
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          LOCK_APPLICATION_WDATA;
          /* copy DATA */
          (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b0
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b1
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b2
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b3
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b4
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b5
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b6
          = MESSAGE_DB(bBufferNr, bPosition++);
          (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b7
          = MESSAGE_DB(bBufferNr, bPosition++);
          UNLOCK_APPLICATION_WDATA;

#   if GENERIC_PROFILE_USED == 1
          /* call receive PDO data evaluation function */
          PROFILE_PDO_ANALYZE(wLocIndex, pUsedMap[bMappedItem + 1].bSubIndex);
#   endif /* GENERIC_PROFILE_USED == 1 */
        } /* if */
        else {
          bPosition += 8; /* next object */
        } /* else */
        break;
#  endif /* OBJECTS_64BIT_USED == 1 */

      default:
        /* ignore the value */  
        break;
    } /* switch */

    if (bPosition > 8) {
      bMappedItem = pUsedMap[0].bObjLength; /* fulfill break condition */
    } /* if */
  } /* for */

# endif /* (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1) */

  /* check receive PDO callback function */
# if PDO_EXEC_USER_RECEIVE_CALLBACKS == 1
    gSlvRxPdoCallback((BYTE)(bBufferNr - PDO_RX));
# endif /* PDO_EXEC_USER_RECEIVE_CALLBACKS == 1 */
}
#endif /* MAX_RX_PDOS > 0 */


/*!
  \brief Create a PDO with mapping.

  This function writes data according to the used mapping entry
  to a PDO.
  This function is called for variable AND fixed mapping.
  Because of the similar mapping structures in both cases.
  If you plan to support only fixed mapping and if you have
  the feeling that this function is to flexible it is recommended
  to rewrite it according to your fixed mapping adjustments.
  
  \param pUsedMap - pointer to mapping entry
  \param bBufferNr - buffer location where PDO is stored

  \retval TRUE - at least one of the mapped values has been changed
  since last update.
  \retval FALSE - no changings since last update of mapped data.
*/
#if MAX_TX_PDOS > 0
# if (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1)
BOOLEAN gSlvCreatePDO(BYTE bBufferNr)
# else
BOOLEAN gSlvCreatePDO(CONST MAPPING PDO_MAPPING_PTR * pUsedMap, BYTE bBufferNr)
# endif /* (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1) */
{
# if (TPDO_MAP_READONLY == 0) || (TPDO_DIRECT_COPY == 0)
  BYTE DATA bMappedItem;        /* actual mapped item                 */
# endif /* (TPDO_MAP_READONLY == 0) */
  BYTE DATA bPosition;          /* actual byte position               */
  BOOLEAN DATA fValChanged;     /* true if values have changed        */
# if GENERIC_PROFILE_USED == 1
  WORD DATA wLocIndex; /* 16 bit index for object dictionary entry */
  BYTE DATA abPdoDataTmpBuffer[8]; /* temp buffer for PDO event trigger evaluation */
# endif /* GENERIC_PROFILE_USED == 1 */

  /* default: no changes */
  fValChanged = FALSE;

# if (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1)
  /*
    Compare the message bytes
    and then copy 8 bytes directly from input buffer to message.
   */
  for (bPosition = 0; bPosition < 8; bPosition++) {
    if (MESSAGE_DB(bBufferNr, bPosition) !=
      *INPUT_DATA(((bBufferNr - PDO_TX) << 3) + bPosition)) {
      fValChanged = TRUE; /* changing detected */
    } /* if */
    MESSAGE_DB(bBufferNr, bPosition) =
      *INPUT_DATA(((bBufferNr - PDO_TX) << 3) + bPosition);
  } /* for */
# else

  if (pUsedMap == 0) {
    return fValChanged;
  } /* if */

  /* reset byte position */
  bPosition = 0;

  /* check all entries */
  for (bMappedItem = 0; bMappedItem < pUsedMap[0].bObjLength; bMappedItem++) {
#  if GENERIC_PROFILE_USED == 1
    /* generate Index */
    wLocIndex = (WORD) (pUsedMap[bMappedItem + 1].bIndexLsb | 
                       (((WORD)pUsedMap[bMappedItem + 1].bIndexMsb) << 8));
#  endif /* GENERIC_PROFILE_USED == 1 */

    /* check object length */
    switch (pUsedMap[bMappedItem + 1].bObjLength) {
      case OBJ_LEN8:
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          /* copy DATA */
          LOCK_APPLICATION_RDATA;

#  if GENERIC_PROFILE_USED == 1
          /* check transmit PDO value change evaluation function */
          fValChanged |= PROFILE_PDO_CREATE(wLocIndex,
                           pUsedMap[bMappedItem + 1].bSubIndex, 
                           (FAR BYTE XDATA *) &MESSAGE_DB(bBufferNr, bPosition));

          /* copy PDO data to temp buffer */  
          abPdoDataTmpBuffer[bPosition]
            = pUsedMap[bMappedItem + 1].pData[0];
#  else
          /* compare buffer with input value */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != pUsedMap[bMappedItem + 1].pData[0]) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = pUsedMap[bMappedItem + 1].pData[0];
          } /* if */
#  endif /* GENERIC_PROFILE_USED == 1 */

          UNLOCK_APPLICATION_RDATA;
          bPosition++;
        }
        else {
          bPosition++;
        } /* else */
        break;

      case OBJ_LEN16:
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          /* copy DATA */
          LOCK_APPLICATION_RDATA;

#  if GENERIC_PROFILE_USED == 1
          /* check transmit PDO value change evaluation function */
          fValChanged |= PROFILE_PDO_CREATE(wLocIndex,
                           pUsedMap[bMappedItem + 1].bSubIndex, 
                           (FAR BYTE XDATA *) &MESSAGE_DB(bBufferNr, bPosition));

          /* copy PDO data low byte to temp */
          abPdoDataTmpBuffer[bPosition]
            = (*((WBYTE*)pUsedMap[bMappedItem + 1].pData)).bc.lb;

          bPosition++;

          /* copy PDO data high byte to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((WBYTE*)pUsedMap[bMappedItem + 1].pData)).bc.mb;
#  else
          /* compare buffer with input value */
          /* low byte */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((WBYTE*)pUsedMap[bMappedItem + 1].pData)).bc.lb) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((WBYTE*)pUsedMap[bMappedItem + 1].pData)).bc.lb;
          } /* if */
          bPosition++;
          /* high byte */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((WBYTE*)pUsedMap[bMappedItem + 1].pData)).bc.mb) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((WBYTE*)pUsedMap[bMappedItem + 1].pData)).bc.mb;
          } /* if */
#  endif /* GENERIC_PROFILE_USED == 1 */

          bPosition++;
          UNLOCK_APPLICATION_RDATA;
        }
        else {
          bPosition += 2;
        } /* else */
      break;

      case OBJ_LEN24:
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          /* copy DATA */
          LOCK_APPLICATION_RDATA;

#  if GENERIC_PROFILE_USED == 1
          /* check transmit PDO value change evaluation function */
          fValChanged |= PROFILE_PDO_CREATE(wLocIndex,
                           pUsedMap[bMappedItem + 1].bSubIndex, 
                           (FAR BYTE XDATA *) &MESSAGE_DB(bBufferNr, bPosition));

          /* copy PDO data low byte to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b0;

          bPosition++;

          /* copy PDO data mid byte to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b1;

          bPosition++;

          /* copy PDO data high byte to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b2;
#  else
          /* compare buffer with input value */
          /* low byte */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b0) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b0;
          } /* if */
          bPosition++;
          /* mid byte */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b1) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b1;
          } /* if */
          bPosition++;
          /* high byte */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b2) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b2;
          } /* if */
#  endif /* GENERIC_PROFILE_USED == 1 */

          bPosition++;
          UNLOCK_APPLICATION_RDATA;
        }
        else {
          bPosition += 3;
        } /* else */
      break;

      case OBJ_LEN32:
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          /* copy DATA */
          LOCK_APPLICATION_RDATA;

#  if GENERIC_PROFILE_USED == 1
          /* check transmit PDO value change evaluation function */
          fValChanged |= PROFILE_PDO_CREATE(wLocIndex,
                           pUsedMap[bMappedItem + 1].bSubIndex, 
                           (FAR BYTE XDATA *) &MESSAGE_DB(bBufferNr, bPosition));

          /* copy PDO data low word low byte to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b0;

          bPosition++;

          /* copy PDO data low word high byte to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b1;

          bPosition++;

          /* copy PDO data high word low byte to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b2;

          bPosition++;

          /* copy PDO data high word high byte to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b3;
#  else
          /* compare buffer with input value */
          /* low word low byte */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b0) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b0;
          } /* if */
          bPosition++;
          /* low word high byte */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b1) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b1;
          } /* if */
          bPosition++;
          /* high word low byte */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b2) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b2;
          } /* if */
          bPosition++;
          /* high word high byte */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b3) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((QBYTE*)pUsedMap[bMappedItem + 1].pData)).fb.b3;
          } /* if */
#  endif /* GENERIC_PROFILE_USED == 1 */

          bPosition++;
          UNLOCK_APPLICATION_RDATA;
        }
        else {
          bPosition += 4;
        } /* else */
      break;

#  if OBJECTS_64BIT_USED == 1
      case OBJ_LEN64:
        if (pUsedMap[bMappedItem + 1].pData != 0) {
          /* copy DATA */
          LOCK_APPLICATION_RDATA;

#   if GENERIC_PROFILE_USED == 1
          /* check transmit PDO value change evaluation function */
          fValChanged |= PROFILE_PDO_CREATE(wLocIndex,
                           pUsedMap[bMappedItem + 1].bSubIndex, 
                           (FAR BYTE XDATA *) &MESSAGE_DB(bBufferNr, bPosition));

          /* copy PDO data to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b0;

          bPosition++;

          /* copy PDO data to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b1;

          bPosition++;

          /* copy PDO data to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b2;

          bPosition++;

          /* copy PDO data to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b3;

          bPosition++;

          /* copy PDO data to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b4;

          bPosition++;

          /* copy PDO data to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b5;

          bPosition++;

          /* copy PDO data temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b6;

          bPosition++;

          /* copy PDO data to temp buffer */
          abPdoDataTmpBuffer[bPosition]
            = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b7;
#   else
          /* compare buffer with input value */
          /*  */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b0) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b0;
          } /* if */
          bPosition++;
          /*  */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b1) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b1;
          } /* if */
          bPosition++;
          /*  */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b2) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b2;
          } /* if */
          bPosition++;
          /*  */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b3) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b3;
          } /* if */
          bPosition++;
          /*  */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b4) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b4;
          } /* if */
          bPosition++;
          /*  */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b5) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b5;
          } /* if */
          bPosition++;
          /*  */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b6) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b6;
          } /* if */
          bPosition++;
          /*  */
          if (MESSAGE_DB(bBufferNr, bPosition)
            != (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b7) {
            /* values have changed */
            fValChanged = TRUE;
            MESSAGE_DB(bBufferNr, bPosition)
              = (*((OBYTE*)pUsedMap[bMappedItem + 1].pData)).eb.b7;
          } /* if */
#   endif /* GENERIC_PROFILE_USED == 1 */

          bPosition++;
          UNLOCK_APPLICATION_RDATA;
        }
        else {
          bPosition += 8;
        } /* else */
      break;
#  endif /* OBJECTS_64BIT_USED == 1 */

    default:
      /* ignore the value */  
      break;
    } /* switch */

    if (bPosition > 8) {
      bMappedItem = pUsedMap[0].bObjLength; /* fulfill break condition */
    } /* if */
  } /* for */

  /* set CAN message length to object length */
  if (tCanMsgBuffer[bBufferNr].tMsg.Dlc != bPosition) {
    tCanMsgBuffer[bBufferNr].tMsg.Dlc = bPosition;
    gCB_BufChangeId(bBufferNr, tCanMsgBuffer[bBufferNr].tMsg.qbId.dw);
  } /* if */

#  if GENERIC_PROFILE_USED == 1
  if (fValChanged == TRUE) {
    /* at least one mapped object has created a "value changed" event */
    /* update the PDO data of all mapped objects */
    MemCpy(&MESSAGE_DB0(bBufferNr), &abPdoDataTmpBuffer[0], tCanMsgBuffer[bBufferNr].tMsg.Dlc);
  } /* if */
#  endif /* GENERIC_PROFILE_USED == 1 */
# endif /* (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1) */
  
  return fValChanged;
}
#endif /* MAX_TX_PDOS > 0 */


/*!
  \brief Check for pending transmit PDOs.

  This function checks if a transmission has to be triggered for a PDO.
  Therefore the PDOs are created using gSlvCreatePDO().
*/
void CheckEventTPdos(void)
{
#if MAX_TX_PDOS > 0
  BYTE DATA i;                  /* counter                              */

  /* event mode for TX PDOs */
  if (ModuleInfo.bCommState == OPERATIONAL) {
    for (i = 0; i < MAX_TX_PDOS; i++) {
      if (mTPDO[i].bMode) {
        switch (TPDO_PARAM[i].mTT) {
          case 0 : /* transmission type 0 */
          case 253 : /* transmit PDO in RTR only mode */
            if (CREATE_PDO(TPDO_MAPPING[i], (BYTE)(PDO_TX + i))
              == TRUE) {
              mTPDO[i].bNewValue = TRUE;
            }
            break;
          case 254 : /* transmit PDO - event manufacturer defined  */
            CREATE_PDO(TPDO_MAPPING[i], (BYTE)(PDO_TX + i));
            /* check about new values */
            if (mTPDO[i].bNewValue == TRUE) {
              /* new values available */
              gSlvSendPDO(i, (BYTE)(PDO_TX + i));
            }
            break;
          case 255 : /* transmit PDO - event defined in device profile */
            if (CREATE_PDO(TPDO_MAPPING[i], (BYTE)(PDO_TX + i))
              == TRUE) {
               mTPDO[i].bNewValue = TRUE;
            } /* if */
            /* check about new values */
            if (mTPDO[i].bNewValue == TRUE) {
              /* new values available */
              gSlvSendPDO(i, (BYTE)(PDO_TX + i));
            }
            break;
          default : /* sync mode - values 1..241 */
            /* transmission type 1 <= tt < 241 */
            if (CREATE_PDO(TPDO_MAPPING[i], (BYTE)(PDO_TX + i))
              == TRUE) {
              mTPDO[i].bNewValue = TRUE;
            } /* if */
            break;
        } /* switch */
      } /* if */
    } /* for */
  } /* if */
#endif /* MAX_TX_PDOS > 0 */
}


/*!
  \brief Get the effective mapping length.

  This function returns the actual mapping length
  for the addressed mapping structure.

  \param tIndexMap - Pointer to a valid mapping.
  \param bSkip - Specifies the mapping item which
  has not to be considered for calculation.
  If bSkip is 0 the complete mapping length is returned.

  \return Number of bits occupied by mapped data.
*/
BYTE gSlvGetMapLen(CONST MAPPING PDO_MAPPING_PTR *tIndexMap, BYTE bSkip)
{
  BYTE i;                       /* counter                              */
  BYTE bLen = 0;                /* sub index                            */

  assert(tIndexMap != (CONST MAPPING PDO_MAPPING_PTR *)0);

  /* check all entries */
  for (i = 1; i <= tIndexMap[0].bObjLength; i++) {
    if (i != bSkip) {
      bLen = (BYTE)(bLen + tIndexMap[i].bObjLength);
    } /* if */
  } /* for */
  return bLen;
} /* gSlvGetMapLen */


/*!
  \brief Force a transmit PDO to be transmitted.

  The transmit PDO will be transmitted even if there
  is no change in the PDO content.
  \param bPdoNr - transmit PDO number
*/
#if MAX_TX_PDOS > 0
void gSlvForceTxPdoEvent(BYTE bPdoNr)
{
  if ((bPdoNr - 1) < MAX_TX_PDOS) {
    mTPDO[bPdoNr - 1].bNewValue = TRUE;
  }
}
#endif /* MAX_TX_PDOS > 0 */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Analyze and create PDOs
  \par File name pdoexec.c
  \version 3
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

