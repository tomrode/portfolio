/*--------------------------------------------------------------------
       PDOCFG.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Initial configuration of PDO entries.
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "portab.h"
#include "cos_main.h"
#include "profil.h"
#include <string.h>
#include "Truck_CAN.h"

/*--------------------------------------------------------------------*/
/*  local definitions                                                 */
/*--------------------------------------------------------------------*/

/*!
  \def RPDO_COMM(id, ttype)
  Add a parameter entry to a communication parameter table structure.
  \param id - CAN id entry
  \param ttype - transmission type
*/
#define RPDO_COMM(id, ttype) \
{ {id}, ttype }

/*!
  \def TPDO_COMM(id, ttype, time)
  Add a parameter entry to a communication parameter table structure.
  \param id - CAN id entry
  \param ttype - transmission type
  \param inhibit - inhibit time
  \param time - event timer
*/
#define TPDO_COMM(id, ttype, inhibit, time) \
{ {id}, time, inhibit, ttype }


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern VComParaArea XDATA gStkGlobDat; /*!< Storage block for non-volatile data. */


/*--------------------------------------------------------------------*/
/*  public data                                                       */
/*--------------------------------------------------------------------*/
#if MAX_RX_PDOS > 0
/*!
  \brief Default communication parameters for receive PDOs.

  This communication parameter block is placed in ROM.
  It should be adapted to the needs of the application.
  The communication parameters in the file rpdocomm.h
  can be overridden by the user.
*/
CONST VRPDOComParData CODE mRPDOAttr[MAX_RX_PDOS] = {
# include "rpdocomm.h"
};
#endif /* MAX_RX_PDOS > 0 */


#if MAX_TX_PDOS > 0
/*!
  \brief Default communication parameters for transmit PDOs.

  This communication parameter block is placed in ROM.
  It should be adapted to the needs of the application.
  The communication parameters in the file tpdocomm.h
  can be overridden by the user.
*/

CONST VTPDOComParData CODE mTPDOAttr[MAX_TX_PDOS] = {
# include "tpdocomm.h"
};
#endif /* MAX_TX_PDOS > 0 */


/*--------------------------------------------------------------------*/
/*  function definitions                                              */
/*--------------------------------------------------------------------*/
/*!
  \brief Initialise the communication parameters.

  The communication parameters will be copied from the
  ROM (#mTPDOAttr, #mRPDOAttr) image to the
  RAM (#gStkGlobDat).
*/
void InitComPars(void)
{
#if (RPDO_PAR_READONLY == 0) || (TPDO_PAR_READONLY == 0)
  BYTE i;
#endif /* (RPDO_PAR_READONLY == 0) || (TPDO_PAR_READONLY == 0) */

  /* initialise communication parameter of RX PDOs */
#if RPDO_PAR_READONLY == 0
# if MAX_RX_PDOS > 0
  for (i = 0; i < MAX_RX_PDOS; i++) {
    if (mRPDOAttr[i].mCOB.fb.b0 || mRPDOAttr[i].mCOB.fb.b1) {
      /* There is ROM'd data - initialise */
      *(VRPDOComParData *)&gStkGlobDat.mRPDOAttr[i]
        = *(VRPDOComParData FCONST CONST_PTR *)&mRPDOAttr[i];
    } /* if */
    else {
      /* There is no valid configuration - invalidate PDO */
      gStkGlobDat.mRPDOAttr[i].mCOB.fb.b3 = 0x80;
    } /* else */
  } /* for */
# endif /* MAX_RX_PDOS > 0 */
#endif /* RPDO_PAR_READONLY == 0 */

  /* initialise communication parameter of TX PDOs */
#if TPDO_PAR_READONLY == 0
# if MAX_TX_PDOS > 0
  for (i = 0; i < MAX_TX_PDOS; i++) {
    if (mTPDOAttr[i].mCOB.fb.b0 || mTPDOAttr[i].mCOB.fb.b1) {
      /* There is ROM'd data - initialise */
      *(VTPDOComParData *)&gStkGlobDat.mTPDOAttr[i]
        = *(VTPDOComParData FCONST CONST_PTR *)&mTPDOAttr[i];
    } /* if */
    else {
      /* There is no valid configuration - invalidate PDO */
      gStkGlobDat.mTPDOAttr[i].mCOB.fb.b3 = 0x80;
    } /* else */
  } /* for */
# endif /* MAX_TX_PDOS > 0 */
#endif /* TPDO_PAR_READONLY == 0 */
} /* InitComPars */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Initial configuration of the PDO related entries
  \par File name pdocfg.c
  \version 8
  \date 28.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart

  The communication parameter relevant initialisation information
  is located in this file.
  All the information is already provided in an intel coded way.
  The provided variables are located in ROM and are transferred to the
  object dictionary during the function call to InitComPars().
*/

