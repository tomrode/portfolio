/*--------------------------------------------------------------------
       BUFFER.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Basic routines to filter and store CAN messages
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "portab.h"
#include "cos_main.h"
#include "cancntrl.h"
#include "inst_def.h"
#include "vqueue.h"
#include "buffer.h"
#include "objdict.h"
#include "sdoserv.h"
#include "vassert.h"
#include "vdbgprnt.h"
#ifndef __18CXX
# include <stdio.h>
#endif /* __18CXX */
#include <string.h>
#include <stdlib.h>
#include "usrclbck.h"


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern vModInfo XDATA ModuleInfo;       /* module information */
extern BOOLEAN DATA fEmcyOverrun;       /* EMCY overrun flag */
extern BOOLEAN DATA fEmcyIdCollision;   /* EMCY ID collision flag */


/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/
/*! semaphore for locking second call of transmit function */
volatile BOOLEAN DATA fTxEnter;
/*! flag for service second call of transmit function */
volatile BOOLEAN DATA fTxService;
/*! CAN message buffer */
CAN_BUF FAR XDATA tCanMsgBuffer[MAX_CAN_BUFFER];

/*!
  CAN identifier filter table.
*/
#if ID_FILTER == 1
# ifdef __18CXX
#  pragma udata idfilter=0x100
BYTE XDATA abIdFilter[256];
#  pragma udata
# else
BYTE XDATA abIdFilter[256];
# endif /* __18CXX */
#else
# if ID_FILTER == 2
WORD XDATA awIdFilter[256];
# else
#  if ID_FILTER == 4
DWORD XDATA alIdFilter[256];
#  else
#   if ID_FILTER == 8
BYTE XDATA abIdFilter[CAN_NUM_ID];
#   endif /* ID_FILTER == 8 */
#  endif /* ID_FILTER == 4 */
# endif /* ID_FILTER == 2 */
#endif /* ID_FILTER == 1 */

#if QUEUED_MODE == 1
# if SIGNAL_EMCYMSG == 1
/*! set limits to signal messages in EMCY range */
CONST VSignalIdArea CODE SignalEmcy[1] = {
  { { EMCY_ID + 1 }, { EMCY_ID + 127 } }
};
# endif /* SIGNAL_EMCYMSG == 1 */

# if SIGNAL_BOOTUPMSG == 1
/*! set limits to signal messages in error control range */
CONST VSignalIdArea CODE SignalBootUp[1] = {
  { { ERRCTRL_ID + 1 }, { ERRCTRL_ID + 127 } }
};
# endif /* SIGNAL_BOOTUPMSG == 1 */
#endif /* QUEUED_MODE == 1 */

/*! CAN receive queue */
#ifdef EIGHT_BIT_MICRO
BYTE XDATA tCanRxQueue[sizeof(tVQActual) + Q_RX_LENGTH];
#else
WORD XDATA tCanRxQueue[(sizeof(tVQActual) + Q_RX_LENGTH) / 2];
#endif /* EIGHT_BIT_MICRO */
tVQUEUE CONST CODE tCanRxQueDesc = {
  Q_RX_CAN_ELEMENTS,
  (tVQActual XDATA *)tCanRxQueue
};

/*! CAN transmit queue */
#ifdef EIGHT_BIT_MICRO
BYTE XDATA tCanTxQueue[sizeof(tVQActual) + Q_TX_LENGTH];
#else
WORD XDATA tCanTxQueue[(sizeof(tVQActual) + Q_TX_LENGTH) / 2];
#endif /* EIGHT_BIT_MICRO */
tVQUEUE CONST CODE tCanTxQueDesc = {
  Q_TX_CAN_ELEMENTS,
  (tVQActual XDATA *)tCanTxQueue
};


/*--------------------------------------------------------------------*/
/*  public functions                                                  */
/*--------------------------------------------------------------------*/

/*!
  \brief Initialize CAN message buffering.

  This function is called from the protocol stack to set up the
  message buffers and optional queues.
*/
void gCB_Init(void)
{
  /* clear CAN identifier filter */
  gCB_ClearIdFilter(CAN_NO_MANAGER);

  /* init receive queue */
  gQue_FlushQueue((VQHANDLE)&tCanRxQueDesc);

  /* init transmit queue */
  gQue_FlushQueue((VQHANDLE)&tCanTxQueDesc);

  fTxEnter = TRUE;
  fTxService = FALSE;
} /* gCB_Init */


/*!
  \brief Reset identifier filter.

  Clear all or some of the identifier filter table entries.
  \param bManager set to CAN_NO_MANAGER to reset any identifier
         otherwise only assigned identifiers are cleared
*/
void gCB_ClearIdFilter(BYTE bManager)
{
#if ID_FILTER > 0
  WORD DATA i;                          /* counter                      */
#endif /* ID_FILTER > 0 */

  if (bManager == CAN_NO_MANAGER) {     /* any identifier               */
#if ID_FILTER == 1
    for (i = 0; i < 256; i++) {
      abIdFilter[i] = 0;                /* clear CAN identifier filter  */
    } /* for */
#else
# if ID_FILTER == 2
    for (i = 0; i < 256; i++) {
      awIdFilter[i] = 0;                /* clear CAN identifier filter  */
    } /* for */
# else
#  if ID_FILTER == 4
    for (i = 0; i < 256; i++) {
      alIdFilter[i] = 0;                /* clear CAN identifier filter  */
    } /* for */
#  else
#   if ID_FILTER == 8
    for (i = 0; i < CAN_NUM_ID; i++) {
      abIdFilter[i] = CAN_NO_MANAGER;   /* clear CAN identifier filter  */
    } /* for */
#   endif /* ID_FILTER == 8 */
#  endif /* ID_FILTER == 4 */
# endif /* ID_FILTER == 2 */
#endif /* ID_FILTER == 1 */
  } /* if */
  else {                                /* only assigned manager        */
#if ID_FILTER == 1
    for (i = 0; i < 256; i++) {
      abIdFilter[i] = 0;                /* clear CAN identifier filter  */
    } /* for */
#else
# if ID_FILTER == 2
    BYTE DATA j;                        /* counter                      */
    WORD DATA k;                        /* bit pair position            */

    for (i = 0; i < 256; i++) {
      for (j = 0; j < 8; j++) {
        k = (WORD)(0x0003 << (j << 1));
        if ((awIdFilter[i] & k) == (WORD)bManager << (j << 1)) {
          awIdFilter[i] &= ~k;          /* clear CAN identifier filter  */
        } /* if */
      } /* for */
    } /* for */
# else
#  if ID_FILTER == 4
    BYTE DATA j;                        /* counter                      */
    DWORD DATA k;                       /* nibble position              */

    for (i = 0; i < 256; i++) {
      for (j = 0; j < 8; j++) {
        k = 0x0FUL << (j << 2);
        if ((alIdFilter[i] & k) == (DWORD)bManager << (j << 2)) {
          alIdFilter[i] &= ~k;          /* clear CAN identifier filter  */
        } /* if */
      } /* for */
    } /* for */
#  else
#   if ID_FILTER == 8
    for (i = 0; i < CAN_NUM_ID; i++) {
      if (abIdFilter[i] == bManager) {
        abIdFilter[i] = CAN_NO_MANAGER; /* clear CAN identifier filter  */
      } /* if */
    } /* for */
#   endif /* ID_FILTER == 8 */
#  endif /* ID_FILTER == 4 */
# endif /* ID_FILTER == 2 */
#endif /* ID_FILTER == 1 */
  } /* else */
} /* gCB_ClearIdFilter */


/*!
  \brief Set identifier filter.

  Configure one of the identifier filter table entries.
  \param wId any CAN identifier
  \param bManager set to CAN_NO_MANAGER to reset the given identifier
         otherwise assign identifier to the manager
  \return NO_ERR if no error occured, VALUE_TOO_HIGH_ERR if identifier
          table exceeded, ID_BUSY if identifier already allocated
*/
BYTE gCB_ConfigureIdFilter(WORD wId, BYTE bManager)
{
  if (wId >= CAN_NUM_ID) {
    return VALUE_TOO_HIGH_ERR;          /* OUT: ID table exceeded       */
  } /* if */

#if ID_FILTER == 1
  {
    BYTE DATA i;                        /* index                        */
    BYTE DATA k;                        /* bit position                 */

    i = (BYTE)(wId >> 3);               /* i = wId / 8                  */
    k = (BYTE)(0x01 << ((BYTE)wId & 0x07)); /* k = 2 ^ (wId % 8)        */

    if (bManager != CAN_NO_MANAGER) {
      if ((abIdFilter[i] & k) != k) {
        abIdFilter[i] |= k;             /* set identifier               */
      } /* if */
      else {
        return ID_BUSY;                 /* OUT: ID already allocated    */
      } /* else */
    } /* if */
    else {
      abIdFilter[i] &= ~k;              /* reset identifier             */
    } /* else */
  }
#else
# if ID_FILTER == 2
  {
    BYTE DATA i;                        /* index                        */
    WORD DATA k;                        /* bit pair position            */

    i = (BYTE)(wId >> 3);
    k = (WORD)(0x0003 << (((BYTE)wId & 0x07) << 1));

    if (bManager != CAN_NO_MANAGER) {
      if ((awIdFilter[i] & k) == 0) {   /* set identifier               */
        awIdFilter[i] |= (WORD)bManager << (((BYTE)wId & 0x07) << 1);
      } /* if */
      else {
        return ID_BUSY;                 /* OUT: ID already allocated    */
      } /* else */
    } /* if */
    else {
      awIdFilter[i] &= ~k;              /* reset identifier             */
    } /* else */
  }
# else
#  if ID_FILTER == 4
  {
    BYTE DATA i;                        /* index                        */
    DWORD DATA k;                       /* nibble position              */

    i = (BYTE)(wId >> 3);
    k = 0x0FUL << (((BYTE)wId & 0x07) << 2);

    if (bManager != CAN_NO_MANAGER) {
      if ((alIdFilter[i] & k) == 0) {   /* set identifier               */
        alIdFilter[i] |= (DWORD)bManager << (((BYTE)wId & 0x07) << 2);
      } /* if */
      else {
        return ID_BUSY;                 /* OUT: ID already allocated    */
      } /* else */
    } /* if */
    else {
      alIdFilter[i] &= ~k;              /* reset identifier             */
    } /* else */
  }
#  else
#   if ID_FILTER == 8
  if (bManager != CAN_NO_MANAGER) {
    if (abIdFilter[wId] == CAN_NO_MANAGER) {
      abIdFilter[wId] = bManager;       /* set identifier               */
    } /* if */
    else {
      return ID_BUSY;                   /* OUT: ID already allocated    */
    } /* else */
  } /* if */
  else {
    abIdFilter[wId] = CAN_NO_MANAGER;   /* reset identifier             */
  } /* else */
#   endif /* ID_FILTER == 8 */
#  endif /* ID_FILTER == 4 */
# endif /* ID_FILTER == 2 */
#endif /* ID_FILTER == 1 */
  return NO_ERR;                        /* OUT: no error                */
} /* gCB_ConfigureIdFilter */


#if ID_FILTER != 0
/*!
  \brief Quick identifier checking for standard frames.

  Check if a standard identifier has an assigned manager.
  \param wId any CAN identifier
  \retval TRUE - manager exist
  \retval FALSE - manager does not exist
*/
# ifdef __MWERKS__
#  ifdef __m56800__
#   pragma interrupt called
#  endif /* __m56800__ */
# endif /* __MWERKS__ */
BYTE gCB_PreCheckIdStd(WORD wId)
{
  BYTE bRet;                            /* return value                 */

# if ID_FILTER == 1
  BYTE DATA i;                          /* index                        */
  BYTE DATA k;                          /* bit position                 */

  i = (BYTE)(wId >> 3);                 /* i = wId / 8                  */
  k = (BYTE)(0x01 << ((BYTE)wId & 0x07)); /* k = 2 ^ (wId % 8)          */

  if ((abIdFilter[i] & k) != k) {       /* check identifier             */
    bRet = FALSE;                       /* no manager allocated         */
  } /* if */
  else {
    bRet = TRUE;                        /* manager allocated            */
  } /* else */
# else
#  if ID_FILTER == 2
  BYTE DATA i;                          /* index                        */
  WORD DATA k;                          /* bit pair position            */

  i = (BYTE)(wId >> 3);
  k = (WORD)(0x0003 << (((BYTE)wId & 0x07) << 1));

  if ((awIdFilter[i] & k) == 0) {       /* check identifier             */
    bRet = FALSE;                       /* no manager allocated         */
  } /* if */
  else {
    bRet = TRUE;                        /* manager allocated            */
  } /* else */
#  else
#   if ID_FILTER == 4
  BYTE DATA i;                          /* index                        */
  DWORD DATA k;                         /* nibble position              */

  i = (BYTE)(wId >> 3);
  k = 0x0FUL << (((BYTE)wId & 0x07) << 2);

  if ((alIdFilter[i] & k) == 0) {       /* check identifier             */
    bRet = FALSE;                       /* no manager allocated         */
  } /* if */
  else {
    bRet = TRUE;                        /* manager allocated            */
  } /* else */
#   else
#    if ID_FILTER == 8
  if (abIdFilter[wId] == CAN_NO_MANAGER) {
    bRet = FALSE;                       /* no manager allocated         */
  } /* if */
  else {
    bRet = TRUE;                        /* manager allocated            */
  } /* else */
#    endif /* ID_FILTER == 8 */
#   endif /* ID_FILTER == 4 */
#  endif /* ID_FILTER == 2 */
# endif /* ID_FILTER == 1 */

  return bRet;
} /* gCB_PreCheckIdStd */
#endif /* ID_FILTER == 0 */


#if ID_FILTER != 0
/*!
  \brief Quick identifier checking for extended frames.

  Check if an extended identifier has an assigned manager.
  \param lId any CAN identifier
  \retval TRUE - manager exist
  \retval FALSE - manager does not exist
*/
# ifdef __MWERKS__
#  ifdef __m56800__
#   pragma interrupt called
#  endif /* __m56800__ */
# endif /* __MWERKS__ */
BYTE gCB_PreCheckIdExt(DWORD lId)
{
  BYTE bRet;                            /* return value                 */

  bRet = TRUE;                          /* manager allocated            */
  return bRet;
} /* gCB_PreCheckIdExt */
#endif /* ID_FILTER == 0 */


/*!
  Get the assigned manager of an identifier.
  \param wId any CAN identifier
  \return assigned manager
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE sCB_GetManagerFromId(WORD wId)
{
#if ID_FILTER == 1
  return (BYTE)((abIdFilter[(BYTE)(wId >> 3)] >> ((BYTE)wId & 0x07)) & 0x01);
#else
# if ID_FILTER == 2
  return (BYTE)((awIdFilter[(BYTE)(wId >> 3)] >> (((BYTE)wId & 0x07) << 1)) & 0x03);
# else
#  if ID_FILTER == 4
  return (BYTE)((alIdFilter[(BYTE)(wId >> 3)] >> (((BYTE)wId & 0x07) << 2)) & 0x0F);
#  else
#   if ID_FILTER == 8
  return abIdFilter[wId];
#   endif /* ID_FILTER == 8 */
#  endif /* ID_FILTER == 4 */
# endif /* ID_FILTER == 2 */
#endif /* ID_FILTER == 1 */

#if ID_FILTER == 0
  return TRUE;
#endif /* ID_FILTER == 0 */
} /* sCB_GetManagerFromId */


/*!
  \brief Define receive identifier and optional handle.

  Configure one of the identifier filter table entries and the optional
  handle table entry.
  \param wIdent any CAN identifier
  \param bManager set to CAN_NO_MANAGER to reset the given identifier
         otherwise assign identifier to the manager
  \return NO_ERR if no error occured, VALUE_TOO_HIGH_ERR if identifier
          table exceeded, ID_BUSY if identifier already allocated
*/
#if QUEUED_MODE == 1
BYTE gCB_ConfigMsg(WORD wIdent, BYTE bManager)
{
  BYTE bRet;                    /* return value                         */

  /* 
   * changing the entry is allowed if:
   * entry belongs to CAN_NO_MANAGER - in fact it is not allocated
   * OR
   * reallocation takes place for the same object
   *
   * additionally the array boundaries are checked
   */
  bRet = gCB_ConfigureIdFilter(wIdent, bManager);

  return bRet;                          /* OUT: return value            */
} /* gCB_ConfigMsg */
#endif /* QUEUED_MODE == 1 */


/*!
  This function will be called from the CAN interrupt service routine,
  if a transmit interrupt occured.
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
void sCB_QueTransmitEntry(void)
{
  BYTE XDATA *pbQueueEntry;

  /* do we have something in our transmit queue ? */
  pbQueueEntry = gQue_PeekTop((VQHANDLE)&tCanTxQueDesc);
  if (pbQueueEntry) {
    if (gCan_SendMsg((CAN_MSG XDATA *)pbQueueEntry) == NO_ERR) {
      /* success */
      gQue_DetachTop((VQHANDLE)&tCanTxQueDesc);
    } /* if */
    else {
      /* next time */
      gQue_RdUnlock((VQHANDLE)&tCanTxQueDesc);
    } /* else */
  } /* if */
} /* sCB_QueTransmitEntry */


/*!
  Write a message to the TX queue.
  \param ptSendBuffer pointer to message buffer
  \return NO_ERR transmission in progress,
          BUSY_ERR no transmission cause of no more entries in queue
*/
BYTE gCB_QueSendMsg(CAN_MSG XDATA *ptSendBuffer)
{
  BYTE XDATA *pbQueueEntry;
  BYTE bRet;

  EnterRegion();
  fTxEnter = FALSE;
  bRet = NO_ERR;

  if (ptSendBuffer) { /* valid send buffer */
    if (ID_ALLOWED(ptSendBuffer)) {
      /* use transmit queue */
      pbQueueEntry = gQue_PeekFree((VQHANDLE)&tCanTxQueDesc);
      if (pbQueueEntry) {
        memcpy(pbQueueEntry, (BYTE XDATA *)ptSendBuffer, sizeof(CAN_MSG));
        gQue_AttachFree((VQHANDLE)&tCanTxQueDesc);
      } /* if */
      else {
        bRet = BUSY_ERR;
      } /* else */
    } /* if */
  } /* if */

  /* transmit next queue entry */
  sCB_QueTransmitEntry();

  LeaveRegion();
  fTxEnter = TRUE;

  if (fTxService == TRUE) { /* service second call */
    EnterRegion();
    fTxService = FALSE;
    /* transmit next queue entry */
    sCB_QueTransmitEntry();
    LeaveRegion();
  } /* if */

  return bRet;
}


/*!
  Executes all callback routines attached to the queues.
*/
void gCB_DispatchQueCallbacks(void)
{
#ifdef NO_CALLBACK_IN_INT
  BYTE XDATA dummy;

  if (gQue_IsElement((VQHANDLE)&tCanRxQueDesc)) {
    dummy = 0;
    gQue_ExecCallback((VQHANDLE)&tCanRxQueDesc, &dummy);
  } /* if */
#endif /* NO_CALLBACK_IN_INT */
} /* gCB_DispatchQueCallbacks */


/*!
  Configure a flat buffer for the reception of a message.
  \param bBNr - buffer number in flat buffer structure.
  \param dwId - Identifier to be used for this buffer.
  \param bDlc - Data length code to be used for message.
  \param bDir - #RX_DIR, #TX_DIR
*/
void gCB_BufSetId(BYTE bBNr, DWORD dwId, BYTE bDlc, BYTE bDir)
{
#if ID_FILTER != 0
  /* set identifier */
  if (bDir) {
    if (gCB_ConfigureIdFilter((WORD)dwId, bBNr) != NO_ERR) {
      if ((dwId <= ERRCTRL_ID) || (dwId > (ERRCTRL_ID + 0x7F))) {
        return;
      } /* if */
    } /* if */
  } /* if */
#endif /* ID_FILTER != 0 */
  VDEBUG_RPT3("buffer: register %lx with dlc %d in buffer %d\n",
    dwId, (WORD)bDlc, (WORD)bBNr);
  tCanMsgBuffer[bBNr].tMsg.qbId.dw = dwId;
  tCanMsgBuffer[bBNr].tMsg.Dlc     = bDlc;
}


/*!
  Change the identifier for a flat buffer location.
  \param bBNr - buffer number in flat buffer structure.
  \param dwId - New identifier.
  \retval FALSE - Identifier would overwrite filter array
  \retval TRUE - Changing accepted.
*/
BYTE gCB_BufChangeId(BYTE bBNr, DWORD dwId)
{
  BYTE bRet;                    /* return value                         */

#if ID_FILTER != 0
  /* assign new identifier */
  if (gCB_ConfigureIdFilter((WORD)dwId, bBNr) == NO_ERR) {
    /* release old identifier if different */
    if ((WORD)dwId != (WORD)tCanMsgBuffer[bBNr].tMsg.qbId.dw) {
      gCB_ConfigureIdFilter((WORD)tCanMsgBuffer[bBNr].tMsg.qbId.dw, CAN_NO_MANAGER);
    } /* if */
#endif /* ID_FILTER != 0 */

    /* set COB ID */
    tCanMsgBuffer[bBNr].tMsg.qbId.dw = dwId;
    bRet = TRUE;

#if ID_FILTER != 0
  } /* if */
  else {
    bRet = FALSE;
  } /* else */
#endif /* ID_FILTER != 0 */

  return bRet;
} /* gCB_BufChangeId */


/*!
  Enable/disable a known identifier for reception.
  \param bBNr - buffer number in flat buffer structure.
  \param fSet - ON - message will be used / OFF - message ignored.
  \retval FALSE - Identifier would overwrite filter array
  \retval TRUE - Changing accepted.
*/
BYTE gCB_BufEnable(BOOLEAN fSet, BYTE bBNr)
{
  BYTE bRet;                    /* return value                         */

  if (fSet) {                           /* enable identifier            */
    bRet = TRUE;
#if ID_FILTER != 0
    /* assign identifier */
    if (gCB_ConfigureIdFilter((WORD)tCanMsgBuffer[bBNr].tMsg.qbId.dw, bBNr) == NO_ERR) {
      bRet = TRUE;
    } /* if */
    else {
      bRet = FALSE;
    } /* else */
#endif /* ID_FILTER != 0 */
  } /* if */
  else {                                /* disable identifier           */
#if ID_FILTER != 0
    /* release identifier */
    gCB_ConfigureIdFilter((WORD)tCanMsgBuffer[bBNr].tMsg.qbId.dw, CAN_NO_MANAGER);
#endif /* ID_FILTER != 0 */
    bRet = TRUE;
  } /* else */

  return bRet;
}


/*!
  Check receive message existance.
  \return buffer number or NO_MESSAGE.
*/
BYTE gCB_TestInMsg(void)
{
#if QUEUED_MODE == 1
  BYTE DATA i;                  /* counter                              */
  BYTE DATA k;                  /* counter                              */
  CAN_MSG XDATA *ptMsg;

# if NMT_MASTER_ENABLE == 1
  /* check for self addressed NMT 0 message */
  if (tCanMsgBuffer[NMT_ZERO_RX].bSta == BUF_RECEIVED) {
    return NMT_ZERO_RX;         /* OUT: buffer number                   */
  }
# endif /* NMT_MASTER_ENABLE == 1 */

# if SYNC_PRODUCER == 1
  /* check for self addressed SYNC message */
  if (tCanMsgBuffer[SYNC_RX].bSta == BUF_RECEIVED) {
    return SYNC_RX;             /* OUT: buffer number                   */
  }
# endif /* SYNC_PRODUCER == 1 */

  if (!gQue_IsElement((VQHANDLE)&tCanRxQueDesc)) {
    return NO_MESSAGE;
  } /* if */
  else {
    ptMsg = (CAN_MSG XDATA *)gQue_PeekTop((VQHANDLE)&tCanRxQueDesc);
    for (i = 0; i < MAX_CAN_BUFFER; i++) {
      if ((tCanMsgBuffer[i].tMsg.qbId.dw & 0x1FFFFFFFUL) == ptMsg->qbId.dw) {
        if (tCanMsgBuffer[i].bSta == BUF_RECEIVED) {
          fEmcyOverrun = TRUE;
        } /* if */
        else {
          /* set status */
          tCanMsgBuffer[i].bSta = BUF_RECEIVED;  
          tCanMsgBuffer[i].tMsg.Rtr = ptMsg->Rtr;
        } /* else */
        /* check data frame */
        if (!ptMsg->Rtr) {
          /* copy DLC message data bytes */
          for (k = 0; k < (BYTE)ptMsg->Dlc; k++) {
            tCanMsgBuffer[i].tMsg.bDb[k] = ptMsg->bDb[k];
          } /* for */
          tCanMsgBuffer[i].tMsg.Dlc = (BYTE)ptMsg->Dlc;
        } /* if */
        gQue_DetachTop((VQHANDLE)&tCanRxQueDesc);
        return i;
      } /* if */
    } /* for */

    /* now evaluate for a message located in a callback area */
# if SIGNAL_EMCYMSG == 1
    if ((ptMsg->qbId.dw >= SignalEmcy[0].qbIdLowLimit.dw)
        && (ptMsg->qbId.dw <= SignalEmcy[0].qbIdHighLimit.dw)) {
      /* received message is located in EMCY area */
      BYTE bTmpId;
      bTmpId = (BYTE)(ptMsg->qbId.dw - EMCY_ID);
      if (bTmpId != ModuleInfo.bBoardAdr) {
        /*
         * If the dlc is not 0 the message is considered as
         * emcy information.
         */
        if (ptMsg->Dlc != 0) {
          EmcyEventRcv(bTmpId, (BYTE *)&ptMsg->bDb);
        } /* if */
#  if SIGNAL_BOOTUPMSG == 1
        else {
          /* otherwise it is an old fashioned boot up message */
          BootUpEventRcv(bTmpId);
        } /* else */
#  endif /* SIGNAL_BOOTUPMSG == 1 */
      } /* if */
    } /* if */
# endif /* SIGNAL_EMCYMSG == 1 */

# if SIGNAL_BOOTUPMSG == 1
    if ((ptMsg->qbId.dw >= SignalBootUp[0].qbIdLowLimit.dw)
        && (ptMsg->qbId.dw <= SignalBootUp[0].qbIdHighLimit.dw)) {
      /* received message is located in bootup message area */
      BYTE bTmpId;
      bTmpId = (BYTE)(ptMsg->qbId.dw - ERRCTRL_ID);
      /*
       * Signal boot up message if it is not our own module id and
       * if data contents are 0.
       */
      if ((bTmpId != ModuleInfo.bBoardAdr) && (ptMsg->bDb[0] == 0)) {
        BootUpEventRcv(bTmpId); 
      } /* if */
    } /* if */
# endif /* SIGNAL_BOOTUPMSG == 1 */
    gQue_DetachTop((VQHANDLE)&tCanRxQueDesc);
  } /* else */

  return NO_MESSAGE;

#else
  static BYTE DATA i = 1;       /* buffer counter                       */

  /* check for NMT 0 message first */
  if (tCanMsgBuffer[NMT_ZERO_RX].bSta == BUF_RECEIVED) {
    return NMT_ZERO_RX;         /* OUT: buffer number                   */
  }

  for (; i < MAX_CAN_BUFFER; i++) {
    /* message received ? */
    if (tCanMsgBuffer[i].bSta == BUF_RECEIVED) {
      return i;                 /* OUT: buffer number                   */
    }
  }

  i = 1;                        /* reset index                          */
  return NO_MESSAGE;            /* OUT: no message                      */
#endif /* QUEUED_MODE == 1 */
}


/*!
  If a message was received correctly, it will be put into a related
  buffer or queue.
  This function will be called from the CAN interrupt service routine.
  \param ptMsg - Pointer to message (is invalid after routine execution).
  \retval TRUE - message could be written to buffer.
  \retval FALSE - message not buffered.
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE gCB_CanBufferMsg(CAN_MSG XDATA *ptMsg)  
{
#if QUEUED_MODE == 1
  BYTE XDATA *pbQueueEntry;

  /* NMT COB ID 0 command ? */
  if (ptMsg->qbId.dw == 0) {
    /* check address */
    if ((ptMsg->bDb[1] == 0) || (ptMsg->bDb[1] == ModuleInfo.bBoardAdr)) {
      /* put message to storage location */
      pbQueueEntry = gQue_PeekFree((VQHANDLE)&tCanRxQueDesc);
      if (pbQueueEntry) {
        memcpy(pbQueueEntry, (BYTE XDATA *)ptMsg, sizeof(CAN_MSG));
        gQue_AttachFree((VQHANDLE)&tCanRxQueDesc);
      } /* if */
      else {
        return FALSE;
      } /* else */
    } /* if */
  } /* if */
  else {
    /* put message to storage location */
    pbQueueEntry = gQue_PeekFree((VQHANDLE)&tCanRxQueDesc);
    if (pbQueueEntry) {
      memcpy(pbQueueEntry, (BYTE XDATA *)ptMsg, sizeof(CAN_MSG));
      gQue_AttachFree((VQHANDLE)&tCanRxQueDesc);
    } /* if */
    else {
      return FALSE;
    } /* else */
  } /* else */
  return TRUE;
#else

  BYTE i;                       /* counter                              */
  BYTE bBNr;                    /* buffer counter                       */

# if ID_FILTER != 0
  /* get buffer number */
  bBNr = sCB_GetManagerFromId((WORD)ptMsg->qbId.dw);
# else
  BOOLEAN fBFound = FALSE;      /* buffer found flag                    */

  /* search assigned buffer */
  for (bBNr = 0; bBNr < NUM_CAN_BUFFER; bBNr++) {
    /* compare ID */
    if (ptMsg->qbId.dw == tCanMsgBuffer[bBNr].tMsg.qbId.dw) {
      fBFound = TRUE;                   /* known identifier             */
      break;
    } /* if */
  } /* for */

  if (fBFound == FALSE) {               /* unknown identifier           */
    return FALSE;
  } /* if */
# endif /* ID_FILTER != 0 */

  if (!ptMsg->Rtr) {                /* remote frame ?                   */
    if (bBNr <= MAX_RX_MSG) {       /* receive buffer ?                 */
      if (bBNr == NMT_ZERO_RX) {    /* NMT COB ID 0 command ?           */
        /* check address */
        if ((ptMsg->bDb[1] == 0)
            || (ptMsg->bDb[1] == ModuleInfo.bBoardAdr)) {
          /* check overrun */
          if (tCanMsgBuffer[NMT_ZERO_RX].bSta == BUF_EMPTY) {
            /* check DLC */
            if (ptMsg->Dlc == 2) {
              /* copy 2 message data bytes */
              tCanMsgBuffer[NMT_ZERO_RX].tMsg.bDb[0] = ptMsg->bDb[0];
              tCanMsgBuffer[NMT_ZERO_RX].tMsg.bDb[1] = ptMsg->bDb[1];
              /* set status */
              tCanMsgBuffer[NMT_ZERO_RX].bSta = BUF_RECEIVED;
              tCanMsgBuffer[NMT_ZERO_RX].tMsg.RxOk = 1;
            }
          }
          else {                    /* set EMCY overrun flag            */
            fEmcyOverrun = TRUE;
          } /* else */
        }
      }
      else if (tCanMsgBuffer[bBNr].bSta == BUF_EMPTY) { /* any message  */
        /* copy DLC message data bytes */
        for (i = 0; i < ptMsg->Dlc; i++) {
          tCanMsgBuffer[bBNr].tMsg.bDb[i] = ptMsg->bDb[i];
        }
        /* set status */
        tCanMsgBuffer[bBNr].bSta = BUF_RECEIVED;
        tCanMsgBuffer[bBNr].tMsg.RxOk = 1;
        tCanMsgBuffer[bBNr].tMsg.Dlc = ptMsg->Dlc;
      }
      else {                        /* set EMCY overrun flag            */
        fEmcyOverrun = TRUE;
      } /* else */
    }
    else {                          /* set EMCY ID collision flag       */
      fEmcyIdCollision = TRUE;
    } /* else */
  }
  else {                            /* handle remote request            */
    if (bBNr > MAX_RX_MSG) {        /* transmit buffer ?                */
      /* set status */
      tCanMsgBuffer[bBNr].bSta = BUF_RECEIVED;
      tCanMsgBuffer[bBNr].tMsg.Rtr = ptMsg->Rtr;
    }
  }
  return TRUE;
#endif /* QUEUED_MODE == 1 */
}


/*!
  This function will be called from the CAN interrupt service routine,
  if a transmit interrupt occured.
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
void gCB_SignalTx(void)
{
#if QUEUED_MODE == 1
  if (fTxEnter != TRUE) { /* second call */
    fTxService = TRUE;
    return;
  } /* if */
  
  /* transmit next queue entry */
  sCB_QueTransmitEntry();
#endif /* QUEUED_MODE == 1 */
} /* gCB_SignalTx */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Basic routines to filter and store CAN messages.
  \par File name buffer.c
  \version 38
  \date 11.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

