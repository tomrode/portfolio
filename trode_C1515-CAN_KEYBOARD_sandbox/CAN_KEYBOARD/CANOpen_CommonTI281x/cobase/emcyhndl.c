/*--------------------------------------------------------------------
       EMCYHNDL.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: EMCY handling functions
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "portab.h"
#include "cos_main.h"
#include "vassert.h"
#include "vdbgprnt.h"
#include <string.h>
#include "cancntrl.h"
#include "emcyhndl.h"
#include "buffer.h"


/*--------------------------------------------------------------------*/
/*  external references                                               */
/*--------------------------------------------------------------------*/
extern vModInfo XDATA ModuleInfo; /* module information */
extern FAR CAN_BUF XDATA tCanMsgBuffer[MAX_CAN_BUFFER];/* CAN msg buffer */
extern BOOLEAN DATA fTxEmcyRequest; /* EMCY transmit indicator */


/*--------------------------------------------------------------------*/
/*  global variables                                                  */
/*--------------------------------------------------------------------*/
vEmcyManage XDATA EmcyTable; /*!< EMCY information */

/*!
  \brief Reset the pre-defined error field

  All error information is removed. This function is called upon
  initialization. Furthermore it is called if a '0' is written
  to object 1003 subindex 0.
*/
void gResetPredefErrors(void)
{
#if ENABLE_EMCYMSG == 1
  BYTE DATA i;
# if EMCY_MAN_SPEC == 1
  BYTE DATA k;
# endif /* EMCY_MAN_SPEC == 1 */
#endif /* ENABLE_EMCYMSG == 1 */

  EmcyTable.bErrReg = 0;
#if ENABLE_EMCYMSG == 1
  EmcyTable.bStatus = EMCY_NO_ERR;
  EmcyTable.bNumber = 0;
  for (i = 0; i < ERR_FIELDS; i++) {
    EmcyTable.mErrInfo[i].wbErrField.wc = ECY_NO_ERROR;
    EmcyTable.mErrInfo[i].bSignaled     = EMCY_MSG_EMPTY;
# if EMCY_MAN_SPEC == 1
    for (k = 0; k < 5; k++) {
      EmcyTable.mErrInfo[i].bManSpec[k] = 0;
    } /* for */
# endif /* EMCY_MAN_SPEC == 1 */
  } /* for */
#endif /* ENABLE_EMCYMSG == 1 */
} /* gResetPredefErrors */


/*!
  Calculate now the new value for the error register.

  \param wEmcyCode - emergency error code
  \param bState - ON = error occurred/OFF = error disappeared
*/
#if ENABLE_EMCYMSG == 1
void sSetErrorRegister(void)
#else
void sSetErrorRegister(WORD wEmcyCode, BYTE bState)
#endif /* ENABLE_EMCYMSG == 1 */
{
#if ENABLE_EMCYMSG == 1
  BYTE DATA i;

  EmcyTable.bErrReg = 0;
  i = 0;
  while (i < EmcyTable.bNumber) {
    if ((EmcyTable.mErrInfo[i].bSignaled == EMCY_MSG_NEW)
      || (EmcyTable.mErrInfo[i].bSignaled == EMCY_MSG_NEW_ADEL)
      || (EmcyTable.mErrInfo[i].bSignaled == EMCY_MSG_ACTIVE)) {
      switch (EmcyTable.mErrInfo[i].wbErrField.wc & 0xF800) {
        case 0x2000: /* current */
          EmcyTable.bErrReg |= EREG_CURRENT;
          break;
        case 0x3000: /* voltage */
          EmcyTable.bErrReg |= EREG_VOLTAGE;
          break;
        case 0x4000: /* temperature */
          EmcyTable.bErrReg |= EREG_TEMP;
          break;
        case 0x1000: /* generic error */
        case 0x5000: 
        case 0x6000: 
        case 0x7000: 
        case 0x9000: 
        case 0xF000: 
          EmcyTable.bErrReg |= EREG_GENERIC;
          break;
        case 0x8000: /* communication error */
          EmcyTable.bErrReg |= EREG_COMM;
          break;
        case 0xF800: /* device profile specific */
          EmcyTable.bErrReg |= EREG_DEV_PROF;
          break;
        default:     /* manufacturer specific */
          EmcyTable.bErrReg |= EREG_MANUFACT;
          break;
      } /* switch */
    } /* if */
    i++;
  } /* while */

  if (EmcyTable.bErrReg) {
    EmcyTable.bErrReg |= EREG_GENERIC;
    EmcyTable.bStatus = EMCY_ERR_OCC;
  } /* if */
  else {
    EmcyTable.bStatus = EMCY_NO_ERR;
  } /* else */

#else

  if (bState == ON) {
    /* calculating the error register */
    switch (wEmcyCode & 0xF800) {
      case 0x2000: /* current */
        EmcyTable.bErrReg |= EREG_CURRENT;
        break;
      case 0x3000: /* voltage */
        EmcyTable.bErrReg |= EREG_VOLTAGE;
        break;
      case 0x4000: /* temperature */
        EmcyTable.bErrReg |= EREG_TEMP;
        break;
      case 0x1000: /* generic error */
      case 0x5000: 
      case 0x6000: 
      case 0x7000: 
      case 0x9000: 
      case 0xF000: 
        EmcyTable.bErrReg |= EREG_GENERIC;
        break;
      case 0x8000: /* communication error */
        EmcyTable.bErrReg |= EREG_COMM;
        break;
      case 0xF800: /* device profile specific */
        EmcyTable.bErrReg |= EREG_DEV_PROF;
        break;
      default:     /* manufacturer specific */
        EmcyTable.bErrReg |= EREG_MANUFACT;
        break;
    } /* switch */
  } /* if */  
  else {
    /* clear generic bit */
    EmcyTable.bErrReg &= ~EREG_GENERIC;
    /* calculating the error register */
    switch (wEmcyCode & 0xF800) {
      case 0x2000: /* current */
        EmcyTable.bErrReg &= ~EREG_CURRENT;
        break;
      case 0x3000: /* voltage */
        EmcyTable.bErrReg &= ~EREG_VOLTAGE;
        break;
      case 0x4000: /* temperature */
        EmcyTable.bErrReg &= ~EREG_TEMP;
        break;
      case 0x1000: /* generic error */
      case 0x5000: 
      case 0x6000: 
      case 0x7000: 
      case 0x9000: 
      case 0xF000: 
        EmcyTable.bErrReg &= ~EREG_GENERIC;
        break;
      case 0x8000: /* communication error */
        EmcyTable.bErrReg &= ~EREG_COMM;
        break;
      case 0xF800: /* device profile specific */
        EmcyTable.bErrReg &= ~EREG_DEV_PROF;
        break;
      default:     /* manufacturer specific */
        EmcyTable.bErrReg &= ~EREG_MANUFACT;
        break;
    } /* switch */
  } /* else */

  /* set generic bit if any error */
  if (EmcyTable.bErrReg) {
    EmcyTable.bErrReg |= EREG_GENERIC;
  } /* if */
#endif /* ENABLE_EMCYMSG == 1 */
}


/*!
  \brief Signal an EMCY relevant event

  The creation of stack internal EMCY events is triggered
  from this function. The message is not transmitted immediately.
  The function performs the following steps
  - loop through all errors and check for already existing code
  - if error shift already available entries and add at position 0
  - if entry to be removed mark only for deletion

  \param wEmcyCode - emergency error code
  \param bState - ON = error occurred/OFF = error disappeared
  \param pbManSpec - manufacturer specific error field

  \retval TRUE - code added/removed successfully
  \retval FALSE - code not added
*/
#if EMCY_MAN_SPEC == 1
BYTE gSetEmcy(WORD wEmcyCode, BYTE bState, BYTE * pbManSpec)
#else
BYTE gSetEmcy(WORD wEmcyCode, BYTE bState)
#endif /* EMCY_MAN_SPEC == 1 */
{
#if ENABLE_EMCYMSG == 1
  BYTE i;
  BYTE bFound;
# if EMCY_MAN_SPEC == 1
  BYTE j;
# endif /* EMCY_MAN_SPEC == 1 */

  i = 0;
  bFound = FALSE;

  if ((ModuleInfo.bModuleState == STACK_ENABLED)
      && (ModuleInfo.bCommState != STOPPED)) {

    assert(EmcyTable.bNumber <= ERR_FIELDS);

    while (i < EmcyTable.bNumber && !bFound) {
      if (EmcyTable.mErrInfo[i].wbErrField.wc == wEmcyCode) {
        bFound = TRUE; /* leave loop */
      } /* if */
      else {
        i++;
      } /* else */
    } /* while */

    if (bState == ON) {
      if (!bFound) {
        /* add new EMCY event */
        if (EmcyTable.bNumber < ERR_FIELDS) {
          /* move all other errors to next position */
# if ERR_FIELDS > 1
          BYTE k;
          for (k = EmcyTable.bNumber; k > 0; k--) {
            EmcyTable.mErrInfo[k].bSignaled
              = EmcyTable.mErrInfo[k - 1].bSignaled;
            EmcyTable.mErrInfo[k].wbErrField.wc
              = EmcyTable.mErrInfo[k - 1].wbErrField.wc;
#  if EMCY_MAN_SPEC == 1
            for (j = 0; j < 5; j++) {
              EmcyTable.mErrInfo[k].bManSpec[j]
                = EmcyTable.mErrInfo[k - 1].bManSpec[j];
            } /* for */
#  endif /* EMCY_MAN_SPEC == 1 */
          } /* for */
# endif /* ERR_FIELDS > 1 */
          /* new entries are always entered at position 0 */
          EmcyTable.mErrInfo[0].bSignaled = EMCY_MSG_NEW;
          EmcyTable.mErrInfo[0].wbErrField.wc = wEmcyCode;
# if EMCY_MAN_SPEC == 1
          if (pbManSpec != (BYTE *)0) {
            for (j = 0; j < 5; j++) {
              EmcyTable.mErrInfo[0].bManSpec[j] = pbManSpec[j];
            } /* for */
          } /* if */
          else {
            for (j = 0; j < 5; j++) {
              EmcyTable.mErrInfo[0].bManSpec[j] = 0;
            } /* for */
          } /* else */
# endif /* EMCY_MAN_SPEC == 1 */
          EmcyTable.bNumber++;
          VDEBUG_RPT1("EMCY code added %x\n", wEmcyCode);
        } /* if */
        else {
          VDEBUG_RPT1("no space to add EMCY code %x\n", wEmcyCode);
          return FALSE;
        } /* else */
      } /* if */
      else {
        return FALSE; /* error code already entered in table */
      } /* else */
    } /* if */  
    else {
      if (bFound == TRUE) {
        if (EmcyTable.mErrInfo[i].bSignaled == EMCY_MSG_ACTIVE) {
          /* mark for deletion */
          EmcyTable.mErrInfo[i].bSignaled = EMCY_MSG_DEL;
# if EMCY_MAN_SPEC == 1
          for (j = 0; j < 5; j++) {
            EmcyTable.mErrInfo[i].bManSpec[j] = 0;
          } /* for */
# endif /* EMCY_MAN_SPEC == 1 */
        } /* if */
        else {
          /* enable auto deletion */
          EmcyTable.mErrInfo[i].bSignaled = EMCY_MSG_NEW_ADEL;
        } /* else */
      } /* if */
      else {
        VDEBUG_RPT1("failed removing EMCY code %x\n", wEmcyCode);
        return FALSE;
      } /* else */
    } /* else */

    /* At this point our error field is actual. */
    /* We calculate now the new value for the error register. */
    sSetErrorRegister();
    return TRUE;
  } /* if */
  else {
    return FALSE;
  } /* else */
#else
  /* We calculate now the new value for the error register. */
  sSetErrorRegister(wEmcyCode, bState);
  return FALSE;
#endif /* ENABLE_EMCYMSG == 1 */
} /* gSetEmcy */


#if ENABLE_EMCYMSG == 1
/*!
  \brief Check for a new EMCY message to be sent.

  The error field is checked for changings this means that
  the entry in the error field must have the attribute #EMCY_MSG_NEW or
  #EMCY_MSG_DEL.
  A successful transmission for a #EMCY_MSG_NEW entry leads to the state
  #EMCY_MSG_ACTIVE for this event. For a #EMCY_MSG_DEL entry the
  transmission causes the deletion of the element.
*/
void gCheckEmcyTransmit(void)
{
  BYTE DATA i;

  /* check buffer access */
  if ((tCanMsgBuffer[EMCY_TX].bSta != BUF_COMPLETE)
# if QUEUED_MODE == 0
      && (tCanMsgBuffer[EMCY_TX].bSta != BUF_TRANSMIT)
# endif /* QUEUED_MODE == 0 */
      && (ModuleInfo.bModuleState == STACK_ENABLED)
      && (ModuleInfo.bCommState != STOPPED)) {

    for (i = 0; i < EmcyTable.bNumber; i++) {
      switch (EmcyTable.mErrInfo[i].bSignaled) {
        case EMCY_MSG_DEL:
          EmcyTable.mErrInfo[i].wbErrField.wc = ECY_NO_ERROR;
          /* At this point our error field has changed. */
          /* We calculate now the new value for the error register. */
          sSetErrorRegister();
          /*lint -fallthrough */
        case EMCY_MSG_NEW:
        case EMCY_MSG_NEW_ADEL:
          MemSet(&MESSAGE_DB(EMCY_TX, 0), 0x00, 8);
          MESSAGE_DB(EMCY_TX, 0) = EmcyTable.mErrInfo[i].wbErrField.bc.lb;
          MESSAGE_DB(EMCY_TX, 1) = EmcyTable.mErrInfo[i].wbErrField.bc.mb;
          MESSAGE_DB(EMCY_TX, 2) = EmcyTable.bErrReg;
# if EMCY_MAN_SPEC == 1
          MESSAGE_DB(EMCY_TX, 3) = EmcyTable.mErrInfo[i].bManSpec[0];
          MESSAGE_DB(EMCY_TX, 4) = EmcyTable.mErrInfo[i].bManSpec[1];
          MESSAGE_DB(EMCY_TX, 5) = EmcyTable.mErrInfo[i].bManSpec[2];
          MESSAGE_DB(EMCY_TX, 6) = EmcyTable.mErrInfo[i].bManSpec[3];
          MESSAGE_DB(EMCY_TX, 7) = EmcyTable.mErrInfo[i].bManSpec[4];
# endif /* EMCY_MAN_SPEC == 1 */
          tCanMsgBuffer[EMCY_TX].bSta = BUF_COMPLETE;
          if (gCan_SendBuf(EMCY_TX) == BUSY_ERR) {
            /* controller busy, try again later */
            fTxEmcyRequest = TRUE;
          } /* if */
          else { /* emergency message transmitted */
# if QUEUED_MODE == 1
            tCanMsgBuffer[EMCY_TX].bSta = BUF_SENT;
            fTxEmcyRequest = FALSE;
# endif /* QUEUED_MODE == 1 */
            switch (EmcyTable.mErrInfo[i].bSignaled) {
              case EMCY_MSG_NEW:
                EmcyTable.mErrInfo[i].bSignaled = EMCY_MSG_ACTIVE;
                break;

              case EMCY_MSG_NEW_ADEL:
                /* mark for deletion */
                EmcyTable.mErrInfo[i].bSignaled = EMCY_MSG_DEL;
# if EMCY_MAN_SPEC == 1
                for (j = 0; j < 5; j++) {
                  EmcyTable.mErrInfo[i].bManSpec[j] = 0;
                } /* for */
# endif /* EMCY_MAN_SPEC == 1 */
                break;

              case EMCY_MSG_DEL: {
# if ERR_FIELDS > 1
                BYTE k;
                for (k = 0; k < (EmcyTable.bNumber - i); k++) {
                  EmcyTable.mErrInfo[k + i].bSignaled
                    = EmcyTable.mErrInfo[k + i + 1].bSignaled;
                  EmcyTable.mErrInfo[k + i].wbErrField.wc
                    = EmcyTable.mErrInfo[k + i + 1].wbErrField.wc;
#  if EMCY_MAN_SPEC == 1
                  EmcyTable.mErrInfo[k + i].bManSpec[0]
                    = EmcyTable.mErrInfo[k + i + 1].bManSpec[0];
                  EmcyTable.mErrInfo[k + i].bManSpec[1]
                    = EmcyTable.mErrInfo[k + i + 1].bManSpec[1];
                  EmcyTable.mErrInfo[k + i].bManSpec[2]
                    = EmcyTable.mErrInfo[k + i + 1].bManSpec[2];
                  EmcyTable.mErrInfo[k + i].bManSpec[3]
                    = EmcyTable.mErrInfo[k + i + 1].bManSpec[3];
                  EmcyTable.mErrInfo[k + i].bManSpec[4]
                    = EmcyTable.mErrInfo[k + i + 1].bManSpec[4];
#  endif /* EMCY_MAN_SPEC == 1 */
                } /* for */
# endif /* ERR_FIELDS > 1 */
                EmcyTable.bNumber--;
                VDEBUG_RPT0("EMCY code removed\n");
                }
                break;

              default:
                break;
            } /* switch */
          } /* else */
# if QUEUED_MODE == 0
          return;
# endif /* QUEUED_MODE == 0 */
//          break;

        default:
          break;
      } /* switch */
    } /* for */
  } /* if */
} /* gCheckEmcyTransmit */
#endif /* ENABLE_EMCYMSG == 1 */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Emergency message handling routines.
  \par File name emcyhndl.c
  \version 12
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

