/*--------------------------------------------------------------------
       OBJDICT.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Object dictionary implementation
  --------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "portab.h"
#include "cos_main.h"
#include "objdict.h"
#include "cancntrl.h"
#include "profil.h"
#include "emcyhndl.h"

#include "sdo_map_interface.h"



/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/
#if RPDO_PAR_READONLY == 1
# define RPDO_PAR_ACC RO /*!< Receive PDO parameters is read only. */
#else
# define RPDO_PAR_ACC RW /*!< Receive PDO parameters is writeable. */
#endif /* RPDO_PAR_READONLY == 1 */

#if RPDO_MAP_READONLY == 1
# define RPDO_MAP_ACC RO /*!< Receive PDO mapping is read only. */
#else
# define RPDO_MAP_ACC RW /*!< Receive PDO mapping is writeable. */
#endif /* RPDO_MAP_READONLY == 1 */

#if TPDO_PAR_READONLY == 1
# define TPDO_PAR_ACC RO /*!< Transmit PDO parameters are read only. */
#else
# define TPDO_PAR_ACC RW /*!< Transmit PDO parameters are writeable. */
#endif /* TPDO_PAR_READONLY == 1 */

#if TPDO_MAP_READONLY == 1
# define TPDO_MAP_ACC RO /*!< Transmit PDO mapping is read only. */
#else
# define TPDO_MAP_ACC RW /*!< Transmit PDO mapping is writeable. */
#endif /* TPDO_MAP_READONLY == 1 */



/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern VComParaArea XDATA gStkGlobDat; /*!< Storage block for non-volatile data. */
extern vEmcyManage XDATA EmcyTable; /* EMCY information */



/*--------------------------------------------------------------------*/
/*  variable definitions                                              */
/*--------------------------------------------------------------------*/

#if USE_DUMMY_MAPPING == 1
STATIC DWORD dwDummyArea; /*!< All write accesses to a dummy variable go to here */
#endif /* USE_DUMMY_MAPPING == 1 */


/*!
  \brief constant items from object dirctionary.

  Some of our entries in the object dictionary are
  constant by nature.
  Due to the access mechanism we need an address where the
  constant data reside.
 */
CONST VCodeArea CODE gConstData = {
  /* device type */
  { DEVICE_TYPE_VAL },
#if STORE_PARAMETERS_SUPP == 1
  /* count only array bodies for 1010 and 1011 separately */
  { (STORE_OBJECTS-2)/2 },
#endif /* STORE_PARAMETERS_SUPP == 1 */
  /* entries in heartbeat list */
  { NUM_OF_HB_CONSUMERS },
#if OBJECT_1029_USED == 1
  /* entries for object 1029 */
  { NUM_ERROR_CLASSES },
#endif /* OBJECT_1029_USED == 1 */
  /* vendor id */
  { VENDOR_ID_VAL },
  /* product code */
  { PRODUCT_CODE_VAL },
  /* revision number */
  { REVISION_NUM_VAL },
  /* serial number */
  { SERIAL_NUM_VAL }
};

/*! \def CONST_ADR(adr)

    \brief Set object memory address for constant values.

    This define adds special handling for systems that do not support 'universal'
    pointers i.e. pointers that reference either RAM or ROM memory locations.
    For those systems the memory address for constant read-only objects is set
    to '0' but therefore requiring the data explicitly being set in the function
    gSdoS_readData().
    To enable this special handling the define #NO_UNIVERSAL_POINTERS has to be
    set in <portab.h>.

    \param adr - object's memory address
 */
#ifndef NO_UNIVERSAL_POINTERS
# define CONST_ADR(adr) adr
#else
# define CONST_ADR(adr) 0
#endif /* NO_UNIVERSAL_POINTERS */

/*! \def SINGLE_OBJ(index, length, address, access, attrib)

    \brief Add a single object to the object dictionary.

    For this object the subindex is set to 0 and the array type
    is #NO_ARRAY.

    \param index - index
    \param length - length of object in bytes
    \param address - object's memory address
    \param access - \ref odaccessattr
    \param attrib - \ref callbackattr
 */
#define SINGLE_OBJ(index, length, address, access, attrib) \
  { address, {index}, 0, length, (attrib | NO_ARRAY | access) }


/*! \def ARRAY_HEAD(index, length, address, access, attrib)

    \brief Add the first member of an array to the object dictionary.

    For this object the subindex is set to 0 and the array type
    is #NO_ARRAY. This statement must be followed by a ARRAY_BODY()
    statement

    \param index - Index.
    \param length - Length of object at sub-index 0 in bytes.
    \param address - Storage address of 'number of entries' location.
    \param access - \ref odaccessattr
    \param attrib - \ref callbackattr
 */
#define ARRAY_HEAD(index, length, address, access, attrib) \
  { address, {index}, 0, length, (attrib | NO_ARRAY | access) }


/*! \def ARRAY_BODY(index, subindex, length, address, access, attrib)

    \brief Add the body of an array to the object dictionary.

    It is allowed to use this statement without a previous
    ARRAY_HEAD(). For this object array type is #IS_ARRAY.

    \param index - Index.
    \param subindex - Highest subindex for this array.
    \param length - Length of object in bytes.
    \param address - Start address of our array entry. The components of the
    array are addressed via this start address, the length of one object and
    the sub-index.
    \param access - \ref odaccessattr
    \param attrib - \ref callbackattr
 */
#define ARRAY_BODY(index, subindex, length, address, access, attrib) \
  { address, {index}, subindex, length, (attrib | IS_ARRAY | access) }


/*! \def STRUCT_HEAD(index, address, length, access, attrib)

    \brief Add the first member of a struct to the object dictionary.

    For this object the subindex is set to 0 and the object type
    is #NO_ARRAY.
    This statement must be followed by a STRUCT_BODY() statement

    \param index - Index.
    \param address - Storage address of 'number of entries' location.
    \param length - Length of object at sub-index 0 in bytes.
    \param access - \ref odaccessattr
    \param attrib - \ref callbackattr
 */
#define STRUCT_HEAD(index, length, address, access, attrib) \
  { address, {index}, 0, length, (attrib | NO_ARRAY | access) }

/*! \def STRUCT_BODY(index, subindex, length, address, access, attrib)

    \brief Add the body of a struct to the object dictionary.

    For this object the type is #NO_ARRAY.

    \param index - Index.
    \param subindex - Sub-index for structure component.
    \param length - Length of object in bytes.
    \param address - Start address of our structure component.
    \param access - \ref odaccessattr
    \param attrib - \ref callbackattr
 */
#define STRUCT_BODY(index, subindex, length, address, access, attrib) \
  { address, {index}, subindex, length, (attrib | NO_ARRAY | access) }

/*!
    \brief Create the parameter entries for a receive PDO.

    This entry consists of max. three parts.
    - sub-index 0
    - COB id
    - transmission type

    \param n - number of PDO
 */
# define RPDO_PARA(n) \
   { CONST_ADR((BYTE FCONST *) "\x02"), {0x1400+n}, 0, 1, (NO_ARRAY | RO) }, \
   { 0, {0x1400+n}, 1, 4, (NO_ARRAY | RPDO_PAR_ACC) }, \
   { 0, {0x1400+n}, 2, 1, (NO_ARRAY | RPDO_PAR_ACC) }

/*!
    \brief Create the parameter entries for a transmit PDO.

    This entry consists of max. five parts.
    - sub-index 0
    - COB id
    - transmission type
    - inhibit time
    - event timer

    \param n - number of PDO
 */
# define TPDO_PARA(n) \
   { CONST_ADR((BYTE FCONST *)"\x05"), {0x1800+n}, 0, 1, (NO_ARRAY | RO) }, \
   { 0, {0x1800+n}, 1, 4, (NO_ARRAY | TPDO_PAR_ACC) }, \
   { 0, {0x1800+n}, 2, 1, (NO_ARRAY | TPDO_PAR_ACC) }, \
   { 0, {0x1800+n}, 3, 2, (NO_ARRAY | TPDO_PAR_ACC) }, \
   { 0, {0x1800+n}, 5, 2, (NO_ARRAY | TPDO_PAR_ACC) }

/*! \def RPDO_MAP(n)
    \brief Create the mapping entries for a receive PDO.
    This statement consists of an ARRAY_HEAD() and ARRAY_BODY() statement.
    \param n - number of PDO
 */
#define RPDO_MAP(n) \
  { 0, {0x1600+n}, 0, 1, (NO_ARRAY | RPDO_MAP_ACC) },  \
  { 0, {0x1600+n}, 8, 4, (IS_ARRAY | RPDO_MAP_ACC) }


/*! \def TPDO_MAP(n)
    \brief Create the mapping entries for a transmit PDO.
    This statement consists of an ARRAY_HEAD() and ARRAY_BODY() statement.
    \param n - number of PDO
 */
#define TPDO_MAP(n) \
  { 0, {0x1A00+n}, 0, 1, (NO_ARRAY | TPDO_MAP_ACC) },  \
  { 0, {0x1A00+n}, 8, 4, (IS_ARRAY | TPDO_MAP_ACC) }


/* include user specific variables for object dictionary */
#include "objvars.h"

/*!
  \brief Object dictionary implementation

  This is our globally known object dictionary
  implementation. The dictionary is placed in const memory -
  this is ROM - in an embedded environment.
  The creator of the object dictionary adds new entries to
  this table by using the macros:
  - #SINGLE_OBJ
  - #ARRAY_HEAD
  - #ARRAY_BODY
  - #STRUCT_HEAD
  - #STRUCT_BODY
  - #RPDO_PARA
  - #RPDO_MAP
  - #TPDO_PARA
  - #TPDO_MAP
*/
CONST OD_ENTRY CODE gObjDict[DICT_ENTRIES + NUM_USER_OBJECTS] = {


#if (USE_DUMMY_MAPPING == 1) && (MAX_RX_PDOS > 0)
  SINGLE_OBJ(OBJECT_UNSIGNED8,  1, ((BYTE *)&dwDummyArea), WOMAP, SDO_EXEC_CB_NOT),/* dummy  8 bit */
  SINGLE_OBJ(OBJECT_UNSIGNED16, 2, ((BYTE *)&dwDummyArea), WOMAP, SDO_EXEC_CB_NOT),/* dummy 16 bit */
  SINGLE_OBJ(OBJECT_UNSIGNED32, 4, ((BYTE *)&dwDummyArea), WOMAP, SDO_EXEC_CB_NOT),/* dummy 32 bit */
  SINGLE_OBJ(OBJECT_UNSIGNED24, 3, ((BYTE *)&dwDummyArea), WOMAP, SDO_EXEC_CB_NOT),/* dummy 24 bit */
#endif /* (USE_DUMMY_MAPPING == 1) && (MAX_RX_PDOS > 0) */

  SINGLE_OBJ(DEVICE_TYPE, 4, CONST_ADR((BYTE FCONST *)gConstData.mDeviceType), RO, SDO_EXEC_CB_NOT),
  SINGLE_OBJ(ERROR_REG,   1, ((BYTE *)&EmcyTable.bErrReg), ROMAP, SDO_EXEC_CB_NOT),

#if ENABLE_EMCYMSG == 1
  ARRAY_HEAD(ERROR_FIELD, 1, 0, RW, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ERROR_FIELD, ERR_FIELDS, 4, 0, RO, SDO_EXEC_CB_NOT),
#endif /* ENABLE_EMCYMSG == 1 */

#if SYNC_USED == 1
  SINGLE_OBJ(COB_ID_SYNC,         4, 0, RW, SDO_EXEC_CB_NOT),
  SINGLE_OBJ(SYNC_CYCLE_PERIOD,   4, 0, RW, SDO_EXEC_CB_NOT),
  SINGLE_OBJ(SYNC_WINDOW_LENGTH,  4, ((BYTE *)&gStkGlobDat.lSyncWinLength.dw),
                                        RW, SDO_EXEC_CB_NOT),
#endif /* SYNC_USED == 1 */

  MAP_DEVICE_NAME
  MAP_HW_VER
  MAP_SW_VERSION

#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
  SINGLE_OBJ(GUARD_TIME,       2, ((BYTE *)&gStkGlobDat.mGuardTime),
                                                          RW, SDO_EXEC_CB_NOT),
  SINGLE_OBJ(LIFE_TIME_FACTOR, 1, ((BYTE *)&gStkGlobDat.mLifeTimeFactor),
                                                          RW, SDO_EXEC_CB_NOT),
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */

#if STORE_PARAMETERS_SUPP == 1
  STRUCT_HEAD(STORE_PARAMETERS,    1, CONST_ADR((BYTE FCONST *)gConstData.mStorageNr),
                                         RO, SDO_EXEC_CB_NOT),
  STRUCT_BODY(STORE_PARAMETERS, 1, 4, 0, RW, SDO_EXEC_CB_NOT),
# if NV_SAVE_COMPAR == 1
  STRUCT_BODY(STORE_PARAMETERS, 2, 4, 0, RW, SDO_EXEC_CB_NOT),
# endif /* NV_SAVE_COMPAR == 1 */
# if NV_SAVE_APPPAR == 1
  STRUCT_BODY(STORE_PARAMETERS, 3, 4, 0, RW, SDO_EXEC_CB_NOT),
# endif /* NV_SAVE_APPPAR == 1 */
# if NV_SAVE_MANPAR == 1
  STRUCT_BODY(STORE_PARAMETERS, 4, 4, 0, RW, SDO_EXEC_CB_NOT),
# endif /* NV_SAVE_MANPAR == 1 */

  STRUCT_HEAD(RESTORE_PARAMETERS,     1, CONST_ADR((BYTE FCONST *)gConstData.mStorageNr),
                                            RO, SDO_EXEC_CB_NOT),
  STRUCT_BODY(RESTORE_PARAMETERS,  1, 4, 0, RW, SDO_EXEC_CB_NOT),
# if NV_SAVE_COMPAR == 1
  STRUCT_BODY(RESTORE_PARAMETERS,  2, 4, 0, RW, SDO_EXEC_CB_NOT),
# endif /* NV_SAVE_COMPAR == 1 */
# if NV_SAVE_APPPAR == 1
  STRUCT_BODY(RESTORE_PARAMETERS,  3, 4, 0, RW, SDO_EXEC_CB_NOT),
# endif /* NV_SAVE_APPPAR == 1 */
# if NV_SAVE_MANPAR == 1
  STRUCT_BODY(RESTORE_PARAMETERS,  4, 4, 0, RW, SDO_EXEC_CB_NOT),
# endif /* NV_SAVE_MANPAR == 1 */

#endif /* STORE_PARAMETERS_SUPP == 1 */

#if TIME_STAMP_USED == 1
  SINGLE_OBJ(COB_ID_TIME_STAMP, 4, 0, RW, SDO_EXEC_CB_NOT),
  SINGLE_OBJ(TIME_STAMP, 4, 0, RWWMAP, SDO_EXEC_CB_NOT),
#endif /* TIME_STAMP_USED == 1 */

#if ENABLE_EMCYMSG == 1
  SINGLE_OBJ(COB_ID_EMCY,       4, 0, RW, SDO_EXEC_CB_NOT),
  SINGLE_OBJ(INHIBIT_TIME_EMCY, 2, 0, RW, SDO_EXEC_CB_NOT),
#endif /* ENABLE_EMCYMSG == 1 */

#if HEARTBEAT_CONSUMER == 1
  ARRAY_HEAD(CON_HEARTBEAT_TIME, 1, CONST_ADR((BYTE FCONST *)gConstData.mHeartBeat),
                                                            RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(CON_HEARTBEAT_TIME, NUM_OF_HB_CONSUMERS, 4, 0, RW, SDO_EXEC_CB_NOT),
#endif /* HEARTBEAT_CONSUMER == 1 */

#if ENABLE_NMT_ERROR_CTRL > 1
  SINGLE_OBJ(PRO_HEARTBEAT_TIME,  2, 0, RW, SDO_EXEC_CB_NOT),
#endif /* ENABLE_NMT_ERROR_CTRL > 1 */

  ARRAY_HEAD(IDENTITY_OBJECT, 1, CONST_ADR((BYTE FCONST *)"\x04"), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(IDENTITY_OBJECT, 4, 4, CONST_ADR((BYTE FCONST *)gConstData.mIdtyVendor),
                                                       RO, SDO_EXEC_CB_NOT),

#if OBJECT_1029_USED == 1
  ARRAY_HEAD(ERROR_BEHAVIOUR, 1, CONST_ADR((BYTE FCONST *)gConstData.mErrClasses), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ERROR_BEHAVIOUR, 1, NUM_ERROR_CLASSES, (BYTE *)&abErrorBehavior, RW, SDO_EXEC_CB_NOT), /* mapping optional! */
#endif /* OBJECT_1029_USED == 1 */

#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
  ARRAY_HEAD(SDO_SERVER_PAR, 1, CONST_ADR((BYTE FCONST *)"\x02"), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(SDO_SERVER_PAR, 2, 4, 0, RO, SDO_EXEC_CB_NOT),

  STRUCT_HEAD(SDO_SERVER_PAR + 1,    1, CONST_ADR((BYTE FCONST *)"\x03"), RO, SDO_EXEC_CB_NOT),
  STRUCT_BODY(SDO_SERVER_PAR + 1, 1, 4, 0, RW, SDO_EXEC_CB_NOT),
  STRUCT_BODY(SDO_SERVER_PAR + 1, 2, 4, 0, RW, SDO_EXEC_CB_NOT),
  STRUCT_BODY(SDO_SERVER_PAR + 1, 3, 1, 0, RW, SDO_EXEC_CB_NOT),
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */

#if MAX_RX_PDOS > 0
  RPDO_PARA(0), /* Base entry for receive PDO parameter (1400). */
  RPDO_MAP(0), /* Base entry for receive PDO mapping (1600). */
#endif /* MAX_RX_PDOS > 0 */

#if MAX_TX_PDOS > 0
  TPDO_PARA(0), /* Base entry for tansmit PDO parameter (1800). */
  TPDO_MAP(0), /* Base entry for transmit PDO mapping (1A00). */
#endif /* MAX_TX_PDOS > 0 */

#if STARTUP_AUTONOMOUSLY == 1
  SINGLE_OBJ(NMT_STARTUP, 4, CONST_ADR((BYTE FCONST *)"\x80"), RO, SDO_EXEC_CB_NOT),
#endif /* STARTUP_AUTONOMOUSLY == 1 */

/* add manufacturer object dictionary entries */
#include "mfctobj.h"
/* add profile specific object dictionary entries */
#include "objentry.h"

};

/*!
  Size of the global known object dictionary.
 */
CONST WORD CODE gObjDictSize = sizeof(gObjDict) / sizeof(gObjDict[0]);


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Object dictionary implementation.
  \par File name objdict.c
  \version 38
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

