/*--------------------------------------------------------------------
       VQUEUE.C
  --------------------------------------------------------------------
       Copyright (c) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Implementation of the generic queue interface
  --------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "inst_def.h"
#include "portab.h"
#include "cos_main.h"
#include "vqueue.h"
#if VQ_CAN_MSG_QUEUE == 1
# include "cancntrl.h"
#endif /* VQ_CAN_MSG_QUEUE */
#include <string.h>
#include <stdlib.h>


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
#if VQ_ACCESS_INDEX == 1
extern tVQUEUE qctrl[];
#endif /* VQ_ACCESS_INDEX */


/*--------------------------------------------------------------------*/
/*  public functions                                                  */
/*--------------------------------------------------------------------*/

/*!
  \brief This function flushes a queue.

  This function flushes the queue specified by ptQueue.
  The queue's head information is resetted.
  The queue's body is not cleared.

  \param ptQueue - Queue's handle.

  \retval #VQ_OK
  \retval #VQ_NOTEXIST
*/
BYTE gQue_FlushQueue(VQHANDLE ptQueue)
{
  if (VALID_HANDLE) {
#if VQ_SEMAPHORES == 1
    WRLOCK  = 0;
    RDLOCK  = 0;
#endif /* VQ_SEMAPHORES */
    REAR  = 0;
    FRONT = 0;
    return VQ_OK;
  } /* if */
  else {
    return VQ_NOTEXIST;
  } /* else */
} /* gQue_FlushQueue */


#if VQ_WRITE == 1
/*!
  \brief Write an element to the queue.

  A data item located at ptData is copied to the queue specified with ptQueue.

  \warning This function is not reentrant for the same queue.
  If this function is used by multiple writers, the whole function should
  be executed without interruption by a second call.

  \param ptQueue - Queue's handle.
  \param ptData - Pointer to data item to be written to queue.

  \retval #VQ_OK
  \retval #VQ_LOCKED
  \retval #VQ_NOTEXIST
  \retval #VQ_FULL
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE gQue_Enqueue(VQHANDLE ptQueue, BYTE XDATA * ptData) REENTRANT
{
  if (VALID_HANDLE) {
    if (WRLOCK == 0) {
      WRLOCK = 1;
      /*
       The queue is considered full if
       FRONT + 1 == REAR (wrap)
       REAR + NELEMENTS - 1 == FRONT (no wrap)
       */
      if (((FRONT + 1) == REAR) || ((REAR + NELEMENTS - 1) == FRONT)) {
        WRLOCK = 0;
        return VQ_FULL; /* queue full */
      } /* if */
      /* copy new element to queue */
      MemCpy(((BYTE XDATA *)QUE_BODY_BASE_ADR + sizeof(tVQActual) +
             (FRONT X_FOLD_BY WIDTH)),	ptData, 1 X_FOLD_BY WIDTH);
      /* wrap the buffer if next element will exceed buffer limits */
      if (++FRONT >= NELEMENTS) {
        /* we point 1 element behind our queue body */
        FRONT = 0;
      } /* if */
      WRLOCK = 0;
      return VQ_OK;
    } /* if */
#if VQ_SEMAPHORES == 1
    else {
      return VQ_LOCKED;
    } /* else */
#endif /* VQ_SEMAPHORES */
  } /* if */
  else {
    return VQ_NOTEXIST;
  } /* else */
} /* gQue_Enqueue */
#endif /* VQ_WRITE */


#if VQ_READ == 1
/*!
  \brief Remove the next element from the queue.

  The next available queue entry is copied to the location ptData
  and the occupied space is released in the queue ptQueue.

  \warning This function is not reentrant for the same queue.
  If this function is used by multiple readers, the whole function should
  be executed without interruption by a second call.

  \param ptQueue - Queue's handle.
  \param ptData - Data item (only valied if function returns #VQ_OK).

  \retval #VQ_OK
  \retval #VQ_LOCKED
  \retval #VQ_NOTEXIST
  \retval #VQ_FULL
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE gQue_Dequeue(VQHANDLE ptQueue, BYTE XDATA * ptData) REENTRANT
{
  if (VALID_HANDLE) {
    if (RDLOCK == 0) {
      RDLOCK = 1;
      /*
       The queue is considered empty if
       FRONT = REAR
      */
      if (FRONT == REAR) {
        RDLOCK = 0;
        return VQ_EMPTY; /* queue empty */
      } /* if */
      /* remove element from queue */
      MemCpy(ptData, ((BYTE XDATA *)QUE_BODY_BASE_ADR + sizeof(tVQActual) +
                      (REAR X_FOLD_BY WIDTH)), 1 X_FOLD_BY WIDTH);
      /* wrap the buffer if next element will exceed buffer limits */
      if ((REAR + 1) >= NELEMENTS) {
        REAR = 0;
      }	/* if */
      else {
        ++REAR;
      } /* else */
      RDLOCK = 0;
      return VQ_OK;
    } /* if */
#if VQ_SEMAPHORES == 1
    else {
      return VQ_LOCKED;
    } /* else */
#endif /* VQ_SEMAPHORES */
  } /* if */
  else {
    return VQ_NOTEXIST;
  } /* else */
} /* gQue_Dequeue */
#endif /* VQ_READ */


#if VQ_READ_AHEAD == 1
/*!
  \brief Get a pointer to an entry in the queue.

  This function returns a pointer the next entry in the queue.
  The entry in the queue is not removed!
  After this call the queue is locked if a valid pointer was returned.

  \param ptQueue - Queue's handle.

  \retval 0 - Pointer to next entry in queue could not be obtained.
  \retval !0 - Pointer to next entry in queue.
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE XDATA * gQue_PeekTop(VQHANDLE ptQueue) REENTRANT
{
  if (VALID_HANDLE) {
    if (RDLOCK == 0) {
      RDLOCK = 1;
      /*
       The queue is considered empty if
       FRONT = REAR
      */
      if ((FRONT == REAR)) {
        RDLOCK = 0;
        return (BYTE XDATA *)0; /* queue empty */
      } /* if */
      /* return pointer to entry */
      return((BYTE XDATA *)QUE_BODY_BASE_ADR + sizeof(tVQActual) +
             (REAR X_FOLD_BY WIDTH));
      /* do not unlock! */
    } /* if */
# if VQ_SEMAPHORES == 1
    else {
      return (BYTE XDATA *)0;
    } /* else */
# endif /* VQ_SEMAPHORES */
  } /* if */
  else {
    return (BYTE XDATA *)0;
  } /* else */
} /* gQue_PeekTop */
#endif /* VQ_READ_AHEAD */


/*!
  \brief Remove an entry.

  This function has to be called after the entry fetched with
  gQue_PeekTop() is evaluated.
  \warning Do not call this function if the prior call to gQue_PeekTop()
  has not returned #VQ_OK.

  \param ptQueue - Queue's handle.

  \retval #VQ_OK
  \retval #VQ_NOTEXIST
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE gQue_DetachTop(VQHANDLE ptQueue) REENTRANT
{
  if (VALID_HANDLE) {
    /* wrap the buffer if next element will exceed buffer limits */
    if (++REAR >= NELEMENTS) {
      REAR = 0;
    }	/* if */
    RDLOCK = 0;
    return VQ_OK;
  } /* if */
  else {
    return VQ_NOTEXIST;
  } /* else */
} /* gQue_DetachTop */


#if VQ_WRITE_AHEAD == 1
/*!
  \brief Get the pointer to a free entry in a queue.

  The next free space for the specified queue is obtained.
  After this call the queue is locked if a valid pointer was returned.

  \param ptQueue - Queue's handle.

  \retval 0 - Pointer to free space in queue could not be obtained.
  \retval !0 - pointer to free queue space
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE XDATA * gQue_PeekFree(VQHANDLE ptQueue) REENTRANT
{
  if (VALID_HANDLE) {
    if (WRLOCK == 0) {
      WRLOCK = 1;
      /*
       The queue is considered full if
       FRONT + 1 == REAR (no wrap)
       REAR + NELEMENTS - 1 == FRONT
       */
      if (((FRONT + 1) == REAR) || ((REAR + NELEMENTS - 1) == FRONT)) {
        WRLOCK = 0;
        return 0; /* queue full */
      } /* if */

      /* copy new element to queue */
      return((BYTE XDATA *)QUE_BODY_BASE_ADR + sizeof(tVQActual) +
             (FRONT X_FOLD_BY WIDTH));
      /* do not unlock here */
    } /* if */
#if VQ_SEMAPHORES == 1
    else {
      return (BYTE XDATA *)0;
    } /* else */
#endif /* VQ_SEMAPHORES */
  } /* if */
  else {
    return (BYTE XDATA *)0;
  } /* else */
} /* gQue_PeekFree */


/*!
  \brief Attach a written entry to the queue.

  This function has to be called after the successful execution of
  gQue_PeekFree() and after the data bytes have been copied.
  After this call the queue is unlocked and there is one more entry
  in the queue.

  \warning Do not call this function without calling gQue_PeekFree() before.
  Otherwise queue entries will get lost.

  \param ptQueue - Queue's handle.

  \retval #VQ_OK
  \retval #VQ_NOTEXIST
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE gQue_AttachFree(VQHANDLE ptQueue) REENTRANT
{
  if (VALID_HANDLE) {
    /* wrap the buffer if next element will exceed buffer limits */
    if (++FRONT >= NELEMENTS) {
      FRONT = 0;
    } /* if */
    WRLOCK = 0;
    return VQ_OK;
  } /* if */
  else {
    return VQ_NOTEXIST;
  } /* else */
} /* gQue_AttachFree */
#endif /* VQ_WRITE_AHEAD */


/*!
  \brief Unlock the read semaphore for a queue without conditions

  If the function pairs:
  - gQue_PeekTop()/gQue_DetachTop()

  are called the read lock is only removed if the function
  gQueDetachTop() is called.
  If this is not posssible gQue_RdUnlock() must be called
  to unlock the queue for further read accesses.
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
void gQue_RdUnlock(VQHANDLE ptQueue) REENTRANT
{
  if (VALID_HANDLE) {
    RDLOCK = 0;
  }
} /* gQue_Unlock */


/*!
  \brief Unlock the write semaphore for a queue without conditions

  If the function pairs:
  - gQuePeekFree()/gQue_DetachFree()

  are called the write lock is only removed if the function
  gQueDetachFree() is called.
  If this is not posssible gQue_WrUnlock() must be called
  to unlock the queue for further write accesses.
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
void gQue_WrUnlock(VQHANDLE ptQueue) REENTRANT
{
  if (VALID_HANDLE) {
    WRLOCK = 0;
  }
} /* gQue_Unlock */


/*!
  \brief This call checks whether there are entries in the queue or not.

  \param ptQueue - Queue's handle.

  \retval 1 - there is an element in the queue.
  \retval 0 - the queue is empty
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE gQue_IsElement(VQHANDLE ptQueue)
{
  if (VALID_HANDLE) {
    /*
     The queue is considered empty if
     FRONT = REAR
    */
    if (FRONT == REAR) {
      return 0; /* queue is empty */
    } /* if */
    else {
      return 1; /* entry available */
    } /* else */
  } /* if */
  else {
    return 0;
  } /* else */
} /* gQue_IsElement */


/*!
  \brief This call returns the number of used entries from the queue.

  This function is intended for more statistical purposes.

  \param ptQueue - Queue's handle.
  \return Number of used entries.
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE gQue_Count(VQHANDLE ptQueue)
{
  if (VALID_HANDLE) {
    if (FRONT < REAR) {
      /* wrapped */
      return(NELEMENTS - 1 - REAR + FRONT);
    } /* if */
    else {
      /* not wrapped */
      return(FRONT - REAR);
    } /* else */
  } /* if */
  else {
    return VQ_NOTEXIST;
  } /* else */
} /* gQue_Used */
      

#if VQ_STRUCT_CALLBACK == 1
/*!
  \brief This call executes the callback attached to a queue.

  \param ptQueue - Queue's handle.
  \param data - data to be feeded to callback

  \retval #VQ_OK
  \retval #VQ_NOTEXIST
  \retval #VQ_NOEXEC
*/
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
BYTE gQue_ExecCallback(INSTANCE VQHANDLE ptQueue, BYTE XDATA * clbckdata)
{
  if (VALID_HANDLE) {
    if (QUE_CALLBACK) {
      (* QUE_CALLBACK)(SELF clbckdata);
      return VQ_OK;
    } /* if */
    else {
      return VQ_NOEXEC;
    } /* else */
  } /* if */
  else {
    return VQ_NOTEXIST;
  } /* else */
} /* gQue_ExecCallback */
#endif /* VQ_STRUCT_CALLBACK */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Functions of the generic queue interface.
  \par File name vqueue.c
  \version 9
  \date 27.02.04
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/






