/*--------------------------------------------------------------------
       SDOSERV.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: SDO server implementation
  --------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "portab.h"
#include "cos_main.h"
#include "vassert.h"
#include "vdbgprnt.h"
#include "objdict.h"
#include "sdoserv.h"
#include "cancntrl.h"
#include "profil.h"
#include "emcyhndl.h"
#include "buffer.h"
#include <string.h> 
#ifndef __18CXX
# include <stdio.h>
#endif /* __18CXX */
#include "usrclbck.h"

#if NMT_MASTER_ENABLE == 1
# include "nmtms.h"
#endif /* NMT_MASTER_ENABLE == 1 */

#if STORE_PARAMETERS_SUPP == 1
# include "nonvolst.h"
#endif /* STORE_PARAMETERS_SUPP == 1 */

#if DELAYED_SDO_ENABLE == 1
# include "sdodlyac.h"
#endif /* DELAYED_SDO_ENABLE == 1 */

#if PROFILE_DS401 == 1
# include "ds401.h"
#endif /* PROFILE_DS401== 1 */


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern VComParaArea XDATA gStkGlobDat; /*!< Storage block for non-volatile data. */
extern CONST OD_ENTRY CODE gObjDict[];
extern CONST WORD CODE gObjDictSize;
extern vEmcyManage XDATA EmcyTable; /* EMCY information */
extern FAR CAN_BUF XDATA tCanMsgBuffer[MAX_CAN_BUFFER];/* CAN message buffer */

#ifdef NO_UNIVERSAL_POINTERS
extern CONST VCodeArea CODE gConstData; /* constant items from object dictionary */
#endif /* NO_UNIVERSAL_POINTERS */

#if MAX_RX_PDOS > 0
extern RPDOparameter XDATA mRPDO[MAX_RX_PDOS];  /* receive PDO param. */
extern CONST VRPDOComParData CODE mRPDOAttr[MAX_RX_PDOS]; /*!< default parameters */
#endif /* MAX_RX_PDOS > 0 */

#if MAX_TX_PDOS > 0
extern TPDOparameter XDATA mTPDO[MAX_TX_PDOS];  /* transmit PDO param. */
extern CONST VTPDOComParData CODE mTPDOAttr[MAX_TX_PDOS]; /*!< default parameters */
#endif /* MAX_TX_PDOS > 0 */

/* RX PDO default mapping */
#if MAX_RX_PDOS > 0
extern CONST MAPPING CODE atRxPdoDefMap[MAX_RX_PDOS][MAX_MAPPED_OBJECTS];
#endif /* MAX_RX_PDOS > 0 */

/* TX PDO default mapping */
#if MAX_TX_PDOS > 0
extern CONST MAPPING CODE atTxPdoDefMap[MAX_TX_PDOS][MAX_MAPPED_OBJECTS];
#endif /* MAX_TX_PDOS > 0 */

#if (SYNC_USED == 1) || (TIME_STAMP_USED == 1)
extern QBYTE XDATA lHighResTime;        /* high resolution time stamp   */
#endif /* (SYNC_USED == 1) || (TIME_STAMP_USED == 1) */

#if SYNC_PRODUCER == 1
extern PRO_SYNC tSyncPro;               /* SYNC producer information    */
#endif /* SYNC_PRODUCER == 1 */

#if EXAMPLE_OBJECTS_USED == 1
extern BYTE gDomainBuffer[EXAMPLE_BUFFER_SIZE]; /* example buffer for domain data */
#endif /* EXAMPLE_OBJECTS_USED == 1 */

extern BOOLEAN DATA fNodeGuarding;      /* node guarding on/off         */

#if HEARTBEAT_CONSUMER == 1
extern CON_HEART XDATA atHeartbeatCon[];      /* consumer heartbeat info      */
#endif /* HEARTBEAT_CONSUMER == 1 */



/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/
/*! SDO server's management structure */
VSdoAttrib XDATA ServerSdo[NUM_SDO_SERVERS];

/*! SDO abort codes table */
CONST DWORD CODE alSdoAbortCodes[] = {
 0                   ,
 SAC_TOG_NOT_ALTERED ,
 SAC_SDO_TIME_OUT    ,
 SAC_UNKNOWN_CS      ,
 SAC_INVAL_BLK_SIZE  ,
 SAC_INVAL_SEQ_NUM   ,
 SAC_CRC_ERROR       ,
 SAC_OUT_OF_MEM      ,
 SAC_ACC_NOT_SUPP    ,
 SAC_WR_ONLY_OBJ     ,
 SAC_RD_ONLY_OBJ     ,
 SAC_OBJ_NOT_EXIST   ,
 SAC_NOT_MAPPED      ,
 SAC_MAP_INCONS      ,
 SAC_GEN_PARA_INC    ,
 SAC_GEN_INT_PARA    ,
 SAC_ACC_FAIL_HW     ,
 SAC_LEN_NOT_MATCH   ,
 SAC_LEN_TOO_HIGH    ,
 SAC_LEN_TOO_LOW     ,
 SAC_NO_SUB_INDEX    ,
 SAC_VAL_RNG_EXCEED  ,
 SAC_PARA_TOO_HIGH   ,
 SAC_PARA_TOO_LOW    ,
 SAC_MAX_LESS_MIN    ,
 SAC_GENERIC_ERROR   ,
 SAC_NOT_STORED      ,
 SAC_LOCAL_CONTROL   ,
 SAC_LOCAL_STATE     ,
 SAC_OD_GEN_FAILED   ,  
};


/*--------------------------------------------------------------------*/
/*  macros                                                            */
/*--------------------------------------------------------------------*/
#if SDO_WRITE_SEG_ALLOWED == 1
# define SERVER_SDO_DATA(inx, offset) ServerSdo[inx].pbData[offset]
#else
# define SERVER_SDO_DATA(inx, offset) ServerSdo[inx].bExpedBuf[offset]
#endif /* SDO_WRITE_SEG_ALLOWED == 1 */



/*--------------------------------------------------------------------*/
/*  function definitions                                              */
/*--------------------------------------------------------------------*/

/*!
  \brief Get address of an object in the object dictionary.

  If the local object dictionary is accessed via CAN
  the attributes of the entry must be evaluated.
  That's why we try to get the address.

  \param wIndex - object's index
  \param bSubIndex - object's subindex
  \retval == 0 - entry not found
  \retval != 0 - object address in dictionary table
*/
CONST OD_ENTRY CONST_PTR * gSdoS_GetEntry(WORD wIndex, BYTE bSubIndex)
{
  WORD i;
  WORD left;
  WORD right;

  /* check first entry */
  if (wIndex < gObjDict[0].wbIndex.wc) {
    return 0;
  } /* if */

  /* check last entry */
  if (wIndex > gObjDict[gObjDictSize - 1].wbIndex.wc) {
    return 0;
  } /* if */

  left = 0;
  right = gObjDictSize;

  while (right >= left) {
    i = (WORD)((left + right) >> 1);
    if(gObjDict[i].wbIndex.wc == wIndex) {
      /* Object found. */
      if((gObjDict[i].AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
        /*
         An array always consists of two entries.
         The head entry (sub-index 0) and the body entry.
         The head entry could be writeable - at least from within the
         stack side. So we should check this entry too.
         The exception from this rule is that for the mapping tables the
         sub-index could be 0 - then the mapping entries are changed
         and afterwards the mapping consistency is checked.
         So we have to check the valid sub-index for array entries in the
         caller function.
         */
        if((bSubIndex > 0) && (bSubIndex<=gObjDict[i].bSubIndex)) {
          return gObjDict+i;
        } /* if */
      } /* if */
      else {
        if(bSubIndex==gObjDict[i].bSubIndex) {
          return gObjDict+i;
        } /* if */
      } /* else */
      if ((((DWORD)wIndex << 8) + bSubIndex) <
          (((DWORD)gObjDict[i].wbIndex.wc << 8) + gObjDict[i].bSubIndex)) {
        right = (WORD)(i - 1);
      } /* if */
      else {
        left = (WORD)(i + 1);
      } /* else */
    } /* if */
    else {
      if ((((DWORD)wIndex << 8) + bSubIndex) <
          (((DWORD)gObjDict[i].wbIndex.wc << 8) + gObjDict[i].bSubIndex)) {
        right = (WORD)(i - 1);
      } /* if */
      else {
        left = (WORD)(i + 1);
      } /* else */
    } /* else */
  } /* while */
  return 0;
} /* gSdoS_GetEntry */


#if CHECK_CONSISTENCY == 1
/*!
  Check the ordering of the object dictionary entries.

  \retval 0 - All entries ordered correctly.
  \retval Index which is not ordered ascending.
*/
BYTE gCheckODOrdering(void)
{
  WORD i;
  for (i = 1; i < gObjDictSize; i++) {
    if ((((DWORD)gObjDict[i].wbIndex.wc << 8) + gObjDict[i].bSubIndex) <=
      (((DWORD)gObjDict[i - 1].wbIndex.wc << 8) + gObjDict[i - 1].bSubIndex)) {
      VDEBUG_RPT2("gCheckODOrdering(): OD entries not in ascending order (%#x/%#x)\n",
        gObjDict[i - 1].wbIndex.wc, gObjDict[i].wbIndex.wc);
      return FALSE;
    } /* if */
  } /* for */
  VDEBUG_RPT0("gCheckODOrdering(): OD entries in correct order\n");
  return TRUE;
} /* gCheckODOrdering */
#endif /* CHECK_CONSISTENCY == 1 */


/*!
  \brief Get the actual PDO number from the index information.

  If PDO relevant information is accessed we have to ensure that always
  the root entry in the object dictionary are accessed.
  Via this call we get the PDO number which can be subtracted lateron.

  \param bServIndex - Actual server
  \return - PDO number.
*/
BYTE sSdoS_GetPdoNumber(BYTE bServIndex)
{
  if (ServerSdo[bServIndex].wbIndex.wc >= RECEIVE_PDO_1_MAPPING_PAR
    && ServerSdo[bServIndex].wbIndex.wc < (RECEIVE_PDO_1_MAPPING_PAR + MAX_RX_PDOS)) {
    /* get PDO number */
    return ((BYTE)(ServerSdo[bServIndex].wbIndex.wc - RECEIVE_PDO_1_MAPPING_PAR));
  } /* if */
  else if (ServerSdo[bServIndex].wbIndex.wc >= TRANSMIT_PDO_1_MAPPING_PAR
    && ServerSdo[bServIndex].wbIndex.wc < (TRANSMIT_PDO_1_MAPPING_PAR + MAX_TX_PDOS)) {
    return ((BYTE)(ServerSdo[bServIndex].wbIndex.wc - TRANSMIT_PDO_1_MAPPING_PAR));
  } /* else if */
  else if (ServerSdo[bServIndex].wbIndex.wc >= RECEIVE_PDO_1_COMMUNI_PAR
    && ServerSdo[bServIndex].wbIndex.wc < (RECEIVE_PDO_1_COMMUNI_PAR + MAX_RX_PDOS)) {
    return ((BYTE)(ServerSdo[bServIndex].wbIndex.wc - RECEIVE_PDO_1_COMMUNI_PAR));
  } /* else if */
  else if (ServerSdo[bServIndex].wbIndex.wc >= TRANSMIT_PDO_1_COMMUNI_PAR
    && ServerSdo[bServIndex].wbIndex.wc < (TRANSMIT_PDO_1_COMMUNI_PAR + MAX_TX_PDOS)) {
    return ((BYTE)(ServerSdo[bServIndex].wbIndex.wc - TRANSMIT_PDO_1_COMMUNI_PAR));
  } /* else if */
  else {
    return 0;
  } /* else */
} /* sSdoS_GetPdoNumber */


/*!
  \brief Reset the server entry

  Via this call the managment information for a server can be reset.

  \param bServIndex - index in array ServerSdo
  \retval TRUE - Entry reset.
  \retval FALSE - Index exceeds array ServerSdo
*/
BYTE gSdoS_ClearServerInfo(BYTE bServIndex)
{
  if (bServIndex < sizeof(ServerSdo) / sizeof(ServerSdo[0])) {
    ServerSdo[bServIndex].bLastState = ST_TRANSFERRED;  
    ServerSdo[bServIndex].wbIndex.wc = 0;       
    ServerSdo[bServIndex].bSubIndex = 0;     
    ServerSdo[bServIndex].pbData = 0;       
    ServerSdo[bServIndex].SDO_DATASIZE = 0;    
#if (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1)
    ServerSdo[bServIndex].DataCounter = 0; 
#endif /* (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1) */
    ServerSdo[bServIndex].bDataToggle = 0;    
    ServerSdo[bServIndex].wTimeOut = 0;       
#if SDO_BLOCK_ALLOWED == 1
    ServerSdo[bServIndex].bActSequenceNr = 0;       
    ServerSdo[bServIndex].bBlockSize = 0;       
    ServerSdo[bServIndex].bProtThreshold = 0;       
    ServerSdo[bServIndex].bCalcCrc = 0;
    ServerSdo[bServIndex].bLastSegment = 0;
#endif /* SDO_BLOCK_ALLOWED == 1 */
#if DELAYED_SDO_ENABLE == 1
    gSdoS_StopDelayedTransfer(bServIndex);
#endif /* DELAYED_SDO_ENABLE == 1 */
    return TRUE;
  } /* if */
  else {
    return FALSE;
  } /* else */
} /* gSdoS_ClearServerInfo */


/*!
  \brief Connect SDO server to CAN buffers.

  We must connect initially our SDO server with the CAN buffers.
  Otherwise the messages would not be routed correctly.

  \param bServIndex - server instance
  \param bTxId - buffer number of SDO's transmit message
  \param bRxId - buffer number of SDO's receive message
  \retval TRUE - buffers allocated
  \retval FALSE - no buffer allocated
*/
BYTE gSdoS_SetServerCanBuf(BYTE bServIndex, BYTE bTxId, BYTE bRxId)
{
  if (bServIndex < sizeof(ServerSdo) / sizeof(ServerSdo[0])) {
    if (gSdoS_ClearServerInfo(bServIndex) == TRUE) {
      /* connect identifiers */
      ServerSdo[bServIndex].bRxId = bRxId;
      ServerSdo[bServIndex].bTxId = bTxId;
      return TRUE;
    } /* if */
    else {
      return FALSE;
    } /* else */
  } /* if */
  else {
    return FALSE;
  } /* else */
} /* gSdoS_SetServerCanBuf */



/*!
  \brief Provide an object's physical address

  Objects specified with a data pointer 0 get their
  real address with this function.
  The address is moved to pbData.
*/
STATIC void sSdoS_GetObjectAddress(BYTE bServIndex) {

  ServerSdo[bServIndex].pbData =
    gSdoS_GetObjectAddress(&ServerSdo[bServIndex]);
  
  if (ServerSdo[bServIndex].pbData == 0) {
    /* set default buffer address */
    if (ServerSdo[bServIndex].SDO_DATASIZE < 5) {
      ServerSdo[bServIndex].pbData = ServerSdo[bServIndex].bExpedBuf;
    } /* if */
#if (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1)
    else {
      ServerSdo[bServIndex].pbData =
        ServerSdo[bServIndex].abSdoDataTmpBuffer;
    } /* else */
#endif /* (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1) */
  } /* if */
  
} /* sSdoS_GetObjectAddress */



/*!
  \brief Write object data indirectly.

  For objects in the object dictionary which are
  defined with address 0 this function will be called during
  a write access.
  All data items which are written by an expedited request
  are taken from the expedited buffer.
  If the user adds such an entry this function must be adapted.

  \param ptEntry - pointer to object
  \param bServIndex - server instance
  \retval == 0 - no problem during write access,
  \retval != 0 - abort code number to be used in abort message
*/
STATIC BYTE sSdoS_WriteData(CONST OD_ENTRY CONST_PTR * ptEntry, BYTE bServIndex)
{
  BYTE DATA bRet = 0;

  /*
  * We have to do a special treatment for mapping entries.
  * This is only true for variable mapping.
  */
  if (ptEntry->wbIndex.wc == RECEIVE_PDO_1_MAPPING_PAR) {
#if MAX_RX_PDOS > 0
# if RPDO_MAP_READONLY == 1
    bRet = GEN_INT_PARA;
# else
    BYTE DATA bPdoNr;           /* index PDO parameter table            */
    BYTE DATA bBits = 0;        /* PDO length counter                   */

    /* get PDO number */
    bPdoNr = (BYTE)(ServerSdo[bServIndex].wbIndex.wc
      - RECEIVE_PDO_1_MAPPING_PAR);

    if (ServerSdo[bServIndex].bSubIndex == 0) {  /* number of mapped objects */
      BYTE bTmp = gStkGlobDat.atRxPdoActMap[bPdoNr][0].bObjLength;
      if (SERVER_SDO_DATA(bServIndex, 0) == 0) {
        /* change request according to DS301 V4.0 */
        bBits = 0;
      } /* if */
      else {
        gStkGlobDat.atRxPdoActMap[bPdoNr][0].bObjLength = SERVER_SDO_DATA(bServIndex, 0);        
        bBits = gSlvGetMapLen(gStkGlobDat.atRxPdoActMap[bPdoNr], ServerSdo[bServIndex].bSubIndex);
      } /* else */

      /* set new number, if lower 9 and bits to 64 */
      if (SERVER_SDO_DATA(bServIndex, 0) < MAX_MAPPED_OBJECTS) {
        if (bBits <= MAX_MAPPED_BITS) {
          gStkGlobDat.atRxPdoActMap[bPdoNr][0].bObjLength = SERVER_SDO_DATA(bServIndex, 0);
        } /* if */
        else {
          gStkGlobDat.atRxPdoActMap[bPdoNr][0].bObjLength = bTmp;
          /* length of objects to be mapped would exceed PDO length -> fault */
          bRet = MAP_INCONS;
        } /* else */
      } /* if */
      else {
        gStkGlobDat.atRxPdoActMap[bPdoNr][0].bObjLength = bTmp;
        /* value of parameter written too high -> fault */
        bRet = PARA_TOO_HIGH;
      } /* else */
    } /* if */
    else if (gStkGlobDat.atRxPdoActMap[bPdoNr][0].bObjLength != 0) {
      /* wrong mapping sequence */
      bRet = ACC_NOT_SUPP;
    } /* else if */
    else if (ServerSdo[bServIndex].bSubIndex < MAX_MAPPED_OBJECTS) { /* mapping parameter */
      CONST OD_ENTRY CONST_PTR * ptMappedEntry;
      /*
      * The new mapping is accepted if the following 
      * conditions are fulfilled:
      * 1. resulting PDO length would fit
      * 2. mapped entry exists in OD
      * 3. entry is mappable
      * At pData (byte 4 of received message )
      * we MUST find the following information:
      * pData[0] - bit length of entry
      * pData[1] - sub index of entry to be mapped
      * pData[2] - index (lsb) of entry to be mapped
      * pData[3] - index (msb) of entry to be mapped
      */
      if (((ptMappedEntry
        = gSdoS_GetEntry((WORD)(SERVER_SDO_DATA(bServIndex, 2)
            + ((WORD)SERVER_SDO_DATA(bServIndex, 3) << 8)),
            SERVER_SDO_DATA(bServIndex, 1))) != 0)
        && ((ptMappedEntry->AccArrayClbck & RXMAP) == RXMAP)) {
        /*
        * Evaluation of correct mapping takes places according to the
        * following rules:
        * 1. number of mapped objects == 0
        * -> evaluation on write of non zero value of number of mapped objects
        * 2. number of mapped objects != 0
        * -> evaluation on write of every single mapped entry
        */
        if (gStkGlobDat.atRxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bObjLength == 0) {
          /* test mode */
          bBits = 0;
        } /* if */
        else {
          /*
          * no special test mode -
          * shift of length is due to number of bytes stored in length
          */
          bBits = (BYTE)((BYTE)(ptMappedEntry->Length << 3)
            + gSlvGetMapLen(gStkGlobDat.atRxPdoActMap[bPdoNr],
            ServerSdo[bServIndex].bSubIndex));
        } /* else */

        if (bBits <= MAX_MAPPED_BITS) {
          /* try to set new address */
          if ((ptMappedEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
            if (ptMappedEntry->pData) {
              gStkGlobDat.atRxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].pData =
                ptMappedEntry->pData
                + (SERVER_SDO_DATA(bServIndex, 1) - 1) * (ptMappedEntry->Length);
            } /* if */
            else {
              bRet = GEN_INT_PARA;
            } /* else */
          } /* if */
          else {
            gStkGlobDat.atRxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].pData =
              ptMappedEntry->pData;
          } /* else */
          if (bRet == 0) {
            /* set new mapping parameter */
            gStkGlobDat.atRxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bObjLength
              = (BYTE)(ptMappedEntry->Length << 3);
            if ((ptMappedEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
              gStkGlobDat.atRxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bSubIndex
                = SERVER_SDO_DATA(bServIndex, 1);
            } /* if */
            else {
              gStkGlobDat.atRxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bSubIndex
                = ptMappedEntry->bSubIndex;
            } /* else */
            gStkGlobDat.atRxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bIndexLsb
              = ptMappedEntry->wbIndex.bc.lb;
            gStkGlobDat.atRxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bIndexMsb
              = ptMappedEntry->wbIndex.bc.mb;
          } /* if */
        } /* if */
        else {
          bRet = MAP_INCONS;
        } /* else */
      } /* if */
      else {
        bRet = NOT_MAPPED;
      } /* else */
    } /* if */
    else {
      bRet = NOT_MAPPED;
    } /* else */
# endif /* RPDO_MAP_READONLY == 1 */
#endif /* MAX_RX_PDOS > 0 */
  } /* if */
  else if (ptEntry->wbIndex.wc == TRANSMIT_PDO_1_MAPPING_PAR) {
#if MAX_TX_PDOS > 0
# if TPDO_MAP_READONLY == 1
    bRet = GEN_INT_PARA;
# else
    BYTE DATA bPdoNr;           /* index PDO parameter table            */
    BYTE DATA bBits = 0;        /* PDO length counter                   */

    /* get PDO number */
    bPdoNr = (BYTE)(ServerSdo[bServIndex].wbIndex.wc
      - TRANSMIT_PDO_1_MAPPING_PAR);

    if (ServerSdo[bServIndex].bSubIndex == 0) { /* number of mapped objects */
      BYTE bTmp = gStkGlobDat.atTxPdoActMap[bPdoNr][0].bObjLength;
      if (SERVER_SDO_DATA(bServIndex, 0) == 0) {
        /* change request according to DS301 V4.0 */
        bBits = 0;
      } /* if */
      else {
        gStkGlobDat.atTxPdoActMap[bPdoNr][0].bObjLength = SERVER_SDO_DATA(bServIndex, 0);
        bBits = gSlvGetMapLen(gStkGlobDat.atTxPdoActMap[bPdoNr], ServerSdo[bServIndex].bSubIndex);
      } /* else */

      /* set new number, if lower 9 and bits to 64 */
      if (SERVER_SDO_DATA(bServIndex, 0) < MAX_MAPPED_OBJECTS) {
        if (bBits <= MAX_MAPPED_BITS) {
          gStkGlobDat.atTxPdoActMap[bPdoNr][0].bObjLength = SERVER_SDO_DATA(bServIndex, 0);
        } /* if */
        else {
          gStkGlobDat.atTxPdoActMap[bPdoNr][0].bObjLength = bTmp;
          /* length of objects to be mapped would exceed PDO length -> fault */
          bRet = MAP_INCONS;
        } /* else */
      } /* if */
      else {
        gStkGlobDat.atTxPdoActMap[bPdoNr][0].bObjLength = bTmp;
        /* value of parameter written too high -> fault */
        bRet = PARA_TOO_HIGH;
      } /* else */
    } /* if */
    else if (gStkGlobDat.atTxPdoActMap[bPdoNr][0].bObjLength != 0) {
      /* wrong mapping sequence */
      bRet = ACC_NOT_SUPP;
    } /* else if */
    else if (ServerSdo[bServIndex].bSubIndex < MAX_MAPPED_OBJECTS) { /* mapping parameter */
      CONST OD_ENTRY CONST_PTR * ptMappedEntry;
      /*
      * The new mapping is accepted if the following 
      * conditions are fulfilled:
      * 1. resulting PDO length would fit
      * 2. mapped entry exists in OD
      * 3. entry is mappable
      * At pData (byte 4 of recived message )
      * we MUST find the following information:
      * pData[0] - bit length of entry
      * pData[1] - sub index of entry to be mapped
      * pData[2] - index (lsb) of entry to be mapped
      * pData[3] - index (msb) of entry to be mapped
      */
      if (((ptMappedEntry
        = gSdoS_GetEntry((WORD)(SERVER_SDO_DATA(bServIndex, 2)
          + ((WORD)SERVER_SDO_DATA(bServIndex, 3) << 8)),
            SERVER_SDO_DATA(bServIndex, 1))) != 0)
        && ((ptMappedEntry->AccArrayClbck & TXMAP) == TXMAP)) {

        /*
        * Evaluation of correct mapping takes places according to the
        * following rules:
        * 1. number of mapped objects == 0
        * -> evaluation on write of non zero value of number of mapped objects
        * 2. number of mapped objects != 0
        * -> evaluation on write of every single mapped entry
        */
        if (gStkGlobDat.atTxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bObjLength == 0) {
          /* test mode */
          bBits = 0;
        } /* if */
        else {
          /*
          * no special test mode -
          * shift of length is due to number of bytes stored in length
          */
          bBits = (BYTE)((BYTE)(ptMappedEntry->Length << 3)
            + gSlvGetMapLen(gStkGlobDat.atTxPdoActMap[bPdoNr], ServerSdo[bServIndex].bSubIndex));
        } /* else */

        if (bBits <= MAX_MAPPED_BITS) {
          /* try to set new address */
          if ((ptMappedEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
            if (ptMappedEntry->pData) {
              gStkGlobDat.atTxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].pData =
                ptMappedEntry->pData
                + (SERVER_SDO_DATA(bServIndex, 1) - 1) * (ptMappedEntry->Length);
            } /* if */
            else {
              bRet = GEN_INT_PARA;
            } /* else */
          } /* if */
          else {
            gStkGlobDat.atTxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].pData =
              ptMappedEntry->pData;
          } /* else */
          if (bRet == 0) {
            /* set new mapping parameter */
            gStkGlobDat.atTxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bObjLength
              = (BYTE)(ptMappedEntry->Length << 3);
            if ((ptMappedEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
              gStkGlobDat.atTxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bSubIndex
                = SERVER_SDO_DATA(bServIndex, 1);
            } /* if */
            else {
              gStkGlobDat.atTxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bSubIndex
                = ptMappedEntry->bSubIndex;
            } /* else */
            gStkGlobDat.atTxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bIndexLsb
              = ptMappedEntry->wbIndex.bc.lb;
            gStkGlobDat.atTxPdoActMap[bPdoNr][ServerSdo[bServIndex].bSubIndex].bIndexMsb
              = ptMappedEntry->wbIndex.bc.mb;
          } /* if */
        } /* if */
        else {
          bRet = MAP_INCONS;
        } /* else */
      } /* if */
      else {
        bRet = NOT_MAPPED;
      } /* else */
    } /* if */
    else {
      bRet = NOT_MAPPED;
    } /* else */
# endif /* TPDO_MAP_READONLY == 1 */
#endif /* MAX_TX_PDOS > 0 */
  } /* else if */
  /*
  * We have to do a special treatment for PDO parameter.
  */
  else if (ptEntry->wbIndex.wc == RECEIVE_PDO_1_COMMUNI_PAR) {
#if MAX_RX_PDOS > 0
# if RPDO_PAR_READONLY == 1
    bRet = GEN_INT_PARA;
# else

    BYTE DATA bPdoNr;           /* index PDO parameter table            */
    QBYTE DATA lNewID;          /* assigned identifier                  */

    /* get PDO number */
    bPdoNr = (BYTE)(ServerSdo[bServIndex].wbIndex.wc
      - RECEIVE_PDO_1_COMMUNI_PAR);

    switch (ptEntry->bSubIndex) {
      case 1:
        /*
          If the PDO is disabled it is only removed from the filter entry
          and disabled in the PDO management structure.
          Furthermore 29 bit identifiers are rejected.
                 | low word  | high word |
                 | lsb | msb | lsb | msb |
           pData |  0  | 1   |  2  | 3   |
        */
        if (SERVER_SDO_DATA(bServIndex, 3) & PDO_INVALID) {
          /* disable PDO */
          gCB_BufEnable(OFF, (BYTE)(PDO_RX + bPdoNr));
          mRPDO[bPdoNr].bMode = 0;
          /* no updating of COB-ID when invalidating PDO! */
        } /* if */
        else {
          /* try to enable PDO */
          if ((SERVER_SDO_DATA(bServIndex, 3) & PDO_EXTENDED) == 0) {
            /* assemble new COB ID */
            lNewID.fb.b0 = SERVER_SDO_DATA(bServIndex, 0);
            lNewID.fb.b1 = SERVER_SDO_DATA(bServIndex, 1);
            lNewID.fb.b2 = SERVER_SDO_DATA(bServIndex, 2);
            lNewID.fb.b3 = (BYTE)(SERVER_SDO_DATA(bServIndex, 3) & 0x1F);

            if ((lNewID.dw & 0x1FFFFFFFUL) == 0) {
              bRet = PARA_TOO_LOW;
            } /* if */
            else if (mRPDO[bPdoNr].bMode == 1) {
              /* already enabled: only current ID accepted */
              if ((lNewID.dw & 0x7FF) != tCanMsgBuffer[(BYTE)(PDO_RX + bPdoNr)].tMsg.qbId.dw) {
                bRet = LOCAL_CONTROL;
              } /* if */
            } /* else if */
            else {
              /* disabled: check new ID */
              /* configure new ID */
              if (gCB_BufChangeId((BYTE)(PDO_RX + bPdoNr), (lNewID.dw & 0x7FF)) == FALSE) {
                bRet = LOCAL_CONTROL;
              } /* if */
              else {
                mRPDO[bPdoNr].bMode = 1;
              } /* else */
            } /* else */
          } /* if */
          else {
            /* no extended IDs supported */
            bRet = VAL_RNG_EXCEED;
          } /* else */
        } /* else */
        break;

      case 2:
        if (SERVER_SDO_DATA(bServIndex, 0) == 254) {
          /* manufacturer specific */
          /* set new transmission type, reset SYNC counter */
#  if SYNC_USED == 1
          mRPDO[bPdoNr].bSyncCounter = 0;
#  endif /* SYNC_USED == 1 */
          RPDO_PARAM[bPdoNr].mTT = 254;
        }
        else if (SERVER_SDO_DATA(bServIndex, 0) == 255) {
          /* device profile specific */
          /* set new transmission type, reset SYNC counter */
#  if SYNC_USED == 1
          mRPDO[bPdoNr].bSyncCounter = 0;
#  endif /* SYNC_USED == 1 */
          RPDO_PARAM[bPdoNr].mTT = 255;
        }
#  if SYNC_USED == 1
        else if (SERVER_SDO_DATA(bServIndex, 0) < 241) {
          /* set new transmission type, reset SYNC counter */
          mRPDO[bPdoNr].bSyncCounter = 0;
          RPDO_PARAM[bPdoNr].mTT = SERVER_SDO_DATA(bServIndex, 0);
        } /* else if */
#  endif /* SYNC_USED == 1 */
        else {
          bRet = PARA_TOO_HIGH;
        } /* else */
        break;
      default:
        
        break;
    } /* switch */
# endif /* RPDO_PAR_READONLY == 1 */
#endif /* MAX_RX_PDOS > 0 */
  } /* else if */
  else if (ptEntry->wbIndex.wc == TRANSMIT_PDO_1_COMMUNI_PAR) {
#if MAX_TX_PDOS > 0
# if TPDO_PAR_READONLY == 1
    bRet = GEN_INT_PARA;
# else

    BYTE DATA bPdoNr;           /* index PDO parameter table            */
    QBYTE DATA lNewID;          /* assigned identifier                  */

    /* get PDO number */
    bPdoNr = (BYTE)(ServerSdo[bServIndex].wbIndex.wc
      - TRANSMIT_PDO_1_COMMUNI_PAR);

    switch (ptEntry->bSubIndex) {
      case 1:
        /*
          If the PDO is disabled it is only removed from the filter entry
          and disabled in the PDO management structure.
          Furthermore 29 bit identifiers are rejected.
                 | low word  | high word |
                 | lsb | msb | lsb | msb |
           pData |  0  | 1   |  2  | 3   |
        */
        if (SERVER_SDO_DATA(bServIndex, 3) & PDO_INVALID) {
          /* disable PDO */
          gCB_BufEnable(OFF, (BYTE)(PDO_TX + bPdoNr));
          mTPDO[bPdoNr].bMode = 0;
          /* no updating of COB-ID when invalidating PDO! */
        } /* if */
        else {
          /* try to enable PDO */
          if ((SERVER_SDO_DATA(bServIndex, 3) & PDO_EXTENDED) == 0) {
            /* assemble new COB ID */
            lNewID.fb.b0 = SERVER_SDO_DATA(bServIndex, 0);
            lNewID.fb.b1 = SERVER_SDO_DATA(bServIndex, 1);
            lNewID.fb.b2 = SERVER_SDO_DATA(bServIndex, 2);
            lNewID.fb.b3 = SERVER_SDO_DATA(bServIndex, 3) & 0x1F;

            if ((lNewID.dw & 0x1FFFFFFFUL) == 0) {
              bRet = PARA_TOO_LOW;
            } /* if */
            else if (mTPDO[bPdoNr].bMode == 1) {
              /* already enabled: only current ID accepted */
              if ((lNewID.dw & 0x7FF) != tCanMsgBuffer[(BYTE)(PDO_TX + bPdoNr)].tMsg.qbId.dw) {
                bRet = LOCAL_CONTROL;
              } /* if */
            } /* else if */
            else
            {
              /* disabled: check new ID */
              /* configure new ID */
              if (gCB_BufChangeId((BYTE)(PDO_TX + bPdoNr), (lNewID.dw & 0x7FF)) == FALSE) {
                bRet = LOCAL_CONTROL;
              } /* if */
              else {
                mTPDO[bPdoNr].bMode = 1;
              } /* else */
            } /* else */
          } /* if */
          else {
            bRet = VAL_RNG_EXCEED;
          } /* else */
        } /* else */
        break;

      case 2:
        if (SERVER_SDO_DATA(bServIndex, 0) == 254) {
          /* manufacturer specific */
          /* set new transmission type, reset SYNC counter */
#  if SYNC_USED == 1
          mTPDO[bPdoNr].bSyncCounter = 0;
#  endif /* SYNC_USED == 1 */
          TPDO_PARAM[bPdoNr].mTT = 254;
        }
        else if (SERVER_SDO_DATA(bServIndex, 0) == 255) {
          /* device profile specific */
          /* set new transmission type, reset SYNC counter */
#  if SYNC_USED == 1
          mTPDO[bPdoNr].bSyncCounter = 0;
#  endif /* SYNC_USED == 1 */
          TPDO_PARAM[bPdoNr].mTT = 255;
        }
        else if (SERVER_SDO_DATA(bServIndex, 0) == 253) {
          /* asynchronous remote request */
          /* set new transmission type, reset SYNC counter */
#  if SYNC_USED == 1
          mTPDO[bPdoNr].bSyncCounter = 0;
#  endif /* SYNC_USED == 1 */
          TPDO_PARAM[bPdoNr].mTT = 253;
        }
#  if SYNC_USED == 1
        else if (SERVER_SDO_DATA(bServIndex, 0) == 252) {
          /* synchronous remote request */
          /* set new transmission type, reset SYNC counter */
          mTPDO[bPdoNr].bSyncCounter = 0;
          TPDO_PARAM[bPdoNr].mTT = 252;
        }
        else if (SERVER_SDO_DATA(bServIndex, 0) < 241) {
          /* set new transmission type, reset SYNC counter */
          mTPDO[bPdoNr].bSyncCounter = 0;
          TPDO_PARAM[bPdoNr].mTT = SERVER_SDO_DATA(bServIndex, 0);
        }
#  endif /* SYNC_USED == 1 */
        else {
          bRet = PARA_TOO_HIGH;
        }
        break;

      case 3:
        /* set new inhibit time */
        if (mTPDO[bPdoNr].bMode == 0 ) {
          TPDO_PARAM[bPdoNr].wInhibitReload
          = ((WORD)SERVER_SDO_DATA(bServIndex, 1) << 8)
          + SERVER_SDO_DATA(bServIndex, 0);
        } /* if */
        else {
          /* it is not allowed to change the inhibit time while the PDO exists */
          bRet = LOCAL_CONTROL;
        } /* else */
        break;

      case 5:
        /* set new event time */
        TPDO_PARAM[bPdoNr].wEventReload
        = ((WORD)SERVER_SDO_DATA(bServIndex, 1) << 8)
        + SERVER_SDO_DATA(bServIndex, 0);
        /* force PDO transmission */
        mTPDO[bPdoNr].wEventTimer = 0;
        break;
      default:
        
        break;
    } /* switch */
# endif /* TPDO_PAR_READONLY == 1 */
#endif /* MAX_TX_PDOS > 0 */
  } /* else if */
  else {

#if (TIME_STAMP_USED == 1) || (MULTI_SDO_SERVER == 1 && NUM_SDO_SERVERS > 1)
    QBYTE DATA lNewID;          /* assigned identifier                  */
#endif /* (TIME_STAMP_USED == 1) || (MULTI_SDO_SERVER == 1 && NUM_SDO_SERVERS > 1) */

    switch (ptEntry->wbIndex.wc) {
#if ENABLE_EMCYMSG == 1
      case ERROR_FIELD:
        if (ptEntry->bSubIndex == 0 && SERVER_SDO_DATA(bServIndex, 0) == 0) {
          gResetPredefErrors();
        } /* if */
        else {
          bRet = GEN_INT_PARA;
        } /* else */
        break;
#endif /* ENABLE_EMCYMSG == 1 */

#if SYNC_USED == 1
      case COB_ID_SYNC:
        /*
        * Allow only
        * standard 11 bit identifier
        * and SYNC consumer mode.
        *        | low word  | high word |
        *        | lsb | msb | lsb | msb |
        *  pData |  0  | 1   |  2  | 3   |
        */
        if ((SERVER_SDO_DATA(bServIndex, 3) & 0x60) == 0) {
          /* assemble new COB ID */
          gStkGlobDat.mSyncId.fb.b0 = SERVER_SDO_DATA(bServIndex, 0);
          gStkGlobDat.mSyncId.fb.b1 = SERVER_SDO_DATA(bServIndex, 1);
          gStkGlobDat.mSyncId.fb.b2 = SERVER_SDO_DATA(bServIndex, 2);
          gStkGlobDat.mSyncId.fb.b3 = SERVER_SDO_DATA(bServIndex, 3);

          if ((gStkGlobDat.mSyncId.dw & 0x1FFFFFFFUL) == 0) {
            bRet = PARA_TOO_LOW;
          } /* if */
          else {
            if ((tCanMsgBuffer[SYNC_RX].tMsg.qbId.dw & 0x1FFFFFFF) !=
                (gStkGlobDat.mSyncId.dw & 0x1FFFFFFF)) {
              if (gCB_BufChangeId(SYNC_RX, gStkGlobDat.mSyncId.dw) == FALSE) {
                bRet = LOCAL_CONTROL;
              } /* if */
            } /* if */
            else {
              /* enable ID */
              gCB_BufEnable(ON, SYNC_RX);
            } /* else */
# if SYNC_PRODUCER == 1
            tCanMsgBuffer[SYNC_TX].tMsg.qbId.dw
              = tCanMsgBuffer[SYNC_RX].tMsg.qbId.dw;
            tSyncPro.bRun = OFF; /* SYNC producer disabled */
# endif /* SYNC_PRODUCER == 1 */
          } /* else */
        } /*if */
# if SYNC_PRODUCER == 1
        else if ((SERVER_SDO_DATA(bServIndex, 3) & 0x60) == 0x40) {
          tSyncPro.wTime = 0; /* reset timer */
          tSyncPro.bRun = ON; /* SYNC producer enabled */
        } /* else if */
# endif /* SYNC_PRODUCER == 1 */
        else {
          bRet = VAL_RNG_EXCEED;
        } /* else */
        break;

      case SYNC_CYCLE_PERIOD:
        /* set new time in us */
        gStkGlobDat.lComCyclePeriod.fb.b0 = SERVER_SDO_DATA(bServIndex, 0);
        gStkGlobDat.lComCyclePeriod.fb.b1 = SERVER_SDO_DATA(bServIndex, 1);
        gStkGlobDat.lComCyclePeriod.fb.b2 = SERVER_SDO_DATA(bServIndex, 2);
        gStkGlobDat.lComCyclePeriod.fb.b3 = SERVER_SDO_DATA(bServIndex, 3);
# if SYNC_PRODUCER == 1
        tSyncPro.wTime = 0; /* reset timer */
        tSyncPro.wReloadTime = (WORD)(gStkGlobDat.lComCyclePeriod.dw / 1000); /* set timer */
# endif /* SYNC_PRODUCER == 1 */
        break;

#endif /* SYNC_USED == 1 */

#if STORE_PARAMETERS_SUPP == 1
      case STORE_PARAMETERS:
        if ((SERVER_SDO_DATA(bServIndex, 0) == 0x73)    /* "s" */
         && (SERVER_SDO_DATA(bServIndex, 1) == 0x61)    /* "a" */
         && (SERVER_SDO_DATA(bServIndex, 2) == 0x76)    /* "v" */
         && (SERVER_SDO_DATA(bServIndex, 3) == 0x65)) { /* "e" */
          if (!gStoreConfiguration(ServerSdo[bServIndex].bSubIndex)) {
            bRet = ACC_FAIL_HW;
          } /* if */
        } /* if */
        else {
          bRet = NOT_STORED;
        } /* else */
        break;

      case RESTORE_PARAMETERS:
        if ((SERVER_SDO_DATA(bServIndex, 0) == 0x6c)    /* "l" */
         && (SERVER_SDO_DATA(bServIndex, 1) == 0x6f)    /* "o" */
         && (SERVER_SDO_DATA(bServIndex, 2) == 0x61)    /* "a" */
         && (SERVER_SDO_DATA(bServIndex, 3) == 0x64)) { /* "d" */
          if (!gNvDelete(ServerSdo[bServIndex].bSubIndex)) {
            bRet = ACC_FAIL_HW;
          } /* if */
        } /* if */
        else {
          bRet = NOT_STORED;
        } /* else */
        break;
#endif /* STORE_PARAMETERS_SUPP == 1 */

#if TIME_STAMP_USED == 1
      case COB_ID_TIME_STAMP:
        if ((SERVER_SDO_DATA(bServIndex, 3) & 0x80) == 0) {
          /* no TIME message consumer: deregister TIME message */
          gCB_BufEnable(OFF, TIME_RX);
          tCanMsgBuffer[TIME_RX].tMsg.qbId.fb.b3 &= 0x7f;
        } /* if */
        else if (SERVER_SDO_DATA(bServIndex, 3) & 0x60) {
          /* TIME message producer and 29-bit ID not supported: send ABORT */
          bRet = VAL_RNG_EXCEED;
        } /* else if */
        else {
          /* set COB-ID and enable TIME message reception */
          /* assemble new COB ID */
          lNewID.fb.b0 = SERVER_SDO_DATA(bServIndex, 0);
          lNewID.fb.b1 = SERVER_SDO_DATA(bServIndex, 1);
          lNewID.fb.b2 = SERVER_SDO_DATA(bServIndex, 2);
          lNewID.fb.b3 = (BYTE)(SERVER_SDO_DATA(bServIndex, 3) & 0x1F);
                  

          if ((lNewID.dw & 0x1FFFFFFFUL) == 0) {
            bRet = PARA_TOO_LOW;
          } /* if */
          else
          {
            if (tCanMsgBuffer[TIME_RX].tMsg.qbId.dw
                   != (lNewID.dw & 0x7FF)) {
              if (gCB_BufChangeId(TIME_RX, (lNewID.dw & 0x7FF)) == FALSE) {
                bRet = LOCAL_CONTROL;
              } /* if */
            } /* if */
            else
            {
              /* enable ID */
              gCB_BufEnable(ON, TIME_RX);
              tCanMsgBuffer[TIME_RX].tMsg.qbId.fb.b3 |= 0x80;
            } /* else */
          } /* else */
        } /* else */
        break;

      case TIME_STAMP:
        /* set new time in us */
        lHighResTime.fb.b0 = SERVER_SDO_DATA(bServIndex, 0);
        lHighResTime.fb.b1 = SERVER_SDO_DATA(bServIndex, 1);
        lHighResTime.fb.b2 = SERVER_SDO_DATA(bServIndex, 2);
        lHighResTime.fb.b3 = SERVER_SDO_DATA(bServIndex, 3);
        break;
#endif /* TIME_STAMP_USED == 1 */

#if ENABLE_EMCYMSG == 1
      case COB_ID_EMCY:
        bRet = RD_ONLY_OBJ;
        break;

      case INHIBIT_TIME_EMCY:
        /* set new time in 100 us */
        gStkGlobDat.mEmcyInhReloadTime.bc.lb = SERVER_SDO_DATA(bServIndex, 0);
        gStkGlobDat.mEmcyInhReloadTime.bc.mb = SERVER_SDO_DATA(bServIndex, 1);
        break;
#endif /* ENABLE_EMCYMSG == 1 */

#if HEARTBEAT_CONSUMER == 1
      case CON_HEARTBEAT_TIME:
        {
          BYTE i, k;
          BYTE bNode;
          WORD wTime;

          /* get array index */
          k = (BYTE)(ServerSdo[bServIndex].bSubIndex - 1);

          /* get heartbeat time */
          wTime = SERVER_SDO_DATA(bServIndex, 0)
                | ((WORD)SERVER_SDO_DATA(bServIndex, 1) << 8);

          if (wTime == 0) {             /* clear entry                      */
            if (gStkGlobDat.atHeartbeatCon[k].bNodeID == 0) {   /* entry unused         */
              bRet = 0;
            } /* if */
            else {                      /* reset entry                      */
              atHeartbeatCon[k].bRun = OFF;
              atHeartbeatCon[k].bStatus = 0;
              atHeartbeatCon[k].wTime = 0;
              gStkGlobDat.atHeartbeatCon[k].wReloadTime.wc = 0;
              gStkGlobDat.atHeartbeatCon[k].bNodeID = 0;
              gCB_BufEnable(OFF, (BYTE)(ERRCTRL_RX + k)); /* disable COB_ID */
            } /* else */
          } /* if */
          else {                        /* set entry                        */
            /* get Node-ID */
            bNode = SERVER_SDO_DATA(bServIndex, 2);
            if ((bNode < 1) || (bNode > 127)) {     /* check Node-ID range  */
              bRet = VAL_RNG_EXCEED;
            } /* if */
            else if (bNode == gStkGlobDat.atHeartbeatCon[k].bNodeID) { /* equal Node-ID */
              /* set new time value */
              atHeartbeatCon[k].bRun = OFF;
              atHeartbeatCon[k].bStatus = 0;
              atHeartbeatCon[k].wTime = wTime;
              gStkGlobDat.atHeartbeatCon[k].wReloadTime.wc = wTime;
              atHeartbeatCon[k].bRun = FIRST_HB;
            } /* else if */
            else if (gStkGlobDat.atHeartbeatCon[k].bNodeID == 0) {  /* entry unused     */
              /* check already assigned slots */
              for (i = 0; i < NUM_OF_HB_CONSUMERS; i++) {
                if (bNode == gStkGlobDat.atHeartbeatCon[i].bNodeID) {   /* entry found  */
                  return GEN_PARA_INC;
                } /* if */
              } /* for */
              /* set new consumer heartbeat */
              gStkGlobDat.atHeartbeatCon[k].bNodeID = bNode;
              atHeartbeatCon[k].bStatus = 0;
              atHeartbeatCon[k].wTime = wTime;
              gStkGlobDat.atHeartbeatCon[k].wReloadTime.wc = wTime;
              atHeartbeatCon[k].bRun = FIRST_HB;
              /* set COB-ID */
              gCB_BufSetId((BYTE)(ERRCTRL_RX + k), ERRCTRL_ID + bNode, 1, RX_DIR);
              gCB_BufEnable(ON, (BYTE)(ERRCTRL_RX + k)); /* enable COB-ID */
            } /* else if */
            else {                      /* unequal Node-ID                  */
              bRet = GEN_PARA_INC;
            } /* else */
          } /* else */
        }
        break;
#endif /* HEARTBEAT_CONSUMER == 1 */

#if ENABLE_NMT_ERROR_CTRL > 1
      case PRO_HEARTBEAT_TIME:
        gStkGlobDat.tHeartbeatPro.wReloadTime.bc.lb = SERVER_SDO_DATA(bServIndex, 0);
        gStkGlobDat.tHeartbeatPro.wReloadTime.bc.mb = SERVER_SDO_DATA(bServIndex, 1);
        gStkGlobDat.tHeartbeatPro.wTime = gStkGlobDat.tHeartbeatPro.wReloadTime.wc;
        /* switch between heartbeat and nodeguarding */
        fNodeGuarding = (BOOLEAN)(gStkGlobDat.tHeartbeatPro.wReloadTime.wc ? OFF : ON);
# if ENABLE_NMT_ERROR_CTRL == 3
        if (fNodeGuarding == ON) {
          gCan_PrepareRemoteMessage(&tCanMsgBuffer[ERRCTRL_TX].tMsg);
        } /* if */
# endif /* ENABLE_NMT_ERROR_CTRL == 3 */
        break;
#endif /* ENABLE_NMT_ERROR_CTRL > 1 */

      case SDO_SERVER_PAR:
        bRet = RD_ONLY_OBJ;
        break;

#if MULTI_SDO_SERVER == 1
# if NUM_SDO_SERVERS == 2
      case SDO_SERVER_PAR + 1:
        switch (ServerSdo[bServIndex].bSubIndex) {
          case 0:
            bRet = RD_ONLY_OBJ;
            break;
          case 1:
            if (SERVER_SDO_DATA(bServIndex, 3) & SDO_INVALID) {
              /* disable SDO_RX + 1 */
              gCB_BufEnable(OFF, SDO_RX + 1);
              ServerSdo[1].bRxIdEnabled = FALSE;
              /* no updating of COB-ID when invalidating SDO! */
            } /* if */
            else { /* enable SDO_RX + 1 */
              if ((SERVER_SDO_DATA(bServIndex, 3) & SDO_EXTENDED) == 0) {
                /* assemble COD-ID */
                lNewID.fb.b0 = SERVER_SDO_DATA(bServIndex, 0);
                lNewID.fb.b1 = SERVER_SDO_DATA(bServIndex, 1);
                lNewID.fb.b2 = SERVER_SDO_DATA(bServIndex, 2);
                lNewID.fb.b3 = (BYTE)(SERVER_SDO_DATA(bServIndex, 3) & 0x1F);

                if ((lNewID.dw & 0x1FFFFFFFUL) == 0) {
                  bRet = PARA_TOO_LOW;
                } /* if */
                else if (ServerSdo[1].bRxIdEnabled == TRUE) {
                  /* already enabled: only current ID accepted */
                  if (lNewID.dw != (tCanMsgBuffer[SDO_RX + 1].tMsg.qbId.dw & 0x7FF)) {
                    bRet = LOCAL_CONTROL;
                  } /* if */
                } /* else if */
                else
                {
                  /* disabled: check new ID */
                  /* configure new ID */
                  if (gCB_BufChangeId(SDO_RX + 1, (lNewID.dw & 0x7FF)) == FALSE) {
                    bRet = LOCAL_CONTROL;
                  } /* if */
                  else
                  {
                    ServerSdo[1].bRxIdEnabled = TRUE;
                  } /* else */
                } /* else */
              } /* if */
              else {
                /* no extended IDs supported */
                bRet = VAL_RNG_EXCEED;
              } /* else */
            } /* else */
            break;
          case 2:
            if (SERVER_SDO_DATA(bServIndex, 3) & SDO_INVALID) {
              /* disable SDO_TX + 1 */
              gCB_BufEnable(OFF, SDO_TX + 1);
              ServerSdo[1].bTxIdEnabled = FALSE;
              /* no updating of COB-ID when invalidating SDO! */
            } /* if */
            else { /* enable SDO_TX + 1 */
              if ((SERVER_SDO_DATA(bServIndex, 3) & SDO_EXTENDED) == 0) {
                /* assemble COD-ID */
                lNewID.fb.b0 = SERVER_SDO_DATA(bServIndex, 0);
                lNewID.fb.b1 = SERVER_SDO_DATA(bServIndex, 1);
                lNewID.fb.b2 = SERVER_SDO_DATA(bServIndex, 2);
                lNewID.fb.b3 = (BYTE)(SERVER_SDO_DATA(bServIndex, 3) & 0x1F);

                if ((lNewID.dw & 0x1FFFFFFFUL) == 0) {
                  bRet = PARA_TOO_LOW;
                } /* if */
                else if (ServerSdo[1].bTxIdEnabled == TRUE) {
                  /* already enabled: only current ID accepted */
                  if (lNewID.dw != (tCanMsgBuffer[SDO_TX + 1].tMsg.qbId.dw & 0x7FF)) {
                    bRet = LOCAL_CONTROL;
                  } /* if */
                } /* else if */
                else
                {
                  /* disabled: check new ID */
                  /* configure new ID */
                  if (gCB_BufChangeId(SDO_TX + 1, (lNewID.dw & 0x7FF)) == FALSE) {
                    bRet = LOCAL_CONTROL;
                  } /* if */
                  else
                  {
                    ServerSdo[1].bTxIdEnabled = TRUE;
                  } /* else */
                } /* else */
              } /* if */
              else {
                /* no extended IDs supported */
                bRet = VAL_RNG_EXCEED;
              } /* else */
            } /* else */
            break;
          case 3: {
              BYTE bNode;
              bNode = SERVER_SDO_DATA(bServIndex, 0);
              if (bNode) {
                if (bNode > 127) {
                  /* value range exceeded */
                  bRet = VAL_RNG_EXCEED;
                } /* if */
                else {
                  ServerSdo[1].bNodeIdClient = bNode;
                } /* else */
              } /* if */
              else {
                /* too low */
                bRet = LEN_TOO_LOW;
              } /* else */
            } /* end block */
            break;
          default:
            bRet = NO_SUB_INDEX;
            break;
        } /* switch */

        if ((ServerSdo[1].bRxIdEnabled == TRUE)
            && (ServerSdo[1].bTxIdEnabled == TRUE)) {
          ServerSdo[1].bChannelEnabled = TRUE;
        } /* if */
        else {
          ServerSdo[1].bChannelEnabled = FALSE;
        } /* else */
        break;
# endif /* NUM_SDO_SERVERS == 2 */
#endif /* MULTI_SDO_SERVER == 1 */

      default:
#if DELAYED_SDO_ENABLE == 1
      /* check for object size max. 4 byte */
      if (ServerSdo[bServIndex].SDO_DATASIZE < 5) {
        /* perform delayed download and save state */
        ServerSdo[bServIndex].bDelayedState = gSdoS_DelayedDownload(bServIndex);

        /* create return value */
        if (ServerSdo[bServIndex].bDelayedState == NO_SUB_INDEX) {
          bRet = NO_SUB_INDEX; /* wrong subindex */
        } /* if */
        else if (ServerSdo[bServIndex].bDelayedState == DELAY_ERROR) {
          bRet = GEN_INT_PARA; /* general error */
        } /* else if */
        else if ((ServerSdo[bServIndex].bDelayedState == DELAY_WRITE_REQ) ||
                 (ServerSdo[bServIndex].bDelayedState == DELAY_COMPLETE)) {
          /* OK: nothing to change */
        } /* else if */
        else
#endif /* DELAYED_SDO_ENABLE == 1 */
        {
          break;
        } /* else */
#if DELAYED_SDO_ENABLE == 1
      } /* if */
      else {
        /* object to large: delayed SDO access only supported up to 4 byte (expedited) */
        bRet = GEN_INT_PARA;
      } /* else */
#endif /* DELAYED_SDO_ENABLE == 1 */
    } /* switch */
  } /* else */
  return bRet;
} /* sSdoS_WriteData */

/*!
  \brief Read object data indirectly.

  For objects in the object dictionary which are defined
  with their location address set to 0 this function will be called during
  a read access.
  The following actions will be taken:
  - If the data item is shorter then 5 bytes (expedited) it is moved to the
  expedited buffer connected to this server instance.
  - The address of the object's buffer area is copied
  to ServerSdo[bServIndex].pbData only if the access is segmented
  - If the user adds such an entry this function must be adapted.

  \param ptEntry - pointer to object
  \param bServIndex - server instance.
  \retval == 0 - no problem during read access,
  \retval != 0 - abort code number to be used in abort message
*/
STATIC BYTE sSdoS_ReadData(CONST OD_ENTRY CONST_PTR * ptEntry, BYTE bServIndex)
{
  BYTE DATA bRet = 0;

  /* clear expedited buffer */
  ServerSdo[bServIndex].bExpedBuf[0] = 0;
  ServerSdo[bServIndex].bExpedBuf[1] = 0;
  ServerSdo[bServIndex].bExpedBuf[2] = 0;
  ServerSdo[bServIndex].bExpedBuf[3] = 0;

  /*
  * We have to do a special treatment for mapping entries.
  * This is only true for variable mapping.
  */
  if (ptEntry->wbIndex.wc == RECEIVE_PDO_1_MAPPING_PAR) {

#if MAX_RX_PDOS > 0
    BYTE DATA bPdoNr;           /* index PDO parameter table            */

    /* get PDO number */
    bPdoNr = (BYTE)(ServerSdo[bServIndex].wbIndex.wc
      - RECEIVE_PDO_1_MAPPING_PAR);

    /* it is an array - so we use the array pointer */
    switch (ServerSdo[bServIndex].bSubIndex) {
    case 0:
      ServerSdo[bServIndex].bExpedBuf[0] = RPDO_MAPPING[bPdoNr][0].bObjLength;
      break;

    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      /* default/variable mapping object mapping parameter */
      ServerSdo[bServIndex].bExpedBuf[0] =
        RPDO_MAPPING[bPdoNr][ServerSdo[bServIndex].bSubIndex].bObjLength;
      ServerSdo[bServIndex].bExpedBuf[1] =
        RPDO_MAPPING[bPdoNr][ServerSdo[bServIndex].bSubIndex].bSubIndex;
      ServerSdo[bServIndex].bExpedBuf[2] =
        RPDO_MAPPING[bPdoNr][ ServerSdo[bServIndex].bSubIndex].bIndexLsb;
      ServerSdo[bServIndex].bExpedBuf[3] =
        RPDO_MAPPING[bPdoNr][ServerSdo[bServIndex].bSubIndex].bIndexMsb;
      break;

    default:
      bRet = GEN_INT_PARA;
      break;
    } /* switch */
#endif /* MAX_RX_PDOS > 0 */
  } /* if */
  else if (ptEntry->wbIndex.wc == TRANSMIT_PDO_1_MAPPING_PAR) {
#if MAX_TX_PDOS > 0
    BYTE DATA bPdoNr;           /* index PDO parameter table            */

    /* get PDO number */
    bPdoNr = (BYTE)(ServerSdo[bServIndex].wbIndex.wc
      - TRANSMIT_PDO_1_MAPPING_PAR);

    /* it is an arry - so we use the array pointer */
    switch (ServerSdo[bServIndex].bSubIndex) {
    case 0:
      ServerSdo[bServIndex].bExpedBuf[0] = TPDO_MAPPING[bPdoNr][0].bObjLength;
      break;

    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
      /* default/variable mapping object mapping parameter */
      ServerSdo[bServIndex].bExpedBuf[0] =
        TPDO_MAPPING[bPdoNr][ServerSdo[bServIndex].bSubIndex].bObjLength;
      ServerSdo[bServIndex].bExpedBuf[1] =
        TPDO_MAPPING[bPdoNr][ServerSdo[bServIndex].bSubIndex].bSubIndex;
      ServerSdo[bServIndex].bExpedBuf[2] =
        TPDO_MAPPING[bPdoNr][ServerSdo[bServIndex].bSubIndex].bIndexLsb;
      ServerSdo[bServIndex].bExpedBuf[3] =
        TPDO_MAPPING[bPdoNr][ServerSdo[bServIndex].bSubIndex].bIndexMsb;
      break;

    default:
      bRet = GEN_INT_PARA;
      break;
    } /* switch */
#endif /* MAX_TX_PDOS > 0 */
  } /* else if */
  /*
  * We have to do a special treatment for PDO parameter.
  */
  else if (ptEntry->wbIndex.wc == RECEIVE_PDO_1_COMMUNI_PAR) {
#if MAX_RX_PDOS > 0
    BYTE DATA bPdoNr;           /* index PDO parameter table            */

    /* get PDO number */
    bPdoNr = (BYTE)(ServerSdo[bServIndex].wbIndex.wc
      - RECEIVE_PDO_1_COMMUNI_PAR);

    switch (ServerSdo[bServIndex].bSubIndex) {
# ifdef NO_UNIVERSAL_POINTERS
    case 0:
      ServerSdo[bServIndex].bExpedBuf[0] = 2;
      break;
# endif /* NO_UNIVERSAL_POINTERS */

    case 1:
      ServerSdo[bServIndex].bExpedBuf[0] = GET_ID((BYTE)(PDO_RX + bPdoNr), 0);
      ServerSdo[bServIndex].bExpedBuf[1] = GET_ID((BYTE)(PDO_RX + bPdoNr), 1);
      /* do not care for extended ID */
      ServerSdo[bServIndex].bExpedBuf[2] = 0;
      ServerSdo[bServIndex].bExpedBuf[3] =
        (mRPDO[bPdoNr].bMode ? 0 : PDO_INVALID);
      break;

    case 2:
      ServerSdo[bServIndex].bExpedBuf[0] = RPDO_PARAM[bPdoNr].mTT;
      break;

    default:
      bRet = GEN_INT_PARA;
      break;
    } /* switch */
#endif /* MAX_RX_PDOS > 0 */
  } /* else if */
  else if (ptEntry->wbIndex.wc == TRANSMIT_PDO_1_COMMUNI_PAR) {
#if MAX_TX_PDOS > 0
    BYTE DATA bPdoNr;           /* index PDO parameter table            */

    /* get PDO number */
    bPdoNr = (BYTE)(ServerSdo[bServIndex].wbIndex.wc
      - TRANSMIT_PDO_1_COMMUNI_PAR);

    switch (ServerSdo[bServIndex].bSubIndex) {
# ifdef NO_UNIVERSAL_POINTERS
    case 0:
      ServerSdo[bServIndex].bExpedBuf[0] = 5;
      break;
# endif /* NO_UNIVERSAL_POINTERS */

    case 1:
      ServerSdo[bServIndex].bExpedBuf[0] = GET_ID((BYTE)(PDO_TX + bPdoNr), 0);
      ServerSdo[bServIndex].bExpedBuf[1] = GET_ID((BYTE)(PDO_TX + bPdoNr), 1);
      /* do not care for extended ID */
      ServerSdo[bServIndex].bExpedBuf[2] = 0;
      ServerSdo[bServIndex].bExpedBuf[3] =
        (mTPDO[bPdoNr].bRemoteAllowed ? 0x0 : PDO_NO_RTR) | 
        (mTPDO[bPdoNr].bMode ? 0x0 : PDO_INVALID);
      break;

    case 2:
      ServerSdo[bServIndex].bExpedBuf[0] = TPDO_PARAM[bPdoNr].mTT;
      break;

    case 3:
      ServerSdo[bServIndex].bExpedBuf[0]
        = (BYTE)TPDO_PARAM[bPdoNr].wInhibitReload;
      ServerSdo[bServIndex].bExpedBuf[1]
        = (BYTE)(TPDO_PARAM[bPdoNr].wInhibitReload >> 8);
      break;

    case 5:
      ServerSdo[bServIndex].bExpedBuf[0]
        = (BYTE)TPDO_PARAM[bPdoNr].wEventReload;
      ServerSdo[bServIndex].bExpedBuf[1]
        = (BYTE)(TPDO_PARAM[bPdoNr].wEventReload >> 8);
      break;

    default:
      bRet = GEN_INT_PARA;
      break;
    } /* switch */
#endif /* MAX_TX_PDOS > 0 */
  } /* else if */
  else {
    switch (ptEntry->wbIndex.wc) {
#ifdef NO_UNIVERSAL_POINTERS
    case DEVICE_TYPE:
      ServerSdo[bServIndex].bExpedBuf[0] = (*((QBYTE CONST_PTR *)gConstData.mDeviceType)).fb.b0;
      ServerSdo[bServIndex].bExpedBuf[1] = (*((QBYTE CONST_PTR *)gConstData.mDeviceType)).fb.b1;
      ServerSdo[bServIndex].bExpedBuf[2] = (*((QBYTE CONST_PTR *)gConstData.mDeviceType)).fb.b2;
      ServerSdo[bServIndex].bExpedBuf[3] = (*((QBYTE CONST_PTR *)gConstData.mDeviceType)).fb.b3;
      break;
#endif /* NO_UNIVERSAL_POINTERS */

#if ENABLE_EMCYMSG == 1
    case ERROR_FIELD:
      if (ServerSdo[bServIndex].bSubIndex == 0) {
        ServerSdo[bServIndex].bExpedBuf[0] = EmcyTable.bNumber; /* number of actual errors available */
      } /* if */
      else {
        if (ServerSdo[bServIndex].bSubIndex <= EmcyTable.bNumber) {
          /* return accessed error code */
          ServerSdo[bServIndex].bExpedBuf[0] =
            EmcyTable.mErrInfo[ServerSdo[bServIndex].bSubIndex - 1].wbErrField.bc.lb;
          ServerSdo[bServIndex].bExpedBuf[1] =
            EmcyTable.mErrInfo[ServerSdo[bServIndex].bSubIndex - 1].wbErrField.bc.mb;
# if EMCY_MAN_SPEC == 1
          ServerSdo[bServIndex].bExpedBuf[2]
            = EmcyTable.mErrInfo[ServerSdo[bServIndex].bSubIndex - 1].bManSpec[0];
          ServerSdo[bServIndex].bExpedBuf[3]
            = EmcyTable.mErrInfo[ServerSdo[bServIndex].bSubIndex - 1].bManSpec[1];
# else
          ServerSdo[bServIndex].bExpedBuf[2] = 0;
          ServerSdo[bServIndex].bExpedBuf[3] = 0;
# endif /* EMCY_MAN_SPEC == 1 */
        } /* if */
        else if (ServerSdo[bServIndex].bSubIndex <= ERR_FIELDS) {
          ServerSdo[bServIndex].bExpedBuf[0] = 0;
          ServerSdo[bServIndex].bExpedBuf[1] = 0;
          ServerSdo[bServIndex].bExpedBuf[2] = 0;
          ServerSdo[bServIndex].bExpedBuf[3] = 0;
        } /* else if */
        else {
          bRet = NO_SUB_INDEX;
        } /* else */
      } /* else */
      break;
#endif /* ENABLE_EMCYMSG == 1 */

#if SYNC_USED == 1
    case COB_ID_SYNC:
      ServerSdo[bServIndex].bExpedBuf[0] = GET_ID(SYNC_RX, 0);
      ServerSdo[bServIndex].bExpedBuf[1] = GET_ID(SYNC_RX, 1);
      ServerSdo[bServIndex].bExpedBuf[2] = GET_ID(SYNC_RX, 2);
      ServerSdo[bServIndex].bExpedBuf[3] = GET_ID(SYNC_RX, 3);
# if SYNC_PRODUCER == 1
      if (tSyncPro.bRun == ON) {
        /* SYNC producer enabled */
        ServerSdo[bServIndex].bExpedBuf[3] |= 0x40;
      } /* if */
# endif /* SYNC_PRODUCER == 1 */
      break;

    case SYNC_CYCLE_PERIOD:
      ServerSdo[bServIndex].bExpedBuf[0] = gStkGlobDat.lComCyclePeriod.fb.b0;
      ServerSdo[bServIndex].bExpedBuf[1] = gStkGlobDat.lComCyclePeriod.fb.b1;
      ServerSdo[bServIndex].bExpedBuf[2] = gStkGlobDat.lComCyclePeriod.fb.b2;
      ServerSdo[bServIndex].bExpedBuf[3] = gStkGlobDat.lComCyclePeriod.fb.b3;
      break;

#endif /* SYNC_USED == 1 */

#if STORE_PARAMETERS_SUPP == 1
    case STORE_PARAMETERS:
      ServerSdo[bServIndex].bExpedBuf[0] = STORAGE_CAPABILITY;
      break;

    case RESTORE_PARAMETERS:
      ServerSdo[bServIndex].bExpedBuf[0] = 0x1; /* restore always possible */
      break;
#endif /* STORE_PARAMETERS_SUPP == 1 */

#if TIME_STAMP_USED == 1
      case COB_ID_TIME_STAMP:
      ServerSdo[bServIndex].bExpedBuf[0] = GET_ID(TIME_RX, 0);
      ServerSdo[bServIndex].bExpedBuf[1] = GET_ID(TIME_RX, 1);
      ServerSdo[bServIndex].bExpedBuf[2] = GET_ID(TIME_RX, 2);
      ServerSdo[bServIndex].bExpedBuf[3] = GET_ID(TIME_RX, 3);
      break;

    case TIME_STAMP:
      ServerSdo[bServIndex].bExpedBuf[0] = lHighResTime.fb.b0;
      ServerSdo[bServIndex].bExpedBuf[1] = lHighResTime.fb.b1;
      ServerSdo[bServIndex].bExpedBuf[2] = lHighResTime.fb.b2;
      ServerSdo[bServIndex].bExpedBuf[3] = lHighResTime.fb.b3;
      break;
#endif /* TIME_STAMP_USED == 1 */

#if ENABLE_EMCYMSG == 1
    case COB_ID_EMCY:
      ServerSdo[bServIndex].bExpedBuf[0] = GET_ID(EMCY_TX, 0);
      ServerSdo[bServIndex].bExpedBuf[1] = GET_ID(EMCY_TX, 1);
      ServerSdo[bServIndex].bExpedBuf[2] = GET_ID(EMCY_TX, 2);
      ServerSdo[bServIndex].bExpedBuf[3] = GET_ID(EMCY_TX, 3);
      break;

    case INHIBIT_TIME_EMCY:
      ServerSdo[bServIndex].bExpedBuf[0] = gStkGlobDat.mEmcyInhReloadTime.bc.lb;
      ServerSdo[bServIndex].bExpedBuf[1] = gStkGlobDat.mEmcyInhReloadTime.bc.mb;
      break;
#endif /* ENABLE_EMCYMSG == 1 */

#if HEARTBEAT_CONSUMER == 1
    case CON_HEARTBEAT_TIME:
# ifdef NO_UNIVERSAL_POINTERS
      if (ServerSdo[bServIndex].bSubIndex == 0) {
        ServerSdo[bServIndex].bExpedBuf[0] = gConstData.mHeartBeat[0];
      } /* if */
      else 
# endif /* NO_UNIVERSAL_POINTERS */
      {
        ServerSdo[bServIndex].bExpedBuf[0] =
          gStkGlobDat.atHeartbeatCon[ServerSdo[bServIndex].bSubIndex - 1].wReloadTime.bc.lb;
        ServerSdo[bServIndex].bExpedBuf[1] =
          gStkGlobDat.atHeartbeatCon[ServerSdo[bServIndex].bSubIndex - 1].wReloadTime.bc.mb;
        ServerSdo[bServIndex].bExpedBuf[2] =
          gStkGlobDat.atHeartbeatCon[ServerSdo[bServIndex].bSubIndex - 1].bNodeID;
        ServerSdo[bServIndex].bExpedBuf[3] = 0;
      } /* else */
      break;
#endif /* HEARTBEAT_CONSUMER == 1 */

#if ENABLE_NMT_ERROR_CTRL > 1
    case PRO_HEARTBEAT_TIME:
      ServerSdo[bServIndex].bExpedBuf[0] = gStkGlobDat.tHeartbeatPro.wReloadTime.bc.lb;
      ServerSdo[bServIndex].bExpedBuf[1] = gStkGlobDat.tHeartbeatPro.wReloadTime.bc.mb;
      break;
#endif /* ENABLE_NMT_ERROR_CTRL > 1 */

#ifdef NO_UNIVERSAL_POINTERS
    case IDENTITY_OBJECT:
      switch (ServerSdo[bServIndex].bSubIndex) {
        case 0:
          ServerSdo[bServIndex].bExpedBuf[0] = 4;
          break;
        default:
          ServerSdo[bServIndex].bExpedBuf[0] =
            ((BYTE CONST_PTR *)gConstData.mIdtyVendor)[(ServerSdo[bServIndex].bSubIndex - 1) * 4];
          ServerSdo[bServIndex].bExpedBuf[1] =
            ((BYTE CONST_PTR *)gConstData.mIdtyVendor)[1 + (ServerSdo[bServIndex].bSubIndex - 1) * 4];
          ServerSdo[bServIndex].bExpedBuf[2] =
            ((BYTE CONST_PTR *)gConstData.mIdtyVendor)[2 + (ServerSdo[bServIndex].bSubIndex - 1) * 4];
          ServerSdo[bServIndex].bExpedBuf[3] =
            ((BYTE CONST_PTR *)gConstData.mIdtyVendor)[3 + (ServerSdo[bServIndex].bSubIndex - 1) * 4];
          break;
      } /* switch */
      break;
 
# if OBJECT_1029_USED == 1
    case ERROR_BEHAVIOUR:
      ServerSdo[bServIndex].bExpedBuf[0] = gConstData.mErrClasses[0];
      break;
# endif /* OBJECT_1029_USED == 1 */
#endif /* NO_UNIVERSAL_POINTERS */

    case SDO_SERVER_PAR:
      switch (ServerSdo[bServIndex].bSubIndex) {
        case 0:
          ServerSdo[bServIndex].bExpedBuf[0] = 2;
          break;
        case 1:
          ServerSdo[bServIndex].bExpedBuf[0] = GET_ID(SDO_RX, 0);
          ServerSdo[bServIndex].bExpedBuf[1] = GET_ID(SDO_RX, 1);
          ServerSdo[bServIndex].bExpedBuf[2] = GET_ID(SDO_RX, 2);
          ServerSdo[bServIndex].bExpedBuf[3] = GET_ID(SDO_RX, 3);
          break;
        case 2:
          ServerSdo[bServIndex].bExpedBuf[0] = GET_ID(SDO_TX, 0);
          ServerSdo[bServIndex].bExpedBuf[1] = GET_ID(SDO_TX, 1);
          ServerSdo[bServIndex].bExpedBuf[2] = GET_ID(SDO_TX, 2);
          ServerSdo[bServIndex].bExpedBuf[3] = GET_ID(SDO_TX, 3);
          break;
        default:
          
          break;
      } /* switch */
      break;

#if MULTI_SDO_SERVER == 1
# if NUM_SDO_SERVERS == 2
    case SDO_SERVER_PAR + 1:
      switch (ServerSdo[bServIndex].bSubIndex) {
        case 0:
          ServerSdo[bServIndex].bExpedBuf[0] = 3;
          break;
        case 1:
          ServerSdo[bServIndex].bExpedBuf[0] = GET_ID(SDO_RX + 1, 0);
          ServerSdo[bServIndex].bExpedBuf[1] = GET_ID(SDO_RX + 1, 1);
          ServerSdo[bServIndex].bExpedBuf[2] = GET_ID(SDO_RX + 1, 2);
          ServerSdo[bServIndex].bExpedBuf[3] = ServerSdo[1].bRxIdEnabled
            ? GET_ID(SDO_RX + 1, 3) : (BYTE)(GET_ID(SDO_RX + 1, 3) | SDO_INVALID);
          break;
        case 2:
          ServerSdo[bServIndex].bExpedBuf[0] = GET_ID(SDO_TX + 1, 0);
          ServerSdo[bServIndex].bExpedBuf[1] = GET_ID(SDO_TX + 1, 1);
          ServerSdo[bServIndex].bExpedBuf[2] = GET_ID(SDO_TX + 1, 2);
          ServerSdo[bServIndex].bExpedBuf[3] = ServerSdo[1].bTxIdEnabled
            ? GET_ID(SDO_TX + 1, 3) : (BYTE)(GET_ID(SDO_TX + 1, 3) | SDO_INVALID);
          break;
        case 3:
          ServerSdo[bServIndex].bExpedBuf[0] = ServerSdo[1].bNodeIdClient;
          break;
        default:
          
          break;
      } /* switch */
      break;
# endif /* NUM_SDO_SERVERS == 2 */
#endif /* MULTI_SDO_SERVER == 1 */

#if STARTUP_AUTONOMOUSLY == 1
# ifdef NO_UNIVERSAL_POINTERS
  case NMT_STARTUP:
    ServerSdo[bServIndex].bExpedBuf[0] = 0x80;
    break;
# endif /* NO_UNIVERSAL_POINTERS */
#endif /* STARTUP_AUTONOMOUSLY == 1 */

    default:
#if DELAYED_SDO_ENABLE == 1
      /* check for object size max. 4 byte */
      if (ServerSdo[bServIndex].SDO_DATASIZE < 5) {
        /* perform delayed upload and save state */
        ServerSdo[bServIndex].bDelayedState = gSdoS_DelayedUpload(bServIndex);
        if (ServerSdo[bServIndex].bDelayedState == NO_SUB_INDEX) {
          bRet = NO_SUB_INDEX; /* wrong subindex */
        } /* if */
        else if (ServerSdo[bServIndex].bDelayedState == WR_ONLY_OBJ) {
          bRet = WR_ONLY_OBJ; /* write only object */
        } /* else if */
        else if (ServerSdo[bServIndex].bDelayedState == DELAY_ERROR) {
          bRet = GEN_INT_PARA; /* general error */
        } /* else if */
        else if ((ServerSdo[bServIndex].bDelayedState == DELAY_READ_REQ) ||
                 (ServerSdo[bServIndex].bDelayedState == DELAY_COMPLETE)) {
          /* OK: nothing to change */
        } /* else if */
        else
#endif /* DELAYED_SDO_ENABLE == 1 */
        {
      break;
        } /* else */
#if DELAYED_SDO_ENABLE == 1
      } /* if */
      else {
        /* object to large: delayed SDO access only supported up to 4 byte (expedited) */
        bRet = GEN_INT_PARA;
      } /* else */
#endif /* DELAYED_SDO_ENABLE == 1 */
    } /* switch */
  } /* else */
  return bRet;
} /* sSdoS_ReadData */


/*!
  \brief Create abort message

  This call creates the abort message.

  \param bServIndex - Index of used server
  \param bAbort - abort message code number
*/
void gSdoS_CreateAbort(BYTE bServIndex, BYTE bAbort)
{
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);
  /* command specifier */
  tTxMsg->bDb[0] = ABORT_DOMAIN;
  /* multiplexer */
  tTxMsg->bDb[1] = ServerSdo[bServIndex].wbIndex.bc.lb;
  tTxMsg->bDb[2] = ServerSdo[bServIndex].wbIndex.bc.mb;
  tTxMsg->bDb[3] = ServerSdo[bServIndex].bSubIndex;
  /* abort code */
#ifdef NO_UNIVERSAL_POINTERS
  tTxMsg->bDb[4] = (*((QBYTE CONST_PTR *)&alSdoAbortCodes[bAbort])).fb.b0;
  tTxMsg->bDb[5] = (*((QBYTE CONST_PTR *)&alSdoAbortCodes[bAbort])).fb.b1;
  tTxMsg->bDb[6] = (*((QBYTE CONST_PTR *)&alSdoAbortCodes[bAbort])).fb.b2;
  tTxMsg->bDb[7] = (*((QBYTE CONST_PTR *)&alSdoAbortCodes[bAbort])).fb.b3;
#else
  tTxMsg->bDb[4] = (*((QBYTE*)&alSdoAbortCodes[bAbort])).fb.b0;
  tTxMsg->bDb[5] = (*((QBYTE*)&alSdoAbortCodes[bAbort])).fb.b1;
  tTxMsg->bDb[6] = (*((QBYTE*)&alSdoAbortCodes[bAbort])).fb.b2;
  tTxMsg->bDb[7] = (*((QBYTE*)&alSdoAbortCodes[bAbort])).fb.b3;
#endif /* NO_UNIVERSAL_POINTERS */
} /* gSdoS_CreateAbort */


#if (SDO_WRITE_SEG_ALLOWED == 0) || (SDO_READ_SEG_ALLOWED == 0) || (SDO_BLOCK_ALLOWED == 0)
/*!
  \brief Create the abort message for unimplemented access.

  If the module supports only expedited SDO transfers
  this call creates the answer to a segmented request.

  \param bServIndex - Index of used server
  \retval TRUE - This is always successful.
*/
STATIC BYTE sSdoS_ProtoRestricted(BYTE bServIndex)
{
  CAN_MSG XDATA *tRxMsg;

  /* message access */
  tRxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bRxId);
  /* store index/sub index */
  ServerSdo[bServIndex].wbIndex.bc.lb = tRxMsg->bDb[1];
  ServerSdo[bServIndex].wbIndex.bc.mb = tRxMsg->bDb[2];
  ServerSdo[bServIndex].bSubIndex = tRxMsg->bDb[3];
  gSdoS_CreateAbort(bServIndex, UNKNOWN_CS);
  gSdoS_ClearServerInfo(bServIndex);
  return TRUE;
} /* sSdoS_ProtoRestricted */
#endif /* (SDO_WRITE_SEG_ALLOWED == 0) || (SDO_READ_SEG_ALLOWED == 0) || (SDO_BLOCK_ALLOWED == 0) */


/*!
  \brief Initiate SDO download indication

  This routine handles the indication of a SDO download request.
  The response message is created.

  \param bServIndex - Index in array ServerSdo.
  \retval TRUE - Service could be initiated - in case of an expedited
  access the data are already transferred.
  \retval FALSE - The request has been failed.
*/
STATIC BYTE sSdoS_InitDown(BYTE bServIndex)
{
  CONST OD_ENTRY CONST_PTR *ptEntry;
  BYTE bExpedited = TRUE;
  BYTE DATA bRet = 0;
  CAN_MSG XDATA *tRxMsg;
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tRxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bRxId);
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);

  if (ServerSdo[bServIndex].bLastState == ST_TRANSFERRED) {
    gSdoS_ClearServerInfo(bServIndex);
    /* store index/sub index */
    ServerSdo[bServIndex].wbIndex.bc.lb = tRxMsg->bDb[1];
    ServerSdo[bServIndex].wbIndex.bc.mb = tRxMsg->bDb[2];
    ServerSdo[bServIndex].bSubIndex = tRxMsg->bDb[3];
    /*
    Even for an expedited transfer we have to provide
    a time out value.
    Otherwise a time out condition would be recognised if the
    time out check function runs in another task under an operating
    system.
    */
    ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
    ServerSdo[bServIndex].bLastState = ST_WRITE_REQ;
    /* multiplexer */
    tTxMsg->bDb[1] = tRxMsg->bDb[1];
    tTxMsg->bDb[2] = tRxMsg->bDb[2];
    tTxMsg->bDb[3] = tRxMsg->bDb[3];

    /* search for existence of entry in OD */
    ptEntry = gSdoS_GetEntry(
      (WORD)(ServerSdo[bServIndex].wbIndex.wc - sSdoS_GetPdoNumber(bServIndex)),
      ServerSdo[bServIndex].bSubIndex);

    if (ptEntry != 0) {
      /* check write bit */
      if ((ptEntry->AccArrayClbck & WO) == WO) {
        /* set command specifier */
        tTxMsg->bDb[0] = SCS_INIT_DOWNLOAD;
        switch (tRxMsg->bDb[0] & 0x03) { /* mask bit 1-0 */
#if SDO_WRITE_SEG_ALLOWED == 1
          case 0:
            /* normal transfer, no data size indicated */
            ServerSdo[bServIndex].SDO_DATASIZE
              = gSdoS_GetObjLenCb(ServerSdo[bServIndex].wbIndex.wc,
                ServerSdo[bServIndex].bSubIndex);
            bExpedited = FALSE;
            break;

          case 1:
            /* normal transfer, data size indicated in bytes 4-7 */
            ServerSdo[bServIndex].qwDataSize.fb.b0 = tRxMsg->bDb[4];
            ServerSdo[bServIndex].qwDataSize.fb.b1 = tRxMsg->bDb[5];
            ServerSdo[bServIndex].qwDataSize.fb.b2 = tRxMsg->bDb[6];
            ServerSdo[bServIndex].qwDataSize.fb.b3 = tRxMsg->bDb[7];
            bExpedited = FALSE;
            break;
#endif /* SDO_WRITE_SEG_ALLOWED == 1 */

          case 2:
            /* expedited transfer, no data size indicated */
            if ((ptEntry->Length >= 1) && (ptEntry->Length <= 4)) {
              /* get length from dictionary */
              ServerSdo[bServIndex].SDO_DATASIZE = ptEntry->Length;
            } /* if */
            else {
              /* assuming 4 byte */
              ServerSdo[bServIndex].SDO_DATASIZE = 4;
            } /* else */
            break;

          case 3:
            /* expedited, data size indicated in bit 3-2 */
            ServerSdo[bServIndex].SDO_DATASIZE
              = (SDO_DATASIZE_CAST)(4 - ((tRxMsg->bDb[0] & 0x0C) >> 2));
            break;

          default:
            bRet = UNKNOWN_CS;
            break;
        } /* switch */

        if (bRet == 0) {
          /* check length information */
          if (ptEntry->Length == 0) {
            if (ServerSdo[bServIndex].SDO_DATASIZE 
                  > gSdoS_GetObjLenCb(ServerSdo[bServIndex].wbIndex.wc,
                                       ServerSdo[bServIndex].bSubIndex)) {
              VDEBUG_RPTE2("Sdo: Too much data for %x %x!\n",
              ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
              bRet = LEN_TOO_HIGH;
            } /* if */
          } /* if */
          else {
            if (ServerSdo[bServIndex].SDO_DATASIZE > (DWORD)ptEntry->Length) {
              VDEBUG_RPTE2("Sdo: Too much data for %x %x!\n",
              ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
              bRet = LEN_TOO_HIGH;
            } /* if */
            if (ServerSdo[bServIndex].SDO_DATASIZE < (DWORD)ptEntry->Length) {
              VDEBUG_RPTE2("Sdo: Too less data for %x %x!\n",
              ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
              bRet = LEN_TOO_LOW;
            } /* if */
          } /* else */
          /* check temp buffer size */
          if ((bRet == 0) && (ServerSdo[bServIndex].SDO_DATASIZE > SDO_DATA_BUFFER_SIZE)) {
            VDEBUG_RPTE2("Sdo: Data buffer too small for %x %x!\n",
            ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
            bRet = OUT_OF_MEM;
          } /* if */
        } /* if */
  
        if (bRet == 0) {
          if (bExpedited == TRUE) {
            /*
              The answer is created as expedited response.
            */
#if SDO_EXEC_USER_WRITE_CALLBACKS == 1
            /* server SDO callback function: check execution tag for this object */
            if (ptEntry->AccArrayClbck & SDO_EXEC_CB_DOWN) {
              /* we always copy the maximum of 4 bytes */
              (*((QBYTE*)ServerSdo[bServIndex].abSdoDataTmpBuffer)).fb.b0 = tRxMsg->bDb[4];
              (*((QBYTE*)ServerSdo[bServIndex].abSdoDataTmpBuffer)).fb.b1 = tRxMsg->bDb[5];
              (*((QBYTE*)ServerSdo[bServIndex].abSdoDataTmpBuffer)).fb.b2 = tRxMsg->bDb[6];
              (*((QBYTE*)ServerSdo[bServIndex].abSdoDataTmpBuffer)).fb.b3 = tRxMsg->bDb[7];
              /* branch to callback funtion */
              bRet = gSdoS_ExpDownCb(&ServerSdo[bServIndex]);
            } /* if */

            if (bRet == 0)
#endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */
            {
              if (ptEntry->pData == 0) {
                sSdoS_GetObjectAddress(bServIndex);
                /*
                  Copy the message contents directly to the 
                  expedited buffer which is guaranteed to be
                  4 bytes in length.
                  Data item is in intel format in message buffer.
                */
                ServerSdo[bServIndex].bExpedBuf[0] = tRxMsg->bDb[4];
                ServerSdo[bServIndex].bExpedBuf[1] = tRxMsg->bDb[5];
                ServerSdo[bServIndex].bExpedBuf[2] = tRxMsg->bDb[6];
                ServerSdo[bServIndex].bExpedBuf[3] = tRxMsg->bDb[7];

                /*
                  During the call of sSdoS_WriteData()
                  the data contents from the incoming message are
                  copied to the final location.
                */
                bRet = sSdoS_WriteData(ptEntry, bServIndex);
                if (bRet != 0) {
                  VDEBUG_RPTE2("Sdo: No data address available for  %x %x!\n",
                  ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
                } /* if */
              } /* if */
              else {
                /*
                  The direct access to the data item is allowed.
                  So we have to gather the address.
                */
                if ((ptEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
                  ServerSdo[bServIndex].pbData =
                    ptEntry->pData
                    + (ServerSdo[bServIndex].bSubIndex - 1)
                    * (ServerSdo[bServIndex].SDO_DATASIZE);
                } /* if */
                else {
                  ServerSdo[bServIndex].pbData = ptEntry->pData;
                } /* else */
                LOCK_APPLICATION_WDATA;
              } /* else */
            } /* if/block */

            if 
            ( (bRet == 0)
#if DELAYED_SDO_ENABLE == 1
              /* do not copy received data again for delayed data access */
              && (ServerSdo[bServIndex].bDelayedState != DELAY_WRITE_REQ)
              && (ServerSdo[bServIndex].bDelayedState != DELAY_COMPLETE )
#endif /* DELAYED_SDO_ENABLE == 1 */
            )
            {
              /* copy data if not already done */
              if (ptEntry->pData != 0) {
                switch ((BYTE)ServerSdo[bServIndex].SDO_DATASIZE) {
                  case 1:
                    ServerSdo[bServIndex].pbData[0] = tRxMsg->bDb[4];
                    break;

                  case 2:
                    (*((WBYTE*)ServerSdo[bServIndex].pbData)).bc.lb = tRxMsg->bDb[4];
                    (*((WBYTE*)ServerSdo[bServIndex].pbData)).bc.mb = tRxMsg->bDb[5];
                    break;

                  case 3:
                    (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b0 = tRxMsg->bDb[4];
                    (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b1 = tRxMsg->bDb[5];
                    (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b2 = tRxMsg->bDb[6];
                    break;

                  case 4:
                    (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b0 = tRxMsg->bDb[4];
                    (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b1 = tRxMsg->bDb[5];
                    (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b2 = tRxMsg->bDb[6];
                    (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b3 = tRxMsg->bDb[7];
                    break;
                  default:
                    
                    break;
                } /* switch */
                UNLOCK_APPLICATION_WDATA;
              } /* if */
#if GENERIC_PROFILE_USED == 1
              PROFILE_SDO_DOWN(ServerSdo[bServIndex].wbIndex.wc,
                               ServerSdo[bServIndex].bSubIndex);
#endif /* GENERIC_PROFILE_USED == 1 */
#if SDO_EXEC_USER_WRITE_CALLBACKS == 1
              /* server SDO callback function: check execution tag for this object */
              if (ptEntry->AccArrayClbck & SDO_EXEC_CB_DOWN) {
                /* write access finished, data are written to final location */
                /* branch to callback funtion */
                gSdoS_ExpDownCbFinish(&ServerSdo[bServIndex]);
              } /* if */
#endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */
            } /* if */
          } /* if expedited */
#if SDO_WRITE_SEG_ALLOWED == 1                                                              
          else {
            /* The answer is created as segmented response. */
            if (ptEntry->pData == 0) {
              sSdoS_GetObjectAddress(bServIndex);
              /* Get the pointer to the data to be used during subsequent */
              /* requests.                                                */
              if (ServerSdo[bServIndex].SDO_DATASIZE <= 4) {
                /* The indirect access to the data item is allowed. */
                ServerSdo[bServIndex].bIndirect = TRUE;
                ServerSdo[bServIndex].ptEntry = ptEntry;
              } /* if */
              else {
                bRet = sSdoS_WriteData(ptEntry, bServIndex);
                if (bRet != 0) {
                  VDEBUG_RPTE2("Sdo: No data address available for  %x %x!\n",
                  ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
                } /* if */
              } /* else */

              if (ServerSdo[bServIndex].pbData == 0) {
                bRet = GEN_INT_PARA;
              } /* if */
            } /* if */
            else {
              /* The direct access to the data item is allowed. */
              /* So we have to gather the address.              */
              ServerSdo[bServIndex].bIndirect = FALSE;
              if ((ptEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
                ServerSdo[bServIndex].pbData =
                  ptEntry->pData
                  + (ServerSdo[bServIndex].bSubIndex - 1)
                  * (ptEntry->Length);
              } /* if */
              else {
                ServerSdo[bServIndex].pbData = ptEntry->pData;
              } /* else */
            } /* else */
# if SDO_EXEC_USER_WRITE_CALLBACKS == 1
            /* server SDO callback function: check execution tag for this object */
            if (ptEntry->AccArrayClbck & SDO_EXEC_CB_DOWN) {
              ServerSdo[bServIndex].bCallbkAttr = ptEntry->AccArrayClbck;
              /* branch to callback funtion */
              bRet = gSdoS_DownCb(&ServerSdo[bServIndex]);
            } /* if */
# endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */
          } /* else */
#else /* SDO_WRITE_SEG_ALLOWED == 1 */
          else {
            bRet = LEN_TOO_HIGH;
          } /* else */
#endif /* SDO_WRITE_SEG_ALLOWED == 1 */
        } /* if write access allowed */
      } /* if */
      else {
        bRet = RD_ONLY_OBJ;
      } /* else */
    } /* if */
    else {
#if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
      /* set command specifier */
      tTxMsg->bDb[0] = SCS_INIT_DOWNLOAD;
      switch (tRxMsg->bDb[0] & 0x03) { /* mask bit 1-0 */
        case 0:
        case 1:
          /* only expedited transfer supported -> abort */
          bRet = GEN_PARA_INC;
        break;

        case 2:
          /* expedited transfer, no data size indicated */
          /* assuming 4 byte */
          ServerSdo[bServIndex].SDO_DATASIZE = 4;
        break;

        case 3:
          /* expedited, data size indicated in bit 3-2 */
          ServerSdo[bServIndex].SDO_DATASIZE =
          (SDO_DATASIZE_CAST)(4 - ((tRxMsg->bDb[0] & 0x0C) >> 2));
        break;

        default:
          bRet = UNKNOWN_CS;
        break;
      } /* switch */

      if (bRet == 0) {
# if SDO_EXEC_USER_WRITE_CALLBACKS == 1
        /* server SDO callback function: check execution tag for this object */
        if (ptEntry->AccArrayClbck & SDO_EXEC_CB_DOWN) {
          /* we always copy the maximum of 4 bytes */
          (*((QBYTE*)ServerSdo[bServIndex].abSdoDataTmpBuffer)).fb.b0 = tRxMsg->bDb[4];
          (*((QBYTE*)ServerSdo[bServIndex].abSdoDataTmpBuffer)).fb.b1 = tRxMsg->bDb[5];
          (*((QBYTE*)ServerSdo[bServIndex].abSdoDataTmpBuffer)).fb.b2 = tRxMsg->bDb[6];
          (*((QBYTE*)ServerSdo[bServIndex].abSdoDataTmpBuffer)).fb.b3 = tRxMsg->bDb[7];
          /* branch to callback funtion */
          bRet = gSdoS_ExpDownCb(&ServerSdo[bServIndex]);
        } /* if */

        if (bRet == 0)
# endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */
        {
          /*
            Copy the message contents directly to the 
            expedited buffer.
            Data item is in intel format in message buffer.
          */
          ServerSdo[bServIndex].bExpedBuf[0] = tRxMsg->bDb[4];
          ServerSdo[bServIndex].bExpedBuf[1] = tRxMsg->bDb[5];
          ServerSdo[bServIndex].bExpedBuf[2] = tRxMsg->bDb[6];
          ServerSdo[bServIndex].bExpedBuf[3] = tRxMsg->bDb[7];
          ServerSdo[bServIndex].pbData =
            (BYTE *)&ServerSdo[bServIndex].bExpedBuf[0];

# if DELAYED_SDO_ENABLE == 1
          ServerSdo[bServIndex].bDelayedState = gSdoS_DelayedDownload(bServIndex);
          if (ServerSdo[bServIndex].bDelayedState == DELAY_ERROR) {
            bRet = GEN_INT_PARA;
          } /* if */
          else if (ServerSdo[bServIndex].bDelayedState > DELAY_ERROR) {
            bRet = ServerSdo[bServIndex].bDelayedState;
          } /* else if */
          else {
            /* set new time out */
            ServerSdo[bServIndex].wTimeOut = DELAYED_SDO_TIMEOUT;
          } /* else */
# endif /* DELAYED_SDO_ENABLE == 1 */
        }
      } /* if */
#else
      /* subindex does not exist */
      /* search for existence of index in OD */
      if (gSdoS_GetEntry((WORD)(ServerSdo[bServIndex].wbIndex.wc
          - sSdoS_GetPdoNumber(bServIndex)), 0) != 0) {
        bRet = NO_SUB_INDEX;
      }
      else {
        bRet = OBJ_NOT_EXIST;
      } /* else */
#endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */
    } /* else */
    /* allocate transfer structure */
  } /* if */
  else {
    bRet = UNKNOWN_CS;
  } /* else */

  if (bRet != 0) {
    gSdoS_CreateAbort(bServIndex, bRet);
    gSdoS_ClearServerInfo(bServIndex);
  } /* if */
  else {
    tTxMsg->bDb[4] = 0;
    tTxMsg->bDb[5] = 0;
    tTxMsg->bDb[6] = 0;
    tTxMsg->bDb[7] = 0;
    /* transfer completed */
    if (bExpedited == TRUE) {
#if DELAYED_SDO_ENABLE == 1
      /* for delayed data access the data is not written at this time */
      /* and so the transfer is not yet completed */
      if (ServerSdo[bServIndex].bDelayedState == DELAY_WRITE_REQ) {
        /* the SDO server information is still needed for the reply */
        /* set new time out */
        ServerSdo[bServIndex].wTimeOut = DELAYED_SDO_TIMEOUT;
      } /* if */
      else
#endif /* DELAYED_SDO_ENABLE == 1 */
      {
        gSdoS_ClearServerInfo(bServIndex);
      }
    } /* if */
  } /* else */
  return (bRet == 0) ? TRUE : FALSE;
} /* sSdoS_InitDown */


#if SDO_WRITE_SEG_ALLOWED == 1
/*!
  \brief SDO download segment indication

  This routine handles the indication of a SDO download segment request.
  The response message is created.

  \param bServIndex - Index in array ServerSdo.
  \retval TRUE - Segment could be evaluated.
  \retval FALSE - The request has been failed.
*/
STATIC BYTE sSdoS_DownNext(BYTE bServIndex)
{
  BYTE bValidBytes;
  BYTE DATA bRet = 0;
  CAN_MSG XDATA *tRxMsg;
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tRxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bRxId);
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);

  if (ServerSdo[bServIndex].bLastState != ST_WRITE_REQ) {
    bRet = UNKNOWN_CS;
  } /* if */

  if ((tRxMsg->bDb[0] & 0x10) != ServerSdo[bServIndex].bDataToggle) {
    bRet = TOG_NOT_ALTERED;
  } /* if */

  if (bRet == 0) {
    bValidBytes = (BYTE)(7 - ((tRxMsg->bDb[0] & 0x0E) >> 1));

    if (ServerSdo[bServIndex].SDO_DATASIZE
      >= (ServerSdo[bServIndex].DataCounter + bValidBytes)) {
      /* OK segment accepted - copy bytes from received message to temp buffer */
      MemCpy(&ServerSdo[bServIndex].abSdoDataTmpBuffer[ServerSdo[bServIndex].DataCounter],
        &tRxMsg->bDb[1], bValidBytes); 

      /* set command specifier */
      tTxMsg->bDb[0] = (BYTE)(SCS_SEGMENT_DOWNLOAD
        | ServerSdo[bServIndex].bDataToggle); /* toggle bit */
      /* old data must be overwritten in tx buffer */
      MemSet(&tTxMsg->bDb[1], 0x00, 7);
      ServerSdo[bServIndex].DataCounter += bValidBytes;
      /* check for indirect object access */
      if (ServerSdo[bServIndex].bIndirect == TRUE) {
        ServerSdo[bServIndex].bIndirect = FALSE;
        bRet = sSdoS_WriteData(ServerSdo[bServIndex].ptEntry, bServIndex);
        if (bRet != 0) {
          VDEBUG_RPTE2("Sdo: No data address available for  %x %x!\n",
          ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
        } /* if */
      } /* if */
      /* end the transfer if completion bit is set */
      if ((bRet == 0) && ((tRxMsg->bDb[0] & 0x01) == 0x01)) {
#if SDO_EXEC_USER_WRITE_CALLBACKS == 1
        /* server SDO callback function: check execution tag for this object */
        if (ServerSdo[bServIndex].bCallbkAttr & SDO_EXEC_CB_DOWN) {
          /* branch to callback funtion */
          bRet = gSdoS_DownCbFinish(&ServerSdo[bServIndex]);
        } /* if */
        if (bRet == 0)
#endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */
        {
          LOCK_APPLICATION_WDATA;
          /* transfer completed - copy all data from temp buffer to object storage location */
          MemCpy(ServerSdo[bServIndex].pbData,
            ServerSdo[bServIndex].abSdoDataTmpBuffer,
            ServerSdo[bServIndex].SDO_DATASIZE); 
          UNLOCK_APPLICATION_WDATA;
# if GENERIC_PROFILE_USED == 1
          PROFILE_SDO_DOWN(ServerSdo[bServIndex].wbIndex.wc,
                           ServerSdo[bServIndex].bSubIndex);
# endif /* GENERIC_PROFILE_USED == 1 */
          gSdoS_ClearServerInfo(bServIndex);
        } /* if */
      } /* if */
      else {
        /* set new time out */
        ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
      } /* else */
    } /* if */
    else {
      bRet = OUT_OF_MEM;
    } /* else */
    /* answer created - switch toggle now */
    ServerSdo[bServIndex].bDataToggle = (BYTE)((~tRxMsg->bDb[0]) & 0x10);
  } /* if */

  if (bRet != 0) {
    gSdoS_CreateAbort(bServIndex, bRet);
    gSdoS_ClearServerInfo(bServIndex);
  } /* if */
  return (bRet == 0) ? TRUE : FALSE;
} /* sSdoS_DownNext */
#endif /* SDO_WRITE_SEG_ALLOWED == 1 */


/*!
  \brief Initiate SDO upload indication

  This routine handles the indication of a SDO upload request.
  The response message is created.

  \param bServIndex - Index in array ServerSdo.
  \retval TRUE - Service could be initiated - in case of an expedited
  access the data are already transferred to the CAN message buffer
  \retval FALSE - The request has been failed.
*/
STATIC BYTE sSdoS_InitUp(BYTE bServIndex)
{
  CONST OD_ENTRY CONST_PTR *ptEntry;
  BYTE DATA bRet = 0;
  CAN_MSG XDATA *tRxMsg;
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tRxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bRxId);
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);

  if (ServerSdo[bServIndex].bLastState == ST_TRANSFERRED) {
    gSdoS_ClearServerInfo(bServIndex);
    /* store index/sub index */
    ServerSdo[bServIndex].wbIndex.bc.lb = tRxMsg->bDb[1];
    ServerSdo[bServIndex].wbIndex.bc.mb = tRxMsg->bDb[2];
    ServerSdo[bServIndex].bSubIndex = tRxMsg->bDb[3];

    /*
    Even for an expedited transfer we have to provide
    a time out value.
    Otherwise a time out condition would be recognised if the
    time out check function runs in another task under an operating
    system.
    */
    ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
    ServerSdo[bServIndex].bLastState = ST_READ_REQ;
    /* copy index subindex to answer */
    tTxMsg->bDb[1] = tRxMsg->bDb[1];
    tTxMsg->bDb[2] = tRxMsg->bDb[2];
    tTxMsg->bDb[3] = tRxMsg->bDb[3];

    ptEntry = gSdoS_GetEntry((WORD)(ServerSdo[bServIndex].wbIndex.wc
                             - sSdoS_GetPdoNumber(bServIndex)),
                             ServerSdo[bServIndex].bSubIndex);
    /* search for existence of entry in OD - and for write bit */
    if (ptEntry != 0) {
      if ((ptEntry->AccArrayClbck & RO) == RO) {
        /*
          Now we need the data location and the length of the item
          in order to answer the request.
        */

        /* check length information */
        if (ptEntry->Length == 0) {
          ServerSdo[bServIndex].SDO_DATASIZE
          = gSdoS_GetObjLenCb(ServerSdo[bServIndex].wbIndex.wc,
            ServerSdo[bServIndex].bSubIndex);
          if (ServerSdo[bServIndex].SDO_DATASIZE == 0) {
            /*
              This is a configuration problem.
              If we come to this point either the entry in OBJDICT.C is
              wrong or we forgot the entry in the function
              sSdo_S_GetObjLen().
              So complain and leave here.
            */
            VDEBUG_RPTE2("Sdo: No length information for entry %x %x!\n",
            ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
            bRet = GEN_PARA_INC;
          } /* if */
        } /* if */
        else {
          ServerSdo[bServIndex].SDO_DATASIZE = ptEntry->Length;
        } /* else */

#if SDO_EXEC_USER_READ_CALLBACKS == 1
        /* server SDO callback function: check execution tag for this object */
        if ((bRet == 0) && (ptEntry->AccArrayClbck & SDO_EXEC_CB_UP)) {
          /* branch to callback funtion */
          bRet = gSdoS_UpCb(&ServerSdo[bServIndex]);
        } /* if */
#endif /* SDO_EXEC_USER_READ_CALLBACKS == 1 */

        /* create answer according to data length */
        if ((ServerSdo[bServIndex].SDO_DATASIZE > 0)
          && (ServerSdo[bServIndex].SDO_DATASIZE < 5)) {
          /* expedited transfer */
#if SDO_EXEC_USER_READ_CALLBACKS == 1
          if (bRet == 0)
#endif /* SDO_EXEC_USER_READ_CALLBACKS == 1 */
          {
            /* create expedited command specifier */
            tTxMsg->bDb[0] = (BYTE)(SCS_INIT_UPLOAD | 0x03
              | (BYTE)((4 - ServerSdo[bServIndex].SDO_DATASIZE) << 2));
            /* fill in data */
            if (ptEntry->pData == 0) {
              sSdoS_GetObjectAddress(bServIndex);
              /*
                The data contents are copied to the expedited buffer in the
                structure VSdoAttrib during the call to sSdoS_ReadData().
                Additionally the pointer to the data are adjusted.
              */
              bRet = sSdoS_ReadData(ptEntry, bServIndex);
              if (bRet != 0) {
                VDEBUG_RPTE2("Sdo: No address information for entry %x %x!\n",
                ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
              } /* if */
              else {
#if DELAYED_SDO_ENABLE == 1
                /* for special data acquisition the data is not available here: skip copying */
                if ((ServerSdo[bServIndex].bDelayedState != DELAY_READ_REQ) &&
                    (ServerSdo[bServIndex].bDelayedState != DELAY_COMPLETE)) {
#endif /* DELAYED_SDO_ENABLE == 1 */
                  /* Copy the expedited data directly to the message */
                  tTxMsg->bDb[4] = ServerSdo[bServIndex].bExpedBuf[0];
                  tTxMsg->bDb[5] = ServerSdo[bServIndex].bExpedBuf[1];
                  tTxMsg->bDb[6] = ServerSdo[bServIndex].bExpedBuf[2];
                  tTxMsg->bDb[7] = ServerSdo[bServIndex].bExpedBuf[3];
#if DELAYED_SDO_ENABLE == 1
                } /* if */
                else {
                  /* set new time out */
                  ServerSdo[bServIndex].wTimeOut = DELAYED_SDO_TIMEOUT;
                } /* else */
#endif /* DELAYED_SDO_ENABLE == 1 */
              } /* else */
            } /* if */
            else {
              /*
                Get EXPEDITED data directly via address provided
                in OBJDICT.C.
                The data address has to be corrected if we access an array.
              */
              if ((ptEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
#if MACHINE_IS_16BIT_ONLY == 1
                switch ((BYTE)ServerSdo[bServIndex].SDO_DATASIZE) {
                  case 1:
                  case 2:
                    ServerSdo[bServIndex].pbData =
                      ptEntry->pData + (ServerSdo[bServIndex].bSubIndex - 1);
                    break;

                  case 3:
                  case 4:
                    ServerSdo[bServIndex].pbData =
                      ptEntry->pData + (ServerSdo[bServIndex].bSubIndex - 1) * 2;
                    break;
                } /* switch */
#else
                ServerSdo[bServIndex].pbData =
                  ptEntry->pData
                  + (ServerSdo[bServIndex].bSubIndex - 1)
                  * (ServerSdo[bServIndex].SDO_DATASIZE);
#endif /* MACHINE_IS_16BIT_ONLY == 1 */
              } /* if */
              else {
                ServerSdo[bServIndex].pbData = ptEntry->pData;
              } /* else */
              LOCK_APPLICATION_RDATA;
              switch ((BYTE)ServerSdo[bServIndex].SDO_DATASIZE) {
                case 1:
                  tTxMsg->bDb[4] = ServerSdo[bServIndex].pbData[0];
                  tTxMsg->bDb[5] = 0;
                  tTxMsg->bDb[6] = 0;
                  tTxMsg->bDb[7] = 0;
                  break;

                case 2:
                  tTxMsg->bDb[4] = (*((WBYTE*)ServerSdo[bServIndex].pbData)).bc.lb;
                  tTxMsg->bDb[5] = (*((WBYTE*)ServerSdo[bServIndex].pbData)).bc.mb;
                  tTxMsg->bDb[6] = 0;
                  tTxMsg->bDb[7] = 0;
                  break;

                case 3:
                  tTxMsg->bDb[4] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b0;
                  tTxMsg->bDb[5] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b1;
                  tTxMsg->bDb[6] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b2;
                  tTxMsg->bDb[7] = 0;
                  break;

                case 4:
                  tTxMsg->bDb[4] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b0;
                  tTxMsg->bDb[5] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b1;
                  tTxMsg->bDb[6] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b2;
                  tTxMsg->bDb[7] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b3;
                  break;
                default:
                  
                  break;
              } /* switch */
              UNLOCK_APPLICATION_RDATA;
            } /* else */
          }
          /* transfer completed successfully? */
          if (bRet == 0) {
#if DELAYED_SDO_ENABLE == 1
            /* for delayed data acquisition the transfer is not yet completed: */
            /* the SDO server information is still needed for the reply */
            if ((ServerSdo[bServIndex].bDelayedState != DELAY_READ_REQ) &&
                (ServerSdo[bServIndex].bDelayedState != DELAY_COMPLETE))
#endif /* DELAYED_SDO_ENABLE == 1 */
            {
#if GENERIC_PROFILE_USED == 1
              PROFILE_SDO_UP(ServerSdo[bServIndex].wbIndex.wc,
                             ServerSdo[bServIndex].bSubIndex);
#endif /* GENERIC_PROFILE_USED == 1 */
              gSdoS_ClearServerInfo(bServIndex);
            } /* if */
          } /* if */
        } /* if */
        else {
#if SDO_READ_SEG_ALLOWED == 1
          /* set command specifier */
          tTxMsg->bDb[0] = SCS_INIT_UPLOAD | 0x01;

          /* Get the address of the data item. */
          if (ptEntry->pData == 0) {
            sSdoS_GetObjectAddress(bServIndex);
            /*
              The data contents are copied to the message itself directly
              during the call to sSdoS_ReadData().
            */
            bRet = sSdoS_ReadData(ptEntry, bServIndex);
            if (bRet != 0) {
              VDEBUG_RPTE2("Sdo: No address information for entry %x %x!\n",
              ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
            } /* if */
          } /* if */
          else {
            /*
              Get data directly via address provided in OBJDICT.C.
              The data address has to be corrected if we access an array.
            */
            if ((ptEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
                ServerSdo[bServIndex].pbData = ptEntry->pData
                  + (ServerSdo[bServIndex].bSubIndex - 1)
                  * (ServerSdo[bServIndex].SDO_DATASIZE);
            } /* if */
            else {
              ServerSdo[bServIndex].pbData = ptEntry->pData;
            } /* else */
          } /* else */

          if (bRet == 0) {
            /* return length */
            tTxMsg->bDb[4] = ServerSdo[bServIndex].qwDataSize.fb.b0;
            tTxMsg->bDb[5] = ServerSdo[bServIndex].qwDataSize.fb.b1;
            tTxMsg->bDb[6] = ServerSdo[bServIndex].qwDataSize.fb.b2;
            tTxMsg->bDb[7] = ServerSdo[bServIndex].qwDataSize.fb.b3;
          } /* if */
#else
          bRet = GEN_INT_PARA;
#endif  /* else SDO_READ_SEG_ALLOWED == 1 */
        } /* else */
      } /* if */
      else {
        bRet = WR_ONLY_OBJ;
      } /* else */
    } /* if */
    else {
#if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
# if SDO_EXEC_USER_READ_CALLBACKS == 1
      /* server SDO callback function: check execution tag for this object */
      if ((bRet == 0) && (ptEntry->AccArrayClbck & SDO_EXEC_CB_UP)) {
        /* branch to callback funtion */
        bRet = gSdoS_UpCb(&ServerSdo[bServIndex]);
      } /* if */

      if (bRet == 0)
# endif /* SDO_EXEC_USER_READ_CALLBACKS == 1 */
      {
# if DELAYED_SDO_ENABLE == 1
        ServerSdo[bServIndex].bDelayedState = gSdoS_DelayedUpload(bServIndex);
        if (ServerSdo[bServIndex].bDelayedState == DELAY_ERROR) {
          bRet = GEN_INT_PARA;
        } /* if */
        else if (ServerSdo[bServIndex].bDelayedState > DELAY_ERROR) {
            bRet = ServerSdo[bServIndex].bDelayedState;
        } /* else if */
        else {
          /* set new time out */
          ServerSdo[bServIndex].wTimeOut = DELAYED_SDO_TIMEOUT;
        } /* else */
# endif /* DELAYED_SDO_ENABLE == 1 */
      }
#else /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */
      /* subindex does not exist */
      /* search for existence of index in OD */
      if (gSdoS_GetEntry((WORD)(ServerSdo[bServIndex].wbIndex.wc
          - sSdoS_GetPdoNumber(bServIndex)), 0) != 0) {
        bRet = NO_SUB_INDEX;
      }
      else {
        bRet = OBJ_NOT_EXIST;
      } /* else */
#endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */
    } /* else */
    /* allocate transfer structure */
  } /* if */
  else {
    bRet = UNKNOWN_CS;
  } /* else */

  if (bRet != 0) {
    gSdoS_CreateAbort(bServIndex, bRet);
    gSdoS_ClearServerInfo(bServIndex);
  } /* if */
  return (bRet == 0) ? TRUE : FALSE;
} /* sSdoS_InitUp */


#if SDO_READ_SEG_ALLOWED == 1
/*!
  \brief SDO upload segment indication

  This routine handles the indication of a SDO upload segment request.
  The response message is created.

  \param bServIndex - Index in array #ServerSdo.
  \retval TRUE - Segment could be evaluated.
  \retval FALSE - The request has been failed.
*/
STATIC BYTE sSdoS_UpNext(BYTE bServIndex)
{
  BYTE bRestBytes;
  BYTE DATA bRet = 0;
  BYTE DATA i;
  CAN_MSG XDATA *tRxMsg;
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tRxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bRxId);
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);

  if (ServerSdo[bServIndex].bLastState != ST_READ_REQ) {
    bRet = UNKNOWN_CS;
  } /* if */

  if ((tRxMsg->bDb[0] & 0x10) != ServerSdo[bServIndex].bDataToggle) {
    bRet = TOG_NOT_ALTERED;
  } /* if */

  if (bRet == 0) {
    if ((ServerSdo[bServIndex].SDO_DATASIZE
      - ServerSdo[bServIndex].DataCounter) <= 7) {
      bRestBytes = (BYTE)(ServerSdo[bServIndex].SDO_DATASIZE
        - ServerSdo[bServIndex].DataCounter);
      LOCK_APPLICATION_RDATA;
      MemCpy(&tTxMsg->bDb[1],
        &ServerSdo[bServIndex].pbData[ServerSdo[bServIndex].DataCounter],
        bRestBytes);
      UNLOCK_APPLICATION_RDATA;
      /* clear unused message bytes */
      for (i = bRestBytes; i < 7; i++) {
        tTxMsg->bDb[i + 1] = 0;
      } /* for */
      tTxMsg->bDb[0] = (BYTE)(SCS_SEGMENT_UPLOAD
        | ServerSdo[bServIndex].bDataToggle /* toggle bit */
        | ((BYTE)(7 - bRestBytes) << 1)     /* number of bytes */
        | 0x01);                            /* last segment */
      ServerSdo[bServIndex].DataCounter += bRestBytes;
#if GENERIC_PROFILE_USED == 1
        PROFILE_SDO_UP(ServerSdo[bServIndex].wbIndex.wc,
                       ServerSdo[bServIndex].bSubIndex);
#endif /* GENERIC_PROFILE_USED == 1 */
      gSdoS_ClearServerInfo(bServIndex);
    } /* if */
    else {
      LOCK_APPLICATION_RDATA;
      MemCpy(&tTxMsg->bDb[1],
        &ServerSdo[bServIndex].pbData[ServerSdo[bServIndex].DataCounter],
        7); 
      UNLOCK_APPLICATION_RDATA;
      tTxMsg->bDb[0] = (BYTE)(SCS_SEGMENT_UPLOAD
        | ServerSdo[bServIndex].bDataToggle); /* toggle bit */
      ServerSdo[bServIndex].DataCounter += 7;
      /* set new time out */
      ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
    } /* else */
    /* answer is already created - switch toggle */
    ServerSdo[bServIndex].bDataToggle = (BYTE)((~tRxMsg->bDb[0]) & 0x10);
  } /* if */

  if (bRet != 0) {
    gSdoS_CreateAbort(bServIndex, bRet);
    gSdoS_ClearServerInfo(bServIndex);
  } /* if */
  return (bRet == 0) ? TRUE : FALSE;
} /* sSdoS_UpNext */
#endif /* SDO_READ_SEG_ALLOWED == 1 */


#if SDO_BLOCK_ALLOWED == 1
/*!
  \brief Initiate SDO block download indication

  This routine handles the indication of a SDO download request.
  The response message is created.

  \param bServIndex - Index in array #ServerSdo.
  \retval TRUE - Service could be initiated - in case of an expedited
  access the data are already transferred.
  \retval FALSE - The request has been failed.
*/
STATIC BYTE sSdoS_BlkInitDown(BYTE bServIndex)
{
  CONST OD_ENTRY CONST_PTR * ptEntry;
  BYTE DATA bRet = 0;
  CAN_MSG XDATA *tRxMsg;
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tRxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bRxId);
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);

  if (ServerSdo[bServIndex].bLastState == ST_TRANSFERRED) {
    gSdoS_ClearServerInfo(bServIndex);
    /* store index/sub index */
    ServerSdo[bServIndex].wbIndex.bc.lb = tRxMsg->bDb[1];
    ServerSdo[bServIndex].wbIndex.bc.mb = tRxMsg->bDb[2];
    ServerSdo[bServIndex].bSubIndex = tRxMsg->bDb[3];
    ServerSdo[bServIndex].bLastState = ST_WRITE_BLK_REQ;
    /* search for existence of entry in OD - and for write bit */
    if ((ptEntry =
      gSdoS_GetEntry((WORD)(ServerSdo[bServIndex].wbIndex.wc
      - sSdoS_GetPdoNumber(bServIndex)),
      ServerSdo[bServIndex].bSubIndex)) != 0) {
      if ((ptEntry->AccArrayClbck & WO) == WO) {
        if ((tRxMsg->bDb[0] & 0x02) == 0x02) {
          /*
          *        size has following format
          * |     4    |     5     |     6    |     7     |
          * |       Low Word       |      High Word       |
          * | Low Byte | High Byte | Low Byte | High Byte |
          */
          ServerSdo[bServIndex].qwDataSize.fb.b0 = tRxMsg->bDb[4];
          ServerSdo[bServIndex].qwDataSize.fb.b1 = tRxMsg->bDb[5];
          ServerSdo[bServIndex].qwDataSize.fb.b2 = tRxMsg->bDb[6];
          ServerSdo[bServIndex].qwDataSize.fb.b3 = tRxMsg->bDb[7];
          VDEBUG_RPT1("SDO Server blk download ind (%ld bytes)\n",
            ServerSdo[bServIndex].SDO_DATASIZE);
        } /* if */
        else {
          VDEBUG_RPT0("SDO Server blk download ind -> no size indicated\n");
          ServerSdo[bServIndex].SDO_DATASIZE
            = gSdoS_GetObjLenCb(ServerSdo[bServIndex].wbIndex.wc,
                ServerSdo[bServIndex].bSubIndex);
        } /* else */
        /* check length information */
        if (ptEntry->Length == 0) {
          if (ServerSdo[bServIndex].SDO_DATASIZE
            > gSdoS_GetObjLenCb(ServerSdo[bServIndex].wbIndex.wc,
            ServerSdo[bServIndex].bSubIndex)) {
            bRet = LEN_TOO_HIGH;
          } /* if */
        } /* if */
        else {
          if (ServerSdo[bServIndex].SDO_DATASIZE > ptEntry->Length) {
            bRet = LEN_TOO_HIGH;
          } /* if */
        } /* else */
        /* check temp buffer size */
        if ((bRet == 0) && (ServerSdo[bServIndex].SDO_DATASIZE > SDO_DATA_BUFFER_SIZE)) {
          VDEBUG_RPTE2("Sdo: Data buffer too small for %x %x!\n",
          ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
          bRet = OUT_OF_MEM;
        } /* if */

        if (bRet == 0) {
          if (ptEntry->pData == 0) {
            sSdoS_GetObjectAddress(bServIndex);
          } /* if */
          else {
            /*
              The direct access to the data item is allowed.
              So we have to gather the address.
            */
            if ((ptEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
              ServerSdo[bServIndex].pbData =
                ptEntry->pData
                + (ServerSdo[bServIndex].bSubIndex - 1)
                * (ServerSdo[bServIndex].SDO_DATASIZE);
            } /* if */
            else {
              ServerSdo[bServIndex].pbData = ptEntry->pData;
            } /* else */
          } /* else */
# if SDO_EXEC_USER_WRITE_CALLBACKS == 1
          /* server SDO callback function: check execution tag for this object */
          if (ptEntry->AccArrayClbck & SDO_EXEC_CB_DOWN) {
            ServerSdo[bServIndex].bCallbkAttr = ptEntry->AccArrayClbck;
            /* branch to callback funtion */
            bRet = gSdoS_DownCb(&ServerSdo[bServIndex]);
          } /* if */
# endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */
        } /* if */
      } /* if writeable */
      else {
        bRet = RD_ONLY_OBJ;
      } /* else */
    } /* if exists */
    else {
      /* subindex does not exist */
      /* search for existence of index in OD */
      if (gSdoS_GetEntry((WORD)(ServerSdo[bServIndex].wbIndex.wc
          - sSdoS_GetPdoNumber(bServIndex)), 0) != 0) {
        bRet = NO_SUB_INDEX;
      }
      else {
        bRet = OBJ_NOT_EXIST;
      } /* else */
    } /* else */
    /* allocate transfer structure */
  } /* if */
  else {
    bRet = UNKNOWN_CS;
  } /* else */

  /* copy index subindex to answer */
  tTxMsg->bDb[1] = tRxMsg->bDb[1];
  tTxMsg->bDb[2] = tRxMsg->bDb[2];
  tTxMsg->bDb[3] = tRxMsg->bDb[3];

  if (bRet != 0) {
    gSdoS_CreateAbort(bServIndex, bRet);
    gSdoS_ClearServerInfo(bServIndex);
  } /* if */
  else {
    tTxMsg->bDb[0] = SCS_BLK_DOWNLOAD | SS_INIT
#if SDO_BLOCK_USE_CRC == 1
      | CRC_SUPP;
#else
      | NO_CRC_SUPP;
#endif /* SDO_BLOCK_USE_CRC == 1 */
    ServerSdo[bServIndex].bCalcCrc = (BYTE)(tRxMsg->bDb[0]
#if SDO_BLOCK_USE_CRC == 1
      & CRC_SUPP);
#else
      & NO_CRC_SUPP);
#endif /* SDO_BLOCK_USE_CRC == 1 */
    tTxMsg->bDb[4] = SDO_MAX_BLK_SIZE;
    tTxMsg->bDb[5] = 0;
    tTxMsg->bDb[6] = 0;
    tTxMsg->bDb[7] = 0;
    /* set new time out */
    ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
  } /* else */
  return (bRet == 0) ? TRUE : FALSE;
} /* sSdoS_BlkInitDown */


/*!
  \brief SDO block download segment indication

  This routine handles the indication of a SDO block download segment request.
  The response message is created.

  \param bServIndex - Index in array #ServerSdo.
  \retval TRUE - Segment could be evaluated.
  \retval FALSE - The request has been failed.
*/
STATIC BYTE sSdoS_BlkDownNext(BYTE bServIndex)
{
  BYTE bValidBytes;
  BYTE DATA bRet = 0;
  BYTE bAnswerOk = FALSE;
  CAN_MSG XDATA *tRxMsg;
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tRxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bRxId);
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);

  if (ServerSdo[bServIndex].bLastState != ST_WRITE_BLK_REQ) {
    VDEBUG_RPT0("SDO Server blk down seg. -> wrong command\n");
    bRet = UNKNOWN_CS;
  } /* if */

  if ((ServerSdo[bServIndex].bActSequenceNr + 1) != (tRxMsg->bDb[0] & 0x7F)) {
    VDEBUG_RPT2("SDO Server blk down seg. -> Sequence error %d != %d\n",
      ServerSdo[bServIndex].bActSequenceNr, (tRxMsg->bDb[0] & 0x7F));
    bRet = INVAL_SEQ_NUM;
  } /* if */

  if (ServerSdo[bServIndex].bActSequenceNr > SDO_MAX_BLK_SIZE) {
    VDEBUG_RPT2("SDO Server blk down seg. -> Block len error %d != %d\n",
      ServerSdo[bServIndex].bActSequenceNr, (tRxMsg->bDb[0] & 0x7F));
    bRet = INVAL_BLK_SIZE;
  } /* if */

  /* length was specified */
  if ((ServerSdo[bServIndex].SDO_DATASIZE
    - ServerSdo[bServIndex].DataCounter) >= 7) {
    bValidBytes = 7;
  } /* if */
  else {
    bValidBytes = (BYTE)(ServerSdo[bServIndex].SDO_DATASIZE
      - ServerSdo[bServIndex].DataCounter);
  } /* else */

  if (bRet == 0) {
    if (ServerSdo[bServIndex].SDO_DATASIZE
      >= (ServerSdo[bServIndex].DataCounter + bValidBytes)) {
      /* OK segment accepted - copy bytes from received message to temp buffer */
      MemCpy(&ServerSdo[bServIndex].abSdoDataTmpBuffer[ServerSdo[bServIndex].DataCounter],
        &tRxMsg->bDb[1], bValidBytes); 

      ServerSdo[bServIndex].bActSequenceNr++;
      if ((ServerSdo[bServIndex].bActSequenceNr == SDO_MAX_BLK_SIZE)
        || ((tRxMsg->bDb[0] & 0x80) == 0x80)) {
        /* answer the block */
        /* old data must be overwritten in tx buffer */
        MemSet(&tTxMsg->bDb[3], 0x00, 5);
        tTxMsg->bDb[0] = SCS_BLK_DOWNLOAD | SS_BLOCK;
        tTxMsg->bDb[1] = ServerSdo[bServIndex].bActSequenceNr;
        tTxMsg->bDb[2] = SDO_MAX_BLK_SIZE;
        ServerSdo[bServIndex].bActSequenceNr = 0;
        bAnswerOk = TRUE;
      } /* if */

      ServerSdo[bServIndex].DataCounter += 7; /* it is corrected during END handling */
      /* end the transfer if completion bit is set */
      if ((tRxMsg->bDb[0] & 0x80) == 0x80) {
        ServerSdo[bServIndex].bLastState = ST_WRITE_END_REQ;
      } /* if */
    } /* if */
    else {
      VDEBUG_RPT1("SDO Server blk down seg. -> Sequence %d overruns buffer\n",
        ServerSdo[bServIndex].bActSequenceNr);
      bRet = OUT_OF_MEM;
    } /* else */
  } /* if */

  if (bRet != 0) {
    gSdoS_CreateAbort(bServIndex, bRet);
    gSdoS_ClearServerInfo(bServIndex);
    bAnswerOk = TRUE;
  } /* if */
  else {
    /* set new time out */
    ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
  } /* else */
  return bAnswerOk;
} /* sSdoS_BlkDownNext */


/*!
  \brief SDO block end download segment indication

  This routine handles the indication of a SDO download segment request.
  The response message is created.

  \param bServIndex - Index in array ServerSdo.
  \retval TRUE - Segment could be evaluated.
  \retval FALSE - The request has been failed.
*/
STATIC BYTE sSdoS_BlkDownEnd(BYTE bServIndex)
{
  CONST OD_ENTRY CONST_PTR * ptEntry;
  CAN_MSG XDATA *tRxMsg;
  CAN_MSG XDATA *tTxMsg;
  BYTE DATA bRet = 0;

  /* message access */
  tRxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bRxId);
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);

  if (ServerSdo[bServIndex].bLastState != ST_WRITE_END_REQ) {
    bRet = UNKNOWN_CS;
  } /* if */

  ServerSdo[bServIndex].DataCounter = ServerSdo[bServIndex].DataCounter
    - ((BYTE)(tRxMsg->bDb[0] & 0x1C) >> 2);

#if SDO_BLOCK_USE_CRC == 1
  if ((bRet == 0) && ServerSdo[bServIndex].bCalcCrc) {
    WBYTE wbCrc;
    wbCrc.bc.lb = tRxMsg->bDb[1];
    wbCrc.bc.mb = tRxMsg->bDb[2];
    if (gSdoS_CrcCalc(ServerSdo[bServIndex].abSdoDataTmpBuffer,
      ServerSdo[bServIndex].DataCounter) != wbCrc.wc) {
      VDEBUG_RPT0("SDO Server blk down end. -> CRC failed\n");
      bRet = CRC_ERROR;
    } /* if */
  } /* if */
#endif /* SDO_BLOCK_USE_CRC == 1 */

  if (bRet == 0) {
#if SDO_EXEC_USER_WRITE_CALLBACKS == 1
    /* server SDO callback function: check execution tag for this object */
    if (ServerSdo[bServIndex].bCallbkAttr & SDO_EXEC_CB_DOWN) {
      /* branch to callback funtion */
      bRet = gSdoS_DownCbFinish(&ServerSdo[bServIndex]);
    } /* if */
    if (bRet == 0)
#endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */
    {
      LOCK_APPLICATION_WDATA;
      /*
      transfer completed
      - copy all data from temp buffer to object storage location
      */
      MemCpy(ServerSdo[bServIndex].pbData,
        ServerSdo[bServIndex].abSdoDataTmpBuffer,
        ServerSdo[bServIndex].SDO_DATASIZE);
      UNLOCK_APPLICATION_WDATA;
      /* search for existence of entry in OD */
      if ((ptEntry =
        gSdoS_GetEntry((WORD)(ServerSdo[bServIndex].wbIndex.wc
        - sSdoS_GetPdoNumber(bServIndex)),
        ServerSdo[bServIndex].bSubIndex)) != 0) {
  
        if (ptEntry->pData == 0) {
          /* check values internally */
          bRet = sSdoS_WriteData(ptEntry, bServIndex);
          if (bRet != 0) {
            VDEBUG_RPTE2("Sdo: Could not write %x %x!\n",
            ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
          } /* if */
          else {
            if (ServerSdo[bServIndex].pbData == 0) {
              bRet = GEN_INT_PARA;
            } /* if */
          } /* else */
        } /* if */
      } /* if */
      else {
        bRet = OBJ_NOT_EXIST;
      } /* else */
#if GENERIC_PROFILE_USED == 1
      PROFILE_SDO_DOWN(ServerSdo[bServIndex].wbIndex.wc,
                       ServerSdo[bServIndex].bSubIndex);
#endif /* GENERIC_PROFILE_USED == 1 */
    } /* if */
  } /* if */

  if (bRet != 0) {
    gSdoS_CreateAbort(bServIndex, bRet);
  } /* if */
  else {
    /* old data must be overwritten in tx buffer */
    MemSet(&tTxMsg->bDb[1], 0x00, 7);
    tTxMsg->bDb[0] = SCS_BLK_DOWNLOAD | SS_END;
  } /* else */
  return (bRet == 0) ? TRUE : FALSE;
} /* sSdoS_BlkDownEnd */


/*!
  \brief Initiate SDO block upload indication

  This routine handles the indication of a SDO block upload request.
  The response message is created.

  \param bServIndex - Index in array ServerSdo.
  \retval TRUE - Service could be initiated - in case of an expedited
  access the data are already transferred.
  \retval FALSE - The request has been failed.
*/
STATIC BYTE sSdoS_BlkInitUp(BYTE bServIndex)
{
  CONST OD_ENTRY CONST_PTR * ptEntry;
  BYTE DATA bRet = 0;
  CAN_MSG XDATA *tRxMsg;
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tRxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bRxId);
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);

  if (ServerSdo[bServIndex].bLastState == ST_TRANSFERRED) {
    gSdoS_ClearServerInfo(bServIndex);
    /* store index/sub index */
    ServerSdo[bServIndex].wbIndex.bc.lb = tRxMsg->bDb[1];
    ServerSdo[bServIndex].wbIndex.bc.mb = tRxMsg->bDb[2];
    ServerSdo[bServIndex].bSubIndex = tRxMsg->bDb[3];

    ServerSdo[bServIndex].bLastState = ST_READ_BLK_REQ;

    /* search for existence of entry in OD - and for read bit */

    if ((ptEntry =
      gSdoS_GetEntry((WORD)(ServerSdo[bServIndex].wbIndex.wc
        - sSdoS_GetPdoNumber(bServIndex)),
        ServerSdo[bServIndex].bSubIndex)) != 0) {
      if ((ptEntry->AccArrayClbck & RO) == RO) {
        /*
          Now we need the data location and the length of the item
          in order to answer the request.
        */

        /* check length information */
        if (ptEntry->Length == 0) {
          ServerSdo[bServIndex].SDO_DATASIZE
          = gSdoS_GetObjLenCb(ServerSdo[bServIndex].wbIndex.wc,
            ServerSdo[bServIndex].bSubIndex);
          if (ServerSdo[bServIndex].SDO_DATASIZE == 0) {
            /*
              This is a configuration problem.
              If we come to this point either the entry in OBJDICT.C is
              wrong or we forgot the entry in the function
              sSdo_S_GetObjLen().
              So complain and leave here.
            */
            VDEBUG_RPTE2("Sdo: No length information for entry %x %x!\n",
            ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
            bRet = GEN_PARA_INC;
          } /* if */
        } /* if */
        else {
          ServerSdo[bServIndex].SDO_DATASIZE = ptEntry->Length;
        } /* else */

        /* get item's address */
        if (ptEntry->pData == 0) {
          sSdoS_GetObjectAddress(bServIndex);
          /*
            The data contents are copied to the expedited buffer in the
            structure VSdoAttrib during the call to sSdoS_ReadData().
            Additionally the pointer to the data is adjusted.
          */
          bRet = sSdoS_ReadData(ptEntry, bServIndex);
          if (bRet != 0) {
            VDEBUG_RPTE2("Sdo: No address information for entry %x %x!\n",
            ServerSdo[bServIndex].wbIndex.wc, ServerSdo[bServIndex].bSubIndex);
          } /* if */
        } /* if */
        else  {
          /* correct target address if array */
          if ((ptEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
            ServerSdo[bServIndex].pbData = ptEntry->pData
              + (ServerSdo[bServIndex].bSubIndex - 1)
              * (ServerSdo[bServIndex].SDO_DATASIZE);
          } /* if */
          else {
            ServerSdo[bServIndex].pbData = ptEntry->pData;
          } /* else */
        } /* else */
#if SDO_EXEC_USER_READ_CALLBACKS == 1
        /* server SDO callback function: check execution tag for this object */
        if ((bRet == 0) && (ptEntry->AccArrayClbck & SDO_EXEC_CB_UP)) {
          /* branch to callback funtion */
          bRet = gSdoS_UpCb(&ServerSdo[bServIndex]);
        } /* if */
#endif /* SDO_EXEC_USER_READ_CALLBACKS == 1 */
      } /* if */
      else {
        bRet = WR_ONLY_OBJ;
      } /* else */

      if ((tRxMsg->bDb[4] > 0) && (tRxMsg->bDb[4] < 128)) {
        if (tRxMsg->bDb[4] != ServerSdo[bServIndex].bBlockSize) {
          VDEBUG_RPT1("SDO Server blk upload init -> blocksize changed to %d\n",
            tRxMsg->bDb[4]);
          ServerSdo[bServIndex].bBlockSize = tRxMsg->bDb[4];
        } /* if */
      } /* if */
      else {
        /* invalid blocksize detected - abort */
        bRet = INVAL_BLK_SIZE;
      } /* else */
    } /* if */
    else {
      /* subindex does not exist */
      /* search for existence of index in OD */
      if (gSdoS_GetEntry((WORD)(ServerSdo[bServIndex].wbIndex.wc
          - sSdoS_GetPdoNumber(bServIndex)), 0) != 0) {
        bRet = NO_SUB_INDEX;
      }
      else {
        bRet = OBJ_NOT_EXIST;
      } /* else */
    } /* else */
    /* allocate transfer structure */
  } /* if */
  else {
    bRet = UNKNOWN_CS;
  } /* else */

  /* copy index subindex to answer */
  tTxMsg->bDb[1] = tRxMsg->bDb[1];
  tTxMsg->bDb[2] = tRxMsg->bDb[2];
  tTxMsg->bDb[3] = tRxMsg->bDb[3];

  if (bRet != 0) {
    gSdoS_CreateAbort(bServIndex, bRet);
    gSdoS_ClearServerInfo(bServIndex);
  } /* if */
  else {
    /* evaluate threshold value */
    if ((tRxMsg->bDb[5] == 0)
        || (ServerSdo[bServIndex].SDO_DATASIZE > tRxMsg->bDb[5])) {
      /* answer in block mode only */
      tTxMsg->bDb[0] = SCS_BLK_UPLOAD
        | SS_INIT
        | SIZE_IND /* size is indicated generally */
#if SDO_BLOCK_USE_CRC == 1
        | CRC_SUPP;
#else
        | NO_CRC_SUPP;
#endif /* SDO_BLOCK_USE_CRC */
      ServerSdo[bServIndex].bCalcCrc = (BYTE)(tRxMsg->bDb[0]
#if SDO_BLOCK_USE_CRC == 1
        & CRC_SUPP);
#else
        & NO_CRC_SUPP);
#endif /* SDO_BLOCK_USE_CRC */
      /* return length */
      tTxMsg->bDb[4] = ServerSdo[bServIndex].qwDataSize.fb.b0;
      tTxMsg->bDb[5] = ServerSdo[bServIndex].qwDataSize.fb.b1;
      tTxMsg->bDb[6] = ServerSdo[bServIndex].qwDataSize.fb.b2;
      tTxMsg->bDb[7] = ServerSdo[bServIndex].qwDataSize.fb.b3;
      VDEBUG_RPT1("SDO Server blk upload init -> (%ld bytes)\n",
        ServerSdo[bServIndex].SDO_DATASIZE);
      /* set new time out */
      ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
    } /* if */
    else {
      /* switch to segmented upload */
      /* create answer according to data length */
      if ((ServerSdo[bServIndex].SDO_DATASIZE > 0)
        && (ServerSdo[bServIndex].SDO_DATASIZE < 5)) {
        /* create expedited command specifier */
        tTxMsg->bDb[0] = (BYTE)(SCS_INIT_UPLOAD | 0x03
          | (BYTE)((4 - ServerSdo[bServIndex].SDO_DATASIZE) << 2));
        /* fill in data */
        LOCK_APPLICATION_RDATA;
        switch ((BYTE)ServerSdo[bServIndex].SDO_DATASIZE) {
        case 1:
          tTxMsg->bDb[4] = ServerSdo[bServIndex].pbData[0];
          tTxMsg->bDb[5] = 0;
          tTxMsg->bDb[6] = 0;
          tTxMsg->bDb[7] = 0;
          break;
        case 2:
          tTxMsg->bDb[4] = (*((WBYTE*)ServerSdo[bServIndex].pbData)).bc.lb;
          tTxMsg->bDb[5] = (*((WBYTE*)ServerSdo[bServIndex].pbData)).bc.mb;
          tTxMsg->bDb[6] = 0;
          tTxMsg->bDb[7] = 0;
          break;
        case 3:
          tTxMsg->bDb[4] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b0;
          tTxMsg->bDb[5] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b1;
          tTxMsg->bDb[6] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b2;
          tTxMsg->bDb[7] = 0;
          break;
        case 4:
          tTxMsg->bDb[4] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b0;
          tTxMsg->bDb[5] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b1;
          tTxMsg->bDb[6] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b2;
          tTxMsg->bDb[7] = (*((QBYTE*)ServerSdo[bServIndex].pbData)).fb.b3;
          break;
        default:
          
          break;
        } /* switch */
        UNLOCK_APPLICATION_RDATA;
        /* transfer completed successfully */
        gSdoS_ClearServerInfo(bServIndex);
      } /* if */
      else {
#if SDO_WRITE_SEG_ALLOWED == 1
        ServerSdo[bServIndex].bLastState = ST_READ_REQ;
        tTxMsg->bDb[0] = SCS_INIT_UPLOAD | 0x01;
        /* return length */
        tTxMsg->bDb[4] = ServerSdo[bServIndex].qwDataSize.fb.b0;
        tTxMsg->bDb[5] = ServerSdo[bServIndex].qwDataSize.fb.b1;
        tTxMsg->bDb[6] = ServerSdo[bServIndex].qwDataSize.fb.b2;
        tTxMsg->bDb[7] = ServerSdo[bServIndex].qwDataSize.fb.b3;
        /* set new time out */
        ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
#else
        bRet = GEN_INT_PARA;
#endif  /* else SDO_WRITE_SEG_ALLOWED == 1 */
      } /* else */
    } /* else */
  } /* else */
  return (bRet == 0) ? TRUE : FALSE;
} /* sSdoS_BlkInitUp */


/*!
  \brief Handle the start segment block upload indication.

  Check the correct state of the server information block.
  
  \param bServIndex - server instance
  \retval TRUE - download initiated
  \retval FALSE - download not initiated
 */
STATIC BYTE sSdoS_BlkStartUp(BYTE bServIndex)
{
  BYTE DATA bRet = 0;

  /* check if download was init'd */
  if (ServerSdo[bServIndex].bLastState != ST_READ_BLK_REQ) {
    bRet = UNKNOWN_CS;
  } /* if */

  if (bRet != 0) {
    gSdoS_CreateAbort(bServIndex, bRet);
    ServerSdo[bServIndex].bLastState = ST_TRANSFERRED;
    gSdoS_ClearServerInfo(bServIndex);
  } /* if */
  else {
    /* set new time out */
    ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
    ServerSdo[bServIndex].bLastState = ST_READ_START_REQ;
  } /* else */
  return (bRet == 0) ? TRUE : FALSE;
} /* sSdoS_BlkStartUp */


/*!
  \brief Transmit the block segments.

  All segments are transmitted in a loop.

  \param bServIndex - server instance
  \retval TRUE - message has to be sent from outside of function
  \retval FALSE - block transmitted completely
 */
BYTE sSdoS_BlkUpNext(BYTE bServIndex)
{
  BYTE bValidBytes; /* valid bytes in segment message */
  BYTE DATA i;
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);
    
  /* check if upload was not completely init'd */
  switch (ServerSdo[bServIndex].bLastState) {
    case ST_READ_START_REQ:
      for (i = 0; i < ServerSdo[bServIndex].bBlockSize; i++) {
        /* for all valid bytes */
        if (ServerSdo[bServIndex].SDO_DATASIZE
          != ServerSdo[bServIndex].DataCounter) {
          /* check for last message */
          if ((ServerSdo[bServIndex].SDO_DATASIZE
            - ServerSdo[bServIndex].DataCounter) <= 7) {
            /* be sure that tha variable could not be overflowed */
            bValidBytes = (BYTE)(ServerSdo[bServIndex].SDO_DATASIZE
              - ServerSdo[bServIndex].DataCounter);
            ServerSdo[bServIndex].bLastSegment = bValidBytes;

            /* create last segment */
            tTxMsg->bDb[0] = (BYTE)((i + 1) | 0x80);
            LOCK_APPLICATION_RDATA;
            MemCpy(&tTxMsg->bDb[1],
              &ServerSdo[bServIndex].pbData[ServerSdo[bServIndex].DataCounter],
              bValidBytes);
            UNLOCK_APPLICATION_RDATA;
            ServerSdo[bServIndex].DataCounter += bValidBytes;
            ServerSdo[bServIndex].bLastState = ST_READ_END_REQ;
          } /* if */
          else {
            /* fill segment completely */
            tTxMsg->bDb[0] = (BYTE)(i + 1);
            LOCK_APPLICATION_RDATA;
            MemCpy(&tTxMsg->bDb[1],
              &ServerSdo[bServIndex].pbData[ServerSdo[bServIndex].DataCounter],
              7);
            UNLOCK_APPLICATION_RDATA;
            ServerSdo[bServIndex].DataCounter += 7;
          } /* else */
          if (gCan_SendBuf(ServerSdo[bServIndex].bTxId) != NO_ERR) {
            VDEBUG_RPT0("SDO Server blk upload seg. -> TX buffer full (resize block length)\n");
            return FALSE;
          } /* if */
        } /* if */
        /* set new time out */
        ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
      } /* for */
    return FALSE;

  case ST_READ_END_REQ:
    /* all bytes transmitted - create END request */
    tTxMsg->bDb[0] = (BYTE)(SCS_BLK_UPLOAD
        | (((BYTE)(7 - ServerSdo[bServIndex].bLastSegment) << 2) & 0x1C)
        | SS_END);
    if (ServerSdo[bServIndex].bCalcCrc) {
#if SDO_BLOCK_USE_CRC == 1
      WBYTE wbCrc;
      wbCrc.wc = gSdoS_CrcCalc(ServerSdo[bServIndex].pbData,
        ServerSdo[bServIndex].SDO_DATASIZE);
      tTxMsg->bDb[1] = wbCrc.bc.lb;
      tTxMsg->bDb[2] = wbCrc.bc.mb;
#endif /* SDO_BLOCK_USE_CRC */
    } /* if */
    else {
      tTxMsg->bDb[1] = 0;
      tTxMsg->bDb[2] = 0;
    } /* else */
    tTxMsg->bDb[3] = 0;
    tTxMsg->bDb[4] = 0;
    tTxMsg->bDb[5] = 0;
    tTxMsg->bDb[6] = 0;
    tTxMsg->bDb[7] = 0;
    VDEBUG_RPT1("SDO Server blk upload seg. -> END request (%d bytes in last segment)\n",
    ServerSdo[bServIndex].bLastSegment);
    /* set new time out */
    ServerSdo[bServIndex].wTimeOut = SDO_TIMEOUT;
    return TRUE;

  default:
    VDEBUG_RPT0("SDO Server blk upload ind. -> Wrong state\n");
    gSdoS_CreateAbort(bServIndex, UNKNOWN_CS);
    ServerSdo[bServIndex].bLastState = ST_TRANSFERRED;
    gSdoS_ClearServerInfo(bServIndex);
    return TRUE;
  } /* switch */
} /* sSdoS_BlkUpNext */


/*!
  \brief Handle the END segment block upload confirmation.

  The client node answers to our end block upload request.
  We evaluate these answers in this call.

  \param bServIndex - client instance
  \retval TRUE - download initiated
  \retval FALSE - download not initiated
 */
STATIC BYTE sSdoS_BlkUpEnd(BYTE bServIndex)
{
  BYTE DATA bRet = 0;

  /* check if download was init'd */
  if (ServerSdo[bServIndex].bLastState != ST_READ_END_REQ) {
    bRet = UNKNOWN_CS;
  } /* if */
  else {
    /* this is the answer to the last segment */
    VDEBUG_RPT1("SDO Server blk upload seg. -> block upload completed (%ld bytes)\n",
      ServerSdo[bServIndex].DataCounter);
    ServerSdo[bServIndex].bLastState = ST_TRANSFERRED;
#if GENERIC_PROFILE_USED == 1
      PROFILE_SDO_UP(ServerSdo[bServIndex].wbIndex.wc,
                     ServerSdo[bServIndex].bSubIndex);
#endif /* GENERIC_PROFILE_USED == 1 */
    gSdoS_ClearServerInfo(bServIndex);
  } /* else */
  return (bRet == 0) ? TRUE : FALSE;
} /* sSdoS_BlkUpEnd */
#endif /* SDO_BLOCK_ALLOWED */


/*!
  \brief Check server instances for time out condition.

  The client transfers are time controlled. The time out value is determined
  by #SDO_TIMEOUT.
  If a time out occurs an abort message is transmitted
  and the transfer is signalled as interrupted.

  \param wRealDifference - difference to last check in milliseconds
*/
void gSdoS_ChkTimeOut(WORD wRealDifference)
{
  BYTE DATA i;

  for (i = 0; i < sizeof(ServerSdo) / sizeof(ServerSdo[0]); i++) {
    /* client is used and transfer is running */
    if ((ServerSdo[i].bLastState != ST_IDLE)
      && (ServerSdo[i].bLastState != ST_TRANSFERRED)) {
      if (ServerSdo[i].wTimeOut > wRealDifference) {
        ServerSdo[i].wTimeOut =
          (WORD)(ServerSdo[i].wTimeOut - wRealDifference);
      } /* if */
      else {
        /* signal time out and abort transfer */
        gSdoS_CreateAbort(i, SDO_TIME_OUT);
        if (gCan_SendBuf(ServerSdo[i].bTxId) == NO_ERR) {
          VDEBUG_RPT0("Server access aborted\n");
          ServerSdo[i].wTimeOut = 0;
          ServerSdo[i].bLastState = ST_TRANSFERRED;
          gSdoS_ClearServerInfo(i);
        } /* if */
      } /* else */
    } /* if */
  } /* for */
} /* gSdoS_ChkTimeOut */ 


/*!
  \brief Main call for slave's SDO server handler

  If a SDO receive message was recognised it is routed
  to this function. From there further actions are taken.
  The function checks if there is a SDO resource available.

  \param bIndex - index in array ServerSdo[]
  \retval TRUE - OK
  \retval FALSE - An error occurred during SDO evaluation.
*/
BYTE gSdoS_HandleSdo(BYTE bIndex)
{
  BYTE DATA bRet = TRUE;

  assert(bIndex < sizeof(ServerSdo) / sizeof(ServerSdo[0]));

#if SDO_BLOCK_ALLOWED == 1
  /*
    First check if we have a running block transfer and must handle
    the single block requests.
  */
  if (ServerSdo[bIndex].bLastState == ST_WRITE_BLK_REQ) {
    if (sSdoS_BlkDownNext(bIndex) == TRUE) {
      return gCan_SendBuf(ServerSdo[bIndex].bTxId);
    } /* if */
    else {
      return FALSE;
    } /* else */
  } /* if */

  /*
    Evaluate the bits 7, 6, 5, 1, 0.
  */
  switch (MESSAGE_DB0(ServerSdo[bIndex].bRxId) & 0xE3) {
    case CCS_BLK_DOWNLOAD | CS_INIT | NO_SIZE_IND: /* initiate block download */
    case CCS_BLK_DOWNLOAD | CS_INIT | SIZE_IND:
      bRet = sSdoS_BlkInitDown(bIndex);
      return gCan_SendBuf(ServerSdo[bIndex].bTxId);
    case CCS_BLK_UPLOAD | CS_INIT: /* initiate block upload */
      bRet = sSdoS_BlkInitUp(bIndex);
      return gCan_SendBuf(ServerSdo[bIndex].bTxId);
    case CCS_BLK_DOWNLOAD | CS_END: /* end block download */
      sSdoS_BlkDownEnd(bIndex);
      bRet = gCan_SendBuf(ServerSdo[bIndex].bTxId);
      gSdoS_ClearServerInfo(bIndex);
      return bRet;
    case CCS_BLK_UPLOAD | CS_END:
      return sSdoS_BlkUpEnd(bIndex);
    case CCS_BLK_UPLOAD | CS_BLOCK: /* upload next block */
      /* transmit block */
      /* transmit first block */
      if (sSdoS_BlkUpNext(bIndex)) {
        /* transmit abort message */
        return gCan_SendBuf(ServerSdo[bIndex].bTxId);
      } /* if */
      else {
        return TRUE;
      } /* else */
    case CCS_BLK_UPLOAD | CS_UPLOAD: /* start block upload */
      if (sSdoS_BlkStartUp(bIndex) == FALSE) {
        /* transmit abort message */
        return gCan_SendBuf(ServerSdo[bIndex].bTxId);
      } /* if */
      else {
        /* transmit first block */
        if (sSdoS_BlkUpNext(bIndex)) {
          /* transmit abort message */
          return gCan_SendBuf(ServerSdo[bIndex].bTxId);
        } /* if */
        else {
          return TRUE;
        } /* else */
      } /* else */
    default:
      break;
  } /* switch */
#endif /* SDO_BLOCK_ALLOWED == 1 */
  /*
    Evaluate the bits 7, 6, 5.
  */
  switch (MESSAGE_DB0(ServerSdo[bIndex].bRxId) & 0xE0) {
    case CCS_INIT_UPLOAD: /* CCS_INIT_UPLOAD */
      bRet = sSdoS_InitUp(bIndex);
#if DELAYED_SDO_ENABLE == 1
      /* send reply message only if data has been copied to message */
      if ((ServerSdo[bIndex].bDelayedState == DELAY_READ_REQ) ||
          (ServerSdo[bIndex].bDelayedState == DELAY_COMPLETE)) {
        return TRUE;
      } /* if */
      else
#endif /* DELAYED_SDO_ENABLE == 1 */
      {
      return gCan_SendBuf(ServerSdo[bIndex].bTxId);
      } /* else */
    case CCS_INIT_DOWNLOAD: /* CCS_INIT_DOWNLOAD */
      bRet = sSdoS_InitDown(bIndex);
#if DELAYED_SDO_ENABLE == 1
      /* send reply message only if data has been copied to message */
      if (ServerSdo[bIndex].bDelayedState == DELAY_WRITE_REQ) {
        return TRUE;
      } /* if */
      else
#endif /* DELAYED_SDO_ENABLE == 1 */
      {
      return gCan_SendBuf(ServerSdo[bIndex].bTxId);
      } /* else */
    case CCS_SEGMENT_UPLOAD: /* CCS_SEGMENT_UPLOAD */
#if SDO_READ_SEG_ALLOWED == 1
      bRet = sSdoS_UpNext(bIndex);
#else
      bRet = sSdoS_ProtoRestricted(bIndex);
#endif /* SDO_READ_SEG_ALLOWED == 1 */
      return gCan_SendBuf(ServerSdo[bIndex].bTxId);
    case CCS_SEGMENT_DOWNLOAD: /* CCS_SEGMENT_DOWNLOAD */
#if SDO_WRITE_SEG_ALLOWED == 1
      bRet = sSdoS_DownNext(bIndex);
#else
      bRet = sSdoS_ProtoRestricted(bIndex);
#endif /* SDO_WRITE_SEG_ALLOWED == 1 */
      return gCan_SendBuf(ServerSdo[bIndex].bTxId);
    case ABORT_DOMAIN: /* ABORT_DOMAIN */
      return gSdoS_ClearServerInfo(bIndex);
#if SDO_BLOCK_ALLOWED == 0
    default:
      bRet = sSdoS_ProtoRestricted(bIndex);
      if (bRet == TRUE) {
        return gCan_SendBuf(ServerSdo[bIndex].bTxId);
      } /* if */
      break;
#endif /* SDO_BLOCK_ALLOWED == 0 */
  } /* switch end mask bit 7-5 */
  return bRet;
}/* gSdoS_HandleSdo */


#if SDO_BLOCK_ALLOWED == 1
# if SDO_BLOCK_USE_CRC == 1
#  if CRC_LOOKUP_TABLE == 1
/*****************************************************************/
/*                                                               */
/* CRC LOOKUP TABLE                                              */
/* ================                                              */
/* The following CRC lookup table was generated automagically    */
/* by the Rocksoft^tm Model CRC Algorithm Table Generation       */
/* Program V1.0 using the following model parameters:            */
/*                                                               */
/*    Width   : 2 bytes.                                         */
/*    Poly    : 0x1021                                           */
/*    Reverse : FALSE.                                           */
/*                                                               */
/* For more information on the Rocksoft^tm Model CRC Algorithm,  */
/* see the document titled "A Painless Guide to CRC Error        */
/* Detection Algorithms" by Ross Williams                        */
/* (ross@guest.adelaide.edu.au.). This document is likely to be  */
/* in the FTP archive "ftp.adelaide.edu.au/pub/rocksoft".        */
/*                                                               */
/*****************************************************************/
CONST WORD crctable[256] = {
  0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
  0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
  0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
  0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
  0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
  0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
  0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
  0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
  0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
  0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
  0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
  0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
  0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
  0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
  0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
  0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
  0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
  0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
  0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
  0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
  0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
  0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
  0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
  0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
  0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
  0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
  0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
  0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
  0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
  0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
  0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
  0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
};
/*****************************************************************/
/*                   End of CRC Lookup Table                     */
/*****************************************************************/


/*!
  \brief CRC calculation routine

  The specified CRC is calculated by this routine.
  The lookup table crctable is used.
  
  \param pStart - Start address of data item.
  \param size - Size of data area.
  \return CRC value
*/
WORD gSdoS_CrcCalc(BYTE * pStart, DWORD size)
{
  WORD DATA crc = 0;
  WORD DATA dat;

  while (size--) {
    dat = (WORD) *pStart++;
    crc = (WORD)((crc << 8) ^ crctable[((crc >> 8) ^ dat) & 0xFF]);
  } /* while */
  VDEBUG_RPT1("CRC = %x\n", crc);
  return crc;
} /* gSdoS_CrcCalc */

#  else /* CRC_LOOKUP_TABLE != 1 */

/*!
  \brief CRC calculation routine

  The specified CRC is calculated by this routine.
  
  \param pStart - Start address of data item.
  \param size - Size of data area.
  \return CRC value
*/
WORD gSdoS_CrcCalc(BYTE *pStart, DWORD size)
{
  BYTE DATA dat;
  BYTE DATA i;
  WORD DATA crc = 0;

  while (size--) {
    dat = (BYTE) *pStart++;
    crc ^= dat << 8;

    for (i = 0; i < 8; i++) {
      if (crc & 0x8000) {
        crc = (crc << 1) ^ 0x1021;
      } /* if */
      else {
        crc <<= 1;
      } /* else */
    } /* for */
  } /* while */
  VDEBUG_RPT1("CRC = %x\n", crc);
  return crc;
} /* gSdoS_CrcCalc */

#  endif /* CRC_LOOKUP_TABLE == 1 */
# endif /* SDO_BLOCK_USE_CRC */
#endif /* SDO_BLOCK_ALLOWED == 1 */


#if SDO_LOCAL_WRITE == 1
/*!
  \brief Write to an entry in the object dictionary.

  This function gives write access to an object in the object dictionary.
  The caller does not need to know the address of the written object.
  The index and sub-index information is sufficient.

  \param wIndex Index of object.
  \param bSubIndex Sub-index of object.
  \param ptValue The value to be written.
  \retval TRUE - object written
  \retval FALSE - object not written
*/
BYTE gWriteObject(const WORD wIndex, const BYTE bSubIndex, BYTE XDATA * ptValue)
{
  CONST OD_ENTRY CONST_PTR * ptEntry;
  BYTE DATA bRet = TRUE; /* no bytes written */

  if (ServerSdo[0].bLastState == ST_TRANSFERRED) {
    gSdoS_ClearServerInfo(0);
    /* store index/sub index */
    ServerSdo[0].wbIndex.wc = wIndex;
    ServerSdo[0].bSubIndex = bSubIndex;

    /* search for existence of entry in OD */
    ptEntry = gSdoS_GetEntry((WORD)(ServerSdo[0].wbIndex.wc -
                sSdoS_GetPdoNumber(0)), ServerSdo[0].bSubIndex);

    if (ptEntry != 0) {
      /* check write bit */
      if ((ptEntry->AccArrayClbck & WO) == WO) {
        ServerSdo[0].SDO_DATASIZE = ptEntry->Length; /* take length as is */
        if ((BYTE)ServerSdo[0].SDO_DATASIZE <= 4)  {
          /*
            Only up to four bytes supported.
          */
          if (ptEntry->pData == 0) {
            sSdoS_GetObjectAddress(0);
            /*
              Copy the message contents directly to the 
              expedited buffer.
              Data item is in intel format in message buffer.
            */
            ServerSdo[0].bExpedBuf[0] = ptValue[0];
            ServerSdo[0].bExpedBuf[1] = ptValue[1];
            ServerSdo[0].bExpedBuf[2] = ptValue[2];
            ServerSdo[0].bExpedBuf[3] = ptValue[3];
            ServerSdo[0].pbData = (BYTE *)&ServerSdo[0].bExpedBuf[0];
            /*
              During the call of sSdoS_WriteData()
              the data contents from the incoming message are
              copied to the final location.
            */
            if (sSdoS_WriteData(ptEntry, 0) != 0) {
              VDEBUG_RPTE2("Sdo: No data address available for  %x %x!\n",
              ServerSdo[0].wbIndex.wc, ServerSdo[0].bSubIndex);
              bRet = FALSE; /* no bytes written */
            } /* if */
          } /* if */
          else {
            /*
              The direct access to the data item is allowed.
              So we have to gather the address.
            */
            if ((ptEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
              ServerSdo[0].pbData = ptEntry->pData
                + (ServerSdo[0].bSubIndex - 1)
                * (ServerSdo[0].SDO_DATASIZE);
            } /* if */
            else {
              ServerSdo[0].pbData = ptEntry->pData;
            } /* else */
            LOCK_APPLICATION_WDATA;
            switch ((BYTE)ServerSdo[0].SDO_DATASIZE) {
              case 1:
                ServerSdo[0].pbData[0] = ptValue[0];
                break;

              case 2:
                (*((WBYTE*)ServerSdo[0].pbData)).bc.lb = ptValue[0];
                (*((WBYTE*)ServerSdo[0].pbData)).bc.mb = ptValue[1];
                break;

              case 3:
                (*((QBYTE*)ServerSdo[0].pbData)).fb.b0 = ptValue[0];
                (*((QBYTE*)ServerSdo[0].pbData)).fb.b1 = ptValue[1];
                (*((QBYTE*)ServerSdo[0].pbData)).fb.b2 = ptValue[2];
                break;

              case 4:
                (*((QBYTE*)ServerSdo[0].pbData)).fb.b0 = ptValue[0];
                (*((QBYTE*)ServerSdo[0].pbData)).fb.b1 = ptValue[1];
                (*((QBYTE*)ServerSdo[0].pbData)).fb.b2 = ptValue[2];
                (*((QBYTE*)ServerSdo[0].pbData)).fb.b3 = ptValue[3];
                break;
            } /* switch */
            UNLOCK_APPLICATION_WDATA;
          } /* else */
        } /* if */
        else {
          /* we support only up to 4 bytes */
          bRet = FALSE; /* no bytes written */
        } /* else */
      } /* if write access allowed */
      else {
        bRet = FALSE; /* no bytes written */            
      } /* else */
    } /* if */
    else {
      bRet = FALSE; /* no bytes written */
    } /* else */
    /* allocate transfer structure */
  } /* if */
  else {
    bRet = FALSE; /* no bytes written */
  } /* else */
  gSdoS_ClearServerInfo(0);
  return bRet;
} /* gWriteObject */
#endif /* SDO_LOCAL_WRITE == 1 */


#if SDO_LOCAL_READ == 1
/*!
  \brief Read an entry from the object dictionary.

  This function gives read access to an object in the object dictionary.
  The caller does not need to know the address of the read object.
  The index and sub-index information is sufficient.

  \param wIndex Index of object.
  \param bSubIndex Sub-index of object.
  \param ptValue The read value is copied to this location.
  \return number of bytes read
*/
BYTE gReadObject(const WORD wIndex, const BYTE bSubIndex, BYTE XDATA * ptValue)
{
  CONST OD_ENTRY CONST_PTR * ptEntry;
  BYTE DATA bRet = 0; /* signal no data */

  if (ServerSdo[0].bLastState == ST_TRANSFERRED) {
    gSdoS_ClearServerInfo(0);
    /* store index/sub index */
    ServerSdo[0].wbIndex.wc = wIndex;
    ServerSdo[0].bSubIndex = bSubIndex;

    /* search for existence of entry in OD */
    ptEntry = gSdoS_GetEntry((WORD)(ServerSdo[0].wbIndex.wc - 
                sSdoS_GetPdoNumber(0)), ServerSdo[0].bSubIndex);

    /* check valid entry and write bit */
    if (ptEntry != 0) {
      if ((ptEntry->AccArrayClbck & RO) == RO) {
        /*
          Now we need the data location and the length of the item
          in order to answer the request.
        */
        ServerSdo[0].SDO_DATASIZE = ptEntry->Length;
        bRet = (BYTE)ServerSdo[0].SDO_DATASIZE;
        /* fill in data */
        if (ptEntry->pData == 0) {
          sSdoS_GetObjectAddress(0);
          /*
            The data contents are copied to the expedited buffer in the
            structure VSdoAttrib during the call to sSdoS_ReadData().
            Additionally the pointer to the data are adjusted.
           */
          if (sSdoS_ReadData(ptEntry, 0) != 0) {
            VDEBUG_RPTE2("Sdo: No address information for entry %x %x!\n",
            ServerSdo[0].wbIndex.wc, ServerSdo[0].bSubIndex);
            bRet = 0; /* no bytes read */
          } /* if */
          else {
            /* Copy the expedited data directly to destination */
            ptValue[0] = ServerSdo[0].bExpedBuf[0];
            ptValue[1] = ServerSdo[0].bExpedBuf[1];
            ptValue[2] = ServerSdo[0].bExpedBuf[2];
            ptValue[3] = ServerSdo[0].bExpedBuf[3];
          } /* else */
        } /* if */
        else {
          /*
            Get EXPEDITED data directly via address provided
            in OBJDICT.C.
            The data address has to be corrected if we access an array.
          */
          if ((ptEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
              ServerSdo[0].pbData = ptEntry->pData
                + (ServerSdo[0].bSubIndex - 1)
                * (ServerSdo[0].SDO_DATASIZE);
          } /* if */
          else {
            ServerSdo[0].pbData = ptEntry->pData;
          } /* else */

          LOCK_APPLICATION_RDATA;
          switch ((BYTE)ServerSdo[0].SDO_DATASIZE) {
            case 1:
              ptValue[0] = ServerSdo[0].pbData[0];
              break;

            case 2:
              ptValue[0] = (*((WBYTE*)ServerSdo[0].pbData)).bc.lb;
              ptValue[1] = (*((WBYTE*)ServerSdo[0].pbData)).bc.mb;
              break;

            case 3:
              ptValue[0] = (*((QBYTE*)ServerSdo[0].pbData)).fb.b0;
              ptValue[1] = (*((QBYTE*)ServerSdo[0].pbData)).fb.b1;
              ptValue[2] = (*((QBYTE*)ServerSdo[0].pbData)).fb.b2;
              break;

            case 4:
              ptValue[0] = (*((QBYTE*)ServerSdo[0].pbData)).fb.b0;
              ptValue[1] = (*((QBYTE*)ServerSdo[0].pbData)).fb.b1;
              ptValue[2] = (*((QBYTE*)ServerSdo[0].pbData)).fb.b2;
              ptValue[3] = (*((QBYTE*)ServerSdo[0].pbData)).fb.b3;
              break;
          } /* switch */
          UNLOCK_APPLICATION_RDATA;
        } /* else */
      } /* if */
      else {
        bRet = 0; /* no bytes read */
      } /* else */
    } /* if */
    else {
      bRet = 0; /* no bytes read */
    } /* else */
    /* allocate transfer structure */
  } /* if */
  else {
    bRet = 0; /* no bytes read */
  } /* else */
  gSdoS_ClearServerInfo(0);
  return bRet;
} /* gReadObject */
#endif /* SDO_LOCAL_READ == 1 */


#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
/*!
  \brief Enable/disable the second SDO server channel.

  \param fSet    - ON: SDO channel will be enabled / OFF: SDO channel will be disabled.
  \param bNodeId - Second SDO server node id.
  \retval TRUE   - Changing accepted.
  \retval FALSE  - Operation failed.
*/
BYTE gSdoCh2Control(BOOLEAN fSet, BYTE bNodeId)
{
  BYTE bRet = TRUE;    /* return value */

  if (fSet) {   /* enable second SDO channel */
    if (ServerSdo[1].bChannelEnabled == FALSE) {
      /* assign identifier */
      if ((ServerSdo[1].bRxIdEnabled = 
            gCB_BufChangeId((SDO_RX + 1), SDORX_ID + bNodeId)) == FALSE) {
        bRet = FALSE;
      } /* if */
      if ((ServerSdo[1].bTxIdEnabled =
            gCB_BufChangeId((SDO_TX + 1), SDOTX_ID + bNodeId)) == FALSE) {
        bRet = FALSE;
      } /* if */
    } /* if */
    else {
      /* already enabled: only current ID accepted */
      if ((tCanMsgBuffer[SDO_RX + 1].tMsg.qbId.dw != (DWORD)(SDORX_ID + bNodeId)) ||
          (tCanMsgBuffer[SDO_TX + 1].tMsg.qbId.dw != (DWORD)(SDOTX_ID + bNodeId))) {
        bRet = FALSE;
      } /* if */
    } /* else */
  } /* if */
  else {    /* disable second SDO channel */
    /* release identifier */
    gCB_BufEnable(OFF, (SDO_RX + 1));
    gCB_BufEnable(OFF, (SDO_TX + 1));
    ServerSdo[1].bRxIdEnabled = FALSE;
    ServerSdo[1].bTxIdEnabled = FALSE;
  } /* else */

  if ((ServerSdo[1].bRxIdEnabled == TRUE) && (ServerSdo[1].bTxIdEnabled == TRUE)) {
    ServerSdo[1].bChannelEnabled = TRUE;
  } /* if */
  else {
    ServerSdo[1].bChannelEnabled = FALSE;
  } /* else */

  return bRet;
} /* gSdoCh2Control() */
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief SDO server implementation.
  \par File name sdoserv.c
  \version 76
  \date 4.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

