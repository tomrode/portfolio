/*--------------------------------------------------------------------
       MAPPING.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Holds the default mapping
  --------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "portab.h"
#include "cos_main.h"
#include "profil.h"
#include "objdict.h"
#if CHECK_CONSISTENCY==1
# include "sdoserv.h"
# include "vdbgprnt.h"
# include "emcyhndl.h"
#endif /* CHECK_CONSISTENCY==1 */
#include <string.h>
#include "Truck_PDO.h"


/*--------------------------------------------------------------------*/
/*  local definitions                                                 */
/*--------------------------------------------------------------------*/

/*!
  \def MAP_ENTRY(index, subindex, length, address)
  Add a mapping entry to a mapping table structure.
  \param index - mapped object's index
  \param subindex - mapped object's subindex
  \param length - mapped object's length (in bits)
  \param address - start address of data associated with mapped object
*/
#define MAP_ENTRY(index, subindex, length, address) \
{ length, subindex, (BYTE)index, (BYTE)((WORD)index >> 8), address }

/*!
  \def NUM_OF_MAP_ENTRIES(number)
  Adds the first entry of the mapping (numbe rof mapped items)
  to a mapping table structure.
  \param number - number of mapped objects
*/
#define NUM_OF_MAP_ENTRIES(number) \
{ number, 0, 0, 0, (void *)0 }

/*!
  \def VOID_MAP_ENTRY
  This is a placeholder for unused but present mapping entries
  in the mapping table.
*/
#define VOID_MAP_ENTRY { 0, 0, 0, 0, (void *)0 }

extern VComParaArea XDATA gStkGlobDat; /*!< Storage block for non-volatile data. */

#include "mapvars.h" /* include file with references to external variables */

/*--------------------------------------------------------------------*/
/*  public data                                                       */
/*--------------------------------------------------------------------*/

/*!
  \brief Default mapping for receive PDOs.

  This mapping is placed in ROM.
  It should be adapted to the needs of the application in the file rmapping.h.

  \note The size of the array is fully specified, since it is possible
  that the initialization data are fewer than entries in the array.
*/
#if MAX_RX_PDOS > 0
CONST MAPPING CODE atRxPdoDefMap[MAX_RX_PDOS][MAX_MAPPED_OBJECTS] = {
# include "rmapping.h"
};
#endif /* MAX_RX_PDOS > 0 */

/*!
  \brief Default mapping for transmit PDOs.

  This mapping is placed in ROM.
  It should be adapted to the needs of the application in the file tmapping.h.

  \note The size of the array is fully specified, since it is possible
  that the initialization data are fewer than entries in the array.
*/
#if MAX_TX_PDOS > 0
CONST MAPPING CODE atTxPdoDefMap[MAX_TX_PDOS][MAX_MAPPED_OBJECTS] = {
# include "tmapping.h"
};
#endif /* MAX_TX_PDOS > 0 */

/*--------------------------------------------------------------------*/
/*  public functions                                                  */
/*--------------------------------------------------------------------*/

/*!
  \brief Initialization structure
*/
typedef struct { MAPPING m[MAX_MAPPED_OBJECTS]; } FullMapping;

/*!
  \brief Initialise the mapping.

  The mapping information will be copied from the
  ROM (#atRxPdoDefMap, #atTxPdoDefMap) image to the
  RAM (#gStkGlobDat).
*/
void InitMapping(void)
{
#if (RPDO_MAP_READONLY==0) || (TPDO_MAP_READONLY==0)
  BYTE i;
#endif /* (RPDO_MAP_READONLY==0) || (TPDO_MAP_READONLY==0) */

  /* initialise mapping for rx pdo */
#if RPDO_MAP_READONLY==0
# if MAX_RX_PDOS > 0
  for (i = 0; i < MAX_RX_PDOS; i++) {
    if (atRxPdoDefMap[i][0].bObjLength) {
      /* There is a mapping - initialise */
      *(FullMapping *)gStkGlobDat.atRxPdoActMap[i]
        = *(FullMapping FCONST CONST_PTR *)atRxPdoDefMap[i];
    } /* if */
    else {
      /* There is no mapping - set mapping to 0 */
      MemSet(gStkGlobDat.atRxPdoActMap[i], 0x00, sizeof(FullMapping));
    } /* else */
  } /* for */
# endif /* MAX_RX_PDOS > 0 */
#endif

  /* initialise mapping for tx pdo */
#if TPDO_MAP_READONLY==0
# if MAX_TX_PDOS > 0
  for (i = 0; i < MAX_TX_PDOS; i++) {
    if (atTxPdoDefMap[i][0].bObjLength) {
      /* There is a mapping - initialise */
      *(FullMapping *)gStkGlobDat.atTxPdoActMap[i]
        = *(FullMapping FCONST CONST_PTR *)atTxPdoDefMap[i];
    } /* if */
    else {
      /* There is no mapping - set mapping to 0 */
      MemSet(gStkGlobDat.atTxPdoActMap[i], 0x00, sizeof(FullMapping));
    } /* else */
  } /* for */
# endif /* MAX_TX_PDOS > 0 */
#endif
} /* InitMapping */


/*!
  \brief structure of verification table
*/
typedef struct _vVerifyTable {
  FullMapping * mMapTable;
  BYTE bAccess;
  BYTE bTables;
} vVerifyTable;



#if CHECK_CONSISTENCY==1
# if (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0)
/*!
  \brief Verification table

  During startup the consistency of the object dictionary
  and the mapping is checked.
  This table is used to initialize the check.
*/
CONST vVerifyTable CODE VerifyMap[] =
{
#  if MAX_RX_PDOS > 0
  {            (FullMapping *)(BYTE *)atRxPdoDefMap, RXMAP, MAX_RX_PDOS},
#   if RPDO_MAP_READONLY==0
  {(FullMapping *)(BYTE *)gStkGlobDat.atRxPdoActMap, RXMAP, MAX_RX_PDOS},
#   endif /* RPDO_MAP_READONLY==0 */
#  endif /* MAX_RX_PDOS > 0 */
#  if MAX_TX_PDOS > 0
  {            (FullMapping *)(BYTE *)atTxPdoDefMap, TXMAP, MAX_TX_PDOS},
#   if TPDO_MAP_READONLY==0
  {(FullMapping *)(BYTE *)gStkGlobDat.atTxPdoActMap, TXMAP, MAX_TX_PDOS},
#   endif /* TPDO_MAP_READONLY==0 */
#  endif /* MAX_TX_PDOS > 0 */
};



/*!
  \brief Verify the contents of the mapping table.

  For optimization reasons the mapping table holds the address of the
  mapped item.
  Since the mapping table and the object dictionary are most likely to
  be changed by the user inconsistencies may occur.
  These inconsistencies can be trapped with this function during startup.
  If the mapping is changed during runtime inconsistencies are
  already detected in the SDO write routines.
*/
void gVerifyMapping(void)
{
  BYTE k, i, j;
  CONST OD_ENTRY CONST_PTR * ptEntry;

  for (k = 0; k < sizeof(VerifyMap)/sizeof(VerifyMap[0]); k++) {
    for (i = 0; i < VerifyMap[k].bTables; i++) {
      if (gSlvGetMapLen(VerifyMap[k].mMapTable[i].m, 0) > 64) {
        VDEBUG_RPT1("gVerifyMapping(): Message length exceeded (PDO %x)\n",
        i + 1);
        gSetEmcyInternal(0x6266, ON);
        gCheckEmcyTransmit();
      } /* if */
      for (j = 1; j < (VerifyMap[k].mMapTable[i].m[0].bObjLength + 1); j++) {
        /*
        Check the following:
        - object/sub-object combination exists
        - address is correct
        - single object's length is OK
        - access type is OK
        - complete mapping length below 64 bits
        */
        ptEntry = gSdoS_GetEntry(
          (WORD)(((WORD)VerifyMap[k].mMapTable[i].m[j].bIndexMsb << 8)
          | (WORD)(VerifyMap[k].mMapTable[i].m[j].bIndexLsb & 0xFF)),
          VerifyMap[k].mMapTable[i].m[j].bSubIndex);
        if (ptEntry) {
          if ((BYTE)(ptEntry->Length << 3) != VerifyMap[k].mMapTable[i].m[j].bObjLength) {
            VDEBUG_RPT2("gVerifyMapping(): Mapped object's length differs (PDO %x/Entry %x)\n",
              i + 1, j);
            gSetEmcyInternal(0x6262, ON);
            gCheckEmcyTransmit();
          } /* if */
          if ((ptEntry->AccArrayClbck & VerifyMap[k].bAccess) !=
            VerifyMap[k].bAccess) {
            VDEBUG_RPT2("gVerifyMapping(): Access type doesn't fit (PDO %x/Entry %x)\n",
              i + 1, j);
            gSetEmcyInternal(0x6263, ON);
            gCheckEmcyTransmit();
          } /* if */
          if (ptEntry->pData) {
            if ((ptEntry->AccArrayClbck & IS_ARRAY) == IS_ARRAY) {
              if ((ptEntry->pData +
                (VerifyMap[k].mMapTable[i].m[j].bSubIndex - 1) * ptEntry->Length) !=
                VerifyMap[k].mMapTable[i].m[j].pData) {
                VDEBUG_RPT2("gVerifyMapping(): Address differs (PDO %x/Entry %x)\n",
                  i + 1, j);
                gSetEmcyInternal(0x6264, ON);
                gCheckEmcyTransmit();
              } /* if */
            } /* if */
            else {
              /* compare directly */
              if (ptEntry->pData != VerifyMap[k].mMapTable[i].m[j].pData) {
                VDEBUG_RPT2("gVerifyMapping(): Address differs (PDO %x/Entry %x)\n",
                  i + 1, j);
                gSetEmcyInternal(0x6264, ON);
                gCheckEmcyTransmit();
              } /* if */
            } /* else */
          } /* if */
        } /* if */
        else {
          VDEBUG_RPT2("gVerifyMapping(): Object doesn't exist (PDO %x/Entry %x)\n",
            i + 1, j);
          gSetEmcyInternal(0x6265, ON);
          gCheckEmcyTransmit();
        } /* else */
      } /* for */
    } /* for */
  } /* for */
  VDEBUG_RPT0("gVerifyMapping(): Mapping Check completed\n");
} /* gVerifyMapping */ 
# endif /* (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0) */
#endif /* CHECK_CONSISTENCY==1 */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Holds the default mapping.
  \par File name mapping.c
  \version 17
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/


