/*--------------------------------------------------------------------
       TGT32028.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2004 Vector Informatik GmbH, Stuttgart

       Function: Target adaptation for the TI TMS320F28x
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "target.h"


/*--------------------------------------------------------------------*/
/*  test only definitions                                             */
/*--------------------------------------------------------------------*/
#define TEST_ON_EVABOARD 0

/*--------------------------------------------------------------------*/
/*  local definitions                                                 */
/*--------------------------------------------------------------------*/
/* on-chip configuration registers */
#define CANTIOC         *((volatile long *)0x602A)
#define CANRIOC         *((volatile long *)0x602C)
#define PCLKCR          *((volatile int *)0x701C)
#define PLLCR           *((volatile int *)0x7021)
#define SCSR            *((volatile int *)0x7022)
#define WDCNTR          *((volatile int *)0x7023)
#define WDKEY           *((volatile int *)0x7025)
#define WDCR            *((volatile int *)0x7029)
#define GPFMUX          *((volatile int *)0x70D4)
#define GPFDIR          *((volatile int *)0x70D5)
#define GPFDAT          *((volatile int *)0x70F4)
#define GPFSET          *((volatile int *)0x70F5)
#define GPFCLR          *((volatile int *)0x70F6)
#define GPFTOG          *((volatile int *)0x70F7)
#define TIMER0TIM       *((volatile int *)0x0C00)
#define TIMER0TIMH      *((volatile int *)0x0C01)
#define TIMER0PRD       *((volatile int *)0x0C02)
#define TIMER0PRDH      *((volatile int *)0x0C03)
#define TIMER0TCR       *((volatile int *)0x0C04)
#define TIMER0TPR       *((volatile int *)0x0C06)
#define TIMER0TPRH      *((volatile int *)0x0C07)
#define PIECTRL         *((volatile int *)0x0CE0)
#define PIEACK          *((volatile int *)0x0CE1)
#define PIEIER1         *((volatile int *)0x0CE2)
#define PIEIFR1         *((volatile int *)0x0CE3)
#define PIEIER9         *((volatile int *)0x0CF2)
#define PIEIFR9         *((volatile int *)0x0CF3)


/*--------------------------------------------------------------------*/
/*  prototype declarations                                            */
/*--------------------------------------------------------------------*/
interrupt void INT13_ISR(void);     /* XINT13 or CPU-Timer 1  */
interrupt void INT14_ISR(void);     /* CPU-Timer2             */
interrupt void DATALOG_ISR(void);   /* Datalogging interrupt  */
interrupt void RTOSINT_ISR(void);   /* RTOS interrupt         */
interrupt void EMUINT_ISR(void);    /* Emulation interrupt    */
interrupt void NMI_ISR(void);       /* Non-maskable interrupt */
interrupt void ILLEGAL_ISR(void);   /* Illegal operation TRAP */

interrupt void PIE_RESERVED(void);
interrupt void rsvd_ISR(void);
interrupt void Tim_ISR(void);


/*--------------------------------------------------------------------*/
/*  external functions                                                */
/*--------------------------------------------------------------------*/
void Tim_PIT(void);
interrupt void gCan_SysIntHandler(void);
interrupt void gCan_MsgIntHandler(void);
extern void vReenable_Scheduler(void);


/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/
typedef interrupt void(*PINT)(void); /* pointer to interrupt */

typedef struct { /* Vector Table Declaration */
  /* Reserved Vectors */
  PINT PIE1_RESERVED;  
  PINT PIE2_RESERVED;
  PINT PIE3_RESERVED;
  PINT PIE4_RESERVED;
  PINT PIE5_RESERVED;
  PINT PIE6_RESERVED;
  PINT PIE7_RESERVED;
  PINT PIE8_RESERVED;
  PINT PIE9_RESERVED;
  PINT PIE10_RESERVED;
  PINT PIE11_RESERVED;
  PINT PIE12_RESERVED;
  PINT PIE13_RESERVED;

  /* Non-Peripheral Vectors */
  PINT XINT13;
  PINT TINT2;
  PINT DATALOG;
  PINT RTOSINT;
  PINT EMUINT;
  PINT XNMI;
  PINT ILLEGAL;
  PINT USER1;
  PINT USER2;
  PINT USER3;
  PINT USER4;
  PINT USER5;
  PINT USER6;
  PINT USER7;
  PINT USER8;
  PINT USER9;
  PINT USER10;
  PINT USER11;
  PINT USER12;
      
  /* Group 1 PIE Peripheral Vectors */
  PINT PDPINTA;
  PINT PDPINTB;
  PINT rsvd1_3;
  PINT XINT1;     
  PINT XINT2;
  PINT ADCINT;
  PINT TINT0;
  PINT WAKEINT;
           
  /* Group 2 PIE Peripheral Vectors */
  PINT CMP1INT;
  PINT CMP2INT;
  PINT CMP3INT;
  PINT T1PINT;
  PINT T1CINT;
  PINT T1UFINT;
  PINT T1OFINT;
  PINT rsvd2_8;
      
  /* Group 3 PIE Peripheral Vectors */
  PINT T2PINT;
  PINT T2CINT;
  PINT T2UFINT;
  PINT T2OFINT;
  PINT CAPINT1;
  PINT CAPINT2;
  PINT CAPINT3;
  PINT rsvd3_8;
      
  /* Group 4 PIE Peripheral Vectors */
  PINT CMP4INT;
  PINT CMP5INT;
  PINT CMP6INT;
  PINT T3PINT;
  PINT T3CINT;
  PINT T3UFINT;
  PINT T3OFINT;
  PINT rsvd4_8;      
     
  /* Group 5 PIE Peripheral Vectors */
  PINT T4PINT;
  PINT T4CINT;
  PINT T4UFINT;
  PINT T4OFINT;
  PINT CAPINT4;
  PINT CAPINT5;
  PINT CAPINT6;
  PINT rsvd5_8;      

  /* Group 6 PIE Peripheral Vectors */
  PINT SPIRXINTA;
  PINT SPITXINTA;
  PINT rsvd6_3;
  PINT rsvd6_4;
  PINT MRINTA;
  PINT MXINTA;
  PINT rsvd6_7;
  PINT rsvd6_8;
      
  /* Group 7 PIE Peripheral Vectors */
  PINT rsvd7_1;
  PINT rsvd7_2;
  PINT rsvd7_3;
  PINT rsvd7_4;
  PINT rsvd7_5;
  PINT rsvd7_6;
  PINT rsvd7_7;
  PINT rsvd7_8;

  /* Group 8 PIE Peripheral Vectors */
  PINT rsvd8_1;
  PINT rsvd8_2;
  PINT rsvd8_3;
  PINT rsvd8_4;
  PINT rsvd8_5;
  PINT rsvd8_6;
  PINT rsvd8_7;
  PINT rsvd8_8; 

  /* Group 9 PIE Peripheral Vectors */
  PINT RXAINT;
  PINT TXAINT;
  PINT RXBINT;
  PINT TXBINT;
  PINT ECAN0INTA;
  PINT ECAN1INTA;
  PINT rsvd9_7;
  PINT rsvd9_8;

  /* Group 10 PIE Peripheral Vectors */
  PINT rsvd10_1;
  PINT rsvd10_2;
  PINT rsvd10_3;
  PINT rsvd10_4;
  PINT rsvd10_5;
  PINT rsvd10_6;
  PINT rsvd10_7;
  PINT rsvd10_8;
            
  /* Group 11 PIE Peripheral Vectors */
  PINT rsvd11_1;
  PINT rsvd11_2;
  PINT rsvd11_3;
  PINT rsvd11_4;
  PINT rsvd11_5;
  PINT rsvd11_6;
  PINT rsvd11_7;
  PINT rsvd11_8;

  /* Group 12 PIE Peripheral Vectors */
  PINT rsvd12_1;
  PINT rsvd12_2;
  PINT rsvd12_3;
  PINT rsvd12_4;
  PINT rsvd12_5;
  PINT rsvd12_6;
  PINT rsvd12_7;
  PINT rsvd12_8;
}
PIE_VECT_TABLE;


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern cregister volatile unsigned int IFR;
extern cregister volatile unsigned int IER;


/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/
#define PieVectTable (*((PIE_VECT_TABLE *)0x0D00)) /* Vector Table Location */
const PIE_VECT_TABLE PieVectTableInit = { /* Vector Table Definition */
  /* Reserved space */
  PIE_RESERVED,
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   
  PIE_RESERVED,   

  /* Non-Peripheral Interrupts */
  INT13_ISR,
  INT14_ISR,
  DATALOG_ISR,
  RTOSINT_ISR,
  EMUINT_ISR,
  NMI_ISR,
  ILLEGAL_ISR,
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   

  /* Group 1 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   

  /* Group 2 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
      
  /* Group 3 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
      
  /* Group 4 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
     
  /* Group 5 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   

  /* Group 6 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
      
  /* Group 7 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   

  /* Group 8 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
      
  /* Group 9 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
      
  /* Group 10 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
            
  /* Group 11 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   

  /* Group 12 PIE Vectors */
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
  rsvd_ISR,   
};


/*--------------------------------------------------------------------*/
/*  public functions                                                  */
/*--------------------------------------------------------------------*/

/*!
  Enable all interrupts.
*/
void Tgt_EnableInterrupts(void)
{
  asm(" clrc INTM");
  asm(" clrc DBGM");
}


/*!
  Disable all interrupts.
*/
void Tgt_DisableInterrupts(void)
{
  asm(" setc INTM");
  asm(" setc DBGM");
}


/*!
  \brief Configure connection between CAN and CPU.

  This function configures the connection between the CAN controller
  used and the microcontroller. It is called from the CAN module.
  Any initialization regarding interrupt behaviour and chip signaling
  should be done here.
*/
void Can_Configure(void)
{
  asm(" eallow"); /* disable protection */
  PieVectTable.ECAN0INTA = gCan_SysIntHandler;
  PieVectTable.ECAN1INTA = gCan_MsgIntHandler;
  asm(" edis"); /* enable protection */

  IER |= 0x0100; /* enable INT9 */
  /* enable ECAN0INTA and ECAN1INTA in the PIE: Group 9 interrupt 5 and 6 */
  PIEIER9 |= 0x0030;
  Tgt_EnableInterrupts();
}


/*!
  \brief Configure connection between timer and CPU.

  This function configures the connection between the timer used and
  the microcontroller. It is called from the timer module.
  Any initialization regarding interrupt behaviour, chip signaling
  and start values should be done here.
*/
void Tim_ConfigureTimer(void)
{
  /* NOTICE: this timings are for 150 MHz CPU clock speed */
  /*         1 ms cycle with L = 0x49F0, H = 0x0002       */
  /*         2 ms cycle with L = 0x93E0, H = 0x0004       */
  /*         5 ms cycle with L = 0x71B0, H = 0x000B       */

  asm(" eallow"); /* disable protection */
  PieVectTable.TINT0 = Tim_ISR;
  asm(" edis"); /* enable protection */
  TIMER0TCR = 0x8810; /* stop timer 0 */
  TIMER0PRD  = 0x71AF; /* set reload value to 5 ms */
  TIMER0PRDH = 0x000B;
  TIMER0TPR = 0; /* set pre-scaler to divide by 1 */
  TIMER0TPRH = 0;

  IER |= 0x0001; /* enable INT1 */
  PIEIER1 |= 0x0040; /* enable TINT0 in the PIE: Group 1 interrupt 7 */
  Tgt_EnableInterrupts();
}


/*!
  \brief Enable timer.

  This function enables the timer. It is called from the timer module.
*/
void Tim_EnableTimer(void)
{
#if TEST_ON_EVABOARD == 1
  GPFSET = 0x4000;
#endif
  /* start timer 0 */
  TIMER0TCR = 0x4820;
}


/*!
  \brief Disable timer.

  This function disables the timer. It is called from the timer module.
*/
void Tim_DisableTimer(void)
{

#if TEST_ON_EVABOARD == 1
  GPFCLR = 0x4000;
#endif
  /* stop timer 0 */
  TIMER0TCR = 0x8810;
}


/*!
  \brief Reload timer.

  This function reloads the timer. It is called from the timer module.
*/
void Tim_ReloadTimer(void)
{
  ;
}


/*!
  \brief Reset timer interrupt request.

  This function clears a pending timer interrupt request.
  It is called from the timer module.
*/
void Timer_ClearPendingInterrupt(void)
{
  /* acknowledge interrupt */
  PIEACK = 1;
  /* re-enable IRQ's from this group */
  IER |= 0x0001; /* enable INT1 */
}


/*!
  \brief Prepare the environment.

  This function is called from the protocol stack to do necessary
  settings after module startup.
*/
void PrepareEnvironment(void)
{
  volatile int dummy;
  unsigned int i;
  unsigned long *Source = (void *) &PieVectTableInit;
  unsigned long *Dest = (void *) &PieVectTable;

  /* disable all interrupts */
  asm(" setc INTM");
  /* disable protection */
  asm(" eallow");
  /* disable watch dog timer */
#if 0  /* this is done in the hardware init sections outside this stack */
  WDCR = 0x0068;
  /* enable PLL to CLKIN = (OSCCLK * 10.0)/2 */
  PLLCR = 0x000A;
  /* wait for the PLL to lock */
  for (dummy = 0; dummy < ((131072 / 2) / 12); dummy++);
  /* enable clock to the CAN module */
  PCLKCR |= 0x4000;
#endif
  /* configure CAN pins using GPIO */
  GPFMUX |= 0x00C0;
#if TEST_ON_EVABOARD == 1
  GPFDIR |= 0x4000;
  GPFCLR = 0x4000;
#endif
  /* connect CANTX pin */
  CANTIOC = 0x08;
  /* connect CANRX pin */
  CANRIOC = 0x08;
  /* disable PIE */
  PIECTRL = 0;
  /* disable CPU interrupts */
  IER = 0x0000;
  /* disable CPU interrupt flags */
  IFR = 0x0000;
  /* copy vector table */
  for (i = 0; i < 128; i++) {
    *Dest++ = *Source++;    
  } /* for */
  /* enable PIE */
  PIECTRL = 1;
  /* ring the CPU */
  PIEACK = 0xFFFF;
  /* enable protection */
  asm(" edis");
}


/*!
  \brief Timer ISR.

  The timer interrupt service routine.
  \sa Tim_PIT()
*/
interrupt void Tim_ISR(void) 
{
  
#if TEST_ON_EVABOARD == 1
  /* test only */
  static unsigned int i = 0;
  if (i++ == 100) {
    GPFTOG = 0x4000;
    i = 0;
  } /* if */
#endif

  Tim_PIT();
}


interrupt void INT13_ISR(void) { ; }   /* XINT13 or CPU-Timer 1  */
interrupt void INT14_ISR(void) { ; }   /* CPU-Timer2             */
interrupt void DATALOG_ISR(void) { ; } /* Datalogging interrupt  */
interrupt void RTOSINT_ISR(void) { ; } /* RTOS interrupt         */
interrupt void EMUINT_ISR(void) { ; }  /* Emulation interrupt    */
interrupt void NMI_ISR(void) { ; }     /* Non-maskable interrupt */
interrupt void ILLEGAL_ISR(void) { ; } /* Illegal operation TRAP */
interrupt void PIE_RESERVED(void) { ; }
interrupt void rsvd_ISR(void) 
{ 
    ; 
}


/*!
  \brief Target reset.

  This function does actually reset this device.
  Possible solutions can be to wait for the watchdog, to trigger a
  reset logic or to execute the startup code.
  \sa ModulReset()
*/
void Tgt_Reset(void)
{
  /* enable watch dog timer */
    asm("   eallow");

    SCSR = 1;
    WDCR = 0x0028;
    asm(" RPT #7 || NOP");
    /* smack the dog */
    WDKEY = 0x0040;
    asm("   edis");
}


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Target adaptation for the Texas Instruments TMS320F28x.
  \par File name tgt32028.c
  \version 5
  \date 1.04.04 15:26
  \author (c) 1998-2004 by Vector Informatik GmbH, Stuttgart

  The definitions included determine the actual hardware layout
  of your board. Fit it to your requirements.
*/
