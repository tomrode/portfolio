/*--------------------------------------------------------------------
       LMTSLV.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for CANopen slave LMT support
  --------------------------------------------------------------------*/

#ifndef _LMTSLV_H_
#define _LMTSLV_H_



/*--------------------------------------------------------------------*/
/*  global definitions                                                */
/*--------------------------------------------------------------------*/

/*!
  \defgroup lmt_cs LMT command specifier
  @{
*/

#define LMT_CS_SW_MANUFACT_NAME      1  /*!< LMT switch mode selective manufacturer name */
#define LMT_CS_SW_PRODUCT_NAME       2  /*!< LMT switch mode selective product name*/
#define LMT_CS_SW_SERIAL_NUMBER      3  /*!< LMT switch mode selective serial number */
#define LMT_CS_ID_MANUFACT_NAME      5  /*!< LMT identify remote slave manufacturer name */
#define LMT_CS_ID_PRODUCT_NAME       6  /*!< LMT identify remote slave product name */
#define LMT_CS_ID_SERIAL_LOW         7  /*!< LMT identify remote slave serial-number low */
#define LMT_CS_ID_SERIAL_HIGH        8  /*!< LMT identify remote slave serial-number high */
#define LMT_CS_ID_SLAVE              9  /*!< LMT identify slave */
#define LMT_CS_CNF_MODULE_NAME      18  /*!< LMT configure module name */
#define LMT_CS_INQ_MANUFACT_NAME    36  /*!< LMT inquire manufacturer name */
#define LMT_CS_INQ_PRODUCT_NAME     37  /*!< LMT inquire product name */
#define LMT_CS_INQ_SERIAL_NUMBER    38  /*!< LMT inquire serial number */

/*!
  @} end of group lmt_cs
*/


/*!
  \def LMT_CLASS
  Determine the layer management class of the node.

  With this statement you can determine the level of executable
  LMT services of the node.

  The values 1 and 2 are allowed for this define.
  - 1 All LMT services with exception of Switch Mode Selective, Inquire 
    LMT Address, Identify Remote Slaves and Identify Slaves are implemented.
  - 2 All mandatory LMT services are implemented.
*/
#define LMT_CLASS 2

/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void gLmtServiceHandler(void);
void gBintoBCD(BYTE*, DWORD);
#endif /* _LMTSLV_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for LMT slave services.
  \par File name lmtslv.h
  \version 2
  \date 6.03.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
