/*--------------------------------------------------------------------
       LSS_MST.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for LSS master services
  --------------------------------------------------------------------*/

#ifndef _LSS_MST_H_
#define _LSS_MST_H_



/*--------------------------------------------------------------------*/
/*  global definitions                                                */
/*--------------------------------------------------------------------*/

/* Default IDs as defined in the CANopen LSS DSP 305 specification. */
#define LSS_SLAVE_ID    2020    /*!< COB ID 2020 LSS service        */
#define LSS_MASTER_ID   2021    /*!< COB ID 2021 LSS service        */

/*!
  \defgroup lss_cs LSS command specifier
  @{
*/

#define LSS_CS_SWITCH_MODE_GL       4   /*!< LSS switch mode global */
#define LSS_CS_CONFIG_NODE_ID       17  /*!< LSS configure Node-ID */
#define LSS_CS_CONFIG_BIT_PAR       19  /*!< LSS configure bit timing */
#define LSS_CS_ACTIV_BIT_PAR        21  /*!< LSS activate bit timing */
#define LSS_CS_STORE_CONFIG         23  /*!< LSS store configuration */
#define LSS_CS_SWITCH_VENDOR        64  /*!< LSS switch mode selective vendor-id */
#define LSS_CS_SWITCH_PRODUCT       65  /*!< LSS switch mode selective product-code */
#define LSS_CS_SWITCH_REVISION      66  /*!< LSS switch mode selective revision-number */
#define LSS_CS_SWITCH_SERIAL        67  /*!< LSS switch mode selective serial-number */
#define LSS_CS_SWITCH_RESPONSE      68  /*!< LSS switch mode selective response */
#define LSS_CS_IDENT_VENDOR         70  /*!< LSS identify remote slave vendor-id */
#define LSS_CS_IDENT_PRODUCT        71  /*!< LSS identify remote slave product-code */
#define LSS_CS_IDENT_REV_LOW        72  /*!< LSS identify remote slave revision-number low */
#define LSS_CS_IDENT_REV_HIGH       73  /*!< LSS identify remote slave revision-number high */
#define LSS_CS_IDENT_SER_LOW        74  /*!< LSS identify remote slave serial-number low */
#define LSS_CS_IDENT_SER_HIGH       75  /*!< LSS identify remote slave serial-number high */
#define LSS_CS_IDENT_NO_CONF_MST    76  /*!< LSS identify non-configured remote slave */
#define LSS_CS_IDENT_NO_CONF_SLV    80  /*!< LSS identify non-configured slave */
#define LSS_CS_IDENT_SLAVE          79  /*!< LSS identify slave */
#define LSS_CS_INQ_ID_VENDOR        90  /*!< LSS inquire identity vendor-id */
#define LSS_CS_INQ_ID_PRODUCT       91  /*!< LSS inquire identity product-code */
#define LSS_CS_INQ_ID_REVISION      92  /*!< LSS inquire identity revision-number */
#define LSS_CS_INQ_ID_SERIAL        93  /*!< LSS inquire identity serial-number */
#define LSS_CS_INQ_ID_NODE          94  /*!< LSS inquire identity node-id */

/*!
  @} end of group lss_cs
*/

/* LSS definitions */
#define	LSS_INVALID_NODE_ID		      0xFF /*!< LSS invalid node_ID */

/* actual lss state */
#define LSS_STS_IDLE                1    /*!< no service in execution */
#define LSS_STS_COMPLETE            2    /*!< service is complete */
#define LSS_STS_RUNNING             3    /*!< a service is in process */
#define LSS_STS_WAIT                4    /*!< service wait for completion */
#define LSS_STS_ERROR               5    /*!< service has generated an error */

/* error specifier */
#define LSS_ERR_NO_TX               30  /*!< service not sended */
#define LSS_ERR_TIMEOUT             31  /*!< service was timed out */
#define LSS_ERR_UNEXPECTED_CS       32  /*!< unexpected command specifier */
#define LSS_ERR_UNKNOWN_SERV        33  /*!< unknown service specifier */

/* service specifier */
#define LSS_SW_M_GLOB               10  /*!< service switch mode global */
#define LSS_SW_M_SEL                11  /*!< service switch mode selective */
#define LSS_CNF_NODE_ID             12  /*!< service configure node-ID */
#define LSS_CNF_BIT_PARA            13  /*!< service configure bit timing parameters */
#define LSS_ACT_BIT_PARA            14  /*!< service activate bit timing parameter */
#define LSS_STORE_CNF               15  /*!< service store configuration */
#define LSS_INQ_VENDOR_ID           16  /*!< service inquire identity vendor-id*/
#define LSS_INQ_PRODUCT_CODE        17  /*!< service inquire identity product-code*/
#define LSS_INQ_REVISION_NO         18  /*!< service inquire identity revision number*/
#define LSS_INQ_SERIAL_NO           19  /*!< service inquire identity serial number*/
#define LSS_INQ_NODE_ID             20  /*!< service inquire identity node-id*/
#define LSS_ID_SLV                  21  /*!< service identify remote slave */
#define LSS_ID_NO_CNF_SLV           22  /*!< service identify non-configured slave */

/* service substates */
#define LSS_VENDOR_ID               1    /*!< vendor-ID */
#define LSS_PRODUCT_CODE            2    /*!< product-code */
#define LSS_REVISION_NO             3    /*!< revision-number */
#define LSS_SERIAL_NO               4    /*!< serial-number */
#define LSS_NODE_ID                 5    /*!< node-ID */
#define LSS_REVISION_NO_HIGH        6    /*!< revision-number high */
#define LSS_REVISION_NO_LOW         7    /*!< revision-number low */
#define LSS_SERIAL_NO_HIGH          8    /*!< serial number high */
#define LSS_SERIAL_NO_LOW           9    /*!< serial number low */

#define LSS_TX_DELAY                100  /*!< delay in ms between two tx */
/*!< 
  time out in ms of any service 

  Remember that time must be longer than the identify remote slave service
  needs for execution, because between every message a break with duration of
  #LSS_TX_DELAY is inserted.
*/
#define LSS_TIMEOUT                 1000 

/* LSS modes */
#define LSS_OPERATION               0    /*!< operation mode */
#define LSS_CONFIGURATION           1    /*!< configuration mode */
/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/

# if LSS_IMPLEMENTATION_LVL_3 == 1  
typedef struct _VSelector
{
  QBYTE dwVendor;
  QBYTE dwProduct;
  QBYTE dwRevision;
  QBYTE dwSerial;
}VSelector;
# endif /* LSS_IMPLEMENTATION_LVL_3 == 1 */ 

# if LSS_IMPLEMENTATION_LVL_4 == 1  
typedef struct _VIdentify
{
  QBYTE dwVendor;
  QBYTE dwProduct;
  QBYTE dwRevLow;
  QBYTE dwRevHigh;
  QBYTE dwSerLow;
  QBYTE dwSerHigh;
}VIdentify;
# endif /* LSS_IMPLEMENTATION_LVL_4 == 1 */

# if LSS_IMPLEMENTATION_LVL_2 == 1  
typedef struct _VBtTbl
{
  BYTE bTblSelect;
  BYTE bTblIndex;
}VBtTbl;
# endif /* LSS_IMPLEMENTATION_LVL_2 == 1 */  

typedef union _VLssParam
{
  BYTE bMode;                 /* lss switch mode */
  BYTE bNid;                  /* node-ID */
# if LSS_IMPLEMENTATION_LVL_3 == 1  
  VSelector tSelector;        /* parameters for switch selective service */
# endif /* LSS_IMPLEMENTATION_LVL_3 == 1 */
# if LSS_IMPLEMENTATION_LVL_4 == 1  
  VIdentify tIdentify;        /* parameters for identification service */
# endif /* LSS_IMPLEMENTATION_LVL_4 == 1 */
# if LSS_IMPLEMENTATION_LVL_2 == 1  
  VBtTbl tBtTable;            /* parameter for config bit timing service */
  WBYTE wSwDelay;             /*  bit timing activation time */
# endif /* LSS_IMPLEMENTATION_LVL_2 == 1  */
}VLssParam;

/*! structure of LSS control information */
typedef struct {
  WORD XDATA wTimeOut;      /*!< time out of services */
  WORD XDATA wLssTxDelay;   /*!< delay between two lss messages */
  BYTE XDATA bLssState;     /*!< service state */
  BYTE XDATA bOldModuleState; /*!< old state of stack before exec service */
  BYTE XDATA bReqServ;      /*!< requested service */
  BYTE XDATA bExpectCs;     /*!< expected command spec. in slave response msg */
  BYTE XDATA bErrorSpec;    /*!< error specifier if an error occurs*/
# if (LSS_IMPLEMENTATION_LVL_3 == 1) || (LSS_IMPLEMENTATION_LVL_4 == 1)
  BYTE XDATA bSubState;     /*!< service substate */
# endif /* (LSS_IMPLEMENTATION_LVL_3 == 1) || (LSS_IMPLEMENTATION_LVL_4 == 1) */  
# if LSS_IMPLEMENTATION_LVL_2 == 1
  BYTE XDATA bBaudRate;       /*!< baud rate for config bit timing service */
  BYTE XDATA bLssSwitchStage; /*!< LSS stage for activate service */
  WORD XDATA wLssSwitchDelay; /*!< LSS delay time for activate service */
# endif /* LSS_IMPLEMENTATION_LVL_2 == 1 */
}VLssCntrl;

/*! service error codes */
typedef struct {
  BYTE bErrorCode;      /*!< lss error code */
  BYTE bSpecErrCode;    /*!< lss specific error code */
}VError;

/*! return value from a called service */
typedef union {
  QBYTE lInqID;     /*!< inquired identity (vendor,product,rev.,ser.,node-ID */
  VError tError;    /*!< stores the error codes if the service returned this */
}VRespDat;

/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void gLssActivateTimer(WORD);
void gLssActivateBitTiming(void);
void gLssTimer(WORD);
void gHandleLssMsg(void);
void gLayerServices(void);
void gInitLss(void);
BYTE gLssStateMachine(void);
BYTE gLssExecuteServ(BYTE, VLssParam*);

#endif /* _LSS_MST_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for LSS master services.
  \par File name lss_mst.h
  \version 4
  \date 24.03.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
