/*--------------------------------------------------------------------
       USRCLBCK.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for the CANopen slave user callbacks
  --------------------------------------------------------------------*/

#ifndef _USRCLBCK_H_
#define _USRCLBCK_H_

/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
#if (SDO_WRITE_SEG_ALLOWED == 0) && (SDO_READ_SEG_ALLOWED == 0)
BYTE
#else
DWORD
#endif /* (SDO_WRITE_SEG_ALLOWED == 0) && (SDO_READ_SEG_ALLOWED == 0) */
gSdoS_GetObjLenCb(WORD, BYTE);

#if SDO_EXEC_USER_READ_CALLBACKS == 1
BYTE gSdoS_UpCb(const VSdoAttrib XDATA *);
#endif /* SDO_EXEC_USER_READ_CALLBACKS 1 */
#if SDO_EXEC_USER_WRITE_CALLBACKS == 1
BYTE gSdoS_ExpDownCb(const VSdoAttrib XDATA *);
void gSdoS_ExpDownCbFinish(const VSdoAttrib XDATA *);
# if SDO_WRITE_SEG_ALLOWED==1
BYTE gSdoS_DownCb(const VSdoAttrib XDATA *);
BYTE gSdoS_DownCbFinish(const VSdoAttrib XDATA *);
# endif /* SDO_WRITE_SEG_ALLOWED==1 */
#endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */

BYTE XDATA * gSdoS_GetObjectAddress(const VSdoAttrib XDATA *ptServerSdoInst);

#if CLIENT_ENABLE == 1
VOID gSdoC_Completed(BYTE);
#endif /* CLIENT_ENABLE == 1 */

#if QUEUED_MODE == 1
# if SIGNAL_EMCYMSG == 1
void EmcyEventRcv(BYTE, BYTE *);
# endif /* SIGNAL_EMCYMSG == 1 */

# if SIGNAL_BOOTUPMSG == 1
void BootUpEventRcv(BYTE);
# endif /* SIGNAL_BOOTUPMSG == 1 */
#endif /* QUEUED_MODE == 1 */

#if SIGNAL_SYNC == 1
void SyncEventRcv(void);
#endif /* SIGNAL_SYNC == 1 */

#if MAX_RX_PDOS > 0
# if PDO_EXEC_USER_RECEIVE_CALLBACKS == 1
void gSlvRxPdoCallback(BYTE);
# endif /* PDO_EXEC_USER_RECEIVE_CALLBACKS == 1 */
#endif /* MAX_RX_PDOS > 0 */

#if SIGNAL_STACK_EVENT==1
void gSlvStackEventCallback(BYTE, BYTE);
#endif /* SIGNAL_STACK_EVENT==1 */

void Process(void);   /* place your process data evaluation code here */
#if GENERIC_PROFILE_USED == 1
void ProcessPreoperational(void);
#endif /* GENERIC_PROFILE_USED == 1 */

BOOLEAN Version3BootUp(void);       /* determine BootUp mode   */
BYTE ReadBoardAdr(void);            /* get modul ID            */
BYTE ReadBaudRate(void);            /* get baud rate           */
void ModulInit(void);               /* hardware initialisation */
void ModulReset(void);              /* reset the module        */
#endif /* _USRCLBCK_H_ */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for the user callbacks.
  \par File name usrclbck.h
  \version 12
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
