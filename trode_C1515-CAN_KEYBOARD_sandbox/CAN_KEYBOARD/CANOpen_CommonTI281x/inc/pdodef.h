/*--------------------------------------------------------------------
       PDODEF.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: PDO functions header
  --------------------------------------------------------------------*/


#ifndef _PDODEF_H_
#define _PDODEF_H_


/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/
/*! set the receive PDO analyze function */
#if MAX_RX_PDOS > 0
# if (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1)
#  define ANALYZE_PDO(mapping, buffer) gSlvAnalyzePDO(buffer)
# else
#  define ANALYZE_PDO(mapping, buffer) gSlvAnalyzePDO(mapping, buffer)
# endif /* (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1) */
#endif /* MAX_RX_PDOS > 0 */

/*! set the transmit PDO create function */
#if MAX_TX_PDOS > 0
# if (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1)
#  define CREATE_PDO(mapping, buffer) gSlvCreatePDO(buffer)
# else
#  define CREATE_PDO(mapping, buffer) gSlvCreatePDO(mapping, buffer)
# endif /* (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1) */
#endif /* MAX_TX_PDOS > 0 */


/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
#if MAX_RX_PDOS > 0
void gSlvSetRpdoIdent(void);
# if (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1)
void gSlvAnalyzePDO(BYTE);
# else
void gSlvAnalyzePDO(CONST MAPPING PDO_MAPPING_PTR *, BYTE);
# endif /* (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1) */
#endif /* MAX_RX_PDOS > 0 */


#if MAX_TX_PDOS > 0
void gSlvSendPDO(BYTE, BYTE);
void gSlvRstPDOTxReq(void);
void gSlvSetTpdoIdent(void);
# if (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1)
BOOLEAN gSlvCreatePDO(BYTE);
# else
BOOLEAN gSlvCreatePDO(CONST MAPPING PDO_MAPPING_PTR *, BYTE);
# endif /* (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1) */
#endif /* MAX_TX_PDOS > 0 */


#if (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0)
BYTE gSlvGetPDOParIndex(BYTE);
#endif /* (MAX_RX_PDOS > 0) || (MAX_TX_PDOS > 0) */


#endif /* _PDODEF_H_ */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief PDO functions header.
  \par File name pdodef.h
  \version 3
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

