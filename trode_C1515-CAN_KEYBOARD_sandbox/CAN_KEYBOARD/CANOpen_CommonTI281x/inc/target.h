/*--------------------------------------------------------------------
       TARGET.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for the target adaptation
  --------------------------------------------------------------------*/
                                
#ifndef _TARGET_H_
#define _TARGET_H_



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void Can_Configure(void);           /* connect can */

void Tim_ConfigureTimer(void);      /* configure timer */
void Tim_EnableTimer(void);         /* enable timer */
void Tim_DisableTimer(void);        /* stop timer */
void Tim_ReloadTimer(void);         /* reload a timer */

void PrepareEnvironment(void);      /* prepare runtime environment */
void Timer_ClearPendingInterrupt(void); /* reset timer interrupt request */

void Tgt_EnableInterrupts(void);    /* enable interrupts */
void Tgt_DisableInterrupts(void);   /* disable interrupts */
void Tgt_Reset(void);               /* reset this device */

#endif /* _TARGET_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for the target adaptation.
  \par File name target.h
  \version 5
  \date 24.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

