/*--------------------------------------------------------------------
       SDOSERV.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for the SDO server
  --------------------------------------------------------------------*/

/*
START CONFIGURATOR CONTROL UNIT

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
SDO_TIMEOUT             | SDO Handling      | GENERAL |    INT  |  10 | 0xFFFF
SDO_DATA_BUFFER_SIZE    | SDO Handling      | GENERAL |    INT  |   4 | 

END CONFIGURATOR CONTROL UNIT
*/

#ifndef _SDOSERV_H_
#define _SDOSERV_H_



/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/
/*! Number of allowed simultaneously open SDO connections (client side) */
#define NUM_SDO_CLIENTS 1

/*! 
  \def SDO_DATA_BUFFER_SIZE
  Buffer size for SDO download data.
  
  Configure at least 4 bytes (needed for expedited transfers).
*/
#define SDO_DATA_BUFFER_SIZE 100

/*!
  \def SDO_TIMEOUT
  Time out value for client SDO access.
*/
#define SDO_TIMEOUT 2000

/*
  Command specifiers used for expedited and segmented sdo transfer.
*/
#define CCS_INIT_UPLOAD      0x40 /*!< ccs = 2 */
#define SCS_INIT_UPLOAD      0x40 /*!< scs = 2 */

#define CCS_SEGMENT_UPLOAD   0x60 /*!< ccs = 3 */
#define SCS_SEGMENT_UPLOAD   0x0  /*!< scs = 0 */

#define CCS_INIT_DOWNLOAD    0x20 /*!< ccs = 1 */
#define SCS_INIT_DOWNLOAD    0x60 /*!< scs = 3 */

#define CCS_SEGMENT_DOWNLOAD 0x0  /*!< ccs = 0 */
#define SCS_SEGMENT_DOWNLOAD 0x20 /*!< scs = 1 */

#define ABORT_DOMAIN         0x80 /*!< ccs/scs = 4 */

/*
  Command specifiers used for sdo block transfer.
*/
#if SDO_BLOCK_ALLOWED == 1
# define SS_INIT   0x00 /*!< server subcommand initiate response */
# define SS_BLOCK  0x02 /*!< server subcommand block download response */
# define SS_END    0x01 /*!< server subcommand end block download response */

# define CS_INIT   0x00 /*!< client subcommand initiate request */
# define CS_BLOCK  0x02 /*!< client subcommand block download request */
# define CS_UPLOAD 0x03 /*!< client subcommand start upload request */
# define CS_END    0x01 /*!< client subcommand end block download request */

# define SIZE_IND    0x02 /*!< size is indicated during init */
# define NO_SIZE_IND 0x00 /*!< no size is indicated during init */

# define CRC_SUPP    0x04 /*!< CRC generation supported */
# define NO_CRC_SUPP 0x00 /*!< CRC generation not supported */

# define CCS_BLK_DOWNLOAD 0xC0 /*!< ccs = 6 */
# define SCS_BLK_DOWNLOAD 0xA0 /*!< scs = 5 */
# define CCS_BLK_UPLOAD   0xA0 /*!< ccs = 5 */
# define SCS_BLK_UPLOAD   0xC0 /*!< scs = 6 */
#endif /* SDO_BLOCK_ALLOWED */

/*!
  \defgroup sdoabortcodes SDO abort codes
  @{
*/
#define SAC_TOG_NOT_ALTERED 0x05030000UL /*!< Toggle bit not alternated. */
#define SAC_SDO_TIME_OUT    0x05040000UL /*!< SDO protocol timed out. */
#define SAC_UNKNOWN_CS      0x05040001UL /*!< Client/server command specifier not valid or unknown. */
#define SAC_INVAL_BLK_SIZE  0x05040002UL /*!< Invalid block size (block mode only). */
#define SAC_INVAL_SEQ_NUM   0x05040003UL /*!< Invalid sequence number (block mode only). */
#define SAC_CRC_ERROR       0x05040004UL /*!< CRC error (block mode only). */
#define SAC_OUT_OF_MEM      0x05040005UL /*!< Out of memory. */
#define SAC_ACC_NOT_SUPP    0x06010000UL /*!< Unsupported access to an object. */
#define SAC_WR_ONLY_OBJ     0x06010001UL /*!< Attempt to read a write only object. */
#define SAC_RD_ONLY_OBJ     0x06010002UL /*!< Attempt to write a read only object. */
#define SAC_OBJ_NOT_EXIST   0x06020000UL /*!< Object does not exist in the object dictionary. */
#define SAC_NOT_MAPPED      0x06040041UL /*!< Object cannot be mapped to the PDO. */
#define SAC_MAP_INCONS      0x06040042UL /*!< The number and length of the objects to be mapped would exceed PDO length. */
#define SAC_GEN_PARA_INC    0x06040043UL /*!< General parameter incompatibility reason. */
#define SAC_GEN_INT_PARA    0x06040047UL /*!< General internal incompatibility in the device. */
#define SAC_ACC_FAIL_HW     0x06060000UL /*!< Access failed due to an hardware error. */
#define SAC_LEN_NOT_MATCH   0x06070010UL /*!< Data type does not match, length of service parameter does not match */
#define SAC_LEN_TOO_HIGH    0x06070012UL /*!< Data type does not match, length of service parameter too high */
#define SAC_LEN_TOO_LOW     0x06070013UL /*!< Data type does not match, length of service parameter too low */
#define SAC_NO_SUB_INDEX    0x06090011UL /*!< Sub-index does not exist. */
#define SAC_VAL_RNG_EXCEED  0x06090030UL /*!< Value range of parameter exceeded (only for write access). */
#define SAC_PARA_TOO_HIGH   0x06090031UL /*!< Value of parameter written too high. */
#define SAC_PARA_TOO_LOW    0x06090032UL /*!< Value of parameter written too low. */
#define SAC_MAX_LESS_MIN    0x06090036UL /*!< Maximum value is less than minimum value. */
#define SAC_GENERIC_ERROR   0x08000000UL /*!< general error */
#define SAC_NOT_STORED      0x08000020UL /*!< Data cannot be transferred or stored to the application. */
#define SAC_LOCAL_CONTROL   0x08000021UL /*!< Data cannot be transferred or stored to the application because of local control. */
#define SAC_LOCAL_STATE     0x08000022UL /*!< Data cannot be transferred or stored to the application because of the present device state. */
#define SAC_OD_GEN_FAILED   0x08000023UL /*!< Object dictionary dynamic generation fails or no object dictionary is present (e.g. object dictionary is generated from file and generation fails because of an file error). */
/*!
  @} end of group sdoabortcodes
*/

/* SDO abort code indices */
#define TOG_NOT_ALTERED 1
#define SDO_TIME_OUT    2
#define UNKNOWN_CS      3
#define INVAL_BLK_SIZE  4
#define INVAL_SEQ_NUM   5
#define CRC_ERROR       6
#define OUT_OF_MEM      7
#define ACC_NOT_SUPP    8
#define WR_ONLY_OBJ     9
#define RD_ONLY_OBJ     10
#define OBJ_NOT_EXIST   11
#define NOT_MAPPED      12
#define MAP_INCONS      13
#define GEN_PARA_INC    14
#define GEN_INT_PARA    15
#define ACC_FAIL_HW     16
#define LEN_NOT_MATCH   17
#define LEN_TOO_HIGH    18
#define LEN_TOO_LOW     19
#define NO_SUB_INDEX    20
#define VAL_RNG_EXCEED  21
#define PARA_TOO_HIGH   22
#define PARA_TOO_LOW    23
#define MAX_LESS_MIN    24
#define GENERIC_ERROR   25
#define NOT_STORED      26
#define LOCAL_CONTROL   27
#define LOCAL_STATE     28
#define OD_GEN_FAILED   29

#define ST_IDLE           0 /*!< sdo is currently not used */
#define ST_RUNNING        1 /*!< sdo transfer is running */
#define ST_TRANSFERRED    2 /*!< sdo transfer is stopped  - successful */
#define ST_WRITE_REQ      3 /*!< download request received from remote client */
#define ST_READ_REQ       4 /*!< upload request received from remote client */

#define SDO_INVALID  0x80 /*!< SDO invalidation bit. */
#define SDO_EXTENDED 0x20 /*!< SDO extended identifier bit. */

#if CLIENT_ENABLE == 1
# define ST_UP_CON        5 /*!< the remote server has answered the upload request */
# define ST_DOWNSEG_CON   6 /*!< all the data bytes from the buffer have been downloaded */
# if SDO_BLOCK_ALLOWED == 1
#  define ST_UP_BLK_INI_CON 7 /*!< block upload init confirmation received from remote server */
#  define ST_UP_BLK_SEG_CON 8 /*!< block upload started */
#  define ST_UP_BLK_END_CON 9 /*!< block upload complete received from remote server */
#  define ST_DOWN_BLK_INI_CON 10 /*!< block download init confirmation received from remote server */
#  define ST_DOWN_BLK_SEG_CON 11 /*!< block download started */
#  define ST_DOWN_BLK_END_CON 12 /*!< block download complete received from remote server */
# endif /* SDO_BLOCK_ALLOWED == 1 */
#endif /* CLIENT_ENABLE == 1*/

#if SDO_BLOCK_ALLOWED == 1
# define ST_WRITE_BLK_REQ 13 /*!< block download request received from remote client */
# define ST_WRITE_END_REQ 14 /*!< last segment of last block received */
# define ST_READ_BLK_REQ  15 /*!< block upload request received from remote client */
# define ST_READ_START_REQ  16 /*!< block transfer started */
# define ST_READ_END_REQ  17 /*!< last segment of last block transmitted */
#endif /* SDO_BLOCK_ALLOWED == 1 */

#if (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1) || (SDO_BLOCK_ALLOWED == 1)
# define SDO_DATASIZE qwDataSize.dw
# define SDO_DATASIZE_CAST DWORD
#else
# define SDO_DATASIZE bDataSize
# define SDO_DATASIZE_CAST BYTE
#endif /* (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1) || (SDO_BLOCK_ALLOWED == 1) */

/*!
  \def STORAGE_CAPABILITY
  Storage capability field

  This value is returned if the objects 1010/1011 are read
  on a sub-index != 0.
  The value holds the flags
  - #STORE_AUTO_FLAG
  - #STORE_CMD_FLAG
*/

/*!
  \def STORE_AUTO_FLAG
  Automatic storage flag

  The following values are valid:
  - 0x02 - automatic storage supported
  - 0x00 - not automatic  storage
*/

/*!
  \def STORE_CMD_FLAG
  Command storage flag

  The following values are valid:
  - 0x01 - storage on command supported
  - 0x00 - not storage on command supported
*/

#if STORE_PARAMETERS_SUPP == 1
# if STORE_AUTOMATICALLY == 1
#  define STORE_AUTO_FLAG 0x02
# else
#  define STORE_AUTO_FLAG 0x00
# endif /* STORE_AUTOMATICALLY == 1 */
# if STORE_ON_COMMAND == 1
#  define STORE_CMD_FLAG 0x01
# else
#  define STORE_CMD_FLAG 0x00
# endif /* STORE_ON_COMMAND 1 */
# define STORAGE_CAPABILITY (STORE_CMD_FLAG | STORE_AUTO_FLAG)
#endif /* STORE_PARAMETERS_SUPP == 1 */



/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/

/*!
  \brief Control structure for SDO transfer.

  After successful initialisation of a SDO transfer the structure
  is filled with the appropriate information.
*/
typedef struct {
  WBYTE wbIndex;          /*!< CANopen index */
  BYTE bSubIndex;         /*!< CANopen subindex 0..255 */
  BYTE bLastState;        /*!< remember the last executed protocol part */
  BYTE FCONST * pbData;   /*!< pointer to SDO data */
  BYTE bExpedBuf[4];      /*!< buffer for expedited data */
#if (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1)
  DWORD DataCounter;      /*!< number of remaining bytes */
# if SDO_EXEC_USER_WRITE_CALLBACKS == 1
  BYTE bCallbkAttr;       /*!< remember callback attribute */
# endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */
#else
# if CLIENT_ENABLE == 1
  BYTE DataCounter;      /*!< number of remaining bytes */
# endif /* CLIENT_ENABLE == 1 */
#endif /* (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1) */
  BYTE bDataToggle;       /*!< state of toggle bit */
  WORD wTimeOut;          /*!< time out supervision */
  BYTE bRxId;             /*!< receive id index */
  BYTE bTxId;             /*!< transmit id index */
#if (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1) || (SDO_BLOCK_ALLOWED == 1)
  QBYTE qwDataSize;       /*!< number of bytes to be transfered */
  CONST OD_ENTRY CONST_PTR * ptEntry; /*!< pointer to object dictionary entry */
  BYTE bIndirect;         /*!< set direct or indirect object access */
#else
  BYTE bDataSize;         /*!< number of bytes to be transfered */
#endif /* (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1) || (SDO_BLOCK_ALLOWED == 1) */
#if SDO_BLOCK_ALLOWED == 1
  BYTE bActSequenceNr;    /*!< actual block number */
  BYTE bBlockSize;        /*!< block size to be used by client */
  BYTE bProtThreshold;    /*!< threshold value to switch to block transfer */
  BYTE bCalcCrc;          /*!< CRC calculation requested */
  BYTE bLastSegment;      /*!< remember bytes in last segment */
#endif /* SDO_BLOCK_ALLOWED == 1*/
#if DELAYED_SDO_ENABLE == 1
  BYTE bDelayedState;     /*!< */
#endif /* DELAYED_SDO_ENABLE == 1 */
#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
  BYTE bChannelEnabled;   /*!< this channel is enabled */ 
  BYTE bRxIdEnabled;      /*!< Rx ID is enabled */ 
  BYTE bTxIdEnabled;      /*!< Tx ID is enabled */ 
  BYTE bNodeIdClient;     /*!< client of this server */ 
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */
  /* temporary buffer for SDO download data (callback, segmented & block transfer) */
  BYTE abSdoDataTmpBuffer[SDO_DATA_BUFFER_SIZE];
} VSdoAttrib;



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
BYTE gSdoS_ClearServerInfo(BYTE);
BYTE gSdoS_HandleSdo(BYTE);
BYTE gSdoC_HandleSdo(void);
void gSdoS_ChkTimeOut(WORD);
BYTE gSdoS_SetServerCanBuf(BYTE, BYTE, BYTE);
void gSdoS_CreateAbort(BYTE, BYTE);

#if SDO_BLOCK_USE_CRC == 1
WORD gSdoS_CrcCalc(BYTE *, DWORD);
#endif /* SDO_BLOCK_USE_CRC == 1 */

#if SDO_LOCAL_READ == 1
BYTE gReadObject(const WORD, const BYTE, BYTE XDATA *);
#endif /* SDO_LOCAL_READ == 1 */

#if SDO_LOCAL_WRITE == 1
BYTE gWriteObject(const WORD, const BYTE, BYTE XDATA *);
#endif /* SDO_LOCAL_WRITE == 1 */

#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
BYTE gSdoCh2Control(BOOLEAN, BYTE);
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */


CONST OD_ENTRY CONST_PTR * gSdoS_GetEntry(WORD, BYTE);

#endif /* _SDOSERV_H_ */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for the SDO server.
  \par File name sdoserv.h
  \version 27
  \date 1.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

