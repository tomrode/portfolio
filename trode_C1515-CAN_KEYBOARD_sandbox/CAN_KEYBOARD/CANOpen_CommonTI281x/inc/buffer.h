/*--------------------------------------------------------------------
       BUFFER.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for basic routines to filter and store CAN
                 messages
  --------------------------------------------------------------------*/

#ifndef _BUFFER_H_
#define _BUFFER_H_


/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/
#if QUEUED_MODE == 1
/*!
  \def Q_RX_CAN_ELEMENTS
  Number of elements in the receive queue.
*/
# define Q_RX_CAN_ELEMENTS 8

/*!
  \def Q_TX_CAN_ELEMENTS
  Number of elements in the transmit queue.
*/
# define Q_TX_CAN_ELEMENTS 8


/* basic definitions (do not change) */
/*! overall receive queue length */
# define Q_RX_LENGTH (Q_RX_CAN_ELEMENTS * sizeof(CAN_MSG))
/*! overall transmit queue length */
# define Q_TX_LENGTH (Q_TX_CAN_ELEMENTS * sizeof(CAN_MSG))
#endif /* QUEUED_MODE == 1 */

#define CAN_NO_MANAGER  255     /*!< ID not attached                 */
#define CAN_RX_MANAGER  1       /*!< ID attached to receive handler  */
#define CAN_TX_MANAGER  2       /*!< ID attached to transmit handler */


/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/
#if QUEUED_MODE == 1
/*!
  We plan to signal the reception of identifiers located in identifier
  areas via function call backs. These areas are the bootup message area
  and the emergency message area. This functionality is only available
  in QUEUED_MODE.
*/
typedef struct {
  QBYTE qbIdLowLimit;
  QBYTE qbIdHighLimit;
} VSignalIdArea;
#endif /* QUEUED_MODE == 1 */


/*--------------------------------------------------------------------*/
/*  macros                                                            */
/*--------------------------------------------------------------------*/
#define EnterRegion()
#define LeaveRegion()
#define ID_ALLOWED_INPREPARED(msg) (1)
#define ID_ALLOWED(msg) (1)


/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void gCB_Init(void);
#if FULLCAN_BUFFER_MODE == 1
#else
void gCB_ClearIdFilter(BYTE);
BYTE gCB_ConfigureIdFilter(WORD, BYTE);
BYTE gCB_PreCheckIdStd(WORD);
BYTE gCB_PreCheckIdExt(DWORD);
BYTE sCB_GetManagerFromId(WORD);
#endif /* FULLCAN_BUFFER_MODE == 1 */

BYTE gCB_ConfigMsg(WORD, BYTE);

BYTE gCB_QueSendMsg(CAN_MSG XDATA *);
void gCB_DispatchQueCallbacks(void);
void gCB_BufSetId(BYTE, DWORD, BYTE, BYTE);
BYTE gCB_BufChangeId(BYTE, DWORD);
BYTE gCB_BufEnable(BOOLEAN, BYTE);
BYTE gCB_TestInMsg(void);
BYTE gCB_CanBufferMsg(CAN_MSG XDATA *);
void gCB_SignalTx(void);

#endif /* _BUFFER_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for basic routines to filter and store CAN messages.
  \par File name buffer.h
  \version 10
  \date 23.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

