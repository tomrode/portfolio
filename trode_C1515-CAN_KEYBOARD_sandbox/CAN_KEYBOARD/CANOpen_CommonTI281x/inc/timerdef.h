/*--------------------------------------------------------------------
       TIMERDEF.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Timer functions header
  --------------------------------------------------------------------*/

/*
START CONFIGURATOR CONTROL UNIT

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
BASIC_TIME              | Target Adaptation | GENERAL |    INT  |   1 | 200
COMM_TIMER_CYCLE        | Target Adaptation | GENERAL |    INT  |   1 | 200

END CONFIGURATOR CONTROL UNIT
*/

#ifndef _TIMERDEF_H_
#define _TIMERDEF_H_



/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/
/*!
  \def TIME_FROM_OS
  Switch timer source from hardware timer to operating system.
  - 0 timer source is hardware timer.
  - 1 timer source is operating system.
*/

#ifdef WIN32
# if USE_CANOE_EMU == 1
#  define TIME_FROM_OS 0  /* 0 */
# else
#  define TIME_FROM_OS 1  /* 1 */
# endif
#else
# define TIME_FROM_OS 0  /* 0 or 1 */
#endif

/*!
  \def BASIC_TIME
  Base time tick

  The basic time in ms, typical 5
*/
#define BASIC_TIME 5

/*!
  \def COMM_TIMER_CYCLE
  Execution cycle of generic timer handler.

  The generic timer handler is executed every given ms, typical 10
*/
#define COMM_TIMER_CYCLE 10

/* Check timing values */
#if BASIC_TIME > COMM_TIMER_CYCLE
# error "Basic time quantum exceeds communication cycle time !"
#endif



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void Tim_InitTimer(WORD);
BOOLEAN Tim_StartTimer(void);
BOOLEAN Tim_StopTimer(void);
WORD Tim_GetCurrentTime(void);
void TimerHandler(void);

#endif /* _TIMERDEF_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Timer functions header.
  \par File name timerdef.h
  \version 13
  \date 10.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

