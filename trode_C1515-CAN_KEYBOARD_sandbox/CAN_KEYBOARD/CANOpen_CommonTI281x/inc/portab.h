/*--------------------------------------------------------------------
       PORTAB.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Portability header
  --------------------------------------------------------------------*/

#ifndef _PORTAB_H_
#define _PORTAB_H_



/*--------------------------------------------------------------------*/
/*  generic defines                                                   */
/*--------------------------------------------------------------------*/

/*! \def NEAR
  \brief Specify that the item is at a near location
*/

/*! \def FAR
  \brief Specify that the item is at a far location
*/

/*! \def HUGE
  \brief Specify that the item is at a huge location
*/

/*! \def SHUGE
  \brief Specify that the item is at a shuge location
*/

/*! \def XDATA
  \brief Specify that the item is at an external data location
*/

/*! \def BDATA
  \brief Specify that the item is at a bit addressable data location
*/

/*! \def IDATA
  \brief Specify that the item is at an internal data location
*/

/*! \def PDATA
  \brief
*/

/*! \def CODE
  \brief Specify that a variable is located in the code area. 
*/

/*! \def CONST
  \brief Specify that a function parameter is const.
*/

/*! \def SYSTEMDATA
  \brief Specify that the item is in the system data area.
*/

/*! \def BITDATA
  \brief Specify that the item is in the bit addressable area.
*/

/*! \def REENTRANT
  \brief Specify that a function is reentrant.
*/

/*! \def MEM_AREA
  \brief Specify that a pointer points to a RAM area.
*/

/*! \def CODE_AREA
  \brief Specify that a variable is located in a ROM area.
*/

/*! \def CONST_PTR
  \brief Specify that a function parameter pointer points to a code area.
*/

/*! \def VOID
  \brief Specify the void token.
*/

/*! \def STATIC
  \brief Specify the static token.
*/

# ifndef STATIC
#  define STATIC static
# endif

# ifndef VOID
#  define VOID void
# endif

#ifdef CCSTI  /* Code Composer Studio for TI */

# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef INTERRUPT
#  define INTERRUPT
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif
#endif /*  CCSTI */

#ifdef __GNUPPC__  /* GNU compiler for power pc */

# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef INTERRUPT
#  define INTERRUPT
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif
#endif /*  __GNUPPC__ */


#ifdef __18CXX /* Microchip C18 compiler for PIC18Fxx8 environment */

#define NO_UNIVERSAL_POINTERS 1

#define EIGHT_BIT_MICRO 1

# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE rom
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef INTERRUPT
#  define INTERRUPT
# endif

# ifndef CONST_PTR
#  define CONST_PTR rom
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* Microchip C18 compiler for PIC18Fxx8 environment */

#ifdef _MCC68K /* microtec compiler for 68000 environment */

# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef INTERRUPT
#  define INTERRUPT interrupt
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* _MCC68K */

#ifdef WIN32 /* PC based evaluation environment - W95/98 NT4.0 */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* WIN32 */

#ifdef _TSK_C166  /* TASKING C C166/ST10 Cross-Compiler Version 6 R2 */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* _TSK_C166 */

#ifdef _C196_  /* TASKING C C196 Cross-Compiler Version 4.0 */
# ifndef NEAR
#  define NEAR near
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#pragma neardata /* default placement of variable data    */
                 /* is the data segment                   */

#endif /* _C196_ */

#ifdef _KL_C166  /* Keil C166 Cross-Compiler Version 3.12 */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* _KL_C166 */

#ifdef __C51__  /* KEIL C51 Cross-Compiler Version 4.01 */

#define EIGHT_BIT_MICRO 1

# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA xdata
# endif

# ifndef BDATA
#  define BDATA bdata
# endif

# ifndef DATA
#  define DATA data
# endif

# ifndef IDATA
#  define IDATA idata
# endif

# ifndef CODE
#  define CODE code
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA bdata
# endif

# ifndef REENTRANT
#  define REENTRANT reentrant
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA code
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* __C51__ */


#ifdef M16C  /* Mitsubishi NC30 Cross-Compiler for M16C version 4.0R2 */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA 
# endif

# ifndef BDATA
#  define BDATA 
# endif

# ifndef DATA
#  define DATA 
# endif

# ifndef IDATA
#  define IDATA 
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA 
# endif

# ifndef REENTRANT
#  define REENTRANT 
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA 
# endif

# ifndef FCONST
#  define FCONST far
# endif

#endif /* M16C */


#ifdef NC308  /* Mitsubishi M32C/80 Series NC308 COMPILER V.3.10 Release 2 */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA 
# endif

# ifndef BDATA
#  define BDATA 
# endif

# ifndef DATA
#  define DATA 
# endif

# ifndef IDATA
#  define IDATA 
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA 
# endif

# ifndef REENTRANT
#  define REENTRANT 
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA 
# endif

# ifndef FCONST
#  define FCONST far
# endif

#endif /* M32C */


#ifdef COSMIC08  /* Cosmic C Cross Compiler V4.1 */

# define NDEBUG
# define NULL (void *)0

# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* COSMIC08 */


#ifdef COSMIC12  /* Cosmic C Cross Compiler V4.2 */

# define NDEBUG
# define NULL (void *)0

# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* COSMIC12 */


#ifdef __GNUC68K__  /* GNU C Cross-Compiler 68K */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef INTERRUPT
#  define INTERRUPT
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* __GNUC68K__ */

#ifdef C_COMP_FUJITSU_FCC907 /* FUJITSU Compiler FCC907s */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef INTERRUPT
#  define INTERRUPT
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* C_COMP_FUJITSU_FCC907 */

#ifdef __RCXA__  /* Raisonance RCXA Cross-Compiler Version 2.05.27 */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR far
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA bdata
# endif

# ifndef DATA
#  define DATA data
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE code
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA bdata
# endif

# ifndef REENTRANT
#  define REENTRANT reentrant
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA code
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* __RCXA__ */


#ifdef __MWERKS__ /* Metrowerks CodeWarrior */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef INTERRUPT
#  define INTERRUPT
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* Metrowerks CodeWarrior */

#ifdef __HITACHI_VERSION__ /* HEW H8S */

#ifndef NULL
# define NULL (void *)0
#endif

# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef INTERRUPT
#  define INTERRUPT interrupt
# endif
       
# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA
# endif

# ifndef FCONST
#  define FCONST
# endif
#endif /* __HITACHI_VERSION__   (HEW) */

#ifdef _SH2 /* HITACHI SuperH RISC compiler SH-2 */

#ifndef NULL
# define NULL (void *)0
#endif

# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef INTERRUPT
#  define INTERRUPT
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* _SH2 */

#ifdef PARADIGM /* x86 based evaluation environment - Paradigm */
# ifndef NEAR
#  define NEAR
# endif

# ifndef FAR
#  define FAR
# endif

# ifndef HUGE
#  define HUGE
# endif

# ifndef SHUGE
#  define SHUGE
# endif

# ifndef XDATA
#  define XDATA
# endif

# ifndef BDATA
#  define BDATA
# endif

# ifndef DATA
#  define DATA
# endif

# ifndef IDATA
#  define IDATA
# endif

# ifndef CODE
#  define CODE
# endif

# ifndef SYSTEMDATA
#  define SYSTEMDATA
# endif

# ifndef BITDATA
#  define BITDATA
# endif

# ifndef REENTRANT
#  define REENTRANT
# endif

# ifndef CONST
#  define CONST const
# endif

# ifndef CONST_PTR
#  define CONST_PTR
# endif

# ifndef MEM_AREA
#  define MEM_AREA
# endif

# ifndef CODE_AREA
#  define CODE_AREA const
# endif

# ifndef FCONST
#  define FCONST
# endif

#endif /* PARADIGM */

#endif /* _PORTAB_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Portability header. This file defines some compiler specific
         adaptations.
  \par File name portab.h
  \version 25
  \date 26.06.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

