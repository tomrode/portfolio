/*--------------------------------------------------------------------
       OBJDICT.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Object dictionary header
  --------------------------------------------------------------------*/

#ifndef _OBJDICT_H_
#define _OBJDICT_H_



/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/

/*!
  \def TIME_STAMP_OBJECTS
  \brief Number of objects used for time stamp.

  The following objects are supported:
  - #COB_ID_TIME_STAMP
  - #TIME_STAMP
*/
#if TIME_STAMP_USED == 1
# define TIME_STAMP_OBJECTS 2
#else
# define TIME_STAMP_OBJECTS 0
#endif /* TIME_STAMP_USED == 1 */

/*!
  \def SYNC_OBJECTS
  \brief Number of objects used for SYNC.

  The following objects are supported:
  - #COB_ID_SYNC
  - #SYNC_CYCLE_PERIOD
  - #SYNC_WINDOW_LENGTH
*/
#if SYNC_USED == 1
# define SYNC_OBJECTS 3
#else
# define SYNC_OBJECTS 0
#endif /* SYNC_USED == 1 */

/*!
  \def EMCY_OBJECTS
  \brief Number of objects used for EMCY handling.

  The following objects are supported:
  - #ERROR_FIELD
  - #COB_ID_EMCY
  - #INHIBIT_TIME_EMCY
*/
#if ENABLE_EMCYMSG == 1
# define EMCY_OBJECTS 4
#else
# define EMCY_OBJECTS 0
#endif /* ENABLE_EMCYMSG == 1 */

/*!
  \def STORE_OBJECTS
  \brief Number of objects used for the store/restore parameters functionality.

  We support up to four sub-indices by default.
*/
#if STORE_PARAMETERS_SUPP == 1

# if NV_SAVE_COMPAR == 1
#  define NV_SAVE_COMPAR_NUM 2
# else
#  define NV_SAVE_COMPAR_NUM 0
# endif /* NV_SAVE_COMPAR == 1 */

# if NV_SAVE_APPPAR == 1
#  define NV_SAVE_APPPAR_NUM 2
# else
#  define NV_SAVE_APPPAR_NUM 0
# endif /* NV_SAVE_APPPAR == 1 */

# if NV_SAVE_MANPAR == 1
#  define NV_SAVE_MANPAR_NUM 2
# else
#  define NV_SAVE_MANPAR_NUM 0
# endif /* NV_SAVE_MANPAR == 1 */

# define STORE_OBJECTS 4 + NV_SAVE_COMPAR_NUM \
          + NV_SAVE_APPPAR_NUM + NV_SAVE_MANPAR_NUM
#else
# define STORE_OBJECTS 0
#endif /* STORE_PARAMETERS_SUPP == 1 */

/*!
  \def HEARTBEAT_OBJECTS
  \brief Number of objects used for Heartbeat.

  The following objects are supported:
  - #CON_HEARTBEAT_TIME (sub-index 0)
  - #CON_HEARTBEAT_TIME (other sub-indices)
*/
#if HEARTBEAT_CONSUMER == 1
# define HEARTBEAT_OBJECTS 2
#else
# define HEARTBEAT_OBJECTS 0
#endif /* HEARTBEAT_CONSUMER == 1 */

/*!
  \def NMT_ERR_CTRL_OBJECTS
  \brief Number of objects used for NMT error control.

  The following objects are supported:
  - #GUARD_TIME
  - #LIFE_TIME_FACTOR
  - #PRO_HEARTBEAT_TIME
*/
#if ENABLE_NMT_ERROR_CTRL == 1
# define NMT_ERR_CTRL_OBJECTS 2
#elif ENABLE_NMT_ERROR_CTRL == 2
# define NMT_ERR_CTRL_OBJECTS 1
#elif ENABLE_NMT_ERROR_CTRL == 3
# define NMT_ERR_CTRL_OBJECTS 3
#else
# define NMT_ERR_CTRL_OBJECTS 0
#endif /* ENABLE_NMT_ERROR_CTRL == 1 */

/*!
  \def SERVER_SDO_ENTRIES
  \brief Number of additional server SDO entries in the object dictionary.
*/
#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
# define SERVER_SDO_ENTRIES 6
#else
# define SERVER_SDO_ENTRIES 0
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */

/*!
  \def RPDO_ENTRIES
  \brief Number of base RPDO entries in object dictionary.
*/
#if MAX_RX_PDOS > 0
# define RPDO_ENTRIES 1
#else
# define RPDO_ENTRIES 0
#endif /* MAX_RX_PDOS > 0 */

/*!
  \brief Number of objects used for RPDO.

  For one RPDO the following objects are supported:
  - communication parameter (sub-index 0)
  - COB id
  - transmission type
  - mapping parameter (sub-index 0)
  - mapping parameter (other sub.indices)
*/
#define RPDO_ENTRY_SIZE 5

/*!
  \def TPDO_ENTRIES
  \brief Number of base TPDO entries in object dictionary.
*/
#if MAX_TX_PDOS > 0
# define TPDO_ENTRIES 1
#else
# define TPDO_ENTRIES 0
#endif /* MAX_TX_PDOS > 0 */

/*!
  \brief Number of objects used for TPDO.

  For one TPDO the following objects are supported:
  - communication parameter (sub-index 0)
  - COB id
  - transmission type
  - inhibit time
  - event timer
  - mapping parameter (sub-index 0)
  - mapping parameter (other sub.indices)
*/
#define TPDO_ENTRY_SIZE 7

/*! Number of PDO related entries in the object dictionary. */
#define PDO_OD_ENTRIES ((TPDO_ENTRIES * TPDO_ENTRY_SIZE) \
                      + (RPDO_ENTRIES * RPDO_ENTRY_SIZE))

/*!
  \def DUMMY_OBJECTS
  \brief Number of dummy objects.
*/

#if (USE_DUMMY_MAPPING == 1) && (MAX_RX_PDOS > 0)
# define DUMMY_OBJECTS 4
# else
# define DUMMY_OBJECTS 0
#endif /*(USE_DUMMY_MAPPING == 1) && (MAX_RX_PDOS > 0)*/

/*!
  \def NMT_STARTUP_OBJECT
  \brief NMTStartup object implemented.
*/
#if STARTUP_AUTONOMOUSLY == 1
# define NMT_STARTUP_OBJECT 1
#else
# define NMT_STARTUP_OBJECT 0
#endif /* STARTUP_AUTONOMOUSLY == 1 */

/*!
  \def ERROR_BEHAVIOUR_OBJECT
  \brief Error Behaviour object implemented.
*/
#if OBJECT_1029_USED == 1
# define ERROR_BEHAVIOUR_OBJECT 2
#else
# define ERROR_BEHAVIOUR_OBJECT 0
#endif /* OBJECT_1029_USED == 1 */

#if EXAMPLE_OBJECTS_USED == 1
# define EXAMPLE_BUFFER_SIZE 240 /*!< buffer size used for example objects */
# define ARRAY_ENTRY_SIZE3     8 /*!< example object 3 - array entry */
# define ARRAY_ENTRY_SIZE4    17 /*!< example object 4 - array entry */
#endif /* EXAMPLE_OBJECTS_USED == 1 */

/*!
  \brief Number of dictionary entries

  The object dictionary structure always has the following default entries
  (number of "real" entries in brackets):
  - #DEVICE_TYPE (1)
  - #ERROR_REG (1)
  - #DEVICE_NAME (1)
  - #HW_VER (1)
  - #SW_VER (1)
  - #IDENTITY_OBJECT (2)

  This results in 7 default entries.
*/
#define DICT_ENTRIES (PDO_OD_ENTRIES \
                      + SYNC_OBJECTS \
                      + TIME_STAMP_OBJECTS \
                      + HEARTBEAT_OBJECTS \
                      + NMT_ERR_CTRL_OBJECTS \
                      + DUMMY_OBJECTS \
                      + EMCY_OBJECTS \
                      + STORE_OBJECTS \
                      + SERVER_SDO_ENTRIES \
                      + NMT_STARTUP_OBJECT \
                      + ERROR_BEHAVIOUR_OBJECT \
                      + 7)


#define TXMAP  0x4 /*!< Entry is mappable to transmit PDOs. */
#define RXMAP  0x8 /*!< Entry is mappable to receive PDOs.  */

/*!
  \defgroup odattr Attributes for objects
  @{
*/

/*!
  \defgroup odaccessattr access attributes for objects
  @{
*/
#define RO     0x1 /*!< Entry is readable and not mappable. */
#define WO     0x2 /*!< Entry is writeable and not mappable. */

#define RW     (RO | WO) /*!< Entry is writeable/readable and not mappable. */
#define ROMAP  (RO | TXMAP) /*!< Entry is readable and mappable to transmit PDOs. */
#define WOMAP  (WO | RXMAP) /*!< Entry is writeable and mappable to receive PDOs. */
#define RWRMAP (RW | TXMAP) /*!< Entry is writeable/readable and mappable to transmit PDOs. */
#define RWWMAP (RW | RXMAP) /*!< Entry is writeable/readable and mappable to receive PDOs.  */
/*!
  @} end of group odaccessattr
*/

/*!
  \defgroup structattr mapping attributes for objects
  @{
*/
#define IS_ARRAY 0x10 /*!< This entry is structured. */
#define NO_ARRAY 0x0  /*!< This entry is no further structured. */
/*!
  @} end of group structattr
*/

/*!
  \defgroup callbackattr call back attributes for objects
  @{
*/
#define SDO_EXEC_CB_NOT   0x0  /*!< SDO callbacks disabled.       */
#define SDO_EXEC_CB_UP    0x20 /*!< SDO upload callbacks enabled.   */
#define SDO_EXEC_CB_DOWN  0x40 /*!< SDO download callbacks enabled. */
/*! All SDO callbacks enabled. */
#define SDO_EXEC_CB_ALL   (SDO_EXEC_CB_UP | SDO_EXEC_CB_DOWN)
/*!
  @} end of group callbackattr
*/

/*!
  @} end of group odattr
*/

/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/

/*!
  \brief Object dictionary entry.

 The object dictionary is a table consisting of an arbitrary
 number of entries structured like _vOD_ENTRY.
 */
typedef struct _vOD_ENTRY {
  BYTE FCONST * pData;      /*!< 
                              - != 0 location of data value,
                              - 0 - data access provided via function
                              If the data item is shorter than
                              4 bytes and the data item is read only
                              it is located in this structure directly.
                              */
  WBYTE wbIndex;             /*!< CANopen index */
  BYTE  bSubIndex;           /*!< CANopen subindex 0..255 */

  BYTE Length;               /*!<
                              Object length

                              - 0 - length provided via function
                                gSdoS_GetObjLenCb()
                              - 1-255 length of item
                              */
  BYTE AccArrayClbck;         /*!<
                              Object attributes

                              These attributes specify the:
                              - access and mapping (\ref odaccessattr)
                              - structure (\ref structattr)
                              - callback (\ref callbackattr)
                              */
}
OD_ENTRY;



/*!
  \brief Fixed array length structure.

 Some of the entries in the object dictionary are
 arrays with a fixed number of entries behind.
 The "number of entries" information could be placed
 in ROM if the subindex 0 is a member of this structure.
 */
typedef struct _VCodeArea {
  DWORD mDeviceType[1]; /*!< device type                                */

#if STORE_PARAMETERS_SUPP == 1
  BYTE mStorageNr[1]; /*!< number of entries for objects 1010/1011      */
#endif /* STORE_PARAMETERS_SUPP == 1 */
  BYTE mHeartBeat   [1]; /*!< size of heartbeat list                    */
#if OBJECT_1029_USED == 1
  BYTE mErrClasses  [1]; /*!< number of error classes                   */
#endif /* OBJECT_1029_USED == 1 */
  DWORD mIdtyVendor [1];/*!< identity structure vendor                  */
  DWORD mIdtyPrdCode[1];/*!< identity structure product code            */
  DWORD mIdtyRevNum [1];/*!< identity structure revision number         */
  DWORD mIdtySerNum [1];/*!< identity structure serial number           */
}
VCodeArea;

#endif /* _OBJDICT_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Object dictionary header.
  \par File name objdict.h
  \version 26
  \date 25.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

