/*--------------------------------------------------------------------
       NONVOLST.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for non volatile storage support.
  --------------------------------------------------------------------*/

#ifndef _NONVOLST_H_
#define _NONVOLST_H_



/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/

/*!
  \defgroup nvselection available sub-indices for storage
  @{
*/
#define PAR_SEL_ALL 1 /*!< all storeable parameters sub-index 1*/
#define PAR_SEL_COM 2 /*!< communication related parameters sub-index 2*/
#define PAR_SEL_APP 3 /*!< application related parameters sub-index 3 */
#define PAR_SEL_MAN 4 /*!< manufacturers defined parameters - sub-index 4 */
/*!
  @} end of group nvselection
*/

/*!
  \brief "magic pattern" for stored blocks

  Every stored block is preceded by a structure of type #VStoreAreaHead.
*/
#define CONFIG_SAVED 0x4D41

#if EMULATE_ON_DISK==1
# ifdef NDEBUG
#  define STORE_BLOCK(descriptor, filename, name, address, length, offset, store, apply) \
   {address, length, offset, descriptor, filename, store, apply}
# else
#  define STORE_BLOCK(descriptor, filename, name, address, length, offset, store, apply) \
   {address, length, offset, descriptor, filename, name, store, apply}
# endif /* NDEBUG */
#else
# ifdef NDEBUG
#  define STORE_BLOCK(descriptor, filename, name, address, length, offset, store, apply) \
   {address, length, offset, descriptor, store, apply}
# else
#  define STORE_BLOCK(descriptor, filename, name, address, length, offset, store, apply) \
   {address, length, offset, descriptor, name, store, apply}
# endif /* NDEBUG */
#endif /* EMULATE_ON_DISK==1 */


/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/

/*---------------------------------------
_VDataArea: This struct holds the values
  of the object dictionary which have to be
  stored/restored.

  Special object mIsFactorySetting:
    This variable MUST be
    an initialized variable, that is
    stored when downloading the software
    into the Flash (or EEPROM) with the
    value CONFIG_SAVED.
    Whenever a Store Command (1010) is
    invoked, the variable has to be set to CONFIG_SAVED
    On invocation of 1011 Restore factory
    settings the variable has to be set
    back to NO_CONFIG_SAVED!

----------------------------------------*/

/*!
  \brief Storage description

  This structure is used to describe all the storageable memory areas
  in more detail.
*/
typedef struct _VStoreDesc {
  VStoreAreaHead XDATA * pHead; /*!< pointer to storage head */
  WORD wLen;   /*!< compiled block length */
  /*!
  Offset in common memory block.

  If there are several storage blocks available they can be stored at different
  physical locations (different files) or to one single location (one file).
  If a single location is used we need the offset of the data block
  in the common memory block.
  */
  WORD offset;
  BYTE bDesc;  /*!< Descriptor see \ref nvselection */
#if EMULATE_ON_DISK==1
  char * filename; /*!< file name */
#endif /* EMULATE_ON_DISK==1 */
#ifndef NDEBUG
  char * Name; /* configuration name for debugging purposes */
#endif /* !NDEBUG */
  /*!
  Callback for "store"

  This callback is executed before the defined memory block is about to be
  stored.
  Place any data collection/verification strategies here.
  */
  void (*store)(void);
  /*!
  Callback for "apply"

  This callback is executed during bootup if the stored
  configuration is found.
  Place any copying of the stored data to their final location here.
  */
  BYTE (*apply)(void);
} VStoreDesc;



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/

WORD gCalculateChecksum(BYTE FAR *, WORD);
BYTE gStoreConfiguration(BYTE);
BYTE gApplyConfiguration(void);
BYTE gLoadConfiguration(void);
BYTE gNvDelete(BYTE);
#if NV_BLOCK_MODE == 1
BYTE gNvLoad(BYTE, WORD, BYTE *);
BYTE gNvSave(BYTE, WORD, BYTE *);
#else
BYTE gNvLoad(BYTE, WORD);
BYTE gNvSave(BYTE, WORD);
BYTE Nvs_ReadChunk(WORD, void *);
BYTE Nvs_WriteChunk(WORD, void *);
#endif /* NV_BLOCK_MODE == 1 */
void gNvInit(void);


#endif  /* _NONVOLST_H_ */


/*!
  \file
  \brief Header for CANopen slave's non-volatile storage handling.
  \par File name nonvolst.h
  \version 10
  \date 6.03.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
