/*--------------------------------------------------------------------
       NMTMS.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for NMT master services
  --------------------------------------------------------------------*/

#ifndef _NMTMS_H_
#define _NMTMS_H_



/*--------------------------------------------------------------------*/
/*  global definitions                                                */
/*--------------------------------------------------------------------*/

/*!
  This value defines the minimum guard time. The Guard Timer is executed
  every GUARD_TIME_FACTOR * COMM_TIMER_CYCLE milliseconds.
*/
#define GUARD_TIME_FACTOR   5



/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/

/*! error specifications */
typedef enum {
  NON_SPEC           = 1,   /*!< no error specification */
  NO_RESSOURCES      = 12,  /*!< no ressources available at the moment */
  NODE_ALREADY_EXIST = 101, /*!< the node is already registered */
  NO_MEM             = 102, /*!< no memory available */
  ADDRESS_NOTVALID   = 103, /*!< module id not valid */
  NOT_EXIST          = 104, /*!< handle doesn't exist */
  STATE_NOT_EXIST    = 106, /*!< communication state not existent */
  GUARDING_FAILED    = 107, /*!< guarding failed for this node */
  TIMEOUT_OCC        = 108  /*!< time out */
}
eErrDesc;
  
/*! possible node states */
typedef enum {
  NM_STOPPED     = 4,   /*!< stopped state */
  NM_OPERATIONAL = 5,   /*!< operational state */
  NM_RESETNODE   = 6,   /*!< reset node transition */
  NM_RESETCOMM   = 7,   /*!< reset communication transition */
  NM_PREOP       = 127  /*!< pre-operational state */
}
eNMState;

/*! guarding state */
typedef enum {
  GD_NOT_ACTIVATED = 0, /*!< guarding is not activated yet */
  GD_ACTIVATED     = 1, /*!< guarding is activated */
  GD_MISSED        = 2, /*!< response is missed */
  GD_DEAD          = 3, /*!< no response and life time elapsed */
  GD_TOGGLE        = 4, /*!< toggle error */
  GD_STATE         = 5  /*!< state changed */
}
eGDState;

/*! structure of the node entry */
typedef struct {
  BYTE bAddress;            /*!< module-id */
  eNMState eActualState;    /*!< node state as expected */
  eGDState eGuard;          /*!< guarding state */
  WORD wGuardTime;          /*!< guard time */
  BYTE bGuardFactor;	      /*!< life time factor */
  BYTE bToggle;             /*!< toggle bit */
  BYTE bSuspended;          /*!< number of suspended guardings */
  BYTE bOutstand;           /*!< response outstanding ? */
  WORD wActualTime;         /*!< actual time count */
  eErrDesc tError;          /*!< last error occurred */
}
tNode;

/*!
  SYNC producer information
*/
typedef struct {
  WORD wTime;                   /*!< timer                             */
  WORD wReloadTime;             /*!< timer reload value                */
  BYTE bRun;                    /*!< enable timer flag                 */
}
PRO_SYNC;



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void                      /* none */
gNmt_SetMaster(           /* enable/disable NMT master services */
BOOLEAN);                 /* ON/OFF */

void                      /* none */
gNdm_Init(void);          /* initialise node manager */

BOOLEAN                   /* TRUE/FALSE */
gNdm_ActivateGuarding(    /* activate the guarding */
BYTE,                     /* node ID */
WORD,                     /* guard time (ms) */
BYTE);                    /* life time factor */

BOOLEAN                   /* TRUE/FALSE */
gNdm_SuspendGuarding(     /* suspend the guarding */
BYTE);                    /* node ID */

eGDState                  /* actual guarding state */
gNdm_CheckGuarding(       /* get guarding state */
BYTE);                    /* node ID */

eErrDesc *                /* error description */
gNdm_GetError(            /* returns errordescription */
BYTE);                    /* node ID */

BOOLEAN                   /* TRUE/FALSE */
gNmt_SendCommand(         /* send a NMT command */
BYTE,                     /* Node-ID of the module we want to send a command */
BYTE);                    /* NMT command specifier */

void                      /* none */
gNmt_SyncStart(void);     /* start SYNC producer */

void                      /* none */
gNmt_SyncStop(void);      /* stop SYNC producer */

void                      /* none */
gNmt_SyncInit(            /* set SYNC producer time */
WORD);                    /* time value in ms */



/*--------------------------------------------------------------------*/
/* function prototypes (internal functions)                           */
/*--------------------------------------------------------------------*/

void sNdm_RemoveNode(BYTE);

void                      /* none */
gNdm_HandleGuarding(      /* guarding message handler */
BYTE);                    /* buffer number */

void                      /* none */
gNdm_GuardTimer(void);    /* guarding timer handler */

void                      /* none */
gNmt_SyncTimer(void);     /* SYNC producer timer handler */

#endif /* _NMTMS_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for NMT master services.
  \par File name nmtms.h
  \version 7
  \date 6.03.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

