/*--------------------------------------------------------------------
       DS401.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for CANopen DS-401 I/O Profile.
  --------------------------------------------------------------------*/

#ifndef _DS401_H_
#define _DS401_H_


/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/
/*! mask for upper or lower limit integer */
#define AI_UPPER_LOWER_LIMIT_MASK   0x03
/*! mask for upper limit integer */
#define AI_UPPER_LIMIT_MASK         0x01
/*! mask for lower limit integer */
#define AI_LOWER_LIMIT_MASK         0x02
/*! mask for all delta */
#define AI_DELTA_MASK               0x1C
/*! mask for delta unsigned */
#define AI_DELTA_UNSIGNED_MASK      0x04
/*! mask for negative delta unsigned */
#define AI_NEGATIVE_DELTA_MASK      0x08
/*! mask for positive delta unsigned */
#define AI_POSITIVE_DELTA_MASK      0x10

/* default values for different DS-401 object dictionary entries */
#define DEFAULT_ZERO                0x00 /*!< general default value: ZERO */
#define DI_MASK_ANYCHANGE_DEF       0xFF /*!< Digital Input: Mask Anychange               */
#define DO_FILTERCONST_DEF          0xFF /*!< Digital Output: Filter constant             */
#define AI_INTTRIGSEL_DEF           0x07 /*!< Analogue Input: Interrupt Trigger Selection */
#define AO_ERRORMODE_DEF            0x01 /*!< Analogue Output: Error Mode                 */



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void gIO_Init(void);
void gIO_Task(void);
void gIO_EvaluateOutputValues(WORD, BYTE);
BOOLEAN gIO_CheckValChangedInput(WORD, BYTE, FAR BYTE XDATA *);
void gIO_ObjectTxHandling(WORD, BYTE);
# if OBJECT_1029_USED == 1
BYTE gIO_CheckError(void);
# endif /* OBJECT_1029_USED == 1 */
#endif /* _DS401_H_ */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for CANopen DS-401 I/O Profile.
  \par File name ds401.h
  \version 4
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
