/*--------------------------------------------------------------------
       SDOCLNT.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for the SDO client
  --------------------------------------------------------------------*/

#ifndef _SDOCLNT_H_
#define _SDOCLNT_H_



/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/
#define SDO_CLIENT_MULTI 0



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
#if SDO_CLIENT_MULTI == 1
BYTE gSdoC_ClearInfo(BYTE);
#else
void gSdoC_ClearInfo(void);
#endif /* SDO_CLIENT_MULTI == 1 */
BYTE gSdoC_Enable(BYTE);
BYTE gSdoC_PrepTransfer(WORD, BYTE, DWORD);
BYTE gSdoC_Upload(BYTE *);
BYTE gSdoC_Download(BYTE *);
DWORD gSdoC_GetAbortReason(void);
#if (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1)
DWORD
#else
BYTE
#endif /* (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1) */
gSdoC_GetRecDataLen(void);
void gSdoC_ChkTimeOut(WORD);

#if SDO_BLOCK_ALLOWED == 1
BYTE gSdoC_BlkDownload(BYTE *);
BYTE gSdoC_BlkUpload(BYTE *);
#endif /* SDO_BLOCK_ALLOWED == 1 */

#endif /* _SDOCLNT_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for the SDO client.
  \par File name sdoclnt.h
  \version 7
  \date 1.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

