/*--------------------------------------------------------------------
       INST_DEF.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for multiple instance support
  --------------------------------------------------------------------*/



/*
START CONFIGURATOR CONTROL UNIT

Macro Name        | Macro Group            |         | type    | limits
-------------------------------------------------------------------------------
MULTIPLE_INSTANCE | Multiple Instantiation |    BOOL |    BOOL |       |

END CONFIGURATOR CONTROL UNIT
*/


#ifndef _INST_DEF_H_
#define _INST_DEF_H_



/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/

/*!
  \def MULTIPLE_INSTANCE
  Enable multiple instantiation.

  The Stack can be used with multiple instances using separate CAN
  channels.
  Since all functions need an additional parameter (instance handle)
  there are several additional macros needed.
  Keep in mind that the target adaptation needs to be prepared for
  the multiple instantiation feature.
  Have a look at the system adaptation files for more details.
*/
#define MULTIPLE_INSTANCE 0

/*!
  \def ACI( var )
  Access instance variables.

  Since the source code can be compiled with or without instance
  support the internal variables needs to be accessed in the
  same way.
  This macro hides the differences between the instantiated
  access and the normal access.
*/

/*!
  \def DPRAM( var )
  Access instance variables located in shared memory.

  Since the source code can be compiled with or without instance
  support the internal variables needs to be accessed in the
  same way.
  This macro hides the differences between the instantiated
  access to the shared memory and the normal access.
*/

/*!
  \def INSTANCE
  Instance handle parameter for non-void functions.

  This macro is used for the declaration and definition of
  non-void functions.
*/

/*!
  \def INSTANCE_VOID
  Instance handle parameter for void functions.

  This macro is used for the declaration and definition of
  void functions.
*/

/*!
  \def SELF
  Instance handle parameter for non-void functions.

  This macro is used for a call to a non-void function.
*/

/*!
  \def SELF_VOID
  Instance handle parameter for non-void functions.

  This macro is used for a call to a void function.
*/

# if MULTIPLE_INSTANCE == 1

#  define ACI( var )    ((VInstance *)pThis)->var
#  define DPRAM( var )  (*((VInstance *)pThis)->pDPRAM).var
#  define INSTANCE      void *pThis,
#  define INSTANCE_VOID void *pThis
#  define SELF          pThis,
#  define SELF_VOID     pThis

# else /* MULTIPLE_INSTANCE */

#  define ACI( var )    var
#  define DPRAM( var )  gDPRAM.var
#  define INSTANCE
#  define INSTANCE_VOID void
#  define SELF
#  define SELF_VOID

# endif /* MULTIPLE_INSTANCE */

#endif /* _INST_DEF_H_ */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for multiple instance support
  \par File name inst_def.h
  \version 1
  \date 4.04.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
