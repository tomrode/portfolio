/*--------------------------------------------------------------------
       SDODLYAC.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for SDO server add on for delayed data access.
  --------------------------------------------------------------------*/

#ifndef _SDODLYAC_H_
#define _SDODLYAC_H_


/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/


/*!
  \defgroup dlycodes state codes for delayed SDO
  @{
*/
#define DELAY_IDLE             0    /*!< no delayed sdo access running                        */
#define DELAY_WRITE_REQ        1    /*!< delayed download request received from remote client */
#define DELAY_READ_REQ         2    /*!< delayed upload request received from remote client   */
#define DELAY_COMPLETE         3    /*!< delayed data access completed                        */
#define DELAY_ERROR            4    /*!< error occurred at delayed access                     */
/*!
  end of group dlycodes
  @}
*/

#define DELAYED_SDO_TIMEOUT    2000 /*!< timeout for delayed data access                      */

#if EXAMPLE_OBJECTS_USED == 1
/* only for example code */
# define SDO_UP_DELAY           750  /* simulated SDO upload delay    */
# define SDO_DOWN_DELAY         500  /* simulated SDO download delay  */
#endif /* EXAMPLE_OBJECTS_USED == 1 */


/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/
/*!
  \brief delayed SDO type
*/
typedef struct _VDelayedSdoInfo {
  WORD wIndex;           /* index */
  BYTE bSubIndex;        /* sub-index */
  BYTE bObjInfo;         /* object related information (\ref dlycodes) */
  BYTE bLength;          /* object length */
  BYTE bExpedData[4];    /* buffer */
} VDelayedSdoInfo;


/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
BYTE gSdoS_DelayedUpload(BYTE);
BYTE gSdoS_DelayedDownload(BYTE);
BYTE gSdoS_DelayedTransferStatus(BYTE, BYTE **);
void gSdoS_FinishDelayedTransfer(BYTE, BYTE **);
void gSdoS_StopDelayedTransfer(BYTE);

#endif /* _SDODLYAC_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for SDO server add on for delayed data access.
  \par File name sdodlyac.h
  \version 6
  \date 6.03.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

