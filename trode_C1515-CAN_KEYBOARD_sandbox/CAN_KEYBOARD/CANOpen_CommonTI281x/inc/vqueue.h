/*--------------------------------------------------------------------
       VQUEUE.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for the generic queue interface
  --------------------------------------------------------------------*/

/*
START CONFIGURATOR CONTROL UNIT

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
VQ_CAN_MSG_QUEUE        | CAN Adaptation    |    BOOL |   BOOL  |     | 
VQ_POWER_OF_TWO         | CAN Adaptation    |    BOOL |   BOOL  |     | 
VQ_STRUCT_NAME          | CAN Adaptation    |    BOOL |   BOOL  |     | 
VQ_STRUCT_CALLBACK      | CAN Adaptation    |    BOOL |   BOOL  |     | 
VQ_SEMAPHORES           | CAN Adaptation    |    BOOL |   BOOL  |     | 
VQ_READ_AHEAD           | CAN Adaptation    |    BOOL |   BOOL  |     | 
VQ_WRITE_AHEAD          | CAN Adaptation    |    BOOL |   BOOL  |     | 
VQ_WRITE                | CAN Adaptation    |    BOOL |   BOOL  |     | 
VQ_READ                 | CAN Adaptation    |    BOOL |   BOOL  |     | 
VQ_ACCESS_INDEX         | CAN Adaptation    |    BOOL |   BOOL  |     | 

END CONFIGURATOR CONTROL UNIT
*/

#ifndef _VQUEUE_H_
#define _VQUEUE_H_


/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/

/*!
  \def VQ_CAN_MSG_QUEUE

  Determine if the queue entry size is equal the CAN message length.
  - 0 - Queue entry has other size.
  - 1 - Queue entry has size of the CAN message.
*/
#define VQ_CAN_MSG_QUEUE 1


/*!
  \def VQ_POWER_OF_TWO

  Determine if all queues carry entries of a size which is a power of two.
  - 0 - Different queues have different entry size.
  - 1 - All entries use the same size.
*/
#define VQ_POWER_OF_TWO 0


/*!
  \def VQ_STRUCT_NAME

  With this define the structure component name of a queue can be omitted.
  The following values are possible:
  - 0 - Do not use the name of the queue.
  - 1 - The queues name can be used.
*/
#define VQ_STRUCT_NAME 0


/*!
  \def VQ_STRUCT_CALLBACK

  With this define the structure component fnUser of a queue can be omitted.
  The following values are possible:
  - 0 - Do not use the callback fnUser of the queue.
  - 1 - The queues callback fnUser can be used.
*/
#define VQ_STRUCT_CALLBACK 0

/*!
  \brief Use semaphores for queue access.
*/
#define VQ_SEMAPHORES 1

/*!
  \def VQ_READ_AHEAD
  \brief Use read ahead routines.
*/
#define VQ_READ_AHEAD 1

/*!
  \def VQ_WRITE_AHEAD
  \brief Use write ahead routines.
*/
#define VQ_WRITE_AHEAD 1

/*!
  \def VQ_WRITE
  \brief Use standard queue write routine.
*/
#define VQ_WRITE 0

/*!
  \def VQ_READ
  \brief Use standard queue read routine.
*/
#define VQ_READ 0

/*!
  \brief Use indexed access to queue descriptors
*/
#define VQ_ACCESS_INDEX 0

/*!
  \def VQ_SET_WIDTH(x)
  \brief Set the width of a queue entry.

  If the width of a queue entry is fixed with #VQ_POWER_OF_TWO
  the width of an entry must be specified as the power of 2.
  Otherwise the size of an entry is not restricted.
*/
#if VQ_POWER_OF_TWO == 1
# define VQ_SET_WIDTH(x)  ((((sizeof(x) & 0x02) >> 0x01) * 0x01) \
                         + (((sizeof(x) & 0x04) >> 0x02) * 0x02) \
                         + (((sizeof(x) & 0x08) >> 0x03) * 0x03) \
                         + (((sizeof(x) & 0x10) >> 0x04) * 0x04) \
                         + (((sizeof(x) & 0x20) >> 0x05) * 0x05) \
                         + (((sizeof(x) & 0x40) >> 0x06) * 0x06) \
                         + (((sizeof(x) & 0x80) >> 0x07) * 0x07))
# define X_FOLD_BY <<
#else
# define VQ_SET_WIDTH(x) sizeof(x)
# define X_FOLD_BY *
#endif /* VQ_POWER_OF_TWO */

#define VQ_GET_WIDTH sizeof(CAN_MSG)

#define VQ_NOTEXIST 0   /*!< Queue doesn't exist */
#define VQ_OK       1   /*!< Function call executed successfully */
#define VQ_FULL     2   /*!< The queue is full */
#define VQ_EMPTY    3   /*!< The queue is empty */
#define VQ_LOCKED   4   /*!< The queue is locked (#VQ_SEMAPHORES) */
#define VQ_NOEXEC   5   /*!< The callback could not be executed */

#if VQ_ACCESS_INDEX == 1
# define FRONT qctrl[ptQueue].ptQBase->bFront
# define REAR  qctrl[ptQueue].ptQBase->bRear
# if VQ_CAN_MSG_QUEUE == 1
#  define WIDTH sizeof(CAN_MSG)
# else
#  define WIDTH qctrl[ptQueue].bWidth
# endif /* VQ_CAN_MSG_QUEUE */
# define NELEMENTS qctrl[ptQueue].bNel
# define VALID_HANDLE 1
# define QUE_BODY_BASE_ADR qctrl[ptQueue].ptQBase
# define QUE_CALLBACK qctrl[ptQueue].fnUser
# if VQ_SEMAPHORES == 1
#  define RDLOCK qctrl[ptQueue].ptQBase->RdLock
#  define WRLOCK qctrl[ptQueue].ptQBase->WrLock
# endif /* VQ_SEMAPHORES */
#else
# define FRONT (ptQueue->ptQBase)->bFront
# define REAR  (ptQueue->ptQBase)->bRear
# if VQ_CAN_MSG_QUEUE == 1
#  define WIDTH sizeof(CAN_MSG)
# else
#  define WIDTH ptQueue->bWidth
# endif /* VQ_CAN_MSG_QUEUE */
# define NELEMENTS ptQueue->bNel
# define VALID_HANDLE ptQueue
# define QUE_BODY_BASE_ADR ptQueue->ptQBase
# define QUE_CALLBACK ptQueue->fnUser
# if VQ_SEMAPHORES == 1
#  define RDLOCK (ptQueue->ptQBase)->RdLock
#  define WRLOCK (ptQueue->ptQBase)->WrLock
# endif /* VQ_SEMAPHORES */
#endif /* VQ_ACCESS_INDEX */


/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/

#if VQ_SEMAPHORES == 1
typedef BOOLEAN tVQ_LOCK; /*!< locking data type */
#endif /* VQ_SEMAPHORES */


/*!
  \brief Queue management structure.

  This structure is located in the first few addresses of the queue's body.
*/
typedef struct _tVQActual {
#if VQ_SEMAPHORES == 1
  tVQ_LOCK RdLock; /*!< Read lock for queue */
  tVQ_LOCK WrLock; /*!< Write lock for queue */
#endif /* VQ_SEMAPHORES */
  BYTE bRear;  /*!< Point where an element is removed from queue */
  BYTE bFront; /*!< Point where an element is filled into queue  */
} tVQActual;


/*!
  \brief Static queue definition structure.

  For every queue this structure has to be filled in.
  A queue is referenced with a pointer to this structure.
*/
typedef struct _tVQueue {
#if VQ_STRUCT_NAME == 1
  BYTE * name; /*!< Name of the queue */
#endif /* VQ_STRUCT_NAME */
#if VQ_STRUCT_CALLBACK == 1
  /*!
    \brief callback function

    This structure component is intended to be used as a place where
    a generic callback for a queue can be stored.
    The callback function does not need to be global in that case.

    \note The callback is not called implicitely.
  */
  void (*fnUser)(INSTANCE BYTE *);
#endif /* VQ_STRUCT_CALLBACK */
  BYTE bNel; /*!< Number of elements in queue */

  /*!
    \brief Size of one queue element.

    If #VQ_POWER_OF_TWO is set to 0, this value holds the size in bytes.

    If #VQ_POWER_OF_TWO is set to 1, it is only allowed to have entry
    sizes with a power of 2. This value holds the shift position
    (exponent) of the highest bit.

    \note The width of an entry is restricted to 128 or 255.
    Example: sizeof(unsigned char[8]) -> 0x08 -> bitposition 3
  */
#if VQ_CAN_MSG_QUEUE == 0
  BYTE bWidth;
#endif /* VQ_CAN_MSG_QUEUE */

  /*!
  Start address of queue data area.
  */
  tVQActual * ptQBase;  /*!< Point to queue */
} tVQUEUE;

#if VQ_ACCESS_INDEX == 1
typedef BYTE VQHANDLE; /*!< Queue's handle */
#else
typedef tVQUEUE CONST CODE * VQHANDLE; /*!< Queue's handle */
#endif /* VQ_ACCESS_INDEX */


/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/

BYTE gQue_FlushQueue(VQHANDLE);
#if VQ_WRITE == 1
BYTE gQue_Enqueue(VQHANDLE, BYTE XDATA *);
#endif /* VQ_WRITE */
#if VQ_READ == 1
BYTE gQue_Dequeue(VQHANDLE, BYTE XDATA *);
#endif /* VQ_READ */
#if VQ_READ_AHEAD == 1
BYTE XDATA * gQue_PeekTop(VQHANDLE) REENTRANT;
BYTE gQue_DetachTop(VQHANDLE) REENTRANT;
#endif /* VQ_READ_AHEAD */
#if VQ_WRITE_AHEAD == 1
BYTE XDATA * gQue_PeekFree(VQHANDLE) REENTRANT;
BYTE gQue_AttachFree(VQHANDLE) REENTRANT;
#endif /* VQ_WRITE_AHEAD */
BYTE gQue_IsElement(VQHANDLE);
BYTE gQue_Count(VQHANDLE);
BYTE gQue_ExecCallback(INSTANCE VQHANDLE, BYTE XDATA *);
#if VQ_SEMAPHORES == 1
void gQue_RdUnlock(VQHANDLE) REENTRANT;
void gQue_WrUnlock(VQHANDLE) REENTRANT;
#else
#define gQue_RdUnlock(a)
#define gQue_WrUnlock(a)
#endif /* VQ_SEMAPHORES */


#endif /* _VQUEUE_H_ */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for the generic queue interface
  \par File name vqueue.h
  \version 6
  \date 3.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

/*!
  \page queuepage10 Queue implementation

  \section queuemodule10 General

  The implemented queue interface is based on the assumption that the
  queue's storage space is already allocated statically by the user
  of the queue.

  The implemented queue interface provides the following functionality:
  - Organisation of a queue mechanism with queues of different sizes.
  - Queue elements can be of the same size for all queues or of different
    sizes for every queue.
  - Queue (resource) locking functionality is included.
  - Add and remove entries from/to a queue without look ahead.
  - Queue flush function available.
  - Boolean check is possible.
  - Number of available entries can be obtained.

  \section queuemodule20 Allocating a queue

  The queue's memory space is allocated by the user of the queue.
  Since there can be differnet queues be used at the same time they are
  differentiated with their handle (#tVQUEUE).
  A queue is resetted with Que_FlushQueue():
  There is no other special initialisation function for a queue.

  \section queuemodule30 Writing and reading a queue entry

  An entry can be written to the queue with the call Que_Enqueue().
  The entry can be removed from the queue with Que_Dequeue().
  Note that the function Que_IsElement() is used for checking
  the filling state of the queue.

  \section queuemodule40 Writing a queue with look ahead

  The write function to a queue can be divided into three basic actions.
  -# determine the free space pointer in the queue's body
  -# copy data to that location
  -# add the entry (change index)

  With the functions Que_PeekFree() and Que_AttachFree() the call of the
  appropriate copy function is up to the user of the queue interface.

  First of all a valid address in the queue's body has to be determined
  with Que_PeekFree().
  Afterwards the user has to copy the data contents to the allocated buffer
  space.
  Finally the queue must be released with Que_AttachFree().
  The new entry is commited now.

  This approach may be necessary under the following conditions:
  - a data item needs to be copied to the queue without any intermediate buffer
  - the copy routine is different from a simple call to memcpy

  \section queuemodule50 Reading a queue with look ahead

  If the queue is accessed with the function Que_Dequeue()
  an entry is removed from the queue and copied to an intermediate buffer.
  If this has to be avoided the functions Que_PeekTop() and Que_DetachTop()
  should be used to evaluate a queue entry.

  First of all the location of the next valid entry in the queue has to be
  determined with the function Que_PeekTop().

  After the contents of the entry have been evaluated the entry has to be
  detached with Que_DetachTop().

  \section queuemodule60 Locking functionality

  Since the queues can be seen as some kind of a ressource a basic locking
  mechanism is implemented.
  The semaphore (or handle to the semaphore) is stored in the queue's body.
  The locking feature is activated with #VQ_SEMAPHORES.
*/

