/*--------------------------------------------------------------------
       PROFIL.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Communication and device profile
  --------------------------------------------------------------------*/

#ifndef _PROFIL_H_
#define _PROFIL_H_



/*
START CONFIGURATOR CONTROL UNIT

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
OUTPUT_BUFFER_LEN       | Process Data      | GENERAL |   TOKEN |     |
INPUT_BUFFER_LEN        | Process Data      | GENERAL |   TOKEN |     |

END CONFIGURATOR CONTROL UNIT
*/

/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/

/* number of entries in object 1029 */
#if OBJECT_1029_USED == 1
# if GENERIC_PROFILE_USED == 1
#  if PROFILE_DS401 == 1
#   define NUM_ERROR_CLASSES 3
#  else
#   define NUM_ERROR_CLASSES 1
#  endif /* PROFILE_DS401 == 1 */
# else
#  define NUM_ERROR_CLASSES  1
# endif /* GENERIC_PROFILE_USED == 1 */
#endif /* OBJECT_1029_USED == 1 */

#if PROFILE_DS401 == 1
# if OBJECT_6422_USED == 1
   /* number of interrupt source banks used in object 6422 */
#  define NUM_AI_INTSOURCE ((INPUT_BUFFER_LEN_AI / 32) + 1)
# endif /* OBJECT_6422_USED == 1 */
#endif /* PROFILE_DS401 == 1 */

/*!
  \defgroup od_datatypes object dictionary data types
  @{
*/
#define OBJECT_UNSIGNED8       0x05 /*!< unsigned integer 8 */
#define OBJECT_UNSIGNED16      0x06 /*!< unsigned integer 16 */
#define OBJECT_UNSIGNED32      0x07 /*!< unsigned integer 32 */
#define OBJECT_UNSIGNED24      0x16 /*!< unsigned integer 24 */
/*!
  @} end group od_datatypes
*/

/*!
  \defgroup od_comobjects communication relevant objects
  @{
*/
#define DEVICE_TYPE                0x1000 /*!< device type                   */
#define ERROR_REG                  0x1001 /*!< error register                */
#define STATUS_REG                 0x1002 /*!< manufacturer status register  */
#define ERROR_FIELD                0x1003 /*!< pre-defined error field       */
#define COB_ID_SYNC                0x1005 /*!< COB-ID SYNC                   */
#define SYNC_CYCLE_PERIOD          0x1006 /*!< communication cycle period    */
#define SYNC_WINDOW_LENGTH         0x1007 /*!< synchronous window length     */
#define DEVICE_NAME                0x1008 /*!< manufacturer device name      */
#define HW_VER                     0x1009 /*!< manufacturer hardware version */
#define SW_VER                     0x100A /*!< manufacturer software version */
#define GUARD_TIME                 0x100C /*!< guard time                    */
#define LIFE_TIME_FACTOR           0x100D /*!< life time factor              */
#if STORE_PARAMETERS_SUPP == 1
# define STORE_PARAMETERS          0x1010 /*!< store parameters              */
# define RESTORE_PARAMETERS        0x1011 /*!< restore parameters            */
#endif /* STORE_PARAMETERS_SUPP == 1 */
#if TIME_STAMP_USED == 1
# define COB_ID_TIME_STAMP         0x1012 /*!< COB-ID TIME                   */
# define TIME_STAMP                0x1013 /*!< high resolution time stamp    */
#endif /* TIME_STAMP_USED == 1 */
#define COB_ID_EMCY                0x1014 /*!< COB-ID EMCY                   */
#define INHIBIT_TIME_EMCY          0x1015 /*!< inhibit time EMCY             */
#define CON_HEARTBEAT_TIME         0x1016 /*!< consumer heartbeat time       */
#define PRO_HEARTBEAT_TIME         0x1017 /*!< producer heartbeat time       */
#define IDENTITY_OBJECT            0x1018 /*!< identity object               */
#if OBJECT_1029_USED == 1
# define ERROR_BEHAVIOUR           0x1029 /*!< error behaviour; substitution of 67FEh  */
#endif /* OBJECT_1029_USED == 1 */
#define SDO_SERVER_PAR             0x1200 /*!< server SDO parameters         */
#define SDO_CLIENT_PAR             0x1280 /*!< client SDO parameters         */
#define RECEIVE_PDO_1_COMMUNI_PAR  0x1400 /*!< receive PDO communication parameter */
#define RECEIVE_PDO_1_MAPPING_PAR  0x1600 /*!< receive PDO mapping parameter */
#define TRANSMIT_PDO_1_COMMUNI_PAR 0x1800 /*!< transmit PDO communication parameter*/
#define TRANSMIT_PDO_1_MAPPING_PAR 0x1A00 /*!< transmit PDO mapping parameter */
#define NMT_STARTUP                0x1F80 /*!< device start-up behaviour     */
/*!
  @} end group od_comobjects
*/

#if EXAMPLE_OBJECTS_USED == 1
# define EXAMPLE_OBJECT1           0x2001 /*!< example object 1 */
# define EXAMPLE_OBJECT2           0x2002 /*!< example object 2 */
# define EXAMPLE_OBJECT3           0x2003 /*!< example object 3 */
# define EXAMPLE_OBJECT4           0x2004 /*!< example object 4 */
# define EXAMPLE_OBJECT5           0x2005 /*!< example object 4 */
#endif /* EXAMPLE_OBJECTS_USED == 1 */

#if SCOFLD_SUPPORT == 1
# define CONFIGURE_SCOFLD          0x3000 /*!< configuration of flash loader */
#endif /* SCOFLD_SUPPORT == 1 */

#define OBJ_LEN8   8  /*!< object length 8 bits */
#define OBJ_LEN16  16 /*!< object length 16 bits */
#define OBJ_LEN24  24 /*!< object length 24 bits */
#define OBJ_LEN32  32 /*!< object length 32 bits */
#if OBJECTS_64BIT_USED == 1
# define OBJ_LEN64  64 /*!< object length 64 bits */
#endif /* OBJECTS_64BIT_USED == 1 */

/*!
  \defgroup od_profobjects profile relevant objects
  @{
*/
#define DIGI_OUT_8BIT               0x6200 /*!< 8 bit digital output */
#define DIGI_OUT_16BIT              0x6300 /*!< 16 bit digital output */
#define DIGI_OUT_32BIT              0x6320 /*!< 32 bit digital output */

#define DIGI_IN_8BIT                0x6000 /*!< 8 bit digital input      */
#define DIGI_IN_16BIT               0x6100 /*!< 16 bit digital input */
#define DIGI_IN_32BIT               0x6120 /*!< 32 bit digital input */

#if PROFILE_DS401 == 1
# define POLARITY_OUT_8BIT          0x6202 /*!< 8 bit polarity output    */
# define FILTER_OUT_8BIT            0x6208 /*!< 8 bit filter mask output */
# define POLARITY_IN_8BIT           0x6002 /*!< 8 bit polarity input     */
# define FILTER_IN_8BIT             0x6003 /*!< 8 bit filter constant input              */
# define GLOB_INT_ENABLE            0x6005 /*!< 8 bit digital input global INT enable    */
# define INT_MASK_ANY_CHANGE        0x6006 /*!< 8 bit digital input INT mask any change  */
# define INT_MASK_LOW_TO_HIGH       0x6007 /*!< 8 bit digital input INT mask low to high */
# define INT_MASK_HIGH_TO_LOW       0x6008 /*!< 8 bit digital input INT mask high to low */
# define ANA_IN_16BIT               0x6401 /*!< 16 bit analogue input  */
# define ANA_OUT_16BIT              0x6411 /*!< 16 bit analogue output */
# define ANA_IN_INT_TRIGGER_SELECT  0x6421 /*!< analogue input interrupt trigger selection   */
# define ANA_IN_INT_SOURCE          0x6422 /*!< analogue input interrupt source              */
# define ANA_IN_GLOBAL_INT_ENABLE   0x6423 /*!< analogue input global interrupt enable       */
# define ANA_IN_INT_UPPER_LIMIT     0x6424 /*!< analogue input interrupt upper limit integer */
# define ANA_IN_INT_LOWER_LIMIT     0x6425 /*!< analogue input interrupt lower limit integer */
# define ANA_IN_INT_DELTA_UNSIG     0x6426 /*!< analogue input interrupt delta unsigned      */
# define ANA_IN_INT_NEG_DELTA_UNSIG 0x6427 /*!< analogue input interrupt negative delta unsigned */
# define ANA_IN_INT_POS_DELTA_UNSIG 0x6428 /*!< analogue input interrupt positive delta unsigned */
# define ANA_OUT_ERROR_MODE         0x6443 /*!< analogue output error mode                       */
# define ANA_OUT_ERROR_VALUE        0x6444 /*!< analogue output error value integer              */
#endif /* PROFILE_DS401 == 1 */


/*!
  \def OUTPUT_BUFFER_LEN
  Output buffer size (user adaptable).

  This value describes the number of data bytes which have to be written to
  the process environment.
  The value directly influences the usage of the buffer #INPUT_DATA().
  The values from this buffer are transferred via RPDO.
  The user's application takes the data from this location and
  makes some further calculations.
  According to the value of #RPDO_MAP_READONLY the location of data items
  transferred via RPDO is fixed or variable.
  If we use a fixed mapping every RPDO shall assigned an area of exactly
  8 bytes in this buffer.
*/
#define OUTPUT_BUFFER_LEN (8 * 4)

/*!
  \def INPUT_BUFFER_LEN
  Input buffer size (user adaptable).

  This value describes the number of data bytes which have to be read from
  the process environment.
  The value directly influences the usage of the buffer #INPUT_DATA().
  The values from this buffer are transferred via TPDO.
  The user's application puts the data to this location and
  the stack transfers them.
  According to the value of #TPDO_MAP_READONLY the location of data items
  transferred via TPDO is fixed or variable.
  If we use a fixed mapping every TPDO shall assigned an area of exactly
  8 bytes in this buffer.
*/
#define INPUT_BUFFER_LEN (8 * 4)

/*!
  @} end of group od_profobjects
*/


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
/* externals from objvars.h */
#if MAX_RX_PDOS > 0
extern BYTE XDATA abOutData[]; /*!< output buffer */
#endif /* MAX_RX_PDOS > 0 */
#if MAX_TX_PDOS > 0
extern BYTE XDATA abInData[];  /*!< input buffer  */
#endif /* MAX_TX_PDOS > 0 */

#if PROFILE_DS401 == 1
# if MAX_RX_PDOS > 0
extern BYTE XDATA abOutDataDig[]; /*!< output buffer digital  */
extern WORD XDATA awOutDataAna[]; /*!< output buffer analogue */
# endif /* MAX_RX_PDOS > 0 */

# if MAX_TX_PDOS > 0
extern BYTE XDATA abInDataDig[];  /*!< input buffer digital   */
extern WORD XDATA awInDataAna[];  /*!< input buffer analogue  */
# endif /* MAX_TX_PDOS > 0 */

# if DS401_TEST == 1
extern BYTE XDATA abInRawDig[]; /*!< raw input buffer digital  */
extern WORD XDATA awInRawAna[]; /*!< raw input buffer analogue */
# endif /* DS401_TEST == 1 */
#endif /* PROFILE_DS401 == 1 */


/*--------------------------------------------------------------------*/
/*  macros                                                            */
/*--------------------------------------------------------------------*/
/*!
  \def OUTPUT_DATA(location)

  \brief Access output data

  The stack uses the array #abOutData to access data in the process data
  output environment.
*/

/*!
  \def INPUT_DATA(location)

  \brief Access input data

  The stack uses the array #abInData to access data in the process data
  input environment.
*/
#if EXAMPLE_PROFILE_USED == 1
# define OUTPUT_DATA(location) (&abOutData[location])
# define INPUT_DATA(location) (&abInData[location])
#else
# define OUTPUT_DATA(location) 0
# define INPUT_DATA(location) 0
#endif /* EXAMPLE_PROFILE_USED == 1 */


/*!
  \def OUTPUT_DATA_DIG(location)

  \brief Access output data for digital outputs

  The stack uses the array #abOutDataDig to access data in the digital 
  process data output environment for the I/O profile DS-401.
*/

/*!
  \def OUTPUT_DATA_ANA(location)

  \brief Access output data for analogue outputs

  The stack uses the array #awOutDataAna to access data in the analogue
  process data output environment for the I/O profile DS-401.
*/
#if PROFILE_DS401 == 1
# if MAX_RX_PDOS > 0
#  define OUTPUT_DATA_DIG(location) (&abOutDataDig[location])
#  define OUTPUT_DATA_ANA(location) ((BYTE *)&awOutDataAna[location])
# endif /* MAX_RX_PDOS > 0 */
#endif /* PROFILE_DS401 == 1 */


/*!
  \def INPUT_DATA_DIG(location)

  \brief Access input data for digital inputs

  The stack uses the array #abInDataDig to access data in the digital
  process data input1 environment for the I/O profile DS-401.
*/

/*!
  \def INPUT_DATA_ANA(location)

  \brief Access input data for analogue inputs

  The stack uses the array #awInDataAna to access data in the analogue
  process data input environment for the I/O profile DS-401.
*/
#if PROFILE_DS401 == 1
# if MAX_TX_PDOS > 0
#  define INPUT_DATA_DIG(location) (&abInDataDig[location])
#  define INPUT_DATA_ANA(location) ((BYTE *)&awInDataAna[location])
# endif /* MAX_TX_PDOS > 0 */
#endif /* PROFILE_DS401 == 1 */


#if PROFILE_DS401 == 1

  /*! profile specific initialization */
# define PROFILE_INIT() gIO_Init()

  /*! profile specific circular task for PRE-OPERATIONAL state */
# define PROFILE_TASK_PREOPERATIONAL() gIO_Task()

  /*! profile specific circular task for OPERATIONAL state */
# define PROFILE_TASK_OPERATIONAL() gIO_Task()

  /*! profile specific evaluation of received PDOs */
# define PROFILE_PDO_ANALYZE(idx, subidx) gIO_EvaluateOutputValues(idx, subidx)

  /*! profile specific creation of transmit PDOs */
# define PROFILE_PDO_CREATE(idx, subidx, dataptr) \
                                 gIO_CheckValChangedInput(idx, subidx, dataptr)

  /*! profile specific handling on SDO write access */
# define PROFILE_SDO_DOWN(idx, subidx) gIO_EvaluateOutputValues(idx, subidx)

  /*! profile specific handling on SDO read access */
# define PROFILE_SDO_UP(idx, subidx) gIO_ObjectTxHandling(idx, subidx)

  /*! profile specific handling on PDO transmission */
# define PROFILE_PDO_TX(idx, subidx) gIO_ObjectTxHandling(idx, subidx)

  /*! profile specific error checking */
# define PROFILE_CHECK_ERRORS() gIO_CheckError()

#else

  /*! profile specific initialization */
# define PROFILE_INIT()

  /*! profile specific circular task for PRE-OPERATIONAL state */
# define PROFILE_TASK_PREOPERATIONAL()

  /*! profile specific circular task for OPERATIONAL state */
# define PROFILE_TASK_OPERATIONAL()

  /*! profile specific evaluation of received PDOs */
# define PROFILE_PDO_ANALYZE(idx, subidx)

  /*! profile specific creation of transmit PDOs */
# define PROFILE_PDO_CREATE(idx, subidx, dataptr)

  /*! profile specific handling on SDO write access */
# define PROFILE_SDO_DOWN(idx, subidx)

  /*! profile specific handling on SDO read access */
# define PROFILE_SDO_UP(idx, subidx)

  /*! profile specific handling on PDO transmission */
# define PROFILE_PDO_TX(idx, subidx)

  /*! profile specific error checking */
# define PROFILE_CHECK_ERRORS()

#endif /* PROFILE_DS401 == 1 */



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/

#endif /* _PROFIL_H_ */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Communication and device profile.
  \par File name profil.h
  \version 15
  \date 25.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

