/*--------------------------------------------------------------------
       VDBGPRNT.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Debug macro definitions
  --------------------------------------------------------------------*/

#ifndef _VDBGPRNT_H
#define _VDBGPRNT_H



/*--------------------------------------------------------------------*/
/*  include files, macros                                             */
/*--------------------------------------------------------------------*/

#ifdef WIN32
# ifdef _MSC_VER
#  include <crtdbg.h> /* prototype for _RPT... macros */

#  define VDEBUG_SETUP                                              \
     do {                                                           \
       (void)_CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);       \
       (void)_CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDOUT);     \
       (void)_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);      \
       (void)_CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDOUT);    \
       (void)_CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);     \
       (void)_CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDOUT);   \
     } while (0)

#  define VDEBUG_RPT0( _msg )                     _RPT0(_CRT_WARN, _msg )
#  define VDEBUG_RPT1( _msg, arg1 )               _RPT1(_CRT_WARN, _msg, arg1 )
#  define VDEBUG_RPT2( _msg, arg1, arg2 )         _RPT2(_CRT_WARN, _msg, arg1, arg2 )
#  define VDEBUG_RPT3( _msg, arg1, arg2, arg3 )   _RPT3(_CRT_WARN, _msg, arg1, arg2, arg3 )
#  define VDEBUG_RPTE0( _msg )                    _RPTF0(_CRT_WARN, _msg )
#  define VDEBUG_RPTE1( _msg, arg1 )              _RPTF1(_CRT_WARN, _msg, arg1 )
#  define VDEBUG_RPTE2( _msg, arg1, arg2 )        _RPTF2(_CRT_WARN, _msg, arg1, arg2 )
#  define VDEBUG_RPTE3( _msg, arg1, arg2, arg3 )  _RPTF3(_CRT_WARN, _msg, arg1, arg2, arg3 )
# else
#  define VDEBUG_SETUP                            ((void)0)
#  define VDEBUG_RPT0( _msg )                     ((void)0)
#  define VDEBUG_RPT1( _msg, arg1 )               ((void)0)
#  define VDEBUG_RPT2( _msg, arg1, arg2 )         ((void)0)
#  define VDEBUG_RPT3( _msg, arg1, arg2, arg3 )   ((void)0)
#  define VDEBUG_RPTE0( _msg )                    ((void)0)
#  define VDEBUG_RPTE1( _msg, arg1 )              ((void)0)
#  define VDEBUG_RPTE2( _msg, arg1, arg2 )        ((void)0)
#  define VDEBUG_RPTE3( _msg, arg1, arg2, arg3 )  ((void)0)
# endif /* _MSC_VER */
#else
# ifndef NDEBUG
#  include <stdio.h> /* prototype for 'printf' */

#  define VDEBUG_SETUP ((void)0)

#  define VDEBUG_RPT0( _msg )   \
     do {                       \
       printf(_msg);            \
     } while (0)

#  define VDEBUG_RPT1( _msg, arg1 )         \
     do {                                   \
       printf( "RPT1: ("#_msg")", arg1);    \
     } while (0)

#  define VDEBUG_RPT2( _msg, arg1, arg2 )       \
     do {                                       \
       printf( "RPT2: ("#_msg")", arg1, arg2);  \
     } while (0)

#  define VDEBUG_RPT3( _msg, arg1, arg2, arg3 )         \
     do {                                               \
       printf( "RPT3: ("#_msg")", arg1, arg2, arg3);    \
     } while (0)

#  define VDEBUG_RPTE0( _msg )                          \
     do {                                               \
       printf( "RPTE0 file %s, line %d: ("#_msg")",     \
               __FILE__, __LINE__ );                    \
     } while (0)

#  define VDEBUG_RPTE1( _msg, arg1 )                    \
     do {                                               \
       printf( "RPTE1 file %s, line %d: ("#_msg")",     \
               __FILE__, __LINE__, arg1 );              \
     } while (0)

#  define VDEBUG_RPTE2( _msg, arg1, arg2 )              \
     do {                                               \
       printf( "RPTE2 file %s, line %d: ("#_msg")",     \
               __FILE__, __LINE__, arg1, arg2 );        \
     } while (0)

#  define VDEBUG_RPTE3( _msg, arg1, arg2, arg3 )        \
     do {                                               \
       printf( "RPTE3 file %s, line %d: ("#_msg")",     \
               __FILE__, __LINE__, arg1, arg2, arg3 );  \
     } while (0)

# else

#  define VDEBUG_SETUP                            ((void)0)
#  define VDEBUG_RPT0( _msg )                     ((void)0)
#  define VDEBUG_RPT1( _msg, arg1 )               ((void)0)
#  define VDEBUG_RPT2( _msg, arg1, arg2 )         ((void)0)
#  define VDEBUG_RPT3( _msg, arg1, arg2, arg3 )   ((void)0)
#  define VDEBUG_RPTE0( _msg )                    ((void)0)
#  define VDEBUG_RPTE1( _msg, arg1 )              ((void)0)
#  define VDEBUG_RPTE2( _msg, arg1, arg2 )        ((void)0)
#  define VDEBUG_RPTE3( _msg, arg1, arg2, arg3 )  ((void)0)
# endif /* NDEBUG */
#endif /* WIN32 */

#endif /* _VDBGPRNT_H */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief This file defines debug macros to be used during projects.
  \par File name vdbgprnt.h
  \version 3
  \date 6.03.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

/*!
  \def VDEBUG_SETUP
  \brief Prepare the debug output channels

  In some environments it is necessary to prepare the channels used
  for input and output previously to usage.
  In an embedded environment it may be necessary to initialize
  the serial interface for instance.
*/

/*!
  \def VDEBUG_RPT0( _msg )
  \brief Print out _msg.

  This macro prints the message _msg over the selected channel.
*/

/*!
  \def VDEBUG_RPT1( _msg, arg1 )             
  \brief Print out _msg with argument arg1.

  This macro prints the message _msg with argument arg1
  over the selected channel.
*/

/*!
  \def VDEBUG_RPT2( _msg, arg1, arg2 )       
  \brief Print out _msg with arguments arg1 and arg 2.

  This macro prints the message _msg with the arguments arg1 and arg 2
  over the selected channel.
*/

/*!
  \def VDEBUG_RPT3( _msg, arg1, arg2, arg3 ) 
  \brief Print out _msg with arguments arg1, arg 2 and arg3.

  This macro prints the message _msg with the arguments arg1, arg 2 and arg3
  over the selected channel.
*/

/*!
  \def VDEBUG_RPTE0( _msg )                  
  \brief Print out _msg.
  The FILE and LINE information is added.

  This macro prints the message _msg over the selected channel.
*/

/*!
  \def VDEBUG_RPTE1( _msg, arg1 )            
  \brief Print out _msg with argument arg1.
  The FILE and LINE information is added.

  This macro prints the message _msg with argument arg1
  over the selected channel.
*/

/*!
  \def VDEBUG_RPTE2( _msg, arg1, arg2 )      
  \brief Print out _msg with arguments arg1 and arg 2.
  The FILE and LINE information is added.

  This macro prints the message _msg with the arguments arg1 and arg 2
  over the selected channel.
*/

/*!
  \def VDEBUG_RPTE3( _msg, arg1, arg2, arg3 )
  \brief Print out _msg with arguments arg1, arg 2 and arg3.
  The FILE and LINE information is added.

  This macro prints the message _msg with the arguments arg1, arg 2 and arg3
  over the selected channel.
*/


