/*--------------------------------------------------------------------
       EMCYHNDL.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for EMCY handler
  --------------------------------------------------------------------*/

/*
START CONFIGURATOR CONTROL UNIT

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
ERR_FIELDS              | EMCY Handling     | GENERAL |    INT  |   1 | 254
EMCY_MAN_SPEC           | EMCY Handling     |    BOOL |   BOOL  |     |

END CONFIGURATOR CONTROL UNIT
*/

#ifndef _EMCYHNDL_H_
#define _EMCYHNDL_H_


/*--------------------------------------------------------------------*/
/*  global defines                                                    */
/*--------------------------------------------------------------------*/
/*
  List of EMCY error codes (object 1003)
*/

#define ECY_NO_ERROR     0x0000 /*!< error reset or no error          */
#define ECY_GENERIC      0x1000 /*!< generic error                    */

#define ECY_GEN_CURR     0x2000 /*!< current                          */
#define ECY_INP_CURR     0x2100 /*!< current, device input side       */
#define ECY_INSIDE_CURR  0x2200 /*!< current, inside of the device    */
#define ECY_OUT_CURR     0x2300 /*!< current, device output side      */

#define ECY_GEN_VLT      0x3000 /*!< voltage                          */
#define ECY_MAIN_VLT     0x3100 /*!< voltage, mains                   */
#define ECY_INS_VLT      0x3200 /*!< voltage, inside of the device    */
#define ECY_OUT_VLT      0x3300 /*!< voltage, output                  */

#define ECY_GEN_TEMP     0x4000 /*!< temperature                      */
#define ECY_AMB_TEMP     0x4100 /*!< temperature, ambient             */
#define ECY_DEV_TEMP     0x4200 /*!< temperature, device              */

#define ECY_GEN_HW       0x5000 /*!< hardware                         */

#define ECY_GEN_SW       0x6000 /*!< software                         */
#define ECY_INTERN_SW    0x6100 /*!< software, internal               */
#define ECY_USER_SW      0x6200 /*!< software, user                   */
#define ECY_DATA_SW      0x6300 /*!< software, data set               */

#define ECY_GEN_ADDMOD   0x7000 /*!< additional modules               */

#define ECY_GEN_MON      0x8000 /*!< monitoring                       */
#define ECY_COM_MON      0x8100 /*!< monitoring, communication        */
#define ECY_OVR_MON      0x8110 /*!< monitoring, CAN overrun (message lost) */
#define ECY_STATE_MON    0x8120 /*!< monitoring, CAN switched to error passive */
#define ECY_ERR_MON      0x8130 /*!< monitoring, error control failure  */
#define ECY_BOFF_MON     0x8140 /*!< monitoring, recovered from bus off */
#define ECY_COB_COLL     0x8150 /*!< monitoring, COB-ID collision       */
#define ECY_GEN_PROT     0x8200 /*!< protocol error                     */
#define ECY_LEN_EXC_PROT 0x8210 /*!< PDO not processed due to length error */
#define ECY_LEN_PROT     0x8220 /*!< PDO length exceeded                */

#define ECY_GEN_EXT      0x9000 /*!< external problem                   */

#define ECY_ADD_FCT      0xF000 /*!< additional function                */

#define ECY_GEN_DEV      0xFF00 /*!< device specific                    */

/* flags in error register (object 1001) */

#define EREG_GENERIC     0x01   /*!< generic error                */
#define EREG_CURRENT     0x02   /*!< current                      */
#define EREG_VOLTAGE     0x04   /*!< voltage                      */
#define EREG_TEMP        0x08   /*!< temperature                  */
#define EREG_COMM        0x10   /*!< communication error          */
#define EREG_DEV_PROF    0x20   /*!< device profile specific      */
#define EREG_RESERVED    0x40   /*!< reserved                     */
#define EREG_MANUFACT    0x80   /*!< manufacturer specific        */

/*!
  \def ERR_FIELDS
  Number of supported entries for error field (1-254)

  \ingroup emcygroup1
*/
#define ERR_FIELDS   2

/*!
  \def EMCY_MAN_SPEC
  Manufacturer specific error field support.

  The EMCY message contains a manufacturer specific error field to
  distribute additional error information. 
  - 0 No manufacturer specific error field supported (always zero).
  - 1 Manufacturer specific error field available.

  \ingroup emcygroup1
*/
#define EMCY_MAN_SPEC    0

#define EMCY_NO_ERR  0 /*!< EMCY state machine -> error free      */
#define EMCY_ERR_OCC 1 /*!< EMCY state machine -> error occurred  */

#define EMCY_MSG_EMPTY    0 /*!< no EMCY code available                         */
#define EMCY_MSG_NEW      1 /*!< new EMCY information invoked - not transmitted */
#define EMCY_MSG_NEW_ADEL 2 /*!< new EMCY information invoked with auto delete  */
#define EMCY_MSG_ACTIVE   3 /*!< EMCY information transmitted successfully      */
#define EMCY_MSG_DEL      4 /*!< EMCY information marked for deletion           */


/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/
/*!
  \brief Error field contents

  Additionallly to the raw EMCY error code we need the information if the
  EMCY message has already signaled the error.
  This is mainly necessary to avoid lost information
  due to inhibited messages.
*/
typedef struct _vErrField {
  BYTE bSignaled;   /*!< stores information if entry is signaled or not */
  WBYTE wbErrField; /*!< error field                                    */
#if EMCY_MAN_SPEC == 1
  BYTE bManSpec[5]; /*!< manufacturer specific error field              */
#endif /* EMCY_MAN_SPEC == 1 */
} vErrField;


/*!
  \brief EMCY handling structure

  This structure holds all the relevant information to manage
  to manage the EMCY message.
  We need the following items:
  - state machine
  - error field
  - error register
  - inhibit time information
*/
typedef struct _vEmcyManage {
  BYTE bErrReg;                 /*!< error register - refers to object 1001  */
#if ENABLE_EMCYMSG == 1
  BYTE bStatus;                 /*!< EMCY state machine                      */
  BYTE bNumber;                 /*!< holds number of stored error codes      */
  vErrField mErrInfo[ERR_FIELDS]; /*!< error field entries                   */
  WBYTE wbEmcyInhTime;          /*!< inhibit time counter for EMCY message   */
#endif /* ENABLE_EMCYMSG == 1 */
} vEmcyManage;


/*--------------------------------------------------------------------*/
/*  macros                                                            */
/*--------------------------------------------------------------------*/
#if USE_CANOE_EMU == 1
extern BYTE tgtSetEmcy(WORD, BYTE);
# define gSetEmcyInternal(a, b)  tgtSetEmcy((a), (b))
#else
# if EMCY_MAN_SPEC == 1
#  define gSetEmcyInternal(a, b)  gSetEmcy((a), (b), (BYTE *)0)
# else
#  define gSetEmcyInternal(a, b)  gSetEmcy((a), (b))
# endif /* EMCY_MAN_SPEC == 1 */
#endif /* USE_CANOE_EMU == 1 */


/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void gResetPredefErrors(void);
#if EMCY_MAN_SPEC == 1
BYTE gSetEmcy(WORD, BYTE, BYTE *);
#else
BYTE gSetEmcy(WORD, BYTE);
#endif /* EMCY_MAN_SPEC == 1 */

#if ENABLE_EMCYMSG==1
void gCheckEmcyTransmit(void);
#else
# define gCheckEmcyTransmit()
#endif /* ENABLE_EMCYMSG==1 */

#endif /* _EMCYHNDL_H_ */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/
/*!
  \file
  \brief Header for EMCY handler.
  \par File name emcyhndl.h
  \version 13
  \date 29.08.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

