/*--------------------------------------------------------------------
       CANCNTRL.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: CAN controller functions header
  --------------------------------------------------------------------*/

#ifndef _CANCNTRL_H_
#define _CANCNTRL_H_



/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/
#define EXTENDED_ID_USED 0

#define CAN_STS_NORMAL   0x00  /*!< CAN controller status normal   */
#define CAN_STS_RESET    0x01  /*!< CAN controller status reset    */
#define CAN_STS_WARNING  0x02  /*!< CAN controller status warning  */
#define CAN_STS_BUS_OFF  0x04  /*!< CAN controller status bus off  */
#define CAN_STS_SLEEPING 0x08  /*!< CAN controller status sleeping */
#define CAN_STS_OVERRUN  0x10  /*!< CAN controller status overrun  */
#define CAN_STS_TX_OK    0x20  /*!< CAN controller tx ok           */

#define CAN_NUM_ID       2048  /*!< number of basic identifiers    */
#define CAN_ID_LIMIT     0xFFF /*!< magic max number               */
#define CAN_MASK         0xFF  /*!< no acceptance mask             */
#define CAN_CODE         0x00  /*!< accept all identifiers         */

/* buffer status */
#define BUF_EMPTY        0     /*!< buffer empty or unused         */
#define BUF_FULL         1     /*!< buffer contents in evaluation  */
#define BUF_SENT         2     /*!< buffer has been transmitted    */
#define BUF_RECEIVED     3     /*!< buffer received                */
#define BUF_COMPLETE     4     /*!< buffer ready to transmit       */
#define BUF_ABORT        5     /*!< buffer transmission cancelled  */
#define BUF_NOT_SENT     6     /*!< buffer not transmitted         */
#define BUF_TRANSMIT     7     /*!< buffer in transmission         */

#define NO_MESSAGE       255   /*!< no message available           */

#define MODE_CONFIG      1     /*!< enter configuration mode       */ 
#define MODE_NORMAL      2     /*!< enter normal operation mode    */
#define MODE_SLEEP       3     /*!< enter sleep mode               */
#define MODE_LISTEN      4     /*!< enter listen only mode         */
#define MODE_LOOPBK      5     /*!< enter loopback mode            */

#define SELECTABLE_BITTIMES 9  /*!< size of bittiming table        */
#define BR_1M           8      /*!< baud rate   1M Bit/s           */
#define BR_800K         7      /*!< baud rate 800K Bit/s           */
#define BR_500K         6      /*!< baud rate 500K Bit/s           */
#define BR_250K         5      /*!< baud rate 250K Bit/s           */
#define BR_125K         4      /*!< baud rate 125K Bit/s           */
#define BR_100K         3      /*!< baud rate 100K Bit/s           */
#define BR_50K          2      /*!< baud rate  50K Bit/s           */
#define BR_20K          1      /*!< baud rate  20K Bit/s           */
#define BR_10K          0      /*!< baud rate  10K Bit/s           */


#if TIME_STAMP_USED == 1
# define TIME_STAMP_BUFFER 1
#else
# define TIME_STAMP_BUFFER 0
#endif /* TIME_STAMP_USED == 1 */

#if SYNC_USED == 1
# define SYNC_RX_BUFFER 1
#else
# define SYNC_RX_BUFFER 0
#endif /* SYNC_USED == 1 */

#if ENABLE_EMCYMSG == 1
# define EMCY_TX_BUFFER 1
#else
# define EMCY_TX_BUFFER 0
#endif /* ENABLE_EMCYMSG == 1 */

#if SYNC_PRODUCER == 1
# define SYNC_TX_BUFFER 1
#else
# define SYNC_TX_BUFFER 0
#endif /* SYNC_PRODUCER == 1 */

#if MULTI_SDO_SERVER == 1
# define SRSDO_RX_BUFFER NUM_SDO_SERVERS
# define SRSDO_TX_BUFFER NUM_SDO_SERVERS
#else
# define SRSDO_RX_BUFFER 1
# define SRSDO_TX_BUFFER 1
#endif /* MULTI_SDO_SERVER == 1 */

#if CLIENT_ENABLE == 1
# define CLSDO_RX_BUFFER 1
# define CLSDO_TX_BUFFER 1
#else
# define CLSDO_RX_BUFFER 0
# define CLSDO_TX_BUFFER 0
#endif /* CLIENT_ENABLE == 1 */

#if HEARTBEAT_CONSUMER == 1
# define HEARTBEAT_CON_BUFFER NUM_OF_HB_CONSUMERS
#else
# define HEARTBEAT_CON_BUFFER 0
#endif /* HEARTBEAT_CONSUMER == 1 */

#if ENABLE_NMT_ERROR_CTRL == 0
# define NMT_ERR_CTRL_BUFFER 0
#else
# define NMT_ERR_CTRL_BUFFER 1
#endif /* ENABLE_NMT_ERROR_CTRL == 0 */

#if NMT_MASTER_ENABLE == 1
# define NMT_MASTER_BUFFER 1
#else
# define NMT_MASTER_BUFFER 0
#endif /* NMT_MASTER_ENABLE == 1 */

#if NMT_GUARDING_ENABLE == 1
# define GUARDING_RX_BUFFER NMT_NODES_TO_GUARD
# define GUARDING_TX_BUFFER 1
#else
# define GUARDING_RX_BUFFER 0
# define GUARDING_TX_BUFFER 0
#endif /* NMT_GUARDING_ENABLE == 1 */

#if ENABLE_LAYER_SERVICES == 1
# if (EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1)
#  define LSS_RX_BUFFER 1
#  define LSS_TX_BUFFER 1
# else
#  define LSS_RX_BUFFER 0
#  define LSS_TX_BUFFER 0
# endif  /* (EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1) */
#else
# define LSS_RX_BUFFER 0
# define LSS_TX_BUFFER 0
#endif /* ENABLE_LAYER_SERVICES == 1 */

#if ACP_USED == 1
# define ACP_RX_BUFFER 1
#else
# define ACP_RX_BUFFER 0
#endif /* ACP_USED == 1 */

/* 
 * number of CAN buffers in tCanMsgBuffer
 * We have to add the following values:
 * BUFFER name      number
 * -----------------------
 *  NMT_ZERO_RX     1
 *  SYNC_RX         0/1
 *  PDO_RX          RXPDOS
 *  SDO_RX          1/n
 *  CLSDO_RX        0/1
 *  LSS_RX          0/1
 *  ACP_RX          0/1
 *  TIME_RX         0/1
 *  ERRCTRL_RX      0/n
 *  ERRCTRL_TX      0/1
 *  EMCY_TX         0/1
 *  SYNC_TX         0/1
 *  SDO_TX          1/n
 *  CLSDO_TX        0/1
 *  LSS_TX          0/1
 *  NMT_GUARD_TX    0/1
 *  NMT_ZERO_TX     0/1
 *  PDO_TX          TXPDOS
 * =======================
 *                  1
 *                + SDO_RX
 *                + SDO_TX
 *                + CLSDO_TX
 *                + CLSDO_RX
 *                + LSS_TX
 *                + LSS_RX
 *                + ACP_RX
 *                + TIME_STAMP
 *                + HEARTBEAT_CON
 *                + ERRCTRL_TX
 *                + GUARDING_RX
 *                + GUARDING_TX
 *                + NMT_MASTER
 *                + EMCY_TX
 *                + SYNC
 *                + RXPDOS
 *                + TXPDOS
 *               =========
 *                  x
 */
#define MAX_CAN_BUFFER  (1 \
                         + SRSDO_RX_BUFFER \
                         + SRSDO_TX_BUFFER \
                         + CLSDO_RX_BUFFER \
                         + CLSDO_TX_BUFFER \
                         + LSS_RX_BUFFER \
                         + ACP_RX_BUFFER \
                         + LSS_TX_BUFFER \
                         + EMCY_TX_BUFFER \
                         + SYNC_RX_BUFFER \
                         + SYNC_TX_BUFFER \
                         + TIME_STAMP_BUFFER \
                         + HEARTBEAT_CON_BUFFER \
                         + NMT_ERR_CTRL_BUFFER \
                         + GUARDING_RX_BUFFER \
                         + NMT_MASTER_BUFFER \
                         + GUARDING_TX_BUFFER \
                         + MAX_RX_PDOS \
                         + MAX_TX_PDOS)

/*
 * This shows the number of CAN messages to be
 * received on this module
 * We have to add the following values:
 * BUFFER name      number
 * -----------------------
 *  NMT_ZERO_RX     1
 *  SYNC_RX         0/1
 *  PDO_RX          RXPDOS
 *  SDO_RX          1/n
 *  CLSDO_RX        0/1
 *  LSS_RX          0/1
 *  ACP_RX          0/1
 *  TIME_RX         0/1
 *  ERRCTRL_RX      0/n
 * =======================
 *                  1
 *                + SDO_RX
 *                + CLSDO_RX
 *                + LSS_RX
 *                + ACP_RX
 *                + TIME_STAMP
 *                + HEARTBEAT_CON
 *                + GUARDING
 *                + SYNC
 *                + RXPDOS
 *               =========
 *                  x
 */
#define MAX_RX_MSG      (1 \
                         + SRSDO_RX_BUFFER \
                         + CLSDO_RX_BUFFER \
                         + LSS_RX_BUFFER \
                         + ACP_RX_BUFFER \
                         + SYNC_RX_BUFFER \
                         + TIME_STAMP_BUFFER \
                         + HEARTBEAT_CON_BUFFER \
                         + GUARDING_RX_BUFFER \
                         + MAX_RX_PDOS)

/*
 * The defines show the ordering of the buffers
 * in the array tCanMsgBuffer.
 */
#define NMT_ZERO_RX   0

#if SYNC_USED == 1
# define SYNC_RX      1
#endif /* SYNC_USED == 1 */

#define PDO_RX        (SYNC_RX_BUFFER + 1)
#define RMTPDO_RX     (PDO_RX + MAX_RX_PDOS - MAX_RX_RMTPDOS)
#define SDO_RX        (PDO_RX + MAX_RX_PDOS)

#if CLIENT_ENABLE == 1
# define CLSDO_RX     (PDO_RX + MAX_RX_PDOS + SRSDO_RX_BUFFER)
#endif /* CLIENT_ENABLE == 1 */

#if ENABLE_LAYER_SERVICES == 1
# if (EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1)
#  define LSS_RX     (PDO_RX + MAX_RX_PDOS + SRSDO_RX_BUFFER + CLSDO_RX_BUFFER)
# else
#  define LSS_RX     (SDO_RX)   /* use SDO-buffer if LSS executed once */
# endif /* EXEC_LAYER_SERVICES_ALWAYS == 1 */
#endif /* ENABLE_LAYER_SERVICES == 1 */

#if TIME_STAMP_USED == 1
# define TIME_RX      (PDO_RX + MAX_RX_PDOS + SRSDO_RX_BUFFER + CLSDO_RX_BUFFER \
                       + LSS_RX_BUFFER)
#endif /* TIME_STAMP_USED == 1 */

#if ACP_USED == 1
# define ACP_RX       (PDO_RX + MAX_RX_PDOS + SRSDO_RX_BUFFER + CLSDO_RX_BUFFER \
                       + LSS_RX_BUFFER + TIME_STAMP_BUFFER)
#endif /* TIME_STAMP_USED == 1 */

#define ERRCTRL_RX    (PDO_RX + MAX_RX_PDOS + SRSDO_RX_BUFFER + CLSDO_RX_BUFFER \
                       + LSS_RX_BUFFER + ACP_RX_BUFFER + TIME_STAMP_BUFFER)

#define ERRCTRL_TX    (PDO_RX + MAX_RX_PDOS + SRSDO_RX_BUFFER + CLSDO_RX_BUFFER \
                       + LSS_RX_BUFFER + ACP_RX_BUFFER + TIME_STAMP_BUFFER \
                       + HEARTBEAT_CON_BUFFER + GUARDING_RX_BUFFER)

#if ENABLE_EMCYMSG == 1
#define EMCY_TX       (ERRCTRL_TX + NMT_ERR_CTRL_BUFFER)
#else
#endif /* ENABLE_EMCYMSG == 1 */

#define SDO_TX        (ERRCTRL_TX + NMT_ERR_CTRL_BUFFER + EMCY_TX_BUFFER)

#if CLIENT_ENABLE == 1
# define CLSDO_TX     (SDO_TX + SRSDO_TX_BUFFER)
#endif /* CLIENT_ENABLE == 1 */

#if ENABLE_LAYER_SERVICES == 1
# if (EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1)
#  define LSS_TX     (SDO_TX + SRSDO_TX_BUFFER + CLSDO_TX_BUFFER)
# else
#  define LSS_TX     (SDO_TX)  /* use SDO-buffer if LSS executed once */
# endif  /* EXEC_LAYER_SERVICES_ALWAYS == 1 */
#endif /* ENABLE_LAYER_SERVICES == 1 */

#if NMT_MASTER_ENABLE == 1
# define NMT_ZERO_TX  (SDO_TX + SRSDO_TX_BUFFER + CLSDO_TX_BUFFER + LSS_TX_BUFFER)
#endif /* NMT_MASTER_ENABLE == 1 */

#if NMT_GUARDING_ENABLE == 1
# define NMT_GUARD_TX (SDO_TX + SRSDO_TX_BUFFER + CLSDO_TX_BUFFER + LSS_TX_BUFFER \
                       + NMT_MASTER_BUFFER)
#endif /* NMT_GUARDING_ENABLE == 1 */

#if SYNC_PRODUCER == 1
# define SYNC_TX      (SDO_TX + SRSDO_TX_BUFFER + CLSDO_TX_BUFFER + LSS_TX_BUFFER \
                       + NMT_MASTER_BUFFER + GUARDING_TX_BUFFER)
#endif /* SYNC_PRODUCER == 1 */

#define PDO_TX        (SDO_TX + SRSDO_TX_BUFFER + CLSDO_TX_BUFFER + LSS_TX_BUFFER \
                       + NMT_MASTER_BUFFER + GUARDING_TX_BUFFER + SYNC_TX_BUFFER)
#define RMTPDO_TX     (PDO_TX + MAX_TX_PDOS - MAX_TX_RMTPDOS)


/*--------------------------------------------------------------------*/
/*  #define statement validation section                              */
/*--------------------------------------------------------------------*/
#if MAX_CAN_BUFFER > 255
# error "Number of available CAN buffers exceeded !"
#endif /* MAX_CAN_BUFFER > 255 */


/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/

/*!
  \brief This structure describes a CAN message

  A single message could be completely described by the identifier and the
  data length code. Additionally it is necessary to know some more
  attributes.
*/
typedef struct {
  QBYTE qbId;           /*!< identifier                              */
  BYTE bDb[8];          /*!< data bytes [0..8]                       */
#ifdef EIGHT_BIT_MICRO
  BYTE Dlc: 4;  /*!< data length code [0..8] (4 bits)        */
  BYTE Rtr: 1;  /*!< 0 - data frame, 1 - remote frame        */
  BYTE Ext: 1;  /*!< 0 - standard frame, 1 - extended frame  */
  BYTE TxOk: 1; /*!< message transmitted successfully        */
  BYTE RxOk: 1; /*!< new message received                    */
#else
  unsigned int Dlc: 4;  /*!< data length code [0..8] (4 bits)        */
  unsigned int Rtr: 1;  /*!< 0 - data frame, 1 - remote frame        */
  unsigned int Ext: 1;  /*!< 0 - standard frame, 1 - extended frame  */
  unsigned int TxOk: 1; /*!< message transmitted successfully        */
  unsigned int RxOk: 1; /*!< new message received                    */
#endif /* EIGHT_BIT_MICRO */
} CAN_MSG;

/*!
  \brief Buffer structure for a single message.

  The code provides some static buffers for messages.
  This is necessary for TX pdos to determine a state change event
  for instance.
*/
typedef struct {
  CAN_MSG tMsg; /*!< message contents */
  BYTE bSta;    /*!< buffer state     */
#ifndef EIGHT_BIT_MICRO
  BYTE bAlign;  /*!< this byte provides alignment */
#endif /* EIGHT_BIT_MICRO */
} CAN_BUF;



/*--------------------------------------------------------------------*/
/*  macros                                                            */
/*--------------------------------------------------------------------*/
#define NUM_COM(base, num)  base ## num
#define GET_ID(BNr, Pos) (tCanMsgBuffer[BNr].tMsg.qbId.fb.NUM_COM(b, Pos))

#if QUEUED_MODE == 1
# define gCan_SendBuf(Buffer) gCB_QueSendMsg(&tCanMsgBuffer[Buffer].tMsg)
#else
# if FULLCAN_BUFFER_MODE == 1
#  define gCan_SendBuf(Buffer) gCan_SendMsg(Buffer)
# else
#  define gCan_SendBuf(Buffer) gCan_SendMsg(&tCanMsgBuffer[Buffer].tMsg)
# endif /* FULLCAN_BUFFER_MODE == 1 */
#endif /* QUEUED_MODE == 1 */



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void gCan_Init(void);
void gCan_CntrlInit(BYTE);
BYTE gCan_GetCanStatus(void);
BYTE gCan_PrepareRemoteMessage(CAN_MSG MEM_AREA *);
BYTE gCan_TxOccurred(void);
BYTE gCan_InstallWakeUp(void);
void gCan_WakeUpOccurred(void);

#if FULLCAN_BUFFER_MODE == 1
BYTE gCan_SendMsg(BYTE);
#else
BYTE gCan_SendMsg(CAN_MSG MEM_AREA *);
#endif /* FULLCAN_BUFFER_MODE == 1 */

#endif /* _CANCNTRL_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief CAN controller functions header.
  \par File name cancntrl.h
  \version 18
  \date 9.05.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

