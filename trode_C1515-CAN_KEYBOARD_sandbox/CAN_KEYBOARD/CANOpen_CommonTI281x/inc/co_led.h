/*-----------------------------------------------------------------------------
       CO_LED.H
  -----------------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for CANopen Indicator Specification functionality
                 (CiA DR-303-3 V1.0)
  ---------------------------------------------------------------------------*/


/*
START CONFIGURATOR CONTROL UNIT

Macro Name               | Macro Group      |         | type    | limits
-------------------------------------------------------------------------------
NEGATIVE_LED_LOGIC       | LED Indicator    | BOOL    |   BOOL  |    | 
LED_FLICKER_MODE_ENABLED | LED Indicator    | BOOL    |   BOOL  |    | 

END CONFIGURATOR CONTROL UNIT
*/


#ifndef _CO_LED_H_
#define _CO_LED_H_


/*-------------------------------------------------------------------*/
/*  user adaptable section - LED hardware dependencies               */
/*-------------------------------------------------------------------*/
/* The physical ports where the LEDs are connected to 
   have to be defined here
 */

/*********************************************************************/
/* THESE SETTINGS ARE VALID FOR THE PC VERSION ONLY !!!                    */
/*********************************************************************/
#ifdef WIN32
# if USE_CANOE_EMU == 1
#  define RUN_LED_PORT    "LED_11"    /*!< port number of RUN_LED    */
#  define RUN_LED_BIT     1           /*!< bit position of RUN_LED   */
#  define ERROR_LED_PORT  "LED_12"    /*!< port number of ERROR_LED  */
#  define ERROR_LED_BIT   2           /*!< bit position of ERROR_LED */
#  define DPR_ERROR_LED   Dummy_Dir   /*!< direction port register   */
#  define DPR_RUN_LED     Dummy_Dir   /*!< direction port register   */
# else
#  define RUN_LED_PORT    Dummy_Port  /*!< port number of RUN_LED    */
#  define RUN_LED_BIT     1           /*!< bit position of RUN_LED   */
#  define ERROR_LED_PORT  Dummy_Port  /*!< port number of ERROR_LED  */
#  define ERROR_LED_BIT   2           /*!< bit position of ERROR_LED */
#  define DPR_ERROR_LED   Dummy_Dir   /*!< direction port register   */
#  define DPR_RUN_LED     Dummy_Dir   /*!< direction port register   */
# endif /* USE_CANOE_EMU == 1 */
#else
/*********************************************************************/
/* THESE SETTINGS ARE VALID FOR THE TARGET ADAPTATION !!!                  */
/*********************************************************************/
/* include target specific header here */
/* EXAMPLE: adaptation for Vector CANister
# include "reg167.h"
*/

/* EXAMPLE configuration: adaptation for Vector CANister */
# define RUN_LED_PORT    P3           /*!< port number of RUN_LED    */
# define RUN_LED_BIT     5            /*!< bit position of RUN_LED   */
# define ERROR_LED_PORT  P3           /*!< port number of ERROR_LED  */
# define ERROR_LED_BIT   8            /*!< bit position of ERROR_LED */
# define DPR_ERROR_LED   DP3          /*!< direction port register   */
# define DPR_RUN_LED     DP3          /*!< direction port register   */
#endif /* WIN32 */

/*!
  \def NEGATIVE_LED_LOGIC
  Definition of the physical LED control.

  In case of positive logic the LED will be turned on if the corresponding
  port-bit is set and turned off if it is reset.
  Negative logic is the same vice versa.

 - 0 Use positive logic.
 - 1 Use negative logic.
*/
#define NEGATIVE_LED_LOGIC 1

/*!
  \def LED_FLICKER_MODE_ENABLED
  Enabling of the additional LED Flickering Mode.

  The CiA DR-303-3 specifies an (optional) "LED flickering" mode for the 
  Autobaud/LSS states. The availability of this mode is configured here.
  To enter/leave the flickering mode the function gLED_SetFlickerMode is used.

 - 0 Flickering Mode not available.
 - 1 Flickering Mode available.
*/
#define LED_FLICKER_MODE_ENABLED 0


/*--------------------------------------------------------------------*/
/*  constants definitions                                             */
/*--------------------------------------------------------------------*/

/* indices for LED modes */
#define LED_OFF          0
#define LED_FLASH1       1
#define LED_FLASH2       2
#define LED_FLASH3       3
#define LED_ON           4
#define LED_BLINK        5

#define NUM_LED_MODES    6  /*!< number of LED signaling modes */

/* priorities for ERROR_LED modes */
#define LED_OFF_PRIO     (0x01 << LED_OFF)
#define LED_FLASH1_PRIO  (0x01 << LED_FLASH1)
#define LED_FLASH2_PRIO  (0x01 << LED_FLASH2)
#define LED_FLASH3_PRIO  (0x01 << LED_FLASH3)
#define LED_ON_PRIO      (0x01 << LED_ON)
#define LED_BLINK_PRIO   (0x01 << LED_BLINK)

#define ERRLED_STATE_HIGH LED_ON_PRIO /*!< highest prior mode for ERROR_LED */

/* number of intervals in one flash-sequence for the different modes */
#define LED_OFF_FSEQ     0
#define LED_FLASH1_FSEQ  1 /* 1 ON phase */
#define LED_FLASH2_FSEQ  3 /* 2 ON and 1 OFF phase  */
#define LED_FLASH3_FSEQ  5 /* 3 ON and 2 OFF phases */
#define LED_ON_FSEQ      0
#define LED_BLINK_FSEQ   0

/* indices for LEDs */
#define ERROR_LED        0 /*!< index of ERROR LED */
#define RUN_LED          1 /*!< index of RUN LED   */
#define NUM_LEDS         2 /*!< number of LEDs     */

/* LED timing values */
#define BLINK_TIME       1    /*!< isophase time for blinking-mode in multiples
                                   of LED_TIMER_INT(200ms): 2.5 Hz */
#define OFF_PHASE        5    /*!< off phase time in multiples of LED_TIMER_INT
                                   (200ms): 1000ms */
#define LED_TIMER_INT    200  /*!< LED timer interval in ms: the timer function 
                                   is called evry 200ms */
#define FLICKER_TIME_INT 50   /*!< isophase time for flickering-mode in ms: 10Hz */

/* resulting LED states for ERROR_LED according to CiA DR-303-3 */
#define LED_NOERR        ERROR_LED, LED_OFF     /*!< no error              */
#define LED_WARNING      ERROR_LED, LED_FLASH1  /*!< warning limit reached */
#define LED_ERRCTRL      ERROR_LED, LED_FLASH2  /*!< error control event   */
#define LED_SYNC         ERROR_LED, LED_FLASH3  /*!< sync error            */
#define LED_BOFF         ERROR_LED, LED_ON      /*!< CAN is bus off        */

/* resulting LED states for RUN_LED according to CiA DR-303-3 */
#define LED_RESET        RUN_LED, LED_OFF       /*!< reset/init phase      */
#define LED_STOPPED      RUN_LED, LED_FLASH1    /*!< STOPPED state         */
#define LED_PREOP        RUN_LED, LED_BLINK     /*!< PRE-OPERATIONAL state */
#define LED_OPERATIONAL  RUN_LED, LED_ON        /*!< OPERATIONAL state     */


/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/
/*!
  LED attributes.
  For every LED we need some managing values.
*/
typedef struct {
  BYTE bToggleTimer;    /*!< toggle timer               */
  BYTE bToggleReload;   /*!< toggle timer reload value  */
  BYTE bFlashCounter;   /*!< flash sequence counter     */
  BYTE bFlashReload;    /*!< flash counter reload value */
  BYTE bLedOn;          /*!< LED turned on              */
  BYTE bLedMode;        /*!< actual LED signaling mode  */
  BYTE bOffPhase;       /*!< LED in OFF_PHASE           */
  BYTE bRun;            /*!< toggle timer running       */
}
LED_PARAMETER;


/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void gLED_Init(void);
void gLED_SetLedState(BYTE, BYTE, BYTE);
void gLED_SetFlickerMode(BYTE);
void gLED_ChkToggleTime(void);

#endif /* _CO_LED_H_ */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for CANopen LED functionality.
  \par File name co_led.h
  \version 7
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

