/*--------------------------------------------------------------------
       VASSERT.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Assert macro definitions
  --------------------------------------------------------------------*/

#ifdef assert
# undef assert
#endif



/*--------------------------------------------------------------------*/
/*  include files, macros                                             */
/*--------------------------------------------------------------------*/
#ifdef WIN32
# include <assert.h>
#else

# ifdef COSMIC12
#  include <assert.h>
# else

# ifdef _KL_C166
#  include <intrins.h>
#  define abort() _trap_(0)
# endif /* _KL_C166 */

# ifdef __C51__
#  define abort() (((void (code *) (void)) 0) ())
# endif /* __C51__ */


# ifndef _VASSERT_H
#  define _VASSERT_H

#  ifndef NDEBUG
#   include <stdio.h>   /* prototype for 'printf' */
#   include <stdlib.h>  /* prototype for 'abort'  */

#   define assert(_expr)                                                \
      do {                                                              \
        if (!(_expr)) {                                                 \
          printf( "Assertion failed: (" #_expr ") file %s, line %d\n",  \
                  __FILE__, __LINE__ );                                 \
          abort();                                                      \
        }                                                               \
      } while (0)

#  else
#   define assert(_expr) ((void)0)
#  endif /* NDEBUG */
# endif /* _VASSERT_H */
# endif /* COSMIC12 */
#endif /* WIN32 */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief This file defines the assert macros to be used during projects.
  \par File name vassert.h
  \version 6
  \date 6.03.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

