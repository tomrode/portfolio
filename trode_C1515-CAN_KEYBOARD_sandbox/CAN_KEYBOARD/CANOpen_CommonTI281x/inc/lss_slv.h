/*--------------------------------------------------------------------
       LSS_SLV.H
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Header for LSS slave services
  --------------------------------------------------------------------*/

#ifndef _LSS_SLV_H_
#define _LSS_SLV_H_



/*--------------------------------------------------------------------*/
/*  global definitions                                                */
/*--------------------------------------------------------------------*/

/* Default IDs as defined in the CANopen LSS DSP 305 specification. */
#define LSS_SLAVE_ID    2020    /*!< COB ID 2020 LSS service        */
#define LSS_MASTER_ID   2021    /*!< COB ID 2021 LSS service        */

/*!
  \defgroup lss_cs LSS command specifier
  @{
*/

#define LSS_CS_SWITCH_MODE_GL       4   /*!< LSS switch mode global */
#define LSS_CS_CONFIG_NODE_ID       17  /*!< LSS configure Node-ID */
#define LSS_CS_CONFIG_BIT_PAR       19  /*!< LSS configure bit timing */
#define LSS_CS_ACTIV_BIT_PAR        21  /*!< LSS activate bit timing */
#define LSS_CS_STORE_CONFIG         23  /*!< LSS store configuration */
#define LSS_CS_SWITCH_VENDOR        64  /*!< LSS switch mode selective vendor-id */
#define LSS_CS_SWITCH_PRODUCT       65  /*!< LSS switch mode selective product-code */
#define LSS_CS_SWITCH_REVISION      66  /*!< LSS switch mode selective revision-number */
#define LSS_CS_SWITCH_SERIAL        67  /*!< LSS switch mode selective serial-number */
#define LSS_CS_SWITCH_RESPONSE      68  /*!< LSS switch mode selective response */
#define LSS_CS_IDENT_VENDOR         70  /*!< LSS identify remote slave vendor-id */
#define LSS_CS_IDENT_PRODUCT        71  /*!< LSS identify remote slave product-code */
#define LSS_CS_IDENT_REV_LOW        72  /*!< LSS identify remote slave revision-number low */
#define LSS_CS_IDENT_REV_HIGH       73  /*!< LSS identify remote slave revision-number high */
#define LSS_CS_IDENT_SER_LOW        74  /*!< LSS identify remote slave serial-number low */
#define LSS_CS_IDENT_SER_HIGH       75  /*!< LSS identify remote slave serial-number high */
#define LSS_CS_IDENT_NO_CONF_MST    76  /*!< LSS identify non-configured remote slave */
#define LSS_CS_IDENT_NO_CONF_SLV    80  /*!< LSS identify non-configured slave */
#define LSS_CS_IDENT_SLAVE          79  /*!< LSS identify slave */
#define LSS_CS_INQ_ID_VENDOR        90  /*!< LSS inquire identity vendor-id */
#define LSS_CS_INQ_ID_PRODUCT       91  /*!< LSS inquire identity product-code */
#define LSS_CS_INQ_ID_REVISION      92  /*!< LSS inquire identity revision-number */
#define LSS_CS_INQ_ID_SERIAL        93  /*!< LSS inquire identity serial-number */
#define LSS_CS_INQ_ID_NODE          94  /*!< LSS inquire identity node-id */

/*!
  @} end of group lss_cs
*/

/* LSS definitions */
#define	LSS_INVALID_NODE_ID		      0xFF /*!< LSS invalid node_ID */


/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/

/*! structure of the LSS address */
typedef struct {
  QBYTE lVendorID;          /*!< vendor-id       */
  QBYTE lProductCode;       /*!< product-code    */
  QBYTE lRevisionNumber;    /*!< revision-number */
  QBYTE lSerialNumber;      /*!< serial-number   */
}
tLssAddress;



/*--------------------------------------------------------------------*/
/*  function prototypes                                               */
/*--------------------------------------------------------------------*/
void gLssActivateTimer(WORD);
void gLssActivateBitTiming(void);
void gLayerServices(void);
void gHandleLssMsg(void);
void gLssTimer(WORD);
void gInitLss(void);
BYTE gLssStateMachine(void);


#endif /* _LSS_SLV_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Header for LSS slave services.
  \par File name lss_slv.h
  \version 4
  \date 6.03.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

