/*--------------------------------------------------------------------
       CPDRVW32.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: CAN driver for the access of the Vector ProCANopen
                 PC based CAN driver interface
  --------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#define STRICT
#include <windows.h>
#include "portab.h"
#include "cos_main.h"
#include "vdbgprnt.h"
#include "cpdvspec.h"
#include "target.h"
#include <stdio.h>
#include "cancntrl.h"
#include "buffer.h"



/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern vModInfo XDATA ModuleInfo; /* module information */
extern BOOLEAN DATA fEmcyOverrun; /* EMCY overrun flag */



/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/
STATIC BYTE bFirstMsgTrans; /*!< after first transmission this is set to TRUE */
STATIC BYTE bCanStatus;     /*!< current CAN controller status */
/*!
  We must ensure that our ISR is only executed after the init routine
  was called. Otherwise it is possible that we find not initalised structures.
  This will crash the stack. Imagine a reset of the microcontroller but NOT
  of the CAN controller.
*/
STATIC BYTE bISRInitiated = FALSE;

DWORD CONST tCCanBtr[9] = {
   { 10,  }, /*  10K bit/s           */
   { 20,  }, /*  20K bit/s           */
   { 50,  }, /*  50K bit/s           */
   { 100, }, /* 100K bit/s           */
   { 125, }, /* 125K bit/s           */
   { 250, }, /* 250K bit/s           */
   { 500, }, /* 500K bit/s           */
   { 800, }, /* 800K bit/s           */
   { 1000 }  /*   1M bit/s           */
};

DWORD hCanHnd = 0l;
int sBusOff;
int sErrorPassive;
int sIsRunning = 0;
int sIsInitialized = 0;



/*--------------------------------------------------------------------*/
/*  library specific pointer                                          */
/*--------------------------------------------------------------------*/
HINSTANCE             hLibrary               = 0;
cpBoardInit           gCpBoardInit           = 0;
cpDriverType          gCpDriverType          = 0;
cpDriverName          gCpDriverName          = 0;
cpGetVersion          gCpGetVersion          = 0;
cpShutDown            gCpShutDown            = 0;
cpLock                gCpLock                = 0;
cpUnlock              gCpUnlock              = 0;
cpSetAcceptanceFilter gCpSetAcceptanceFilter = 0;
cpMeasureInit         gCpMeasureInit         = 0;
cpMeasureStart        gCpMeasureStart        = 0;
cpMeasureStop         gCpMeasureStop         = 0;
cpSendMessage         gCpSendMessage         = 0;
cpGetMessage          gCpGetMessage          = 0;
cpError               gCpError               = 0;
cpGetIniSectionName   gCpGetIniSectionName   = 0;



/*--------------------------------------------------------------------*/
/*  public functions                                                  */
/*--------------------------------------------------------------------*/

/*! Release the hardware. */
void gCan_FreeHardware(void)
{
  if (hLibrary) { /* only if really loaded */
    gCpShutDown();
    FreeLibrary(hLibrary);
    hLibrary = 0;
  } /* if */
  sIsInitialized = 0;
  sIsRunning = 0;
}


/*! Release the library. */
int gCan_Free(void)
{
  /* make sure that library was found */
  if (hLibrary) {
    if (hCanHnd) gCpMeasureStop(hCanHnd);
    if (gCpUnlock) gCpUnlock(hCanHnd);
  } /* if */

  hCanHnd = 0;
  return 1;
}


/*! Initialize the CAN management structures and load the library. */
void gCan_Init(void)
{
  int res = 0;

  sBusOff = 0;
  sErrorPassive = 0;
  bFirstMsgTrans = FALSE;  /* no transmission up to now */
  bCanStatus = CAN_STS_NORMAL; /* reset status information */

  gCan_Free();

  gCan_FreeHardware();

  hLibrary = LoadLibrary("CPDRV32.DLL");
  if (!hLibrary) return;
  gCpBoardInit = (cpBoardInit)GetProcAddress(hLibrary,"cpBoardInit");
  if (!gCpBoardInit) return;
  gCpDriverType = (cpDriverType)GetProcAddress(hLibrary,"cpDriverType");
  if (!gCpDriverType) return;
  gCpDriverName = (cpDriverName)GetProcAddress(hLibrary,"cpDriverName");
  if (!gCpDriverName) return;
  gCpGetVersion = (cpGetVersion)GetProcAddress(hLibrary,"cpGetVersion");
  if (!gCpGetVersion) return;
  gCpShutDown = (cpShutDown)GetProcAddress(hLibrary,"cpShutDown");
  if (!gCpShutDown) return;
  gCpLock = (cpLock)GetProcAddress(hLibrary,"cpLock");
  if (!gCpLock) return;
  gCpUnlock = (cpUnlock)GetProcAddress(hLibrary,"cpUnlock");
  if (!gCpUnlock) return;
  gCpSetAcceptanceFilter = (cpSetAcceptanceFilter)GetProcAddress(hLibrary,"cpSetAcceptanceFilter");
  if (!gCpSetAcceptanceFilter) return;
  gCpMeasureInit = (cpMeasureInit)GetProcAddress(hLibrary,"cpMeasureInit");
  if (!gCpMeasureInit) return;
  gCpMeasureStart = (cpMeasureStart)GetProcAddress(hLibrary,"cpMeasureStart");
  if (!gCpMeasureStart) return;
  gCpMeasureStop = (cpMeasureStop)GetProcAddress(hLibrary,"cpMeasureStop");
  if (!gCpMeasureStop) return;
  gCpSendMessage = (cpSendMessage)GetProcAddress(hLibrary,"cpSendMessage");
  if (!gCpSendMessage) return;
  gCpGetMessage = (cpGetMessage)GetProcAddress(hLibrary,"cpGetMessage");
  if (!gCpGetMessage) return;
  gCpError = (cpError)GetProcAddress(hLibrary,"cpError");
  if (!gCpError) return;
  gCpGetIniSectionName = (cpGetIniSectionName)GetProcAddress(hLibrary,"cpGetIniSectionName");
  if (!gCpGetIniSectionName) return;

  if (hLibrary) {
    res = gCpBoardInit();
  } /* if */
    
  sIsInitialized = res != 0;
}


/*! Get last error. */
int gCan_LastError(void)
{
  if (hLibrary) {
    return gCpError(hCanHnd);
  } /* if */
  else {
    return 0;
  } /* else */
}


/*! Allocate CAN driver. */
int gCan_Allocate(void)
{
  if (hCanHnd) return 1;
  hCanHnd = gCpLock();
  return hCanHnd != 0;
}


/*!
  Set new mode of operation.

  \param bNewMode - new mode of operation

  \retval TRUE - new mode established
  \retval FALSE - new mode refused
*/
BYTE gCan_ActivateMode(BYTE bNewMode)
{
  BYTE bChangeOk = TRUE;

  if (!hCanHnd) return FALSE;
  if (!hLibrary) return FALSE;
  if (!sIsInitialized) return FALSE;

  switch (bNewMode) {
    case MODE_CONFIG:
      sIsRunning = 0;
      /* test if module responds with NOTRDY bit */
      if (gCpMeasureStop(hCanHnd)) {
        bChangeOk = FALSE; /* not switched to config mode */
      } /* if */
      else {
        bCanStatus |= CAN_STS_RESET;
      } /* else */
      break;

    case MODE_NORMAL:
      sBusOff = 0;
      sErrorPassive = 0;
      if(gCpMeasureStart(hCanHnd)) {
        sIsRunning = 1;
        bCanStatus &= ~CAN_STS_RESET;
      } /* if */
      else {
        sIsRunning = 0;
        bChangeOk = FALSE; /* not switched to normal mode */
      } /* else */
      break;

    case MODE_SLEEP:
      bChangeOk = FALSE; /* not switched to normal mode */
      break;

    case MODE_LOOPBK:
      bChangeOk = FALSE; /* not switched to loopback mode */
      break;

    case MODE_LISTEN:
      bChangeOk = FALSE; /* not switched to loopback mode */
      break;

    default:
      bChangeOk = FALSE; /* unknown mode */
      break;
  }
  return bChangeOk;
}


/*!
  Initialize the CAN driver.

  \param bIndex selects the bit timing
*/
void gCan_CntrlInit(BYTE bIndex)
{
  /* the CPDRV interface has only one channel! */
  char buf[30];

  if (!hLibrary) return;

  gCan_ActivateMode(MODE_CONFIG);

  gCan_Free();

  /* set the baudrate via file interface */
  sprintf(buf,"%u",(DWORD)tCCanBtr[bIndex]);
  WritePrivateProfileString(gCpGetIniSectionName(), "Baudrate",
                            (LPCTSTR)buf, "CANDRV.INI");

  gCan_Allocate();

  /* initialize measurement */
  if (gCpMeasureInit(hCanHnd)) VDEBUG_RPT0("CAN initialized\n");
  if (gCan_ActivateMode(MODE_NORMAL)) VDEBUG_RPT0("CAN started\n");
}


/*!
  Put a CAN message on the bus.
  \param ptMsg pointer to buffer
  \return NO_ERR transmission in progress,
          BUSY_ERR no transmission cause of other transmission in progress
*/
BYTE gCan_SendMsg(CAN_MSG MEM_AREA *tSendBuffer)
{
  BYTE i;                           /* counter                          */
  VCpMessage LocMsg;
  BYTE bRet = NO_ERR;
  VCpError vError;

  if (!hLibrary) return BUSY_ERR;
  if (!hCanHnd) return BUSY_ERR;
  if (!sIsInitialized) return BUSY_ERR;
  if (!sIsRunning) return BUSY_ERR;

  if (tSendBuffer) { /* handle the buffer */
    // convert message into CPCANDRV structure
    LocMsg.id  = (WORD)tSendBuffer->qbId.dw;
    LocMsg.rtr = (BYTE)tSendBuffer->Rtr;
    LocMsg.dlc = (BYTE)tSendBuffer->Dlc;
    for (i = 0; i < 8 ; i++) {
      LocMsg.data[i] = tSendBuffer->bDb[i];
    } /* for */
    /* try to send message - if CAN locked copy to tx queue */
    if (!gCpSendMessage(hCanHnd, &LocMsg)) {
      /* write message into transmit queue */
      bRet = BUSY_ERR;              /* OUT: no free queue entry         */
      // check for error codes
      vError = gCpError(hCanHnd);
    } /* if */
    else {
      bFirstMsgTrans = TRUE;
    } /* else */
  } /* if */
  return bRet;
}


/*!
  \brief CAN ISR.

  The CAN interrupt service routine.
*/
void gCan_IntHandler(void)
{
  BYTE i;
  CAN_MSG tCanMsgBuffer;     /* standard buffer format */
  VCpMessage vCanDrvBuffer;  /* buffer in CANDRV format */
  VCpError vError;
  BYTE bErrEvent = FALSE;
  BYTE atLeastOne = FALSE;

  if (!hLibrary) return;
  if (!hCanHnd) return;
  if (!sIsInitialized) return;
  if (!sIsRunning) return;

  /* new message available ? */
  while (gCpGetMessage(hCanHnd, &vCanDrvBuffer)) {
    /* convert the message and put it into the appropriate queue */
    tCanMsgBuffer.qbId.dw = vCanDrvBuffer.id & ~(1 << 31);
    tCanMsgBuffer.Rtr  = vCanDrvBuffer.rtr;
    tCanMsgBuffer.Dlc  = vCanDrvBuffer.dlc;
    tCanMsgBuffer.Ext  = (vCanDrvBuffer.id & (1 << 31)) ? 1 : 0;

    /* check DLC */
    if (tCanMsgBuffer.Dlc > 8) {
      tCanMsgBuffer.Dlc = 8;
    } /* if */

    for (i = 0; i < tCanMsgBuffer.Dlc; i++) {
      tCanMsgBuffer.bDb[i] = vCanDrvBuffer.data[i];
    } /* for */

    sErrorPassive = 0;
    sBusOff = 0;
    atLeastOne = TRUE;

#if ID_FILTER != 0
    /* check identifier */
    if (gCB_PreCheckIdStd((WORD)tCanMsgBuffer.qbId.dw) == FALSE) {
      continue;
    } /* if */
#endif /* ID_FILTER */

    /* put message to storage location */
    if (!gCB_CanBufferMsg(&tCanMsgBuffer)) {
      break;
    } /* if */
  } /* while */

  /* check for error codes */
  vError = gCpError(hCanHnd);
  if (vError != ERR_RX_QUEUE_EMPTY && vError != ERR_TX_QUEUE_FULL) {
    bErrEvent = TRUE;
  } /* if */

  if (bErrEvent) { /* put L2 error to fifo */
    switch (vError) {
      case ERR_ERROR_PASSIVE:
        bCanStatus |= CAN_STS_WARNING;
        sErrorPassive = 1;
        break;

      case ERR_BUSOFF:
        bCanStatus |= CAN_STS_BUS_OFF;
        sBusOff = 1;
        sErrorPassive = 0;
        break;

      case ERR_ERROR_ACTIVE:
        bCanStatus &= ~(CAN_STS_WARNING | CAN_STS_BUS_OFF);
        sBusOff = 0;
        sErrorPassive = 0;
        break;

      case ERR_RX_REG_OVR:
      case ERR_RX_QUEUE_OVR:
        /* set EMCY overrun flag */
        fEmcyOverrun = TRUE;
        break;
    } /* switch */
    atLeastOne = TRUE;
  } /* if */

  /* signal empty TX buffer */
  gCB_SignalTx();
}


/*!
  \brief Enter sleep mode.

  With this call we go to sleep. The stack is suspended and no
  longer executed.
  \retval TRUE - sleep mode established
  \retval FALSE - sleep mode refused
*/
BYTE gCan_InstallWakeUp(void) 
{
  /* initiate sleeping modus */
  if (gCan_ActivateMode(MODE_SLEEP) == TRUE) {
    ModuleInfo.bModuleState = STACK_FREEZED;
    return 1;
  } /* if */
  else {
    return 0;
  } /* else */
}


/*!
  \brief Wake up from sleep mode.

  This call controls the recover from sleep mode.
*/
void gCan_WakeUpOccurred(void) 
{
  /* clear CAN identifier filter */
  gCB_ClearIdFilter(CAN_NO_MANAGER);
  /* initiate CAN controller */
  gCan_CntrlInit(ModuleInfo.bBaudRate);
}


/*!
  Prepare a CAN buffer to handle a given remote message.
  \param ptMsg pointer to buffer
  \return NO_ERR buffer established
*/
BYTE gCan_PrepareRemoteMessage(CAN_MSG MEM_AREA *ptMsg)
{
  if (ptMsg == 0) {
    return NO_ERR;
  } /* if */

  return NO_ERR;
}

/*!
  Read the current status information of the CAN controller.
  The variable \e bCanStatus is local to the CAN module. Via this call
  we have an encapsulated access to this information.
  \return
  \link #CAN_STS_NORMAL CAN_STS_NORMAL \endlink
  or
  \link #CAN_STS_RESET CAN_STS_RESET \endlink
  | \link #CAN_STS_WARNING CAN_STS_WARNING \endlink
  | \link #CAN_STS_BUS_OFF CAN_STS_BUS_OFF \endlink
  | \link #CAN_STS_SLEEPING CAN_STS_SLEEPING \endlink
  | \link #CAN_STS_OVERRUN CAN_STS_OVERRUN \endlink
  | \link #CAN_STS_TX_OK CAN_STS_TX_OK \endlink
*/
BYTE gCan_GetCanStatus(void)
{
  return bCanStatus;
}


/*!
  This function signals that the first message (boot-up message) is transmitted
  successfully.
  \retval TRUE - First message successfully transmitted.
  \retval FALSE - First message not yet transmitted.
*/
BYTE gCan_TxOccurred(void) 
{
  return bFirstMsgTrans;
}



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief CAN driver for the Vector ProCANopen interface.
  \par File name cpdrvw32.c
  \version 6
  \date 23.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

/*!
  \var tCCanBtr
  \brief CAN controller bit timing values.

  The bit timing values are defined in ascending order.
  - 10K bit/s
  - 20K bit/s
  - 50K bit/s
  - 100K bit/s
  - 125K bit/s
  - 250K bit/s
  - 500K bit/s
  - 800K bit/s
  - 1M bit/s
*/
