/*--------------------------------------------------------------------
       ECAN28.C
  --------------------------------------------------------------------
       Copyright (c) 1998-2005 Vector Informatik GmbH, Stuttgart

       Function: CAN driver for the TI TMS320F28x eCAN
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/

#include "portab.h"
#include "cos_main.h"
#include "target.h"
#include "cancntrl.h"
#include "buffer.h"


/*--------------------------------------------------------------------*/
/*  local definitions                                                 */
/*--------------------------------------------------------------------*/

/*! CAN controller memory address */
#define CAN_BASIC_ADR   0x6000

/*! use workaround for TI errata SPRZ193H */
#define SPRZ193H        1

/* CAN register control segment */
#define CAN_ME          0x00
#define CAN_MD          0x02
#define CAN_TRS         0x04
#define CAN_TRR         0x06
#define CAN_TA          0x08
#define CAN_AA          0x0A
#define CAN_RMP         0x0C
#define CAN_RML         0x0E
#define CAN_RFP         0x10
#define CAN_MC          0x14
#define CAN_BTC         0x16
#define CAN_ES          0x18
#define CAN_TEC         0x1A
#define CAN_REC         0x1C
#define CAN_GIF0        0x1E
#define CAN_GIM         0x20
#define CAN_GIF1        0x22
#define CAN_MIM         0x24
#define CAN_MIL         0x26
#define CAN_OPC         0x28
#define CAN_TIOC        0x2A
#define CAN_RIOC        0x2C
#define CAN_LNT         0x2E
#define CAN_TOC         0x30
#define CAN_TOS         0x32

/* CAN message buffer organisation */
#define CAN_MID         0x00
#define CAN_MCF         0x02
#define CAN_MDL         0x04
#define CAN_MDH         0x06
#define CAN_MSG_OFFSET  0x08 /*!< message object distance */
#define CAN_LAM         0x40
#define CAN_MOTS        0x80
#define CAN_MOTO        0xC0

/* CAN message buffer location */
#define CAN_TX_BUF      0x0100
#define CAN_RTR_BUF     0x0108
#define CAN_RX_BUF_L    0x0110
#define CAN_RX_BUF_F    0x01F8

/* CAN controller flags */
#define CAN_FLAG_SUSP   0x00010000UL
#define CAN_FLAG_MBCC   0x00008000UL
#define CAN_FLAG_TCC    0x00004000UL
#define CAN_FLAG_SCB    0x00002000UL
#define CAN_FLAG_CCR    0x00001000UL
#define CAN_FLAG_PDR    0x00000800UL
#define CAN_FLAG_DBO    0x00000400UL
#define CAN_FLAG_WUBA   0x00000200UL
#define CAN_FLAG_CDR    0x00000100UL
#define CAN_FLAG_ABO    0x00000080UL
#define CAN_FLAG_STM    0x00000040UL
#define CAN_FLAG_SRES   0x00000020UL

#define CAN_FLAG_FE     0x01000000UL
#define CAN_FLAG_BE     0x00800000UL
#define CAN_FLAG_SA1    0x00400000UL
#define CAN_FLAG_CRCE   0x00200000UL
#define CAN_FLAG_SE     0x00100000UL
#define CAN_FLAG_ACKE   0x00080000UL
#define CAN_FLAG_BO     0x00040000UL
#define CAN_FLAG_EP     0x00020000UL
#define CAN_FLAG_EW     0x00010000UL
#define CAN_FLAG_SMA    0x00000020UL
#define CAN_FLAG_CCE    0x00000010UL
#define CAN_FLAG_PDA    0x00000008UL
#define CAN_FLAG_RM     0x00000002UL
#define CAN_FLAG_TM     0x00000001UL

#define CAN_FLAG_IDE    0x80000000UL
#define CAN_FLAG_AME    0x40000000UL
#define CAN_FLAG_AAM    0x20000000UL

#define CAN_FLAG_BOFF   0x00000400UL
#define CAN_FLAG_PASV   0x00000200UL
#define CAN_FLAG_WARN   0x00000100UL

#define PIEACK          *((volatile int *)0x0CE1)

#if 0
/* values defined for 150 Mhz Vector Values*/
DWORD CONST tCCanBtr[9] = {
  0x00C70052, /*  10K bit/s, do not use */
  0x00C70052, /*  20K bit/s, do not use */
  0x00C70052, /*  50K bit/s */
  0x00630052, /* 100K bit/s */
  0x004F0052, /* 125K bit/s */
  0x00270052, /* 250K bit/s */
  0x00130052, /* 500K bit/s */
  0x000A0062, /* 800K bit/s */
  0x00090052  /*   1M bit/s */
};
#endif

/* values defined for 150 Mhz Crown Values */
DWORD CONST tCCanBtr[9] = {
  0x0095016C, /*  50K bit/s */
  0x0048016C, /* 100K bit/s */
  0x003B016C, /* 125K bit/s */
  0x0024016C, /* 200K bit/s */
  0x001D016C, /* 250K bit/s */
  0x00150162, /* 400K bit/s */
  0x000E016C, /* 500K bit/s */
  0x000A0062, /* 800K bit/s */
  0x0005017F, /*   1M bit/s */
};

#if 0
/* values defined for 120 Mhz Crown Values */
DWORD CONST tCCanBtr[9] = {
  0x00950153, /*  50K bit/s */
  0x003B016C, /* 100K bit/s */
  0x003B0153, /* 125K bit/s */
  0x001D016C, /* 200K bit/s */
  0x001D0153, /* 250K bit/s */
  0x000E016C, /* 400K bit/s */
  0x000E0153, /* 500K bit/s */
  0x0005017F, /* 800K bit/s */
  0x0005016C, /*   1M bit/s */
};

#endif

/*--------------------------------------------------------------------*/
/*  macros                                                            */
/*--------------------------------------------------------------------*/

#if SPRZ193H == 0
/* read one word from memory */
#define ReadWord(adr)               ((WORD)(*(WORD volatile *)(adr)))
/* write one word to memory */
#define WriteWord(dst, src)         (*(WORD volatile *)(dst)=((WORD)src))
/* read one double word from memory */
#define ReadDword(adr)              ((DWORD)(*(DWORD volatile *)(adr)))
/* write one double word to memory */
#define WriteDword(dst, src)        (*(DWORD volatile *)(dst)=((DWORD)src))
/* set bit at memory location */
#define SetBitValueW(adr, val)      ((*(WORD volatile *)(adr)) |= (WORD)(val))
/* set bit at memory location */
#define SetBitValueD(adr, val)      {\
                                      register DWORD lIoData;\
                                      lIoData = *(DWORD volatile *)(adr);\
                                      lIoData |= (DWORD)(val);\
                                      *(DWORD volatile *)(adr) = lIoData;\
                                    }
/* reset bit at memory location */
#define ResetBitValueW(adr, val)    ((*(WORD volatile *)(adr)) &= ~((WORD)(val)))
/* reset bit at memory location */
#define ResetBitValueD(adr, val)    {\
                                      register DWORD lIoData;\
                                      lIoData = *(DWORD volatile *)(adr);\
                                      lIoData &= ~((DWORD)(val));\
                                      *(DWORD volatile *)(adr) = lIoData;\
                                    }
/* test bit at memory location */
#define TestBitValueW(adr, val)      (((*(WORD volatile *)(adr)) & (WORD)(val)) ? TRUE : FALSE)
/* test bit at memory location */
#define TestBitValueD(adr, val)      (((*(DWORD volatile *)(adr)) & (DWORD)(val)) ? TRUE : FALSE)
#endif /* SPRZ193H == 0 */


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/

extern vModInfo XDATA ModuleInfo; /* module information */
extern BOOLEAN DATA fEmcyOverrun; /* EMCY overrun flag */


/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/

STATIC BYTE bFirstMsgTrans; /*!< after first transmission this is set to TRUE */
STATIC BYTE bCanStatus;     /*!< current CAN controller status */
/*!
  We must ensure that our ISR is only executed after the init routine
  was called. Otherwise it is possible that we find not initalised structures.
  This will crash the stack. Imagine a reset of the microcontroller but NOT
  of the CAN controller.
*/
STATIC BYTE bISRInitiated = FALSE;
CAN_MSG stCanMsgBuffer; /*!< local buffer to store one CAN message */


/*--------------------------------------------------------------------*/
/*  private functions                                                 */
/*--------------------------------------------------------------------*/

#if SPRZ193H == 1
/*!
  Read one word from memory.

  \param adr - address to read from
  \return value at given address
*/
static WORD ReadWord(DWORD adr)
{
  WORD rval;

  asm(" PUSH  ST1 ");  /* save status register on stack */
  asm(" SETC  INTM "); /* disable all interrupts */
  /* get value */
  rval = *(WORD volatile *)adr;
  if (!rval) {
    rval = *(WORD volatile *)adr;
  } /* if */
  asm(" POP   ST1 ");  /* restore status register */
  return rval;
}

/*!
  Read one double word from memory.

  \param adr - address to read from
  \return value at given address
*/
static DWORD ReadDword(DWORD adr)
{
  DWORD rval;

  asm(" PUSH  ST1 ");  /* save status register on stack */
  asm(" SETC  INTM "); /* disable all interrupts */
  /* get value */
  rval = *(DWORD volatile *)adr;
  if (!rval) {
    rval = *(DWORD volatile *)adr;
  } /* if */
  asm(" POP   ST1 ");  /* restore status register */
  return rval;
}

/*!
  Write one word to memory.

  \param dst - address to write to
  \param src - content to write
*/
static void WriteWord(DWORD dst, WORD src)
{
  asm(" PUSH  ST1 ");  /* save status register on stack */
  asm(" SETC  INTM "); /* disable all interrupts */
  *(WORD volatile *)dst = src; /* set value first time */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  *(WORD volatile *)dst = src; /* set value second time */
  asm(" POP   ST1 ");  /* restore status register */
}

/*!
  Write one double word to memory.

  \param dst - address to write to
  \param src - content to write
*/
static void WriteDword(DWORD dst, DWORD src)
{
  asm(" PUSH  ST1 ");  /* save status register on stack */
  asm(" SETC  INTM "); /* disable all interrupts */
  *(DWORD volatile *)dst = src; /* set value first time */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  *(DWORD volatile *)dst = src; /* set value second time */
  asm(" POP   ST1 ");  /* restore status register */
}

/*!
  Set bit at memory location (word).

  \param dst - address to write to
  \param val - bits to set
*/
static void SetBitValueW(DWORD dst, WORD val)
{
  WriteWord(dst, ReadWord(dst) | val);
}

/*!
  Set bit at memory location (double word).

  \param dst - address to write to
  \param val - bits to set
*/
static void SetBitValueD(DWORD dst, DWORD val)
{
  WriteDword(dst, ReadDword(dst) | val);
}

/*!
  Reset bit at memory location (word).

  \param dst - address to write to
  \param val - bits to clear
*/
static void ResetBitValueW(DWORD dst, WORD val)
{
  WriteWord(dst, ReadWord(dst) & ~val);
}

/*!
  Reset bit at memory location (double word).

  \param dst - address to write to
  \param val - bits to clear
*/
static void ResetBitValueD(DWORD dst, DWORD val)
{
  WriteDword(dst, ReadDword(dst) & ~val);
}

/*!
  Test bit at memory location (word).

  \param adr - address to read from
  \param val - bits to test
  \retval TRUE - bit is set
  \retval FALSE - bit is reset
*/
static WORD TestBitValueW(DWORD adr, WORD val)
{
  return (ReadWord(adr) & val) ? TRUE : FALSE;
}

/*!
  Test bit at memory location (double word).

  \param adr - address to read from
  \param val - bits to test
  \retval TRUE - bit is set
  \retval FALSE - bit is reset
*/
static WORD TestBitValueD(DWORD adr, DWORD val)
{
  return (ReadDword(adr) & val) ? TRUE : FALSE;
}
#endif /* SPRZ193H == 1 */


/*--------------------------------------------------------------------*/
/*  public functions                                                  */
/*--------------------------------------------------------------------*/

/*!
  Initialize the CAN management structures.
*/
void gCan_Init(void)
{
  bFirstMsgTrans = FALSE;  /* no transmission up to now */
  bCanStatus = CAN_STS_NORMAL; /* reset status information */
} /* gCan_Init */


/*!
  Set new mode of operation.

  \param bNewMode - new mode of operation
  \retval TRUE - new mode established
  \retval FALSE - new mode refused
*/
BYTE gCan_ActivateMode(BYTE bNewMode)
{
  BYTE bChangeOk = TRUE;

  switch (bNewMode) {
    case MODE_CONFIG:
      asm(" eallow"); /* disable protection */
      /* configuration mode request */
      WriteDword(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_CCR);
      while (!TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_CCE));
      /* software reset */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_SRES);
      /* select HECC mode */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_SCB);
      asm(" edis"); /* enable protection */
      bCanStatus |= CAN_STS_RESET;
      break;

    case MODE_NORMAL:
      asm(" eallow"); /* disable protection */
      /* CAN data stored in little-endian order */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_DBO);
      /* CAN in free run mode */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_SUSP);
      /* disable loop back self test mode */
      ResetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_STM);
      /* controller wake-up */
      ResetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_PDR);
      /* normal mode request */
      ResetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_CCR);
      while (TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_CCE));
      asm(" edis"); /* enable protection */
      bCanStatus &= ~CAN_STS_RESET;
      bCanStatus &= ~CAN_STS_SLEEPING;
      break;

    case MODE_SLEEP:
      asm(" eallow"); /* disable protection */
      /* controller in sleep mode */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_PDR);
      asm(" edis"); /* enable protection */
      bCanStatus |= CAN_STS_SLEEPING;
      break;

    case MODE_LOOPBK:
      asm(" eallow"); /* disable protection */
      /* enable loop back self test mode */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_STM);
      asm(" edis"); /* enable protection */
      break;

    case MODE_LISTEN:
      bChangeOk = FALSE; /* not switched to listen only mode */
      break;

    default:
      bChangeOk = FALSE; /* unknown mode */
      break;
  } /* switch */
  return bChangeOk;
}


/*!
  Initialize the CAN controller.
  \param bIndex selects the bit timing
*/
void gCan_CntrlInit(BYTE bIndex)
{
  BYTE i;                       /* counter */

  gCan_ActivateMode(MODE_CONFIG);

  asm(" eallow"); /* disable protection */

  /* set baud rate */
  WriteDword(CAN_BTC + CAN_BASIC_ADR, tCCanBtr[bIndex]);

  /* disable all message buffers */
  WriteDword(CAN_ME + CAN_BASIC_ADR, 0);

  /* set all message buffers to invalid */
  for (i = 0; i < 32; i++) {
    /* identifier zero, set AME */
    WriteDword(CAN_MID + CAN_TX_BUF + i * CAN_MSG_OFFSET + CAN_BASIC_ADR, 0x40000000UL);
    /* control off */
    WriteDword(CAN_MCF + CAN_TX_BUF + i * CAN_MSG_OFFSET + CAN_BASIC_ADR, 0);
    /* control off */
    WriteDword(CAN_MDL + CAN_TX_BUF + i * CAN_MSG_OFFSET + CAN_BASIC_ADR, 0);
    /* control off */
    WriteDword(CAN_MDH + CAN_TX_BUF + i * CAN_MSG_OFFSET + CAN_BASIC_ADR, 0);
    /* set mask to don't care*/
    WriteDword(CAN_LAM + i * 2 + CAN_BASIC_ADR, 0x9FFFFFFFUL);
  } /* for */

  asm(" edis"); /* enable protection */

  gCan_ActivateMode(MODE_NORMAL);

  Can_Configure();

  asm(" eallow"); /* disable protection */

  /* enable status interrupts on interrupt line 0 */
  /* enable message interrupts on interrupt line 1 */
  /* for bus-off, error passiv and warning level  */
  WriteDword(CAN_GIM + CAN_BASIC_ADR, 0x00000703);

  /* enable message buffer interrupts */
  WriteDword(CAN_MIM + CAN_BASIC_ADR, 0xFFFFFFFFUL);

  /* connect message buffer interrupts to line 1 */
  WriteDword(CAN_MIL + CAN_BASIC_ADR, 0xFFFFFFFFUL);

  asm(" edis"); /* enable protection */

  /* set first and second message buffer to transmit */
  /* and all other message buffers to receive */
  WriteDword(CAN_MD + CAN_BASIC_ADR, 0xFFFFFFFCUL);

  /* enable overwrite protection for all but one receive buffer */
  WriteDword(CAN_OPC + CAN_BASIC_ADR, 0xFFFFFFF8UL);

  /* enable all message buffers */
  WriteDword(CAN_ME + CAN_BASIC_ADR, 0xFFFFFFFFUL);

  bISRInitiated = TRUE;
}


/*!
  \brief Enter sleep mode.

  With this call we go to sleep. The stack is suspended and no
  longer executed.
  \retval TRUE - sleep mode established
  \retval FALSE - sleep mode refused
*/
BYTE gCan_InstallWakeUp(void)
{
  /* initiate sleeping mode */
  if (gCan_ActivateMode(MODE_SLEEP) == TRUE) {
    ModuleInfo.bModuleState = STACK_FREEZED;
    return TRUE;
  } /* if */
  else {
    return FALSE;
  } /* else */
}


/*!
  \brief Wake up from sleep mode.

  This call controls the recover from sleep mode.
*/
void gCan_WakeUpOccurred(void)
{
  /* initiate normal mode */
  if (gCan_ActivateMode(MODE_NORMAL) == TRUE) {
    ModuleInfo.bModuleState = STACK_WAKEUP;
  } /* if */
}


/*!
  Put a CAN message on the bus.
  \param ptMsg pointer to buffer
  \return NO_ERR transmission in progress,
          WAIT_ERR no transmission cause of inhibit time,
          BUSY_ERR no transmission cause of other transmission in progress
*/
BYTE gCan_SendMsg(CAN_MSG MEM_AREA * ptMsg)
{
  DWORD lId;   /* actually we support std/ext identifier */
  QBYTE lData; /* data frame content  */

  if (ptMsg == 0) {
    return NO_ERR;
  } /* if */

  if (ptMsg->Ext == 1) { /* extended frame */
    lId = ptMsg->qbId.dw | 0x80000000UL;
  } /* if */
  else {                 /* standard frame */
    lId = ptMsg->qbId.dw << 18;
  } /* else */

  /* controller busy ? */
  if (!TestBitValueD(CAN_TRS + CAN_BASIC_ADR, 1)) {
    /* disable TX message buffer */
    ResetBitValueD(CAN_ME + CAN_BASIC_ADR, 1);
    /* set COB ID */
    WriteDword(CAN_MID + CAN_TX_BUF + CAN_BASIC_ADR, lId);
    /* set DLC and RTR flag */
    WriteDword(CAN_MCF + CAN_TX_BUF + CAN_BASIC_ADR,
               (ptMsg->Rtr) ? ptMsg->Dlc | 0x10 : ptMsg->Dlc);
    /* copy message data bytes */
    lData.fb.b0 = ptMsg->bDb[0];
    lData.fb.b1 = ptMsg->bDb[1];
    lData.fb.b2 = ptMsg->bDb[2];
    lData.fb.b3 = ptMsg->bDb[3];
    WriteDword(CAN_MDL + CAN_TX_BUF + CAN_BASIC_ADR, lData.dw);
    lData.fb.b0 = ptMsg->bDb[4];
    lData.fb.b1 = ptMsg->bDb[5];
    lData.fb.b2 = ptMsg->bDb[6];
    lData.fb.b3 = ptMsg->bDb[7];
    WriteDword(CAN_MDH + CAN_TX_BUF + CAN_BASIC_ADR, lData.dw);
    /* enable TX message buffer */
    SetBitValueD(CAN_ME + CAN_BASIC_ADR, 1);
    /* set CAN controller transmission request */
    SetBitValueD(CAN_TRS + CAN_BASIC_ADR, 1);
  } /* if */
  else {
    return BUSY_ERR;                    /* OUT: busy error              */
  } /* else */
  return NO_ERR;                        /* OUT: no error                */
}


/*!
  \brief CAN message buffer ISR.

  The CAN receive/transmit interrupt service routine.
*/
interrupt void gCan_MsgIntHandler(void)
{
  QBYTE lData;                      /* data frame content  */
  DWORD lSlider;                    /* message buffer position */
  BYTE bBNr;                        /* message buffer number */
#if ID_FILTER != 0
  WORD wId;                         /* received identifier */
#endif /* ID_FILTER */

  asm(" CLRC INTM");  // allow preemption on this IRQ
  /* transmit interrupt ? */
  if (TestBitValueD(CAN_TA + CAN_BASIC_ADR, 1)) {
    /* acknowledge transmit interrupt */
    WriteDword(CAN_TA + CAN_BASIC_ADR, 1);

    /* transmit interrupt - last transmission successful */
    bFirstMsgTrans = TRUE;
    bCanStatus |= CAN_STS_TX_OK;

    /* signal empty TX buffer */
    gCB_SignalTx();
  } /* if */

  lSlider = 0x80000000UL;
  bBNr = 31;

  while (ReadDword(CAN_RMP + CAN_BASIC_ADR) & 0xFFFFFFFCUL) {
    do {
      if (TestBitValueD(CAN_RMP + CAN_BASIC_ADR, lSlider)) {
#if ID_FILTER != 0
        /* 29 bit identifier not allowed */
        if (TestBitValueD(CAN_MID + CAN_TX_BUF + bBNr * CAN_MSG_OFFSET
                          + CAN_BASIC_ADR, 0x80000000UL)) {
          /* release receive buffer */
          WriteDword(CAN_RMP + CAN_BASIC_ADR, lSlider);
          continue;
        } /* if */

        /* get identifier */
        wId = (WORD)(ReadDword(CAN_MID + CAN_TX_BUF + bBNr
                               * CAN_MSG_OFFSET + CAN_BASIC_ADR) >> 18) & 0x07FF;

        /* check identifier */
        if (gCB_PreCheckIdStd(wId) == FALSE) {
          /* release receive buffer */
          WriteDword(CAN_RMP + CAN_BASIC_ADR, lSlider);
          continue;
        } /* if */

        /* standard frame received */
        stCanMsgBuffer.Ext = 0;

        /* copy COB ID */
        stCanMsgBuffer.qbId.dw = wId;
#else
        /* check 29 bit identifier */
        if (TestBitValueD(CAN_MID + CAN_TX_BUF + bBNr * CAN_MSG_OFFSET
                          + CAN_BASIC_ADR, 0x80000000UL)) {
          /* extended frame received */
          stCanMsgBuffer.Ext = 1;
          /* get COB ID */
          stCanMsgBuffer.qbId.dw = ReadDword(CAN_MID + CAN_TX_BUF
                                             + bBNr * CAN_MSG_OFFSET
                                             + CAN_BASIC_ADR) & 0x1FFFFFFFUL;
        } /* if */
        else {
          /* standard frame received */
          stCanMsgBuffer.Ext = 0;
          /* get COB ID */
          stCanMsgBuffer.qbId.dw = (ReadDword(CAN_MID + CAN_TX_BUF
                                              + bBNr * CAN_MSG_OFFSET
                                              + CAN_BASIC_ADR) >> 18) & 0x07FF;
        } /* else */
#endif /* ID_FILTER */

        /* get RTR */
        stCanMsgBuffer.Rtr = TestBitValueD(CAN_MCF + CAN_TX_BUF
                                           + bBNr * CAN_MSG_OFFSET
                                           + CAN_BASIC_ADR, 0x10);

        /* get DLC */
        stCanMsgBuffer.Dlc = (BYTE)(ReadDword(CAN_MCF + CAN_TX_BUF
                                              + bBNr * CAN_MSG_OFFSET
                                              + CAN_BASIC_ADR) & 0x0F);
    
        /* check DLC */
        if (stCanMsgBuffer.Dlc > 8) {
          stCanMsgBuffer.Dlc = 8;
        } /* if */
    
        /* copy message data bytes */
        lData.dw = ReadDword(CAN_MDL + CAN_TX_BUF
                             + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR);
        stCanMsgBuffer.bDb[0] = lData.fb.b0;
        stCanMsgBuffer.bDb[1] = lData.fb.b1;
        stCanMsgBuffer.bDb[2] = lData.fb.b2;
        stCanMsgBuffer.bDb[3] = lData.fb.b3;
        lData.dw = ReadDword(CAN_MDH + CAN_TX_BUF
                             + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR);
        stCanMsgBuffer.bDb[4] = lData.fb.b0;
        stCanMsgBuffer.bDb[5] = lData.fb.b1;
        stCanMsgBuffer.bDb[6] = lData.fb.b2;
        stCanMsgBuffer.bDb[7] = lData.fb.b3;

        /* check overrun condition */
        if (bBNr == 2) {
          if (TestBitValueD(CAN_RML + CAN_BASIC_ADR, 4)) {
            /* release receive buffer */
            WriteDword(CAN_RMP + CAN_BASIC_ADR, 4);
            /* set EMCY overrun flag */
            fEmcyOverrun = TRUE;
            continue;
          } /* if */
        } /* if */

        /* release receive buffer */
        WriteDword(CAN_RMP + CAN_BASIC_ADR, lSlider);

        /* put message to storage location */
        gCB_CanBufferMsg(&stCanMsgBuffer);
      } /* if */
    }
    while(0); /* do while */

    /* who's next ? */
    lSlider >>= 1;
    bBNr--;
    if (bBNr == 1) { /* wrap around */
      lSlider = 0x80000000UL;
      bBNr = 31;
    } /* if */
  } /* while */

  /* acknowledge interrupt */
  PIEACK = 0x0100;
}


/*!
  \brief CAN error ISR.

  The CAN error interrupt service routine.
*/
interrupt void gCan_SysIntHandler(void)
{
  if (TestBitValueD(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_WARN)) {
    /* warning level interrupt */
    bCanStatus |= CAN_STS_WARNING;
    /* no bus-off detected */
    bCanStatus &= ~CAN_STS_BUS_OFF;
    /* clear error interrupt flag */
    WriteDword(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_WARN);
  } /* if */

  if (TestBitValueD(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_PASV)) {
    /* warning level interrupt */
    bCanStatus |= CAN_STS_WARNING;
    /* no bus-off detected */
    bCanStatus &= ~CAN_STS_BUS_OFF;
    /* clear error interrupt flag */
    WriteDword(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_PASV);
  } /* if */

  if (TestBitValueD(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_BOFF)) {
    /* bus-off interrupt */
    bCanStatus |= CAN_STS_BUS_OFF;
    /* leave warning level */
    bCanStatus &= ~CAN_STS_WARNING;
    /* clear error interrupt flag */
    WriteDword(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_BOFF);
  } /* if */
}


/*!
  Prepare a CAN buffer to handle a given remote message.
  \param ptMsg pointer to buffer
  \return NO_ERR buffer established
*/
#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
BYTE gCan_PrepareRemoteMessage(CAN_MSG MEM_AREA *ptMsg)
{
  if (ptMsg == 0) {
    return NO_ERR;
  } /* if */
  return NO_ERR;
}
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */


/*!
  Read the current status information of the CAN controller.
  The variable \e bCanStatus is local to the CAN module. Via this call
  we have an encapsulated access to this information.
  \return
  \link #CAN_STS_NORMAL CAN_STS_NORMAL \endlink
  or
  \link #CAN_STS_RESET CAN_STS_RESET \endlink
  | \link #CAN_STS_WARNING CAN_STS_WARNING \endlink
  | \link #CAN_STS_BUS_OFF CAN_STS_BUS_OFF \endlink
  | \link #CAN_STS_SLEEPING CAN_STS_SLEEPING \endlink
  | \link #CAN_STS_OVERRUN CAN_STS_OVERRUN \endlink
  | \link #CAN_STS_TX_OK CAN_STS_TX_OK \endlink
*/
BYTE gCan_GetCanStatus(void)
{
  /* update all status flags */
  WriteDword(CAN_ES + CAN_BASIC_ADR, 0x01BF0000);

  if (TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_EW)
      || TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_EP)) {
    /* warning level detected */
    bCanStatus |= CAN_STS_WARNING;
  } /* if */
  else {
    /* no warning level detected */
    bCanStatus &= ~CAN_STS_WARNING;
  } /* if */

  if (TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_BO)) {
    /* bus-off detected */
    bCanStatus |= CAN_STS_BUS_OFF;
  } /* if */
  else {
    /* no bus-off detected */
    bCanStatus &= ~CAN_STS_BUS_OFF;
  } /* else */
  return bCanStatus;
}


/*!
  This function signals that the first message (boot-up message) is transmitted
  successfully.
  \retval TRUE - First message successfully transmitted.
  \retval FALSE - First message not yet transmitted.
*/
BYTE gCan_TxOccurred(void) 
{
  return bFirstMsgTrans;
}


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief CAN driver for the Texas Instruments TMS320F28x eCAN.
  \par File name ecan28.c
  \version 5
  \date 18.05.05 17:39
  \author (c) 1998-2005 by Vector Informatik GmbH, Stuttgart

  NOTE: This driver release includes some workarounds regarding the
        TI errata SPRZ193H CAN - CPU access to the eCAN registers.
*/

/*!
  \var tCCanBtr
  \brief CAN controller bit timing values.

  The bit timing values are defined for a 150 MHz clock in ascending order,
  first BTR0 then BTR1.
  - 10K bit/s, DO NOT USE
  - 20K bit/s, DO NOT USE
  - 50K bit/s
  - 100K bit/s
  - 125K bit/s
  - 250K bit/s
  - 500K bit/s
  - 800K bit/s
  - 1M bit/s
*/
