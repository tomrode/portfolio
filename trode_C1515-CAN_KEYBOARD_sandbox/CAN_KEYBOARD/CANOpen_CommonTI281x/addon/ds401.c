/*--------------------------------------------------------------------
       DS401.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: CANopen DS-401 I/O Profile Functionality
  --------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/

#include "portab.h"
#include "cos_main.h"
#include "vassert.h"
#include "vdbgprnt.h"
#include "cancntrl.h"
#include "buffer.h"
#include "profil.h"
#include "objdict.h"
#include "sdoserv.h"
#include "emcyhndl.h"
#include "ds401.h"
#include <string.h>

#ifndef __18CXX
# if DS401_TEST == 1
#  include "stdio.h"
# endif /* DS401_TEST == 1 */
#endif /* __18CXX */


#if (PROFILE_DS401 == 1) && (GENERIC_PROFILE_USED == 1)

/*--------------------------------------------------------------------*/
/*  local data                                                        */
/*--------------------------------------------------------------------*/
/*! buffer for raw data of digital input */
STATIC BYTE XDATA abDI_RawDataInput[INPUT_BUFFER_LEN_DI];
/*! buffer for raw data of analogue input */
STATIC WORD XDATA awAI_RawDataInput[INPUT_BUFFER_LEN_AI];
/*! buffer for data of digital output */
STATIC BYTE XDATA abDO_OutputData[OUTPUT_BUFFER_LEN_DO];
/*! buffer for data of analogue output */
STATIC WORD XDATA awAO_OutputData[OUTPUT_BUFFER_LEN_AO];

/*!
  Shift values for analogue input channels.
  Each analogue input channel has its own shift value for left adjusted 
  data storage in this table. The shift value results from the D/A converter
  resolution and a 16-bit base.
  This table has to be adapted for the correct number of entries (available 
  analogue input channels) and the suitable shift value for each channel.
*/
BYTE CONST tIO_ConverterResShift[INPUT_BUFFER_LEN_AI] = {
   { 6 }, /*  analogue input  1 */
#if INPUT_BUFFER_LEN_AI > 1
   { 6 }, /*  analogue input  2 */
#endif
#if INPUT_BUFFER_LEN_AI > 2
   { 6 }, /*  analogue input  3 */
#endif
#if INPUT_BUFFER_LEN_AI > 3
   { 6 }, /*  analogue input  4 */
#endif
#if INPUT_BUFFER_LEN_AI > 4
   { 6 }, /*  analogue input  5 */
#endif
#if INPUT_BUFFER_LEN_AI > 5
   { 6 }, /*  analogue input  6 */
#endif
#if INPUT_BUFFER_LEN_AI > 6
   { 6 }, /*  analogue input  7 */
#endif
#if INPUT_BUFFER_LEN_AI > 7
   { 6 }, /*  analogue input  8 */
#endif
#if INPUT_BUFFER_LEN_AI > 8
   { 4 }, /*  analogue input  9 */
#endif
#if INPUT_BUFFER_LEN_AI > 9
   { 4 }, /*  analogue input 10 */
#endif
#if INPUT_BUFFER_LEN_AI > 10
   { 4 }, /*  analogue input 11 */
#endif
#if INPUT_BUFFER_LEN_AI > 11
   { 4 }, /*  analogue input 12 */
#endif
#if INPUT_BUFFER_LEN_AI > 12
   { 4 }, /*  analogue input 13 */
#endif
#if INPUT_BUFFER_LEN_AI > 13
   { 4 }, /*  analogue input 14 */
#endif
#if INPUT_BUFFER_LEN_AI > 14
   { 4 }, /*  analogue input 15 */
#endif
#if INPUT_BUFFER_LEN_AI > 15
   { 4 }, /*  analogue input 16 */
#endif
};


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/

/* input buffer digital  */
extern  BYTE XDATA abInDataDig[INPUT_BUFFER_LEN_DI];
/* output buffer digital */
extern  BYTE XDATA abOutDataDig[OUTPUT_BUFFER_LEN_DO];
/* location of polarity values (Index 6002h) */
extern  BYTE abDI_PolarityInput[INPUT_BUFFER_LEN_DI];
/* location of filter constant values (Index 6003h) */
extern  BYTE abDI_FilterConstantInput[INPUT_BUFFER_LEN_DI];
/* global interrupt enable digital (Index 6005h) */
extern  BOOLEAN fDI_GlobalIntEnable;
/* location of any change mask values (Index 6006h) */
extern  BYTE abDI_IntMaskAnyChange[INPUT_BUFFER_LEN_DI];
/* location of low to high mask values (Index 6007h) */
extern  BYTE abDI_IntMaskLowToHigh[INPUT_BUFFER_LEN_DI];
/* location of high to low mask values (Index 6008h) */
extern  BYTE abDI_IntMaskHighToLow[INPUT_BUFFER_LEN_DI];
/* location of polarity values (Index 6202h) */
extern  BYTE abDO_ChangePolarityOutput[OUTPUT_BUFFER_LEN_DO];
/* location of filter mask values (Index 6208h) */
extern  BYTE abDO_FilterConstantOutput[OUTPUT_BUFFER_LEN_DO];
/* location of previous data values of object 6411h */
extern vEmcyManage XDATA EmcyTable;
/* location of interrupt trigger selection (Index 6421h) */
extern BYTE abAI_IntTriggerSelection[INPUT_BUFFER_LEN_AI];
#if OBJECT_6422_USED == 1
/* location of interrupt source (Index 6422h) */
extern DWORD adwAI_IntSource[];
#endif /* OBJECT_6422_used == 1 */
/* global interrupt enable (Index 6423h) */
extern BOOLEAN fAI_GlobalIntEnable;
/* location of interrupt upper limit integer (Index 6424h) */
extern DWORD adwAI_IntUpperLimitInteger[INPUT_BUFFER_LEN_AI];
/* location of interrupt lower limit integer (Index 6425h) */
extern DWORD adwAI_IntLowerLimitInteger[INPUT_BUFFER_LEN_AI];
/* location of interrupt delta unsigned (Index 6426h) */
extern DWORD adwAI_IntDeltaUnsigned[INPUT_BUFFER_LEN_AI];
/* location of interrupt negative delta unsigned (Index 6427h) */
extern DWORD adwAI_IntNegDeltaUnsigned[INPUT_BUFFER_LEN_AI];
/* location of interrupt positive delta unsigned (Index 6428h) */
extern DWORD adwAI_IntPosDeltaUnsigned[INPUT_BUFFER_LEN_AI];
/* location of error mode (Index 6443h) */
extern  BYTE abAO_ErrorMode[OUTPUT_BUFFER_LEN_AO];
/* location of error value integer (Index 6444h) */
extern DWORD adwAO_ErrorValueInteger[OUTPUT_BUFFER_LEN_AO];
/* module information */
extern vModInfo XDATA ModuleInfo;

#if DS401_TEST == 1
extern BYTE XDATA abInRawDig[INPUT_BUFFER_LEN_DI]; /* input buffer for test purposes! */
extern WORD XDATA awInRawAna[INPUT_BUFFER_LEN_AI]; /* input buffer for test purposes! */
#endif /* DS401_TEST == 1 */


/*--------------------------------------------------------------------*/
/*  local function prototypes                                         */
/*--------------------------------------------------------------------*/
STATIC void DI_Update(void);
STATIC void AI_Update(void);
STATIC void DO_WriteData(BYTE);
STATIC void AO_WriteData(BYTE);
STATIC void AI_ReadData(void);
STATIC void DI_ReadData(void);
STATIC void InitIO(void);


/*--------------------------------------------------------------------*/
/*  function definitions                                              */
/*--------------------------------------------------------------------*/

/*!
  \brief Initialize I/O module.

  This function initializes the I/O module. Default values are set to the object
  dictionary entries and the I/O hardware dependent settings can be performed.
  Additional object initializations may be placed inside this function.  
*/
void gIO_Init(void)
{
  /* initialize raw input data for digital inputs with values from object 6000h */
  MemCpy(abDI_RawDataInput, abInDataDig, INPUT_BUFFER_LEN_DI);

  /* initialize digital output data with values from object 6200h */
  MemCpy(abDO_OutputData, abOutDataDig, OUTPUT_BUFFER_LEN_DO);

  /* initialize raw input data for analogue inputs with values from object 6401h */
  MemCpy(awAI_RawDataInput, awInDataAna, INPUT_BUFFER_LEN_AI * 2);

  /* initialize analogue output data with values from object 6411h */
  MemCpy(awAO_OutputData, awOutDataAna, OUTPUT_BUFFER_LEN_AO * 2);

  /* object 6002 */
  MemSet(abDI_PolarityInput, DEFAULT_ZERO, INPUT_BUFFER_LEN_DI);

  /* object 6003 */
  MemSet(abDI_FilterConstantInput, DEFAULT_ZERO, INPUT_BUFFER_LEN_DI);

  /* object 6005 */
  fDI_GlobalIntEnable = TRUE;
 
  /* object 6006 */
  MemSet(abDI_IntMaskAnyChange, DI_MASK_ANYCHANGE_DEF, INPUT_BUFFER_LEN_DI);

  /* object 6007 */
  MemSet(abDI_IntMaskLowToHigh, DEFAULT_ZERO, INPUT_BUFFER_LEN_DI);

  /*object 6008 */
  MemSet(abDI_IntMaskHighToLow, DEFAULT_ZERO, INPUT_BUFFER_LEN_DI);

  /* object 6202 */
  MemSet(abDO_ChangePolarityOutput, DEFAULT_ZERO, OUTPUT_BUFFER_LEN_DO);

  /* object 6208 */
  MemSet(abDO_FilterConstantOutput, DO_FILTERCONST_DEF, OUTPUT_BUFFER_LEN_DO);

  /* object 6421 */
  MemSet(abAI_IntTriggerSelection, AI_INTTRIGSEL_DEF, INPUT_BUFFER_LEN_AI);

#if OBJECT_6422_USED == 1
  /* object 6422 */
  MemSet(adwAI_IntSource, DEFAULT_ZERO, NUM_AI_INTSOURCE * 4);
#endif /* OBJECT_6422_USED == 1 */

  /* objext 6423 */
  fAI_GlobalIntEnable = FALSE;

  /* objext 6424 */
  MemSet(adwAI_IntUpperLimitInteger, DEFAULT_ZERO, INPUT_BUFFER_LEN_AI * 4);

  /* objext 6425 */
  MemSet(adwAI_IntLowerLimitInteger, DEFAULT_ZERO, INPUT_BUFFER_LEN_AI * 4);

  /* objext 6426 */
  MemSet(adwAI_IntDeltaUnsigned, DEFAULT_ZERO, INPUT_BUFFER_LEN_AI * 4);
 
  /* object 6427 */
  MemSet(adwAI_IntNegDeltaUnsigned, DEFAULT_ZERO, INPUT_BUFFER_LEN_AI * 4);

  /* object 6428 */
  MemSet(adwAI_IntPosDeltaUnsigned, DEFAULT_ZERO ,INPUT_BUFFER_LEN_AI * 4);

  /* object 6443 */
  MemSet(abAO_ErrorMode, AO_ERRORMODE_DEF, OUTPUT_BUFFER_LEN_AO);

  /* object 6444 */
  MemSet(adwAO_ErrorValueInteger, DEFAULT_ZERO, OUTPUT_BUFFER_LEN_AO);

  InitIO(); /* configure the hardware dependent I/O interface */
} /* gIO_Init */


/*!
  \brief Process digital and analogue input channels.
  
  This function reads and updates the digital and analogue inputs.
  It is called cyclically from inside the main loop in #OPERATIONAL and
  #PRE_OPERATIONAL state.
  Additional cyclic operations belonging to the I/O profile may be executed here.
*/
void gIO_Task(void)
{
  /* process digital input channels */
  DI_ReadData();  /* Read digital physical inputs  */
  DI_Update();    /* Update all DI channels        */

  /* process analogue input channels */
  AI_ReadData();  /* Read analogue physical inputs */
  AI_Update();    /* Update all AI channels        */
} /* gIO_Task */


/*!
  \brief Updating of digital input channels.

  This function updates the digital input channels by processing the raw data 
  read from the physical inputs before. The result is then copied to the 
  corresponding buffer area. This function is called by gIO_Task().
*/
STATIC void DI_Update(void)
{
  BYTE DATA i, j;
  BYTE DATA bPosition; /* actual single bit position */
  BYTE DATA bRetVal;   /* finally holds calculated value to be written to object
                          dictionary location */

  for (i = 0; i < INPUT_BUFFER_LEN_DI; i++) { /* process all digital input channels */
    /* check filter constant and polarity */
    if ((abDI_FilterConstantInput[i] != 0) || (abDI_PolarityInput[i] != 0)) {
      /* at leat one parameter configured: process input data */

      bRetVal = abInDataDig[i]; /* initialize return value with current value */
      bPosition = 0x01; /* set bit position to extract bit 0 */

      for (j = 0; j < (sizeof(BYTE) * 8); j++) { /* rotate through all bits */
        if (((abDI_FilterConstantInput[i] & bPosition) == bPosition)) {
          /* don't care: leave bit value as it is */
        }
        else {  
          /* evaluate the bit */
          if (((abDI_PolarityInput[i] & bPosition) == bPosition)) {
            /* change polarity configured: invert bit */
            if (((abDI_RawDataInput[i] & bPosition) == bPosition)) {
              /* reset bit */
              bRetVal &= ~(bPosition);
            } /* if */
            else {
              /* set bit */
              bRetVal |= bPosition;
            } /* else */
          }
          else {
            /* no inversion configured: set read value */
            if (((abDI_RawDataInput[i] & bPosition) == bPosition)) {
              /* set bit */
              bRetVal |= bPosition;
            } /* if */
            else {
              /* reset bit */
              bRetVal &= ~(bPosition);
            } /* else */
          } /* else */
        } /* else */
        bPosition <<= 0x01; /* set bit position to extract next bit */
      } /* for */
    } /* if */
    else {
      /* no parameter configured: copy raw data */
      bRetVal = (BYTE) abDI_RawDataInput[i];
    } /* else */

    /* copy new value to object 6000h */
    abInDataDig[i] = bRetVal;
  } /* for */
} /* DI_Update */


/*!
  \brief Updating of analogue input channels.

  This function updates the analogue input channels by processing the raw data
  read from the physical input before. The result is then copied to the 
  corresponding buffer area. This function is called by gIO_Task().
  Further processing of the analogue input values (e.g. signal conditioning)
  may be placed inside this function.
*/
STATIC void AI_Update(void)
{
  BYTE DATA i;

  for (i = 0; i < INPUT_BUFFER_LEN_AI; i++) {
    /* process analogue input channels */
    /* place further data processing (signal conditioning,...) here */

    /* copy (raw) input values to object 6401h */
    /* store values left adjusted */
    awInDataAna[i] = awAI_RawDataInput[i] << tIO_ConverterResShift[i];
  } /* for */
} /* AI_Update */


/*!
  \brief Checking for changed digital and analogue input data.

  This function returns TRUE, if the input data has changed so that a new 
  transmit PDO has to be sent. The current input values are compared with
  the last ones sent taking into account the parameter of the object dictionary.
  A call to this function is performed inside gSlvCreatePDO()
  (see #PROFILE_PDO_CREATE).
  Processing of further objects may be placed inside this function by extending
  the switch()-block with additional objects/code.
  
  \param wIdx      - Index
  \param bSubIdx   - Subindex
  \param bPrevData - Pointer to previous sent data
  \return TRUE if value has changed
*/
BOOLEAN gIO_CheckValChangedInput(WORD wIdx, BYTE bSubIdx, FAR BYTE XDATA * bPrevData)
{
  BYTE DATA i;
  BYTE DATA bPosition; /* actual single bit position                */
  BYTE DATA bOffset;   /* offset of current object in data location */
  BYTE bLimitIntCount; /* counter for limit exceeded interrupts     */
  BYTE bDeltaIntCount; /* counter for delta exceeded interrupts     */
  WORD wLoc16BitPrev;  /* previously sent 16-bit value              */
  BOOLEAN DATA bRet;   /* return value                              */

  /* initialize local variables */
  bLimitIntCount = 0;
  bDeltaIntCount = 0;
  bRet = FALSE;

  if (bSubIdx != 0) {
    /* valid subindex: calculate offset for object data */
    bOffset = (bSubIdx - 1);

    /* branch to correspondig object handling */
    switch (wIdx) {
      case DIGI_IN_8BIT:
        /* digital input 8-bit: object 6000h */

        if (fDI_GlobalIntEnable) {
          /* global interrupt enabled: transmitting of data allowed */

          /* check for changes */
          if (abDI_IntMaskAnyChange[bOffset] > 0) {
            /* object 6006 */
            bPosition = 0x01; /* set bit position to extract bit 0 */

            /* rotate through all bits */
            for (i = 0; (i < (sizeof(BYTE) * 8)) && (bRet == FALSE); i++) {
              if (((abDI_IntMaskAnyChange[bOffset] & bPosition) == bPosition)) {
                /* check bit value */
                if ((abInDataDig[bOffset] & bPosition) !=
                   ((BYTE) bPrevData[0] & bPosition)) {
                  /* bit value has changed */
                  bRet = TRUE;
                } /* if */
              } /* if */
              bPosition <<= 0x01; /* set bit position to extract next bit */
            } /* for */
          } /* if */

          /* no new value so far: check with next object */
          if ((bRet == FALSE) && (abDI_IntMaskLowToHigh[bOffset] > 0)) {
            /* object 6007 */
            bPosition = 0x01; /* set bit position to extract bit 0 */

            /* rotate through all bits */
            for (i = 0; (i < (sizeof(BYTE) * 8)) && (bRet == FALSE); i++) {
              if (((abDI_IntMaskLowToHigh[bOffset] & bPosition) == bPosition)) {
                /* check bit value */
                if (((abInDataDig[bOffset] & bPosition) == bPosition) &&
                    (((BYTE) bPrevData[0] & bPosition) == 0)) {
                  /* bit value has changed */
                  bRet = TRUE;
                } /* if */
              } /* if */
              bPosition <<= 0x01; /* set bit position to extract next bit */
            } /* for */
          } /* if */

          /* no new value so far: check with next object */
          if ((bRet == FALSE) && (abDI_IntMaskHighToLow[bOffset] > 0)) {
            /* object 6008 */
            bPosition = 0x01; /* set bit position to extract bit 0 */

            /* rotate through all bits */
            for (i = 0; (i < (sizeof(BYTE) * 8)) && (bRet == FALSE); i++) {
              if (((abDI_IntMaskHighToLow[bOffset] & bPosition) == bPosition)) {
                /* check bit value */
                if (((abInDataDig[bOffset] & bPosition) == 0) &&
                    (((BYTE) bPrevData[0] & bPosition) == bPosition)) {
                  /* bit value has changed */
                  bRet = TRUE;
                } /* if */
              } /* if */
              bPosition <<= 0x01; /* set bit position to extract next bit */
            } /* for */
          } /* if */
        } /* if */
      break;

      case ANA_IN_16BIT:
        /* analogue input 16-bit: object 6401h */

        /* create local 16 bit comparison value of previous data */
        wLoc16BitPrev = (((WORD)bPrevData[1]) << 8) | bPrevData[0];
        
        if (
            fAI_GlobalIntEnable && 
            (abAI_IntTriggerSelection[bOffset] & AI_UPPER_LOWER_LIMIT_MASK)
           ) {
          /* global interrupt enabled & limit selected: transmitting of data allowed */
          /* check for changes */

          if (abAI_IntTriggerSelection[bOffset] & AI_UPPER_LIMIT_MASK) {
            /* UPPER LIMIT object 6424h selected */
            if ((awInDataAna[bOffset] >= 
                /* create 16-bit value */
                (WORD)(adwAI_IntUpperLimitInteger[bOffset] >> 16))
               &&
                /* compare with previous value */
                (awInDataAna[bOffset] != wLoc16BitPrev)) {
              /* input data exceeds limit & value has changed */
              bLimitIntCount++;
            } /* if */
          } /* if */

          if (abAI_IntTriggerSelection[bOffset] & AI_LOWER_LIMIT_MASK) {
            /* LOWER LIMIT object 6425h selected */
            if (
                (awInDataAna[bOffset] < 
                /* create 16-bit value */
                (WORD)(adwAI_IntLowerLimitInteger[bOffset] >> 16))
               &&
                /* compare with previous data */
                (awInDataAna[bOffset] != wLoc16BitPrev)
               ) {
              /* input data exceeds limit & value has changed */
              bLimitIntCount++;
            } /* if */
          } /* if */

          if (abAI_IntTriggerSelection[bOffset] & AI_DELTA_MASK) {
            /* VALUE DELTA configured */
            if (abAI_IntTriggerSelection[bOffset] & AI_DELTA_UNSIGNED_MASK) {
              /* value delta object 6426h selected */
              if (
                  awInDataAna[bOffset] >= 
                  /* previous value + 16-bit delta value left adjusted */
                  (wLoc16BitPrev + (((WORD)adwAI_IntDeltaUnsigned[bOffset])
                                           << tIO_ConverterResShift[bOffset]))
                 ||
                  awInDataAna[bOffset] <=
                  /* previous value - 16-bit delta value left adjusted */
                  (wLoc16BitPrev - (((WORD)adwAI_IntDeltaUnsigned[bOffset])
                                           << tIO_ConverterResShift[bOffset]))
                 ) {
                /* delta value exceeded */
                bDeltaIntCount++;
              } /* if */
            } /* if */

            if (abAI_IntTriggerSelection[bOffset] & AI_POSITIVE_DELTA_MASK) {
              /* negative value delta object 6427h selected*/
              if (
                  awInDataAna[bOffset] <= 
                  /* previous value - 16-bit delta value left adjusted */
                  (wLoc16BitPrev - (((WORD)adwAI_IntNegDeltaUnsigned[bOffset])
                                           << tIO_ConverterResShift[bOffset]))
                 ) {
                /* delta value exceeded */
                bDeltaIntCount++;
              } /* if */
            } /* if */

            if (abAI_IntTriggerSelection[bOffset] & AI_NEGATIVE_DELTA_MASK) {
              /* positive value delta object 6428h selected */
              if (
                  awInDataAna[bOffset] >=
                  /* previous value + 16-bit delta value left adjusted */
                  (wLoc16BitPrev + (((WORD)adwAI_IntPosDeltaUnsigned[bOffset])
                                           << tIO_ConverterResShift[bOffset]))
                 ) {
                /* delta value exceeded */
                bDeltaIntCount++;
              } /* if */
            } /* if */
          } /* if */

          /* check for ONE limit exceeded */
          if (bLimitIntCount == 1) {
            /* upper XOR lower limit exceeded */
            /* additional delta trigger? */
            if (abAI_IntTriggerSelection[bOffset] & AI_DELTA_MASK) {
              /* additional trigger condition configured: interrupt? */
              if (bDeltaIntCount > 0) {
                /* value delta occurred: new value available */
                bRet = TRUE;
#if OBJECT_6422_USED == 1
                /* update object 6422h Interrupt Source: set corresponding bit */
                adwAI_IntSource[(bOffset / 32)] |= (((DWORD)0x01) << bOffset);
#endif /* OBJECT_6422_USED == 1 */
              } /* if */
            } /* if */
            else {
              /* new value available */
              bRet = TRUE;
#if OBJECT_6422_USED == 1
              /* update object 6422h Interrupt Source: set corresponding bit */
              adwAI_IntSource[(bOffset / 32)] |= (((DWORD)0x01) << bOffset);
#endif /* OBJECT_6422_USED == 1 */
            } /* else */
          } /* if */
        } /* if */
      break;

      default:
        /* no processing for further objects implemented! */
      break;
    } /* switch */
  } /* if */

  return bRet;
} /* gIO_CheckValChangedInput */


/*!
  \brief Processing of output data received by PDO or SDO.

  This function processes received output data. It is distinguished between 
  digital and analogue data. Data is processed taking into account the parameters
  from the object dictionary. Finally the data is put to the output location.
  A call to this function is performed inside gSlvAnalyzePDO()
  (see #PROFILE_PDO_ANALYZE) and within the <sdoserv.c> module (SDO download: 
  see #PROFILE_SDO_DOWN).
  Processing of further objects may be placed inside this function by extending
  the switch()-block with additional objects/code.

  \param wIdx      - Index
  \param bSubIdx   - Subindex
*/
void gIO_EvaluateOutputValues(WORD wIdx, BYTE bSubIdx)
{
  BYTE DATA i;
  BYTE DATA bPosition;
  BYTE DATA bOffset;   /* offset of current object in data location */
  BYTE DATA bRetVal;   /* finally holds calculated value to be written to 
                          physical output location */

  if (bSubIdx != 0) {
    /* valid subindex: calculate offset for object data */
    bOffset = (bSubIdx - 1);

    /* branch to correspondig object handling */
    switch (wIdx) {
      case DIGI_OUT_8BIT:
        /* digital output 8-bit: object 6200h */

        /* check filter constant and polarity */
        if ((abDO_FilterConstantOutput[bOffset] != 0xFF) ||
            (abDO_ChangePolarityOutput[bOffset] != 0)) {
          /* filter or polarity parameter set */

          /* initialize return value with current value */
          bRetVal = abDO_OutputData[bOffset];
          bPosition = 0x01; /* set bit position to extract bit 0 */

          for (i = 0; i < (sizeof(BYTE) * 8); i++) { /* rotate through all bits */
            if (((abDO_FilterConstantOutput[bOffset] & bPosition) == bPosition)) {
              /* bit not filtered: evaluate */
              if (((abDO_ChangePolarityOutput[bOffset] & bPosition) == bPosition)) {
                /* invert bit */
                if (((abOutDataDig[bOffset] & bPosition) == bPosition)) {
                  /* reset bit */
                  bRetVal &= ~(bPosition);
                } /* if */
                else {
                  /* set bit */
                  bRetVal |= bPosition;
                } /* else */
              }
              else {
                /* no inversion */
                if (((abOutDataDig[bOffset] & bPosition) == bPosition)) {
                  /* set bit */
                  bRetVal |= bPosition;
                } /* if */
                else {
                  /* reset bit */
                  bRetVal &= ~(bPosition);
                } /* else */
              } /* else */
            } /* if */
            else {
              /* don't care: leave bit value as it is */
            } /* else */
            bPosition <<= 0x01; /* set bit position to extract next bit */
          } /* for */
        } /* if */
        else {
          /* default parameter: nothing to process */
          /* copy raw data from received PDO */
          bRetVal = abOutDataDig[bOffset];
        } /* else */

        /* copy return value to output data location */
        abDO_OutputData[bOffset] = bRetVal;    

        /* write data to physical output */
        DO_WriteData(bOffset);
      break;

      case ANA_OUT_16BIT:
        /* analogue output 16-bit: object 6411h */

        /* internal error or STOPPED state? */
        if ((EmcyTable.bErrReg == 0) && (ModuleInfo.bCommState != STOPPED)) {
          /* no device failure and not STOPPED: copy value to output data location */
          awAO_OutputData[bOffset] = (WORD) awOutDataAna[bOffset];
        } /* if */
        else {
          /* device failure or STOPPED state! */
          if (abAO_ErrorMode[bOffset]) {
            /* error mode enabled data value is found in object 6444h */
            /* set error value 16-bit */
            awAO_OutputData[bOffset] =
              (WORD) (adwAO_ErrorValueInteger[bOffset] >> 16);
          } /* if */
          else {
            /* error mode disabled: output last valid data */
          } /* else */
        } /* else */

        /* place further data processing (signal conditioning,...) here */

        /* write data to physical output */
        AO_WriteData(bOffset);
      break;

      default:
        /* no processing for further objects implemented! */
      break;
    } /* switch */
  } /* if */
} /* gIO_EvaluateOutputValues */


/*!
  \brief Initialize physical I/O.

  This function is called by gIO_Init() to initialise the physical I/O used.
  Please put your own code here to adapt to your system requirements.
*/
STATIC void InitIO(void)
{
} /* InitIO */


/*!
  \brief Read digital data from physical input.

  This function is called by gIO_Task() and reads the data from ALL physical
  digital inputs. Please put your own code here to adapt to your system requiremets.
*/
STATIC void DI_ReadData(void)
{
#if DS401_TEST == 1
  BYTE DATA i;

  /* dummy input for testing purposes */
  for (i = 0; i < INPUT_BUFFER_LEN_DI; i++) {
    abDI_RawDataInput[i] = (BYTE) abInRawDig[i];
  } /* for */
#endif /* DS401_TEST == 1 */
} /* DI_ReadData */


/*!
  \brief Read analogue data from physical input.

  This function is called by gIO_Task() and reads the data from ALL physical
  analogue inputs. Please put your own code here to adapt to your system requiremets.
*/
STATIC void AI_ReadData(void)
{
#if DS401_TEST == 1
  BYTE DATA i;

  /* dummy input for testing purposes */
  for (i = 0; i < INPUT_BUFFER_LEN_AI; i++) {
    awAI_RawDataInput[i] = (WORD) awInRawAna[i];
  } /* for */
#endif /* DS401_TEST == 1 */
} /* AI_ReadData */


/*!
  \brief Write digital data to physical output.

  This function is called by gIO_EvaluateOutputValues() and puts the data to the 
  physical output. Please put your own code here to adapt to your system requiremets.
*/
STATIC void DO_WriteData(BYTE bOutDataOffset)
{
  /* put 8-bit digital value abDO_OutputData[bOutDataOffset] to physical output! */
#if DS401_TEST == 1
  /* dummy output for testing purposes */
  VDEBUG_RPT2("DO_OUT8-%.2d = %X\n", (bOutDataOffset + 1),
              abDO_OutputData[bOutDataOffset]);
#endif /* DS401_TEST == 1 */
} /* DO_WriteData */


/*!
  \brief Write analogue data to physical output.

  This function is called by gIO_EvaluateOutputValues() and puts the data to the 
  D/A converter and finally to the physical output.
  Please put your own code here to adapt to your system requiremets.
*/
STATIC void AO_WriteData(BYTE bOutDataOffset)
{
  /* put 16-bit analogue value awAO_OutputData[bOutDataOffset] to D/A converrter */
#if DS401_TEST == 1
  /* dummy output for testing purposes */
  VDEBUG_RPT2("AO_OUT16-%.2d = %X\n", (bOutDataOffset + 1), awAO_OutputData[bOutDataOffset]);
#endif /* DS401_TEST == 1 */
} /* AO_WriteData */


/*!
  \brief Object transmission handler.

  This function can perform special additional handling when a certain object is
  transmitted by SDO (upload) or PDO. Additional code dealing with further 
  objects may be placed here.
*/
void gIO_ObjectTxHandling(WORD wIdx, BYTE bSubIdx)
{
  if (bSubIdx != 0) {
    /* valid subindex */
    switch (wIdx) {
#if OBJECT_6422_USED == 1
      case ANA_IN_INT_SOURCE:
        /* set object 6422h data to zero (see I/O profile DS-401 specification) */
        adwAI_IntSource[(bSubIdx - 1) / 32] = 0;
      break;
#endif /* OBJECT_6422_USED == 1 */

      default:
      break;
    } /* switch */
  } /* if */
} /* gIO_ObjectTxHandling */


# if OBJECT_1029_USED == 1
/*!
  \brief Check occurrence of digital/analogue OUTPUT or INPUT error.

  This function is called only if object 1029h is implemented.
  The function is called if a device profile specific error is detected in the
  Error Register (object 1001h): this is the case if the corresponding bit 
  (bit 5 in EmcyTable.bErrReg) is set, which has to be implemented for the 
  occurrence of any profile specific error in addition to this function.
  The return value of this function represents the error class for the given 
  error situation (i.e. the corresponding subindex of object 1029h). In addition
  to the mandatory subindex "1" for communication errors (which is handled by 
  the stack itself) for the I/O profile DS-401 there are (at least) two more 
  entries possible:
  - 2  for output errors
  - 3  for input errors

  \return corresponding error class for behaviour as specified in Object 1029h
*/
BYTE gIO_CheckError() {
  /* device specific function: put code here for determination of occurred error */
  /* and return appropriate error class applying to object 1029h */

  return 0xFF; /* default: return invalid/unknown error class */
} /* gIO_CheckError */
# endif /* OBJECT_1029_USED == 1 */

#endif /* (PROFILE_DS401 == 1) && (GENERIC_PROFILE_USED == 1) */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief CANopen DS-401 I/O Profile Functionality.
  \par File name ds401.c
  \version 7
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart

  For more information see section \ref ds401page
*/
