/*--------------------------------------------------------------------
       CO_LED.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: CANopen Indicator Specification handling
       (CiA DR-303-3 V1.0)
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "portab.h"
#include "cos_main.h"
#include "target.h"
#include "co_led.h"

#if STATUS_LEDS_USED == 1

/*--------------------------------------------------------------------*/
/*  macros for LED control                                            */
/*--------------------------------------------------------------------*/
#if USE_CANOE_EMU == 1
# define SET_LED(port, bit) CANoeAPI_PutValueInt(port, TRUE)   /*!< turn LED on  */
# define CLR_LED(port, bit) CANoeAPI_PutValueInt(port, FALSE)  /*!< turn LED off */
# define TOGGLE_LED(port, bit) CANoeAPI_PutValueInt(port, ~CANoeAPI_GetValueInt(port) & 0x01)  /*!< toggle LED   */
#else
# if NEGATIVE_LED_LOGIC == 1
#  define SET_LED(port, bit) (port &= ~((WORD) 1 << bit))   /*!< turn LED on  */
#  define CLR_LED(port, bit) (port |=  ((WORD) 1 << bit))   /*!< turn LED off */
#  define CHK_LED_ON(port, bit) ((port & ((WORD) 1 << bit)) == 0)
# else
#  define SET_LED(port, bit) (port |=  ((WORD) 1 << bit))   /*!< turn LED on  */
#  define CLR_LED(port, bit) (port &= ~((WORD) 1 << bit))   /*!< turn LED off */
#  define CHK_LED_ON(port, bit) ((port & ((WORD) 1 << bit)) == 1)
# endif /* NEGATIVE_LED_LOGIC == 1 */
#define TOGGLE_LED(port, bit) (port ^= ((WORD) 1 << bit))  /*!< toggle LED   */
#endif /* USE_CANOE_EMU == 1 */


/*--------------------------------------------------------------------*/
/*  local definitions                                                 */
/*--------------------------------------------------------------------*/
/*! priority and flash sequence paramter for the different LED modes */
BYTE CONST tLedModeParam[NUM_LED_MODES][2] = {
 /* priority,         flash sequence */
  { LED_OFF_PRIO,     LED_OFF_FSEQ     },   /* LED_OFF     */
  { LED_FLASH1_PRIO,  LED_FLASH1_FSEQ  },   /* LED_FLASH1  */
  { LED_FLASH2_PRIO,  LED_FLASH2_FSEQ  },   /* LED_FLASH2  */
  { LED_FLASH3_PRIO,  LED_FLASH3_FSEQ  },   /* LED_FLASH3  */
  { LED_ON_PRIO,      LED_ON_FSEQ      },   /* LED_ON      */
  { LED_BLINK_PRIO,   LED_BLINK_FSEQ   }    /* LED_BLINK   */
};


/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/
/*! holds the managing values of the different LEDs */
STATIC LED_PARAMETER atLedParam[NUM_LEDS];
/*! holds the current non-exclusive states of the ERROR_LED */
STATIC BYTE bErrorLedStates;

#ifdef WIN32
WORD Dummy_Port, Dummy_Dir; /* dummy vars for WIN32 test environment  */
#endif /* WIN32 */


/*--------------------------------------------------------------------*/
/*  public data                                                       */
/*--------------------------------------------------------------------*/
#if LED_FLICKER_MODE_ENABLED == 1
BYTE fFlickerModeEntered; /* global flag for flickering mode */
BYTE abLedOnRestore[NUM_LEDS]; /* restore states for LEDs */
#endif /* LED_FLICKER_MODE_ENABLED == 1 */

/*--------------------------------------------------------------------*/
/*  external functions                                                       */
/*--------------------------------------------------------------------*/
#if USE_CANOE_EMU == 1
void CANoeAPI_PutValueInt   (const char*, int);
void CANoeAPI_PutValueData  (const char*, BYTE*, int);
int  CANoeAPI_GetValueInt   (const char*);
void CANoeAPI_GetValueData  (const char*, BYTE*, int);
#endif /* USE_CANOE_EMU == 1 */

/*--------------------------------------------------------------------*/
/*  public functions                                                  */
/*--------------------------------------------------------------------*/
/*!
  \brief Initialize LED management structures.

  Any target specific initialization regarding LED signaling should be
  done here.
*/
void gLED_Init(void)
{
  BYTE i;

#ifndef WIN32
  /* EXAMPLE: base initialization for Vector CANister */
  DP2 |= 0xFF; /* data direction port 2: output */
  P2  |= 0xFF; /* port 2: clear LEDs (negative logic) */
  DP3 |= 0xF0; /* data direction port 3: output */
  P3  |= 0xF0; /* port 3: clear LEDs (negative logic) */
#endif /* WIN32 */
  /* Initialize used physical I/O ports for LED control. The corresponding
     port bits for the 2 LEDs have to be configured to output. */
  DPR_ERROR_LED |= ((WORD) 1) << ERROR_LED_BIT; /* set ERROR LED port bit to output */
  DPR_RUN_LED   |= ((WORD) 1) << RUN_LED_BIT;   /* set RUN LED port bit to output   */

  /* turn LEDs off */
  CLR_LED(ERROR_LED_PORT, ERROR_LED_BIT);
  CLR_LED(RUN_LED_PORT, RUN_LED_BIT);
  /* reset LED management values */
  for (i = 0; i < NUM_LEDS; i++) {
    atLedParam[i].bToggleTimer = 0;
    atLedParam[i].bToggleReload = 0;
    atLedParam[i].bFlashCounter = 0;
    atLedParam[i].bFlashReload = 0;
    atLedParam[i].bLedOn = FALSE;
    atLedParam[i].bLedMode = LED_OFF_PRIO;
    atLedParam[i].bRun = FALSE;
    atLedParam[i].bOffPhase = FALSE;
#if LED_FLICKER_MODE_ENABLED == 1
    abLedOnRestore[i] = FALSE;
#endif /* LED_FLICKER_MODE_ENABLED == 1 */
  } /* for */

  bErrorLedStates = tLedModeParam[ERROR_LED][0]; /* set ERROR_LED off-state */
#if LED_FLICKER_MODE_ENABLED == 1
  fFlickerModeEntered = FALSE;
#endif /* LED_FLICKER_MODE_ENABLED == 1 */
} /* gLED_Init */

/*!
  \brief Set/Reset a new LED mode.

  The change of the LEDs signaling mode is done by this function.
  For the ERROR_LED the mode is set/reset in bErrorLedStates and
  subsequently the highest prior state currently set is determined.
  For the RUN_LED the handed over (exclusive) state is directly set.
  Then the corresponding settings for the LED/mode combination are performed.

  \param bLedNr - LED number (ERROR/RUN LED)
  \param bMode  - signaling mode of LED
  \param bState - set corresponding mode ON or OFF (only relevant for ERROR_LED)
*/
void gLED_SetLedState(BYTE bLedNr, BYTE bMode, BYTE bState)
{
  BYTE bLocMode = LED_OFF_PRIO;   /* no state set: turn ERROR_LED off */
  BYTE bMask = ERRLED_STATE_HIGH; /* start at highest prior state     */
  BYTE bParamIndex = LED_ON;      /* highest index for ERROR_LED      */

  /* ERROR_LED: set/reset mode */
  if (bLedNr == ERROR_LED) {
    if (bState == ON) {
      bErrorLedStates |= tLedModeParam[bMode][0]; /* set priority bit */
    } /* if */
    else {
      bErrorLedStates &= ~tLedModeParam[bMode][0]; /* reset priority bit */
    } /* else */

    /* determine current highest prior mode */
    while (bMask > 1) {
      if (bErrorLedStates & bMask) {
        /* error state found */
        bLocMode = bMask;
        bMask = 0;
      } /* if */
      else {
        bMask >>= 1;   /* move mask and */
        bParamIndex--; /* index to next position */
      } /* else */
    } /* while */
  }
  else {
    /* set values for RUN_LED */
    bLocMode = tLedModeParam[bMode][0];
    bParamIndex = bMode;
  } /* else */

  /* set mode values */
  /* set mode only if different from actual one */
  if (atLedParam[bLedNr].bLedMode != bLocMode) {
    /* new mode is going to be entered */
    atLedParam[bLedNr].bLedMode = bLocMode; /* store new mode */
    switch (bLocMode) {
      /* OFF/ON mode */
      case LED_OFF_PRIO:
      case LED_ON_PRIO:
        atLedParam[bLedNr].bRun = FALSE; /* stop LED timer */
        if (bLocMode == LED_OFF_PRIO) {
          /* turn LED off */
          atLedParam[bLedNr].bLedOn = FALSE; /* update mode status */
          switch (bLedNr) {
            case ERROR_LED:
              CLR_LED(ERROR_LED_PORT, ERROR_LED_BIT);
            break;
            case RUN_LED:
              CLR_LED(RUN_LED_PORT, RUN_LED_BIT);
            break;
            default:
            break;
          } /* switch */
        } /* if */
        else {
          /* turn LED on */
          atLedParam[bLedNr].bLedOn = TRUE; /* update mode status */
          switch (bLedNr) {
            case ERROR_LED:
              SET_LED(ERROR_LED_PORT, ERROR_LED_BIT);
            break;
            case RUN_LED:
              SET_LED(RUN_LED_PORT, RUN_LED_BIT);
            break;
            default:
            break;
          } /* switch */
        } /* else */
      break;

      /* flashing modes */
      case LED_FLASH1_PRIO:
      case LED_FLASH2_PRIO:
      case LED_FLASH3_PRIO:
        atLedParam[bLedNr].bToggleReload = BLINK_TIME; /* set flash length */
        atLedParam[bLedNr].bFlashReload  = tLedModeParam[bParamIndex][1]; /* set flash sequence */
        /* add one more cycle for first delay (off-phase) at beginning of new mode */
        atLedParam[bLedNr].bFlashCounter = atLedParam[bLedNr].bFlashReload + 1;
        /* switch LED */
        switch (bLedNr) {
          case ERROR_LED:
            if ((atLedParam[RUN_LED].bLedMode == LED_ON_PRIO) || 
                (atLedParam[RUN_LED].bLedMode == LED_OFF_PRIO)) {
              /* RUN_LED ON or OFF */
              atLedParam[bLedNr].bToggleTimer =
                atLedParam[bLedNr].bToggleReload; /* start at next timer cycle */
            }
            else { /* RUN_LED blinking or flashing */
              if (atLedParam[RUN_LED].bOffPhase == TRUE) {
                /* RUN_LED flashing mode - OFF_PHASE */
                if (atLedParam[RUN_LED].bToggleTimer > BLINK_TIME) {
                  /* enough time for one ERROR_LED on-phase */
                  /* before the RUN_LED on-phase */
                  atLedParam[bLedNr].bToggleTimer = 
                    atLedParam[RUN_LED].bToggleTimer - BLINK_TIME;
                }
                else {
                  /* time to short: omission/additional delay necessary */
                  if (atLedParam[bLedNr].bLedMode == LED_FLASH1_PRIO) {
                    /* ERROR_LED single flash mode: wait for start of next period */
                    /* set start time before next RUN_LED on-phase */
                    atLedParam[bLedNr].bToggleTimer = 
                      atLedParam[RUN_LED].bToggleTimer + OFF_PHASE;
                  }
                  else {
                    /* double/triple flash mode */
                    /* omit one ERROR_LED flash?? */
                    atLedParam[bLedNr].bToggleTimer = 
                      atLedParam[RUN_LED].bToggleTimer + BLINK_TIME;
                    atLedParam[bLedNr].bFlashCounter -= 2; /* one flash omitted */
                  } /* else */
                } /* else */
              } /* if */
              else {
                /* RUN_LED in normal BLINK-interval */
                if (atLedParam[RUN_LED].bLedMode == LED_BLINK_PRIO) {
                  /* blinking mode */
                  /* normal interval: go for next toggle-point */
                  if (atLedParam[RUN_LED].bLedOn == TRUE) {
                    /* RUN_LED on: toggle at next timer cycle */
                    atLedParam[bLedNr].bToggleTimer = 
                      atLedParam[RUN_LED].bToggleTimer;
                  }
                  else {
                    /* RUN_LED off: toggle at next but one timer cycle */
                    atLedParam[bLedNr].bToggleTimer = 
                      atLedParam[RUN_LED].bToggleTimer + BLINK_TIME;
                  } /* else */
                }
                else {
                  /* single-flash phase: RUN_LED is on */
                  if (atLedParam[bLedNr].bLedMode == LED_FLASH1_PRIO) {
                    /* ERROR_LED single flash mode: wait for next period */
                    atLedParam[bLedNr].bToggleTimer = OFF_PHASE;
                  }
                  else {
                    /* double/triple flash mode: */
                    /* omit one ERROR_LED flash?? */
                    atLedParam[bLedNr].bToggleTimer = 
                      atLedParam[RUN_LED].bToggleTimer;
                    atLedParam[bLedNr].bFlashCounter -= 2; /* one flash omitted */
                  } /* else */
                } /* else */
              } /* else */
            } /* else */
            /* turn LED off */
            atLedParam[bLedNr].bLedOn = FALSE;
            CLR_LED(ERROR_LED_PORT, ERROR_LED_BIT);
          break;

          case RUN_LED:
            if ((atLedParam[ERROR_LED].bLedMode == LED_ON_PRIO) || 
                (atLedParam[ERROR_LED].bLedMode == LED_OFF_PRIO)) {
              /* ERROR_LED ON or OFF */
              atLedParam[bLedNr].bToggleTimer = 
                atLedParam[bLedNr].bToggleReload; /* start at next timer cycle */
            }
            else {   /* ERROR_LED in flash-mode */
                  /* ERROR_LED flashing - OFF_PHASE */
              if ((atLedParam[ERROR_LED].bOffPhase == TRUE) ||
                  /* ERROR_LED just starting */
                  (atLedParam[ERROR_LED].bFlashCounter == 
                    (atLedParam[ERROR_LED].bFlashReload + 1))) {
                /* start after next ERROR_LED flash */
                atLedParam[bLedNr].bToggleTimer = 
                  atLedParam[ERROR_LED].bToggleTimer + BLINK_TIME;
              }
              else {
                /* ERROR_LED flash sequence entered */
                if (atLedParam[ERROR_LED].bFlashCounter == 
                  atLedParam[ERROR_LED].bFlashReload) {
                  /* ERROR_LED still during first flash */
                  /* start at next timer cycle */
                  atLedParam[bLedNr].bToggleTimer = 
                    atLedParam[ERROR_LED].bToggleTimer;
                } /* else */
                else {
                  /* ERROR_LED flash sequence progressed to far: wait for next period */
                  atLedParam[bLedNr].bToggleTimer = 
                    atLedParam[ERROR_LED].bToggleTimer + 
                    (atLedParam[ERROR_LED].bFlashCounter * BLINK_TIME) +
                    OFF_PHASE;
                } /* else */
              } /* else */
            } /* else */
            /* turn LEDs off */
            atLedParam[bLedNr].bLedOn = FALSE;
            CLR_LED(RUN_LED_PORT, RUN_LED_BIT);
          break;
          default:
          break;
        } /* switch */
        atLedParam[bLedNr].bRun = TRUE; /* start LED timer */
      break;

      /* blinking mode */
      case LED_BLINK_PRIO:
        atLedParam[bLedNr].bToggleReload = BLINK_TIME; /* blinking length */
        atLedParam[bLedNr].bFlashReload = 0; /* no flash sequence */
        atLedParam[bLedNr].bFlashCounter = 0;
        switch (bLedNr) {
          case ERROR_LED:
            /* no blinking mode specified for this LED */
          break;

          case RUN_LED:
            if ((atLedParam[ERROR_LED].bLedMode == LED_ON_PRIO) || 
                (atLedParam[ERROR_LED].bLedMode == LED_OFF_PRIO)) {
              /* ERROR_LED ON or OFF */
              atLedParam[bLedNr].bToggleTimer = 
                atLedParam[bLedNr].bToggleReload; /* start at next timer cycle */
            }
            else { /* ERROR_LED flashing */
              if ((atLedParam[ERROR_LED].bOffPhase == TRUE) ||
                  (atLedParam[ERROR_LED].bLedOn == FALSE)) {
                /* ERROR_LED in OFF_PHASE or flashing phase and off: */
                /* start after next ERROR_LED on-phase               */
                atLedParam[bLedNr].bToggleTimer = 
                  atLedParam[ERROR_LED].bToggleTimer + BLINK_TIME;
              }
              else {
                /* ERROR_LED in flashing phase and on: */
                /* start at next timer cycle           */
                atLedParam[bLedNr].bToggleTimer = 
                  atLedParam[ERROR_LED].bToggleTimer;
              } /* else */
            } /* else */
            /* turn LEDs off */
            atLedParam[bLedNr].bLedOn = FALSE;
            CLR_LED(RUN_LED_PORT, RUN_LED_BIT);
          break;
        } /* switch */
        atLedParam[bLedNr].bRun = TRUE; /* start LED timer */
      break;
    } /* switch */
  } /* if */
} /* gLED_SetLedState */


#if LED_FLICKER_MODE_ENABLED == 1
/*!
  \brief Entering/Leaving the LED flickering mode.

  This function sets or resets the flickering mode (AutoBaud/LSS) of the status 
  LEDs. The LED states are stored/restored and the LED mode parameters are 
  frozen/reactivated.

  \param bState - set mode ON or OFF
*/
void gLED_SetFlickerMode(BYTE bState)
{
  if (bState == ON) {
    /* set flickering mode only if not already active */
    if (fFlickerModeEntered == FALSE) {
      /* save current LED states */
      abLedOnRestore[ERROR_LED] = atLedParam[ERROR_LED].bLedOn;
      abLedOnRestore[RUN_LED]   = atLedParam[RUN_LED].bLedOn;
      /* set initial LED pattern */
      CLR_LED(ERROR_LED_PORT, ERROR_LED_BIT);
      SET_LED(RUN_LED_PORT, RUN_LED_BIT);
      fFlickerModeEntered = TRUE; /* start flickering timer and 
                                     freeze regular modes        */
    } /* if */
  } /* if */
  else {
    /* reset flickering mode only if active */
    if (fFlickerModeEntered == TRUE) {
      /* restore LED states */
      atLedParam[ERROR_LED].bLedOn = abLedOnRestore[ERROR_LED];
      atLedParam[RUN_LED].bLedOn   = abLedOnRestore[RUN_LED];
      /* set original LED pattern */
      if (abLedOnRestore[ERROR_LED] == FALSE) {
        CLR_LED(ERROR_LED_PORT, ERROR_LED_BIT);
      } /* if */
      else {
        SET_LED(ERROR_LED_PORT, ERROR_LED_BIT);
      } /* else */

      if (abLedOnRestore[RUN_LED] == FALSE) {
        CLR_LED(RUN_LED_PORT, RUN_LED_BIT);
      } /* if */
      else {
        SET_LED(RUN_LED_PORT, RUN_LED_BIT);
      } /* else */
      fFlickerModeEntered = FALSE; /* stop flickering timer and 
                                      reactivate regular modes   */
    } /* if */
  } /* else */
}
#endif /* LED_FLICKER_MODE_ENABLED == 1 */

/*!
  \brief Monitoring of the LED timing.

  This function is called periodically from within TimerHandler. It checks the
  timer expiration, reloads the timer and toggles the LEDs.
  In regular mode this function is called with LED_TIMER_INT, in flickering 
  mode with FLICKER_TIME_INT.
*/
void gLED_ChkToggleTime()
{
  BYTE i;

#if LED_FLICKER_MODE_ENABLED == 1
  /* flickering mode activated? */
  if (fFlickerModeEntered == TRUE) {
    /* just toggle both LEDs */
    TOGGLE_LED(ERROR_LED_PORT, ERROR_LED_BIT);
    TOGGLE_LED(RUN_LED_PORT, RUN_LED_BIT);
  }
  else
#endif /* LED_FLICKER_MODE_ENABLED == 1 */
  /* LEDs in regular mode */
  {
    for (i = 0; i < NUM_LEDS; i++) { /* check all LEDs */
      if (atLedParam[i].bRun ) {
        /* LED not in ON or OFF mode */
        if (atLedParam[i].bToggleTimer > 1) {
          /* decrease timer */
          atLedParam[i].bToggleTimer = atLedParam[i].bToggleTimer - 1;
        }
        else {
          /* timer expired */
          if ((atLedParam[i].bFlashReload > 0) && 
              (--atLedParam[i].bFlashCounter == 0)) {
            /* flash sequence mode: sequence finished */
            /* reload flash sequence value and add one cycle for OFF_PHASE */
            atLedParam[i].bFlashCounter = atLedParam[i].bFlashReload + 1;
            atLedParam[i].bOffPhase = TRUE; /* set OFF_PHASE state */
            atLedParam[i].bToggleTimer = OFF_PHASE; /* set OFF_PHASE duration */
            atLedParam[i].bLedOn = FALSE;
            switch (i) {
              /* turn LED off  */
              case ERROR_LED:
                CLR_LED(ERROR_LED_PORT, ERROR_LED_BIT);
              break;
              case RUN_LED:
                CLR_LED(RUN_LED_PORT, RUN_LED_BIT);
              break;                                                          
              default:
              break;
            } /* switch */
          } /* if */
          else {
            /* regular time interval */
            atLedParam[i].bOffPhase = FALSE; /* reset OFF_PHASE state */
            /* reload timer and toggle LED */
            atLedParam[i].bToggleTimer = atLedParam[i].bToggleReload;
            atLedParam[i].bLedOn = (atLedParam[i].bLedOn == TRUE) ? FALSE
                                                                  : TRUE;
            switch (i) {
              /* toggle LED */
              case ERROR_LED:
                TOGGLE_LED(ERROR_LED_PORT, ERROR_LED_BIT);
              break;
              case RUN_LED:
                TOGGLE_LED(RUN_LED_PORT, RUN_LED_BIT);
              break;
              default:
              break;
            } /* switch */
          } /* else */
        } /* else */
      } /* if */
    } /* for */
  } /* else */
} /* gLED_ChkToggleTime */


#endif /* STATUS_LEDS_USED == 1 */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief CANopen Indicator Specification module (CiA DR-303-3)
  \par File name co_led.c
  \version 8
  \date 17.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart

  For more information see section \ref co_ledpage
*/
