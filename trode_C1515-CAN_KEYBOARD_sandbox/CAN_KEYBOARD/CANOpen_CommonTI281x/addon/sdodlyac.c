/*--------------------------------------------------------------------
       SDODLYAC.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: SDO server add on for delayed data access.
  --------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "portab.h"
#include "cos_main.h"
#include <string.h>
#include "cancntrl.h"
#include "objdict.h"
#include "sdoserv.h"
#include "buffer.h"
#include "sdodlyac.h"


#if DELAYED_SDO_ENABLE == 1

/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
/* external from SDOSERV.C */
/* SDO server's management structure */
extern VSdoAttrib XDATA ServerSdo[NUM_SDO_SERVERS];
/* SDO abort codes table */
extern CONST DWORD CODE alSdoAbortCodes[];

/* external from BUFFER.C */
extern FAR CAN_BUF XDATA tCanMsgBuffer[MAX_CAN_BUFFER]; /* CAN message buffer */

/* external from COS_MAIN.C */
/* pointer to data from delayed SDO transfer */
extern BYTE *pbDelayedSdoData[NUM_SDO_SERVERS];



/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/
/* temporary storage location for retrieved data from delayed SDO transfer */
#if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
VDelayedSdoInfo tDelayedSdoBuffer[NUM_SDO_SERVERS];
#endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */

#if EXAMPLE_OBJECTS_USED == 1
/* some variables for sample code */
/* storage locations: sample objects 2100h & 2200h */
BYTE bDelayedTempBuffer[4];   /* temporary data buffer */
BYTE bLocalDelayedData2100[4] = {0x11, 0x22, 0x33, 0x44};
BYTE bLocalDelayedData2200[8] = {0, 0, 0, 0, 0, 0, 0, 0};
WORD wDlySdoUpTimer = 0;      /* timer for simulated delay upload     */
WORD wDlySdoDownTimer = 0;    /* timer for simulated delay download   */
BYTE bDlyUpEvent = 0;         /* data access finished flag upload     */
BYTE bDlyDownEvent = 0;       /* data access finished flag download   */
#endif /* EXAMPLE_OBJECTS_USED == 1 */



/*--------------------------------------------------------------------*/
/*  function definitions                                              */
/*--------------------------------------------------------------------*/

/*!
  \brief Initiate delayed SDO upload data access.

  This routine handles SDO upload data requests for objects that do not
  support direct memory access. Calls to individual interfaces can be placed here.
  The proper return value has to be passed back.
  The corresponding action is device dependent and has to be implemented.

  Usually a request for the specified object (index/subindex) is initiated and
  #DELAY_READ_REQ passed back to the caller. When this request is answered the 
  corresponding action and object information is handled within 
  gSdoS_DelayedTransferStatus().

  For #HANDLE_UNKNOWN_OBJ_IN_APPL==1 the structure tDelayedSdoBuffer[bServIndex]
  has to be initialized in addition (index/subindex). The information can be 
  used in gSdoS_DelayedTransferStatus() and gSdoS_FinishDelayedTransfer.

  If any error situation is recognized immediately the appropriate value
  can be returned directly.

  Index and Subindex are available via ServerSdo[bServIndex].wbIndex.wc and
  ServerSdo[bServIndex].bSubindex.

  \param bServIndex - actual server
  \retval DELAY_READ_REQ - the read request was initiated successfully
  \retval DELAY_ERROR - general error occurred
  \retval OBJ_NOT_EXIST - no indirect data access specified for this object
  \retval NO_SUB_INDEX - the subindex for the requested object is invalid
  \retval WR_ONLY_OBJ - attempt to read a write only object
*/
BYTE gSdoS_DelayedUpload(BYTE bServIndex)
{
#if EXAMPLE_OBJECTS_USED == 1
  /* check index range */
  if ((ServerSdo[bServIndex].wbIndex.wc >= 0x2100) &&
      (ServerSdo[bServIndex].wbIndex.wc <= 0x2200)) {
    switch (ServerSdo[bServIndex].wbIndex.wc) {
      /* example object 2100h */
      case 0x2100:
        /* check for valid subindex */
        if ((ServerSdo[bServIndex].bSubIndex >= 0)
            && (ServerSdo[bServIndex].bSubIndex < 2)) { /* subindices 1 & 2 */
          /* initiate remote data access: this depends on individual interface */
# if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
          tDelayedSdoBuffer[bServIndex].wIndex = ServerSdo[bServIndex].wbIndex.wc;
          tDelayedSdoBuffer[bServIndex].bSubIndex = ServerSdo[bServIndex].bSubIndex;
# endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */

          /* EXAMPLE: simulation! */
          /* simulate delay: further object handling is done in gSdoS_DelayedTransderStatus() */
          wDlySdoUpTimer = SDO_UP_DELAY;
          bDlyUpEvent = 0x80;
          return DELAY_READ_REQ; /* return read request initiated */
        } /* if */
        else {
          /* report error directly for subindices >= 3 */
          return NO_SUB_INDEX; /* subindex not available */
        } /* else */
        break;

      /* example object 2200h */
      case 0x2200:
        /* check for valid subindex */
        if ((ServerSdo[bServIndex].bSubIndex >= 0) && (ServerSdo[bServIndex].bSubIndex < 3)) { /* subindices 0-2 */
          /* initiate remote data access: this depends on individual interface */
# if HANDLE_UNKNOWN_OBJ_IN_APPL == 1          
          tDelayedSdoBuffer[bServIndex].wIndex = ServerSdo[bServIndex].wbIndex.wc;
          tDelayedSdoBuffer[bServIndex].bSubIndex = ServerSdo[bServIndex].bSubIndex;
# endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */

          /* EXAMPLE: simulation! */
          /* simulate delay: further object handling is done in gSdoS_DelayedTransderStatus() */
          wDlySdoUpTimer = SDO_UP_DELAY;
          bDlyUpEvent = 0x80;
          return DELAY_READ_REQ; /* return read request initiated */
        } /* if */
        else {
          /* report error directly for subindices >= 3 */
          return NO_SUB_INDEX; /* subindex not available */
        } /* else */
        break;

      default:
        /* report error directly */
        return OBJ_NOT_EXIST; /* object not available */
        break;
    } /* switch */
  } /* if */
  else
#endif /* EXAMPLE_OBJECTS_USED == 1 */
  {
    return OBJ_NOT_EXIST; /* object not available */
  } /* else */
} /* gSdoS_DelayedUpload() */


/*!
  \brief Initiate delayed SDO download data access.

  This routine handles SDO download data requests for objects that do not 
  support direct memory access. Calls to individual interfaces can be placed here.
  The proper return value has to be passed back.
  The corresponding action is device dependent and has to be implemented.

  Usually a request for the specified object (index/subindex) is initiated and
  #DELAY_WRITE_REQ passed back to the caller. When this request is answered the 
  corresponding action and object information is handled within 
  gSdoS_DelayedTransferStatus().

  For #HANDLE_UNKNOWN_OBJ_IN_APPL==1 the structure tDelayedSdoBuffer[bServIndex]
  has to be initialized in addition (index/subindex/length). The information can
  be used in gSdoS_DelayedTransferStatus() and gSdoS_FinishDelayedTransfer.
  NOTE: delayed SDO access is only possible via expedited transfer (max. 4 data bytes)

  If any error situation is recognized immediately the appropriate value
  can be returned directly.

  Index, Subindex, Length and Datapointer are available via 
  ServerSdo[bServIndex].wbIndex.wc,
  ServerSdo[bServIndex].bSubindex,
  ServerSdo[bServIndex].SDO_DATASIZE and
  ServerSdo[bServIndex].pbData.

  \param bServIndex - actual server
  \retval DELAY_WRITE_REQ - the write request was initiated successfully
  \retval OBJ_NOT_EXIST - no indirect data access specified for this object
  \retval NO_SUB_INDEX - the subindex for the requested object is invalid
  \retval RD_ONLY_OBJ - attempt to write a read only object
  \retval LEN_TOO_HIGH - data type does not match, length of service parameter too high
  \retval LEN_TOO_LOW - data type does not match, length of service parameter too low 
  \retval DELAY_ERROR - general error occurred
*/
BYTE gSdoS_DelayedDownload(BYTE bServIndex)
{
#if EXAMPLE_OBJECTS_USED == 1
  /* check index range */
  if ((ServerSdo[bServIndex].wbIndex.wc >= 0x2100) &&
      (ServerSdo[bServIndex].wbIndex.wc <= 0x2200)) {
    switch (ServerSdo[bServIndex].wbIndex.wc) {
      /* example object 2100h */
      case 0x2100:
# if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
        /* initialize Delayed SDO Buffer */
        tDelayedSdoBuffer[bServIndex].wIndex = ServerSdo[bServIndex].wbIndex.wc;
        tDelayedSdoBuffer[bServIndex].bSubIndex = ServerSdo[bServIndex].bSubIndex;
        tDelayedSdoBuffer[bServIndex].bLength = (BYTE)ServerSdo[bServIndex].SDO_DATASIZE;
        /* copy up to 4 received data bytes */
        MemCpy((void*)tDelayedSdoBuffer[bServIndex].bExpedData,
               (void*)ServerSdo[bServIndex].pbData,
               (BYTE)ServerSdo[bServIndex].SDO_DATASIZE);
# endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */
        /* simulate delay: further object handling is done in gSdoS_DelayedTransderStatus() */
        wDlySdoDownTimer = SDO_DOWN_DELAY; /* example: force delay */
        bDlyDownEvent = 0x80;
        return DELAY_WRITE_REQ; /* return write request initiated */
      break;

      /* example object 2200h */
      case 0x2200:
        /* check for valid subindex */
        if ((ServerSdo[bServIndex].bSubIndex > 0) && (ServerSdo[bServIndex].bSubIndex < 3)) {
          /* initiate remote data access: this depends on individual interface */

# if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
          /* initialize Delayed SDO Buffer */
          tDelayedSdoBuffer[bServIndex].wIndex = ServerSdo[bServIndex].wbIndex.wc;
          tDelayedSdoBuffer[bServIndex].bSubIndex = ServerSdo[bServIndex].bSubIndex;
          tDelayedSdoBuffer[bServIndex].bLength = (BYTE)ServerSdo[bServIndex].SDO_DATASIZE;
          /* copy up to 4 received data bytes */
          MemCpy((void*)tDelayedSdoBuffer[bServIndex].bExpedData,
                 (void*)ServerSdo[bServIndex].pbData,
                 (BYTE) ServerSdo[bServIndex].SDO_DATASIZE);
# endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */
          /* EXAMPLE: simulation! */
          /* simulate delay: further object handling is done in gSdoS_DelayedTransderStatus() */
          wDlySdoDownTimer = SDO_DOWN_DELAY; /* example: force delay */
          bDlyDownEvent = 0x80;
          return DELAY_WRITE_REQ; /* return write request initiated */
        } /* if */
        else {
          /* report error directly */
          if (ServerSdo[bServIndex].bSubIndex == 0) return RD_ONLY_OBJ;
          else return NO_SUB_INDEX; /* subindex not available */
        } /* else */
        break;

      default:
        /* report error directly */
        return OBJ_NOT_EXIST; /* object not available */
        break;
    } /* switch */
  } /* if */
  else
#endif /* EXAMPLE_OBJECTS_USED == 1 */
  {
    return OBJ_NOT_EXIST; /* object not available */
  } /* else */
} /* gSdoS_DelayedDownload() */


/*!
  \brief Check status of outstanding delayed SDO request.

  This routine supplies the current state of an outstanding delayed SDO request.
  If a previously initiated request (upload or download) has finished successfully
  the completion is indicated to the stack
  (ServerSdo[bServIndex].bDelayedState = DELAY_COMPLETE). If the request is 
  still pending the current state is passed back (bDelayedState).

  For SDO-Downloads the data buffer pointer has to be set to NULL (*pBuffer = NULL).

  The -finished- event is device specific and has to be implemented:
  if the object information is available, the structure tDelayedSdoBuffer[bServIndex]
  has to be filled with the retrieved information (Length/ObjInfo).

  tDelayedSdoBuffer[bServIndex].bObjInfo has to be assigned
    DELAY_COMPLETE    if object access was successful
    DELAY_ERROR       if general error occurred
    OBJ_NOT_EXIST     if object index does not exist
    NO_SUB_INDEX      if object subindex does not exist
    WR_ONLY_OBJ       if object is WRITE-ONLY and SDO upload is performed
    RD_ONLY_OBJ       if object is READ-ONLY and SDO download is performed
    LEN_TOO_HIGH      if data type does not match, length of service parameter too high
    LEN_TOO_LOW       if data type does not match, length of service parameter too low 

  tDelayedSdoBuffer[bServIndex].bLength holds the length of the retrieved object.
  NOTE: delayed SDO access is only possible via expedited transfer (max. 4 data bytes)

  \param bServIndex - actual SDO server channel
  \param pBuffer    - pointer to location where address of retrieved data has
                      to be stored (only for UPLOAD)
                      For DOWNLOAD pointer has to be set to NULL

  \retval DELAY_COMPLETE - the previously initiated request has finished
  \retval bDelayedState  - current state (still pending)
*/
BYTE gSdoS_DelayedTransferStatus(BYTE bServIndex, BYTE **pBuffer)
{
#if EXAMPLE_OBJECTS_USED == 1
  /* UPLOAD */
  if (bDlyUpEvent & 0x01) { /* simulated -finished- event for upload*/
    /* EXAMPLE code!!! */
    /* delayed UPLOAD returned */
    
# if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
    switch (tDelayedSdoBuffer[bServIndex].wIndex) {
      case 0x2100: /* object 2100h */
        if (tDelayedSdoBuffer[bServIndex].bSubIndex == 0) { /* nr of subindices */
          /* set simulated values */
          tDelayedSdoBuffer[bServIndex].bLength = 1; /* set object length */
          tDelayedSdoBuffer[bServIndex].bExpedData[0] = 1;
          /* set pointer to data location */
          *pBuffer = (BYTE *)tDelayedSdoBuffer[bServIndex].bExpedData;
        } /* if */
        if (tDelayedSdoBuffer[bServIndex].bSubIndex == 1) { /* subindex 1 */
          /* set simulated values */
          tDelayedSdoBuffer[bServIndex].bLength = 4; /* set object length */
          /* set pointer to data location */
          *pBuffer = (BYTE *)bLocalDelayedData2100;
        } /* if */
        /* signal transfer OK */
        tDelayedSdoBuffer[bServIndex].bObjInfo = DELAY_COMPLETE;
        break;
      
      case 0x2200: /* object 2200h */
        if (tDelayedSdoBuffer[bServIndex].bSubIndex == 0) { /* nr of subindices */
          /* set simulated values */
          tDelayedSdoBuffer[bServIndex].bLength = 1; /* set object length */
          tDelayedSdoBuffer[bServIndex].bExpedData[0] = 2;
          /* set pointer to data location */
          *pBuffer = (BYTE *)tDelayedSdoBuffer[bServIndex].bExpedData;
        } /* if */
        if ((tDelayedSdoBuffer[bServIndex].bSubIndex > 0) &&
            (tDelayedSdoBuffer[bServIndex].bSubIndex < 3)) { /* subindices 1 & 2 */
          /* set simulated values */
          tDelayedSdoBuffer[bServIndex].bLength = 4; /* set object length */
          /* set pointer to data location */
          *pBuffer = (BYTE *)(bLocalDelayedData2200 + ((tDelayedSdoBuffer[bServIndex].bSubIndex - 1) * 4));
        } /* if */
        /* signal transfer OK */
        tDelayedSdoBuffer[bServIndex].bObjInfo = DELAY_COMPLETE;
        break;
    } /* switch */
# endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */

    bDlyUpEvent = 0; /* reset simulated delay */
    /* set new state for return value */
    /* further processing is done in main loop */
    /* retrieving object information finished */
    ServerSdo[bServIndex].bDelayedState = DELAY_COMPLETE;
  } /* if */

  /* DOWNLOAD */
  if (bDlyDownEvent & 0x01) { /* simulated -finished- event for download */
    /* EXAMPLE code!!! */
    /* delayed DOWNLOAD returned */

# if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
    /* object information: assuming OK (set other state if neccesary) */
    switch (tDelayedSdoBuffer[bServIndex].wIndex) {
      case 0x2100: /* object 2100h */
        if (tDelayedSdoBuffer[bServIndex].bSubIndex < 2) {
          /* entry not writable */
          tDelayedSdoBuffer[bServIndex].bObjInfo = RD_ONLY_OBJ;
        } /* if */
        else {
          /* subindex not existent */
          tDelayedSdoBuffer[bServIndex].bObjInfo = NO_SUB_INDEX;
        } /* else */
        break;

      case 0x2200: /* object 2200h */
        if (tDelayedSdoBuffer[bServIndex].bSubIndex == 0) {
          /* entry not writable */
          tDelayedSdoBuffer[bServIndex].bObjInfo = RD_ONLY_OBJ;
        } /* if */
        else if ((tDelayedSdoBuffer[bServIndex].bSubIndex > 0) &&
                 (tDelayedSdoBuffer[bServIndex].bSubIndex < 3)) {
          /* copy received data to simulated storage location */
          MemCpy((void*)(bLocalDelayedData2200 + 
                        ((tDelayedSdoBuffer[bServIndex].bSubIndex - 1) * 4)), 
                 (void*)tDelayedSdoBuffer[bServIndex].bExpedData,
                 tDelayedSdoBuffer[bServIndex].bLength);
        } /* else if */
        else {
          /* subindex not existent */
          tDelayedSdoBuffer[bServIndex].bObjInfo = NO_SUB_INDEX;
        } /* else */
        break;

      default:
        /* object does not exist */
        tDelayedSdoBuffer[bServIndex].bObjInfo = OBJ_NOT_EXIST;
        break;
    } /* switch */
# endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */

    *pBuffer = NULL; /* no data for reply needed: clear pointer */
    bDlyDownEvent = 0; /* reset simulated delay */
    /* set new state for return value */
    /* retrieving object information finished */
    ServerSdo[bServIndex].bDelayedState = DELAY_COMPLETE;
  } /* if */
#endif /* EXAMPLE_OBJECTS_USED == 1 */
  return ServerSdo[bServIndex].bDelayedState; /* return current state */
} /* gSdoS_DelayedTransferStatus() */


/*!
  \brief Completion of delayed SDO transfer.

  This routine completes a previously initiated delayed SDO transfer when the
  data access has finished. The reply message is completed, the server SDO
  information cleared and finally the message is transmitted.
  
  \param bServIndex - actual SDO server channel
  \param pDlyData - pointer to delayed SDO data pointer
*/
void gSdoS_FinishDelayedTransfer(BYTE bServIndex, BYTE **pDlyData)
{
  CAN_MSG XDATA *tTxMsg;

  /* message access */
  tTxMsg = MESSAGE_PTR(ServerSdo[bServIndex].bTxId);

  /* check for valid SDO server channel */
  if (bServIndex < NUM_SDO_SERVERS) {
    /* distinguish between upload and download: */
    /* for download no additional data in reply is needed */
#if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
    if (tDelayedSdoBuffer[bServIndex].bObjInfo != DELAY_COMPLETE) {
      if (tDelayedSdoBuffer[bServIndex].bObjInfo > DELAY_ERROR) {
        gSdoS_CreateAbort(bServIndex, tDelayedSdoBuffer[bServIndex].bObjInfo);
      } /* if */
      else {
        gSdoS_CreateAbort(bServIndex, GEN_INT_PARA);
      } /* else */
    } /* if */
    else
#endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */
    {      
      if (*pDlyData != NULL) {
        /* SDO upload */
        /* object available: create answer according to data length */
#if HANDLE_UNKNOWN_OBJ_IN_APPL == 1
        /* set data length */
        ServerSdo[bServIndex].SDO_DATASIZE = tDelayedSdoBuffer[bServIndex].bLength;
#endif /* HANDLE_UNKNOWN_OBJ_IN_APPL == 1 */
        /* create expedited command specifier */
        tTxMsg->bDb[0] = (BYTE)(SCS_INIT_UPLOAD | 0x03 | 
          (BYTE)((4 - ServerSdo[bServIndex].SDO_DATASIZE) << 2));
        /* Copy the data directly to the message */
        tTxMsg->bDb[4] = (BYTE)(*pDlyData)[0];
        tTxMsg->bDb[5] = (BYTE)(*pDlyData)[1];
        tTxMsg->bDb[6] = (BYTE)(*pDlyData)[2];
        tTxMsg->bDb[7] = (BYTE)(*pDlyData)[3];
      } /* if */
    } /* else */
    /* clear SDO server information */
    gSdoS_ClearServerInfo(bServIndex);
    gCan_SendBuf(ServerSdo[bServIndex].bTxId);
  } /* if */
} /* gSdoS_FinishDelayedTransfer() */


/*!
  \brief Cancel delayed data request.

  This routine cancels a previously initiated delayed data request.
  The corresponding action is device dependent and has to be implemented.

  \param bServIndex - actual SDO server channel
*/
void gSdoS_StopDelayedTransfer(BYTE bServIndex)
{
  if (ServerSdo[bServIndex].bDelayedState != DELAY_IDLE) {
    /* cancel data transaction: interface dependent */
    /* action for different states ... */
    switch (ServerSdo[bServIndex].bDelayedState) {
      case DELAY_READ_REQ:
        /* do something */
      break;

      case DELAY_WRITE_REQ:
        /* do something */
      break;

      default:
        /* do something */
      break;
    } /* switch */
    ServerSdo[bServIndex].bDelayedState = DELAY_IDLE; /* set new state: idle */
  } /* if */
  pbDelayedSdoData[bServIndex] = NULL; /* clear data pointer */
} /* gSdoS_StopDelayedTransfer() */

#endif /* DELAYED_SDO_ENABLE == 1 */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief SDO server add on for delayed data access.
  \par File name sdodlyac.c
  \version 8
  \date 6.03.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart

  Following functions have to be adapted to application requirements:
    -gSdoS_DelayedUpload()
    -gSdoS_DelayedDownload()
    -gSdoS_DelayedTransferStatus()
*/

