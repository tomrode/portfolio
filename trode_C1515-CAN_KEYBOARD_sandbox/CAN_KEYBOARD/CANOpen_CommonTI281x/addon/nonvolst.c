/*--------------------------------------------------------------------
       NONVOLST.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: non volatile storage support.
  --------------------------------------------------------------------*/



/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include "portab.h"
#include "cos_main.h"
#include "nonvolst.h"
#include "vdbgprnt.h"

#ifdef WIN32
# include <stdio.h>
#endif


#if STORE_PARAMETERS_SUPP == 1

# if EMULATE_ON_DISK==1
#  include <stdlib.h>
#  include <string.h>
# endif /* EMULATE_ON_DISK==1 */



/*--------------------------------------------------------------------*/
/*  local definitions                                                 */
/*--------------------------------------------------------------------*/
#define NV_CHUNKSIZE 4 /*!< smallest amount of data to store [1,2,4] */
#define NV_OFFSET 4 /*!< offset to start address of nonvolatile memory */



/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern CONST BYTE CODE gStoreBlkSz;
extern CONST VStoreDesc CODE StoreBlks[];



/*--------------------------------------------------------------------*/
/*  public functions                                                  */
/*--------------------------------------------------------------------*/

/*!
  \brief Calculate a checksum over a memory region.

  This function sums up the contents of a memory region.

  \param pbStart - memory start address
  \param bytes - number of bytes in block
  \return - Checksum.
*/
WORD gCalculateChecksum(BYTE FAR * pbStart, WORD bytes)
{
  WORD i = 0;
  WORD localsum = 0;

  do {
    localsum += pbStart[i];
    i++;
  } while (i < bytes);
  localsum = ~localsum;
  localsum++;

  return localsum;
} /* gCalculateChecksum */


/*!
  \fn gNvDelete(BYTE bSelect)
  \brief Invalidate configuration.

  On write access to #RESTORE_PARAMETERS in the object dictionary
  this function is called. The selected sub-index is transferred.
  The selected parameters have to be erased or invalidated.

  \param bSelect - Selected group of parameters
  \retval TRUE stored parameter group deleted
  \retval FALSE stored parameter group not deleted

  \sa gNvLoad(), gNvSave()
*/

/*!
  \fn gNvLoad(BYTE bSelect, WORD wSize, BYTE * pDest)
  \brief Load parameters from non-volatile memory.

  This function loads the specified parameter block to an address
  in memory.

  \param bSelect - Every parameter block has a unique selection.
  \param wSize - size of parameter block
  \param pDest - destination address

  \retval TRUE parameter block loaded
  \retval FALSE parameter block not loaded

  \sa gNvDelete(), gNvSave()
*/

/*!
  \fn gNvSave(BYTE bSelect, WORD wSize, BYTE * pDest)
  \brief Save parameters in non-volatile memory.

  This function saves the specified parameter block in
  non-volatile memory.

  \param bSelect - Every parameter block has a unique selection.
  \param wSize - size of parameter block
  \param pDest - source address

  \retval TRUE parameter block saved
  \retval FALSE parameter block not saved

  \sa gNvLoad(), gNvDelete()
*/

#if EMULATE_ON_DISK==1

BYTE gNvLoad(BYTE bSelect, WORD wSize, BYTE * pDest)
{
  BYTE i;
  FILE *stream;      /* file pointer */
  int numread;       /* read bytes */
  VStoreAreaHead * checkbuf;   /* buffer for checksum evaluation */
  BYTE rc = TRUE;    /* return code */

  /*
  run through storage block description and determine what is to
  load and what not. */
  for (i = 0; i < gStoreBlkSz; i++) {
    if (StoreBlks[i].bDesc == bSelect) {
      /* Open file in binary mode: */
      if((stream = fopen(StoreBlks[i].filename, "rb")) != NULL) {
        if (fseek(stream, StoreBlks[i].offset, SEEK_SET) == 0) {
          checkbuf = malloc(wSize); /* buffer for checksum comparison */
          if (checkbuf) {
            /* Read image from stream */
            numread = fread((void *)checkbuf,
              sizeof(char),
              wSize,
              stream);
            if (numread != wSize) {
              VDEBUG_RPT3("%d bytes expected/ %d bytes read from %s\n",
                wSize, numread, StoreBlks[i].filename);
              rc = FALSE;
            } /* if */
            else {
              if (checkbuf->wCheckSum !=
                gCalculateChecksum((BYTE FAR *)(checkbuf + 1),
                  (WORD)(wSize - sizeof(VStoreAreaHead)))) {
                VDEBUG_RPT1("Checksum failed for %s\n", StoreBlks[i].filename);
                rc = FALSE;
              } /* if */
              else {
                MemCpy(StoreBlks[i].pHead, checkbuf, wSize);
              } /* else */
            } /* else */
          } /* if */
          else {
            VDEBUG_RPT1("Could not allocate %d bytes\n", wSize);
            rc = FALSE;
          } /* else */
          free(checkbuf);
        } /* if */
        else {
          rc = FALSE;
        } /* else */
        fclose(stream);
      } /* if */
      else {
        rc = FALSE;
      } /* else */
    } /* else */
  } /* for */
  return(rc);
} /* gNvLoad */


BYTE gNvSave(BYTE bSelect, WORD wSize, BYTE * pDest)
{
  BYTE i;
  FILE *stream;
  int numwritten;
  BYTE rc = TRUE; /* return code */

  /*
  run through storage block descritpion and determine what is to
  store and what not. */
  for (i = 0; i < gStoreBlkSz; i++) {
    if (StoreBlks[i].bDesc == bSelect) {
      /* Open file in binary mode: */
      if((stream = fopen(StoreBlks[i].filename, "wb")) != NULL) {
        if (fseek(stream, StoreBlks[i].offset, SEEK_SET) == 0) {
          /* Write image to stream */
          numwritten = fwrite((const void *)StoreBlks[i].pHead,
            sizeof(char),
            wSize,
            stream);
          VDEBUG_RPT2("%d Bytes written to %s\n",
            numwritten, StoreBlks[i].filename);
          if (numwritten != wSize) {
            rc = FALSE;
          } /* if */
        } /* if */
        else {
          rc = FALSE;
        } /* else */
        fclose(stream);
      } /* if */
      else {
        rc = FALSE;
      } /* else */
    } /* else */
  } /* for */
  return(rc);
} /* gNvSave */


BYTE gNvDelete(BYTE bSelect)
{
  BYTE i;
  BYTE rc = TRUE; /* return code */

  for (i = 0; i < gStoreBlkSz; i++) {
    if ((bSelect == PAR_SEL_ALL) || (bSelect == StoreBlks[i].bDesc)) {
      /*
      We write only the storage block's head back to the memory.
      Set "magic" and checksum to 0.
      */
      StoreBlks[i].pHead->wMagic    = 0;
      StoreBlks[i].pHead->wCheckSum = 0;
      if (!gNvSave(StoreBlks[i].bDesc,
        sizeof(VStoreAreaHead),
        (BYTE *)StoreBlks[i].pHead)) {
        rc = FALSE;
      } /* if */
    } /* if */
  } /* for */
  return(rc);
} /* gNvDelete */

# elif EMULATE_FLASH == 1

BYTE gNvLoad(BYTE bSelect, WORD wSize, BYTE * pDest) { return(TRUE); } /* gNvLoad */
BYTE gNvSave(BYTE bSelect, WORD wSize, BYTE * pDest) { return(TRUE); } /* gNvSave */
BYTE gNvDelete(BYTE bSelect) { return(TRUE); } /* gNvDelete */

# elif NV_BLOCK_MODE == 0

WORD sCalculateChecksum(WORD wStart, WORD wSize)
{
  WORD i;                       /* counter */
  WORD k;                       /* counter */
  WORD localsum = 0;
  BYTE rc = TRUE;               /* return code */
#if NV_CHUNKSIZE == 1
  BYTE Chunk;
#endif /* NV_CHUNKSIZE == 1 */
#if NV_CHUNKSIZE == 2
  WORD Chunk;
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
  DWORD Chunk;
#endif /* NV_CHUNKSIZE == 4 */

  i = 0;
  k = wSize;
  while (k > (NV_CHUNKSIZE - 1)) {
    /* read chunk */
    rc = Nvs_ReadChunk(wStart + i + NV_OFFSET, (void *)(&Chunk));
    if (rc == FALSE) return 0;
    i += NV_CHUNKSIZE;
    k -= NV_CHUNKSIZE;
#if NV_CHUNKSIZE == 1
    localsum += Chunk;
#endif /* NV_CHUNKSIZE == 1 */
#if NV_CHUNKSIZE == 2
    localsum += Chunk >> 8;
    localsum += Chunk & 0xFF;
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
    localsum += (WORD)(Chunk >> 24);
    localsum += (WORD)((Chunk >> 16) & 0xFF);
    localsum += (WORD)((Chunk >> 8) & 0xFF);
    localsum += (WORD)(Chunk & 0xFF);
#endif /* NV_CHUNKSIZE == 4 */
  } /* while */

  /* read remaining bytes */
  switch (k) {
    case 0:
      break;
    case 1:
      rc = Nvs_ReadChunk(wStart + i + NV_OFFSET, (void *)(&Chunk));
      if (rc == FALSE) return 0;
#if NV_CHUNKSIZE == 2
# if _BIG_ENDIAN == 1
      localsum += Chunk >> 8;
# else
      localsum += Chunk & 0xFF;
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
      localsum += (WORD)(Chunk >> 24);
# else
      localsum += (WORD)(Chunk & 0xFF);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
      break;
    case 2:
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
      localsum += (WORD)(Chunk >> 24);
      localsum += (WORD)((Chunk >> 16) & 0xFF);
# else
      localsum += (WORD)((Chunk >> 8) & 0xFF);
      localsum += (WORD)(Chunk & 0xFF);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
      break;
    case 3:
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
      localsum += (WORD)(Chunk >> 24);
      localsum += (WORD)((Chunk >> 16) & 0xFF);
      localsum += (WORD)((Chunk >> 8) & 0xFF);
# else
      localsum += (WORD)((Chunk >> 16) & 0xFF);
      localsum += (WORD)((Chunk >> 8) & 0xFF);
      localsum += (WORD)(Chunk & 0xFF);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
      break;
  } /* switch */

  localsum = ~localsum;
  localsum++;

  return localsum;
} /* sCalculateChecksum */


BYTE sNvCpy(BYTE * pDest, WORD wStart, WORD wSize)
{
  WORD i;                       /* counter */
  WORD j;                       /* counter */
  WORD k;                       /* counter */
  BYTE rc = TRUE;               /* return code */
#if NV_CHUNKSIZE == 1
  BYTE Chunk;
#endif /* NV_CHUNKSIZE == 1 */
#if NV_CHUNKSIZE == 2
  WORD Chunk;
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
  DWORD Chunk;
#endif /* NV_CHUNKSIZE == 4 */

  i = 0;
  j = 0;
  k = wSize;
  while (k > (NV_CHUNKSIZE - 1)) {
    /* read chunk */
    rc = Nvs_ReadChunk(wStart + i + NV_OFFSET, (void *)(&Chunk));
    if (rc == FALSE) return rc;
    i += NV_CHUNKSIZE;
    k -= NV_CHUNKSIZE;
#if NV_CHUNKSIZE == 1
    pDest[j++] = Chunk;
#endif /* NV_CHUNKSIZE == 1 */
#if NV_CHUNKSIZE == 2
# if _BIG_ENDIAN == 1
    pDest[j++] = (BYTE)(Chunk >> 8);
    pDest[j++] = (BYTE)(Chunk & 0xFF);
# else
    pDest[j++] = (BYTE)(Chunk & 0xFF);
    pDest[j++] = (BYTE)(Chunk >> 8);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
    pDest[j++] = (BYTE)(Chunk >> 24);
    pDest[j++] = (BYTE)((Chunk >> 16) & 0xFF);
    pDest[j++] = (BYTE)((Chunk >> 8) & 0xFF);
    pDest[j++] = (BYTE)(Chunk & 0xFF);
# else
    pDest[j++] = (BYTE)(Chunk & 0xFF);
    pDest[j++] = (BYTE)((Chunk >> 8) & 0xFF);
    pDest[j++] = (BYTE)((Chunk >> 16) & 0xFF);
    pDest[j++] = (BYTE)(Chunk >> 24);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
  } /* while */

  /* read remaining bytes */
  switch (k) {
    case 0:
      break;
    case 1:
      rc = Nvs_ReadChunk(wStart + i + NV_OFFSET, (void *)(&Chunk));
      if (rc == FALSE) return rc;
#if NV_CHUNKSIZE == 2
# if _BIG_ENDIAN == 1
      pDest[j] = (BYTE)(Chunk >> 8);
# else
      pDest[j] = (BYTE)(Chunk & 0xFF);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
      pDest[j] = (BYTE)(Chunk >> 24);
# else
      pDest[j] = (BYTE)(Chunk & 0xFF);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
      break;
    case 2:
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
      pDest[j++] = (BYTE)(Chunk >> 24);
      pDest[j++] = (BYTE)((Chunk >> 16) & 0xFF);
# else
      pDest[j++] = (BYTE)(Chunk & 0xFF);
      pDest[j++] = (BYTE)((Chunk >> 8) & 0xFF);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
      break;
    case 3:
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
      pDest[j++] = (BYTE)(Chunk >> 24);
      pDest[j++] = (BYTE)((Chunk >> 16) & 0xFF);
      pDest[j++] = (BYTE)((Chunk >> 8) & 0xFF);
# else
      pDest[j++] = (BYTE)(Chunk & 0xFF);
      pDest[j++] = (BYTE)((Chunk >> 8) & 0xFF);
      pDest[j++] = (BYTE)((Chunk >> 16) & 0xFF);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
      break;
  } /* switch */

  return rc;
} /* sNvCpy */


BYTE gNvLoad(BYTE bSelect, WORD wSize)
{
  BYTE i;
  WORD FilledUp;
  WORD wCheckSum;
  BYTE rc = TRUE;    /* return code */
#if NV_CHUNKSIZE == 1
  BYTE Chunk;
#endif /* NV_CHUNKSIZE == 1 */
#if NV_CHUNKSIZE == 2
  WORD Chunk;
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
  DWORD Chunk;
#endif /* NV_CHUNKSIZE == 4 */

  /* loop storage block description */
  for (i = 0; i < gStoreBlkSz; i++) {
    if (i == 0) { /* reset block size */
      FilledUp = 0;
    } /* if */
    else { /* count corrected block size */
      FilledUp += (StoreBlks[i - 1].wLen & (NV_CHUNKSIZE - 1))
                ? StoreBlks[i - 1].wLen + NV_CHUNKSIZE - (StoreBlks[i - 1].wLen & (NV_CHUNKSIZE - 1))
                : StoreBlks[i - 1].wLen;
    } /* else */
    if (StoreBlks[i].bDesc == bSelect) {
#if NV_CHUNKSIZE == 1
      rc = Nvs_ReadChunk(FilledUp + 2 + NV_OFFSET, (void *)(&Chunk));
      if (rc == FALSE) return rc;
# if _BIG_ENDIAN == 1
      wCheckSum = Chunk * 256;
# else
      wCheckSum = Chunk;
# endif /* _BIG_ENDIAN == 1 */
      rc = Nvs_ReadChunk(FilledUp + 3 + NV_OFFSET, (void *)(&Chunk));
      if (rc == FALSE) return rc;
# if _BIG_ENDIAN == 1
      wCheckSum |= Chunk;
# else
      wCheckSum |= Chunk * 256;
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 1 */
#if NV_CHUNKSIZE == 2
      rc = Nvs_ReadChunk(FilledUp + 2 + NV_OFFSET, (void *)(&Chunk));
      if (rc == FALSE) return rc;
      wCheckSum = Chunk;
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
      rc = Nvs_ReadChunk(FilledUp + NV_OFFSET, (void *)(&Chunk));
      if (rc == FALSE) return rc;
# if _BIG_ENDIAN == 1
      wCheckSum = Chunk & 0xFFFF;
# else
      wCheckSum = Chunk >> 16;
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */

      if (wCheckSum !=
          sCalculateChecksum(FilledUp + sizeof(VStoreAreaHead),
          (WORD)(wSize - sizeof(VStoreAreaHead)))) {
        VDEBUG_RPT1("Checksum failed for %s\n", StoreBlks[i].Name);
        rc = FALSE;
      } /* if */
      else {
        rc = sNvCpy((BYTE *)StoreBlks[i].pHead, FilledUp, wSize);
      } /* else */
    } /* if */
  } /* for */
  return rc;
} /* gNvLoad */


BYTE gNvSave(BYTE bSelect, WORD wSize)
{
  BYTE i;                       /* counter */
  WORD j;                       /* counter */
  WORD k;                       /* counter */
  WORD FilledUp;                /* counter */
  BYTE rc = TRUE;               /* return code */
#if NV_CHUNKSIZE == 2
  WORD LastChunk;
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
  DWORD LastChunk;
#endif /* NV_CHUNKSIZE == 4 */

  /* loop storage block description */
  for (i = 0; i < gStoreBlkSz; i++) {
    if (i == 0) { /* reset block size */
      FilledUp = 0;
    } /* if */
    else { /* count corrected block size */
      FilledUp += (StoreBlks[i - 1].wLen & (NV_CHUNKSIZE - 1))
                ? StoreBlks[i - 1].wLen + NV_CHUNKSIZE - (StoreBlks[i - 1].wLen & (NV_CHUNKSIZE - 1))
                : StoreBlks[i - 1].wLen;
    } /* else */
    /* specified block ? */
    if (StoreBlks[i].bDesc == bSelect) {
      j = 0;
      k = wSize;
      while (k > (NV_CHUNKSIZE - 1)) {
        /* store chunk */
        rc = Nvs_WriteChunk(FilledUp + NV_OFFSET, (void *)((BYTE *)StoreBlks[i].pHead + j));
        if (rc == FALSE) return rc;
        j += NV_CHUNKSIZE;
        FilledUp += NV_CHUNKSIZE;
        k -= NV_CHUNKSIZE;
      } /* while */

      /* store remaining bytes */
      switch (k) {
        case 0:
          break;
        case 1:
#if NV_CHUNKSIZE == 2
# if _BIG_ENDIAN == 1
          LastChunk = (WORD)(*((BYTE *)StoreBlks[i].pHead + wSize - 1)) << 8;
# else
          LastChunk = *((BYTE *)StoreBlks[i].pHead + wSize - 1);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 2 */
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
          LastChunk = (DWORD)(*((BYTE *)StoreBlks[i].pHead + wSize - 1)) << 24;
# else
          LastChunk = *((BYTE *)StoreBlks[i].pHead + wSize - 1);
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
          rc = Nvs_WriteChunk(FilledUp + NV_OFFSET, (void *)(&LastChunk));
          break;
        case 2:
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
          LastChunk = (DWORD)(*((WORD *)((BYTE *)StoreBlks[i].pHead + wSize - 2))) << 16;
# else
          LastChunk = *((WORD *)((BYTE *)StoreBlks[i].pHead + wSize - 2));
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
          rc = Nvs_WriteChunk(FilledUp + NV_OFFSET, (void *)(&LastChunk));
          break;
        case 3:
#if NV_CHUNKSIZE == 4
# if _BIG_ENDIAN == 1
          LastChunk = (DWORD)(*((WORD *)((BYTE *)StoreBlks[i].pHead + wSize - 3))) << 16;
          LastChunk |= (DWORD)(*((BYTE *)StoreBlks[i].pHead + wSize - 1)) << 8;
# else
          LastChunk = *((WORD *)((BYTE *)StoreBlks[i].pHead + wSize - 3));
          LastChunk |= (DWORD)(*((BYTE *)StoreBlks[i].pHead + wSize - 1)) << 24;
# endif /* _BIG_ENDIAN == 1 */
#endif /* NV_CHUNKSIZE == 4 */
          rc = Nvs_WriteChunk(FilledUp + NV_OFFSET, (void *)(&LastChunk));
          break;
      } /* switch */
      if (rc == FALSE) return rc;
      VDEBUG_RPT2("%d Bytes written to %s\n", wSize, StoreBlks[i].Name);
    } /* if */
  } /* for */
  return rc;
} /* gNvSave */


BYTE gNvDelete(BYTE bSelect)
{
  BYTE i;
  BYTE rc = TRUE; /* return code */

  for (i = 0; i < gStoreBlkSz; i++) {
    if ((bSelect == PAR_SEL_ALL) || (bSelect == StoreBlks[i].bDesc)) {
      /*
      We write only the storage block's head back to the memory.
      Set "magic" and checksum to 0.
      */
      StoreBlks[i].pHead->wMagic    = 0;
      StoreBlks[i].pHead->wCheckSum = 0;
      if (!gNvSave(StoreBlks[i].bDesc, sizeof(VStoreAreaHead))) {
        rc = FALSE;
      } /* if */
    } /* if */
  } /* for */
  return rc;
} /* gNvDelete */

# endif /* no emulation */


/*!
  \brief Init storage interface
*/
void gNvInit(void) {
  BYTE i;

  for (i = 0; i < gStoreBlkSz; i++) {
    StoreBlks[i].pHead->wBlockLen = StoreBlks[i].wLen;
  } /* for */

  /* add more initialization stuff here */
} /* gNvInit */

/*!
  \brief Store a configuration in non-volatile memory.

  This function is called on an access to object #STORE_PARAMETERS.
  It loops through the storage description block and performs the follwoing
  actions:
  -# call the defined save routine
  -# create the checksum for the defined area
  -# call gNvSave() for the selected area.

  \param bSelect - #nvselection

  \retval TRUE parameter selection stored
  \retval FALSE parameter selection not stored
*/
BYTE gStoreConfiguration(BYTE bSelect)
{
  BYTE i;
  BYTE rc = TRUE; /* return code */

  for (i = 0; i < gStoreBlkSz; i++) {
    if ((bSelect == PAR_SEL_ALL) || (bSelect == StoreBlks[i].bDesc)) {
      /* store block */
      if (StoreBlks[i].store) {
        StoreBlks[i].store(); /* execute block collection function */
      } /* if */
      StoreBlks[i].pHead->wMagic = CONFIG_SAVED;
      StoreBlks[i].pHead->wCheckSum =
        gCalculateChecksum((BYTE FAR *)(StoreBlks[i].pHead + 1),
          (WORD)(StoreBlks[i].pHead->wBlockLen
          - sizeof(VStoreAreaHead)));
      if (gNvSave(
           StoreBlks[i].bDesc,
           StoreBlks[i].pHead->wBlockLen
#if NV_BLOCK_MODE == 1
           ,(BYTE *)StoreBlks[i].pHead
#endif /* NV_BLOCK_MODE == 1 */
         )) {
        VDEBUG_RPT2("gStoreConfiguration(%s): %d bytes stored\n",
        StoreBlks[i].Name, StoreBlks[i].pHead->wBlockLen);
      } /* if */
      else {
        VDEBUG_RPT1("gStoreConfiguration(%s): Data not stored\n",
        StoreBlks[i].Name);
        rc = FALSE;
      } /* else */
    } /* if */
    else {
      /* ignore block */
    } /* else */
  } /* for */

  return rc;
} /* gStoreConfiguration */


/*!
  Apply a stored configuration.

  \retval TRUE configuration applied
  \retval FALSE no configuration applied
*/
BYTE gApplyConfiguration(void)
{
  BYTE i;
  BYTE rc = TRUE; /* return code */

  /*
  run through storage block description and determine what is to
  apply and what not. */
  for (i = 0; i < gStoreBlkSz; i++) {
    /* store block */
    if ((StoreBlks[i].pHead->wMagic == CONFIG_SAVED) && StoreBlks[i].apply) {
      if (StoreBlks[i].apply()) {
        VDEBUG_RPT2("gApplyConfiguration(%s): %d bytes applied\n",
          StoreBlks[i].Name, StoreBlks[i].pHead->wBlockLen);
      } /* if */
      else {
        VDEBUG_RPT1("gApplyConfiguration(%s): Data not applied\n",
        StoreBlks[i].Name);
      } /* else */
    } /* if */
  } /* for */

  return rc;

} /* gApplyConfiguration */



/*!
  \brief Get a stored configuration.

  This function gets the stored information and copies it to the
  appropriate data locations.

  \retval TRUE configuration loaded
  \retval FALSE no configuration found  
*/
BYTE gLoadConfiguration(void)
{
  BYTE i;
  BYTE rc = TRUE; /* return code */

  /*
  run through storage block description and determine what is to
  load and what not. */
  for (i = 0; i < gStoreBlkSz; i++) {
    if (gNvLoad(
         StoreBlks[i].bDesc,
         StoreBlks[i].pHead->wBlockLen
#if NV_BLOCK_MODE == 1
         ,(BYTE *)StoreBlks[i].pHead
#endif /* NV_BLOCK_MODE == 1 */
       )) {
      VDEBUG_RPT2("gLoadConfiguration(%s): %d bytes loaded\n",
      StoreBlks[i].Name, StoreBlks[i].pHead->wBlockLen);
    } /* if */
    else {
      VDEBUG_RPT1("gLoadConfiguration(%s): Data not loaded\n",
      StoreBlks[i].Name);
      rc = FALSE;
    } /* else */
  } /* for */

  return rc;
} /* gLoadConfiguration */


#endif /* STORE_PARAMETERS_SUPP == 1 */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Non volatile storage support for CANopen slave.
  \par File name nonvolst.c
  \version 23
  \date 8.06.04
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
