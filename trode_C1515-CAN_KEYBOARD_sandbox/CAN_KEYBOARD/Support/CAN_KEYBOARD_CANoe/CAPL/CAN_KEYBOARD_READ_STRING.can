/*@!Encoding:1252*/
/******************************************************************************************************************************
*                                             CAN_KEYBOARD_READ_STRING.can 
*                                                                               Author: Thomas Rode
*                                                                               Company: Crown
*                                                                               Date: 27 Jan 2015
*
*  Intent: Script used as a test for CAN Keyboard to see output strings, Alt characters, Function keys.   
*          
*
***************************************************************************************************************************/
variables
{  
  /** - Boolean state*/
  const TRUE                  = (1);
  const FALSE                 = (0);
    
  
  /** - Size of string array  */
  const STRING_COUNT           = (100);
 
  /** - Alternate keys */ 
  const ALT_ARROW_LSB         = (0x12);
  const ALT_ARROW_UP          = (0x01);
  const ALT_ARROW_DOWN        = (0x02);
  const ALT_ARROW_LEFT        = (0x03);
  const ALT_ARROW_RIGHT       = (0x04);
  
  /** - Function keys */
  const FN_11_LSB             = (0x11);
  const FN1_11                = (0x01);
  const FN2_11                = (0x02);
  const FN3_11                = (0x03);
  const FN4_11                = (0x04);
  const FN5_11                = (0x05);
  const FN6_11                = (0x06);
  const FN7_11                = (0x07);
  const FN8_11                = (0x08);
  const FN9_11                = (0x09);
  const FN0_11                = (0x00);
  const FN_12_LSB             = (0x12);
  const FN_12_PGUP            = (0x11);
  const FN_12_PGDN            = (0x12);
  const FN_12_HOME            = (0x13);
  const FN_12_END             = (0x14);
  
  /** - File write section */
   const MAX_QUESIZE   = (200);
   const MAX_SERIALNUM = (35);
   const MAX_PARTNUM   = (10);
   const MAX_FILENAME  = (35);
  
 /** - Multi dim array if need befor output files*/ 
   char KeysTyped[MAX_QUESIZE][STRING_COUNT];
  
  /** - CAN message declaration */  
  message 0x000 NMT            = {DLC=2};
  message 0x1f6 CAN_KBD_RES    = {DLC=7};
  message 0x276 CAN_KBD_REQ    = {DLC=1};
  message 0x776 CAN_KBD_NMT    = {DLC=1};
  
  /** - Timer section */
  mstimer KBD_REQ_TIMER;
  mstimer NMT_TIMER;
   
  /** - Variable section */
 
  char StringArr[STRING_COUNT];
  char WriteBtnCntAr[26];
  char HardWareTestAr[100] = {
                               0,1,2,3,4,5,6,7,8,9,10,11,12,
                               13,14,15,16,17,18,19,20,21,22,
                               23,24,25,26,27,28,29,30,31,32,
                               33,34,35,36,37,38,39,40,41,42,
                               43,44,45,46,47,48,49,50,51,52,
                               53,54,55
                             };
  byte LangSel;
  byte MonitorOnly;
  byte WriteBtn;
  byte WriteBtnCnt=0;
  byte ClearButtonFlag;
  int LangSelRes;
  int RxPDOCntSysVarOut;
 
  enum
  {
     ENGLISH = 1,
     GERMAN = 2,
     SPANISH = 3, 
     LANG4 = 4,
     LANG5 = 5,
     LANG6 = 6,
     LANG7 = 7,
     LANG8 = 8,
     LANG9 = 9, 
     SWITCH_TEST = 0xFF
  }Languages_t;
  
  enum
  {
     BOOTUP = 0,
     STOPPED = 4,
     OPERATIONAL = 5,
     PRE_OPERATIONAL = 127
  }NMT_StateCodes_t;
  
  enum
  {
     CONTINIOUS    = 1,
     ENTER_KEY     = 2,
     STRING        = 3,
     HARDWARE_TEST = 4
    
  }MONITOR_MODE;
}

/** - Event Procedure section */
on start
{
  write(" Please select environment: 'b' bench simulation, 't' on real truck \n");
  write(" Please select language if using bench simulation: '1' = english, '2' = german, '3' = spanish\n");
  write(" Please select the monitor mode: 'c' = Continious Monitor Mode");
  write("                                 'r' = Enter Key Monitor Mode ");
  write("                                 's' = String output Continiously");
  write("                                 'h' = Hardware Switch Test Continiously \n");
   /** -  System variable language selected initializer */ 
  SysSetVariableString(sysvar::CANKBD::MonAr, "Monitor Mode Off");
  SysSetVariableString(sysvar::CANKBD::LangAr, "No Language selected Selected"); 
  /** - Message reception count out to panel*/
  SysSetVariableInt(sysvar::CANKBD::RxPDOCnt,RxPDOCntSysVarOut);
   /** -  System variable language selected*/ 
  SysSetVariableString(sysvar::CANKBD::NMT, "Inactive");
  
  /** - Set Write button off initially*/
  WriteBtn = 0;
  SysSetVariableInt(sysvar::CANKBD::WriteBtn, WriteBtn);
  
  /** -  System variable Environment selected*/ 
  SysSetVariableString(sysvar::CANKBD::Env, "No Environment Selected"); 
  
   /** Reset Current Raw value display in sysvar*/
  SysSetVariableInt(sysvar::CANKBD::RawByte0,0);
  SysSetVariableInt(sysvar::CANKBD::RawByte1,0);
  SysSetVariableInt(sysvar::CANKBD::RawByte2,0);
  SysSetVariableInt(sysvar::CANKBD::RawByte3,0);
  SysSetVariableInt(sysvar::CANKBD::RawByte4,0);
  SysSetVariableInt(sysvar::CANKBD::RawByte5,0);
  SysSetVariableInt(sysvar::CANKBD::RawByte6,0);
  
  /** Reset Last Raw value display in sysvar*/
  SysSetVariableInt(sysvar::CANKBD::LastRawByte0,0);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte1,0);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte2,0);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte3,0);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte4,0);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte5,0);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte6,0);
  
  
}
on timer NMT_TIMER
{
   /** - Output NMT Start */
   NMT.byte(0) = 0x01;
   NMT.byte(1) = 0x76;
   output(NMT);
  
   /** - start up language selection message */  
   Settimer(KBD_REQ_TIMER, 1);
}
on timer KBD_REQ_TIMER
{
   /** -Send out the message */
   CAN_KBD_REQ.byte(0) = LangSel;
   output(CAN_KBD_REQ);
  
   /** - Re-send every 200 ms*/
   settimer(KBD_REQ_TIMER, 200);
}
on key '1'
{
  /** - English key strike */
  LangSel = ENGLISH; 
}

on key '2'
{
   /** - German key strike */
  LangSel =  GERMAN;
}

on key '3'
{
   /** - Spanish key strike*/
  LangSel = SPANISH; 
}

on key '4'
{
   /** - Just out of range*/
  LangSel = 0x04; 
}
on key '5'
{
   /** - Just out of range*/
  LangSel = 0x05;
}
on key '6'
{
   /** - Just out of range*/
  LangSel = 0x06; 
}
on key '7'
{
   /** - Just out of range*/
  LangSel = 0x07;
}
on key '8'
{
   /** - Just out of range*/
  LangSel = 0x08; 
}

on key '9'
{
   /** - Out of Range strike*/
  LangSel = 0x09; 
}
on key '0'
{
   /** - Out of Range strike*/
  LangSel = 0x00; 
}

on key 'h'
{
  
  /** - set monitor mode flag*/
  MonitorOnly = HARDWARE_TEST;
  
  /** - Hardware test strike*/
  LangSel = 0xFF; 
}

on key 'b'
{
   /**- Make NMT start after 500 ms */
   settimer(NMT_TIMER,500);
  
   /** -  System variable Environment selected*/ 
   SysSetVariableString(sysvar::CANKBD::Env, "Bench Simulation Selected"); 
}

on key 'c'
{
  /** - set monitor mode flag*/
  MonitorOnly = CONTINIOUS;
  
  /** - Output to the write window*/
  write("Continious Monitor Mode Selected");
  
   /** -  System variable language selected*/ 
  SysSetVariableString(sysvar::CANKBD::MonAr, "Continious Monitor Mode");
  
}

on key 'r'
{
  /** - set monitor mode flag*/
  MonitorOnly = ENTER_KEY;
  
   /** - Output to the write window*/
  write("Enter Key Monitor Mode, type on keyboard then hit Enter to see result");
  
   /** -  System variable language selected*/ 
  SysSetVariableString(sysvar::CANKBD::MonAr, "Enter Key Monitor Mode");
  
}

on key 's'
{
  /** - set monitor mode flag*/
  MonitorOnly = STRING;
  
   /** - Output to the write window*/
  write("String Continious Monitor Mode, type on keyboard");
  
   /** -  System variable language selected*/ 
  SysSetVariableString(sysvar::CANKBD::MonAr, "String Continious Monitor Mode");
  
}
on key 't'
{
  /** - Kill all silualation on a real truck*/
  canceltimer( KBD_REQ_TIMER);
  canceltimer( NMT_TIMER); 
  
   /** -  System variable Environment selected*/ 
   SysSetVariableString(sysvar::CANKBD::Env, "Truck Simulation Selected"); 
}
on message 0x776 
{
 byte Nmt; 
 CAN_KBD_NMT = this;
 
 if(CAN_KBD_NMT.byte(0) == BOOTUP)
 {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::NMT, "NMT Bootup");
 }
 else if(CAN_KBD_NMT.byte(0) == STOPPED)
 {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::NMT, "NMT Stopped");
 }
 else if(CAN_KBD_NMT.byte(0) == OPERATIONAL)
 {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::NMT, "NMT Operational");
 }
 else if(CAN_KBD_NMT.byte(0) == PRE_OPERATIONAL)
 {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::NMT, "NMT Pre Operational");
 }
  
}
  
  
on message 0x276
{
  int LangSelRes;
  
  LangSelRes = CAN_KBD_REQ.byte(0);
  
  
  if(LangSelRes == ENGLISH)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "English Selected");
    
  }
  else if(LangSelRes == GERMAN)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "German Selected");
    
  }
  else if(LangSelRes == SPANISH)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "Spanish Selected");
     
  }
  else if(LangSelRes == LANG4)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "Fourth language Selected");    
  }
  else if(LangSelRes == LANG5)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "Fifth language Selected");    
  }
  else if(LangSelRes == LANG6)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "Sixth language Selected");    
  }
  else if(LangSelRes == LANG7)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "Seventh language Selected");    
  }
  else if(LangSelRes == LANG8)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "Eigth language Selected");    
  }
  else if(LangSelRes == LANG9)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "Nineth language Selected");    
  }
  else if(LangSelRes == SWITCH_TEST)
  {
     /** -  System variable language selected*/ 
     SysSetVariableString(sysvar::CANKBD::LangAr, "Hardware Test Mode Selected");    
  }
  
  
}

/** - This is the real response from KBD */ 
on message 0x1f6
{
  byte CurrentRawByte[7]; 
  byte LastRawByte[7];
  byte RawByte[7];
  byte i;
  byte ii;
  byte Exit;
  int RxPDOCntSysVarOut;
 
  CAN_KBD_RES = this;
   
  /** - Message reception count out to panel*/
  SysSetVariableInt(sysvar::CANKBD::RxPDOCnt,RxPDOCntSysVarOut++);
  
      
 
   
  if (MonitorOnly == STRING)
  {
      MonitorOnly = STRING;
       /** - Is there a value on the CAN message to display*/
       if(CAN_KBD_RES.byte(1) > 0)
       { 
        
         do
          {
               /** - Make sure string buffer not too large or a ClearStringBtn SysVar hit*/       
               if((i >= STRING_COUNT) || (ClearButtonFlag == 1))
               {    
                   /** - string size exceeded*/ 
                   if(i >= STRING_COUNT )
                   {
                       write("String Too Big \n"); 
                   }
                
                   for(i=0; i < STRING_COUNT; i++)
                   {
                       /** - Clear out string buffer*/ 
                       StringArr[i] = 0;     
                   }
                   /** - Clear index*/
                   i = 0;    
              
                   /** Clear out button flag*/
                   ClearButtonFlag = 0;
                  
                   /** - Do Not Exit the loop */
                   Exit = FALSE;
                } 
               else  
               {  
                   /** - Place character into sting buffer from byte 1 */ 
                   StringArr[i] = CAN_KBD_RES.byte(1);
                
                   /** - Back space not character hit*/
                   if(CAN_KBD_RES.byte(1) != 0x08) 
                   {             
                      /** - Increment the index*/
                      i++; 
                   }
                   else
                   {
                       /** - Back space character hit*/
                       if (StringArr[0] == 0x08)
                       {
                          /** - reset i inside for StringArr*/
                          i = 0;      
                       }
                       else
                       {                    
                          /** Decrement i*/
                          i--; 
                     
                          /** - Delete character*/
                          StringArr[i+1] = 0x20;                    
                       }
                   } 
                   /** - Output hit string if in Range*/
                   if( i < STRING_COUNT)
                   { 
                       /** - Output hit string */
                       write("The Character hit in string mode: %s \n", StringArr);   
                   }
                 
                   /** - Exit the loop */
                   Exit = TRUE;
               } // end else   
          
           } while( FALSE == Exit ); 
   
      } // end if(CAN_KBD_RES.byte(1) > 0)   
          
  }
  
  else if (MonitorOnly == CONTINIOUS)
  {
    
        /** - Place character into sting buffer from byte 1 */ 
        StringArr[i] = CAN_KBD_RES.byte(1);
       
        /** - Output the Alt Character here*/
        write("The Character hit Continious mode: %s ", StringArr);
      
    
        for(i=0; i < STRING_COUNT; i++)
        {
           /** - Clear out string buffer*/ 
           StringArr[i] = 0; 
        }
        /** - Clear index*/
        i = 0;
    
  }
  
  else if (MonitorOnly == HARDWARE_TEST)
  {
       byte indx;
    
        /** - Place character into sting buffer from byte 1 */ 
        StringArr[i] = CAN_KBD_RES.byte(1);
        indx =  CAN_KBD_RES.byte(1);
    
        indx = HardWareTestAr[indx];
    
        /** - Output the Alt Character here*/
        write("The Character hit Hardware Test mode: %d ", indx);
      
        
       
    
        for(i=0; i < STRING_COUNT; i++)
        {
           /** - Clear out string buffer*/ 
           StringArr[i] = 0; 
          
           /** - Clear out Hardware test*/
           // HardWareTestAr[i] = 0;
        }
        /** - Clear index*/
        i = 0;
    

    
  }
  
  
  else if(MonitorOnly == ENTER_KEY)
  {  
    /** - This condition when the Enter button hit*/
    if( (CAN_KBD_RES.byte(0) == 0x01) && (CAN_KBD_RES.byte(1) == 0x0D) )
    { 
        /** - Output the string here*/
        write("The string here in enter key mode: %s ", StringArr);
        for(i=0; i < STRING_COUNT; i++)
        {
           /** - Clear out string buffer*/ 
           StringArr[i] = 0; 
        }
        /** - Clear index*/
        i = 0;
    }
    /** - This condition when ascii character, space bar, comma, period   
                                              /**  - Space bar */             /** - Period */                    /** - Comma */                
    else if((CAN_KBD_RES.byte(0) == 0x01) && (CAN_KBD_RES.byte(1) == 0x20) || (CAN_KBD_RES.byte(1) == 0x2E) || (CAN_KBD_RES.byte(1) == 0x2C) || 
            /** - ASCII 0 - 9 */    
           ((CAN_KBD_RES.byte(1) >= 0x30) && (CAN_KBD_RES.byte(1) <= 0x39)) || 
             /** - ASCII A - Z */                                             /** ASCII a - z */
           ((CAN_KBD_RES.byte(1) >= 'A') && (CAN_KBD_RES.byte(1) <= 'Z')) || ((CAN_KBD_RES.byte(1) >= 'a') && (CAN_KBD_RES.byte(1) <= 'z')) )
    {
         /** - Place character into sting buffer from byte 1 */ 
         StringArr[i] = CAN_KBD_RES.byte(1); 
       
         /** - Increment index into StringArr */
         if (i < STRING_COUNT)
         {  
             i++;
         }
    }
     /** - These next 29 conditions for Alt function  keys*/
    else if((CAN_KBD_RES.byte(0) == 0x01) && ((CAN_KBD_RES.byte(1) == 0x21) || (CAN_KBD_RES.byte(1) == 0x40) || (CAN_KBD_RES.byte(1) == 0x23) ||
                                              (CAN_KBD_RES.byte(1) == 0x24) || (CAN_KBD_RES.byte(1) == 0x25) || (CAN_KBD_RES.byte(1) == 0x5E) ||
                                              (CAN_KBD_RES.byte(1) == 0x26) || (CAN_KBD_RES.byte(1) == 0x2A) || (CAN_KBD_RES.byte(1) == 0x28) ||
                                              (CAN_KBD_RES.byte(1) == 0x29) || (CAN_KBD_RES.byte(1) == 0x5F) || (CAN_KBD_RES.byte(1) == 0x7E) ||
                                              (CAN_KBD_RES.byte(1) == 0x3B) || (CAN_KBD_RES.byte(1) == 0x5B) || (CAN_KBD_RES.byte(1) == 0x5D) ||
                                              (CAN_KBD_RES.byte(1) == 0x5C) || (CAN_KBD_RES.byte(1) == 0x7B) || (CAN_KBD_RES.byte(1) == 0x3A) ||
                                              (CAN_KBD_RES.byte(1) == 0x22) || (CAN_KBD_RES.byte(1) == 0x27) || (CAN_KBD_RES.byte(1) == 0x2F) ||
                                              (CAN_KBD_RES.byte(1) == 0x2D) || (CAN_KBD_RES.byte(1) == 0x3C) || (CAN_KBD_RES.byte(1) == 0x3E) ||
                                              (CAN_KBD_RES.byte(1) == 0x3F)) )
    {
        /** - Place character into sting buffer from byte 1 */ 
         StringArr[i] = CAN_KBD_RES.byte(1);
       
         /** - Output the Alt Character here*/
         write("The Alt Character: %s ", StringArr);
     
      
        for(i=0; i < STRING_COUNT; i++)
        {
           /** - Clear out string buffer*/ 
           StringArr[i] = 0; 
        }
        /** - Clear index*/
        i = 0;
         
        
    }
    /** - These next 4 conditions for Alt function arrow keys*/
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == ALT_ARROW_UP) && (CAN_KBD_RES.byte(2) == ALT_ARROW_LSB))
    {
      write(" Alt Up arrow pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == ALT_ARROW_DOWN) && (CAN_KBD_RES.byte(2) == ALT_ARROW_LSB))
    {
      write(" Alt Down arrow pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == ALT_ARROW_LEFT) && (CAN_KBD_RES.byte(2) == ALT_ARROW_LSB))
    {
      write(" Alt Left arrow pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == ALT_ARROW_RIGHT) && (CAN_KBD_RES.byte(2) == ALT_ARROW_LSB))
    {
      write(" Alt Right arrow pressed");  
    }
    /** - These next 14 conditions for FN function keys*/
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN1_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-1  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN2_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-2  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN3_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-3  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN4_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-4  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN5_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-5  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN6_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-6  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN7_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-7  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN8_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-8  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN9_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-9  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x01) && (CAN_KBD_RES.byte(1) == FN0_11) && (CAN_KBD_RES.byte(2) == FN_11_LSB))
    {
      write(" Fnct-0  pressed");  
    }
    /** - These next 4 conditions for FN page function keys*/
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN_12_PGUP) && (CAN_KBD_RES.byte(2) == FN_12_LSB))
    {
      write(" Fnct Page Up  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN_12_PGDN) && (CAN_KBD_RES.byte(2) == FN_12_LSB))
    {
      write(" Fnct Page Down  pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN_12_HOME) && (CAN_KBD_RES.byte(2) == FN_12_LSB))
    {
      write(" Fnct HOME pressed");  
    }
    else if((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == FN_12_END) && (CAN_KBD_RES.byte(2) == FN_12_LSB))
    {
      write(" Fnct END pressed");  
    }
    /** - Misc functions*/
    else if ((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == 0x21) && (CAN_KBD_RES.byte(2) == 0x12))
    {
      write("Screen pressed");  
    }
    else if ((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == 0x97) && (CAN_KBD_RES.byte(2) == 0xC3))
    {
      write(" x Multiply pressed");  
    }
    else if ((CAN_KBD_RES.byte(0) == 0x02) && (CAN_KBD_RES.byte(1) == 0xB7) && (CAN_KBD_RES.byte(2) == 0xC3))
    {
      write(" / Divide pressed");  
    } 
    else if ((CAN_KBD_RES.byte(0) == 0x01) && (CAN_KBD_RES.byte(1) == 0x1B))
    {
      write(" Escape pressed");  
    } 
     else if ((CAN_KBD_RES.byte(0) == 0x01) && (CAN_KBD_RES.byte(1) == 0x08))
    {
      write(" Backspace pressed");  
    } 
    else
    {}
  } 
   /** - Update sysvariable for Raw message reading Had to do this the long way was not working with just 'i'*/
  for(ii=0; ii<7;ii++)
  {  
     RawByte[ii] = CAN_KBD_RES.byte(ii);
    /** - Byte 0*/
    if(( RawByte[0] > 0) && (CurrentRawByte[0] !=  LastRawByte[0]) )
    {
       LastRawByte[0] = RawByte[0];
    }
    else
    {  
       /** Update the status*/
       CurrentRawByte[0] =  RawByte[0];
    }
    /** - Byte 1 LSB of UTF-8*/
    if(( RawByte[1] > 0) && (CurrentRawByte[1] !=  LastRawByte[1]) )
    {
       LastRawByte[1] = RawByte[1];
    }
    else
    {  
       /** Update the status*/
       CurrentRawByte[1] =  RawByte[1];
    }
    /** - Byte 2 */
    if(( RawByte[2] > 0) &&  (CurrentRawByte[2] !=  LastRawByte[2]))
    {
       LastRawByte[2] = RawByte[2];
    }
    else if(LastRawByte[0] < 2)
    {
        LastRawByte[2] = 0;
    }
    else  
    {  
       /** Update the status*/
      CurrentRawByte[2] =  RawByte[2];
      
    }
    /** - Byte 3*/
    if(( RawByte[3] > 0) &&  (CurrentRawByte[3] !=  LastRawByte[3]))
    {
       LastRawByte[3] = RawByte[3];
    }
    else if(LastRawByte[0] < 3)
    {
        LastRawByte[3] = 0;
    }
    else
    {  
       /** Update the status*/
      CurrentRawByte[3] =  RawByte[3];
    }
     /** - Byte 4*/
    if(( RawByte[4] > 0) &&  (CurrentRawByte[4] !=  LastRawByte[4]))
    {
       LastRawByte[4] = RawByte[4];
    }
    else if(LastRawByte[0] < 4)
    {
        LastRawByte[4] = 0;
    }
    else
    {  
       /** Update the status*/
      CurrentRawByte[4] =  RawByte[4];
    }
    /** - Byte 5 Cnt+Alt+Fn Reset*/
    if(( RawByte[5] > 0) &&  (CurrentRawByte[5] !=  LastRawByte[5]))
    {
       LastRawByte[5] = RawByte[5];
    }
    else
    {  
       /** Update the status*/
      LastRawByte[5] = RawByte[5];
      //CurrentRawByte[5] =  RawByte[5];
    }
     /** - Byte 6*/
    if(( RawByte[6] > 0) &&  (CurrentRawByte[6] !=  LastRawByte[6]))
    {
       LastRawByte[6] = RawByte[6];
    }
    else
    {  
       /** Update the status*/
      CurrentRawByte[6] =  RawByte[6];
    }
       CurrentRawByte[0] =  RawByte[0];
       CurrentRawByte[1] =  RawByte[1];
       CurrentRawByte[2] =  RawByte[2];
       CurrentRawByte[3] =  RawByte[3];
       CurrentRawByte[4] =  RawByte[4];
       CurrentRawByte[5] =  RawByte[5];
       CurrentRawByte[6] =  RawByte[6];
     //CurrentRawByte[ii] =  RawByte[ii];
  } 
  /** Capture Current Raw value display in sysvar*/
  SysSetVariableInt(sysvar::CANKBD::RawByte0,RawByte[0]);
  SysSetVariableInt(sysvar::CANKBD::RawByte1,RawByte[1]);
  SysSetVariableInt(sysvar::CANKBD::RawByte2,RawByte[2]);
  SysSetVariableInt(sysvar::CANKBD::RawByte3,RawByte[3]);
  SysSetVariableInt(sysvar::CANKBD::RawByte4,RawByte[4]);
  SysSetVariableInt(sysvar::CANKBD::RawByte5,RawByte[5]);
  SysSetVariableInt(sysvar::CANKBD::RawByte6,RawByte[6]);
  
  /** Capture Last Raw value display in sysvar*/
  SysSetVariableInt(sysvar::CANKBD::LastRawByte0,LastRawByte[0]);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte1,LastRawByte[1]);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte2,LastRawByte[2]);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte3,LastRawByte[3]);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte4,LastRawByte[4]);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte5,LastRawByte[5]);
  SysSetVariableInt(sysvar::CANKBD::LastRawByte6,LastRawByte[6]);
     
}

on key 'w'
{
  Store_ECU_Info();
}
on sysvar CANKBD::WriteBtn
{
   /** - Set Write button off initially*/
 byte BtnFlg; 
    
  if( (SysGetVariableInt(sysvar::CANKBD::WriteBtn) == 1) && ( BtnFlg == 0) )
  {
     /** - Set flag true*/
     BtnFlg = 1;
    
     /** - Write txt file*/
     Store_ECU_Info();
     
     /** - increment the file name*/
      WriteBtnCnt++;

  }
  if( (SysGetVariableInt(sysvar::CANKBD::WriteBtn) == 0) && ( BtnFlg == 1) )
  {  
    BtnFlg = 0;
  }

}

on sysvar CANKBD::ClearStringBtn
{
    byte i;
  
  
    if( SysGetVariableInt(sysvar::CANKBD::ClearStringBtn) == 1)
    {
      Write(" String Cleared Button hit \n "); 
      
       /** - indicate globally that a clear button request occured*/
       ClearButtonFlag = 1;
       
    }
  
}

void Store_ECU_Info(void)
{
  
     /* The file name in .txt file*/
    byte i;
    byte LangChangeToEngCnt;
    byte LangChangeToGerCnt;
    byte LangChangeToSpaCnt;
    char FileName[MAX_FILENAME] = "CAN_KEYBOARD"; 
    char FileNameLangEng[MAX_FILENAME] = "English";
    char FileNameLangGer[MAX_FILENAME] = "German";
    char FileNameLangSpa[MAX_FILENAME] = "Spanish";
    char File_Name[MAX_FILENAME];
    char TempString[STRING_COUNT];
    char Num_to_String[5];
    dword FileHandler;
    int index;
	  int que_indx;
    int count;
  
                /* ABSOLUTE PATH THIS CAN BE A PROBLEM */
     //setWritePath("C:\\Documents and Settings\\Rodet\\Desktop\\HSM_CSWM_MSM\\DCX_FOLDER_CSWM\\CUSW\\PPF_E2B_1118_BCAN\\PPF_E2B_1118_BCAN\\output"); 
     // setWritePath("d:\\users\\F53368A\\Desktop\\CANMainDemos\\log");

    strncpy(File_Name, "", MAX_FILENAME );    // YES it worked, this function copies source to des and insures a terminating zero
   
    index=0;
  
    /** - This is the file name pre-fix "CAN_KEYBOARD"*/
    for (count=0;count< MAX_FILENAME ;count++)   
    {  
         TempString[count] = FileName[count];  
    }
    
    /** - Add on top the language into file name*/
    if(LangSel == ENGLISH)
    {
       /** - Make sure it writes all suffixes correct*/ 
       for(i=0;i<=1;i++)
       { 
          if( LangChangeToEngCnt == FALSE)
          {  
              /** place underscore here*/
              TempString[12] = 0x5F; 
      
              /** Starting point after underscore*/
              index = 13;
              for(count=0; count < 7; count++)
              {
                 /** place english into the suffix of Root name*/
                 TempString[index] = FileNameLangEng[count];
                 index++;
              }
      
              /** place underscore here*/
              TempString[index] = 0x5F; 
      
              /** place lower case letter at end of suffix upto 26 counts */
              if( WriteBtnCnt < elcount(WriteBtnCntAr))
              {  
                 TempString[index+1] = (0x61 + WriteBtnCnt);
              }
              
              /** Interlock the english count while allwing the others */
              LangChangeToEngCnt = FALSE;
              LangChangeToGerCnt = TRUE;
              LangChangeToSpaCnt = TRUE;
           
          }
          else
          {
             /** Reset the count */
             WriteBtnCnt = 0;
        
            /** - Remain in state which increase counts to temp string*/  
            LangChangeToEngCnt = FALSE; 
          }  
      } 
    } 
    
    
    /** - Add on top the language into file name*/
    else if(LangSel == GERMAN)
    { 
     /** - Make sure it writes all suffixes correct*/ 
     for(i=0;i<=1;i++)
     { 
        if( LangChangeToGerCnt == FALSE)
        {  
            /** place underscore here*/
            TempString[12] = 0x5F; 
      
            /** Starting point after underscore*/
            index = 13;
      
            for(count=0; count < 6; count++)
            { 
               /** place english into the suffix of Root name*/
               TempString[index] = FileNameLangGer[count];
               index++;
            }
          
            /** place underscore here*/
            TempString[index] = 0x5F; 
      
           /** place lower case letter at end of suffix upto 26 counts*/
           if( WriteBtnCnt < elcount(WriteBtnCntAr))
           {  
               TempString[index+1] = (0x61 + WriteBtnCnt);
           }  
           /** Interlock the German count while allowing the others */
           LangChangeToEngCnt = TRUE;
           LangChangeToGerCnt = FALSE;
           LangChangeToSpaCnt = TRUE;
       }
       else
       {
           /** Reset the count */
           WriteBtnCnt = 0;
        
           /** - Remain in state which increase counts to temp string*/  
           LangChangeToGerCnt = FALSE; 
       }
     } 
                
    }
     /** - Add on top the language into file name*/
    else if(LangSel == SPANISH)
    {
     /** - Make sure it writes all suffixes correct*/ 
     for(i=0;i<=1;i++)
     {
       if( LangChangeToSpaCnt == FALSE)
       { 
      
           /** place underscore here*/
           TempString[12] = 0x5F; 
      
           /** Starting point after underscore*/
           index = 13;
      
           for(count=0; count < 7; count++)
           {
              /** place "spanish" into the suffix of Root name*/
              TempString[index] = FileNameLangSpa[count];
              index++;       
           } 
       
           /** place underscore here*/
           TempString[index] = 0x5F; 
      
           /** place lower case letter at end of suffix upto 26 counts*/
           if( WriteBtnCnt < elcount(WriteBtnCntAr))
           {  
              TempString[index+1] = (0x61 + WriteBtnCnt);
           } 
            /** Interlock the German count while allowing the others */
           LangChangeToEngCnt = TRUE;
           LangChangeToGerCnt = TRUE;
           LangChangeToSpaCnt = FALSE;
          
       }
       else
       {
           /** Reset the count */
           WriteBtnCnt = 0;
        
           /** - Remain in state which increase counts to temp string*/  
           LangChangeToSpaCnt = FALSE; 
        
       }
     }  
    }
    
    
    
    /* Below used to name file in "".TXT*/
    if (strncmp(TempString, "", MAX_SERIALNUM) != 0)
    {
        strncat(File_Name, TempString, 255 );
        strncat(File_Name, ".txt", 255 );
    }
    else
    {
        strncat(File_Name, "Unknown_ECU.txt", 255 );
    }
     
    /* This is the last step it has opened the file named ##############.txt*/ 
    FileHandler = OpenFileWrite(File_Name,0);
       
    
    writeline("Typed Strings for project",  TempString, FileHandler);
	  writeline("*******************", "***************************", FileHandler);   /* Add a blank space beteween entries*/
    
    for (count=0; count < STRING_COUNT; count++)   
    {
        /** - Clear out temp string for next line to write*/
        TempString[count] = 0;
    }
    
    for (count=0; count < STRING_COUNT; count++)   
    {    
          /**  - This is what was typed */
          TempString[count] = StringArr[count]; 
    }
    
    /** - Write current line */
    writeline("Current String ", TempString, FileHandler);    
 
    /** - Close file*/ 
    FileClose(FileHandler); 
    
    /** - Indicate to button off*/
    WriteBtn = 0;
    SysSetVariableInt(sysvar::CANKBD::WriteBtn, WriteBtn);
    
    
}

writeline (char FieldTitle[], char FieldValue[], dword FileHandler)
{
    char TempLine[255];
    byte num_of_elements;

    strncpy(TempLine, FieldTitle, 255);
    strncat(TempLine, ": ", 255);
    
//    num_of_elements = strlen(TempLine);
//    TempLine[num_of_elements - 1] = 0x09; // Tab

    if ( strncmp(FieldValue, "", 255) != 0)
    {
        strncat(TempLine, FieldValue, 255);
    }
    else
    {
        strncat(TempLine, "(Unknown)", 255);
    }

    strncat(TempLine, "\n", 255);

    filePutString(TempLine, elcount(TempLine), FileHandler);
     
}
