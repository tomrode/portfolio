/*
// TI File $Revision:: 7    $
//###########################################################################
//
// FILE:	F281x_App.cmd
//
// TITLE:	Linker Command File For F281x Application Example
//
//###########################################################################
//
//  Ver | dd mmm yyyy | Who  | Description of changes
// =====|=============|======|===============================================
//  1.00| 10 Dec 2003 | TI   | TMS Release
//###########################################################################
*/

/* Define the memory block start/length for the F2812  
   PAGE 0 will be used to organize program sections
   PAGE 1 will be used to organize data sections

   Notes: 
         Memory blocks on F2812 are uniform (ie same
         physical memory) in both PAGE 0 and PAGE 1.  
         That is the same memory region should not be
         defined for both PAGE 0 and PAGE 1.
         Doing so will result in corruption of program 
         and/or data. 
*/

MEMORY
{
PAGE 0:    /* Program Memory */
           /* Memory (RAM/FLASH/OTP) blocks can be moved to PAGE1 for data allocation */
   RAML0L1     : origin = 0x008000, length = 0x000A00     /* on-chip RAM block L0 to L2*/
   OTP         : origin = 0x3D7800, length = 0x000400     /* on-chip OTP */

   PARAMFLAG   : origin = 0x3E8000, length = 0x000001     /* flag for parameters changed 0x0000 is factory default, 0x1234 is revised */
   FLASHH      : origin = 0x3E8001, length = 0x001FFF     /* on-chip FLASH -  Parameter storage was 0x3E8001*/
   BEGIN       : origin = 0x3EA000, length = 0x000002     /* Part of FLASHG.  Used for Application start from Boot  */
   APP         : origin = 0x3EA002, length = 0x00BDFD     /* on-chip Flash B - G */
   TRADEMARK   : origin = 0x3F5E00, length = 0x00001F	  /* File Information Block for application */
   PARTNUM     : origin = 0x3F5E20, length = 0x00002F
   FILLMAP     : origin = 0x3F5E50, length = 0x00000F
   MEMMAP      : origin = 0x3F5E60, length = 0x000050
   COMPATABLE  : origin = 0x3F5EB0, length = 0x000140
   APPCRC      : origin = 0x3F5FF0, length = 0x000001
   APPCS       : origin = 0x3F5FF2, length = 0x000001

   CSM_RSVD    : origin = 0x3F7F80, length = 0x000076     /* Part of FLASHA.  Program with all 0x0000 when CSM is in use. */
   CSM_PWL     : origin = 0x3F7FF8, length = 0x000008     /* Part of FLASHA.  CSM password locations in FLASHA */

   IQTABLES    : origin = 0x3FE000, length = 0x000B50     /* IQ Math Tables in Boot ROM */
   IQTABLES2   : origin = 0x3FEB50, length = 0x00008C     /* IQ Math Tables in Boot ROM */
   IQTABLES3   : origin = 0x3FEBDC, length = 0x0000AA	  /* IQ Math Tables in Boot ROM */

   ROM         : origin = 0x3FF000, length = 0x000FC0     /* Boot ROM available if MP/MCn=0 */
   RESET       : origin = 0x3FFFC0, length = 0x000002     /* part of boot ROM (MP/MCn=0) or XINTF zone 7 (MP/MCn=1) */
   VECTORS     : origin = 0x3FFFC2, length = 0x00003E     /* part of boot ROM (MP/MCn=0) or XINTF zone 7 (MP/MCn=1) */

PAGE 1 :   /* Data Memory */
           /* Memory (RAM/FLASH/OTP) blocks can be moved to PAGE0 for program allocation */
           /* Registers remain on PAGE1                                                  */

   BOOT_RSVD   : origin = 0x000000, length = 0x000050     /* Part of M0, BOOT rom will use this for stack */
   RAMM0       : origin = 0x000050, length = 0x0003B0     /* on-chip RAM block M0 */
   RAMM1       : origin = 0x000400, length = 0x000400     /* on-chip RAM block M1 */
   RAML2L3     : origin = 0x008A00, length = 0x001600     /* on-chip RAM block L1 */

}

/* Allocate sections to memory blocks.
   Note:
         codestart user defined section in DSP28_CodeStartBranch.asm used to redirect code 
                   execution when booting to flash
         ramfuncs  user defined section to store functions that will be copied from Flash into RAM
*/ 
 
SECTIONS
{

   
   /* Allocate program areas: */
   .cinit              : > APP      PAGE = 0
   .pinit              : > APP,     PAGE = 0
   .text               : > APP      PAGE = 0

   /* User Defined Sections   */
   codestart           : > BEGIN       PAGE = 0
   ramfuncs            : LOAD = APP, 
                         RUN = RAML0L1, 
                         LOAD_START(_RamfuncsLoadStart),
                         LOAD_END(_RamfuncsLoadEnd),
                         RUN_START(_RamfuncsRunStart),
                         PAGE = 0
   parameterflag	   : > PARAMFLAG	PAGE = 0
   parameters		   : > FLASHH 		PAGE = 0
   
   /* define the information block for the application */
   trademark           : > TRADEMARK
   partnumber          : > PARTNUM
   fillmap             : > FILLMAP
   memorymap           : > MEMMAP
   compatableboards    : > COMPATABLE
   appcrc              : > APPCRC
   appcs               : > APPCS
   
   /* Allocate uninitalized data sections: */
   .stack              : > RAMM0      PAGE = 1
   .ebss               : > RAML2L3    PAGE = 1
   .esysmem            : > RAML2L3    PAGE = 1
   .cio		       : > RAMM0      PAGE = 1
   .sysmem  	       : > RAML2L3    PAGE = 1

   /* Initalized sections go in Flash */
   /* Needs to be on page 0 for SDFlash to program it */
   .econst             : > APP      PAGE = 0      
   .switch             : > APP      PAGE = 0      

   /* Allocate IQ math areas: */

   IQmathTables        : > IQTABLES         PAGE = 0, TYPE = NOLOAD   /* Math Tables In ROM */

   /*==========================================================*/
   /* IQ math functions:                                       */
   /*==========================================================*/
   IQmath        :  > APP
   {
			IQmath.lib<IQ19div.obj> (IQmath)
   }
   IQmathTables2    : > IQTABLES2, PAGE = 0, TYPE = NOLOAD
   /*
   {

              IQmath.lib<IQNexpTable.obj> (IQmathTablesRam)

   }
   */
    /* Uncomment the section below if calling the IQNasin() or IQasin()
       functions from the IQMath.lib library in order to utilize the
       relevant IQ Math table in Boot ROM (This saves space and Boot ROM
       is 1 wait-state). If this section is not uncommented, IQmathTables2
       will be loaded into other memory (SARAM, Flash, etc.) and will take
       up space, but 0 wait-state is possible.
    */
    
    IQmathTables3    : > IQTABLES3, PAGE = 0, TYPE = NOLOAD
    /*
    {

               IQmath.lib<IQNasinTable.obj> (IQmathTablesRam)

    }
    */
   /* .reset is a standard section used by the compiler.  It contains the */ 
   /* the address of the start of _c_int00 for C Code.   /*
   /* When using the boot ROM this section and the CPU vector */
   /* table is not needed.  Thus the default type is set here to  */
   /* DSECT  */ 
   .reset              : > RESET,      PAGE = 0, TYPE = DSECT
   vectors             : > VECTORS     PAGE = 0, TYPE = DSECT

}
