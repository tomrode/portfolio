/************************************************************************************************
 * File:       PdoChk.h
 *
 * Purpose:    Defines the PDO check interface
 *
 * Project:    C1101
 *
 * Copyright:  2006 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 05/21/2004
 *
 ************************************************************************************************
*/

#include "PdoChkUser.h"

#ifndef  pdochk_registration

   /*******************************************************************************
    *                                                                             *
    *  G l o b a l   S c o p e   S e c t i o n                                    *
    *                                                                             *
    *******************************************************************************/

   // Defines
   typedef struct
   {
      boolean fFirstPdoSeen;
      ubyte   ubConcernCount;
      ubyte   ubConcernCountMax;
   }PDO_CHK;

#endif

#ifdef PDOCHK_MODULE

   #ifndef pdochk_registration_local
   #define pdochk_registration_local 1

   /*******************************************************************************
    *                                                                             *
    *  I n t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Defines

   // Function prototypes

   // Global variables

   // Macros

   #endif


#else
   #ifndef pdochk_registration
   #define pdochk_registration 1
   /*******************************************************************************
    *                                                                             *
    *  E x t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Externaly visable global variables

   // Externally visable function prototypes
   extern void    vPdoTimeoutInit(PDO_CHK *pPdo, uword uwUpdateRateMs, uword uwTimeoutMs);
   extern boolean fPdoTimeout(PDO_CHK *pPdo, uword uwPdoBit, uword uwPdoStatus, boolean fDatalog);

   #endif
#endif

