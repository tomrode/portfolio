//****************************************************************************************
// File:       char_buffer.h
//
// Purpose:    Definition file for a FIFO, circular char buffer. It provides methods to 
//             allow one module to erase or append data to the buffer and another module 
//             to peek at or extract data from the buffer, encapsulating direct accesses
//             to the actual array. 
//             Note that the buffer is statically sized, and the interface functions 
//             provide proper checking for overwriting.
//
// Project:    InfoLink (C1153)
//
// Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
//
// Author:     Max Peelman
//
// Revision History:
//          
//      11/15/2004 Max Peelman: 
//        -created document
//
//      01/28/2005 Max Peelman: 
//        -changed this interface so that the actual buffer array is now located external
//          to this module, so that CHAR_BUFFER_TYPE contains a pointer to the array 
//          rather than the actual array - this allows for better reuse, because each 
//          module can specify how big its buffer should be
//     
//      02/01/2005 Max Peelman:
//        -changed ubCBuf_ExtractBytes() to uwCBuf_ExtractBytes(), and modified the
//          source file to properly implement the return value as defined here
//
//****************************************************************************************

// if using Keil (C166) then we must #include "main.h" for many primitive data types
#ifdef _KL_C166
    #include "main.h"
#endif
#include "data_types.h"


//*************************************************************************
//                                                                        *
//    G l o b a l   S c o p e   S e c t i o n                             *
//                                                                        *
//  (This data is useful to all source files)                             *
//*************************************************************************
#ifndef char_buffer_global_scope
#define char_buffer_global_scope 1

    //-------------------------
    // Defines
    //-------------------------

        // The Char Buffer object
        // NOTE: manipulation of the members of this struct by sources using this module
        //       is meant to occur through the use of functions and NOT by direct
        //       manipulation!
        // In other words, if you're just using this module, don't reference the members
        // directly...you should be able to find a global function that suits your needs. 
        typedef struct
        {
            // next array element for appending data
            uword uwAppendElement;
            // next array element for extracting data
            uword uwExtractElement;
            // number of bytes currently used for data
            uword uwLen;
            // pointer to the actual buffer
            ubyte* pubBuf;
            // size of the buffer in bytes
            uword uwBufSize;
        } CHAR_BUFFER_TYPE;

    //-------------------------
    // Global variables
    //-------------------------

    //-------------------------
    // Macros
    //-------------------------
        

#endif  // Global Scope Section


//*************************************************************************
//                                                                        *
//  E x t e r n a l   S c o p e   S e c t i o n                           *
//                                                                        *
//  (This is for source files using this module, not implementing it)     *
//*************************************************************************
#ifndef CHAR_BUFFER_SOURCE
    #ifndef char_buffer_external_scope
    #define char_buffer_external_scope 1
      
    //-------------------------
    // Global variables
    //-------------------------
        
    //-------------------------
    // Global Function Prototypes
    //-------------------------

        //*************************************************************************
        //  Function:   vCBuf_Init                                                          
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Initializes the buffer object, done only at program startup.
        //    NOTE: This MUST be called before any other operations on the buffer,
        //          since it provides the link between the buffer object and the
        //          actual array to use for holding the data!
        //                                                                        
        //  Calling Sequence:
        //
        //      void vCBuf_Init (CHAR_BUFFER_TYPE* pcbBuf,
        //                       ubyte* pubBufArray,
        //                       uword uwBufSize)        
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //          pubBufArray is the array the buffer should use
        //          uwBufLen is the size of the buffer array
        //
        //       returns: 
        //          nothing                                                                        
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern void vCBuf_Init (CHAR_BUFFER_TYPE* pcbBuf,
                                ubyte* pubBufArray,
                                uword uwBufSize);

        //*************************************************************************
        //  Function:   fCBuf_AppendByte                                                          
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Appends a single byte to the buffer, with error checking. 
        //                                                                        
        //  Calling Sequence:
        //                               
        //      boolean fCBuf_AppendByte (CHAR_BUFFER_TYPE* pcbBuf,
        //                                ubyte ubVal)
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //          ubVal is the data to add to the buffer
        //       
        //       returns: 
        //          success (TRUE) or buffer full (FALSE) 
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern boolean fCBuf_AppendByte (CHAR_BUFFER_TYPE* pcbBuf,
                                         ubyte ubVal);

        //*************************************************************************
        //  Function:   fCBuf_AppendBytes                                                          
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Appends one or more bytes to the buffer, with error checking. 
        //                                                                        
        //  Calling Sequence:
        //
        //      boolean fCBuf_AppendBytes (CHAR_BUFFER_TYPE* pcbBuf,
        //                                 ubyte *pubData,
        //                                 uword uwNumBytes)
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //          pubData points to the data to add to the buffer
        //          uwNumBytes contains the number of bytes to add
        //       
        //       returns: 
        //          success (TRUE) or not enough room in buffer (FALSE)
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern boolean fCBuf_AppendBytes (CHAR_BUFFER_TYPE* pcbBuf,
                                          ubyte *pubData,
                                          uword uwNumBytes);

        //*************************************************************************
        //  Function:   fCBuf_PeekByte                                                          
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Retrieves, without removing, a single byte value from anywhere in the
        //  buffer.
        //                                                                        
        //  Calling Sequence:
        //
        //      boolean fCBuf_PeekByte (CHAR_BUFFER_TYPE* pcbBuf,
        //                              uword uwIndex,
        //                              ubyte *pubVal)
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //          uwIndex is the byte index into the buffer
        //          pubVal points to storage for the data value
        //       
        //       returns: 
        //          success (TRUE)
        //          error (FALSE) for index out-of-range; index must be between
        //            0 and ( uwCBuf_Len() - 1 )
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern boolean fCBuf_PeekByte (CHAR_BUFFER_TYPE* pcbBuf,
                                       uword uwIndex,
                                       ubyte *pubVal);

        //*************************************************************************
        //  Function:   fCBuf_ExtractByte                                                          
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Extracts a single byte from the buffer, with error checking. 
        //                                                                        
        //  Calling Sequence:
        //
        //      boolean fCBuf_ExtractByte (CHAR_BUFFER_TYPE* pcbBuf,
        //                                 ubyte *pubVal)
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //          pubVal points to storage for the data value
        //       
        //       returns: 
        //          success (TRUE) or buffer empty (FALSE)                                                                        
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern boolean fCBuf_ExtractByte (CHAR_BUFFER_TYPE* pcbBuf,
                                          ubyte *pubVal);

        //*************************************************************************
        //  Function:   uwCBuf_ExtractBytes                                                          
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Extracts one or more bytes from the buffer, with error checking. 
        //                                                                        
        //  Calling Sequence:
        //
        //      uword uwCBuf_ExtractBytes (CHAR_BUFFER_TYPE* pcbBuf,
        //                                 ubyte *pubData,
        //                                 uword uwNumBytes)
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //          pubData points to storage for the extracted data
        //          uwNumBytes contains the number of bytes to extract
        //       
        //       returns: 
        //          success -> same value as uwNumBytes
        //          less bytes than requested -> 0 to (uwNumBytes - 1)
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern uword uwCBuf_ExtractBytes (CHAR_BUFFER_TYPE* pcbBuf,
                                          ubyte *pubData,
                                          uword uwNumBytes);

        //*************************************************************************
        //  Function:   uwCBuf_Len                                                          
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Returns current number of bytes in the buffer. 
        //                                                                        
        //  Calling Sequence:
        //
        //      uword uwCBuf_Len (CHAR_BUFFER_TYPE* pcbBuf)
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //       
        //       returns: 
        //          number of bytes in the buffer
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern uword uwCBuf_Len (CHAR_BUFFER_TYPE* pcbBuf);

        //*************************************************************************
        //  Function:   vCBuf_Erase                                                          
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Erases the buffer. 
        //                                                                        
        //  Calling Sequence:
        //
        //      void vCBuf_Erase (CHAR_BUFFER_TYPE* pcbBuf)        
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //
        //       returns: 
        //          nothing                                                                        
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern void vCBuf_Erase (CHAR_BUFFER_TYPE* pcbBuf);
    
        //*************************************************************************
        //  Function:   fCBuf_IsEmpty                                                          
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Answers the question, "Is the buffer empty?" 
        //                                                                        
        //  Calling Sequence:
        //
        //      boolean fCBuf_IsEmpty (CHAR_BUFFER_TYPE* pcbBuf)        
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //
        //       returns: 
        //          TRUE if buffer is empty, FALSE otherwise                                                   
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern boolean fCBuf_IsEmpty (CHAR_BUFFER_TYPE* pcbBuf);

        //*************************************************************************
        //  Function:   fCBuf_IsFull
        //                                                                        
        //  Author:     Max Peelman                                               
        //                                                                        
        //  Description:                                                          
        //    Answers the question, "Is the buffer full?" 
        //                                                                        
        //  Calling Sequence:
        //
        //      boolean fCBuf_IsFull (CHAR_BUFFER_TYPE* pcbBuf)        
        //       
        //       where:
        //          pcbBuf is the buffer we want to work with
        //
        //       returns: 
        //          TRUE if buffer is full, FALSE otherwise                                                   
        //                                                                        
        //  Called by:      Any modules using the CharBuffer
        //
        //  Hardware I/O:   None
        //
        //  Assumptions/Limitations:
        //    None
        //                                                                        
        //*************************************************************************
        extern boolean fCBuf_IsFull (CHAR_BUFFER_TYPE* pcbBuf);

    #endif
#endif  // External Scope Section

//*************************************************************************
//                                                                        *
//  I n t e r n a l   S c o p e   S e c t i o n                           *
//                                                                        *
//  (This is for the implementing source file(s) only!)                   *
//*************************************************************************
#ifdef CHAR_BUFFER_SOURCE
    #ifndef char_buffer_internal_scope
    #define char_buffer_internal_scope 1

    //-------------------------
    // Defines
    //-------------------------

    //-------------------------
    // Internal variables
    //-------------------------

    //-------------------------
    // Function prototypes   
    //-------------------------

        // Global functions
        //   -these are also defined above, so refer there for comment blocks...
        void vCBuf_Init (CHAR_BUFFER_TYPE* pcbBuf,
                         ubyte* pubBufArray,
                         uword uwBufSize);
        boolean fCBuf_AppendByte (CHAR_BUFFER_TYPE* pcbBuf,
                                  ubyte ubVal);
        boolean fCBuf_AppendBytes (CHAR_BUFFER_TYPE* pcbBuf,
                                   ubyte *pubData,
                                   uword uwNumBytes);
        boolean fCBuf_PeekByte (CHAR_BUFFER_TYPE* pcbBuf,
                                uword uwIndex,
                                ubyte *pubVal);
        boolean fCBuf_ExtractByte (CHAR_BUFFER_TYPE* pcbBuf,
                                   ubyte *pubVal);
        uword uwCBuf_ExtractBytes (CHAR_BUFFER_TYPE* pcbBuf,
                                   ubyte *pubData,
                                   uword uwNumBytes);
        uword uwCBuf_Len (CHAR_BUFFER_TYPE* pcbBuf);
        void vCBuf_Erase (CHAR_BUFFER_TYPE* pcbBuf);
        boolean fCBuf_IsEmpty (CHAR_BUFFER_TYPE* pcbBuf);
        boolean fCBuf_IsFull (CHAR_BUFFER_TYPE* pcbBuf);

        // Local functions - implementation-specific
        
    #endif
#endif  // Internal Scope Section


