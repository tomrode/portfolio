/************************************************************************************************
 * File:       slowdn.h
 *
 * Purpose:    Defines the main hoist slowdown interface
 *
 * Project:    C584
 *
 * Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 12/11/2003
 *
 ************************************************************************************************
*/

#ifndef  slowdn_registration

   /*******************************************************************************
    *                                                                             *
    *  G l o b a l   S c o p e   S e c t i o n                                    *
    *                                                                             *
    *******************************************************************************/

   // Defines
   typedef enum
   {
      VEL_POS = 0,
      VEL_NEG = 1
   }VEL_TYPE;


   typedef enum
   {
      SD_NONE       = 0,
      SD_POS_SLOW   = 1,
      SD_POS_CUT    = 2,
      SD_POS_STOP   = 3,
      SD_NEG_SLOW   = 4,
      SD_NEG_CUT    = 5,
      SD_NEG_STOP   = 6,
      SD_BOTH_SLOW  = 7,
      SD_INVALID    = 8
   }SLOWDN_TYPE;

   typedef enum
   {
      STP_INIT           = 0,
      STP_UNRESTRICTED   = 1,
      STP_IN_DECEL       = 2,
      STP_AT_STOP        = 3,
      STP_RESTRICTED     = 4
   }STOP_STATE;

   typedef struct
   {
      sword       swPosition;
      sword       swPositionLast;
      ubyte       Type;
      ubyte       Trigger;
      ubyte       Mph;
      boolean     fOverride;
      boolean     fOverride_Latch;
      boolean     fStopped;
      boolean     fInSlowdown;
      ubyte       ubStopState;
      sword       swStopHyst;
      sword       swDecelDist;
      sword       swAccelDist;
      sword       swMinNegSpd;
      sword       swMinPosSpd;
      boolean     fAccelAfterSd;
   }SLOWDN;

      /* Tolerence for mechanical limit test     */
   #define MAIN_SLOWDOWN_RATE_MS          16
   #define MAIN_IN_MECHANICAL_LIMIT_CNTS  (sword)(gParams.swMainLimitTolMs/MAIN_SLOWDOWN_RATE_MS)
   #define RCO_SPEED_HYSTERSIS             5


#endif

#ifdef SLOWDN_MODULE

   #ifndef slowdn_registration_local
   #define slowdn_registration_local 1

   /*******************************************************************************
    *                                                                             *
    *  I n t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/
   typedef struct
   {
      sword swNumPoints;
      sword swInpoint [3];
      sword swOutpoint[3];
   }MAP_SLOWDN;

   // Function prototypes
   boolean fGetStop(SLOWDN *pSd, VEL_TYPE VelDir, sword swCurrPos, uword uwSwitchMask, ubyte ubSpeed);
   sword   swGetSlowdown(SLOWDN *pSd, VEL_TYPE VelDir, sword swCurrPos, sword swMaxNegSpd, sword swMaxPosSpd, ubyte ubSpeed, boolean fDodatalog);
   void vServiceMainRaiseInMechanicalLimit(void);
   boolean fMainRaiseInMechanicalLimit(void);
   boolean fMainHoistVelocityNearZero(void);
   uword uwEncodeCutoutSwitches(boolean fSw1, boolean fSw2, boolean fSw3, boolean fSw4 );

   // Global variables
   boolean fMainInMechanicalLimit = FALSE;
   sword   swMainInMechanicalLimitTimer = 0;
   sword   swSpeed = 0;

   // Macros

   #endif


#else
   #ifndef slowdn_registration
   #define slowdn_registration 1
   /*******************************************************************************
    *                                                                             *
    *  E x t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Externaly visable global variables

   // Externally visable function prototypes
   extern void    vSlowDownSetup(SLOWDN *pSd, sword swDecelDist, sword swAccelDist, sword swStopHyst, sword swMinNegSpeed, sword swMinPosSpeed, boolean fAccelAfterSd);
   extern sword   swSlowDownClamp(SLOWDN  *pSd, VEL_TYPE VelDir, sword swCurrPos, sword swMaxNegSpd, sword swMaxPosSpd, uword uwSwitchMask, ubyte ubSpeed, boolean fDatalog);
   extern void    vSetSlowDownOverride(SLOWDN *pSd, boolean fState, boolean fNeutralCmd);
   extern void vServiceMainRaiseInMechanicalLimit(void);
   extern boolean fMainRaiseInMechanicalLimit(void);
   extern uword uwEncodeCutoutSwitches(boolean fSw1, boolean fSw2, boolean fSw3, boolean fSw4 );

   #endif
#endif

