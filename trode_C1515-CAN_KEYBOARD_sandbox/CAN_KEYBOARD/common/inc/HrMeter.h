/************************************************************************************************
 * File:       HrMeter.h
 *
 * Purpose:    Defines the hour meter interface
 *
 * Project:    C1103
 *
 * Copyright:  2006 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 05/14/2004
 *             Revised for TI DSP 10/19/2006 by Bill McCroskey
 ************************************************************************************************
*/

//#include "main.h"
#include "types.h"

#ifndef  hrmeter_registration

   /*******************************************************************************
    *                                                                             *
    *  G l o b a l   S c o p e   S e c t i o n                                    *
    *                                                                             *
    *******************************************************************************/

   // Defines
   typedef struct
   {
      volatile ulong ulHrMeter;        // MSW = Hours from 0 - 65535
                              // LSW = Seconds from 0 - 3599
      volatile uword uwMilliseconds;   // Accumulates milliseconds
      volatile ulong ulSaveTimerMs;    // Save timer
   }HR_METER;

#endif

#ifdef HRMETER_MODULE

   #ifndef hrmeter_registration_local
   #define hrmeter_registration_local 1

   /*******************************************************************************
    *                                                                             *
    *  I n t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Defines

   // Function prototypes
   void    vHrMeterService(HR_METER *pHrMeter, uword uwServiceRateMs, boolean fAdvanceHrMeter);
   void    vHrMeterSet(HR_METER *pHrMeter, ulong ulHrMeter);
   uword   uwHrMeterGetHours(HR_METER *pHrMeter);
   uword   uwHrMeterGetHourTenths(HR_METER *pHrMeter);
   uword   uwHrMeterGetMinutes(HR_METER *pHrMeter);
   uword   uwHrMeterGetSeconds(HR_METER *pHrMeter, boolean fMinutesAdjusted);
   boolean fHrMeterTimeToSave(HR_METER *pHrMeter, ulong ulSaveRateMs);

   // Global variables

   // Macros

   #endif


#else
   #ifndef hrmeter_registration
   #define hrmeter_registration 1
   /*******************************************************************************
    *                                                                             *
    *  E x t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Externaly visable global variables

   // Externally visable function prototypes
   extern void    vHrMeterService(HR_METER *pHrMeter, uword uwServiceRateMs, boolean fAdvanceHrMeter);
   extern void    vHrMeterSet(HR_METER *pHrMeter, ulong ulHrMeter);
   extern uword   uwHrMeterGetHours(HR_METER *pHrMeter);
   extern uword   uwHrMeterGetHourTenths(HR_METER *pHrMeter);
   extern uword   uwHrMeterGetMinutes(HR_METER *pHrMeter);
   extern uword   uwHrMeterGetSeconds(HR_METER *pHrMeter, boolean fMinutesAdjusted);
   extern boolean fHrMeterTimeToSave(HR_METER *pHrMeter, ulong ulSaveRateMs);

   #endif
#endif

