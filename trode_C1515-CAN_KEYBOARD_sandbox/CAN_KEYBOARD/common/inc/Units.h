/******************************************************************************
 *  File:       Units.h
 *
 *  Purpose:    Describes units used to describe a variable
 *
 *  Project:    C584
 *
 *  Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Walt Conley
 *
 *  Revision History:
 *              Written 10/25/2004
 *              Revised
 *
 ******************************************************************************
*/
#ifndef UNITS_H
#define UNITS_H


typedef enum
{
   UNIT_COUNT   =  0,
   UNIT_VOLT    =  1,
   UNIT_MM      =  2,
   UNIT_INCH    =  3,
   UNIT_LB      =  4,
   UNIT_KG      =  5,
   UNIT_SEC     =  6,
   UNIT_MIN     =  7,
   UNIT_HOUR    =  8,
   UNIT_AMPS    =  9,
   UNIT_DEG_C   = 10,
   UNIT_DEG_F   = 11,
   UNIT_DEG     = 12,
   UNIT_RAD     = 13,
   UNIT_HZ      = 14,
   UNIT_RPM     = 15,
   UNIT_RPS     = 16,
   UNIT_MPH     = 17,
   UNIT_KPH     = 18,
   UNIT_WATT    = 19,
   UNIT_OHM     = 20,
   UNIT_LITER   = 21,
   UNIT_GALLON  = 22,
   UNIT_NM      = 23,
   UNIT_PERCENT = 24,
   UNIT_MA      = 25,
   UNIT_PSI     = 26,
   UNIT_G       = 27
}UNITS_TYPE;

#endif
