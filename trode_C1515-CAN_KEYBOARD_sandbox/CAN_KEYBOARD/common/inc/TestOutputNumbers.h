 /************************************************************************************************
 *  File:       TestOutputNumbers.h
 *
 *  Purpose:    Defines the Test Output Commands
 *
 *  Project:    C1314
 *
 *  Copyright:  2008 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 4/30/2008
 *
 ************************************************************************************************
*/

// This group does not require ED
#define TEST_OUTPUT_NONE              0

#define TEST_OUTPUT_ALM1                1
#define TEST_OUTPUT_LAMP                2
#define TEST_OUTPUT_ALM2                3
#define TEST_OUTPUT_MODULE_FANS         4
#define TEST_OUTPUT_COMPARTMENT_FANS    5
#define TEST_OUTPUT_HN                  6

// This group requires ED pulled in
#define TEST_OUTPUT_M2_U                128
#define TEST_OUTPUT_M2_V                129
#define TEST_OUTPUT_M2_W                130
#define TEST_OUTPUT_M1_U                131
#define TEST_OUTPUT_M1_V                132
#define TEST_OUTPUT_M1_W                133
#define TEST_OUTPUT_PVL                 134
#define TEST_OUTPUT_PCV2_HSL            135
#define TEST_OUTPUT_SV3                 136
#define TEST_OUTPUT_PVC1                137
#define TEST_OUTPUT_PVA_ACCY            138
#define TEST_OUTPUT_SV6                 139
#define TEST_OUTPUT_SV7                 140
#define TEST_OUTPUT_SVBY_BYPASS         141
#define TEST_OUTPUT_SCV_HOIST           142
#define TEST_OUTPUT_SVL_LOWER           143
#define TEST_OUTPUT_SVA_ACCY            144
#define TEST_OUTPUT_SVR_REACH           145
#define TEST_OUTPUT_SVS_SIDESHIFT       146
#define TEST_OUTPUT_SVT_TILT            147
#define TEST_OUTPUT_BRK_OUTER           148
#define TEST_OUTPUT_BRK_INNER           149
#define TEST_OUTPUT_BRK_CASTER          150
#define TEST_OUTPUT_ZAPI_DC_MOTOR       151
#define TEST_OUTPUT_ZAPI_P2_CONTACTOR   152

#define TEST_OUTPUT_ACM_HTR                 154
#define TEST_OUTPUT_ACM_PCV1_HSL            155
#define TEST_OUTPUT_ACM_SVT_TILT            156
#define TEST_OUTPUT_ACM_SVS_SIDESHIFT       157
#define TEST_OUTPUT_ACM_SVR_REACH           158
#define TEST_OUTPUT_ACM_LIGHT               159
#define TEST_OUTPUT_ACM_SV7                 160
