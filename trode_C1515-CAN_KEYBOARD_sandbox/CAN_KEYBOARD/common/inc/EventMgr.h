/************************************************************************************************
 *  File:       EventMgr.h
 *
 *  Purpose:    Defines the interface to the Event Reporting system
 *
 *  Project:    C584
 *
 *  Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/13/2004
 *
 ************************************************************************************************
*/

#include "fram.h"
#include "EventMgrUser.h"   /* allows the user to size the structures for a given project */

#ifndef Event_Mgr_Registration
    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/


    #define QUEUE_OK            0
    #define QUEUE_FULL          1
    #define QUEUE_EMPTY         2

    #define MIN_NON_SRO_EVENT   100
    #define MAX_EVENT_NUMBER    899

    // Types
    typedef enum
    {
        Empty = 0,
        HasContent,
        WaitForMaskPDO
    } EVENT_STATES;


    typedef struct
    {
        ubyte                   ubEventIndex;
        boolean                 fSetError;
        void(*pfnEventCallback)(uword, boolean, boolean);
    } PENDING_LIST_TYPE;

    typedef struct
    {
        ubyte               ubHead;
        ubyte               ubTail;
        boolean             fHasContent;
        PENDING_LIST_TYPE   Array[EVENT_PENDING_SIZE];
    } PENDING_QUEUE_TYPE;

    typedef struct
    {
        uword                   uwEventCode;
        boolean                 fSetError;
        ubyte                   ubTimeoutCount;
        ubyte                   ubRepeatCount;
        EVENT_STATES            eState;
        void(*pfnEventCallback)(uword, boolean, boolean);
    } REPORTED_TYPE;

    typedef struct
    {
        ubyte   ubHead;
        ubyte   ubTail;
        boolean fHasContent;
        ulong   Array[EVENT_ACKED_SIZE];
    } ACKED_QUEUE_TYPE;


    // constants


#endif

#ifdef EventMgrFile

    #ifndef EventMgrLocalReg
    #define EventMgrLocalReg 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Definitions

    // Funciton prototypes
    ubyte ubQueue_Pending_Event(ubyte ubEventIndex, boolean fSetError, void(*pfnCallback) (uword, boolean, boolean));
    ubyte ubDequeue_Pending_Event(ubyte *ubEventIndex, boolean *pfSetError, void(**pfnCallback) (uword, boolean, boolean));

    ubyte ubQueue_Acked_Event(ulong ulEventCode);
    ubyte ubDequeue_Acked_Event(ulong *ulEventCode);

    ubyte ubLogEvent(ubyte ubEventIndex, boolean fSetError, void(*pfnCallback)(uword, boolean, boolean));

    ubyte ubAckEvent(ulong);

    void vLogEventToFRAM(uword);

    void vProcessAcked(void);
    void vProcessPending(void);
    void vProcessReported(void);

    void vEventMaskReport(void);

    void vInit_EventMgr();
    void vClearLocalEventList();

    void vEventMgr_Task();

    // Global Variables
    volatile PENDING_QUEUE_TYPE gPendingEvnts;       // Stores the pending events
    volatile REPORTED_TYPE      gReportedEvnts[EVENT_REPORTED_SIZE];     // stores the reported list
    volatile ACKED_QUEUE_TYPE   gAckedEvnts;         // Stores the acked events
    volatile boolean            gfPacketSent;        // Indicates a packet was sent this tick
    volatile boolean            gfPacketTimeOut;     // Indicates a packet had to be resent
    volatile boolean            gfDisplayTimeOut;    // Indicates the display never acked a packet
    volatile uword              guwEventCodeSent;    // holds the code that was sent to the display
    volatile uword              guwEventCodeLogged;  // holds the last logged code for datalogging
    volatile boolean            gfSetEventSent;      // holds the on/off flag for the sent event
    volatile boolean            gfMadeCallback;      // holds the status of callbacks made to the functional area
    volatile LOCAL_EVENTS_TYPE  gLocalEvents;        // holds the list of local events
    volatile ulong gulAckedEvent;                // Storage for CAN stack to store acked event from display module
    volatile boolean            gfAckedHadContent;  // storage for datalog
    volatile boolean            gfPendingHadContent; // storage for datalog
    #endif


#else
    #ifndef Event_Mgr_Registration
    #define Event_Mgr_Registration 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Externaly visable function prototypes
    extern ubyte ubLogEvent(ubyte ubEventIndex, boolean fSetError, void(*pfnCallback)(uword, boolean, boolean));

    extern ubyte ubAckEvent(ulong);

    extern void vInit_EventMgr();
    extern void vEventMgr_Task();
    extern void vClearLocalEventList();
    extern void vEventMaskReport(void);

    // Externally visable global variables
    extern volatile LOCAL_EVENTS_TYPE  gLocalEvents;        // holds the list of local events
    extern volatile ulong gulAckedEvent;                // Storage for CAN stack to store acked event from display module

    #endif
#endif

