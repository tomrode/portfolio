//****************************************************************************************
// File:       key_vals.h
//
// Purpose:    Defines values related to each keypad key for use by both the IHM and UIM.
//
// Project:    InfoLink (C1153)
//
// Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
//
// Author:     Max Peelman
//
// Revision History:
//          
//      04/20/2005 Max Peelman: 
//        -created document
//
//****************************************************************************************



//*************************************************************************
//                                                                        *
//    G l o b a l   S c o p e   S e c t i o n                             *
//                                                                        *
//  (This data is useful to all source files)                             *
//*************************************************************************
#ifndef key_vals_global_scope
#define key_vals_global_scope 1

    //-------------------------
    // Defines
    //-------------------------

        // all possible key values
        typedef enum
        {
            KEY_NONE               =   0,
            KEY_0                  =   1,
            KEY_1                  =   2,
            KEY_2                  =   3,
            KEY_3                  =   4,
            KEY_4                  =   5,
            KEY_5                  =   6,
            KEY_6                  =   7,
            KEY_7                  =   8,
            KEY_8                  =   9,
            KEY_9                  =  10,
            KEY_MENU               =  11,
            KEY_ENTER              =  12,
            KEY_CLEAR              =  13,
            KEY_UP                 =  14,
            KEY_DOWN               =  15
        } KYPD_KEY_VALS;

    //-------------------------
    // Global variables
    //-------------------------


    //-------------------------
    // Macros
    //-------------------------
        


#endif  // Global Scope Section


//*************************************************************************
//                                                                        *
//  E x t e r n a l   S c o p e   S e c t i o n                           *
//                                                                        *
//  (This is for source files using this module, not implementing it)     *
//*************************************************************************
#ifndef KEY_VALS_SOURCE
    #ifndef key_vals_external_scope
    #define key_vals_external_scope 1
      
    //-------------------------
    // Global variables
    //-------------------------
        

    //-------------------------
    // Global Function Prototypes
    //-------------------------



    #endif
#endif  // External Scope Section

//*************************************************************************
//                                                                        *
//  I n t e r n a l   S c o p e   S e c t i o n                           *
//                                                                        *
//  (This is for the implementing source file(s) only!)                   *
//*************************************************************************
#ifdef KEY_VALS_SOURCE
    #ifndef key_vals_internal_scope
    #define key_vals_internal_scope 1

    //-------------------------
    // Defines
    //-------------------------
    

    //-------------------------
    // Internal variables
    //-------------------------


    //-------------------------
    // Function prototypes   
    //-------------------------



    #endif
#endif  // Internal Scope Section


