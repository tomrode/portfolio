/************************************************************************************************
 * File:       RangeChk.h
 *
 * Purpose:    Defines the Range check interfrace
 *
 * Project:    C584
 *
 * Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 05/19/2004
 *
 ************************************************************************************************
*/

#ifndef  rangechk_registration

   /*******************************************************************************
    *                                                                             *
    *  G l o b a l   S c o p e   S e c t i o n                                    *
    *                                                                             *
    *******************************************************************************/

   // Defines
   typedef struct
   {
      boolean fPerformTest;
      sword   swConcernCount;
      sword   swConcernCountMax;
   }RNG_CHK;

   typedef enum
   {
      RNG_OK = 0,
      RNG_LO = 1,
      RNG_HI = 2
   }RNG_RTN;

   typedef enum
   {
      CHK_MIN = 0,
      CHK_MAX = 1,
      CHK_RNG = 2
   }RNG_CHK_TYPE;

#endif

#ifdef RANGECHK_MODULE

   #ifndef rangechk_registration_local
   #define rangechk_registration_local 1

   /*******************************************************************************
    *                                                                             *
    *  I n t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Defines

   // Function prototypes

   // Global variables

   // Macros

   #endif


#else
   #ifndef rangechk_registration
   #define rangechk_registration 1
   /*******************************************************************************
    *                                                                             *
    *  E x t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Externaly visable global variables

   // Externally visable function prototypes
   extern void    vRangeCheckInit(RNG_CHK *pRc, sword swToleranceMs, sword swUpdateRateMs);
   extern void    vRangeCheckDisable(RNG_CHK *pRc);
   extern void    vRangeCheckReset(RNG_CHK *pRc);
   extern RNG_RTN fnRangeCheck(RNG_CHK *pRc, sword swValue, sword swMin, sword swMax, boolean fDataLog, RNG_CHK_TYPE Check);

   #endif
#endif

