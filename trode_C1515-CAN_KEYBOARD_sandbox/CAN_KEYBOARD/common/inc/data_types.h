//****************************************************************************************
// File:       data_types.h
//
// Purpose:    Typedefs for some common primitives that conform to Crown's Hungarian
//             notation conventions.
//
// Project:    InfoLink (C1153)
//
// Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
//
// Author:     Max Peelman
//
// Revision History:
//
//      11/15/2004 Max Peelman:
//        -created document
//
//      04/07/2004 Max Peelman:
//        -add support for HCS12 and C166 and #ifndef checking for each typedef
//
//****************************************************************************************

#ifndef DATA_TYPES_H
    #define DATA_TYPES_H

//-------------------------
// Common Typedefs
//-------------------------

    // are we using CodeWarrior (HCS12)?
    #ifdef __MWERKS__
        #ifndef _BOOLEAN_DEFD
            #define _BOOLEAN_DEFD
            typedef unsigned char   boolean;  // true/false;      prefix:  f
        #endif
        #ifndef _UBYTE_DEFD
            #define _UBYTE_DEFD
            typedef unsigned char   ubyte;    // 1 byte unsigned; prefix: ub
        #endif
        #ifndef _SBYTE_DEFD
            #define _SBYTE_DEFD
            typedef signed char     sbyte;    // 1 byte signed;   prefix: sb
        #endif
        #ifndef _UWORD_DEFD
            #define _UWORD_DEFD
            typedef unsigned int    uword;    // 2 byte unsigned; prefix: uw
        #endif
        #ifndef _SWORD_DEFD
            #define _SWORD_DEFD
            typedef signed int      sword;    // 2 byte signed;   prefix: sw
        #endif
        #ifndef _ULONG_DEFD
            #define _ULONG_DEFD
            typedef unsigned long   ulong;    // 4 byte unsigned; prefix: ul
        #endif
        #ifndef _SLONG_DEFD
            #define _SLONG_DEFD
            typedef signed long     slong;    // 4 byte signed;   prefix: sl
        #endif
        #ifndef FALSE
        #define FALSE 0
        #endif
        #ifndef TRUE
        #define TRUE !FALSE
        #endif

    #endif   // #ifdef __MWERKS__

    // are we using Keil (C166)?
    #ifdef _KL_C166
        // DAvE's "main.h" typedefs many of the types already...
        #ifndef _BOOLEAN_DEFD
            #define _BOOLEAN_DEFD
            #ifndef boolean
            #ifndef _MAIN_H_
                typedef unsigned char   boolean;  // true/false;      prefix:  f
            #endif
            #endif
        #endif
    #endif   // #ifdef _KL_C166

//-------------------------
// Common Macros
//-------------------------

    #define  fToBoolean(expression) ( (boolean)( (expression) != 0 ) )
    #define  uwArraySize(arrayName) ( (uword)( sizeof(arrayName) / sizeof(arrayName[0]) ) )


#endif // #ifndef DATA_TYPES_H