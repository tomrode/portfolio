/************************************************************************************************
 * File:       objects.h
 *
 * Purpose:    Defines object indexes for all object dictionary entrys in the 3WTSP
 *             vehicle control system.
 *
 * Project:    C584
 *
 * Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *          Written 11/13/2003
 *
 ************************************************************************************************
*/
#ifndef OBJECTS_H
#define OBJECTS_H


    /* Standard CANopen objects */
#define OBJ_DEVICE_NAME           0x1008
#define OBJ_ZAPI_EEPROM_CNTL      0x1010

/* Define Objects for features data */    // Bit Position - Request Sub Index 1  - DR_GROUP1
#define OBJ_FTR_LANGUAGE                    0x2000   //    0 (lsb)
#define OBJ_FTR_METRIC                      0x2001   //    1
#define OBJ_FTR_REACH_TYPE                  0x2002   //    2
#define OBJ_FTR_MODEL_NUMBER                0x2003   //    3
#define OBJ_FTR_MAIN_WGT_MAX                0x2004   //    4
#define OBJ_FTR_TRUCK_TYPE                  0x2005   //    5
#define OBJ_FTR_MAIN_HGT_MAX                0x2006   //    6
#define OBJ_FTR_SIDESHIFT_TYPE              0x2007   //    7
#define OBJ_FTR_DISPLAY_TYPE                0x2008   //    8
#define OBJ_FTR_STEER_DIRECTION             0x2009   //    9
#define OBJ_FTR_ADD_ONS                     0x200A   //   10
#define OBJ_FTR_TRUCK_WEIGHT                0x200B   //   11
#define OBJ_FTR_TILT                        0x200C   //   12
#define OBJ_FTR_CDM_NUM_ZONES               0x200D   //   13
#define OBJ_FTR_CDM_ZONE0_HGT               0x200E   //   14
#define OBJ_FTR_CDM_ZONE0_WGT               0x200F   //   15
#define OBJ_FTR_CDM_ZONE1_HGT               0x2010   //   16
#define OBJ_FTR_CDM_ZONE1_WGT               0x2011   //   17
#define OBJ_FTR_CDM_ZONE2_HGT               0x2012   //   18
#define OBJ_FTR_CDM_ZONE2_WGT               0x2013   //   19
#define OBJ_FTR_CDM_ZONE3_HGT               0x2014   //   20
#define OBJ_FTR_CDM_ZONE3_WGT               0x2015   //   21
#define OBJ_FTR_CHAIN_SLACK                 0x2016   //   22
#define OBJ_FTR_TRAV_ALARM                  0x2017   //   23
#define OBJ_FTR_BATT_RETAIN                 0x2018   //   24
#define OBJ_FTR_MODULE_FAN                  0x2019   //   25
#define OBJ_FTR_MOTOR_FAN                   0x201A   //   26
#define OBJ_FTR_SPEED_CUTBACK               0x201B   //   27
#define OBJ_FTR_MESSAGE_MODE                0x201C   //   28
#define OBJ_FTR_USER_PERF                   0x201D   //   29
#define OBJ_FTR_SPARE                       0x201E   //   30
#define OBJ_FTR_CLPSD_HEIGHT                0x201F   //   31 (msb)

/* Define Objects for features data */    // Bit Position - Request Sub Index 2  - DR_GROUP2
#define OBJ_FTR_USER_RACK               0x2020   //    0 (lsb)
#define OBJ_FTR_TILT_ASSIST             0x2021   //    1
#define OBJ_FTR_FORK_HOME_TRV           0x2022   //    2
#define OBJ_FTR_ZONESW_HGT              0x2023   //    3
#define OBJ_FTR_HOUR_MODE               0x2024   //    4
#define OBJ_FTR_SLIP_CTRL               0x2025   //    5
#define OBJ_FTR_GRADE_CTRL              0x2026   //    6
#define OBJ_FTR_HIGH_SPD_LOWER          0x2027   //    7
#define OBJ_FTR_GROUP2_SPARE1           0x2028   //    8
#define OBJ_FTR_ZONESW_SPD              0x2029   //    9
#define OBJ_FTR_GROUP2_SPARE3           0x202A   //   10
#define OBJ_FTR_GROUP2_SPARE4           0x202B   //   11
#define OBJ_FTR_PIN_TIMEOUT             0x202C   //   12
#define OBJ_FTR_PIN_ENABLE              0x202D   //   13
#define OBJ_FTR_PIN_LEV_0               0x202E   //   14
#define OBJ_FTR_PIN_COD_0               0x202F   //   15
#define OBJ_FTR_PIN_LEV_1               0x2030   //   16
#define OBJ_FTR_PIN_COD_1               0x2031   //   17
#define OBJ_FTR_PIN_LEV_2               0x2032   //   18
#define OBJ_FTR_PIN_COD_2               0x2033   //   19
#define OBJ_FTR_PIN_LEV_3               0x2034   //   20
#define OBJ_FTR_PIN_COD_3               0x2035   //   21
#define OBJ_FTR_PIN_LEV_4               0x2036   //   22
#define OBJ_FTR_PIN_COD_4               0x2037   //   23
#define OBJ_FTR_PIN_LEV_5               0x2038   //   24
#define OBJ_FTR_PIN_COD_5               0x2039   //   25
#define OBJ_FTR_PIN_LEV_6               0x203A   //   26
#define OBJ_FTR_PIN_COD_6               0x203B   //   27
#define OBJ_FTR_PIN_LEV_7               0x203C   //   28
#define OBJ_FTR_PIN_COD_7               0x203D   //   29
#define OBJ_FTR_PIN_LEV_8               0x203E   //   30
#define OBJ_FTR_PIN_COD_8               0x203F   //   31 (msb)

/* Define Objects for features data */    // Bit Position - Request Sub Index 3  - DR_GROUP3
#define OBJ_FTR_PIN_LEV_9               0x2040   //    0 (lsb)
#define OBJ_FTR_PIN_COD_9               0x2041   //    1
#define OBJ_FTR_PIN_LEV_10              0x2042   //    2
#define OBJ_FTR_PIN_COD_10              0x2043   //    3
#define OBJ_FTR_PIN_LEV_11              0x2044   //    4
#define OBJ_FTR_PIN_COD_11              0x2045   //    5
#define OBJ_FTR_PIN_LEV_12              0x2046   //    6
#define OBJ_FTR_PIN_COD_12              0x2047   //    7
#define OBJ_FTR_PIN_LEV_13              0x2048   //    8
#define OBJ_FTR_PIN_COD_13              0x2049   //    9
#define OBJ_FTR_PIN_LEV_14              0x204A   //   10
#define OBJ_FTR_PIN_COD_14              0x204B   //   11
#define OBJ_FTR_PIN_LEV_15              0x204C   //   12
#define OBJ_FTR_PIN_COD_15              0x204D   //   13
#define OBJ_FTR_PIN_LEV_16              0x204E   //   14
#define OBJ_FTR_PIN_COD_16              0x204F   //   15
#define OBJ_FTR_PIN_LEV_17              0x2050   //   16
#define OBJ_FTR_PIN_COD_17              0x2051   //   17
#define OBJ_FTR_PIN_LEV_18              0x2052   //   18
#define OBJ_FTR_PIN_COD_18              0x2053   //   19
#define OBJ_FTR_PIN_LEV_19              0x2054   //   20
#define OBJ_FTR_PIN_COD_19              0x2055   //   21
#define OBJ_FTR_PIN_LEV_20              0x2056   //   22
#define OBJ_FTR_PIN_COD_20              0x2057   //   23
#define OBJ_FTR_PIN_LEV_21              0x2058   //   24
#define OBJ_FTR_PIN_COD_21              0x2059   //   25
#define OBJ_FTR_PIN_LEV_22              0x205A   //   26
#define OBJ_FTR_PIN_COD_22              0x205B   //   27
#define OBJ_FTR_PIN_LEV_23              0x205C   //   28
#define OBJ_FTR_PIN_COD_23              0x205D   //   29
#define OBJ_FTR_PIN_LEV_24              0x205E   //   30
#define OBJ_FTR_PIN_COD_24              0x205F   //   31 (msb)

/*  Bit Position - Request Sub Index 4, Height Cutout DR_GROUP4 */
#define OBJ_CUTOUT1_HEIGHT              0x2060   //     0
#define OBJ_CUTOUT1_TYPE                0x2061   //     1
#define OBJ_CUTOUT1_ZONE                0x2062   //     2
#define OBJ_CUTOUT1_TRIGGER             0x2063   //     3
#define OBJ_CUTOUT1_MPH                 0x2064   //     4
#define OBJ_CUTOUT2_HEIGHT              0x2065   //     5
#define OBJ_CUTOUT2_TYPE                0x2066   //     6
#define OBJ_CUTOUT2_ZONE                0x2067   //     7
#define OBJ_CUTOUT2_TRIGGER             0x2068   //     8
#define OBJ_CUTOUT2_MPH                 0x2069   //     9
#define OBJ_CUTOUT3_HEIGHT              0x206A   //    10
#define OBJ_CUTOUT3_TYPE                0x206B   //    11
#define OBJ_CUTOUT3_ZONE                0x206C   //    12
#define OBJ_CUTOUT3_TRIGGER             0x206D   //    13
#define OBJ_CUTOUT3_MPH                 0x206E   //    14
#define OBJ_CUTOUT4_HEIGHT              0x206F   //    15
#define OBJ_CUTOUT4_TYPE                0x2070   //    16
#define OBJ_CUTOUT4_ZONE                0x2071   //    17
#define OBJ_CUTOUT4_TRIGGER             0x2072   //    18
#define OBJ_CUTOUT4_MPH                 0x2073   //    19
#define OBJ_CUTOUT5_HEIGHT              0x2074   //    20
#define OBJ_CUTOUT5_TYPE                0x2075   //    21
#define OBJ_CUTOUT5_ZONE                0x2076   //    22
#define OBJ_CUTOUT5_TRIGGER             0x2077   //    23
#define OBJ_CUTOUT5_MPH                 0x2078   //    24
#define OBJ_CUTOUT6_HEIGHT              0x2079   //    25
#define OBJ_CUTOUT6_TYPE                0x207A   //    26
#define OBJ_CUTOUT6_ZONE                0x207B   //    27
#define OBJ_CUTOUT6_TRIGGER             0x207C   //    28
#define OBJ_CUTOUT6_MPH                 0x207D   //    29
#define OBJ_GROUP4_SPARE1               0x207E   //    30
#define OBJ_GROUP4_SPARE2               0x207F   //    31 (msb)

/* Define Objects for performance data */ // Bit Position - Request Sub Index 5  - DR_GROUP5
#define OBJ_PRF_P1_TRAV_ACCEL           0x2080   //    0 (lsb)
#define OBJ_PRF_P1_MAX_EMPTY_FWD_SPD    0x2081   //    1
#define OBJ_PRF_P1_MAX_EMPTY_REV_SPD    0x2082   //    2
#define OBJ_PRF_P1_MAX_LOAD_FWD_SPD     0x2083   //    3
#define OBJ_PRF_P1_MAX_LOAD_REV_SPD     0x2084   //    4
#define OBJ_PRF_P1_RAISE_SPD            0x2085   //    5
#define OBJ_PRF_P1_LOWER_SPD            0x2086   //    6
#define OBJ_PRF_P1_LIFT_POWER           0x2087   //    7
#define OBJ_PRF_P1_LOAD_WEIGHT          0x2088   //    8
#define OBJ_PRF_P2_TRAV_ACCEL           0x2089   //    9
#define OBJ_PRF_P2_MAX_EMPTY_FWD_SPD    0x208A   //   10
#define OBJ_PRF_P2_MAX_EMPTY_REV_SPD    0x208B   //   11
#define OBJ_PRF_P2_MAX_LOAD_FWD_SPD     0x208C   //   12
#define OBJ_PRF_P2_MAX_LOAD_REV_SPD     0x208D   //   13
#define OBJ_PRF_P2_RAISE_SPD            0x208E   //   14
#define OBJ_PRF_P2_LOWER_SPD            0x208F   //   15
#define OBJ_PRF_P2_LIFT_POWER           0x2090   //   16
#define OBJ_PRF_P2_LOAD_WEIGHT          0x2091   //   17
#define OBJ_ZAPI_TRUCK_VOLTAGE          0x2091   //   Zapi Module for Truck voltage parameter
#define OBJ_PRF_P3_TRAV_ACCEL           0x2092   //   18
#define OBJ_PRF_P3_MAX_EMPTY_FWD_SPD    0x2093   //   19
#define OBJ_PRF_P3_MAX_EMPTY_REV_SPD    0x2094   //   20
#define OBJ_PRF_P3_MAX_LOAD_FWD_SPD     0x2095   //   21
#define OBJ_PRF_P3_MAX_LOAD_REV_SPD     0x2096   //   22
#define OBJ_PRF_P3_RAISE_SPD            0x2097   //   23
#define OBJ_PRF_P3_LOWER_SPD            0x2098   //   24
#define OBJ_PRF_P3_LIFT_POWER           0x2099   //   25
#define OBJ_PRF_P3_LOAD_WEIGHT          0x209A   //   26
#define OBJ_PRF_BDI                     0x209B   //   27
#define OBJ_PRF_PLUG                    0x209C   //   28
#define OBJ_PRF_DRIVE_BRAKE             0x209D   //   29
#define OBJ_PRF_CASTER_BRAKE            0x209E   //   30
#define OBJ_PRF_COAST                   0x209F   //   31 (msb)
                                          //
/* Define Objects for performance data */ // Bit Position - Request Sub Index 6  - DR_GROUP6
#define OBJ_PRF_ACCY_SPD_LT_FLS         0x20A0   //    0 (lsb)
#define OBJ_PRF_ACCY_SPD_GT_FLS         0x20A1   //    1
#define OBJ_PRF_ACCY_SPD_GT_270         0x20A2   //    2
#define OBJ_PRF_TRVL_SPD_GT_FLS         0x20A3   //    3
#define OBJ_PRF_TRVL_SPD_GT_270         0x20A4   //    4
#define OBJ_PRF_ACCY_RAMP_ACCEL         0x20A5   //    5
#define OBJ_PRF_ACCY_RAMP_DECEL         0x20A6   //    6
#define OBJ_PRF_HOIST_RAISE_ACCEL       0x20A7   //    7
#define OBJ_PRF_HOIST_RAISE_DECEL       0x20A8   //    8
#define OBJ_PRF_HOIST_LOWER_ACCEL       0x20A9   //    9
#define OBJ_PRF_HOIST_LOWER_DECEL       0x20AA   //    10
#define OBJ_PRF_CUTBACK_MAX_SPD         0x20AB   //    11
#define OBJ_PRF_CUTBACK_LOAD_WGT        0x20AC   //    12
#define OBJ_PRF_CUTBACK_HEIGHT          0x20AD   //    13
#define OBJ_PRF_HOIST_LOWER_BO_ACCEL    0x20AE   //    14
#define OBJ_RACK_SELECT_LOADED_OFFSET   0x20AF   //    15
#define OBJ_PRF_CUTBACK_TRIGGER         0x20B0   //    16
#define OBJ_PRF_CUTBACK_FORKHOME        0x20B1   //    17
#define OBJ_FTR_STARTUP_INTERLOCK       0x20B2   //    18
#define OBJ_FTR_REMOTE_RL               0x20B3   //    19
#define OBJ_FTR_PM_LIFT_CUTOUT          0x20B4   //    20
#define OBJ_FTR_REGEN_LOWER             0x20B5   //    21
#define OBJ_GROUP6_SPARE07              0x20B6   //    22
#define OBJ_GROUP6_SPARE08              0x20B7   //    23
#define OBJ_GROUP6_SPARE09              0x20B8   //    24
#define OBJ_GROUP6_SPARE10              0x20B9   //    25
#define OBJ_GROUP6_SPARE11              0x20BA   //    26
#define OBJ_GROUP6_SPARE12              0x20BB   //    27
#define OBJ_GROUP6_SPARE13              0x20BC   //    28
#define OBJ_ESTR_CAL_CW_CNTS            0x20BD   //    29
#define OBJ_ESTR_CAL_CCW_CNTS           0x20BE   //    30
#define OBJ_ESTR_CAL_ZERO_CNTS          0x20BF   //    31  (msb)

/* Define Objects for User data */ // Bit Position - Request Sub Index 7  - DR_GROUP7
#define OBJ_ALL_SETUPS_CRC              0x20C0   //    0 (lsb)
#define OBJ_ALL_SETUPS_REV              0x20C1   //    1
#define OBJ_FTRS_CRC                    0x20C2   //    2
#define OBJ_FTRS_REV                    0x20C3   //    3
#define OBJ_PERFS_CRC                   0x20C4   //    4
#define OBJ_PERFS_REV                   0x20C5   //    5
#define OBJ_CUTOUTS_CRC                 0x20C6   //    6
#define OBJ_CUTOUTS_REV                 0x20C7   //    7
#define OBJ_RACKSELECT_1                0x20C8   //    8
#define OBJ_RACKSELECT_2                0x20C9   //    9
#define OBJ_RACKSELECT_3                0x20CA   //    10
#define OBJ_RACKSELECT_4                0x20CB   //    11
#define OBJ_RACKSELECT_5                0x20CC   //    12
#define OBJ_RACKSELECT_6                0x20CD   //    13
#define OBJ_RACKSELECT_7                0x20CE   //    14
#define OBJ_RACKSELECT_8                0x20CF   //    15
#define OBJ_RACKSELECT_9                0x20D0   //    16
#define OBJ_RACKSELECT_10               0x20D1   //    17
#define OBJ_RACKSELECT_11               0x20D2   //    18
#define OBJ_RACKSELECT_12               0x20D3   //    19
#define OBJ_RACKSELECT_13               0x20D4   //    20
#define OBJ_RACKSELECT_14               0x20D5   //    21
#define OBJ_RACKSELECT_15               0x20D6   //    22
#define OBJ_RACKSELECT_16               0x20D7   //    23
#define OBJ_RACKSELECT_17               0x20D8   //    24
#define OBJ_RACKSELECT_18               0x20D9   //    25
#define OBJ_RACKSELECT_19               0x20DA   //    26
#define OBJ_RACKSELECT_20               0x20DB   //    27
#define OBJ_RACKSELECT_21               0x20DC   //    28
#define OBJ_RACKSELECT_22               0x20DD   //    29
#define OBJ_RACKSELECT_23               0x20DE   //    30
#define OBJ_RACKSELECT_24               0x20DF   //    31  (msb)

      /* Truck defined objects       */
/* ITA Set-Up  0x2000 - 0x20FF  Items must match ITA spec in this region   */

/* ITA Set-Up Manufacturer Specific 0x2100 - 0x21FF   */
#define OBJ_LOCAL_CAL_MODE       0x2100
#define OBJ_LOCAL_CAL_STATE      0x2101
#define OBJ_CAL_DISP_TEXT_ID     0x2102
#define OBJ_CAL_PROC_VAR         0x2103
#define OBJ_CAL_ACTION           0x2104
#define OBJ_CAL_TIME_DELAY       0x2105
#define OBJ_CAL_DE_VAR           0x2106
#define OBJ_CO_ERR_IDX           0x2107
#define OBJ_CO_ERR_SI            0x2108
#define OBJ_CO_ERR_INDEX1        0x2109
#define OBJ_CO_ERR_INDEX2        0x210A
#define OBJ_CO_ERR_INDEX3        0x210B

#define OBJ_ZAPI_MOD_TEMP        0x210F
#define OBJ_ZAPI_MOTOR_TEMP      0x2110
#define OBJ_ZAPI_MOD_VOLTAGE     0x2114
#define OBJ_ZAPI_MOTOR_VOLTAGE   0x2115
#define OBJ_ZAPI_MOTOR_CURRENT   0x211B
#define OBJ_ZAPI_INPUT_CURRENT   0x21F1
#define OBJ_ZAPI_BDI_PERCENT     0x21F2
#define OBJ_ZAPI_TARGET_SPEED    0x21F7
#define OBJ_ZAPI_HDM_WARNING     0x21FA
#define OBJ_ZAPI_MODULE_STAT     0x21FB
#define OBJ_ZAPI_MOTOR_SPEED     0x21FE

/* ITA Status 0x2200 - 0x221D  Items must match ITA spec in this region   */
#define OBJ_VEHICLE_SPEED        0x2213
#define OBJ_TEST_OUTPUT_CMD      0x2214

/* ITA Status Manufacturer Specific 0x2300 - 0x23FF   */
#define OBJ_VCM_FLAGS            0x2300
#define OBJ_DM_FLAGS             0x2301
#define OBJ_GCM_FLAGS            0x2302
#define OBJ_SCM_FLAGS            0x2303
#define OBJ_OCM_FLAGS            0x2304
#define OBJ_ACM_FLAGS            0x2305
#define OBJ_TCM_FLAGS            0x2306
#define OBJ_HCM_FLAGS            0x2307
#define OBJ_TRAVEL_DIST          0x2310
#define OBJ_EMERGENCY_PDO        0x2316
#define OBJ_RESET_COUNTS         0x231B
#define OBJ_BATTERY_HISTORY      0x231C
#define OBJ_RACKSELECT_LBL       0x231D

/* ITA Digtal Inputs  0x2400 - 0x2421  Items must match ITA spec in this region   */


/* ITA Digital Input Manufacturer Specific 0x2500 - 0x25FF   */
#define OBJ_VCM_SWITCHES            0x2500
#define OBJ_DM_SWITCHES             0x2501
#define OBJ_GCM_SWITCHES            0x2502
#define OBJ_SCM_SWITCHES            0x2503
#define OBJ_OCM_SWITCHES            0x2504
#define OBJ_LHM_SWITCHES            0x2505
#define OBJ_TCM_SWITCHES            0x2506
#define OBJ_HCM_SWITCHES            0x2507
#define OBJ_KEY_SWITCHES            0x2508
#define OBJ_TOW_FROM_MENU           0x2509
#define OBJ_APP_SPECIFIC_FLAGS      0x2536
#define OBJ_BRAKE_CMDS              0x2537
#define OBJ_ACM_COMMAND1            0x253D
#define OBJ_ACM_CMDSPD1             0x253E
#define OBJ_PWM_DEBUG_FLAG          0x253F
#define OBJ_DIGITAL_IN              0x2540
#define OBJ_ANALYZER_DIGITAL_INPUT  0x2541


/* ITA Analog Input 0x2600 - 0x2637  Items must match ITA spec in this region */
#define OBJ_TRACTION_POT         0x2600
#define OBJ_STR_WHL_ANGLE        0x2607
#define OBJ_HCM_BATT_VOLT        0x2608
#define OBJ_SCM_BATT_VOLT        0x2609
#define OBJ_SCM_BATT_CURR        0x260A
#define OBJ_MHOIST_THR_RAW       0x2618
#define OBJ_AHOIST_THR_RAW       0x2619
#define OBJ_TRAV_THR_RAW         0x2620
#define OBJ_PIVOT_THR_RAW        0x2621
#define OBJ_LOAD_WEIGHT          0x2630
#define OBJ_PLAT_WEIGHT          0x2631
#define OBJ_FORK_HEIGHT          0x2632
#define OBJ_BDI_SOC              0x2633
#define OBJ_RACK_SEL_STAT        0x2634

/* ITA Analog Input Manufacturer Specific 0x2700 - 0x27FF   */
#define OBJ_ECR7_RPM                0x2718
#define OBJ_ECR7_COUNTS             0x2719
#define OBJ_VCM_ANALYZER_POT1       0x271D
#define OBJ_ADC_DATA                0x2725
#define OBJ_ADC_EXP_DATA            0x2726
#define OBJ_ADC_SCALED              0x2727
#define OBJ_ADC_SCALED_IQ_FORMAT    0x2728
#define OBJ_TCM_INPUTS              0x2729
#define OBJ_PDO_IO                  0x272A
#define OBJ_BRUSH_WEAR                  0x272B
#define OBJ_KEY_VOLTAGE                 0x272C
#define OBJ_IMS_VOLTAGE                 0x272D
#define OBJ_EXT_12V                     0x272E
#define OBJ_MOTOR_TEMP                  0x272F
#define OBJ_POT_VOLTAGE                 0x2730
#define OBJ_AIN1_VOLTAGE                0x2731
#define OBJ_AIN2_VOLTAGE                0x2732
#define OBJ_IMS_TEMP1                   0x2733
#define OBJ_IMS_TEMP2                   0x2734
#define OBJ_ANALOG_5V                   0x2735
#define OBJ_LOGIC_5V                    0x2736
#define OBJ_LOGIC_15V                   0x2737
#define OBJ_CURRENT_SENSOR_TEMP         0x2738
#define OBJ_ANALYZER_TEST_OUTPUT_TO     0x2739
#define OBJ_ANALYZER_ANALOG_INPUTS      0x273A
#define OBJ_HCM_INPUTS                  0x273B
#define OBJ_TARGET_SPEED_CROWN_AC       0x273C


/* ITA Digital Output 0x2800 - 0x281B  Items must match ITA spec in this region  */
#define OBJ_ENCODER1_COUNTS         0x2800
#define OBJ_ENCODER2_COUNTS         0x2801
#define OBJ_ENCODER3_COUNTS         0x2802
#define OBJ_ENCODER4_COUNTS         0x2803
#define OBJ_ENCODER5_COUNTS         0x2804
#define OBJ_BRIDGE_STAT             0x2805
#define OBJ_GP_SHORT_STAT           0x2806
#define OBJ_SWITCH_FPGA             0x2807
#define OBJ_SV_SHORT_STAT           0x2808

/* ITA Digital Output Manufacturer Specific 0x2900 - 0x29FF   */
#define OBJ_ANALYZER_ED_DRIVE       0x2900
#define OBJ_LED_OUTPUTS             0x2901
#define OBJ_PC_COMMANDS             0x2902
#define OBJ_PC_DATA                 0x2903
#define OBJ_STEERING_CMD            0x2904
#define OBJ_ANALYZER_DIGITAL_OUTPUT 0x2905

/* ITA Analog Outputs 0x2A00 - 0x2A07   Items must match ITA spec in this region */
#define OBJ_HYDR_STATUS_WORD        0x2A00
#define OBJ_HYDR_TARGET_RPM         0x2A01
#define OBJ_HYDR_CURRENT_OFFSET     0x2A02
#define OBJ_HYDR_STATUS_WORD2       0x2A03

#define OBJ_HYDR_RX_STATUS_WORD     0x2A04
#define OBJ_HYDR_RX_ACTUAL_SPD      0x2A05
#define OBJ_HYDR_RX_SLIP_GAIN       0x2A06
#define OBJ_HYDR_RX_DCBUS_I         0x2A07
#define OBJ_TCM_OUTPUTS             0x2A08

/* ITA Analog Outputs Manufacturer Specific 0x2B00 - 0x2BFF   */
#define OBJ_ANALYZER_M3_PWM        0x2B05
#define OBJ_PWM_OUT                0x2B16
#define OBJ_TRACTION_CURRENT       0x2B17
#define OBJ_PWM_OUT_CURRENT        0x2B18
#define OBJ_ANALYZER_ANALOG_OUTPUT 0x2B19

/* ITA Vehicle Information 0x2C00 - 0x2C05  Items must match ITA spec in this region  */

/* ITA Vehicle Information Manufacturer Specific 0x2D00 - 0x2DFF   */
#define OBJ_SDO_TOLKEN           0x2D00
#define OBJ_STATE_OSM            0x2D01
#define OBJ_VEH_ENABLE_MASK      0x2D02
#define OBJ_TEST_OUTPUTS         0x2D09
#define OBJ_LOCAL_OSM_STATE      0x2D0A
#define OBJ_LOCAL_OSM_TEST       0x2D0B
#define OBJ_ODM_SFWR_CRC         0x2D0C
#define OBJ_ODM_SFWR_CKS         0x2D0D
#define OBJ_OSM_ERRORS           0x2D0E
#define OBJ_ACCESS2_PSWD         0x2D10
#define OBJ_ACCESS3_PSWD         0x2D11
#define OBJ_TCM_SFWR_PN          0x2D12
#define OBJ_VCM_SFWR_PN          0x2D13
#define OBJ_SCM_SFWR_PN          0x2D14
#define OBJ_GCM_SFWR_PN          0x2D15
#define OBJ_TCM_SFWR_CRC         0x2D16
#define OBJ_TCM_SFWR_CKS         0x2D17
#define OBJ_TCM_HWPN_PN_SPLIT    0x2D18
#define OBJ_TCM_SWPN_PN_SPLIT    0x2D19
#define OBJ_TCM_MODULE_PN_SPLIT  0x2D1A
#define OBJ_HCM_SFWR_PN          0x2D1B
#define OBJ_HDM_SFWR_PN          0x2D1C
#define OBJ_SDM_SFWR_PN          0x2D1D

#define OBJ_SERVICE_PASSWORD     0x2DFE
#define OBJ_V2_SETUP_DATA_REQ    0x2DFF

/* ITA Device Information 0x2E00 - 0x2E02   Items must match ITA spec in this region */

/* ITA Device Information Manufacturer Specific 0x2F00 - 0x2FFF   */
#define OBJ_LOCAL_EV_HIST        0x2F00
#define OBJ_MOD_HOUR_METERS      0x2F01
#define OBJ_RUN_HOUR_METERS      0x2F02
#define OBJ_ODOMETER             0x2F03
#define OBJ_MISC_HOUR_METERS     0x2F04
#define OBJ_PM_HOUR_METER        0x2F05
#define OBJ_TRADEMARK            0x2F08
#define OBJ_STEER_MOTOR_HR_METER 0x2F09 // Temporary only should be deleted next rev
#define OBJ_TRACT_MOTOR_HR_METER 0x2F0A // Temporary only should be deleted next rev
#define OBJ_TCM_MOD_HOUR_METER   0x2F0B // Temporary only should be deleted next rev


/* Crown Specific */
#define OBJ_ASM_OP_READY         0x3000
#define OBJ_EMY_MSG_ACK          0x3001
#define OBJ_CAL_MODE             0x3002
#define OBJ_CAL_ACTION_COMPLETE  0x3003
#define OBJ_VEH_PEDOLOGY         0x3004
#define OBJ_VCM_PEDOLOGY         0x3005
#define OBJ_CAL_PV_FORMAT        0x3006
#define OBJ_CAL_DE_FORMAT        0x3007
#define OBJ_FRESH_VEH_STAT       0x3010
#define OBJ_FRESH_SCM            0x3011
#define OBJ_FRESH_VCM            0x3012
#define OBJ_FRESH_VCM_PDO2       0x3013
#define OBJ_FRESH_TCM_PDO1       0x3014
#define OBJ_M1_SPEED_RPM         0x3015
#define OBJ_FLUSH_HYDR_SYSTEM    0x3020
#define OBJ_FLUSH_PRIME_DONE     0x3021
#define OBJ_EV_TO_SET_TEST       0x3024
#define OBJ_EV_TO_CLR_TEST       0x3025
#define OBJ_INFO1_DISABLE        0x312A
#define OBJ_HOUR_BLOCK_READ      0x312B
#define OBJ_TESTER_PDO1          0x31E0
#define OBJ_TESTER_PDO2          0x31E1
#define OBJ_ICON_TEST            0x31E2
#define OBJ_DATALOG_REQ          0x31F0
#define OBJ_DATALOG_DATA1        0x31F1
#define OBJ_DATALOG_DATA2        0x31F2
#define OBJ_UNUSED               0x31F3
#define OBJ_ZEROS                0x31F4
#define OBJ_ALARM_CTRL           0x31F5
#define OBJ_DATALOG_DATA3        0x31F6
#define OBJ_DATALOG_DATA4        0x31F7
#define OBJ_DATASET_CNTL         0x31F8
#define OBJ_DATASET_DATA         0x31F9
#define OBJ_ANALYZER_DATA        0x31FF
#define OBJ_CALIB_CNTL           0x3200
#define OBJ_CALIB_DATA           0x3201
#define OBJ_PERF_CNTL            0x3202
#define OBJ_PERF_DATA            0x3203
#define OBJ_PARAM_CNTL           0x3204
#define OBJ_PARAM_DATA           0x3205
#define OBJ_PARAM_DATA2          0x3206
#define OBJ_PARAM_DATA3          0x3207
#define OBJ_PARAM_DATA4          0x3208
#define OBJ_PARAM_DATA5          0x3209
#define OBJ_PARAM_DATA6          0x320A
#define OBJ_PARAM_DATA7          0x320B
#define OBJ_PARAM_DATA8          0x320C
#define OBJ_PARAM_DATA9          0x320D
#define OBJ_PARAM_DATA10         0x320E
#define OBJ_PARAM_DATA11         0x320F
#define OBJ_PARAM_DATA12         0x3210
#define OBJ_FEATURE_LIST         0x321A
#define OBJ_FEATURE_CNTL         0x321B
#define OBJ_FEATURE_DATA         0x321C
#define OBJ_USER_SETUP_CNTL      0x321D
#define OBJ_USER_SETUP_DATA      0x321E
#define OBJ_MONITOR_CNTL         0x3220
#define OBJ_MONITOR_DATA         0x3221
#define OBJ_SETUP_CNTL           0x3230
#define OBJ_SETUP_DATA           0x3231
#define OBJ_RUNTIME_CAL_CNTL     0x3232
#define OBJ_RUNTIME_CAL_DATA     0x3233
#define OBJ_TIME_CNTL            0x3240
#define OBJ_TIME_DATA            0x3241
#define OBJ_SCM_CANOPEN_EMGCY    0x3242
#define OBJ_GCM_CANOPEN_EMGCY    0x3243
#define OBJ_DM_CANOPEN_EMGCY     0x3244
#define OBJ_VCM_CANOPEN_EMGCY    0x3245
#define OBJ_ACM_CANOPEN_EMGCY    0x3246
#define OBJ_PDO_STATUS           0x3247
#define OBJ_DATALOG_BLOCK        0x3248
#define OBJ_CLONE_SYSTEM         0x3250
#define OBJ_PERSISTANT_DATA      0x3251

#define OBJ_ACM_STATUS           0x3860
#define OBJ_ACM_COMMAND          0x3861
#define OBJ_ACM_ENABLE           0x3862
#define OBJ_TILT_POS             0x3863
#define OBJ_REACH_POS            0x3864
#define OBJ_ACM_FORK_HEIGHT      0x3865
#define OBJ_ACM_FORK_SPEED       0x3866
#define OBJ_ACM_SWITCHES         0x3867
#define OBJ_JUNK                 0x386F
#define OBJ_CAN_KEYBOARD         0x3870 
#define OBJ_CAN_KEYBOARD_RCV     0x3871 

#endif

