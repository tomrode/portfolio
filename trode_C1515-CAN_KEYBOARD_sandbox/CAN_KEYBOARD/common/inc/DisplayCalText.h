/******************************************************************************
 *  File:       DisplayCalText.h
 *
 *  Purpose:    Defines the indexes of various text messages used for
 *              calibration
 *
 *  Project:    C584
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 3/31/2004
 *              Revised
 *
 ******************************************************************************
*/
/******************************************************************************
 *  File:       DisplayCalText.h
 *
 *  Purpose:    Defines the indexes of various text messages used for
 *              calibration
 *
 *  Project:    C584
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 3/31/2004
 *              Revised
 *
 ******************************************************************************
*/

// LHM load sensor calibration text IDs
#define ACMSG_LS_RAISE_SELECT_FOOT     0
#define ACMSG_LS_LOWER_SELECT_FOOT     1
#define ACMSG_LS_RAISE_ABV_STG         2
#define ACMSG_LS_PLEASE_RAISE          3
#define ACMSG_LS_STOP_CMD              4
#define ACMSG_LS_STABILIZE             5
#define ACMSG_LS_PASS                  6
#define ACMSG_LS_FAIL                  7

// Main Lower valve calibration text IDs
#define ACMSG_MAIN_LOWER_VALVE_BEGIN      0
#define ACMSG_MAIN_LOWER_VALVE_SUSPENDED  1
#define ACMSG_MAIN_LOWER_VALVE_FIND_BO    2  // BO
#define ACMSG_MAIN_LOWER_VALVE_FIND_SPD1  3  // Code depends on IDs SPD1-8 to be contiguous -2"/sec
#define ACMSG_MAIN_LOWER_VALVE_FIND_SPD2  4  // Code depends on IDs SPD1-8 to be contiguous -4"/sec
#define ACMSG_MAIN_LOWER_VALVE_FIND_SPD3  5  // Code depends on IDs SPD1-8 to be contiguous -8"/sec
#define ACMSG_MAIN_LOWER_VALVE_FIND_SPD4  6  // Code depends on IDs SPD1-8 to be contiguous -12"/sec
#define ACMSG_MAIN_LOWER_VALVE_FIND_SPD5  7  // Code depends on IDs SPD1-8 to be contiguous -Limit
#define ACMSG_MAIN_LOWER_VALVE_FIND_SPD6  8  // Code depends on IDs SPD1-8 to be contiguous -Spare
#define ACMSG_MAIN_LOWER_VALVE_FIND_SPD7  9  // Code depends on IDs SPD1-8 to be contiguous -Spare
#define ACMSG_MAIN_LOWER_VALVE_STOP      10
#define ACMSG_MAIN_LOWER_VALVE_PASSED    11
#define ACMSG_MAIN_LOWER_VALVE_FAILED    12

// Tilt valve calibration text IDs
#define ACMSG_TILT_VALVE_SEL_TILT         0
#define ACMSG_TILT_VALVE_SUSPENDED        1
#define ACMSG_TILT_START_POINT            2
#define ACMSG_TILT_VALVE_LOWER            3
#define ACMSG_TILT_VALVE_FIND_BO          4
#define ACMSG_TILT_VALVE_RETURN_BACK      5
#define ACMSG_TILT_VALVE_FIND_SPD         6
#define ACMSG_TILT_VALVE_STOP             7
#define ACMSG_TILT_VALVE_PASSED           8
#define ACMSG_TILT_VALVE_FAILED           9

// Tilt sensor calibration text IDs
#define ACMSG_TILT_SENSOR_TILT_BACK       0
#define ACMSG_TILT_SENSOR_TILT_DOWN       1
#define ACMSG_TILT_SENSOR_TILT_ZERO       2
#define ACMSG_TILT_SENSOR_DEF_YES         3
#define ACMSG_TILT_SENSOR_DEF_NO          4
#define ACMSG_TILT_SENSOR_DEF_USED        5
#define ACMSG_TILT_SENSOR_TILT_LEVEL      6
#define ACMSG_TILT_SENSOR_PASSED          7
#define ACMSG_TILT_SENSOR_FAILED          8

// hydraulic steer sensor calibration text IDs
#define ACMSG_HYDRO_STEER_SENSOR_STEER_ZERO     0  /* Steer to Zero position */
#define ACMSG_HYDRO_STEER_SENSOR_STEER_CW       1  /* Steer Full CW */
#define ACMSG_HYDRO_STEER_SENSOR_STEER_CCW      2  /* Steer Full CCW */
#define ACMSG_HYDRO_STEER_SENSOR_PASSED         3
#define ACMSG_HYDRO_STEER_SENSOR_FAILED         4

// Hoist throttle calibration text IDs
#define ACMSG_HOIST_THR_RAISE             0
#define ACMSG_HOIST_THR_LOWER             1
#define ACMSG_HOIST_THR_NEUTRAL           2
#define ACMSG_HOIST_THR_PASSED            3
#define ACMSG_HOIST_THR_FAILED            4

// Accessory throttle calibration text IDs
#define ACMSG_ACCY_THR_RAISE              0
#define ACMSG_ACCY_THR_LOWER              1
#define ACMSG_ACCY_THR_NEUTRAL            2
#define ACMSG_ACCY_THR_PASSED             3
#define ACMSG_ACCY_THR_FAILED             4

// DM traction throttle calibration text IDs
#define ACMSG_TRAC_THROT_FWD              0
#define ACMSG_TRAC_THROT_REV              1
#define ACMSG_TRAC_THROT_NEU              2
#define ACMSG_TRAC_THROT_PASSED           3
#define ACMSG_TRAC_THROT_FAILED           4

// Electric steer sensor calibration text IDs
#define ACMSG_E_STEER_SENSOR_STEER_CW       0  /* Steer Full CW             */
#define ACMSG_E_STEER_SENSOR_STEER_CCW      1  /* Steer Full CCW            */
#define ACMSG_E_STEER_SENSOR_STEER_ZERO     2  /* Steer to Zero position    */
#define ACMSG_E_STEER_SENSOR_PASSED         3  /* Cal Values accepted       */
#define ACMSG_E_STEER_SENSOR_FAILED         4  /* Cal Value in error        */
#define ACMSG_E_STEER_SENSOR_SAVE_N         5  
#define ACMSG_E_STEER_SENSOR_SAVE_Y         6  

// DM brake pot calibration text IDs
#define ACMSG_BRAKE_POT_PRESS            0
#define ACMSG_BRAKE_POT_RELEASE          1
#define ACMSG_BRAKE_POT_PASSED           2
#define ACMSG_BRAKE_POT_FAILED           3

// Proportional Relief valve(prva) calibration
#define ACMSG_PRVA_CONNECT_GAGE            0 // Connect Gage, Press Enter
#define ACMSG_PRVA_MAINTAIN_TILT          1 // Maintain Tilt Command
#define ACMSG_PRVA_MONITOR_GAGE           2 // Monitor Gage
#define ACMSG_PRVA_STOP_TILT              3 // Stop Tilt
#define ACMSG_PRVA_RECORD_GAGE            4 // Press to enter gage pressure
#define ACMSG_PRVA_PASSED                 5 // Calibration Accepted
#define ACMSG_PRVA_FAILED                 6 // Calibration failed

// Zapi Battery adjust calibration
#define ACMSG_BATTCAL_CONNECT_METER       0 // Connect a meter, Press Enter
#define ACMSG_BATTCAL_PRESS_ADJ           1 // Press Up/Down until Display matches meter
#define ACMSG_BATTCAL_INPUT               2 // Bv=XX.XX
#define ACMSG_BATT_ADJ_PASSED             3 // Calib Passed
#define ACMSG_BATT_ADJ_FAILED             4 // Calib failed


#define ACMSG_SPL_REQ_LOWER          0  // Lower below staging to begin
#define ACMSG_SPL_FINDSW             1  // Finding switch, maintain command
#define ACMSG_SPL_FINDBO             2  // Finding Breakout, maintain command
#define ASMSG_SPL_STOP_CMD           3  // Stop Command
#define ACMSG_SPL_RAISE_TO_MAX       4  // Raise to max height, Then lower
#define ACMSG_SPL_FINDING_PT1        5  // Finding point, maintain lower
#define ACMSG_SPL_FINDING_PT2        6  // Finding point, maintain lower
#define ACMSG_SPL_FINDING_PT3        7  // Finding point, maintain lower
#define ACMSG_SPL_FINDING_PT4        8  // Finding point, maintain lower
#define ACMSG_SPL_FINDING_PT5        9  // Finding point, maintain lower
#define ACMSG_SPL_FINDING_PT6       10  // Finding point, maintain lower
#define ACMSG_SPL_PASSED            11  // Calib Passed
#define ACMSG_SPL_FAILED            12  // Calib failed

// Accelerometer Cal Text
#define ACMSG_ACCEL_LEVEL           0   // Level the truck
#define ACMSG_ACCEL_AVG             1   // Finding average
#define ACMSG_ACCEL_REVERSE         2   // Reverse truck position
#define ACMSG_ACCEL_SET_BRAKE       3   // Set Truck Brake
#define ACMSG_ACCEL_PASSED          4
#define ACMSG_ACCEL_FAILED          5


#define MAIN_HGT_SENSOR_RAISE_RESET   0
#define MAIN_HGT_SENSOR_RAISE_MAX     1
#define MAIN_HGT_SENSOR_ENTER_CALHT   2
#define MAIN_HGT_SENSOR_PASS          3
#define MAIN_HGT_SENSOR_FAIL          4


#define ACMSG_REACH_SENSOR_REACH_RETRACT  0
#define ACMSG_REACH_SENSOR_REACH_EXTEND   1
#define ACMSG_REACH_SENSOR_PASSED         2
#define ACMSG_REACH_SENSOR_FAILED         3

