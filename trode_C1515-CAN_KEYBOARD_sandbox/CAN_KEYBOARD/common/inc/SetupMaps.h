/******************************************************************************
 *  File:       SetupMaps.h
 *
 *  Purpose:    Defines truck setup values shared between truck modules.
 *
 *  Project:    C584
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     David J. Obringer
 *
 *  Revision History:
 *              Written 02/08/2005
 *              Revised
 *
 ******************************************************************************
*/
#ifndef setupmaps_registration
   /****************************************************************************
    *                                                                          *
    *  G l o b a l   S c o p e   S e c t i o n                                 *
    *                                                                          *
    ***************************************************************************/

    // Defines

    typedef enum
    {
        DR_GROUP1  = 0,
        DR_GROUP2  = 1,
        DR_GROUP3  = 2,
        DR_GROUP4  = 3,
        DR_GROUP5  = 4,
        DR_GROUP6  = 5,
        DR_GROUP7  = 6,
        DR_MAX     = 7
    }DR_TYPE;


    /*  Request bits used to request data from OSM master. This is used as an
     *  index into the ulDataRequestBits[] array which is mapped to object
     *  OBJ_V2_SETUP_DATA_REQ in the object dictionary. If this section changes
     *  be sure to update the object range table in OsmMasterUser.h.
     */

    /* Request bits for features data subindex 1 - DR_GROUP1 */
    #define REQ_FTR_LANGUAGE                0x00000001 /*    1   */
    #define REQ_FTR_METRIC                  0x00000002 /*    2   */
    #define REQ_FTR_REACH_TYPE              0x00000004 /*    3   */
    #define REQ_FTR_MODEL_NUMBER            0x00000008 /*    4   */
    #define REQ_FTR_MAIN_WGT_MAX            0x00000010 /*    5   */
    #define REQ_FTR_TRUCK_TYPE              0x00000020 /*    6   */
    #define REQ_FTR_MAIN_HGT_MAX            0x00000040 /*    7   */
    #define REQ_FTR_SIDESHIFT_TYPE          0x00000080 /*    8   */
    #define REQ_FTR_DISPLAY_TYPE            0x00000100 /*    9   */
    #define REQ_FTR_STEER_DIRECTION         0x00000200 /*   10   */
    #define REQ_FTR_ADD_ONS                 0x00000400 /*   11   */
    #define REQ_FTR_TRUCK_WEIGHT            0x00000800 /*   12   */
    #define REQ_FTR_TILT                    0x00001000 /*   13   */
    #define REQ_FTR_CDM_NUM_ZONES           0x00002000 /*   14   */
    #define REQ_FTR_CDM_ZONE0_HGT           0x00004000 /*   15   */
    #define REQ_FTR_CDM_ZONE0_WGT           0x00008000 /*   16   */
    #define REQ_FTR_CDM_ZONE1_HGT           0x00010000 /*   17   */
    #define REQ_FTR_CDM_ZONE1_WGT           0x00020000 /*   18   */
    #define REQ_FTR_CDM_ZONE2_HGT           0x00040000 /*   19   */
    #define REQ_FTR_CDM_ZONE2_WGT           0x00080000 /*   20   */
    #define REQ_FTR_CDM_ZONE3_HGT           0x00100000 /*   21   */
    #define REQ_FTR_CDM_ZONE3_WGT           0x00200000 /*   22   */
    #define REQ_FTR_CHAIN_SLACK             0x00400000 /*   23   */
    #define REQ_FTR_TRAV_ALARM              0x00800000 /*   24   */
    #define REQ_FTR_BATT_RETAIN             0x01000000 /*   25   */
    #define REQ_FTR_MODULE_FAN              0x02000000 /*   26   */
    #define REQ_FTR_MOTOR_FAN               0x04000000 /*   27   */
    #define REQ_FTR_SPEED_CUTBACK           0x08000000 /*   28   */
    #define REQ_FTR_MESSAGE_MODE            0x10000000 /*   29   */
    #define REQ_FTR_USER_PERF               0x20000000 /*   30   */
    #define REQ_FTR_SPARE                   0x40000000 /*   31   */
    #define REQ_FTR_CLPSD_HEIGHT            0x80000000 /*   32   */

    /* Request bits for features data subindex 2 - DR_GROUP2  */
    #define REQ_FTR_USER_RACK               0x00000001 /*    1   */
    #define REQ_FTR_TILT_ASSIST             0x00000002 /*    2   */
    #define REQ_FTR_FORK_HOME_TRV           0x00000004 /*    3   */
    #define REQ_FTR_ZONESW_HGT              0x00000008 /*    4   */
    #define REQ_FTR_HOUR_MODE               0x00000010 /*    5   */
    #define REQ_FTR_SLIP_CTRL               0x00000020 /*    6   */
    #define REQ_FTR_GRADE_CTRL              0x00000040 /*    7   */
    #define REQ_FTR_HIGH_SPD_LOWER          0x00000080 /*    8   */
    #define REQ_FTR_GROUP2_SPARE1           0x00000100 /*    9   */
    #define REQ_FTR_ZONESW_SPD              0x00000200 /*   10   */
    #define REQ_FTR_GROUP2_SPARE3           0x00000400 /*   11   */
    #define REQ_FTR_GROUP2_SPARE4           0x00000800 /*   12   */
    #define REQ_FTR_PIN_TIMEOUT             0x00001000 /*   13   */
    #define REQ_FTR_PIN_ENABLE              0x00002000 /*   14   */
    #define REQ_FTR_PIN_LEV_0               0x00004000 /*   15   */
    #define REQ_FTR_PIN_COD_0               0x00008000 /*   16   */
    #define REQ_FTR_PIN_LEV_1               0x00010000 /*   17   */
    #define REQ_FTR_PIN_COD_1               0x00020000 /*   18   */
    #define REQ_FTR_PIN_LEV_2               0x00040000 /*   19   */
    #define REQ_FTR_PIN_COD_2               0x00080000 /*   20   */
    #define REQ_FTR_PIN_LEV_3               0x00100000 /*   21   */
    #define REQ_FTR_PIN_COD_3               0x00200000 /*   22   */
    #define REQ_FTR_PIN_LEV_4               0x00400000 /*   23   */
    #define REQ_FTR_PIN_COD_4               0x00800000 /*   24   */
    #define REQ_FTR_PIN_LEV_5               0x01000000 /*   25   */
    #define REQ_FTR_PIN_COD_5               0x02000000 /*   26   */
    #define REQ_FTR_PIN_LEV_6               0x04000000 /*   27   */
    #define REQ_FTR_PIN_COD_6               0x08000000 /*   28   */
    #define REQ_FTR_PIN_LEV_7               0x10000000 /*   29   */
    #define REQ_FTR_PIN_COD_7               0x20000000 /*   30   */
    #define REQ_FTR_PIN_LEV_8               0x40000000 /*   31   */
    #define REQ_FTR_PIN_COD_8               0x80000000 /*   32   */

    /* Request bits for features data subindex 3 - DR_GROUP3 */
    #define REQ_FTR_PIN_LEV_9               0x00000001  /*   1   */
    #define REQ_FTR_PIN_COD_9               0x00000002  /*   2   */
    #define REQ_FTR_PIN_LEV_10              0x00000004  /*   3   */
    #define REQ_FTR_PIN_COD_10              0x00000008  /*   4   */
    #define REQ_FTR_PIN_LEV_11              0x00000010  /*   5   */
    #define REQ_FTR_PIN_COD_11              0x00000020  /*   6   */
    #define REQ_FTR_PIN_LEV_12              0x00000040  /*   7   */
    #define REQ_FTR_PIN_COD_12              0x00000080  /*   8   */
    #define REQ_FTR_PIN_LEV_13              0x00000100  /*   9   */
    #define REQ_FTR_PIN_COD_13              0x00000200  /*  10   */
    #define REQ_FTR_PIN_LEV_14              0x00000400  /*  11   */
    #define REQ_FTR_PIN_COD_14              0x00000800  /*  12   */
    #define REQ_FTR_PIN_LEV_15              0x00001000  /*  13   */
    #define REQ_FTR_PIN_COD_15              0x00002000  /*  14   */
    #define REQ_FTR_PIN_LEV_16              0x00004000  /*  15   */
    #define REQ_FTR_PIN_COD_16              0x00008000  /*  16   */
    #define REQ_FTR_PIN_LEV_17              0x00010000  /*  17   */
    #define REQ_FTR_PIN_COD_17              0x00020000  /*  18   */
    #define REQ_FTR_PIN_LEV_18              0x00040000  /*  19   */
    #define REQ_FTR_PIN_COD_18              0x00080000  /*  20   */
    #define REQ_FTR_PIN_LEV_19              0x00100000  /*  21   */
    #define REQ_FTR_PIN_COD_19              0x00200000  /*  22   */
    #define REQ_FTR_PIN_LEV_20              0x00400000  /*  23   */
    #define REQ_FTR_PIN_COD_20              0x00800000  /*  24   */
    #define REQ_FTR_PIN_LEV_21              0x01000000  /*  25   */
    #define REQ_FTR_PIN_COD_21              0x02000000  /*  26   */
    #define REQ_FTR_PIN_LEV_22              0x04000000  /*  27   */
    #define REQ_FTR_PIN_COD_22              0x08000000  /*  28   */
    #define REQ_FTR_PIN_LEV_23              0x10000000  /*  29   */
    #define REQ_FTR_PIN_COD_23              0x20000000  /*  30   */
    #define REQ_FTR_PIN_LEV_24              0x40000000  /*  31   */
    #define REQ_FTR_PIN_COD_24              0x80000000  /*  32   */

    /* Request bits for features data subindex 4 - DR_GROUP4 */
    #define REQ_CUTOUT1_HEIGHT              0x00000001  /*   1    */
    #define REQ_CUTOUT1_TYPE                0x00000002  /*   2    */
    #define REQ_CUTOUT1_ZONE                0x00000004  /*   3    */
    #define REQ_CUTOUT1_TRIGGER             0x00000008  /*   4    */
    #define REQ_CUTOUT1_MPH                 0x00000010  /*   5    */
    #define REQ_CUTOUT2_HEIGHT              0x00000020  /*   6    */
    #define REQ_CUTOUT2_TYPE                0x00000040  /*   7    */
    #define REQ_CUTOUT2_ZONE                0x00000080  /*   8    */
    #define REQ_CUTOUT2_TRIGGER             0x00000100  /*   9    */
    #define REQ_CUTOUT2_MPH                 0x00000200  /*  10    */
    #define REQ_CUTOUT3_HEIGHT              0x00000400  /*  11    */
    #define REQ_CUTOUT3_TYPE                0x00000800  /*  12    */
    #define REQ_CUTOUT3_ZONE                0x00001000  /*  13    */
    #define REQ_CUTOUT3_TRIGGER             0x00002000  /*  14    */
    #define REQ_CUTOUT3_MPH                 0x00004000  /*  15    */
    #define REQ_CUTOUT4_HEIGHT              0x00008000  /*  16    */
    #define REQ_CUTOUT4_TYPE                0x00010000  /*  17    */
    #define REQ_CUTOUT4_ZONE                0x00020000  /*  18    */
    #define REQ_CUTOUT4_TRIGGER             0x00040000  /*  19    */
    #define REQ_CUTOUT4_MPH                 0x00080000  /*  20    */
    #define REQ_CUTOUT5_HEIGHT              0x00100000  /*  21    */
    #define REQ_CUTOUT5_TYPE                0x00200000  /*  22    */
    #define REQ_CUTOUT5_ZONE                0x00400000  /*  23    */
    #define REQ_CUTOUT5_TRIGGER             0x00800000  /*  24    */
    #define REQ_CUTOUT5_MPH                 0x01000000  /*  25    */
    #define REQ_CUTOUT6_HEIGHT              0x02000000  /*  26    */
    #define REQ_CUTOUT6_TYPE                0x04000000  /*  27    */
    #define REQ_CUTOUT6_ZONE                0x08000000  /*  28    */
    #define REQ_CUTOUT6_TRIGGER             0x10000000  /*  29    */
    #define REQ_CUTOUT6_MPH                 0x20000000  /*  30    */
    #define REQ_GROUP4_SPARE1               0x40000000  /*  31    */
    #define REQ_GROUP4_SPARE2               0x80000000  /*  32    */

    /* Request bits for features data subindex 5 - DR_GROUP5 */
    #define REQ_PRF_P1_TRAV_ACCEL           0x00000001  /*   1    */
    #define REQ_PRF_P1_MAX_EMPTY_FWD_SPD    0x00000002  /*   2    */
    #define REQ_PRF_P1_MAX_EMPTY_REV_SPD    0x00000004  /*   3    */
    #define REQ_PRF_P1_MAX_LOAD_FWD_SPD     0x00000008  /*   4    */
    #define REQ_PRF_P1_MAX_LOAD_REV_SPD     0x00000010  /*   5    */
    #define REQ_PRF_P1_RAISE_SPD            0x00000020  /*   6    */
    #define REQ_PRF_P1_LOWER_SPD            0x00000040  /*   7    */
    #define REQ_PRF_P1_LIFT_POWER           0x00000080  /*   8    */
    #define REQ_PRF_P1_LOAD_WEIGHT          0x00000100  /*   9    */
    #define REQ_PRF_P2_TRAV_ACCEL           0x00000200  /*  10    */
    #define REQ_PRF_P2_MAX_EMPTY_FWD_SPD    0x00000400  /*  11    */
    #define REQ_PRF_P2_MAX_EMPTY_REV_SPD    0x00000800  /*  12    */
    #define REQ_PRF_P2_MAX_LOAD_FWD_SPD     0x00001000  /*  13    */
    #define REQ_PRF_P2_MAX_LOAD_REV_SPD     0x00002000  /*  14    */
    #define REQ_PRF_P2_RAISE_SPD            0x00004000  /*  15    */
    #define REQ_PRF_P2_LOWER_SPD            0x00008000  /*  16    */
    #define REQ_PRF_P2_LIFT_POWER           0x00010000  /*  17    */
    #define REQ_PRF_P2_LOAD_WEIGHT          0x00020000  /*  18    */
    #define REQ_PRF_P3_TRAV_ACCEL           0x00040000  /*  19    */
    #define REQ_PRF_P3_MAX_EMPTY_FWD_SPD    0x00080000  /*  20    */
    #define REQ_PRF_P3_MAX_EMPTY_REV_SPD    0x00100000  /*  21    */
    #define REQ_PRF_P3_MAX_LOAD_FWD_SPD     0x00200000  /*  22    */
    #define REQ_PRF_P3_MAX_LOAD_REV_SPD     0x00400000  /*  23    */
    #define REQ_PRF_P3_RAISE_SPD            0x00800000  /*  24    */
    #define REQ_PRF_P3_LOWER_SPD            0x01000000  /*  25    */
    #define REQ_PRF_P3_LIFT_POWER           0x02000000  /*  26    */
    #define REQ_PRF_P3_LOAD_WEIGHT          0x04000000  /*  27    */
    #define REQ_PRF_BDI                     0x08000000  /*  28    */
    #define REQ_PRF_PLUG                    0x10000000  /*  29    */
    #define REQ_PRF_DRIVE_BRAKE             0x20000000  /*  30    */
    #define REQ_PRF_CASTER_BRAKE            0x40000000  /*  31    */
    #define REQ_PRF_COAST                   0x80000000  /*  32    */

    /* Request bits for features data subindex 1 - DR_GROUP6 */
    #define REQ_PRF_ACCY_SPD_LT_FLS         0x00000001  /*   1    */
    #define REQ_PRF_ACCY_SPD_GT_FLS         0x00000002  /*   2    */
    #define REQ_PRF_ACCY_SPD_GT_270         0x00000004  /*   3    */
    #define REQ_PRF_TRVL_SPD_GT_FLS         0x00000008  /*   4    */
    #define REQ_PRF_TRVL_SPD_GT_270         0x00000010  /*   5    */
    #define REQ_PRF_ACCY_RAMP_ACCEL         0x00000020  /*   6    */
    #define REQ_PRF_ACCY_RAMP_DECEL         0x00000040  /*   7    */
    #define REQ_PRF_HOIST_RAISE_ACCEL       0x00000080  /*   8    */
    #define REQ_PRF_HOIST_RAISE_DECEL       0x00000100  /*   9    */
    #define REQ_PRF_HOIST_LOWER_ACCEL       0x00000200  /*  10    */
    #define REQ_PRF_HOIST_LOWER_DECEL       0x00000400  /*  11    */
    #define REQ_PRF_CUTBACK_MAX_SPD         0x00000800  /*  12    */
    #define REQ_PRF_CUTBACK_LOAD_WGT        0x00001000  /*  13    */
    #define REQ_PRF_CUTBACK_HEIGHT          0x00002000  /*  14    */
    #define REQ_GROUP6_SPARE1               0x00004000  /*  15    */
    #define REQ_RACKSEL_LOADED_OFFSET       0x00008000  /*  16    */
    #define REQ_PRF_CUTBACK_TRIGGER         0x00010000  /*  17    */
    #define REQ_PRF_CUTBACK_FORKHOME        0x00020000  /*  18    */
    #define REQ_FTR_STARTUP_INTERLOCK       0x00040000  /*  19    */
    #define REQ_FTR_REMOTE_RL               0x00080000  /*  20    */
    #define REQ_GROUP6_SPARE5               0x00100000  /*  21    */
    #define REQ_REGEN_LOWER                 0x00200000  /*  22    */
    #define REQ_GROUP6_SPARE7               0x00400000  /*  23    */
    #define REQ_GROUP6_SPARE8               0x00800000  /*  24    */
    #define REQ_GROUP6_SPARE9               0x01000000  /*  25    */
    #define REQ_GROUP6_SPARE10              0x02000000  /*  26    */
    #define REQ_GROUP6_SPARE11              0x04000000  /*  27    */
    #define REQ_GROUP6_SPARE12              0x08000000  /*  28    */
    #define REQ_GROUP6_SPARE13              0x10000000  /*  29    */
    #define REQ_ESTR_CAL_CW_CNTS            0x20000000  /*  30    */
    #define REQ_ESTR_CAL_CCW_CNTS           0x40000000  /*  31    */
    #define REQ_ESTR_CAL_ZERO_CNTS          0x80000000  /*  32    */

    /* Request bits for features data subindex 1 - DR_GROUP7 */
    #define REQ_ALL_SETUPS_CRC              0x00000001  /*   1    */
    #define REQ_ALL_SETUPS_REV              0x00000002  /*   2    */
    #define REQ_FTRS_CRC                    0x00000004  /*   3    */
    #define REQ_FTRS_REV                    0x00000008  /*   4    */
    #define REQ_PERFS_CRC                   0x00000010  /*   5    */
    #define REQ_PERFS_REV                   0x00000020  /*   6    */
    #define REQ_CUTOUTS_CRC                 0x00000040  /*   7    */
    #define REQ_CUTOUTS_REV                 0x00000080  /*   8    */
    #define REQ_RACKSELECT_1                0x00000100  /*   9    */
    #define REQ_RACKSELECT_2                0x00000200  /*  10    */
    #define REQ_RACKSELECT_3                0x00000400  /*  11    */
    #define REQ_RACKSELECT_4                0x00000800  /*  12    */
    #define REQ_RACKSELECT_5                0x00001000  /*  13    */
    #define REQ_RACKSELECT_6                0x00002000  /*  14    */
    #define REQ_RACKSELECT_7                0x00004000  /*  15    */
    #define REQ_RACKSELECT_8                0x00008000  /*  16    */
    #define REQ_RACKSELECT_9                0x00010000  /*  17    */
    #define REQ_RACKSELECT_10               0x00020000  /*  18    */
    #define REQ_RACKSELECT_11               0x00040000  /*  19    */
    #define REQ_RACKSELECT_12               0x00080000  /*  20    */
    #define REQ_RACKSELECT_13               0x00100000  /*  21    */
    #define REQ_RACKSELECT_14               0x00200000  /*  22    */
    #define REQ_RACKSELECT_15               0x00400000  /*  23    */
    #define REQ_RACKSELECT_16               0x00800000  /*  24    */
    #define REQ_RACKSELECT_17               0x01000000  /*  25    */
    #define REQ_RACKSELECT_18               0x02000000  /*  26    */
    #define REQ_RACKSELECT_19               0x04000000  /*  27    */
    #define REQ_RACKSELECT_20               0x08000000  /*  28    */
    #define REQ_RACKSELECT_21               0x10000000  /*  29    */
    #define REQ_RACKSELECT_22               0x20000000  /*  30    */
    #define REQ_RACKSELECT_23               0x40000000  /*  31    */
    #define REQ_RACKSELECT_24               0x80000000  /*  32    */

    /* Feature setups mapping definitions*/
    typedef enum
    {
        NO_ALARM    = 0,
        FWD_ALARM   = 1,
        REV_ALARM   = 2,
        BOTH_ALARM  = 3
    } ALARM_TYPE;

    typedef enum
    {
        FAN_OFF         = 0,
        FAN_THERMAL     = 1,
        FAN_ALWAYS      = 2
    } FANS_CONFIG_TYPE;

    typedef enum
    {
        BATT_SW_NO  = 0,
        BATT_SW_COAST = 1,
        BATT_SW_WITH_HORN = 2,
        BATT_SW_BRAKE = 3,
        BATT_SW_BRAKE_WITH_HORN = 4
    } BATTERY_SWITCH_TYPE;

    typedef enum
    {
        USER_PERF_DISABLED = 0,
        USER_PERF_ENABLED =  1
    } USER_PERF_TYPE;

    typedef enum
    {
        USER_CODES_DISABLED = 0,
        USER_CODES_ENABLED  = 1
    } USER_CODES_TYPE;

    typedef enum
    {
        ZONE_SW_NO  = 0,
        ZONE_SW_YES = 1
    } ZONE_SWITCH_TYPE;

    typedef enum
    {
        DMM_NONE        = 0,
        DMM_TIMER       = 1,
        DMM_TRIP        = 2,
        DMM_HOURS       = 3,
        DMM_ODOMETER    = 4,
        DMM_SPD         = 5,
        DMM_MODEL       = 6,
        DMM_HEIGHT      = 7,
        DMM_WEIGHT      = 8,
        DMM_ALL         = 9
    } MSG_MODE_TYPE;

    typedef enum
    {
        TRK_OP_DISABLED = 0,
        TRK_OP_ENABLED  = 1
    } TRUCK_OPERATION_TYPE;

    typedef enum
    {
        HR_MTR_RTIME1       =  0,
        HR_MTR_RTIME2       =  1,
        HR_MTR_ACCESS1      =  2,
        HR_MTR_ACCESS3      =  3,
        HR_MTR_ACCESS4      =  4,
        HR_MTR_ACCESS5      =  5,
        HR_MTR_ACCESS6      =  6,
        HR_MTR_TRV_MOTOR    =  7,
        HR_MTR_LIFT_MOTOR   =  8,
        HR_MTR_PLAT_LOWER   =  9,
        HR_MTR_ODOMETER     = 10
    } HR_METER_SHOW_TYPE;

    typedef enum
    {
        DISABLED = 0,
        ENABLED  = 1
    } OFF_ON_TYPE;

    typedef enum
    {
        STEER_FWD   = 0,
        STEER_REV   = 1
    } STEER_DIRECTION_TYPE;

    typedef enum            /*   CUTOUTS   */
    {
        CUTOUT_NONE       = 0,
        CUTOUT_PCT        = 1,
        CUTOUT_PCT_LATCH  = 2,
        CUTOUT_PST        = 3,
        CUTOUT_NCT        = 4,
        CUTOUT_NCT_LATCH  = 5
    } CUTOUT_TYPE_TYPE;

    typedef enum
    {
        CUTOUT_ZONEA    = 0,
        CUTOUT_ZONEB    = 1,
        CUTOUT_ZONEC    = 2,
        CUTOUT_ALL      = 3
    } CUTOUT_ZONE_TYPE;

    typedef enum
    {
       TR_SW1    = 0,
       TR_SW2    = 1,
       TR_SW3    = 2,
       TR_SW4    = 3,
       TR_SENSOR = 4     //Sensor
    }SD_TRIGGER_TYPE;

    typedef enum
    {
        MAX_LIFT_3000  = 3000,
        MAX_LIFT_3500  = 3500,
        MAX_LIFT_4000  = 4000,
        MAX_LIFT_4500  = 4500,
        MAX_LIFT_5000  = 5000,
        MAX_LIFT_5500  = 5500
    }LIFT_CAPACITY_TYPE;

    typedef enum
    {
        MAX_HEIGHT_198  = 198,
        MAX_HEIGHT_210  = 210,
        MAX_HEIGHT_240  = 240,
        MAX_HEIGHT_270  = 270,
        MAX_HEIGHT_300  = 300,
        MAX_HEIGHT_321  = 321,
        MAX_HEIGHT_341  = 341,
        MAX_HEIGHT_366  = 366,
        MAX_HEIGHT_400  = 400,
        MAX_HEIGHT_421  = 421,
        MAX_HEIGHT_442  = 442
    }MAX_LIFT_HEIGHT;

    typedef enum
    {
        RIDER_REACH     = 0,
        DOUBLE_REACH    = 1
    }REACH_TYPE;

    typedef enum
    {
        SS_DISABLED = 0,
        SS_ENABLED  = 1
    }SIDESHIFT_TYPE;

    typedef enum
    {
        ENHANCED        = 0,
        ENHANCED_CDM    = 1
    }DISPLAY_TYPE;

    typedef enum
    {
        STANDARD_TRK    = 0,
        SPECIAL_TRK     = 1
    }TRUCK_TYPE;

    typedef enum
    {
        HI_SPEED_LOWER_OFF  = 0,
        HI_SPEED_LOWER_ON   = 1
    }HIGH_SPD_LOWER_TYPE;

    typedef enum
    {
        SLIP_CTRL_OFF   = 0,
        SLIP_CTRL_ON    = 1
    }SLIP_CONTROL_TYPE;

    typedef enum
    {
        GRADE_CTRL_OFF   = 0,
        GRADE_CTRL_ON    = 1
    }GRADE_CONTROL_TYPE;

    typedef enum
    {
        ADDON_STD               = 0,
        ADDON_STRB_LGHTS        = 1,
        ADDON_TRV_ALARM         = 2,
        ADDON_WORK_LGHTS        = 3,
        ADDON_STRB_ALARM        = 4,
        ADDON_WORK_STRB_LGHTS   = 5,
        ADDON_ALL               = 6,
        ADDON_ALARM_WORK        = 7
    }ADD_ONS_TYPE;

    typedef enum
    {
        DC_LIFT_SYSTEM = 0,
        AC_LIFT_SYSTEM = 1,
        NUM_MOTORS
    }LIFT_SYSTEM_TYPE;

    typedef enum
    {
        HYDR_STR_SYSTEM = 0,
        ELEC_STR_SYSTEM = 1
    }STEER_SYSTEM_TYPE;

    typedef enum
    {
        NO_MODEL_DP  = 0,
        MODEL_5715   = 1,
        MODEL_5725   = 2,
        MODEL_5795   = 3
    }DATA_PLATE_MODEL_TYPE;

    typedef enum
    {
        RACK_SELECT_OFF   = 0,
        RACK_SELECT_ON    = 1
    }RACK_SELECT_TYPE;

    /* Speed Cutback Enumerations */
    typedef enum
    {
        SPD_CUTBACK_HEIGHT_TRIGGER  = 0,
        SPD_CUTBACK_FLS_TRIGGER     = 1,
        SPD_CUTBACK_HGTRS2_TRIGGER  = 2,
        SPD_CUTBACK_HTS1_TRIGGER    = 3,
        SPD_CUTBACK_HTS2_TRIGGER    = 4,
        SPD_CUTBACK_LMS_TRIGGER     = 5
    }SPD_CUTBACK_TRIGGER_TYPE;

    typedef enum
    {
        SPD_CUTBACK_FORK_HOME_ENABLED  = 0,
        SPD_CUTBACK_FORK_HOME_DISABLED = 1
    }SPD_CUTBACK_FORK_HOME_TYPE;

    typedef enum
    {
        SPD_CUTBACK_WGT_ALWAYS  = 0,
        SPD_CUTBACK_WGT_EMPTY   = 1,
        SPD_CUTBACK_WGT_GT_500  = 2,
        SPD_CUTBACK_WGT_GT_1000 = 3,
        SPD_CUTBACK_WGT_GT_1500 = 4,
        SPD_CUTBACK_WGT_GT_2000 = 5,
        SPD_CUTBACK_WGT_GT_2500 = 6,
        SPD_CUTBACK_WGT_GT_3000 = 7,
        SPD_CUTBACK_WGT_GT_3500 = 8,
        SPD_CUTBACK_WGT_GT_4000 = 9
    }SPD_CUTBACK_WEIGHT_TYPE;

    /**********************************************************************************************/
    /********************************* Truck Model Numbers ****************************************/
    /**********************************************************************************************/
    /* NOTE: If this is changed make sure the macros below are still valid and adjust accordingly */
    /**********************************************************************************************/
    typedef enum
    {
        NO_MODEL      =  0,  /* No Model Set */
        MODEL_6015    =  1,
        MODEL_6025    =  2,
        MODEL_6025M   =  3,
        MODEL_6027    =  4,
        MODEL_6027M   =  5,
        MODEL_6028    =  6,
        MODEL_6028M   =  7,
        MODEL_6087    =  8,
        MODEL_6087M   =  9,
        MODEL_6088    = 10,
        MODEL_6088M   = 11,
        MODEL_6075    = 12
    } TRUCK_MODEL_TYPE;

    typedef enum
    {
        TRACTION_MODEL_0   =  0,
        TRACTION_MODEL_1   =  1,
        TRACTION_MODEL_2   =  2,
        TRACTION_MODEL_3   =  3,
        TRACTION_MODEL_4   =  4,
        TRACTION_MODEL_5   =  5,
        TRACTION_MODEL_6   =  6,
        TRACTION_MODEL_7   =  7,
        TRACTION_MODEL_8   =  8
    } TRACTION_MODEL_TYPE;

    typedef enum
    {                 /*  Values are in inches * 10  */
        RESET_HGT_CONV_MAST     = 180,    /*  Conventional Mast, Reset height offset from collapsed height*/
        RESET_HGT_MONO_42_INCH  = 158,    /*  42" Mono Mast,     Reset height offset from collapsed height*/
        RESET_HGT_MONO_48_INCH  = 201     /*  48" Mono Mast,     Reset height offset from collapsed height*/
    }RESET_HEIGHT_OFFSET;


    /*Traction Model Combo's*/
    #define gfTractionModel_0(TruckModel, MaxLoadWeight, Reach) ( fToBoolean((TruckModel == MODEL_6015) && (MaxLoadWeight == MAX_LIFT_3500) ) )

    #define gfTractionModel_1(TruckModel, MaxLoadWeight, Reach) ( fToBoolean((TruckModel == MODEL_6015) &&                                      \
                                                                             ( (MaxLoadWeight == MAX_LIFT_4000) ||                              \
                                                                               (MaxLoadWeight == MAX_LIFT_4500) ) ) )

    #define gfTractionModel_2(TruckModel, MaxLoadWeight, Reach) ( fToBoolean( ((TruckModel == MODEL_6025) ||                                    \
                                                                               (TruckModel == MODEL_6027) || (TruckModel == MODEL_6028) )       \
                                                                              && (MaxLoadWeight == MAX_LIFT_3500)                               \
                                                                              && (Reach == RIDER_REACH) ) )

    #define gfTractionModel_3(TruckModel, MaxLoadWeight, Reach) ( fToBoolean( ((Reach == RIDER_REACH) &&                                                                      \
                                                                               (((TruckModel == MODEL_6025M) || (TruckModel == MODEL_6027M) || (TruckModel == MODEL_6028M))   \
                                                                               || (((TruckModel == MODEL_6025) || (TruckModel == MODEL_6027) || (TruckModel == MODEL_6028))   \
                                                                               && ((MaxLoadWeight == MAX_LIFT_4000) || (MaxLoadWeight == MAX_LIFT_4500))))                    \
                                                                               )))

    #define gfTractionModel_4(TruckModel, MaxLoadWeight, Reach) ( fToBoolean( ((TruckModel == MODEL_6087)  || (TruckModel == MODEL_6087M) ||    \
                                                                               (TruckModel == MODEL_6088)  || (TruckModel == MODEL_6088M) ||    \
                                                                               (TruckModel == MODEL_6075) )                                     \
                                                                              && (Reach == RIDER_REACH) )                                       \
                                                                              && (MaxLoadWeight == MAX_LIFT_4500) )

    #define gfTractionModel_5(TruckModel, MaxLoadWeight, Reach) ( fToBoolean( ((TruckModel == MODEL_6025)  || (TruckModel == MODEL_6025M) ||    \
                                                                               (TruckModel == MODEL_6027)  || (TruckModel == MODEL_6027M) ||    \
                                                                               (TruckModel == MODEL_6028)  || (TruckModel == MODEL_6028M) )     \
                                                                              && (MaxLoadWeight == MAX_LIFT_3000)                               \
                                                                              && (Reach == DOUBLE_REACH) )  )

    #define gfTractionModel_6(TruckModel, MaxLoadWeight, Reach) ( fToBoolean( ((TruckModel == MODEL_6025)  || (TruckModel == MODEL_6025M) ||    \
                                                                               (TruckModel == MODEL_6027)  || (TruckModel == MODEL_6027M) ||    \
                                                                               (TruckModel == MODEL_6028)  || (TruckModel == MODEL_6028M) ) &&  \
                                                                              (                                                                 \
                                                                               ((Reach == RIDER_REACH) && (MaxLoadWeight == MAX_LIFT_5500 )) || \
                                                                               ((Reach == DOUBLE_REACH) && ((MaxLoadWeight == MAX_LIFT_4500) || \
                                                                                                            (MaxLoadWeight == MAX_LIFT_5000) || \
                                                                                                            (MaxLoadWeight == MAX_LIFT_5500)))  \
                                                                              )))

    #define gfTractionModel_7(TruckModel, MaxLoadWeight, Reach) ( fToBoolean( ((TruckModel == MODEL_6087 ) || (TruckModel == MODEL_6087M) ||    \
                                                                               (TruckModel == MODEL_6088 ) || (TruckModel == MODEL_6088M) ||    \
                                                                               (TruckModel == MODEL_6075) )                                     \
                                                                              && (MaxLoadWeight == MAX_LIFT_3000)                               \
                                                                              && (Reach == DOUBLE_REACH) )    )

    #define gfTractionModel_8(TruckModel, MaxLoadWeight, Reach) ( fToBoolean( ((TruckModel == MODEL_6087 ) || (TruckModel == MODEL_6087M) ||    \
                                                                               (TruckModel == MODEL_6088 ) || (TruckModel == MODEL_6088M) ||    \
                                                                               (TruckModel == MODEL_6075) ) &&                                  \
                                                                              (                                                                 \
                                                                               ((Reach == RIDER_REACH) && (MaxLoadWeight == MAX_LIFT_5500 )) || \
                                                                               ((Reach == DOUBLE_REACH) && ((MaxLoadWeight == MAX_LIFT_4500) || \
                                                                                                            (MaxLoadWeight == MAX_LIFT_5000) || \
                                                                                                            (MaxLoadWeight == MAX_LIFT_5500)))  \
                                                                              )))

    /*Determine if setup is a RR5500 Transport configuration */
    #define gfRD5500_Transport(MaxLoadWeight, Reach) ( fToBoolean( ((Reach == RIDER_REACH) && (MaxLoadWeight == MAX_LIFT_5500 )) || \
                                                                    ((Reach == DOUBLE_REACH) && ((MaxLoadWeight == MAX_LIFT_4500) || \
                                                                                                 (MaxLoadWeight == MAX_LIFT_5000) || \
                                                                                                 (MaxLoadWeight == MAX_LIFT_5500)))  \
                                                                  ))

    /*Determine PowerUnit Size*/
    #define gfPowerUnit42inch(TruckModel) ( fToBoolean( (TruckModel == MODEL_6015 ) ||                                  \
                                                        (TruckModel == MODEL_6025 ) || (TruckModel == MODEL_6025M) ||   \
                                                        (TruckModel == MODEL_6027 ) || (TruckModel == MODEL_6027M) ||   \
                                                        (TruckModel == MODEL_6028 ) || (TruckModel == MODEL_6028M) ) )

    #define gfPowerUnit48inch(TruckModel) ( fToBoolean( (TruckModel == MODEL_6087 ) || (TruckModel == MODEL_6087M) ||   \
                                                        (TruckModel == MODEL_6088 ) || (TruckModel == MODEL_6088M) ||   \
                                                        (TruckModel == MODEL_6075) ) )

    /*Determine Truck Voltage*/
    #define gf24VoltTruck(TruckModel) ( fToBoolean( (TruckModel == MODEL_6015 ) ) )

    #define gf36VoltTruck(TruckModel) ( fToBoolean( (TruckModel == MODEL_6025 ) || (TruckModel == MODEL_6025M) ||   \
                                                    (TruckModel == MODEL_6027 ) || (TruckModel == MODEL_6027M) ||   \
                                                    (TruckModel == MODEL_6028 ) || (TruckModel == MODEL_6028M) ||   \
                                                    (TruckModel == MODEL_6087 ) || (TruckModel == MODEL_6087M) ||   \
                                                    (TruckModel == MODEL_6088 ) || (TruckModel == MODEL_6088M) ||   \
                                                    (TruckModel == MODEL_6075) ) )

    /*Determine Hydraulics Type*/
    #define gfHydraulicsDC(TruckModel) ( fToBoolean( (TruckModel == MODEL_6015 ) ||                                 \
                                                     (TruckModel == MODEL_6025 ) || (TruckModel == MODEL_6025M) ||  \
                                                     (TruckModel == MODEL_6075) ) )

    #define gfHydraulicsAC(TruckModel) ( fToBoolean( (TruckModel == MODEL_6027 ) || (TruckModel == MODEL_6027M) ||  \
                                                     (TruckModel == MODEL_6028 ) || (TruckModel == MODEL_6028M) ||  \
                                                     (TruckModel == MODEL_6087 ) || (TruckModel == MODEL_6087M) ||  \
                                                     (TruckModel == MODEL_6088 ) || (TruckModel == MODEL_6088M) ))  \

    /*Determine Steering Type*/
    #define gfElectricSteer(TruckModel) ( fToBoolean( (TruckModel == MODEL_6028 ) || (TruckModel == MODEL_6028M) ||     \
                                                      (TruckModel == MODEL_6088 ) || (TruckModel == MODEL_6088M) ) )

    #define gfHydraulicSteer(TruckModel) ( fToBoolean( (TruckModel == MODEL_6015 ) ||                                   \
                                                       (TruckModel == MODEL_6025 ) || (TruckModel == MODEL_6025M) ||    \
                                                       (TruckModel == MODEL_6027 ) || (TruckModel == MODEL_6027M) ||    \
                                                       (TruckModel == MODEL_6087 ) || (TruckModel == MODEL_6087M) ||    \
                                                       (TruckModel == MODEL_6075) ) )
     /*Determine Mast Type*/
    #define gfStdMast(TruckModel) ( fToBoolean( (TruckModel == MODEL_6015 ) || (TruckModel == MODEL_6025) ||      \
                                                (TruckModel == MODEL_6027 ) || (TruckModel == MODEL_6028) ||      \
                                                (TruckModel == MODEL_6087 ) || (TruckModel == MODEL_6088) ||      \
                                                (TruckModel == MODEL_6075) ) )

    #define gfMonoMast(TruckModel) ( fToBoolean( (TruckModel == MODEL_6025M ) || (TruckModel == MODEL_6027M) ||     \
                                                 (TruckModel == MODEL_6028M ) || (TruckModel == MODEL_6087M) ||     \
                                                 (TruckModel == MODEL_6088M) ) )



#endif  // ifndef _SETUPMAPS_H_

#ifdef SETUPMAPS_MODULE

    #ifndef setupmaps_registration_local
    #define setupmaps_registration_local 1

   /****************************************************************************
    *                                                                          *
    *  I n t e r n a l   S c o p e   S e c t i o n                             *
    *                                                                          *
    ****************************************************************************/

    // Defines

    // Constants

    // Global variables

    // Macros

    #endif

#else

    #ifndef setupmaps_registration
    #define setupmaps_registration 1

   /****************************************************************************
    *                                                                          *
    *  E x t e r n a l   S c o p e   S e c t i o n                             *
    *                                                                          *
    ****************************************************************************/

    // Externaly visable global variables

    // Externaly visable function prototypes

    #endif

#endif
