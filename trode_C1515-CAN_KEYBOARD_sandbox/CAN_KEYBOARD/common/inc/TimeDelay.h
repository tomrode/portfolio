/************************************************************************************************
 * File:       TimeDelay.h
 *
 * Purpose:    Defines the main hoist slowdown interface
 *
 * Project:    C584
 *
 * Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 12/11/2003
 *
 ************************************************************************************************
*/

#ifndef  timedelay_registration

   /*******************************************************************************
    *                                                                             *
    *  G l o b a l   S c o p e   S e c t i o n                                    *
    *                                                                             *
    *******************************************************************************/

   // Defines
   typedef struct
   {
      uword uwCounts;
      uword uwCountsMax;
   }TIMEDLY;

#endif

#ifdef TIMEDELAY_MODULE

   #ifndef timedelay_registration_local
   #define timedelay_registration_local 1

   /*******************************************************************************
    *                                                                             *
    *  I n t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Defines

   // Function prototypes

   // Global variables

   // Macros

   #endif


#else
   #ifndef timedelay_registration
   #define timedelay_registration 1
   /*******************************************************************************
    *                                                                             *
    *  E x t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Externaly visable global variables

   // Externally visable function prototypes
   extern void    vTimeDelaySetup(TIMEDLY *pDelay, uword uwDelayTimeMs, uword uwUpdateRateMs, boolean fInitiallyExpired, uword uwDelayTimeMsMin, uword uwDelayTimeMsMax);
   extern void    vTimeDelayClear(TIMEDLY *pDelay);
   extern void    vTimeDelayService(TIMEDLY *pDelay);
   extern boolean fTimeDelayExpired(TIMEDLY *pDelay);

   #endif
#endif


