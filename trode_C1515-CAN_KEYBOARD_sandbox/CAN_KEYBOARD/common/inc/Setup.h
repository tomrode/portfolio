/******************************************************************************
 *  File:      Setup.h
 *
 *  Purpose:   This file is used to define bits used to request setup data.
 *
 *  Project:   C1101
 *
 *  Copyright: 2006 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:    Walt Conley
 *
 *  Revision History:
 *
 ******************************************************************************
*/
//#include "objects.h"

#ifndef setup_registration

   /****************************************************************************
    *                                                                          *
    *  G l o b a l   S c o p e   S e c t i o n                                 *
    *                                                                          *
    ***************************************************************************/
   
    // Defines
   
    /*  Request bits used to request data from OSM master. This is used as an
     *  index into the ulDataRequestBits[] array which is mapped to object
     *  OBJ_SETUP_DATA_REQ in the object dictionary. If this section changes
     *  be sure to update the object range table in OsmMasterUser.h.
     */
  
#endif

#ifdef SETUP_MODULE

    #ifndef setup_registration_local
    #define setup_registration_local 1
    
   /****************************************************************************
    *                                                                          *
    *  I n t e r n a l   S c o p e   S e c t i o n                             *
    *                                                                          *
    ****************************************************************************/
   
    // Defines
   
    // Constants
   
    // Global variables
   
    // Macros
   
    #endif

#else

    #ifndef setup_registration
    #define setup_registration 1
   
   /****************************************************************************
    *                                                                          *
    *  E x t e r n a l   S c o p e   S e c t i o n                             *
    *                                                                          *
    ****************************************************************************/
   
    // Externaly visable global variables
   
    // Externaly visable function prototypes
   
    #endif

#endif


