/******************************************************************************
 *  File:       CalProcesses.h
 *
 *  Purpose:    Contains various definitions related to auto calibration
 *
 *  Project:    C584
 *
 *  Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 3/31/2004
 *              Revised
 *
 ******************************************************************************
*/
#ifndef CAL_PROCESSES_H
#define CAL_PROCESSES_H


// Define auto calibrations modes
typedef enum
{
   CAL_PROC_HOIST_THROT      =  1,
   CAL_PROC_ACCY_THROT       =  2,
   CAL_PROC_LOAD_SENSOR      =  3,
   CAL_PROC_MAIN_LOWER_VALVE =  4,
   CAL_PROC_TILT_SENSOR      =  5,
   CAL_PROC_ACCESSORY_VALVE  =  6,
   CAL_PROC_ZAPI_TRAC_THROT  =  7,
   CAL_PROC_SDM_STR_POT      =  8,
   CAL_PROC_ZAPI_BRK_POT     =  9,
   CAL_PROC_MAIN_HGT_SENSOR  = 10,
   CAL_PROC_BATTERY_ADJ      = 11,
   CAL_PROC_SPL_MANUAL_CAL   = 12,
   CAL_PROC_ACCEL_CAL        = 13,
   CAL_PROC_HYDRAULIC_STEER_FB_CAL = 14,
   CAL_PROC_REACH_SENSOR     = 15
}CALIB_MODE;

// Define auto calibration actions
typedef enum
{
   CALIB_ACT_GET_KEYPRESS = 0,
   CALIB_ACT_INPUT_DATA   = 1,
   CALIB_ACT_SEL_TO_CONT  = 2,
   CALIB_ACT_TIME_DELAY   = 3
}CALIB_ACTION;

#endif
