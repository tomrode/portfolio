//****************************************************************************************
// File:       char_buffer.c
//
// Purpose:    Implementation file for a FIFO, circular char buffer.
//
// Project:    InfoLink (C1153)
//
// Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
//
// Author:     Max Peelman
//
// Revision History:
//          
//      11/15/2004 Max Peelman: 
//        -created document
//
//      01/12/2005 Max Peelman:
//        -fixed bug in fCBuf_PeekByte() where it was incorrectly using uwAppendElement
//          rather than uwExtractElement when reading from the buffer
//
//****************************************************************************************

#include <string.h>     // memset(), memcpy()...

// we must #define CHAR_BUFFER_SOURCE prior to #include-ing the header so
//   we gain local scope to the functions and data, then #undef after we're done...
#define CHAR_BUFFER_SOURCE
#include "char_buffer.h"
#undef CHAR_BUFFER_SOURCE


//*************************************************************************
//  Initialization function
//  
//    -currently this is the same function as Erasing, but that may change
//                                                                      
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
void vCBuf_Init (CHAR_BUFFER_TYPE* pcbBuf,
                 ubyte* pubBufArray,
                 uword uwBufSize)
{
    // store given buffer array info, then clear out everything...
    pcbBuf->pubBuf = pubBufArray;
    pcbBuf->uwBufSize = uwBufSize;
    vCBuf_Erase(pcbBuf);
}

//*************************************************************************
//  Append a single byte to the buffer
//                                                                        
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
boolean fCBuf_AppendByte (CHAR_BUFFER_TYPE* pcbBuf,
                          ubyte ubVal)
{

    // NULL-pointer check...
    if (!pcbBuf)
    {
        return FALSE;
    }
    // buffer room check...
    if (pcbBuf->uwLen >= pcbBuf->uwBufSize)
    {
        return FALSE;
    }

    pcbBuf->pubBuf[pcbBuf->uwAppendElement] = ubVal;
    pcbBuf->uwLen++;
    // increment array index for next append, check for roll-over...
    if ( pcbBuf->uwAppendElement < (pcbBuf->uwBufSize - 1) )
    {
        pcbBuf->uwAppendElement++;
    }
    else
    {
        pcbBuf->uwAppendElement = 0;
    }

    return TRUE;
}

//*************************************************************************
//  Append one or more bytes to the buffer
//                                                                        
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
boolean fCBuf_AppendBytes (CHAR_BUFFER_TYPE* pcbBuf,
                           ubyte *pubData,
                           uword uwNumBytes)
{
    uword uwIndex;

    // NULL-pointer check...
    if (!pcbBuf)
    {
        return FALSE;
    }
    // buffer room check...
    if ( (pcbBuf->uwLen + uwNumBytes) >= pcbBuf->uwBufSize )
    {
        return FALSE;
    }

    // loop to add each byte, checking for roll-over of the array...
    for (uwIndex = 0; uwIndex < uwNumBytes; ++uwIndex)
    {
         pcbBuf->pubBuf[pcbBuf->uwAppendElement] = *(pubData + uwIndex);
         if ( pcbBuf->uwAppendElement < (pcbBuf->uwBufSize - 1) )
         {
             pcbBuf->uwAppendElement++;
         }
         else
         {
             pcbBuf->uwAppendElement = 0;
         }
    }
    // adjust buffer length with one addition here
    pcbBuf->uwLen += uwNumBytes;

    return TRUE;
}

//*************************************************************************
//  Peek at a single byte in the buffer, without affecting the buffer
//
//  NOTE: uwIndex must be between 0 and ( uwCBuf_Len() - 1 ); in other
//          words, the index is relative to the current amount of data in
//          the buffer
//                                                                        
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
boolean fCBuf_PeekByte (CHAR_BUFFER_TYPE* pcbBuf,
                        uword uwIndex,
                        ubyte *pubVal)
{

    // NULL-pointer check...
    if (!pcbBuf)
    {
        return FALSE;
    }
    // bad index check...
    if (uwIndex >= pcbBuf->uwLen)
    {
        return FALSE;
    }

    // add the index to the start index to get the byte desired; we must
    //   check for roll-over of the array...
    uwIndex +=  pcbBuf->uwExtractElement;
    if (uwIndex >= pcbBuf->uwBufSize)
    {
        uwIndex -= pcbBuf->uwBufSize;
    }
    *(pubVal) = pcbBuf->pubBuf[uwIndex];

    return TRUE;
}

//*************************************************************************
//  Extract a single byte from the buffer
//                                                                        
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
boolean fCBuf_ExtractByte (CHAR_BUFFER_TYPE* pcbBuf,
                           ubyte *pubVal)
{

    // NULL-pointer check...
    if (!pcbBuf)
    {
        return FALSE;
    }
    // empty buffer check...
    if (pcbBuf->uwLen == 0)
    {
        return FALSE;
    }

    *(pubVal) = pcbBuf->pubBuf[pcbBuf->uwExtractElement];
    pcbBuf->uwLen--;
    // increment array index for next extract, check for roll-over...
    if ( pcbBuf->uwExtractElement < (pcbBuf->uwBufSize - 1) )
    {
        pcbBuf->uwExtractElement++;
    }
    else
    {
        pcbBuf->uwExtractElement = 0;
    }

    return TRUE;
}

//*************************************************************************
//  Extract one or more bytes from the buffer
//                                                                        
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
uword uwCBuf_ExtractBytes (CHAR_BUFFER_TYPE* pcbBuf,
                           ubyte *pubData,
                           uword uwNumBytes)
{
    uword uwBytesExtracted;

    // NULL-pointer check...
    if (!pcbBuf)
    {
        return 0;
    }

    // loop to extract each byte, until:
    //   1. requested number of bytes is reached OR
    //   2. buffer is empty
    for ( uwBytesExtracted = 0; 
          (uwBytesExtracted < uwNumBytes) && (pcbBuf->uwLen);
          uwBytesExtracted++, pcbBuf->uwLen-- )
    {
         *(pubData + uwBytesExtracted) = pcbBuf->pubBuf[pcbBuf->uwExtractElement];
         // increment buffer element, check for roll-over of the array...
         pcbBuf->uwExtractElement++;
         if (pcbBuf->uwExtractElement >= pcbBuf->uwBufSize)
         {
             pcbBuf->uwExtractElement = 0;
         }
    }

    return uwBytesExtracted;
}

//*************************************************************************
//  Retrieve current buffer usage
//                                                                        
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
uword uwCBuf_Len (CHAR_BUFFER_TYPE* pcbBuf)
{

    // NULL-pointer check...
    if (!pcbBuf)
    {
        return 0;
    }

    return pcbBuf->uwLen;
}

//*************************************************************************
//  Erase current buffer contents
//                                                                        
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
void vCBuf_Erase (CHAR_BUFFER_TYPE* pcbBuf)
{

    // NULL-pointer check...
    if (!pcbBuf)
    {
        return;
    }

    pcbBuf->uwAppendElement = 0;
    pcbBuf->uwExtractElement = 0;
    pcbBuf->uwLen = 0;
    // if buffer array is given, erase its contents...
    if (pcbBuf->pubBuf)
    {
        memset(pcbBuf->pubBuf, 0, pcbBuf->uwBufSize);
    }

}

//*************************************************************************
//  Is the buffer empty?
//                                                                        
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
boolean fCBuf_IsEmpty (CHAR_BUFFER_TYPE* pcbBuf)
{
    // NULL-pointer check...
    if (!pcbBuf)
    {
        return FALSE;
    }

    return (pcbBuf->uwLen == 0);
}

//*************************************************************************
//  Is the buffer full?
//                                                                        
//  (Refer to function prototype in header file for more details)                                               
//*************************************************************************
boolean fCBuf_IsFull (CHAR_BUFFER_TYPE* pcbBuf)
{
    // NULL-pointer check...
    if (!pcbBuf)
    {
        return FALSE;
    }

    return (pcbBuf->uwLen == pcbBuf->uwBufSize);
}


