/***********************************************************************************************
 * File:       PdoChk.c
 *
 * Purpose:    Contains functions used to monitor receive PDOs.
 *
 * Project:    C1101
 *
 * Copyright:  2006 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 05/21/2004
 *
 ************************************************************************************************
 */
#include "datalog.h"
#define PDOCHK_MODULE 1
#include "PdoChk.h"
#undef  PDOCHK_MODULE
#include "stddef.h"

/*
 ** vPdoTimeoutInit
 *
 *  FILENAME:     PdoChk.c
 *
 *  PARAMETERS:   pPdo           = Pointer to PDO_CHK
 *                swUpdateRateMs = Update rate in milliseconds
 *                swTimeoutms    = Timeout in milliseconds
 *
 *  DESCRIPTION:  This function is used to initialze a PDO check. It
 *                calculates the number if ticks the PDO has to be absent
 *                before acknowleging a fault condition.
 *
 *  RETURNS:      Nothing
 *
 */
void vPdoTimeoutInit(PDO_CHK *pPdo, uword uwUpdateRateMs, uword uwTimeoutMs)
{
   uword uwConcernCountMax = 0;

   // Calculate concern count max and limit it to ubyte range
   if ( uwUpdateRateMs )
   {
      uwConcernCountMax = uwTimeoutMs / uwUpdateRateMs;
   }
   if ( uwConcernCountMax > 255 )
   {
      uwConcernCountMax = 255;
   }

   // Initialize structure
   pPdo->fFirstPdoSeen     = FALSE;
   pPdo->ubConcernCount    = 0;
   pPdo->ubConcernCountMax = (ubyte)(uwConcernCountMax);
}

/*
 ** fPdoTimeout
 *
 *  FILENAME:     PdoChk.c
 *
 *  PARAMETERS:   pPdo          = Pointer to a PDO_CHK structure.
 *                uwPdoBit      = Bit that is set in uwPdoStatus to tell this
 *                                function when a PDO is received.
 *                wPdoStatus    = Shows what PDOs have been received.
 *                fDatalog      = TRUE if datalogging should be performed
 *
 *  DESCRIPTION:
 *
 *  RETURNS:
 *
 */
boolean fPdoTimeout(PDO_CHK *pPdo,
                    uword   uwPdoBit,
                    uword   uwPdoStatus,
                    boolean fDatalog
                   )
{
   boolean fTimeout = FALSE;

   // Only start checking after we receive the first PDO
   if ( pPdo->fFirstPdoSeen )
   {
      // Check if a PDO was received
      if ( uwPdoStatus & uwPdoBit )
      {
         // Yes, clear the concern count
         pPdo->ubConcernCount = 0;
      }
      else
      {
         // No, increment the concern count
         pPdo->ubConcernCount++;
      }

      // Timeout if if PDO is absent too long
      if ( pPdo->ubConcernCount > pPdo->ubConcernCountMax )
      {
         fTimeout = TRUE;
      }
   }
   else
   {
      // Mark that the first PDO has been seen
      if ( uwPdoStatus & uwPdoBit )
      {
         pPdo->fFirstPdoSeen = TRUE;
      }
   }
   if ( fDatalog )
   {
      Can1.ubData[0] = (ubyte)(pPdo->fFirstPdoSeen         );
      Can1.ubData[1] = (ubyte)(pPdo->ubConcernCount        );
      Can1.ubData[2] = (ubyte)(pPdo->ubConcernCountMax     );
      Can1.ubData[3] = (ubyte)(uwPdoBit                >> 8);
      Can1.ubData[4] = (ubyte)(uwPdoBit                    );
      Can1.ubData[5] = (ubyte)(uwPdoStatus             >> 8);
      Can1.ubData[6] = (ubyte)(uwPdoStatus                 );
      vSaveDatalog(&Can1, NULL);
   }
   return(fTimeout);
}

