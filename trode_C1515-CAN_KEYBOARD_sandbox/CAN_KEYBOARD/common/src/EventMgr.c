/************************************************************************************************
 *  File:       EventsMgr.c
 *
 *  Purpose:    Implements the Event Reporting system
 *
 *  Project:    C584
 *
 *  Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/13/2004
 *
 ************************************************************************************************
*/

#include "watchdog.h"
#define EventMgrFile
#include "EventMgr.h"
#undef EventMgrFile
#include "Truck_PDO.h"
#include "Datalog.h"
#include <string.h>

void (*pfnEventCallback)(uword uwCode, boolean fSet, boolean fPassed);

/************************************************************************************************
 *  Function:   vInit_EventMgr
 *
 *  Purpose:    Sets up event manager module
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gPendingEvnts;       // Stores the pending events
 *              gReportedEvnts[EVENT_REPORTED_SIZE];     // stores the reported list
 *              gAckedEvnts;         // Stores the acked events
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/14/2004
 *
 ************************************************************************************************
*/
void vInit_EventMgr()
{
    // init the data structures
    gPendingEvnts.fHasContent = FALSE;
    gPendingEvnts.ubHead = 0;
    gPendingEvnts.ubTail = 0;
    memset((void *)&gPendingEvnts.Array[0], 0, (sizeof(PENDING_LIST_TYPE) * EVENT_PENDING_SIZE));
    memset((void *)&gReportedEvnts[0], 0, (sizeof(REPORTED_TYPE) * EVENT_REPORTED_SIZE));
    gAckedEvnts.fHasContent = FALSE;
    gAckedEvnts.ubHead = 0;
    gAckedEvnts.ubTail = 0;
    memset((void *)&gAckedEvnts.Array[0], 0, (sizeof(ulong) * EVENT_ACKED_SIZE));
    vGetFRAM_Events(&gLocalEvents);      // read in past events

}

/************************************************************************************************
 *  Function:   ubQueue_Pending_Event
 *
 *  Purpose:    Inserts a pending event into the queue
 *
 *  Input:      ubEventIndex -- An index into the event code list
 *              fSetError -- True if the event is being set else False
 *              pfnCallback -- Pointer to the call back function in the functional area
 *                             that reported the event.  Used to send back ACKs from the display
 *
 *  Output:     N/A
 *
 *  Return:     QUEUE_OK, or QUEUE_FULL
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gPendingEvnts;       // Stores the pending events
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/14/2004
 *
 ************************************************************************************************
*/
ubyte ubQueue_Pending_Event(ubyte ubEventIndex, boolean fSetError, void(*pfnCallback) (uword, boolean, boolean))
{
    ubyte ubNextSlot;
    ubyte ubResult = QUEUE_OK;

    if ((gPendingEvnts.fHasContent) && (gPendingEvnts.ubHead == gPendingEvnts.ubTail))
    {
        ubResult = QUEUE_FULL;
    }
    else
    {
        gPendingEvnts.Array[gPendingEvnts.ubHead].ubEventIndex = ubEventIndex;
        gPendingEvnts.Array[gPendingEvnts.ubHead].fSetError = fSetError;
        gPendingEvnts.Array[gPendingEvnts.ubHead].pfnEventCallback = pfnCallback;
        ubNextSlot = gPendingEvnts.ubHead + 1;
        if (ubNextSlot >= EVENT_PENDING_SIZE)
        {
            ubNextSlot = 0;
        }
        gPendingEvnts.fHasContent = TRUE;
        gPendingEvnts.ubHead = ubNextSlot;
    }
    return ubResult;
}

/************************************************************************************************
 *  Function:   ubDequeue_Pending_Event
 *
 *  Purpose:    Pulls a pending event from the queue
 *
 *  Input:      pubEventIndex -- Pointer to an index into the event code list
 *              pfSetError -- Pointer to flag which is True if event is being set
 *              ppfnCallback -- Pointer to a pointer to the call back function in the functional area
 *                             that reported the event.  Used to send back ACKs from the display
 *
 *  Output:     N/A
 *
 *  Return:     QUEUE_OK, or QUEUE_EMPTY
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gPendingEvnts;       // Stores the pending events
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/14/2004
 *
 ************************************************************************************************
*/
ubyte ubDequeue_Pending_Event(ubyte *pubEventIndex, boolean *pfSetError, void(**ppfnCallback) (uword, boolean, boolean))
{
    ubyte ubNextSlot;
    ubyte ubResult = QUEUE_OK;

    if (!gPendingEvnts.fHasContent)
    {
        ubResult = QUEUE_EMPTY;
    }
    else
    {
        gfPendingHadContent = TRUE;
        *pubEventIndex = gPendingEvnts.Array[gPendingEvnts.ubTail].ubEventIndex;
        *pfSetError = gPendingEvnts.Array[gPendingEvnts.ubTail].fSetError;
        *ppfnCallback = gPendingEvnts.Array[gPendingEvnts.ubTail].pfnEventCallback;
        ubNextSlot = gPendingEvnts.ubTail + 1;
        if (ubNextSlot >= EVENT_PENDING_SIZE)
        {
            ubNextSlot = 0;
        }
        if (ubNextSlot == gPendingEvnts.ubHead)
        {
            gPendingEvnts.fHasContent = FALSE;
        }
        gPendingEvnts.ubTail = ubNextSlot;
    }
    return ubResult;
}

/************************************************************************************************
 *  Function:   ubQueue_Acked_Event
 *
 *  Purpose:    Inserts an ACKED event into the queue
 *
 *  Input:      ulEventCode -- The event code acked
 *
 *  Output:     N/A
 *
 *  Return:     QUEUE_OK, or QUEUE_FULL
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gAckedEvnts;       // Stores the acked events
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/14/2004
 *
 ************************************************************************************************
*/
ubyte ubQueue_Acked_Event(ulong ulEventCode)
{
    ubyte ubNextSlot;
    ubyte ubResult = QUEUE_OK;

    if ((gAckedEvnts.fHasContent) && (gAckedEvnts.ubHead == gAckedEvnts.ubTail))
    {
        ubResult = QUEUE_FULL;
    }
    else
    {
        gAckedEvnts.Array[gAckedEvnts.ubHead] = ulEventCode;
        ubNextSlot = gAckedEvnts.ubHead + 1;
        if (ubNextSlot >= EVENT_ACKED_SIZE)
        {
            ubNextSlot = 0;
        }
        gAckedEvnts.fHasContent = TRUE;
        gAckedEvnts.ubHead = ubNextSlot;
    }
    return ubResult;
}

/************************************************************************************************
 *  Function:   ubDequeue_Acked_Event
 *
 *  Purpose:    Pulls an ACKED event from the queue
 *
 *  Input:      pulEventCode -- pointer to he event code acked slot to fill
 *
 *  Output:     N/A
 *
 *  Return:     QUEUE_OK, or QUEUE_FULL
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gAckedEvnts;       // Stores the acked events
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/14/2004
 *
 ************************************************************************************************
*/
ubyte ubDequeue_Acked_Event(ulong *pulEventCode)
{
    ubyte ubNextSlot;
    ubyte ubResult = QUEUE_OK;

    if (!gAckedEvnts.fHasContent)
    {
        ubResult = QUEUE_EMPTY;
    }
    else
    {
        gfAckedHadContent = TRUE;
        *pulEventCode = gAckedEvnts.Array[gAckedEvnts.ubTail];
        gAckedEvnts.Array[gAckedEvnts.ubTail] = 0;
        ubNextSlot = gAckedEvnts.ubTail + 1;
        if (ubNextSlot >= EVENT_ACKED_SIZE)
        {
            ubNextSlot = 0;
        }
        if (ubNextSlot == gAckedEvnts.ubHead)
        {
            gAckedEvnts.fHasContent = FALSE;
        }
        gAckedEvnts.ubTail = ubNextSlot;
    }
    return ubResult;
}

/************************************************************************************************
 *  Function:   ubLogEvent
 *
 *  Purpose:    Log the event by placing it in the pending queue. This function is called be the
 *              various functional areas of the software.  The other software tasks are blocked
 *              while the common data structures are being accessed, but the hardware interrupts
 *              remain active.
 *
 *  Input:      ubEventIndex -- An index into the event code list
 *              fSetError -- True if the event is being set, else False
 *              pfnCallback -- Pointer to the call back function in the functional area
 *                             that reported the event.  Used to send back ACKs from the display
 *
 *  Output:     N/A
 *
 *  Return:     QUEUE_OK, or QUEUE_FULL
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/14/2004
 *
 ************************************************************************************************
*/
ubyte ubLogEvent(ubyte ubEventIndex, boolean fSetError, void(*pfnCallback)(uword, boolean, boolean))
{
    ubyte ubResult;

    vDisableTasks();
    ubResult = ubQueue_Pending_Event(ubEventIndex, fSetError, pfnCallback);
    vEnableTasks();

    return ubResult;
}


/************************************************************************************************
 *  Function:   ubAckEvent
 *
 *  Purpose:    Ack an event by placing it in the Acked queue for processing.  This function is
 *              called from the SDO write callback function when writes to the ACK data index are
 *              indicated.
 *
 *  Input:      ulEventCode -- The event code acked
 *
 *  Output:     N/A
 *
 *  Return:     QUEUE_OK, or QUEUE_FULL
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/14/2004
 *
 ************************************************************************************************
*/
ubyte ubAckEvent(ulong ulEventCode)
{
    ubyte ubResult;

    vDisableTasks();
    ubResult = ubQueue_Acked_Event(ulEventCode);
    vEnableTasks();

    return ubResult;
}

/************************************************************************************************
 *  Function:   vLogEventToFRAM
 *
 *  Purpose:    Log an event to the local list.  Do not put the event in the list if that
 *              event is already in the list.
 *
 *  Input:      ulEventCode -- The event code
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/19/2004
 *
 ************************************************************************************************
*/
void vLogEventToFRAM(uword uwEventCode)
{
    boolean fFound;
    ubyte   ubIndex;

    fFound = FALSE;
    ubIndex = 0;
    while ((ubIndex < LOCAL_EVENT_LIST_SIZE) && (fFound == FALSE))
    {
        if (gLocalEvents.History.uwEvent[ubIndex] == uwEventCode)
        {
            fFound = TRUE;
        }
        ubIndex++;
    }

    if (fFound == FALSE)   // if we did not find the event in the list
    {
        // copy elements down by one
        for (ubIndex = (LOCAL_EVENT_LIST_SIZE - 1); ubIndex > 0; ubIndex--)
        {
            gLocalEvents.History.uwEvent[ubIndex] = gLocalEvents.History.uwEvent[ubIndex-1];
        }
        gLocalEvents.History.uwEvent[0] = uwEventCode;
        gLocalEvents.History.uwEventsLogged = uwMin((gLocalEvents.History.uwEventsLogged + 1),
                                             LOCAL_EVENT_LIST_SIZE);
        guwEventCodeLogged = uwEventCode;   // save for datalog
        gfUpdateLocalEvents = TRUE;         // mark that events must be saved in Main();
    }
}

/************************************************************************************************
 *  Function:   vProcessAcked
 *
 *  Purpose:    Handle the Acked queue
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/15/2004
 *
 ************************************************************************************************
*/
void vProcessAcked(void)
{
    ulong ulQueuedEvent;
    uword uwEventCode;
    boolean fSet;
    ubyte ubResult = QUEUE_OK;
    ubyte ubIndex;

    while (ubResult == QUEUE_OK)
    {
        // SDO callbacks happen in the idle task, this task is preempting
        // so no need to block tasks until we do the callbacks.
        ubResult =  ubDequeue_Acked_Event(&ulQueuedEvent);
        if (ubResult == QUEUE_OK)
        {
            uwEventCode = (uword)(ulQueuedEvent & 0x0000FFFF);
            fSet = (boolean)((ulQueuedEvent >> 16) & 1);
            // find this event in the reported list and clear it
            for (ubIndex = 0; ubIndex < EVENT_REPORTED_SIZE; ubIndex++)
            {
                if (gReportedEvnts[ubIndex].eState == HasContent)
                {
                    if ((gReportedEvnts[ubIndex].fSetError == fSet) &&
                        (gReportedEvnts[ubIndex].uwEventCode == uwEventCode)
                       )
                    {
                       gReportedEvnts[ubIndex].eState = WaitForMaskPDO;
                    } // end of if acked event matches the slot
                } // end of if the slot is in use
            } // end of for each element of the list
        } // end of if a value was read from the acked queue
    } // end of while loop
}

/************************************************************************************************
 *  Function:   vEventMaskReport
 *
 *  Purpose:    Handle the reported queue on event of receiving the event mask PDO
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/26/2004
 *
 ************************************************************************************************
*/
void vEventMaskReport(void)
{
    uword uwEventCode;
    boolean fSet;
    ubyte ubIndex;

    for (ubIndex = 0; ubIndex < EVENT_REPORTED_SIZE; ubIndex++)
    {
        if (gReportedEvnts[ubIndex].eState == WaitForMaskPDO)
        {
            vDisableTasks();
            uwEventCode = gReportedEvnts[ubIndex].uwEventCode;
            fSet = gReportedEvnts[ubIndex].fSetError;
            if (gReportedEvnts[ubIndex].pfnEventCallback != NULL)
            {
                pfnEventCallback = gReportedEvnts[ubIndex].pfnEventCallback;
                pfnEventCallback(uwEventCode, fSet, TRUE);
                gfMadeCallback = TRUE;
            }  // end of if callback is not null
            gReportedEvnts[ubIndex].eState = Empty;
            vEnableTasks();
        } // end of if the slot is in use
    } // end of for each element of the list
}

/************************************************************************************************
 *  Function:   vProcessPending
 *
 *  Purpose:    Handle the pending queue
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/15/2004
 *              Revised 2/6/2004 to handle potential problem with requeue if of pending if
 *                               reported is full.
 *
 ************************************************************************************************
 */
void vProcessPending(void)
{
    ubyte   ubEventIndex;
    boolean fSetEvent;
    void (*pfnCallback)(uword uwCode, boolean fSet, boolean fPassed);
    ubyte   ubResult;
    ubyte   ubIndex;
    boolean fFound;
    ubyte   ubEmptyIndex = 0;

    fFound = FALSE;
    if (gPendingEvnts.fHasContent == TRUE)  // if there is something in the pending queue
    {
        // make sure there is a potential slot in the reported queue
        ubIndex = 0;
        while ((ubIndex < EVENT_REPORTED_SIZE) && (fFound == FALSE))
        {
            if (gReportedEvnts[ubIndex].eState == Empty)
            {
                fFound = TRUE;
                ubEmptyIndex = ubIndex;
            }
            ubIndex++;
        }
    }
    if (fFound == TRUE) // there is a potential slot in the reported queue
    {
        vDisableTasks();
        ubResult = ubDequeue_Pending_Event(&ubEventIndex, &fSetEvent, &pfnCallback);
        vEnableTasks();

        if (ubResult == QUEUE_OK)
        {
            // Make sure the event is not already pending in the reported list
            ubIndex = 0;
            fFound = FALSE;
            while ((ubIndex < EVENT_REPORTED_SIZE) && (fFound == FALSE))
            {
                if (gReportedEvnts[ubIndex].eState != Empty)
                {
                    if ((gReportedEvnts[ubIndex].uwEventCode == gEvent_Table[ubEventIndex].uwEventCode) &&
                        (gReportedEvnts[ubIndex].fSetError == fSetEvent)
                       )
                    {
                        fFound = TRUE;     // already exists in list
                    }
                }
                ubIndex++;
            }
            if (fFound == FALSE)   // if we did not find a duplicate in the list
            {
                // use the empty slot found above and saved in ubEmptyIndex
                gfSetEventSent = fSetEvent;                       // save for datalog
                guwEventCodeSent = gEvent_Table[ubEventIndex].uwEventCode;  // save for datalog
                gReportedEvnts[ubEmptyIndex].eState = HasContent;
                gReportedEvnts[ubEmptyIndex].fSetError = fSetEvent;
                gReportedEvnts[ubEmptyIndex].uwEventCode = guwEventCodeSent;
                gReportedEvnts[ubEmptyIndex].pfnEventCallback = pfnCallback;
                gReportedEvnts[ubEmptyIndex].ubTimeoutCount = 0;
                gReportedEvnts[ubEmptyIndex].ubRepeatCount = 0;
                vSend_Error_TX(fSetEvent, guwEventCodeSent);
                gfPacketSent = TRUE;

                if ((gEvent_Table[ubEventIndex].uwEventCode >= MIN_NON_SRO_EVENT) &&
                    (gEvent_Table[ubEventIndex].uwEventCode <= MAX_EVENT_NUMBER))
                {
                    // if this is a real event and not just an SRO or Advise

                    if (fSetEvent)
                    {
                        vLogEventToFRAM(guwEventCodeSent);
                    }
                }
            }
        }
    }
}

/************************************************************************************************
 *  Function:   vProcessReported
 *
 *  Purpose:    Handle the reported queue
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/15/2004
 *
 ************************************************************************************************
 */
void vProcessReported(void)
{
    ubyte ubIndex;
    uword uwEventCode;
    boolean fSet;

    // check each event in the reported list for timeouts
    ubIndex = 0;
    while(ubIndex < EVENT_REPORTED_SIZE)
    {
        if (gReportedEvnts[ubIndex].eState == HasContent)
        {
            gReportedEvnts[ubIndex].ubTimeoutCount++;
            if  (gfPacketSent == FALSE)
            {
                if (gReportedEvnts[ubIndex].ubTimeoutCount >= EVENT_ACK_TIMEOUT_TICKS)
                {
                    // resend the packet or time out on retries
                    gfPacketTimeOut = TRUE;
                    gReportedEvnts[ubIndex].ubTimeoutCount = 0;
                    if ( (gReportedEvnts[ubIndex].ubRepeatCount < EVENT_RETRY_LIMIT) &&
                         (EVENT_RETRY_LIMIT > 0)
                       )
                    {
                        gReportedEvnts[ubIndex].ubRepeatCount++;
                    }
                    if ( (gReportedEvnts[ubIndex].ubRepeatCount >= EVENT_RETRY_LIMIT) &&
                         (EVENT_RETRY_LIMIT > 0)
                       )
                    {
                        // The display is not acking events, callback with failure
                        vDisableTasks();
                        uwEventCode = gReportedEvnts[ubIndex].uwEventCode;
                        fSet = gReportedEvnts[ubIndex].fSetError;
                        if (gReportedEvnts[ubIndex].pfnEventCallback != NULL)
                        {
                            pfnEventCallback = gReportedEvnts[ubIndex].pfnEventCallback;
                            pfnEventCallback(uwEventCode, fSet, FALSE);
                            gfMadeCallback = TRUE;
                        }  // end of if callback is not null
                        gReportedEvnts[ubIndex].eState = Empty;
                        vEnableTasks();
                    }
                    else
                    {
                        vSend_Error_TX(gReportedEvnts[ubIndex].fSetError,
                                           gReportedEvnts[ubIndex].uwEventCode);
                        guwEventCodeSent = gReportedEvnts[ubIndex].uwEventCode;  // save for datalog
                        gfSetEventSent = gReportedEvnts[ubIndex].fSetError;
                        gfPacketSent = TRUE;
                    }
                }
            }
        } // end of if the slot is in use
        ubIndex++;
    } // end of for each element of the list
}

/************************************************************************************************
 *  Function:   vEventMgr_Task
 *
 *  Purpose:    Entry point of event handling service.  This process is run by a capture-compare
 *              counter's interrupt.  It performs the "housekeeping" for the data structures.
 *
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/15/2004
 *
 ************************************************************************************************
 */
void vEventMgr_Task()
{
    DATA_LOG_TYPE  Can1, Can2;   // used for datalogging
    ubyte i;

    gfPacketSent = FALSE;
    gfPacketTimeOut = FALSE;
    guwEventCodeSent = 0;       // used in datalog
    guwEventCodeLogged = 0;     // used in datalog
    gfSetEventSent = FALSE;
    gfAckedHadContent = FALSE;
    gfPendingHadContent = FALSE;

    vProcessAcked();
    vProcessReported();
    if (gfPacketSent == FALSE)
    {
        vProcessPending();
    }

    if ( mDataLogNumber == mEventMgrDataLog )
    {
        Can1.ubData[0] = gPendingEvnts.ubHead;
        Can1.ubData[1] = gPendingEvnts.ubTail;
        Can1.ubData[2] = gfPendingHadContent;
        Can1.ubData[3] = 0;
        for (i=0; i< EVENT_REPORTED_SIZE; i++)
        {
            Can1.ubData[3] |= (ubyte)(((gReportedEvnts[i].eState == HasContent)) << i);
        }
        Can1.ubData[4] = gfPacketSent;
        Can1.ubData[5] = gfPacketTimeOut;
        Can1.ubData[6] = 0;
        for (i=0; i< EVENT_REPORTED_SIZE; i++)
        {
            Can1.ubData[6] |= (ubyte)(((gReportedEvnts[i].eState == WaitForMaskPDO)) << i);
        }
        Can1.ubData[7] = gAckedEvnts.ubHead;
        Can2.ubData[0] = gAckedEvnts.ubTail;
        Can2.ubData[1] = gfAckedHadContent;
        Can2.ubData[2] = (ubyte)(guwEventCodeSent >> 8);
        Can2.ubData[3] = (ubyte)(guwEventCodeSent);
        Can2.ubData[4] = gfSetEventSent;
        Can2.ubData[5] = gfMadeCallback;
        Can2.ubData[6] = (ubyte)(guwEventCodeLogged >> 8);
        Can2.ubData[7] = (ubyte)(guwEventCodeLogged);
        vSaveDatalog(&Can1, &Can2);
    }
    gfMadeCallback = FALSE;
    mWatchdogRecordTask(EVENT_TASK_RAN_FLAG);
}

/************************************************************************************************
 *  Function:   vClearLocalEventList
 *
 *  Purpose:    Clears the local event list in both memory and FRAM.  This function is
 *              called from the SDO write callback function when writes to the clear data index are
 *              indicated.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/19/2004
 *
 ************************************************************************************************
*/
void vClearLocalEventList(void)
{
    memset((void *)&gLocalEvents, 0, sizeof(LOCAL_EVENTS_TYPE));
    gfUpdateLocalEvents = 1;
}
