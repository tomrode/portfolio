/***********************************************************************************************
 * File:       TimeDelay.c
 *
 * Purpose:    Contains generic functions to provide a software controled
 *             time delay
 *
 * Project:    C584
 *
 * Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 04/13/2004
 *
 ************************************************************************************************
*/
#include "types.h"
#define TIMEDELAY_MODULE 1
#include "TimeDelay.h"

/*
 ** vTimeDelaySetup
 *
 *  FILENAME:     C:\viewstore\wc_hydr1\miniturret\app\common\src\TimeDelay.c
 *
 *  PARAMETERS:   pDelay = Pointer to TIMEDLY structure that defines the delay
 *                uwDelayTimeMs = Desired delay time in milliseconds
 *                uwUpdateRate = How often the vTimeDelayService() function is
 *                               called in milliseconds
 *                fInitiallyExpired = TRUE to initialize the timer as expired
 *                                    otherwise FALSE
 *                uwDelayTimeMsMin = Minimum delay time in milliseconds
 *                uwDelayTimeMsMax = Maximum delay time in milliseconds
 *
 *  DESCRIPTION:  This function is used to set up a delay time with the
 *                necessary parameters.
 *
 *  RETURNS:      Nothing
 *
 */
void vTimeDelaySetup(TIMEDLY *pDelay,
                     uword    uwDelayTimeMs,
                     uword    uwUpdateRateMs,
                     boolean  fInitiallyExpired,
                     uword    uwDelayTimeMsMin,
                     uword    uwDelayTimeMsMax
                    )
{
   // Limit Delay to min/max
   if ( uwDelayTimeMs < uwDelayTimeMsMin )
   {
      uwDelayTimeMs = uwDelayTimeMsMin;
   }
   if ( uwDelayTimeMs > uwDelayTimeMsMax )
   {
      uwDelayTimeMs = uwDelayTimeMsMax;
   }

   // Not a good idea to divide by zero
   if ( uwUpdateRateMs )
   {
      pDelay->uwCountsMax = uwDelayTimeMs / uwUpdateRateMs;
   }
   else
   {
      pDelay->uwCountsMax = 0;
   }

   // Initialize timer to desired value
   if ( fInitiallyExpired )
   {
      pDelay->uwCounts = pDelay->uwCountsMax;
   }
   else
   {
      pDelay->uwCounts = 0;
   }
}

/*
 ** vTimeDelayClear
 *
 *  FILENAME:     C:\viewstore\wc_hydr1\miniturret\app\common\src\TimeDelay.c
 *
 *  PARAMETERS:   pDelay = Pointer to TIMEDLY structure that defines the delay
 *
 *  DESCRIPTION:  This function will clear the timer.
 *
 *  RETURNS:      Nothing
 *
 */
void vTimeDelayClear(TIMEDLY *pDelay)
{
   pDelay->uwCounts = 0;
}

/*
 ** vTimeDelayService
 *
 *  FILENAME:     C:\viewstore\wc_hydr1\miniturret\app\common\src\TimeDelay.c
 *
 *  PARAMETERS:   pDelay = Pointer to TIMEDLY structure that defines the delay
 *
 *  DESCRIPTION:  This function will increment the timer and clamp it to the
 *                maximum value to prevent it from rolling over.
 *
 *  RETURNS:      Nothing
 *
 */
void vTimeDelayService(TIMEDLY *pDelay)
{
   pDelay->uwCounts++;
   if ( pDelay->uwCounts > pDelay->uwCountsMax )
   {
      pDelay->uwCounts = pDelay->uwCountsMax;
   }
}

/*
 ** fTimeDelayExpired
 *
 *  FILENAME:     C:\viewstore\wc_hydr1\miniturret\app\common\src\TimeDelay.c
 *
 *  PARAMETERS:   pDelay = Pointer to TIMEDLY structure that defines the delay
 *
 *  DESCRIPTION:  This function checks to see if the timer has expired. The
 *                timer can be cleared and reused by using vTimeDelayClear()
 *
 *  RETURNS:      TRUE if expired otherwise FALSE
 *
 */
boolean fTimeDelayExpired(TIMEDLY *pDelay)
{
   boolean fExpired = FALSE;

   if ( pDelay->uwCounts >= pDelay->uwCountsMax )
   {
      fExpired = TRUE;
   }
   return(fExpired);
}

#undef  TIMEDELAY_MODULE

