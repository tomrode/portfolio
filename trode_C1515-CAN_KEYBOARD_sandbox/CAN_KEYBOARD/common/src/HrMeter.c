/***********************************************************************************************
 * File:       HrMeter.c
 *
 * Purpose:    Contains generic functions to provide a software controled
 *             hour meter with hours and seconds
 *
 * Project:    C1103
 *
 * Copyright:  2006 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 05/14/2004
 *             Revised for TI DSP 10/19/2006 by Bill McCroskey
 ************************************************************************************************
*/
#ifdef _TI_2812
#include "types.h"
#else
#include "main.h"
#endif
#define HRMETER_MODULE 1
#include "HrMeter.h"
#undef  HRMETER_MODULE

/*
 ** vHrMeterService
 *
 *  FILENAME:     HrMeter.c
 *
 *  PARAMETERS:   pHrMeter        = pointer to HR_METER structure.
 *                uwServiceRateMs = periodic rate in milliseconds that this
 *                                  function is called.
 *                fAdvanceHrMeter = TRUE if hour meter should be advanced.
 *
 *  DESCRIPTION:  This function is used to service the specified hour meter.
 *                It will add the number of milliseconds specified by
 *                uwServiceRateMs to the hour meter if fAdvanceHrMeter is
 *                TRUE. A save timer is also maintained which can be checked
 *                by the function fHrMeterTimeToSave().
 *
 *  RETURNS:      Nothing
 *
 */
void vHrMeterService(HR_METER *pHrMeter,
                     uword    uwServiceRateMs,
                     boolean  fAdvanceHrMeter
                    )
{
   volatile uword uwHours   = (uword)(pHrMeter->ulHrMeter >> 16);
   volatile uword uwSeconds = (uword)(pHrMeter->ulHrMeter & 0x0000ffff);

   // Advance hour meter if requested
   if ( fAdvanceHrMeter )
   {
      // Add in milliseconds
      pHrMeter->uwMilliseconds += uwServiceRateMs;

      // See if hour has advanced a second
      if ( pHrMeter->uwMilliseconds >= 1000 )
      {
         // Yes, advance seconds
         uwSeconds++;
         pHrMeter->uwMilliseconds -= 1000;

         // See if we advanced an hour
         if ( uwSeconds >= 3600 )
         {
            // Yes, advance hours but don't let it roll over
            if ( uwHours < 65535 )
            {
               uwHours++;
            }
            uwSeconds = 0;
         }
      }
   }

   // Advance the save timer
   pHrMeter->ulSaveTimerMs += uwServiceRateMs;

   // Pack the hour meter
   pHrMeter->ulHrMeter = ((ulong)uwHours << 16) + (ulong)uwSeconds;
}

/*
 ** vHrMeterSet
 *
 *  FILENAME:     HrMeter.c
 *
 *  PARAMETERS:   pHrMeter  = pointer to HR_METER structure.
 *                ulHrMeter = Encoded hour meter reading.
 *
 *  DESCRIPTION:  This function is used to initialize or modify the specified
 *                hour meter.
 *
 *  RETURNS:      Nothing
 *
 */
void vHrMeterSet(HR_METER *pHrMeter, ulong ulHrMeter)
{
   pHrMeter->ulHrMeter = ulHrMeter;
   pHrMeter->uwMilliseconds = 0;
   pHrMeter->ulSaveTimerMs  = 0;
}

/*
 ** uwHrMeterGetHours
 *
 *  FILENAME:     L:\miniturret\app\common\src\HrMeter.c
 *
 *  PARAMETERS:   pHrMeter  = pointer to HR_METER structure.
 *
 *  DESCRIPTION:  This function returns the number of hours in the specified
 *                hour meter without having to directly manipulate the encoded
 *                value.
 *
 *  RETURNS:      Number of hours from 0 to 65535
 *
 */
uword uwHrMeterGetHours(HR_METER *pHrMeter)
{
   return((uword)(pHrMeter->ulHrMeter >> 16));
}

uword uwHrMeterGetHourTenths(HR_METER *pHrMeter)
{
   return((uword)(pHrMeter->ulHrMeter & 0x0000ffff) / 360);
}

/*
 ** uwHrMeterGetMinutes
 *
 *  FILENAME:     L:\miniturret\app\common\src\HrMeter.c
 *
 *  PARAMETERS:   pHrMeter  = pointer to HR_METER structure.
 *
 *  DESCRIPTION:  This function returns the number of minutes in the specified
 *                hour meter without having to directly manipulate the encoded
 *                value.
 *
 *  RETURNS:      Number of minutes from 0 to 59
 *
 */
uword uwHrMeterGetMinutes(HR_METER *pHrMeter)
{
   return((uword)(pHrMeter->ulHrMeter & 0x0000ffff) / 60);
}

/*
 ** uwHrMeterGetSeconds
 *
 *  FILENAME:     HrMeter.c
 *
 *  PARAMETERS:   pHrMeter         = pointer to HR_METER structure.
 *                fMinutesAdjusted = TRUE to adjust for minutes.
 *
 *  DESCRIPTION:  This function returns the number of seconds in the specified
 *                hour meter without having to directly manipulate the encoded
 *                value.
 *
 *  RETURNS:      If fMinutesAdjusted is TRUE, the number of total seconds are
 *                divided by 60 and the remainder is returned. Otherwise the
 *                the total number of seconds is returned.
 *
 *                Example: Seconds = 125
 *                For fMinutesAdjusted == TRUE,  returns 5
 *                For fMinutesAdjusted == FALSE, returns 125
 */
uword uwHrMeterGetSeconds(HR_METER *pHrMeter, boolean fMinutesAdjusted)
{
   uword uwSeconds = (uword)(pHrMeter->ulHrMeter & 0x0000ffff);

   if ( fMinutesAdjusted )
   {
      uwSeconds %= 60;
   }
   return(uwSeconds);
}

/*
 ** fHrMeterTimeToSave
 *
 *  FILENAME:     HrMeter.c
 *
 *  PARAMETERS:   pHrMeter     = pointer to HR_METER structure.
 *                ulSaveRateMs = Save rate in milliseconds
 *
 *  DESCRIPTION:  This function checks if it is time to save the
 *                hour meter to FRAM.
 *
 *  RETURNS:      TRUE if it time to save, otherwise FALSE
 *
 */
boolean fHrMeterTimeToSave(HR_METER *pHrMeter, ulong ulSaveRateMs)
{
   boolean fSave = FALSE;

   if ( pHrMeter->ulSaveTimerMs >= ulSaveRateMs )
   {
      fSave = TRUE;
      pHrMeter->ulSaveTimerMs = 0;
   }
   return(fSave);
}

