/***********************************************************************************************
 * File:       RangeChk.c
 *
 * Purpose:    Generic range check functions. Configurable to check min, max
 *             or range. The signal must be out of range for a configurable
 *             amount of time before the check fails.
 *
 * Project:    C584
 *
 * Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 05/19/2004
 *
 ************************************************************************************************
 */
#include "datalog.h"
#define RANGECHK_MODULE 1
#include "RangeChk.h"

/*
 ** vRangeCheckInit
 *
 *  FILENAME:     L:\miniturret\app\common\src\RangeChk.c
 *
 *  PARAMETERS:   pRc            = Pointer to RNG_CHK structure
 *                swToleranceMs  = Time value has to be out of range to fault
 *                swUpdateRateMs = How often fnRangeCheck() is called in ms
 *
 *  DESCRIPTION:  Initialize Range check. Must be done before calling
 *                fnRangeCheck() for the first time.
 *
 *  RETURNS:      Nothing
 *
 */
void vRangeCheckInit(RNG_CHK *pRc, sword swToleranceMs, sword swUpdateRateMs)
{
   pRc->fPerformTest       = TRUE;
   pRc->swConcernCount     = 0;
   if ( swUpdateRateMs )
   {
      pRc->swConcernCountMax = swToleranceMs / swUpdateRateMs;
   }
   else
   {
      pRc->swConcernCountMax = 0;
   }
}

/*
 ** vRangeCheckDisable
 *
 *  FILENAME:     L:\miniturret\app\common\src\RangeChk.c
 *
 *  PARAMETERS:   pRc            = Pointer to RNG_CHK structure
 *
 *  DESCRIPTION:  Disables the range check. This is commonly done after the
 *                test fails the first time.
 *
 *  RETURNS:      Nothing
 *
 */
void vRangeCheckDisable(RNG_CHK *pRc)
{
   pRc->fPerformTest = FALSE;
}

/*
 ** vRangeCheckReset
 *
 *  FILENAME:     C:\viewstore\wc_flow1\miniturret\app\common\src\RangeChk.c
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  This check resets the test by clearing the concern count
 *
 *  RETURNS:      Nothing
 *
 */
void vRangeCheckReset(RNG_CHK *pRc)
{
   pRc->swConcernCount = 0;
}

/*
 ** fnRangeCheck
 *
 *  FILENAME:     L:\miniturret\app\common\src\RangeChk.c
 *
 *  PARAMETERS:   pRc      = Pointer to RNG_CHK structure
 *                swValue  = Value that is checked
 *                swMin    = Min value for fault
 *                swMax    = Max value for fault
 *                fDatalog = TRUE to datalog otherwise FALSE
 *                Check    = CHK_MIN, CHK_MAX or CHK_RNG
 *
 *  DESCRIPTION:  Performs range check on the specified value. vRangeCheckInit()
 *                must be called prior to calling this function for the first
 *                time.
 *
 *  RETURNS:      RNG_OK if not outside range for longer than the specified time
 *                RNG_LO if lower than the minimum for longer than allowed
 *                RNG_HI if higher than the maximum for longer than allowed
 */
RNG_RTN fnRangeCheck(RNG_CHK *pRc,
                     sword   swValue,
                     sword   swMin,
                     sword   swMax,
                     boolean fDataLog,
                     RNG_CHK_TYPE Check
                    )
{
   RNG_RTN RangeResult = RNG_OK;

   // If test has already failed then skip it
   if ( pRc->fPerformTest )
   {
      // Make sure value is within the acceptable range
      switch ( Check )
      {
      case CHK_MIN:
         if ( swValue < swMin )
         {
            if ( pRc->swConcernCount > pRc->swConcernCountMax )
            {
               RangeResult = RNG_LO;
            }
            else
            {
               pRc->swConcernCount++;
            }
         }
         else
         {
            pRc->swConcernCount = 0;
         }
         break;

      case CHK_MAX:
         if ( swValue > swMax )
         {
            if ( pRc->swConcernCount > pRc->swConcernCountMax )
            {
               RangeResult = RNG_HI;
            }
            else
            {
               pRc->swConcernCount++;
            }
         }
         else
         {
            pRc->swConcernCount = 0;
         }
         break;

      default:
      case CHK_RNG:
         if ( (swValue < swMin) || (swValue > swMax) )
         {
            if ( pRc->swConcernCount > pRc->swConcernCountMax )
            {
               if ( swValue < swMin )
               {
                  RangeResult = RNG_LO;
               }
               else
               {
                  RangeResult = RNG_HI;
               }
            }
            else
            {
               pRc->swConcernCount++;
            }
         }
         else
         {
            pRc->swConcernCount = 0;
         }
         break;
      }
   }

   // Datalog if requested
   if ( fDataLog )
   {
      Can1.ubData[0] = (ubyte)(pRc->fPerformTest          );
      Can1.ubData[1] = (ubyte)(Check                      );
      Can1.ubData[2] = (ubyte)(swValue                >> 8);
      Can1.ubData[3] = (ubyte)(swValue                    );
      Can1.ubData[4] = (ubyte)(swMin                  >> 8);
      Can1.ubData[5] = (ubyte)(swMin                      );
      Can1.ubData[6] = (ubyte)(swMax                  >> 8);
      Can1.ubData[7] = (ubyte)(swMax                      );
      Can2.ubData[0] = (ubyte)(pRc->swConcernCount    >> 8);
      Can2.ubData[1] = (ubyte)(pRc->swConcernCount        );
      Can2.ubData[2] = (ubyte)(pRc->swConcernCountMax >> 8);
      Can2.ubData[3] = (ubyte)(pRc->swConcernCountMax     );
      Can2.ubData[4] = (ubyte)RangeResult;
      vSaveDatalog(&Can1, &Can2);
   }
   return(RangeResult);
}

#undef  RANGECHK_MODULE
