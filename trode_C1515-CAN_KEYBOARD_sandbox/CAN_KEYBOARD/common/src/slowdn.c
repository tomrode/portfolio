/***********************************************************************************************
 * File:       slowdn.c
 *
 * Purpose:    Controls slowdowns/stops for main hoist
 *
 * Project:    C584
 *
 * Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 12/11/2003
 *
 ************************************************************************************************
*/
#include <stdlib.h>
#include "types.h"
#include "param.h"
#include "mhoist.h"
#include "mainThr.h"
#include "datalog.h"
#include "Truck_PDO.h"
#include "control.h"
#include "Height.h"
#include "Param.h"
#include "AcSlave.h"
#include "Sro.h"
#define SLOWDN_MODULE 1
#include "slowdn.h"

/*
 ** vSlowDownSetup
 *
 *  FILENAME:     L:\miniturret\app\common\src\slowdn.c
 *
 *  PARAMETERS:   pSd           = Pointer to SLOWDN structure
 *                swDecelDist   = Start of deceleration ramp
 *                swAccelDist   = End of acceleration ramp
 *                swStopHyst    = Hysteresis used for stop position
 *                swMinNegSpeed = Minimum speed in negative direction
 *                swMinPosSpeed = Minimum speed in positive direction
 *                fAccelAfterSd = TRUE to accelerate after a slowdown
 *                                FALSE to maintain slowdown speed
 *
 *  DESCRIPTION:  Used to define parameters controlling a slowdown or stop.
 *
 *  RETURNS:      Nothing
 *
 */
void vSlowDownSetup(SLOWDN *pSd,
                    sword swDecelDist,
                    sword swAccelDist,
                    sword swStopHyst,
                    sword swMinNegSpeed,
                    sword swMinPosSpeed,
                    boolean fAccelAfterSd
                   )
{
   pSd->swDecelDist   = swDecelDist;
   pSd->swAccelDist   = swAccelDist;
   pSd->swStopHyst    = swStopHyst;
   pSd->swMinNegSpd   = swMinNegSpeed;
   pSd->swMinPosSpd   = swMinPosSpeed;
   pSd->ubStopState   = STP_INIT;
   pSd->fAccelAfterSd = fAccelAfterSd;
}

uword uwEncodeCutoutSwitches(boolean fSw1, boolean fSw2, boolean fSw3, boolean fSw4 )
{
    uword uwOut = 0;

    if(fSw1)
    {
        uwOut |= (1u << TR_SW1);
    }

    if(fSw2)
    {
        uwOut |= (1u << TR_SW2);
    }

    if(fSw3)
    {
        uwOut |= (1u << TR_SW3);
    }

    if(fSw4)
    {
        uwOut |= (1u << TR_SW4);
    }

    return( uwOut );
}

/*
 ** swSlowDownClamp
 *
 *  FILENAME:     L:\miniturret\app\common\src\slowdn.c
 *
 *  PARAMETERS:   pSd         = Pointer to SLOWDN type
 *                VelDir      = Current direction VEL_POS or VEL_NEG
 *                swCurrPos   = Current position
 *                swMaxNegSpd = Maximum speed setpoint allowed in the VEL_NEG
 *                              direction.
 *                swMaxPosSpd = Maximum speed setpoint allowed in the VEL_POS
 *                              direction.
 *
 *  DESCRIPTION:  Handles the slowdown and stop for the specified cutout
 *
 *  RETURNS:      Speed clamp for the specified cutout.
 *
 */
sword swSlowDownClamp(SLOWDN  *pSd,
                      VEL_TYPE VelDir,
                      sword    swCurrPos,
                      sword    swMaxNegSpd,
                      sword    swMaxPosSpd,
                      uword    uwSwitchMask,
                      ubyte    ubSpeed,
                      boolean  fDatalog
                     )
{
   sword swSpdClamp;
   pSd->fInSlowdown = FALSE;

   if( pSd->Trigger == TR_SENSOR )
   {
       // Calculate speed clamp within slowdown region
       swSpdClamp = swGetSlowdown(pSd,
                                  VelDir,
                                  swCurrPos,
                                  swMaxNegSpd,
                                  swMaxPosSpd,
                                  ubSpeed,
                                  fDatalog
                                 );
   }
   else
   {   // Trigger on SwitchInput
       if( VelDir == VEL_POS )
       {
           swSpdClamp = swMaxPosSpd;
       }
       else
       {
           swSpdClamp = swMaxNegSpd;
       }
   }

   // Set clamp to zero if stop required
   if ( fGetStop(pSd, VelDir, swCurrPos, uwSwitchMask, ubSpeed) )
   {
      swSpdClamp = 0;
      pSd->fInSlowdown = TRUE;
   }

   return(swSpdClamp);
}

/*
 ** swGetSlowdown
 *
 *  FILENAME:     L:\miniturret\app\common\src\slowdn.c
 *
 *  PARAMETERS:   pSd         = Pointer to SLOWDN type
 *                VelDir      = Current direction VEL_POS or VEL_NEG
 *                swCurrPos   = Current position
 *                swMaxNegSpd = Maximum speed setpoint allowed in the VEL_NEG
 *                              direction.
 *                swMaxPosSpd = Maximum speed setpoint allowed in the VEL_POS
 *                              direction.
 *
 *  DESCRIPTION:  Handles the slowdown for the specified cutout.
 *
 *  RETURNS:      Speed clamp for the specified cutout.
 *
 */
sword swGetSlowdown(SLOWDN  *pSd,
                     VEL_TYPE VelDir,
                     sword    swCurrPos,
                     sword    swMaxNegSpd,
                     sword    swMaxPosSpd,
                     ubyte    ubSpeed,
                     boolean  fDoDatalog
                    )
{
   sword      swDecelS;
   sword      swAccelE;
   sword      swSpdClamp;
   MAP_SLOWDN SlowDownMap;
   boolean    fInSlowdown = FALSE;
   uword      location = 0;

   // Initialize speed clamp
   if ( VelDir == VEL_POS )
   {
      swSpdClamp = swMaxPosSpd;
   }
   else
   {
      swSpdClamp = swMaxNegSpd;
   }

   // Make sure it is a valid cutout type
   if ( pSd->Type > SD_NONE && pSd->Type < SD_INVALID )
   {
      if(     (pSd->Mph == 0 )
           || (pSd->Mph != 0 && ubSpeed >= pSd->Mph )
         )
      {
          location |= 0x0001;
          // Initialize Slowdown variables based on direction of velocity request
          if ( VelDir == VEL_POS )
          {
             location |= 0x0002;
             // Calculate slowdown range
             swDecelS = pSd->swPosition - pSd->swDecelDist;
             swAccelE = pSd->swPosition + pSd->swAccelDist;

             // Check if in Slowdown range
             if (    (swCurrPos > swDecelS)
                 && ((swCurrPos < swAccelE) || (pSd->fAccelAfterSd == FALSE) )
                )
             {
                location |= 0x0004;
                // Check type to see if we need to slowdown
                switch (pSd->Type )
                {
                case SD_POS_SLOW:
                case SD_POS_CUT:
                case SD_POS_STOP:
                case SD_BOTH_SLOW:
                   if ( pSd->fAccelAfterSd )
                   {
                      location |= 0x0008;
                      // Accelerate above slowdown
                      SlowDownMap.swNumPoints = 3;
                      SlowDownMap.swInpoint [0] = swDecelS;
                      SlowDownMap.swInpoint [1] = pSd->swPosition;
                      SlowDownMap.swInpoint [2] = swAccelE;
                   }
                   else
                   {
                       location |= 0x0010;
                      // Maintain min speed above slowdown
                      SlowDownMap.swNumPoints = 2;
                      SlowDownMap.swInpoint [0] = swDecelS;
                      SlowDownMap.swInpoint [1] = pSd->swPosition;
                      SlowDownMap.swInpoint [2] = pSd->swPosition;
                   }

                   fInSlowdown               = TRUE;
                   SlowDownMap.swOutpoint[0] = swMaxPosSpd;
                   SlowDownMap.swOutpoint[1] = pSd->swMinPosSpd;
                   SlowDownMap.swOutpoint[2] = swMaxPosSpd;

                   // If in decel range only allow position to decrease
                   if ( (swCurrPos > swDecelS) && (swCurrPos < pSd->swPosition) )
                   {
                      location |= 0x0020;
                      if ( swCurrPos < pSd->swPositionLast )
                      {
                         location |= 0x0040;
                         swCurrPos = pSd->swPositionLast;
                      }
                   }

                   break;

                default:
                   break;
                }
             }
          }
          else  // Negative velocity
          {
             location |= 0x0080;
             // Calculate slowdown range
             swDecelS = pSd->swPosition + pSd->swDecelDist;
             swAccelE = pSd->swPosition - pSd->swAccelDist;

             // Check if in Slowdown range
             if (    (swCurrPos < swDecelS)
                 && ((swCurrPos > swAccelE) || (pSd->fAccelAfterSd == FALSE) )
                )
             {
                 location |= 0x0100;
                // Check type to see if we need to slowdown
                switch (pSd->Type )
                {
                case SD_NEG_SLOW:
                case SD_NEG_CUT:
                case SD_NEG_STOP:
                case SD_BOTH_SLOW:
                       if ( pSd->fAccelAfterSd )
                       {
                           location |= 0x0008;
                           // Accelerate above slowdown
                           SlowDownMap.swNumPoints = 3;
                           SlowDownMap.swInpoint [0] = swAccelE;
                           SlowDownMap.swInpoint [1] = pSd->swPosition;
                           SlowDownMap.swInpoint [2] = swDecelS;

                           fInSlowdown               = TRUE;
                           SlowDownMap.swOutpoint[0] = swMaxNegSpd;
                           SlowDownMap.swOutpoint[1] = pSd->swMinNegSpd;
                           SlowDownMap.swOutpoint[2] = swMaxNegSpd;

                       }
                       else
                       {
                           location |= 0x0010;
                           // Maintain min speed above slowdown
                           SlowDownMap.swNumPoints = 3;
                           SlowDownMap.swInpoint [0] = swAccelE;
                           SlowDownMap.swInpoint [1] = pSd->swPosition;
                           SlowDownMap.swInpoint [2] = swDecelS;

                           fInSlowdown               = TRUE;
                           SlowDownMap.swOutpoint[0] = pSd->swMinNegSpd;
                           SlowDownMap.swOutpoint[1] = pSd->swMinNegSpd;
                           SlowDownMap.swOutpoint[2] = swMaxNegSpd;

                        }

                        // If in decel range only allow position to decrease
                        if ( (swCurrPos < swDecelS) && (swCurrPos > pSd->swPosition) )
                        {
                            location |= 0x1000;
                           if ( swCurrPos > pSd->swPositionLast )
                           {
                              location |= 0x2000;
                              swCurrPos = pSd->swPositionLast;
                           }
                        }
                   break;

                default:
                    location |= 0x4000;
                   break;
                }
             }
          }

          // Calculate slowdown clamp
          if ( fInSlowdown )
          {
             location |= 0x8000;
             swSpdClamp = swCntlMap(swCurrPos,
                                    SlowDownMap.swNumPoints,
                                    (sword *)&SlowDownMap.swInpoint,
                                    (sword *)&SlowDownMap.swOutpoint
                                   );
          }
      }
   }

   // Save position for next pass
   pSd->swPositionLast = swCurrPos;
   pSd->fInSlowdown = fInSlowdown;

   if (mCheckDatalog( DL230_HOIST_SLOWDOWN ) && fDoDatalog)
   {
      Can1.ubData[0] = (ubyte)(VelDir);
      Can1.ubData[1] = (ubyte)(swDecelS >> 8);
      Can1.ubData[2] = (ubyte)(swDecelS     );
      Can1.ubData[3] = (ubyte)(swAccelE >> 8);
      Can1.ubData[4] = (ubyte)(swAccelE     );
      Can1.ubData[5] = (ubyte)pSd->Type;
      Can1.ubData[6] = (ubyte)pSd->Mph;
      Can1.ubData[7] = (ubyte)pSd->fAccelAfterSd;

      Can2.ubData[0] = (ubyte)(SlowDownMap.swInpoint[0] >> 8);
      Can2.ubData[1] = (ubyte)(SlowDownMap.swInpoint[0]     );
      Can2.ubData[2] = (ubyte)(SlowDownMap.swInpoint[1] >> 8);
      Can2.ubData[3] = (ubyte)(SlowDownMap.swInpoint[1]     );
      Can2.ubData[4] = (ubyte)(SlowDownMap.swInpoint[2] >> 8);
      Can2.ubData[5] = (ubyte)(SlowDownMap.swInpoint[2]     );
      Can2.ubData[6] = (ubyte)(SlowDownMap.swOutpoint[0] >> 8);
      Can2.ubData[7] = (ubyte)(SlowDownMap.swOutpoint[0]     );

      Can3.ubData[0] = (ubyte)(SlowDownMap.swOutpoint[1] >> 8);
      Can3.ubData[1] = (ubyte)(SlowDownMap.swOutpoint[1]     );
      Can3.ubData[2] = (ubyte)(SlowDownMap.swOutpoint[2] >> 8);
      Can3.ubData[3] = (ubyte)(SlowDownMap.swOutpoint[2]     );
      Can3.ubData[4] = (ubyte)(location >> 8);
      Can3.ubData[5] = (ubyte)(location     );
      Can3.ubData[6] = (ubyte)(swSpdClamp   >> 8);
      Can3.ubData[7] = (ubyte)(swSpdClamp       );

      Can4.ubData[0] = (ubyte)(swCurrPos >> 8);
      Can4.ubData[1] = (ubyte)(swCurrPos     );
      Can4.ubData[2] = (ubyte)(pSd->swPositionLast >> 8);
      Can4.ubData[3] = (ubyte)(pSd->swPositionLast     );
      Can4.ubData[4] = (ubyte)(pSd->swPosition     >> 8);
      Can4.ubData[5] = (ubyte)(pSd->swPosition         );
      Can4.ubData[6] = (ubyte)(SlowDownMap.swNumPoints );
      Can4.ubData[7] = (ubyte)fInSlowdown;

      vSaveDatalogExtended(&Can1, &Can2, &Can3, &Can4);
   }

   // Return speed clamp
   return(swSpdClamp);
}

/*
 ** fGetStop
 *
 *  FILENAME:     L:\miniturret\app\common\src\slowdn.c
 *
 *  PARAMETERS:   pSd         = Pointer to SLOWDN type
 *                VelDir      = Current direction VEL_POS or VEL_NEG
 *                swCurrPos   = Current position
 *
 *  DESCRIPTION:  State machine that controls the stop of the specified cutout
 *
 *  RETURNS:      TRUE if stopped otherwise FALSE
 *
 */
boolean fGetStop(SLOWDN *pSd, VEL_TYPE VelDir, sword swCurrPos, uword uwSwitchMask, ubyte ubSpeed)
{
   boolean fStop = FALSE;

   // Handle stop for cutouts and stops
   if (   (pSd->Type == SD_POS_CUT) || (pSd->Type == SD_POS_STOP)
       || (pSd->Type == SD_NEG_CUT) || (pSd->Type == SD_NEG_STOP)
      )
   {
      /*
       * Process current state
       */
      switch ( pSd->ubStopState )
      {
      case STP_INIT:
         break;

      case STP_UNRESTRICTED:
         break;

      case STP_AT_STOP:
         if (   ((pSd->Type == SD_POS_CUT ) && (VelDir == VEL_POS))
             || ((pSd->Type == SD_POS_STOP) && (VelDir == VEL_POS))
             || ((pSd->Type == SD_NEG_CUT ) && (VelDir == VEL_NEG))
             || ((pSd->Type == SD_NEG_STOP) && (VelDir == VEL_NEG))
            )
         {
            fStop = TRUE;
         }
         break;

      case STP_RESTRICTED:
         if (   ((pSd->Type == SD_POS_CUT) && (VelDir == VEL_POS))
             || ((pSd->Type == SD_NEG_CUT) && (VelDir == VEL_NEG))
            )
         {
            if ( pSd->fOverride == FALSE )
            {
               fStop = TRUE;
            }
         }
         break;

      default:
         fStop = TRUE;
         break;
      }

      /*
       * Find next state
       */
      switch ( pSd->ubStopState )
      {
      case STP_INIT:
         pSd->ubStopState = STP_UNRESTRICTED;
         break;

      case STP_UNRESTRICTED:
         switch ( pSd->Type )
         {
         case SD_POS_CUT:
         case SD_POS_STOP:
            if ( VelDir == VEL_POS )
            {
                switch( pSd->Trigger )
                {
                case TR_SW1:
                case TR_SW2:
                case TR_SW3:
                case TR_SW4:
                    if (     (uwSwitchMask & (1 << pSd->Trigger))
                         &&  ((pSd->Mph == 0)
                         ||  ((pSd->Mph != 0) && (ubSpeed >= pSd->Mph)))
                       )
                    {
                        pSd->ubStopState = STP_AT_STOP;
                    }
                    break;

                case TR_SENSOR:
                default:
                    if(    (swCurrPos >= pSd->swPosition)
                        && ((pSd->Mph == 0)
                        || ((pSd->Mph != 0) && (ubSpeed >= pSd->Mph)))
                      )
                    {
                       pSd->ubStopState = STP_AT_STOP;
                    }
                    break;
                }
            }
            break;

         case SD_NEG_CUT:
         case SD_NEG_STOP:
             if ( VelDir == VEL_NEG )
             {
                 switch( pSd->Trigger )
                 {
                 case TR_SW1:
                 case TR_SW2:
                 case TR_SW3:
                 case TR_SW4:
                     if (   !(uwSwitchMask & (1 << pSd->Trigger))
                          && ((pSd->Mph == 0)
                          || ((pSd->Mph != 0) && (ubSpeed >= pSd->Mph)))
                        )
                     {
                         pSd->ubStopState = STP_AT_STOP;
                     }
                     break;

                 case TR_SENSOR:
                 default:
                     if(    (swCurrPos <= pSd->swPosition )
                         && ((pSd->Mph == 0)
                         || ((pSd->Mph != 0) && (ubSpeed >= pSd->Mph)))
                       )
                     {
                        pSd->ubStopState = STP_AT_STOP;
                     }
                     break;
                 }
             }
            break;

         default:
            break;
         }
         break;

      case STP_AT_STOP:
         switch ( pSd->Type )
         {
         case SD_POS_CUT:
         case SD_POS_STOP:
             if ( VelDir == VEL_POS )
             {
                 switch( pSd->Trigger )
                 {
                 case TR_SW1:
                 case TR_SW2:
                 case TR_SW3:
                 case TR_SW4:
                     if (    !(uwSwitchMask & (1 << pSd->Trigger))
                           || (   (pSd->Mph != 0)
                              &&  (
                                       ((pSd->Mph > RCO_SPEED_HYSTERSIS) && (ubSpeed < (pSd->Mph - RCO_SPEED_HYSTERSIS)))
                                    || ((pSd->Mph <= RCO_SPEED_HYSTERSIS) && (ubSpeed == 0))
                                  )
                              )
                        )
                     {
                         pSd->ubStopState = STP_UNRESTRICTED;
                     }
                     break;

                 case TR_SENSOR:
                 default:
                     if(    (swCurrPos <= (pSd->swPosition - pSd->swStopHyst))
                         || (   (pSd->Mph != 0)
                            &&  (
                                     ((pSd->Mph > RCO_SPEED_HYSTERSIS) && (ubSpeed < (pSd->Mph - RCO_SPEED_HYSTERSIS)))
                                  || ((pSd->Mph <= RCO_SPEED_HYSTERSIS) && (ubSpeed == 0))
                                )
                            )
                       )
                     {
                        pSd->ubStopState = STP_UNRESTRICTED;
                     }
                     break;
                 }
             }
             if( (pSd->fOverride == FALSE) && (pSd->Type == SD_POS_CUT) )
             {
                pSd->ubStopState = STP_RESTRICTED;
             }
            break;

         case SD_NEG_CUT:
         case SD_NEG_STOP:
             if ( VelDir == VEL_NEG )
             {
                 switch( pSd->Trigger )
                 {
                 case TR_SW1:
                 case TR_SW2:
                 case TR_SW3:
                 case TR_SW4:
                     if (    (uwSwitchMask & (1 << pSd->Trigger))
                          || (   (pSd->Mph != 0)
                             &&  (
                                      ((pSd->Mph > RCO_SPEED_HYSTERSIS) && (ubSpeed < (pSd->Mph - RCO_SPEED_HYSTERSIS)))
                                   || ((pSd->Mph <= RCO_SPEED_HYSTERSIS) && (ubSpeed == 0))
                                 )
                             )
                        )
                     {
                         pSd->ubStopState = STP_UNRESTRICTED;
                     }
                     break;

                 case TR_SENSOR:
                 default:
                     if(    (swCurrPos >= (pSd->swPosition + pSd->swStopHyst))
                         || (   (pSd->Mph != 0)
                            &&  (
                                    ((pSd->Mph > RCO_SPEED_HYSTERSIS) && (ubSpeed < (pSd->Mph - RCO_SPEED_HYSTERSIS)))
                                 || ((pSd->Mph <= RCO_SPEED_HYSTERSIS) && (ubSpeed == 0))
                                )
                            )
                       )
                     {
                        pSd->ubStopState = STP_UNRESTRICTED;
                     }
                     break;
                 }
             }
            if( (pSd->fOverride == FALSE) && (pSd->Type == SD_NEG_CUT) )
            {
               pSd->ubStopState = STP_RESTRICTED;
            }
            break;

         default:
            break;
         }
         break;

      case STP_RESTRICTED:
         switch ( pSd->Type )
         {
         case SD_POS_CUT:
         case SD_POS_STOP:
             if ( VelDir == VEL_POS )
             {
                 switch( pSd->Trigger )
                 {
                 case TR_SW1:
                 case TR_SW2:
                 case TR_SW3:
                 case TR_SW4:
                     if (  !(uwSwitchMask & (1 << pSd->Trigger))
                         || (   (pSd->Mph != 0)
                            &&  (
                                     ((pSd->Mph > RCO_SPEED_HYSTERSIS) && (ubSpeed < (pSd->Mph - RCO_SPEED_HYSTERSIS)))
                                  || ((pSd->Mph <= RCO_SPEED_HYSTERSIS) && (ubSpeed == 0))
                                )
                            )
                        )
                     {
                         pSd->ubStopState = STP_UNRESTRICTED;
                     }
                     break;

                 case TR_SENSOR:
                 default:
                     if(    (swCurrPos <= (pSd->swPosition - pSd->swStopHyst))
                         || (   (pSd->Mph != 0)
                            &&  (
                                     ((pSd->Mph > RCO_SPEED_HYSTERSIS) && (ubSpeed < (pSd->Mph - RCO_SPEED_HYSTERSIS)))
                                  || ((pSd->Mph <= RCO_SPEED_HYSTERSIS) && (ubSpeed == 0))
                                )
                            )
                       )
                     {
                        pSd->ubStopState = STP_UNRESTRICTED;
                     }
                     break;
                 }
            }
            else if( VelDir == VEL_NEG )
            {
               pSd->ubStopState = STP_UNRESTRICTED;
            }
            break;

         case SD_NEG_CUT:
         case SD_NEG_STOP:
            if ( VelDir == VEL_NEG )
            {
                switch( pSd->Trigger )
                {
                case TR_SW1:
                case TR_SW2:
                case TR_SW3:
                case TR_SW4:
                    if (   (uwSwitchMask & (1 << pSd->Trigger))
                        || (   (pSd->Mph != 0)
                           &&  (
                                    ((pSd->Mph > RCO_SPEED_HYSTERSIS) && (ubSpeed < (pSd->Mph - RCO_SPEED_HYSTERSIS)))
                                 || ((pSd->Mph <= RCO_SPEED_HYSTERSIS) && (ubSpeed == 0))
                               )
                           )
                       )
                    {
                        pSd->ubStopState = STP_UNRESTRICTED;
                    }
                    break;

                case TR_SENSOR:
                default:
                    if(    (swCurrPos >= (pSd->swPosition + pSd->swStopHyst))
                        || (   (pSd->Mph != 0)
                           &&  (
                                    ((pSd->Mph > RCO_SPEED_HYSTERSIS) && (ubSpeed < (pSd->Mph - RCO_SPEED_HYSTERSIS)))
                                 || ((pSd->Mph <= RCO_SPEED_HYSTERSIS) && (ubSpeed == 0))
                               )
                           )
                      )
                    {
                       pSd->ubStopState = STP_UNRESTRICTED;
                    }
                    break;
                 }
            }
            break;

         default:
            break;
         }
         break;

      default:
         break;
      }
   }

   // Update in cutout structure and return stopped status
   pSd->fStopped = fStop;

   return(fStop);
}


/*
 ** vSetSlowDownOverride
 *
 *  FILENAME:     L:\miniturret\app\common\src\slowdn.c
 *
 *  PARAMETERS:   pSd    = Pointer to SLOWDN type
 *                fState = What fOverride should be set to TRUE or FALSE
 *
 *  DESCRIPTION:  Sets fOverride to the desired state
 *
 *  RETURNS:      Nothing
 *
 */
void vSetSlowDownOverride(SLOWDN *pSd, boolean fState, boolean fNeutralCmd)
{
   static boolean fPrevState = FALSE;
   boolean fResult = fState;

   if(pSd->fOverride_Latch == TRUE)
   {
       if(pSd->ubStopState == STP_RESTRICTED)
       {
           if( fNeutralCmd == FALSE)
           {
               if(    (fState == TRUE && fPrevState == FALSE)
                   || (fPrevState == TRUE)
                 )
               {
                   fResult = TRUE;
                   fPrevState = TRUE;
               }
           }
           else
           {
               fResult = FALSE;
               fPrevState = FALSE;
           }
       }
   }

   // Datalog if necessary
   if ( mCheckDatalog( DL229_OVERRIDE_LATCH ) && pSd->fOverride_Latch == TRUE )
   {
      Can1.ubData[0] = (ubyte)pSd->fOverride_Latch;
      Can1.ubData[1] = (ubyte)pSd->ubStopState;
      Can1.ubData[2] = (ubyte)pSd->fStopped;
      Can1.ubData[3] = (ubyte)fNeutralCmd;
      Can1.ubData[4] = (ubyte)fState;
      Can1.ubData[5] = (ubyte)fPrevState;
      Can1.ubData[6] = (ubyte)fResult;
      Can1.ubData[7] = (ubyte)gfHoistAllowed();
      Can2.ubData[0] = (ubyte)(gswMhThrottleScaled >> 8);
      Can2.ubData[1] = (ubyte)(gswMhThrottleScaled     );
      Can2.ubData[2] = (ubyte)(pSd->swPosition >> 8);
      Can2.ubData[3] = (ubyte)(pSd->swPosition     );
      Can2.ubData[4] = (ubyte)(pSd->Mph            );
      Can2.ubData[5] = (ubyte)(pSd->Trigger        );
      Can2.ubData[6] = (ubyte)(pSd->Type           );

      vSaveDatalog(&Can1, &Can2);
   }

   pSd->fOverride = fResult;
}

/*
 ** vServiceMainRaiseInMechanicalLimit
 *
 *  FILENAME:     slowdn.c
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  This function checks if we are in a main raise mechanical
 *                limit. The encoder is calibrated so we know pretty much where
 *                the limit is but we still may not be able to reach it exactly.
 *
 *  Note:         Use the function fMainRaiseInMechanicalLimit() to get the
 *                output of this function.
 *
 *  RETURNS:      Nothing
 *
 */
void vServiceMainRaiseInMechanicalLimit(void)
{
   VEL_TYPE VelDir;
   sword swMainMastHeight = gswMainHeight + gParams.uwMaxHeightLimitTol;

   // Get velocity direction
   VelDir  = fnGetMainHoistVelocityDir();

   // Check if trying to raise into a mechanical stop
   if (   (gCutouts[NUM_CUTOUTS-2].Type             == SD_POS_SLOW)    /*Only Set in Cal during Main Height determination*/
       && (VelDir                                   == VEL_POS)
       && (fMainHoistVelocityNearZero()             == TRUE)
       && (swMainMastHeight >= (gCutouts[NUM_CUTOUTS-2].swPosition + mHoistPosition(12)))   /*Test if at feature Max Height*/
      )
   {
      // Yes, has timeout occured?
      if ( swMainInMechanicalLimitTimer > (sword)MAIN_IN_MECHANICAL_LIMIT_CNTS )
      {
         // Yes, in a limit
         fMainInMechanicalLimit = TRUE;
      }
      else
      {
         // No, continue timeout
         fMainInMechanicalLimit = FALSE;
         swMainInMechanicalLimitTimer++;
      }
   }
   else
   {
      // No, not raising into a limit
      fMainInMechanicalLimit = FALSE;
      swMainInMechanicalLimitTimer = 0;
   }

   // Datalog if necessary
   if ( mCheckDatalog( DL192_MAIN_IN_MECH_LIMIT ) )
   {
      Can1.ubData[0] = fMainInMechanicalLimit;
      Can1.ubData[1] = (ubyte)VelDir;
      Can1.ubData[2] = (fMainHoistVelocityNearZero());
      Can1.ubData[3] = (gCutouts[NUM_CUTOUTS-2].Type);
      Can1.ubData[4] = (ubyte)(swMainMastHeight >>8);
      Can1.ubData[5] = (ubyte)(swMainMastHeight);
      Can1.ubData[6] = (ubyte)(gCutouts[NUM_CUTOUTS-2].swPosition >> 8);
      Can1.ubData[7] = (ubyte)(gCutouts[NUM_CUTOUTS-2].swPosition);
      Can2.ubData[0] = (ubyte)(swMainInMechanicalLimitTimer >> 8);
      Can2.ubData[1] = (ubyte)(swMainInMechanicalLimitTimer);
      Can2.ubData[2] = (ubyte)(swSpeed  >> 8);
      Can2.ubData[3] = (ubyte)(swSpeed);
      Can2.ubData[4] = (ubyte)(gslGetMastVelocity()  >> 8);
      Can2.ubData[5] = (ubyte)(gslGetMastVelocity());

      vSaveDatalog(&Can1, &Can2);
   }
}

/*
 ** fMainHoistVelocityNearZero
 *
 *  FILENAME:     slowdn.c
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  This function is used to see if Main hoist velocity
 *                is near zero.
 *
 *  RETURNS:      TRUE if velocity is near zero, otherwise FALSE
 *
 */
boolean fMainHoistVelocityNearZero(void)
{
   boolean fNearZero = FALSE;
   sword swMainHoistVelocity = (sword)gslGetMastVelocity();

   swSpeed = swCntlFilter(&SpdFilter, swMainHoistVelocity);

   if ( swSpeed < gParams.swMainInLimitNoVelocity )
   {
      fNearZero = TRUE;
   }
   return(fNearZero);
}

/*
 ** fMainRaiseInMechanicalLimit
 *
 *  FILENAME:     slowdn.c
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  This function is used to get the status of the function
 *                vServiceMainRaiseInMechanicalLimit(). It is used to see
 *                if the main raise command has raised into a mechanical
 *                limit.
 *
 *  RETURNS:      TRUE if in a mechanical limit, otherwise FALSE
 *
 */
boolean fMainRaiseInMechanicalLimit(void)
{
   return(fMainInMechanicalLimit);
}

#undef  SLOWDN_MODULE
