;//###########################################################################
;//
;// FILE:  ParamFlag.asm
;//
;// TITLE: Stores the parameter flag in flash.
;//
;//###########################################################################


***********************************************************************


    .sect "parameterflag"

     .word 0h  ; 0 for normal operation and downloads, 01234h for debug with JTAG download
     ;.word 01234h  ; 0 for normal operation and downloads, 01234h for debug with JTAG download
     .end              ; NOTE:  you can't download a file with 01234h for a value with the app downloader

; end of file
