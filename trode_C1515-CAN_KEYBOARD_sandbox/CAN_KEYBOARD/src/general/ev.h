/************************************************************************************************
 *  File:       Ev.h
 *
 *  Purpose:    Event Manager drive service
 *
 *  Project:    C1314
 *
 *  Copyright:  2007 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/1/2007
 *
 ************************************************************************************************
*/

#include "types.h"
#include "fram.h"
#include "param.h"
#include "IQmathLib.h"

#ifndef EV_Registration
    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/

    #define SUPPLY_VOLTS_RESET 6.0

    #define CMPRMAX_8K     3750

    #define CMPRMAXdiv2_8K 1875
    #define MAXPWMLIMIT_8K 3750
    #define MINPWMLIMIT_8K 0
    #define PID_DELAY_COUNT 4  /* 2 for ADC every other pulse, 4 for every pulse  */

    #define SV_PWM_TRIP_LIMIT 10

    #define SV_PWM_OUTPUT_MASK 0x0003
    #define SV_PWM_8K_SEQ1_OUTPUT_MASK 0x0001
    #define SV_PWM_8K_SEQ2_OUTPUT_MASK 0x0002


    typedef enum
    {
        SV_PWM_CURRENT = 0,
        SV_PWM_VOLTAGE = 1,
        SV_PWM_PERCENT = 2,
        SV_PWM_ON_OFF  = 3
    } SV_PWM_MODES;

    typedef struct
    {
        slong slDitherStep;
        _iq19 slDitherOutput_q19;
    } SV_DITHER_TYPE;

#endif

#ifdef EVFile

    #ifndef EVLocalReg
    #define EVLocalReg 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Definitions

    // Function prototypes
    void InitEv(void);

    // mark which tick the 2KHz PID loops should run -- helps distribute processor load
    const uword gkuwSV_PWM_Tick[NUM_OF_SV_PWMS] =
    /*  SV1 SV2 */
       {0,  1   };

    void vSV_SetMode(uword ch, SV_PWM_MODES mode);
    void vSV_PWM_8K_Seq1_Service(void);
    void vSV_PWM_8K_Seq2_Service(void);
    void vSV_Percent_Service(uword ch);
    void vSV_On_Off_Service(uword ch);
    void vSV_CurrentService_2K(uword ch);
    void vSV_VoltageService(uword ch);
    void vSV_SetPWM(uword ch, uword uwOutput);
    void vSV_Set_Enable(uword ch, boolean enable);
    void vEnable_PWM(void);
    void vDisable_PWM(void);
    void vInit_PID_Loop(uword uwChan);

    void vSV_Cmd_Percent(uword ch, uword percent_x100);
    void vSV_Cmd_On_Off(uword ch,  boolean cmd);
    void vSV_Cmd_Current(uword ch, _iq19  Icmd);
    void vSV_Cmd_Voltage(uword ch, _iq19  Vcmd);
    void vCheckForOC_Trip(uword ch);
    boolean fSV_PWM_OverCurrentTrip(uword ch);
    void vSV_PWM_OC_Reset(uword ch);

    void vLossOfPower(void);
    void vLogLackOfPowerDrop(void);
    void vResetLossOfPowerIRQ(void);

    void vOverVoltageIRQ(void);
    void vCheckOverVoltageCleared(void);

    void vSV_PWM_8K_Service(uword ch, uword uwPassCount);

    boolean gfPWM_Outputs_Started;


    // Global Variables
    SV_PWM_MODES gSV_Modes[NUM_OF_SV_PWMS];         // stored mode of operation
    boolean gfSV_On_Off_Cmds[NUM_OF_SV_PWMS];       // Boolean commands
    _iq19 gslSV_Current_Cmds_q19[NUM_OF_SV_PWMS];    // Commands for current control mode
    _iq19 gslSV_Voltage_Cmds_q19[NUM_OF_SV_PWMS];    // Commands for Voltage control mode
    uword guwSV_Percent_Cmds_x100[NUM_OF_SV_PWMS];   // Commands for percent on mode
    SV_DITHER_TYPE gSV_Dither[NUM_OF_SV_PWMS];       // Dither control structure


    // trip flags
    volatile boolean gfSV_OC_Trip[NUM_OF_SV_PWMS];
    volatile uword guwSV_8K_Seq1_Latched_OC_Fault_Mask;
    volatile uword guwSV_8K_Seq2_Latched_OC_Fault_Mask;
    volatile uword guwSV_Lockout_Bits;
    volatile uword guwSV_TripCount[NUM_OF_SV_PWMS];
    volatile boolean gfPowerFail_Trip;
    volatile boolean gfPowerOverVoltage_Trip;

    volatile Uint16 Step_8K_Seq1;    // 8K A/D Task Execution Flag
                                     //   0 - Serviced
                                     //   1 - Ready to Be Serviced
    volatile Uint16 Step_8K_Seq2;    // 8K A/D Task Execution Flag
                                     //   0 - Serviced
                                     //   1 - Ready to Be Serviced



    // Current PID loops, SV Valves
    _iq19 gslYk_q19[NUM_OF_SV_PWMS];
    _iq19 gslXk_q19[NUM_OF_SV_PWMS];
    _iq19 gslYkm1_q19[NUM_OF_SV_PWMS];
    _iq19 gslXkm1_q19[NUM_OF_SV_PWMS];

    #endif


#else
    #ifndef EV_Registration
    #define EV_Registration 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Externaly visable function prototypes
    extern void InitEv(void);
    extern void vSV_SetMode(uword ch, SV_PWM_MODES mode);
    extern void vSV_PWM_8K_Seq1_Service(void);
    extern void vSV_PWM_8K_Seq2_Service(void);

    extern void vEnable_PWM(void);
    extern void vDisable_PWM(void);

    extern void vSV_Cmd_Percent(uword ch, uword percent_x100);
    extern void vSV_Cmd_On_Off(uword ch,  boolean cmd);
    extern void vSV_Cmd_Current(uword ch, _iq19  Icmd);
    extern void vSV_Cmd_Voltage(uword ch, _iq19  Vcmd);
    extern boolean fSV_PWM_OverCurrentTrip(uword ch);
    extern void vSV_PWM_OC_Reset(uword ch);


    extern void vLossOfPower(void);
    extern void vLogLackOfPowerDrop(void);
    extern void vResetLossOfPowerIRQ(void);

    extern void vOverVoltageIRQ(void);
    extern void vCheckOverVoltageCleared(void);

    // Externally visable global variables
    extern boolean gfPWM_Outputs_Started;

    // trip flags
    extern volatile uword guwSV_8K_Seq1_Latched_OC_Fault_Mask;
    extern volatile uword guwSV_8K_Seq2_Latched_OC_Fault_Mask;
    extern volatile uword guwBrake_8K_Latched_OC_Fault_Mask;
    extern volatile uword guwSV_Lockout_Bits;

    extern volatile Uint16 Step_8K_Seq1;
    extern volatile Uint16 Step_8K_Seq2;

    extern volatile boolean gfPowerFail_Trip;
    extern volatile boolean gfPowerOverVoltage_Trip;

    #endif
#endif

