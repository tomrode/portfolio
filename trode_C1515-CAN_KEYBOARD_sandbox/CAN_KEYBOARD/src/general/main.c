/**
 *  @file                    main.c
 *  @brief                   This file contains functions related to main() which is where C code execution begins
 *  @copyright               2014-2015 Crown Equipment Corp., New Bremen, OH  45869
 *  @date                    11/19/2014
 *
 *  @remark Author:          Tom Rode
 *  @remark Project Tree:    C1515
 *
 *  @note                   N/A
 *
 */

#include "portab.h"
#include "cos_main.h"
#include "Truck_PDO.h"
#include "EvHandler.h"
#include "EventMgr.h"
#include "Watchdog.h"
#include "InputSwitches.h"
#include "ev.h"
#include "ADCData.h"
#include "spi.h"
#include "GPT1.h"       /* Interface to the first CPU timer, Timer 0 */
#include "DSP2803x_GlobalPrototypes.h"
#include "scheduler.h"
#include "datalog.h"
#include "Truck_PDO.h"
#include "Adc.h"
#include "utf_8.h"


//#define  MOD_LED_EN 1
// EXAMPLE_BIOS or EXAMPLE_NONBIOS are defined in the CCS project build options
#ifdef EXAMPLE_BIOS
    #include "example_BIOS.h"
#endif

#ifdef EXAMPLE_NONBIOS
    #include "example_nonBIOS.h"
#endif


/*========================================================================*
 *  SECTION - Local Typedefs                                              *
 *========================================================================*
 *========================================================================**/


/*========================================================================*
 *  SECTION - External variables that cannot be defined in header files   *
 *========================================================================*
 */

/*========================================================================*
 *  SECTION - Function prototypes                                         *
 *========================================================================*
 */
extern void InitGpio(void);
extern void PrepareEnvironment(void);
extern void StateMachine (void);
extern void CANopenStack (void);
extern void TimerHandler (void);
extern void CheckEventTPdos (void);
#if ENABLE_EMCYMSG == 1
extern void gCheckEmcyTransmit(); /* handle emergency messages */
#endif /* ENABLE_EMCYMSG == 1 */
extern void PrepareEnvironment(void);
extern void SPI_vInit(void);
extern vModInfo XDATA ModuleInfo; /* module information */
void v500usTask();
void v5msTask();

/*========================================================================*
 *  SECTION - Local variables                                             *
 *========================================================================*
 */

/**
 * @fn      main()
 *
 * @brief   Main function for F2803x.
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    Ported over from CIM project
 *
 * @author  Bill McCroskey, Tom Rode
 *
 */
void main(void)
																																																																																																																																																		{
     /** ###Functional overview: */

    memcpy( &RamfuncsRunStart, &RamfuncsLoadStart, &RamfuncsLoadEnd - &RamfuncsLoadStart);

    /** - Configure General purpose I/O */
    InitGpio();

    /** - Initialize Hardware CpuTimer0 */
    GPT1_vInit();

    /** - Initialize and enable the PIE (FILE: PieCtrl.c) */
    InitPieCtrl();

    /** - Set TIMER2 free=soft=1 */
    *(volatile unsigned int *)0x00000C14 |= 0x0C00;

    /** - CPU Initialization */
    /** - Initialize the CPU (FILE: SysCtrl.c) */
    InitSysCtrl();

    /** - Enable System watchdog */
    vEnableWatchdog();

    /** - Kick the watch dog */
    vServiceWatchdog();

    /** - Event handler Initialization */
    //vInitEventHandler();

    /** - Event handler Initialization */
    //vInit_EventMgr();

    /** - Get this hardware running so the ASC setup can read the baud rate */
    /** - Initializes the High-Speed Synchronous Serial Interface SCIA */
    SPI_vInit();

    /** - Loads the data structures of FRAM data */
    vInit_Fram_Data();

    /** - Kick the watch dog */
    vServiceWatchdog();

    /** -  Protocol stack function call to do necessary settings after module startup */
    PrepareEnvironment();

    /** -  Initialize the Event Manager (FILE: Ev.c) */
    //InitEv();

    /**   Initialize the ADC (FILE: Adc.c) */
    InitAdc();

    /** - Kick the watch dog */
    vServiceWatchdog();

    /** - Copy all FLASH sections that need to run from RAM (use memcpy() from RTS library) */
    vInit_Object_Dictionary();

    /** - Sets up the datalog system */
    vInit_Datalog();

    /** - GPIO Control register Unprotected */
    EALLOW;

    /** - IRQ service for the CPU timer */
    PieVectTable.TINT2 = Scheduler_Service;

    /** - enable INT 14 and INT1 */
    IER |= 0x2001;

    /** - GPIO Control register protected */
    EDIS;

    /** - Sets up the scheduler system */
    vScheduler_Init();

    /** - de_bounce task */
    vScheduler_Setup_Task(0,  0x1002, 3 , 2,   2,   1, &v500usTask);

    /** - Scan hardware keyboard task, CAN UPDATE, 20 is 5ms*/
    vScheduler_Setup_Task(11, 0x100C, 2 , 20,  7,   1, &v5msTask);

    /** - Starts all the tasks defined to be active */
    vScheduler_StartTasks();

    /** - GPIO Control register Unprotected */
    EALLOW;

    /** - Shut off Module LEd */
    MODULE_LED = FALSE;

    /** - GPIO Control register Unprotected */


    /** - Sync the timers */
    EPwm1Regs.TBCTR = 0;
    CpuTimer2Regs.TIM.half.LSW = 7500;
    CpuTimer2Regs.TIM.half.MSW = 0;
    CpuTimer1Regs.TIM.half.LSW = 1875;
    CpuTimer1Regs.TIM.half.MSW = 0;

    /** - GPIO Control register protected */
    EDIS;

    /** - Enable external HW watchdog */
    vEnableExternalWatchdog();

    /** - Initialize Keyboard scanning Manager */
    gHwKeyDecodeInit();

    /** - Null Task*/
    while (1)
    {
        /** - Kick the watch dog */
        vServiceWatchdog();

        /** - control module's state machine */
        StateMachine();

        /** - call the can handling stack */
        CANopenStack();

#if ENABLE_EMCYMSG == 1

        /* handle emergency messages */
        gCheckEmcyTransmit();

#endif /* ENABLE_EMCYMSG == 1 */

        /** - handle timed functions */
        TimerHandler();

        /** - handle event controlled transmit PDOs */
        CheckEventTPdos();

        /** - evaluate new emcy errors */
        gEvalEmcy();

        /** - Process NVM*/
        vServiceFram();

        /** - Process ADC*/
        vADC_Reality_Check();

    }

}

/**
 * @fn      vFaultLedOn(boolean fOn)
 *
 * @brief   Debug.
 *
 * @param   fOn = Trigger variable to output to GPIO39
 *
 * @return  N/A
 *
 * @note    Ported over from CIM project
 *
 * @author  Bill McCroskey, Tom Rode
 *
 */
void vFaultLedOn(boolean fOn)
{
     /** ###Functional overview: */

    if ( TRUE == fOn)
    {
        //MODULE_LED  = 1u;
    }
    else
    {
        //MODULE_LED = 0u;
    }
}

/**
 * @fn      v500usTask()
 *
 * @brief   Scheduled task.
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    Switch debouncing
 *
 * @author  Tom Rode
 *
 */
void v500usTask()
{
    /** ###Functional overview: */

    /** - De-bounce all key pad switches */
	vGetDebRowSw();

	/** - ADC Conversion*/
    vSwAdc_Keyboard_Conversion();

}

/**
 * @fn      v5msTask()
 *
 * @brief   Scheduled task.
 *
 * @param   N/A
 *
 * @return  N/A
 *
 * @note    Ported over from CIM project
 *
 * @author  Bill McCroskey, Tom Rode
 *
 */
void v5msTask()
{
    static uint16_t uwTimer = 0u;

    /** ###Functional overview: */

    /** - Scan hardware switches */
    gScanKeyDecodeManager();
    MODULE_LED = ( GERMAN_LANG == gRpdoCanLanguageSelectInput.ubData) ? TRUE : FALSE;
    /** - tick timer for CAN PDO Tx*/
    uwTimer++;

    /** - 5ms * 16 = 80 ms CAN PDO transmit */
    if ( uwTimer >= 16u )
    {
        /** - CAN stack PDO Tx*/
        gSlvForceTxPdoEvent(CAN_KYBD_TXMSG);

        /** - Reset tick timer */
        uwTimer = 0u;

#if MOD_LED_EN == 1
        /** - This is an indication of application */
        /*MODULE_LED = ~(MODULE_LED & TRUE);*/
#endif

    }

}






/*** end of file *****************************************************/
