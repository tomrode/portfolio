/***********************************************************************************************
 * File:       EvHandler.c
 *
 * Purpose:    This file contains functions that are used as the interface
 *             between the application software and the Event Manager.
 *             The various event handling layers are as follows:
 *             1. Application software - Queues events in the event handler.
 *             2. Event Handler - Queues events into the event manager,
 *                maintains guwEventHandlerMask based on all individual events,
 *                and uses event manager call-back functions to see if the Event
 *                master acknowledged the event and retries if necessary.
 *             3. Event Manager - Transmits events to Event master via CAN
 *                network, uses aknowledge to verify event master received the
 *                event, retries a configurable number of times and generates
 *                a call-back function indicating if the transmission passed or
 *                failed.
 *
 * Project:    C584
 *
 * Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 05/20/2004
 *             Revised 09/16/2004 for use on SCM
 *
 ************************************************************************************************
 */
#include "EventMgr.h"
#include "datalog.h"
#include "Events.h"
#define EVHANDLER_MODULE 1
#include "EvHandler.h"
#undef  EVHANDLER_MODULE
#include "stddef.h"

/*************************************************************************************************
 ** gfHandlEvent
 *
 *  PARAMETERS:   ubEventIndex = Event index from VCM_Events.h
 *                fSetEvent    = TRUE to set an event, FALSE to clear it.
 *
 *  DESCRIPTION:  This function is used by the application software to set or
 *                clear an event. The desired operation is added to the event
 *                handler queue and is processed by vProcessEvents(). An event
 *                will only be set if the event is currently cleared and vise
 *                versa. Therefore, this function can be called multiple time
 *                and the only time an action will be taken is when there is
 *                a change in status. If an event is pending which means it was
 *                queued but hasn't been acknowledged yet, the fault will be be
 *                queued (since it's already queued).
 *
 *  RETURNS:      TRUE if the event was successfully queued, otherwise FALSE
 *
 **************************************************************************************************
 */
boolean gfHandleEvent(ubyte ubEventIndex, boolean fSetEvent)
{
    boolean fEvProcessed = FALSE;
    uword uwEventCode;

    uwEventCode = gEvent_Table[ubEventIndex].uwEventCode;
    // Mark the fault to be flashed
    if (fSetEvent && ((uwEventCode >= MIN_NON_SRO_EVENT) && (uwEventCode <= MAX_EVENT_NUMBER)))
    {
        if (gfFaultActive == FALSE)
        {
            gfFaultActive = TRUE;
            uwFlashCode = gEvent_Table[ubEventIndex].uwEventCode;
            gubActiveFlashCodeIndex = ubEventIndex;
        }
    }
    // If index is too big, set an invalid event
    if ( ubEventIndex >= NUMBER_OF_EVENTS )
    {
        ubEventIndex = EVNT_INVALID_EVENT;
    }

   // The state machine for a particular fault must be in the idle state for
   // the fault to be processed. Furthermore the fault must be clear for the
   // fault to be set and it must be set for the fault to be cleared.
    if ( fSetEvent )
    {
      // Make sure event is clear before allowing it to be set
        if ( ubGetState(ubEventIndex) == CLR_IDLE )
        {
            vSetLeCalled(ubEventIndex, TRUE);
            fEvProcessed = TRUE;
        }
    }
    else
    {
      // Make sure event is set before allowing it to be cleared
        if ( ubGetState(ubEventIndex) == SET_IDLE )
        {
            vSetLeCalled(ubEventIndex, TRUE);
            fEvProcessed = TRUE;
        }
    }
    return(fEvProcessed);
}

/*************************************************************************************************
 ** gGetFaultStatus
 *
 *  PARAMETERS:   ubEventIndex = Event index from VCM_Events.h
 *
 *  DESCRIPTION:  This function checks the status of the fault using the state
 *                machine state.
 *
 *  RETURNS:      FLT_CLR     = Fault is clear
 *                FLT_SET     = Fault is set
 *                FLT_PENDING = Fault has been set or cleared but has not yet
 *                              been acknowledged.
 *************************************************************************************************
 */
FLT_STAT gGetFaultStatus(ubyte ubEventIndex)
{
    FLT_STAT FaultStatus = FLT_PENDING;
    ubyte ubState = ubGetState(ubEventIndex);

    switch ( ubState )
    {
        case CLR_IDLE:
            FaultStatus = FLT_CLR;
            break;

        case SET_IDLE:
            FaultStatus = FLT_SET;
            break;

        default:
            break;
    }
    return(FaultStatus);
}

/*************************************************************************************************
 ** vProcessEvents
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  This function is called by the main thread. It processes each
 *                available event by running its state machine. It also creates
 *                a the local mask that is based on the status of all events.
 *                This mask is combined with the display module mask to create
 *                the over-all control word.
 *
 *  RETURNS:      Nothing
 *
 ************************************************************************************************
 */
void vProcessEvents(void)
{
    static ubyte   ubEventIndex;            // static to not have to access through stack
    static ubyte   ubEventChkIndex;
    static uword   uwLocalMask = (uword)eDISABLE_NONE;


    // init these each pass
    uwLocalMask = (uword)eDISABLE_NONE;
    gfFaultWasActive = FALSE;

    for ( ubEventIndex = 0; ubEventIndex < NUMBER_OF_EVENTS; ubEventIndex++ )
    {
        if ( ubFaultStatus[ubEventIndex] )
        {
            uwLocalMask &= uwHandleFault(ubEventIndex);
        }
    }
    guwEventHandlerMask = uwLocalMask;

    vFlashCode();
    if (gfFaultActive)
    {
        // make sure fault is still set
        if (gGetFaultStatus(gubActiveFlashCodeIndex) == FLT_CLR)
        {
            gfFaultWasActive = TRUE;
            gfFaultActive = FALSE;
        }
    }
    if (gfFaultWasActive)
    {
        // look for another event to flash
        for (ubEventChkIndex = 0; ubEventChkIndex < NUMBER_OF_EVENTS; ubEventChkIndex++)
        {
            if (gGetFaultStatus(ubEventChkIndex) == FLT_SET)
            {
                gfFaultActive = TRUE;
                uwFlashCode = gEvent_Table[ubEventChkIndex].uwEventCode;
                gubActiveFlashCodeIndex = ubEventChkIndex;
            }
        }
    }
    vLogFaultStatus();
    guwLocalControlWord = gDM_Data.uwControlWord & guwEventHandlerMask;

}

/*************************************************************************************************
 ** uwHandleFault
 *
 *  PARAMETERS:   ubEventIndex = Event index from Events.h
 *
 *  DESCRIPTION:  This function is the event handler state machine. It uses
 *                ubFaultStatus[] to hold the state and various flags.
 *
 *  RETURNS:      The event mask based on the status of the specified event.
 *
 *************************************************************************************************
 */
uword uwHandleFault(ubyte ubEventIndex)
{
    static ubyte ubLogStatus = 0;
    static ubyte ubState;

   /*
    *    Process current state
    */
    ubLogStatus = 0;
    uwLocalMask = (uword)eDISABLE_NONE;
    ubState = ubGetState(ubEventIndex);

    if (ubState == CLR_IDLE)
    {
        // Present State

        // Next State
        if ( fGetLeCalled(ubEventIndex) )
        {
            ubState = SET_QUEUE;
            vSetState(ubEventIndex, ubState);
        }
    }
    else if (ubState == SET_QUEUE)
    {
        // Present State
        vSetLeCalled(ubEventIndex, FALSE);
        vSetCbCalled(ubEventIndex, FALSE);
        vSetCbPassed(ubEventIndex, FALSE);
        ubLogStatus = ubLogEvent(ubEventIndex,
                                 TRUE,
                                 &vEventCallback
                                );
        uwLocalMask = gEvent_Table[ubEventIndex].uwEventMask;
        // Next State
        if ( ubLogStatus == QUEUE_OK )
        {
            ubState = SET_WAIT;
            vSetState(ubEventIndex, ubState);
        }
    }
    else if (ubState == SET_WAIT)
    {
        // Present State
        uwLocalMask = gEvent_Table[ubEventIndex].uwEventMask;
        // Next State
        if ( fGetCbCalled(ubEventIndex) )
        {
            if ( fGetCbPassed(ubEventIndex) )
            {
                ubState = SET_IDLE;
                vSetState(ubEventIndex, ubState);
            }
            else
            {
                ubState = SET_QUEUE;
                vSetState(ubEventIndex, ubState);
            }
        }
    }
    else if (ubState == SET_IDLE)
    {
        // Present State

        // Next State
        if ( fGetLeCalled(ubEventIndex) )
        {
            ubState = CLR_QUEUE;
            vSetState(ubEventIndex, ubState);
        }
    }
    else if (ubState == CLR_QUEUE)
    {
        // Present State
        vSetLeCalled(ubEventIndex, FALSE);
        vSetCbCalled(ubEventIndex, FALSE);
        vSetCbPassed(ubEventIndex, FALSE);
        ubLogStatus = ubLogEvent(ubEventIndex,
                                 FALSE,
                                 &vEventCallback
                                );
        // Next State
        if ( ubLogStatus == QUEUE_OK )
        {
            ubState = CLR_WAIT;
            vSetState(ubEventIndex, ubState);
        }
    }
    else if (ubState == CLR_WAIT)
    {
        // Present State

        // Next State
        if ( fGetCbCalled(ubEventIndex) )
        {
            if ( fGetCbPassed(ubEventIndex) )
            {
                ubState = CLR_IDLE;
                vSetState(ubEventIndex, ubState);
            }
            else
            {
                ubState = CLR_QUEUE;
                vSetState(ubEventIndex, ubState);
            }
        }
    }
    else
    {
        ubState = CLR_IDLE;
        vSetState(ubEventIndex, ubState);
    }

   // Return local mask for this fault
    return(uwLocalMask);
}
/*************************************************************************************************
 ** vEventCallback
 *
 *  PARAMETERS:   uwEvent   = Event that was acknowledged
 *                fSetEvent = TRUE if callback acknowledges the setting an event
 *                fPassed   = TRUE if aknowledge was successful
 *
 *  DESCRIPTION:  This is the call-back function that is called by the event
 *                manager when an event is acknowledged by the event master
 *                or has timed out. If fPassed is TRUE the event was
 *                acknowledged, if fPassed is FALSE a time-out has occured.
 *
 *  RETURNS:      Nothing
 *
 *************************************************************************************************
 */
void vEventCallback(uword uwEvent, boolean fSetEvent, boolean fPassed)
{
    ubyte ubEventIndex = ubGetEventIndex(uwEvent);
    ubyte ubState      = ubGetState(ubEventIndex);

    if ( ((fSetEvent) && (ubState == SET_WAIT))
         || ((!fSetEvent) && (ubState == CLR_WAIT))
       )
    {
        vSetCbPassed(ubEventIndex, fPassed);   // Set first to make thread-safe
        vSetCbCalled(ubEventIndex, TRUE);
    }
}

/*************************************************************************************************
 ** ubGetEventIndex
 *
 *  PARAMETERS:   uwEvent = Event number
 *
 *  DESCRIPTION:  This function returns the corresponding event index for
 *                the specified event or EV_INVALID_EVENT if it is not in
 *                the event table.
 *
 *  RETURNS:      Event index from 0 - EV_INVALID_EVENT
 *
 *************************************************************************************************
 */
ubyte ubGetEventIndex(uword uwEvent)
{
    ubyte ubEventIndex;

   // Step through each event until the specified event is found
    for ( ubEventIndex = 0; ubEventIndex < EVNT_INVALID_EVENT; ubEventIndex++ )
    {
        if ( gEvent_Table[ubEventIndex].uwEventCode == uwEvent )
        {
            break;
        }
    }

   // return Event index
    return(ubEventIndex);
}

/*************************************************************************************************
 ** vSetState
 *
 *  PARAMETERS:   ubEventIndex = Event index from VCM_Events.h
 *                ubState      = State to make active in ubFaultStatus[]
 *
 *  DESCRIPTION:  This function is used to encode the specified state
 *                machine state into ubFaultStatus[].
 *
 *  RETURNS:      Nothing
 *
 *  NOTE:         Software processes are disabled to make threadsafe.
 *
 *************************************************************************************************
 */
void vSetState(ubyte ubEventIndex, ubyte ubState)
{
    vDisableTasks();
    ubFaultStatus[ubEventIndex] &= ~BIT_STATE;
    ubFaultStatus[ubEventIndex] |= ubState;
    vEnableTasks();
}

/*************************************************************************************************
 ** vSetLeCalled
 *
 *  FILENAME:     C:\viewstore\wc_flow1\miniturret\app\vcm\app\EvHandler.c
 *
 *  PARAMETERS:   ubEventIndex = Event index from VCM_Events.h
 *                fCalled      = TRUE or FALSE
 *
 *  DESCRIPTION:  This function is used to encode the specified state of the
 *                gfVcmLogEventlog() called variable of ubFaultStatus[].
 *
 *  RETURNS:      Nothing
 *
 *
 *************************************************************************************************
 */
void vSetLeCalled(ubyte ubEventIndex, boolean fCalled)
{
    volatile ubyte *pubStatus = &ubFaultStatus[ubEventIndex];

    if ( fCalled )
    {
        *pubStatus |= BIT_LE_CALLED;
    }
    else
    {
        *pubStatus &= ~BIT_LE_CALLED;
    }
}

/*************************************************************************************************
 ** vSetCbCalled
 *
 *  PARAMETERS:   ubEventIndex = Event index from VCM_Events.h
 *                fCalled      = TRUE or FALSE
 *
 *  DESCRIPTION:  This function is used to encode the specified state of the
 *                vEventCallback() called variable of ubFaultStatus[].
 *
 *  RETURNS:      Nothing
 *
 *
 *************************************************************************************************
 */
void vSetCbCalled(ubyte ubEventIndex, boolean fCalled)
{
    volatile ubyte *pubStatus = &ubFaultStatus[ubEventIndex];

    if ( fCalled )
    {
        *pubStatus |= BIT_CB_CALLED;
    }
    else
    {
        *pubStatus &= ~BIT_CB_CALLED;
    }
}

/*************************************************************************************************
 ** vSetCbPassed
 *
 *  PARAMETERS:   ubEventIndex = Event index from VCM_Events.h
 *                fPassed      = TRUE or FALSE
 *
 *  DESCRIPTION:  This function is used to encode the specified state of the
 *                call-back passed variable of ubFaultStatus[].
 *
 *  RETURNS:      Nothing
 *
 *
 *************************************************************************************************
 */
void vSetCbPassed(ubyte ubEventIndex, boolean fPassed)
{
    volatile ubyte *pubStatus = &ubFaultStatus[ubEventIndex];

    if ( fPassed )
    {
        *pubStatus |= BIT_CB_PASSED;
    }
    else
    {
        *pubStatus &= ~BIT_CB_PASSED;
    }
}

/*************************************************************************************************
 ** vLogFaultStatus
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  This function is used to datalog the status of all events
 *                in the event table.
 *
 *  RETURNS:      Nothing
 *
 *************************************************************************************************
 */
void vLogFaultStatus(void)
{
    static ubyte ubIndex = 0;

    if ( gubData_log_number == DL107_FAULT_STATUS )
    {
        Can1.ubData[0] = ubIndex;
        Can1.ubData[1] = ubFaultStatus[ubIndex];
        Can1.ubData[2] = (ubyte)(gEvent_Table[ubIndex].uwEventCode >> 8);
        Can1.ubData[3] = (ubyte)(gEvent_Table[ubIndex].uwEventCode     );
        Can1.ubData[4] = (ubyte)(gEvent_Table[ubIndex].uwEventMask >> 8);
        Can1.ubData[5] = (ubyte)(gEvent_Table[ubIndex].uwEventMask     );
        Can1.ubData[6] = 0;
        Can1.ubData[7] = 0;
        vSaveDatalog(&Can1, NULL);

        ubIndex++;
        if ( ubIndex >= NUMBER_OF_EVENTS )
        {
            ubIndex = 0;
        }
    }
    // Datalog if requested
    if (gubData_log_number == DL154_EV_HANDLER)
    {
        Can1.ubData[0] = ubGetState(gubData_log_option);
        Can1.ubData[1] = fGetLeCalled(gubData_log_option);
        Can1.ubData[2] = fGetCbCalled(gubData_log_option);
        Can1.ubData[3] = fGetCbPassed(gubData_log_option);
        Can1.ubData[4] = (ubyte)(uwLocalMask         >> 8);
        Can1.ubData[5] = (ubyte)(uwLocalMask             );
        Can1.ubData[6] = (ubyte)(guwEventHandlerMask >> 8);
        Can1.ubData[7] = (ubyte)(guwEventHandlerMask     );
        vSaveDatalog(&Can1, NULL);
    }
}

/************************************************************************************************
 *  Function:   vFlashCode
 *
 *  Purpose:    State machine for the LED fault code flash system
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 10/15/2004
 *
 ************************************************************************************************
*/
void vFlashCode()
{
    // find next state
    switch( eFlashState )
    {
        case Idle:
            if (gfFaultActive)
            {
                ubFlickerCount = 0;
                ubFlickerOnCount = 0;
                eFlashState = Flicker_On;
            }
            break;

        case Flicker_On:
            if (ubFlickerOnCount >= FLICKER_ON)
            {
                ubFlickerOffCount = 0;
                ubFlickerCount++;
                eFlashState = Flicker_Off;
            }
            break;

        case Flicker_Off:
            if (ubFlickerCount >= FLICKER_COUNTS)
            {
                uwInterdigitCount = 0;
                vComputeDigits();
                eFlashState = InterDigit1;
            }
            else
            {
                if (ubFlickerOffCount >= FLICKER_OFF)
                {
                    ubFlickerOnCount = 0;
                    eFlashState = Flicker_On;
                }
            }
            break;

        case InterDigit1:
            if (uwInterdigitCount >= INTER_DIGIT)
            {
                uwOnTimeCount = 0;
                eFlashState = Hund_On;
            }
            break;

        case Hund_On:
            if (uwOnTimeCount >= ON_TIME)
            {
                uwOffTimeCount = 0;
                eFlashState = Hund_Off;
            }
            break;

        case Hund_Off:
            if (uwOffTimeCount >= OFF_TIME)
            {
                swHdigit--;
                if (swHdigit > 0)
                {
                    uwOnTimeCount = 0;
                    eFlashState = Hund_On;
                }
                else
                {
                    uwInterdigitCount = 0;
                    eFlashState = InterDigit2;
                }
            }
            break;

        case InterDigit2:
            if (uwInterdigitCount >= INTER_DIGIT)
            {
                uwOnTimeCount = 0;
                eFlashState = Tens_On;
            }
            break;

        case Tens_On:
            if (uwOnTimeCount >= ON_TIME)
            {
                uwOffTimeCount = 0;
                eFlashState = Tens_Off;
            }
            break;

        case Tens_Off:
            if (uwOffTimeCount >= OFF_TIME)
            {
                swTdigit--;
                if (swTdigit > 0)
                {
                    uwOnTimeCount = 0;
                    eFlashState = Tens_On;
                }
                else
                {
                    uwInterdigitCount = 0;
                    eFlashState = InterDigit3;
                }
            }
            break;

        case InterDigit3:
            if (uwInterdigitCount >= INTER_DIGIT)
            {
                uwOnTimeCount = 0;
                eFlashState = Ones_On;
            }
            break;

        case Ones_On:
            if (uwOnTimeCount >= ON_TIME)
            {
                uwOffTimeCount = 0;
                eFlashState = Ones_Off;
            }
            break;

        case Ones_Off:
            if (uwOffTimeCount >= OFF_TIME)
            {
                swOdigit--;
                if (swOdigit > 0)
                {
                    uwOnTimeCount = 0;
                    eFlashState = Ones_On;
                }
                else
                {
                    uwInterdigitCount = 0;
                    eFlashState = InterDigit4;
                }
            }
            break;

        case InterDigit4:
            if (uwInterdigitCount >= INTER_DIGIT)
            {
                ubFlickerCount = 0;
                ubFlickerOnCount = 0;
                if (gfFaultActive)
                {
                    eFlashState = Flicker_On;
                }
                else
                {
                    eFlashState = Idle;
                }
            }
            break;

        default:
            eFlashState = Idle;
            break;

    }

    // find state output
    switch( eFlashState )
    {
        case Idle:
            LED_OFF();
            break;

        case Flicker_On:
            ubFlickerOnCount++;
            LED_ON();
            break;

        case Flicker_Off:
            ubFlickerOffCount++;
            LED_OFF();
            break;

        case InterDigit1:
        case InterDigit2:
        case InterDigit3:
        case InterDigit4:
            LED_OFF();
            uwInterdigitCount++;
            break;

        case Hund_On:
        case Tens_On:
        case Ones_On:
            uwOnTimeCount++;
            LED_ON();
            break;

        case Hund_Off:
        case Tens_Off:
        case Ones_Off:
            uwOffTimeCount++;
            LED_OFF();
            break;


        default:
            LED_OFF();
            break;

    }

}


/************************************************************************************************
 *  Function:   vComputeDigits
 *
 *  Purpose:    Computes the hundreds, tens, and ones digits of the code to flash
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 10/15/2004
 *
 ************************************************************************************************
*/
void vComputeDigits()
{
    swHdigit = (sword) uwFlashCode / 100;
    swTdigit = ((sword)uwFlashCode - (swHdigit * 100)) / 10;
    swOdigit = (sword)uwFlashCode - (swHdigit * 100) - (swTdigit * 10);
    if (swHdigit == 0)
    {
        swHdigit = 10;
    }

    if (swTdigit == 0)
    {
        swTdigit = 10;
    }

    if (swOdigit == 0)
    {
        swOdigit = 10;
    }
}

/************************************************************************************************
 *  Function:   vInitEventHandler
 *
 *  Purpose:    Initializes the memory for the event handler
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 11/30/2005
 *
 ************************************************************************************************
*/
void  vInitEventHandler(void)
{
    int i;
    for (i=0; i < NUMBER_OF_EVENTS; i++)
    {
        ubFaultStatus[i] = 0;
    }
    gfFaultActive = FALSE;
    gfFaultWasActive = FALSE;
    gubActiveFlashCodeIndex = 0;
    uwFlashCode = 0;
    eFlashState = Idle;
    gfServiceEvHandler = FALSE;
    uwLocalMask = (uword)eDISABLE_NONE;
}

