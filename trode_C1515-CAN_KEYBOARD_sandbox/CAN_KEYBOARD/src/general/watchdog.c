/**
 *  @file                    Watchdog.c
 *  @brief                   Implements the the watchdog guarding control system
 *  @copyright               Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *  @date                    12/15/2009
 *
 *  @remark Author:          Bill McCroskey
 *  @remark Project Tree:    C1515
 *
 *  @note                     N/A
 *
*/

#include "datalog.h"
#include "DSP2803x_Device.h"     /* DSP281x Headerfile Include File */
#include "datalog.h"
#include "stddef.h"
#include "ev.h"
#include "types.h"
#define WATCHDOG_MODULE 1
#include "watchdog.h"
#undef  WATCHDOG_MODULE

/*========================================================================*
 *  SECTION - Local function prototypes                                   *
 *========================================================================*
 */
void DisableDog(void);

/**
 * @fn       vWatchdogGuarding
 *
 * @brief    This function Send datalog if WatchDog event.
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Bill McCroskey
 *
 */
void vWatchdogGuarding()
{
    ulong ulTemp;

    /** ###Functional overview: */ 

    ulTemp = gulWatchdogFlags;  /* save for datalog */

    if ( gulWatchdogFlags == ALL_TASKS_RAN )
    {
        vServiceWatchdog();
        gulWatchdogFlags = 0;
    }

    if (mCheckDatalog(DL5_WATCHDOG_DATALOG))
    {
        Can1.ubData[0] = (ubyte)((ulTemp >> 24) & 0xFF);
        Can1.ubData[1] = (ubyte)((ulTemp >> 16) & 0xFF);
        Can1.ubData[2] = (ubyte)((ulTemp >> 8) & 0xFF);
        Can1.ubData[3] = (ubyte)(ulTemp & 0xFF);
        Can1.ubData[4] = 0;
        Can1.ubData[5] = 0;
        Can1.ubData[6] = 0;
        Can1.ubData[7] = 0;

        vSaveDatalog(&Can1, NULL);
    }
}


/**
 * @fn       vServiceWatchdog
 *
 * @brief    Service Watch dog
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Bill McCroskey
 *
 */
void vServiceWatchdog(void)
{
    /** ###Functional overview: */

    KickDog();
}

/**
 * @fn       vEnableWatchdog
 *
 * @brief    Watchdog Enable
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Bill McCroskey
 *
 */
void vEnableWatchdog(void)
{
    /** ###Functional overview: */

    EALLOW;
    SysCtrlRegs.SCSR = 0x01;      /* Enable /WDRST and disable /WDINT and set WDOVERRIDE */
    SysCtrlRegs.WDCR = 0x00AF;    /* start watchdog */
    SysCtrlRegs.SCSR = 0x01;      /* Enable /WDRST and disable /WDINT and clear WDOVERRIDE */
    EDIS;
}

/**
 * @fn       vEnableExternalWatchdog
 *
 * @brief    External Watchdog Enable
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Bill McCroskey
 *
 */
void vEnableExternalWatchdog(void)
{
    /** ###Functional overview: */ 

    GpioDataRegs.GPASET.bit.GPIO21  = 1;
}

/**
 * @fn       vDisableWatchdog
 *
 * @brief    Watchdog disable
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Bill McCroskey
 *
 */
void vDisableWatchdog(void)
{
    /** ###Functional overview: */ 

    DisableDog();
}

/**
 * @fn       vResetNow
 *
 * @brief    Fast reset 
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Bill McCroskey
 *
 */
void vResetNow(void)
{
    /** ###Functional overview: */ 
   
    EALLOW;
    SysCtrlRegs.WDKEY = 0x0011;   /* violate the clear sequence to cause reset */
    SysCtrlRegs.WDKEY = 0x0022;
    SysCtrlRegs.WDCR  = 0x0008;   /* this is a violation */
    EDIS;
}

/**
 * @fn       KickDog
 *
 * @brief    This function resets the watchdog timer. Enable this function for using KickDog in the application
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Bill McCroskey
 *
 */
void KickDog(void)
{
    /** ###Functional overview: */ 
 
    if( TRUE != gubWatchdogCmd )
    {
        EALLOW;
        SysCtrlRegs.WDKEY = 0x0055;
        SysCtrlRegs.WDKEY = 0x00AA;
        EDIS;
    }
}
/**
 * @fn       vShutOffSysClockOut
 *
 * @brief    Puts the DSP in standby mode and halts the external clock.
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Bill McCroskey
 *
 */
void vShutOffSysClockOut(void)
{

    /** ###Functional overview: */ 

    /** - Set up the registers for who can wake us up */
    EALLOW;
    GpioIntRegs.GPIOLPMSEL.all = 0;                  /* Disable all GPIO from generating wakeups */
    EDIS;

    /** - Get I/O shut down */
    vDisable_PWM();

    EALLOW;
    /** - Before setting PLLCR turn off missing clock detect logic */
    SysCtrlRegs.PLLSTS.bit.MCLKOFF = 1;
    EDIS;

    EALLOW;
    /** - Now, set number of clocks before waking */
    SysCtrlRegs.LPMCR0.bit.QUALSTDBY = 50;           /** - clock cycles before wakeup */
    SysCtrlRegs.LPMCR0.bit.LPM = 1;                  /** - standby state now after idle */
    asm("    IDLE");
    EDIS;
}
