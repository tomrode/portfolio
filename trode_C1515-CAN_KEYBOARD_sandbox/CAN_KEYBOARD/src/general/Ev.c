/************************************************************************************************
 *  File:       Ev.c
 *
 *  Purpose:    Event services on the EV Manager module
 *
 *  Project:    C1314
 *
 *  Copyright:  2007 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/1/2007
 *              Ported to '28235 04/25/2008
 *              Ported to '28035 11/18/2009
 ************************************************************************************************
*/

#include "DSP2803x_Device.h"
#define EVFile
#include "ev.h"
#undef  EVFile

#include "types.h"
#include "datalog.h"
#include "util.h"
#include "AdcData.h"
#include "datalog.h"
#include "Param.h"
#include "watchdog.h"
#include "Events.h"
#include "EvHandler.h"

// EXAMPLE_BIOS or EXAMPLE_NONBIOS are defined in the CCS project build options
#ifdef EXAMPLE_BIOS
    #include "example_BIOS.h"
#endif

#ifdef EXAMPLE_NONBIOS
    #include "example_nonBIOS.h"
#endif


/************************************************************************************************
 *  Function:   InitEv
 *
 *  Purpose:    Sets up the EV hardware sub system.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void InitEv(void)
{

    uword i;

    gfPowerFail_Trip = FALSE;
    gfPowerOverVoltage_Trip = FALSE;

    EALLOW;
    for (i=0; i< NUM_OF_SV_PWMS; i++)
    {
        vSV_SetMode(i, SV_PWM_PERCENT);
        guwSV_Percent_Cmds_x100[i] = 0;
        gfSV_OC_Trip[i] = FALSE;
        gfSV_On_Off_Cmds[i] = FALSE;
        gslSV_Current_Cmds_q19[i] = 0;
        gslSV_Voltage_Cmds_q19[i] = 0;
        guwSV_TripCount[i] = 0;
        vInit_PID_Loop(i);
        gSV_Dither[i].slDitherOutput_q19 = 0;
        gSV_Dither[i].slDitherStep = 0;
    }
    guwSV_Lockout_Bits = SV_PWM_OUTPUT_MASK;

    Step_8K_Seq1 = 0;               //   0 - Serviced
    Step_8K_Seq2 = 0;               //   0 - Serviced

    // **********************************************************
    // Setup ePWM1.  SV1
    // **********************************************************

    EPwm1Regs.TBPRD = CMPRMAX_8K;
    EPwm1Regs.TBPHS.half.TBPHS = 0;        // no phase shift
    EPwm1Regs.TBCTR = 0;            // Start point, not synced
    EPwm1Regs.TBCTL.all  = 0x6012;
    /*
        Register    TBCTL
        Bit(s)  Field       Value   Comment
        15:14   Free, Soft  01      Stop after full cycle
        13      PHSDIR      1       Count up after sync
        12:10   CLKDIV      000     Clock pre-scale of 1
        9:7     HSPCLKDIV   000     Divide by 1
        6       SWFSYNC     0
        5:4     SYNCOSEL    01      CTR = zero
        3       PRDLD       0       Load from Shadow
        2       PHSEN       0
        1:0     CTRMODE     10      Up-Down mode
    */

    EPwm1Regs.CMPCTL.all = 0x0005;
    /*
        Register    CMPCTL
        Bit(s)  Field       Value   Comment
        15:10   Reserved
        9       SHDWBFULL   Don't Care
        8       CLKDIV      000     Clock pre-scale of 1
        7       Reserved
        6       SHDWBMODE   0       CMPB uses shadow register
        5       Reserved
        4       SHDWAMODE   0       CMPA uses shadow register
        3-2     LOADBMODE   01      Load on period
        1-0     LOADAMODE   01      Load on period
    */

    EPwm1Regs.DBCTL.all = 0x0;
    /*
        Register    DBCTL
        Bit(s)  Field       Value   Comment
        15:6    Reserved
        5:4     IN_MODE     00      EPWMxA In is source for rising and falling delays
        3:2     POLSEL      00      Active High Complementary (B is inverted)
        1:0     OUT_MODE    00      Full dead-band control
    */

    EPwm1Regs.DBRED = 0;
    /*
        Register    DBRED
        Bit(s)  Field       Value   Comment
        15:10   Reserved
        9:0     DEL         0     Rising edge delay count for 5uS
    */

    EPwm1Regs.DBFED = 0;
    /*
        Register    DBFED
        Bit(s)  Field       Value   Comment
        15:10   Reserved
        9:0     DEL         0     Falling edge delay count for 5uS
    */

    EPwm1Regs.PCCTL.all = 0;    // PWM-Chopper is not used
    EPwm1Regs.TZSEL.all = 0x0100;    // Trip zone is used
    EPwm1Regs.TZCTL.all = 0x000E; // No action on EPWMxB, force low action on EPWMxA
    EPwm1Regs.TZEINT.all = 0x0000;     // disables One-shot IRQ generation for trips

    EPwm1Regs.ETSEL.all = 0x2A02;
    /*
        Register    ETSEL - Selection Register
        Bit(s)  Field       Value   Comment
        15      SOCBEN      0       Disable EPWMxSOCB pulse
        14:12   SOCBSEL     010     Event based on period
        11      SOCAEN      1       Enable EPWMxSOCA pulse
        10:8    SOCASEL     010     Event based on period
        7:4     Reserved
        3       INTEN       0       Disable EPWMx_INT generation
        2:0     INTSEL      010     Event based on period
    */

    EPwm1Regs.ETPS.all = 0x0100;
    /*
        Register    ETPS - Prescale Register
        Bit(s)  Field       Value   Comment
        15:14   SOCBCNT     N/A     Count of events
        13:12   SOCBPRD     00      Disable, no events counted
        11:10   SOCACNT     N/A     Count of events
        9:8     SOCAPRD     01      Generate SOCA on first event (may switch to second if CPU load is too high)
        7:4     Reserved
        3:2     INTCNT      N/A     Count of events
        1:0     INTPRD      00      Disable, no events counted
    */

    // Action qualifier for startup mode
    EPwm1Regs.AQCTLA.all = 0;   // All actions disabled
    EPwm1Regs.AQCTLB.all = 0;   // All actions disabled
    EPwm1Regs.AQSFRC.all = 0x0080;
    /*
        Register    AQSFRC - Software Force Register
        Bit(s)  Field       Value   Comment
        15:8    Reserved
        7:6     RLDCSF      10      Load on zero or period
        5       OTSFB       0
        4:3     ACTSFB      00      Action Disabled
        2       OTSFA       0
        1:0     ACTSFA      00      Action Disabled
    */
    EPwm1Regs.AQCSFRC.all = 0x0001;
    /*
        Register    AQCSFRC - Continuous Software Force Register
        Bit(s)  Field       Value   Comment
        15:4    Reserved
        3:2     CSFB        00      Forcing Disabled Port B
        1:0     CSFA        01      Continuous Low Port A
    */

    // **********************************************************
    // Setup ePWM2.  SV2
    // **********************************************************
    EPwm2Regs.TBPRD = CMPRMAX_8K;
    EPwm2Regs.TBPHS.half.TBPHS = (CMPRMAX_8K - 1);  // 180 deg. phase shift
    EPwm2Regs.TBCTR = 0;            // Start point, not synced
    EPwm2Regs.TBCTL.all = 0x6016;
    /*
        Register    TBCTL
        Bit(s)  Field       Value   Comment
        15:14   Free, Soft  01      Stop after full cycle
        13      PHSDIR      1       Count up after sync
        12:10   CLKDIV      000     Clock pre-scale of 1
        9:7     HSPCLKDIV   000     Divide by 1
        6       SWFSYNC     0
        5:4     SYNCOSEL    01      CTR = zero
        3       PRDLD       0       Load from Shadow
        2       PHSEN       1
        1:0     CTRMODE     10      Up-Down mode
    */

    EPwm2Regs.CMPCTL.all = 0x0005;
    /*
        Register    CMPCTL
        Bit(s)  Field       Value   Comment
        15:10   Reserved
        9       SHDWBFULL   Don't Care
        8       CLKDIV      000     Clock pre-scale of 1
        7       Reserved
        6       SHDWBMODE   0       CMPB uses shadow register
        5       Reserved
        4       SHDWAMODE   0       CMPA uses shadow register
        3-2     LOADBMODE   01      Load on period
        1-0     LOADAMODE   01      Load on period
    */

    EPwm2Regs.DBCTL.all = 0x0000;   // No Deadband used on this block
    EPwm2Regs.DBRED = 0;
    EPwm2Regs.DBFED = 0;

    EPwm2Regs.PCCTL.all = 0;    // PWM-Chopper is not used
    EPwm2Regs.TZSEL.all = 0x0200;    // Trip zone is used
    EPwm2Regs.TZCTL.all = 0x000E; // No action on EPWMxB, force low action on EPWMxA
    EPwm2Regs.TZEINT.all = 0x0000;     // disables one-shot IRQ generation for trips

    EPwm2Regs.ETSEL.all = 0xA202;
    /*
        Register    ETSEL - Selection Register
        Bit(s)  Field       Value   Comment
        15      SOCBEN      1       Enable EPWMxSOCB pulse
        14:12   SOCBSEL     010     Event based on period
        11      SOCAEN      0       Disable EPWMxSOCA pulse
        10:8    SOCASEL     010     Event based on period
        7:4     Reserved
        3       INTEN       0       Disable EPWMx_INT generation
        2:0     INTSEL      010     Event based on period
    */

    EPwm2Regs.ETPS.all = 0x1000;
    /*
        Register    ETPS - Prescale Register
        Bit(s)  Field       Value   Comment
        15:14   SOCBCNT     N/A     Count of events
        13:12   SOCBPRD     01      Generate SOCB on first event
        11:10   SOCACNT     N/A     Count of events
        9:8     SOCAPRD     00      Disable, no events counted
        7:4     Reserved
        3:2     INTCNT      N/A     Count of events
        1:0     INTPRD      00      Disable, no events counted
    */

    // Action qualifier for startup mode
    EPwm2Regs.AQCTLA.all = 0;   // All actions disabled
    EPwm2Regs.AQCTLB.all = 0;   // All actions disabled
    EPwm2Regs.AQSFRC.all = 0x0080;
    /*
        Register    AQSFRC - Software Force Register
        Bit(s)  Field       Value   Comment
        15:8    Reserved
        7:6     RLDCSF      10      Load on zero or period
        5       OTSFB       0
        4:3     ACTSFB      00      Action Disabled
        2       OTSFA       0
        1:0     ACTSFA      00      Action Disabled
    */
    EPwm2Regs.AQCSFRC.all = 0x0001;
    /*
        Register    AQCSFRC - Continuous Software Force Register
        Bit(s)  Field       Value   Comment
        15:4    Reserved
        3:2     CSFB        00      Forcing disabled Port B
        1:0     CSFA        01      Continuous Low Port A
    */

    EDIS;

    gfPWM_Outputs_Started = FALSE;
} // end InitEv()


/************************************************************************************************
 *  Function:   vEnable_PWM
 *
 *  Purpose:    Turn on the outputs for PWM mode.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Turns on PWM.
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vEnable_PWM(void)
{
    // **********************************************************
    // Setup ePWM1.  SV1
    // **********************************************************
    // Action qualifier for run mode
    EPwm1Regs.AQCTLA.all = 0x0060;
    /*
        Register    AQCTLA
        Bit(s)  Field       Value   Comment
        15-12   Reserved
        11-10   CBD         00      Action Disabled
        9-8     CBU         00      Action Disabled
        7-6     CAD         01      Clear: force EPWMxA output low
        5-4     CAU         10      Set: force EPWMxA output high
        3-2     PRD         00      Action Disabled
        1-0     ZRO         00      Action Disabled
    */
    EPwm1Regs.AQCTLB.all = 0;   // All actions disabled
    EPwm1Regs.AQSFRC.all = 0x0080;
    /*
        Register    AQSFRC - Software Force Register
        Bit(s)  Field       Value   Comment
        15:8    Reserved
        7:6     RLDCSF      10      Load on zero or period
        5       OTSFB       0
        4:3     ACTSFB      00      Action Disabled
        2       OTSFA       0
        1:0     ACTSFA      00      Action Disabled
    */
    EPwm1Regs.AQCSFRC.all = 0x0000;
    /*
        Register    AQCSFRC - Continuous Software Force Register
        Bit(s)  Field       Value   Comment
        15:4    Reserved
        3:2     CSFB        00      Forcing Diabled Port B
        1:0     CSFA        00      Forcing Diabled Port A
    */

    // **********************************************************
    // Setup ePWM2.  SV2
    // **********************************************************
    // Action qualifier for run mode
    EPwm2Regs.AQCTLA.all = 0x0060;
    /*
        Register    AQCTLA
        Bit(s)  Field       Value   Comment
        15-12   Reserved
        11-10   CBD         00      Action Disabled
        9-8     CBU         00      Action Disabled
        7-6     CAD         01      Clear: force EPWMxA output low
        5-4     CAU         10      Set: force EPWMxA output high
        3-2     PRD         00      Action Disabled
        1-0     ZRO         00      Action Disabled
    */
    EPwm2Regs.AQCTLB.all = 0x0000;
    /*
        Register    AQCTLB
        Bit(s)  Field       Value   Comment
        15:12   Reserved
        11-10   CBD         00      Clear: force EPWMxB output low
        9-8     CBU         00      Set: force EPWMxB output high
        7-6     CAD         00      Action Disabled
        5-4     CAU         00      Action Disabled
        3-2     PRD         00      Action Disabled
        1-0     ZRO         00      Action Disabled
    */
    EPwm2Regs.AQSFRC.all = 0x0080;
    /*
        Register    AQSFRC - Software Force Register
        Bit(s)  Field       Value   Comment
        15:8    Reserved
        7:6     RLDCSF      10      Load on zero or period
        5       OTSFB       0
        4:3     ACTSFB      00      Action Disabled
        2       OTSFA       0
        1:0     ACTSFA      00      Action Disabled
    */
    EPwm2Regs.AQCSFRC.all = 0x0000;
    /*
        Register    AQCSFRC - Continuous Software Force Register
        Bit(s)  Field       Value   Comment
        15:4    Reserved
        3:2     CSFB        00      Forcing Diabled Port B
        1:0     CSFA        00      Forcing Diabled Port A
    */

    gfPWM_Outputs_Started = TRUE;
}

/************************************************************************************************
 *  Function:   vDisable_SV_PWM
 *
 *  Purpose:    Switch off all the outputs.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Turns off PWM.
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vDisable_PWM(void)
{
    // **********************************************************
    // Setup ePWM1.  SV1
    // **********************************************************
    // Action qualifier for off mode
    EPwm1Regs.AQCTLA.all = 0;   // All actions disabled
    EPwm1Regs.AQCTLB.all = 0;   // All actions disabled
    EPwm1Regs.AQSFRC.all = 0x0080;
    /*
        Register    AQSFRC - Software Force Register
        Bit(s)  Field       Value   Comment
        15:8    Reserved
        7:6     RLDCSF      10      Load on zero or period
        5       OTSFB       0
        4:3     ACTSFB      00      Action Disabled
        2       OTSFA       0
        1:0     ACTSFA      00      Action Disabled
    */
    EPwm1Regs.AQCSFRC.all = 0x0001;
    /*
        Register    AQCSFRC - Continuous Software Force Register
        Bit(s)  Field       Value   Comment
        15:4    Reserved
        3:2     CSFB        00      Continuous Low Port B
        1:0     CSFA        01      Continuous Low Port A
    */

    // **********************************************************
    // Setup ePWM2.  SV2
    // **********************************************************
    // Action qualifier for off mode
    EPwm2Regs.AQCTLA.all = 0;   // All actions disabled
    EPwm2Regs.AQCTLB.all = 0;   // All actions disabled
    EPwm2Regs.AQSFRC.all = 0x0080;
    /*
        Register    AQSFRC - Software Force Register
        Bit(s)  Field       Value   Comment
        15:8    Reserved
        7:6     RLDCSF      10      Load on zero or period
        5       OTSFB       0
        4:3     ACTSFB      00      Action Disabled
        2       OTSFA       0
        1:0     ACTSFA      00      Action Disabled
    */
    EPwm2Regs.AQCSFRC.all = 0x0001;
    /*
        Register    AQCSFRC - Continuous Software Force Register
        Bit(s)  Field       Value   Comment
        15:4    Reserved
        3:2     CSFB        00      Continuous Low Port B
        1:0     CSFA        01      Continuous Low Port A
    */

    gfPWM_Outputs_Started = FALSE;
}

/************************************************************************************************
 *  Function:   vSV_SetMode
 *
 *  Purpose:    Sets up operation mode of a channel in the SV driver group.
 *
 *  Input:      ch -- uword -- The output channel to configure
 *              mode -- SV_PWM_MODES -- The mode of operation to set
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vSV_SetMode(uword ch, SV_PWM_MODES mode)
{
    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));
    gSV_Modes[ch] = mode;
}

/************************************************************************************************
 *  Function:   vCheckForOC_Trip
 *
 *  Purpose:    Checks the latched fault status words for faults on a given PWM channel.
 *
 *  Input:      ch -- uword -- the channel to check.
 *
 *  Output:     Status set in the gfSV_OC_Trip array
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vCheckForOC_Trip(uword ch)
{
    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    gfSV_OC_Trip[ch] = FALSE;
    switch(ch)
    {
        case 0:
            if(EPwm1Regs.TZFLG.bit.OST)
            {
                gfSV_OC_Trip[ch] = TRUE;
            }
            break;
        case 1:
            if(EPwm2Regs.TZFLG.bit.OST)
            {
                gfSV_OC_Trip[ch] = TRUE;
            }
            break;
        default:
            break;
    }
}

/************************************************************************************************
 *  Function:   vSV_PWM_8K_Service
 *
 *  Purpose:    Handles the PWM service on a given SV PWM outputs on the 8kHz driver group
 *
 *  Input:      Channel number, pass count
 *
 *  Output:     Output drive on the SV channels
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 11/18/2009
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vSV_PWM_8K_Service, "ramfuncs");
void vSV_PWM_8K_Service(uword ch, uword uwPassCount)
{
    if ((guwSV_Lockout_Bits >> ch) & 1)         // high is enabled
    {
        vCheckForOC_Trip(ch);
        if (gfSV_OC_Trip[ch])
        {
            guwSV_TripCount[ch]++;
            if (guwSV_TripCount[ch] >= SV_PWM_TRIP_LIMIT)
            {
                guwSV_Lockout_Bits &= ~((uword)1 << ch); // set low to lock
                vSV_Set_Enable(ch, FALSE);
            }
            else
            {
                vSV_Set_Enable(ch, TRUE);
            }
        }  // end of if (gfSV_OC_Trip[ch])
        else
        {
            vSV_Set_Enable(ch, TRUE);
            switch(gSV_Modes[ch])
            {
                case SV_PWM_CURRENT:
                    if (uwPassCount == gkuwSV_PWM_Tick[ch])
                    {
                        vSV_CurrentService_2K(ch); // only call at 2kHz rate or gains get too small
                    }
                    break;

                case SV_PWM_VOLTAGE:
                    vSV_VoltageService(ch);
                    break;

                case SV_PWM_PERCENT:
                    vSV_Percent_Service(ch);
                    break;

                case SV_PWM_ON_OFF:
                    vSV_On_Off_Service(ch);
                    break;

                default:
                    break;
            }
        }  // end of else for if (gfSV_OC_Trip[ch])
    } // end of if locked out
    else
    {
        vSV_Set_Enable(ch, FALSE);
    }
}


/************************************************************************************************
 *  Function:   vSV_PWM_8K_Seq1_Service
 *
 *  Purpose:    Handles the PWM service on SV PWM outputs on the 8kHz driver group
 *
 *  Input:      N/A
 *
 *  Output:     Output drive on the SV channels
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vSV_PWM_8K_Seq1_Service, "ramfuncs");
void vSV_PWM_8K_Seq1_Service(void)
{
    static uword suwPassCount = 0;

    vSV_PWM_8K_Service(0, suwPassCount);

    suwPassCount++;
    if (suwPassCount >= PID_DELAY_COUNT)
    {
        suwPassCount = 0;
    }
}

/************************************************************************************************
 *  Function:   vSV_PWM_8K_Seq2_Service
 *
 *  Purpose:    Handles the PWM service on SV PWM outputs on the 2kHz driver group
 *
 *  Input:      N/A
 *
 *  Output:     Output drive on the SV channels
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vSV_PWM_8K_Seq2_Service, "ramfuncs");
void vSV_PWM_8K_Seq2_Service(void)
{
    static uword suwPassCount = 0;

    vSV_PWM_8K_Service(1, suwPassCount);

    suwPassCount++;
    if (suwPassCount >= PID_DELAY_COUNT)
    {
        suwPassCount = 0;
    }
}

/************************************************************************************************
 *  Function:   vSV_SetPWM
 *
 *  Purpose:    Routes a requested output to the channel's PWM hardware
 *
 *  Input:      ch -- the output channel
 *              uwOutput -- the comparator value to load to the EV hardware
 *
 *  Output:     Output drive on the SV channels
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vSV_SetPWM, "ramfuncs");
void vSV_SetPWM(uword ch, uword uwOutput)
{
    switch(ch)
    {
        case 0:     // SV1
            EPwm1Regs.CMPA.half.CMPA = uwOutput;
            break;

        case 1:     // SV2
            EPwm2Regs.CMPA.half.CMPA = uwOutput;
            break;

        default:
            break;
    }
}

/************************************************************************************************
 *  Function:   vSV_Percent_Service
 *
 *  Purpose:    Controls the percent duty cycle for a channel that is set for percent output mode.
 *
 *  Input:      ch -- the output channel
 *
 *  Output:     Output drive on the SV channels
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vSV_Percent_Service, "ramfuncs");
void vSV_Percent_Service(uword ch)
{
    slong slTemp;
    uword uwOutput;

    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    slTemp = (slong)MAXPWMLIMIT_8K *
             (slong)guwSV_Percent_Cmds_x100[ch];
    slTemp /= (slong)10000;
    uwOutput = (uword)((slong)MAXPWMLIMIT_8K - slTemp);
    vSV_SetPWM(ch, uwOutput);
}

/************************************************************************************************
 *  Function:   vSV_On_Off_Service
 *
 *  Purpose:    Controls the On/Off duty cycle for a channel that is set for On/Off output mode.
 *
 *  Input:      ch -- the output channel
 *
 *  Output:     Output drive on the SV channels
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vSV_On_Off_Service(uword ch)
{
    uword uwOutput;

    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    if (gfSV_On_Off_Cmds[ch])
    {
        uwOutput = 0;
    }
    else
    {
        uwOutput = (uword)MAXPWMLIMIT_8K +1;
    }
    vSV_SetPWM(ch, uwOutput);
}

/************************************************************************************************
 *  Function:   vSV_CurrentService_2K
 *
 *  Purpose:    Controls the current output a channel that is set for current output mode.
 *
 *  Input:      ch -- the output channel
 *
 *  Output:     Output drive on the SV channels
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/29/2007
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vSV_CurrentService_2K, "ramfuncs");
void vSV_CurrentService_2K(uword ch)
{
   // const _iq19 T = _IQ19(0.0005);      // 2000 Hz rate
    static uword suwDL_Count = 0;
    _iq19 slCommand_q19;
    //_iq19 slLowLimit_q19;
   // _iq19 slHiLimit_q19;
    _iq19 slCurrent_FB_q19;
    //_iq19 slTemp1_q19;
    //_iq19 slTemp2_q19;
    //_iq19 slTemp3_q19;
    //_iq19 slTemp4_q19;
    _iq19 slTemp5_q19;
    _iq17 slTempOut_q17;
    uword uwOutput;

    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));
    //slLowLimit_q19 = parm_SV_PWM_Lo_Command_Clip_q19(ch);
    //slHiLimit_q19 = parm_SV_PWM_Hi_Command_Clip_q19(ch);
    slCommand_q19 = gslSV_Current_Cmds_q19[ch];
   // if (gslSV_Current_Cmds_q19[ch] < slLowLimit_q19)
    //{
        //slCommand_q19 = 0;
    //}
    //if (gslSV_Current_Cmds_q19[ch] > slHiLimit_q19)
    //{
        //slCommand_q19 = slHiLimit_q19;
    //}
    // Check for dither control -- Pre PID loop
    //if (parm_SV_PWM_DitherEnable(ch))
    //{
        //if (gSV_Dither[ch].slDitherStep < (parm_SV_PWM_DitherRate(ch) >> 1))
        //{
            //gSV_Dither[ch].slDitherOutput_q19 = parm_SV_PWM_DitherLevel_q19(ch);
        //}
        //else
        //{
            //gSV_Dither[ch].slDitherOutput_q19 = -parm_SV_PWM_DitherLevel_q19(ch);
        //}
        //slCommand_q19 += gSV_Dither[ch].slDitherOutput_q19;
        //if (slCommand_q19 < parm_SV_PWM_MinDitherCurrent_q19(ch))
        //{
            //slCommand_q19 = parm_SV_PWM_MinDitherCurrent_q19(ch);
        //}
        //gSV_Dither[ch].slDitherStep++;
        //if (gSV_Dither[ch].slDitherStep >= parm_SV_PWM_DitherRate(ch))
        //{
            //gSV_Dither[ch].slDitherStep = 0;
        //}
    //}

    switch (ch)
    {
        case 0:
        default:
            slCurrent_FB_q19 = gADC_Scaled.slSV1_I_mA_q19;
            break;

        case 1:
            slCurrent_FB_q19 = gADC_Scaled.slSV2_I_mA_q19;
            break;
    }
    if (slCurrent_FB_q19 < 0)
    {
        slCurrent_FB_q19 = 0;
    }
    gslXk_q19[ch] = slCommand_q19 - slCurrent_FB_q19;                    // error term
    gslXk_q19[ch] = slClip(_IQ19(-4000),  gslXk_q19[ch], _IQ19(4000));

    //slTemp1_q19 = _IQ19mpy(parm_SV_PWM_Current_Ki_q19(ch), gslXk_q19[ch]);      // Ki * X(k)
   // slTemp2_q19 = _IQ19mpy(slTemp1_q19, T);                                     // Ki * X(k) * T
    //slTemp3_q19 = _IQ19mpy(parm_SV_PWM_Current_Kp_q19(ch),  gslXk_q19[ch]);     // Kp * X(k)
    //slTemp4_q19 = _IQ19mpy(parm_SV_PWM_Current_Kp_q19(ch),  gslXkm1_q19[ch]);   // Kp * X(k-1)
   // slTemp5_q19 = gslYkm1_q19[ch] + slTemp2_q19 + slTemp3_q19 - slTemp4_q19;

   // slTemp5_q19 = slClip(_IQ19(0), slTemp5_q19,  _IQ19(1));

    if (slCommand_q19 == 0)
    {
        slTemp5_q19 = 0;        // clamp output if command is zero
    }
    gslYk_q19[ch] = slTemp5_q19;

    // scale to output
    slTempOut_q17 = (slong)MAXPWMLIMIT_8K << 17;
    slTempOut_q17 = _IQ17mpy(slTempOut_q17, (gslYk_q19[ch] >> 2));    // do this in q17
    slTempOut_q17 = slTempOut_q17 >> 17;
    uwOutput = (uword)((slong)MAXPWMLIMIT_8K - slTempOut_q17);
    uwOutput = uwClip(0, uwOutput, MAXPWMLIMIT_8K);
    vSV_SetPWM(ch, uwOutput);
    gslYkm1_q19[ch] = gslYk_q19[ch];
    gslXkm1_q19[ch] = gslXk_q19[ch];

    if (mCheckDatalog(DL26_SV_CURRENT_LOG) && mCheckDatalogOption(ch))
    {
        if ( suwDL_Count == 0 )
        {
            Can1.ubData[0] = (ubyte)((slCurrent_FB_q19 >> 24) & 0xFF); // q3 format
            Can1.ubData[1] = (ubyte)((slCurrent_FB_q19 >> 16) & 0xFF);
            Can1.ubData[2] = (ubyte)((slCommand_q19 >> 24) & 0xFF);  // q3 format
            Can1.ubData[3] = (ubyte)((slCommand_q19 >> 16) & 0xFF);
            Can1.ubData[4] = (ubyte)((gslXk_q19[ch] >> 24) & 0xFF);    // q3 format
            Can1.ubData[5] = (ubyte)((gslXk_q19[ch] >> 16) & 0xFF);
            Can1.ubData[6] = 0;
            Can1.ubData[7] = 0;

           //Can2.ubData[0] = (ubyte)((slTemp1_q19 >> 24) & 0xFF);      // q19 format
           //Can2.ubData[1] = (ubyte)((slTemp1_q19 >> 16) & 0xFF);
            //Can2.ubData[2] = (ubyte)((slTemp1_q19 >> 8) & 0xFF);
            //Can2.ubData[3] = (ubyte)(slTemp1_q19 & 0xFF);
            //Can2.ubData[4] = (ubyte)((slTemp2_q19 >> 24) & 0xFF);      // q19 format
           //Can2.ubData[5] = (ubyte)((slTemp2_q19 >> 16) & 0xFF);
            //Can2.ubData[6] = (ubyte)((slTemp2_q19 >> 8) & 0xFF);
            //Can2.ubData[7] = (ubyte)(slTemp2_q19 & 0xFF);

           //Can3.ubData[0] = (ubyte)((slTemp3_q19 >> 24) & 0xFF);      // q19 format
           //Can3.ubData[1] = (ubyte)((slTemp3_q19 >> 16) & 0xFF);
           //Can3.ubData[2] = (ubyte)((slTemp3_q19 >> 8) & 0xFF);
           //Can3.ubData[3] = (ubyte)(slTemp3_q19 & 0xFF);
           //Can3.ubData[4] = (ubyte)((slTemp4_q19 >> 24) & 0xFF);      // q19 format
           //Can3.ubData[5] = (ubyte)((slTemp4_q19 >> 16) & 0xFF);
            //Can3.ubData[6] = (ubyte)((slTemp4_q19 >> 8) & 0xFF);
            //Can3.ubData[7] = (ubyte)(slTemp4_q19 & 0xFF);

            Can4.ubData[0] = (ubyte)((slTemp5_q19 >> 24) & 0xFF);      // q19 format
            Can4.ubData[1] = (ubyte)((slTemp5_q19 >> 16) & 0xFF);
            Can4.ubData[2] = (ubyte)((slTemp5_q19 >> 8) & 0xFF);
            Can4.ubData[3] = (ubyte)(slTemp5_q19 & 0xFF);
            Can4.ubData[4] = (ubyte)(0);
            Can4.ubData[5] = (ubyte)((uwOutput >> 8) & 0xFF);
            Can4.ubData[6] = (ubyte)(uwOutput & 0xFF);
            Can4.ubData[7] = ch;

            vSaveDatalogExtended(&Can1, &Can2, &Can3, &Can4);
        }
        suwDL_Count++;
        if (suwDL_Count >= 3)
        {
            suwDL_Count = 0;
        }
    }
}

/************************************************************************************************
 *  Function:   vSV_VoltageService
 *
 *  Purpose:    Controls the voltage output a channel that is set for voltage output mode.
 *
 *  Input:      ch -- the output channel
 *
 *  Output:     Output drive on the SV channels
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/29/2007
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vSV_VoltageService, "ramfuncs");
void vSV_VoltageService(uword ch)
{
    _iq19 slTemp_q19;
    _iq19 slTempOut;
    _iq17 slTempOut_q17;
    uword uwOutput;

    // Voltage Control
    if (gADC_Scaled.slBattSense_Volts_q19 != 0)  // check for divide by zero
    {
        slTemp_q19 = _IQ19div((gslSV_Voltage_Cmds_q19[ch] + _IQ19(0.6) /* diode drop */),
                               gADC_Scaled.slBattSense_Volts_q19);
    }
    else
    {
        slTemp_q19 = 0;
    }
    if (slTemp_q19 >= _IQ19(1))
    {
        slTemp_q19 = _IQ19(1);    // go full on
    }
    if (gslSV_Voltage_Cmds_q19[ch] == 0)
    {
        slTemp_q19 = 0;
    }

    slTempOut_q17 = (slong)MAXPWMLIMIT_8K << 17;
    slTempOut_q17 = _IQ17mpy(slTempOut_q17, (slTemp_q19 >> 2));
    slTempOut = slTempOut_q17 >> 17;
    uwOutput = (uword)((slong)MAXPWMLIMIT_8K - slTempOut);
    uwOutput = uwClip(0, uwOutput, MAXPWMLIMIT_8K);
    vSV_SetPWM(ch, uwOutput);
}

/************************************************************************************************
 *  Function:   vSV_Set_Enable
 *
 *  Purpose:    Controls the output enable of a channel.
 *
 *  Input:      ch -- the output channel
 *              enable -- boolean -- TRUE for enable, FALSE for disable
 *
 *  Output:     Output driver enable control
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vSV_Set_Enable, "ramfuncs");
void vSV_Set_Enable(uword ch, boolean enable)
{
    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    /* If enabled, the ePWM module controls the pin */
    /* If disabled, the pin becomes an Ouput set low */
    switch(ch)
    {
        case 0:     // SV1
            EALLOW;
            if (enable)
            {
                GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 1;             // EPWM1A -- SV1
                EPwm1Regs.TZCLR.bit.OST = 1;
            }
            else
            {
                GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 0;             // GPIO -- SV1
                GpioCtrlRegs.GPADIR.bit.GPIO0 = 1;              // Output
                GpioDataRegs.GPACLEAR.bit.GPIO0 = 1;
            }
            EDIS;
            break;

        case 1:     // SV2
            EALLOW;
            if (enable)
            {
                GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 1;             // EPWM2A -- SV2
                EPwm2Regs.TZCLR.bit.OST = 1;
            }
            else
            {
                GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 0;             // GPIO -- SV2
                GpioCtrlRegs.GPADIR.bit.GPIO2 = 1;              // Output
                GpioDataRegs.GPACLEAR.bit.GPIO2 = 1;
            }
            EDIS;
            break;


        default:
            break;
    }
}

/************************************************************************************************
 *  Function:   vSV_Cmd_Percent
 *
 *  Purpose:    Sets the PWM percent command for a given SV channel.
 *
 *  Input:      ch -- the output channel
 *              percent_x100 -- uword -- Percent * 100 i.e. 10000 is 100.00 percent
 *
 *  Output:     Loads a command for the driver.
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vSV_Cmd_Percent(uword ch, uword percent_x100)
{
    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    guwSV_Percent_Cmds_x100[ch] = uwClip(0, percent_x100, 10000);
}

/************************************************************************************************
 *  Function:   vSV_Cmd_On_Off
 *
 *  Purpose:    Sets the PWM On/Off command for a given SV channel.
 *
 *  Input:      ch -- the output channel
 *              cmd -- boolean -- TRUE for on, FALSE for off
 *
 *  Output:     Loads a command for the driver.
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vSV_Cmd_On_Off(uword ch,  boolean cmd)
{
    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    gfSV_On_Off_Cmds[ch] = cmd;
}

/************************************************************************************************
 *  Function:   vSV_Cmd_Current
 *
 *  Purpose:    Sets the PWM driver current command for a given SV channel.
 *
 *  Input:      ch -- the output channel
 *              Icmd -- IQ19 -- mA current command
 *
 *  Output:     Loads a command for the driver.
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vSV_Cmd_Current(uword ch, _iq19 Icmd)
{
    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    gslSV_Current_Cmds_q19[ch] = Icmd;
}

/************************************************************************************************
 *  Function:   vSV_Cmd_Voltage
 *
 *  Purpose:    Sets the PWM driver voltage command for a given SV channel.
 *
 *  Input:      ch -- the output channel
 *              Vcmd -- IQ19 -- Voltage output command
 *
 *  Output:     Loads a command for the driver.
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vSV_Cmd_Voltage(uword ch, _iq19  Vcmd)
{
    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    gslSV_Voltage_Cmds_q19[ch] = Vcmd;
}

/************************************************************************************************
 *  Function:   fSV_PWM_OverCurrentTrip
 *
 *  Purpose:    Checks for an over current trip on a channel.
 *
 *  Input:      ch -- the output channel
 *
 *  Output:     N/A
 *
 *  Return:     TRUE if the channel has tripped, else FALSE
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
boolean fSV_PWM_OverCurrentTrip(uword ch)
{
    boolean fResult = FALSE;
    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    if (guwSV_TripCount[ch] >= SV_PWM_TRIP_LIMIT)
    {
        fResult = TRUE;
    }
    return fResult;
}

/************************************************************************************************
 *  Function:   vSV_PWM_OC_Reset
 *
 *  Purpose:    Resets a channel that has tripped on an overcurrent fault
 *
 *  Input:      ch -- the output channel
 *
 *  Output:     Clears the trip status
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vSV_PWM_OC_Reset(uword ch)
{
    ch = uwClip(0, ch, (NUM_OF_SV_PWMS - 1));

    gfSV_OC_Trip[ch] = FALSE;
    guwSV_Lockout_Bits |= ((uword)1 << ch);
    guwSV_TripCount[ch] = 0;

    vSV_Set_Enable(ch, TRUE);
}
/************************************************************************************************
 *  Function:   vInit_PID_Loop
 *
 *  Purpose:    Clears past output and loop variables for SV PID loops
 *
 *  Input:      Channel to be zeroed
 *
 *  Output:     Past data is cleared
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 10/26/2006
 *
 ************************************************************************************************
*/
void vInit_PID_Loop(uword uwChan)
{
    gslYk_q19[uwChan] = 0;
    gslXk_q19[uwChan] = 0;
    gslYkm1_q19[uwChan] = 0;
    gslXkm1_q19[uwChan] = 0;
}

/************************************************************************************************
 *  Function:   vTickleExternalWatchdog
 *
 *  Purpose:    Toggles watchdog tickle pin
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     David Timmerman
 *
 *  Revision History:
 *
 ************************************************************************************************
*/
void vTickleExternalWatchdog()
{
    if(gubWatchdogCmd != 2)
    {
        GpioDataRegs.GPATOGGLE.bit.GPIO22  = 1;
    }
}

/************************************************************************************************
 *  Function:   vLossOfPower
 *
 *  Purpose:    Interrupt service routine for loss of module power
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Sets a flag so an event will be generated in 0.5 seconds to check if the module
 *              did not power down
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 09/11/2007
 *
 ************************************************************************************************
*/
void vLossOfPower(void)
{
//    PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
    gfPowerFail_Trip = TRUE;
    gfPowerDownLock = TRUE;     // lock the FRAM
    vDisable_PWM();

//    PieCtrlRegs.PIEIER12.bit.INTx1 = 0;  // Disable XINT1 in group 1
}

/************************************************************************************************
 *  Function:   vLogLackOfPowerDrop
 *
 *  Purpose:    Task service for job to act on board did not power down
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Logs the event
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 09/11/2007
 *
 ************************************************************************************************
*/
void vLogLackOfPowerDrop(void)
{
    gfHandleEvent(EVNT_NO_POWER_DOWN_AFTER_INTERRUPT, TRUE);
}

/************************************************************************************************
 *  Function:   vResetLossOfPowerIRQ
 *
 *  Purpose:    Clears the interrupt flags and arms for another low power IRQ
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Enables IRQ, clears flags
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 09/11/2007
 *
 ************************************************************************************************
*/
void vResetLossOfPowerIRQ(void)
{
//    PieCtrlRegs.PIEIER12.bit.INTx1 = 1;  // Enable XINT1 in group 1
    gfPowerDownLock = FALSE;        // Release lock on FRAM
    gfPowerFail_Trip = FALSE;

}


/************************************************************************************************
 *  Function:   vOverVoltageIRQ
 *
 *  Purpose:    Interrupt service routine for Over Voltage condition
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Posts an event
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 09/11/2007
 *
 ************************************************************************************************
*/
void vOverVoltageIRQ(void)
{
    volatile FLT_STAT eResult;
    volatile int i;
    volatile boolean fSeenHigh = FALSE;

//    PieCtrlRegs.PIEACK.all = PIEACK_GROUP12;
    // make sure line stays low


    if (!fSeenHigh)       // it stayed low
    {
        gfPowerOverVoltage_Trip = TRUE;
        vDisable_PWM();

        PieCtrlRegs.PIEIER12.bit.INTx2 = 0;         // Interrupt is disabled
        eResult = gGetFaultStatus(EVNT_OVER_VOLTAGE);
        if (eResult == FLT_CLR)
        {
            gfHandleEvent(EVNT_OVER_VOLTAGE, TRUE);
        }
    }
}

/************************************************************************************************
 *  Function:   vCheckOverVoltageCleared
 *
 *  Purpose:    Checks if the over voltage condition has cleared
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Clears an event
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 09/11/2007
 *
 ************************************************************************************************
*/
void vCheckOverVoltageCleared(void)
{
    volatile FLT_STAT eResult;
    volatile int i;
    volatile boolean fSeenLow = FALSE;

    // make sure line stays high



    if (!fSeenLow)       // it stayed high
    {
        gfPowerOverVoltage_Trip = FALSE;
        eResult = gGetFaultStatus(EVNT_OVER_VOLTAGE);
        if (eResult == FLT_SET)
        {
            gfHandleEvent(EVNT_OVER_VOLTAGE, FALSE);
        }
//        PieCtrlRegs.PIEIER12.bit.INTx2 = 1;         // Interrupt is enabled
    }
}




/*** end of file *****************************************************/

