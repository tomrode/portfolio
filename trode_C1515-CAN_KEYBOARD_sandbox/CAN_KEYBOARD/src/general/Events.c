/************************************************************************************************
 *  File:       SCM_Events.c
 *
 *  Purpose:    Implements the constants of Event Reporting system
 *
 *  Project:    C584
 *
 *  Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/13/2004
 *
 ************************************************************************************************
*/

#define EventNumberFile
#include "Events.h"
#undef EventNumberFile

// Dummy file to contain variables for this module
