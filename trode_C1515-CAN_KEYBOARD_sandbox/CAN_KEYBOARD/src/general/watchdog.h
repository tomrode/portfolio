/************************************************************************************************
 *  File:       Watchdog.h
 *
 *  Purpose:    Defines the watchdog guarding control system interface
 *
 *  Project:    C1103
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 12/22/2005
 *
 ************************************************************************************************
*/

#include "types.h"

#ifndef watchdog_registration

    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/
    #define HSWD_ADC_MOTOR_TIMEOUT  3               /* max timeout value for high speed watchdog */


    #define MAIN_RAN_FLAG           0x00000001
    #define EVENT_TASK_RAN_FLAG     0x00000002
    #define EVHANDLE_TASK_RAN_FLAG  0x00000004
    #define WC_RAN_FLAG             0x00000008
    #define WC_OUTPUT_RAN_FLAG      0x00000010
    #define PDO_CHECK_RAN_FLAG      0x00000020

    #define ALL_TASKS_RAN           0x0000001F

    // Macros
    #define mWatchdogRecordTask(Value) (gulWatchdogFlags |= Value)



#endif

#ifdef WATCHDOG_MODULE

    #ifndef watchdog_registration_local
    #define watchdog_registration_local 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Defines


    //Funciton prototypes
    void vWatchdogGuarding(void);
    void vServiceWatchdog(void);
    void vEnableWatchdog(void);
    void vEnableExternalWatchdog(void);
    void vDisableWatchdog(void);
    void vResetNow(void);
    void vSwitchExternalClearSource(void);
    void vClearExternalClearSource(void);
    void vServiceExternalWatchdog(void);
    void KickDog(void);
    void vShutOffSysClockOut(void);
    // Global variables
    volatile ulong   gulWatchdogFlags = 0;

    // Macros



    #endif


#else
    #ifndef watchdog_registration
    #define watchdog_registration 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/

    // Externaly visable global variables
    extern volatile  ulong   gulWatchdogFlags;

    // Externaly visable function prototypes
    extern void vWatchdogGuarding(void);
    extern void vServiceWatchdog(void);
    extern void vEnableWatchdog(void);
    extern void vEnableExternalWatchdog(void);
    extern void vDisableWatchdog(void);
    extern void vResetNow(void);
    extern void vShutOffSysClockOut(void);
    
    #endif
#endif
