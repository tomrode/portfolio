/************************************************************************************************
 * File:       EvHandler.h
 *
 * Purpose:    Defines the event handler stuff that interfaces with the event
 *             manager.
 *
 * Project:    C584
 *
 * Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Bill McCroskey
 *
 * Revision History:
 *             Written 09/16/2004
 *
 ************************************************************************************************
*/

#include "Events.h"

#ifndef  evhandler_registration

   /*******************************************************************************
    *                                                                             *
    *  G l o b a l   S c o p e   S e c t i o n                                    *
    *                                                                             *
    *******************************************************************************/

   // Defines
   #define FLICKER_COUNTS 10

   // Ticks at 5mS
   #define FLICKER_ON      10
   #define FLICKER_OFF     10
   #define INTER_DIGIT     200
   #define ON_TIME         20
   #define OFF_TIME        80

   extern void vFaultLedOn(boolean);

   #define LED_OFF() (vFaultLedOn(FALSE))
   #define LED_ON()  (vFaultLedOn(TRUE))

   typedef enum
   {
      FLT_CLR     = 0,
      FLT_SET     = 1,
      FLT_PENDING = 2
   }FLT_STAT;

   typedef struct
   {
      ubyte ubEventIndex;
      boolean fSetEvent;
      boolean fPassed;
      boolean fProcess;
   }CALLBACK_INFO;

   typedef enum
   {
       Idle = 0,
       Flicker_On = 1,
       Flicker_Off = 2,
       InterDigit1 = 3,
       Hund_On = 4,
       Hund_Off = 5,
       InterDigit2 = 6,
       Tens_On = 7,
       Tens_Off = 8,
       InterDigit3 = 9,
       Ones_On = 10,
       Ones_Off = 11,
       InterDigit4 = 12
   } FLASH_STATE_TYPE;

#endif

#ifdef EVHANDLER_MODULE

   #ifndef evhandler_registration_local
   #define evhandler_registration_local 1

   /*******************************************************************************
    *                                                                             *
    *  I n t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Defines
   typedef enum
   {
      CLR_IDLE  = 0,
      SET_QUEUE = 1,
      SET_WAIT  = 2,
      SET_IDLE  = 3,
      CLR_QUEUE = 4,
      CLR_WAIT  = 5
   } EVH_STATE_TYPE;


   // Bits in ubFaultStatus[]
   #define BIT_STATE      0x0F
   #define BIT_LE_CALLED  0x10
   #define BIT_CB_CALLED  0x20
   #define BIT_CB_PASSED  0x40

   #define ubGetState(Index)   (ubFaultStatus[##Index]  & BIT_STATE)
   #define fGetLeCalled(Index) ((ubFaultStatus[##Index] & BIT_LE_CALLED) != 0)
   #define fGetCbCalled(Index) ((ubFaultStatus[##Index] & BIT_CB_CALLED) != 0)
   #define fGetCbPassed(Index) ((ubFaultStatus[##Index] & BIT_CB_PASSED) != 0)

   // Function prototypes
   void  vLogFaultStatus(void);
   ubyte ubGetEventIndex(uword uwEvent);
   uword uwHandleFault(ubyte ubEventIndex);
   void  vEventCallback(uword uwEvent, boolean fSetEvent, boolean fPassed);
   void  vSetState(ubyte ubEventIndex, ubyte ubState);
   void  vSetLeCalled(ubyte ubEventIndex, boolean fCalled);
   void  vSetCbCalled(ubyte ubEventIndex, boolean fCalled);
   void  vSetCbPassed(ubyte ubEventIndex, boolean fPassed);
   boolean  gfHandleEvent(ubyte ubEventIndex, boolean fSetEvent);
   void  vFlashCode();
   void  vComputeDigits();
   void  vInitEventHandler(void);

   // Global variables
   volatile ubyte ubFaultStatus[NUMBER_OF_EVENTS];
   volatile uword guwEventHandlerMask = (uword)eDISABLE_NONE;
   volatile boolean  gfFaultActive;
   volatile boolean  gfFaultWasActive;
   volatile ubyte    gubActiveFlashCodeIndex;     // index of code being flashed
   uword    uwFlashCode;
   sword    swHdigit;
   sword    swTdigit;
   sword    swOdigit;
   FLASH_STATE_TYPE eFlashState;
   ubyte    ubFlickerCount;
   ubyte    ubFlickerOnCount;
   ubyte    ubFlickerOffCount;
   uword    uwInterdigitCount;
   uword    uwOnTimeCount;
   uword    uwOffTimeCount;
   uword    uwLocalMask;
   volatile boolean gfServiceEvHandler;

   // Macros

   #endif


#else
   #ifndef evhandler_registration
   #define evhandler_registration 1
   /*******************************************************************************
    *                                                                             *
    *  E x t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Externaly visable global variables
   extern volatile uword guwEventHandlerMask;
   extern volatile boolean  gfFaultActive;
   extern volatile boolean  gfServiceEvHandler;

   // Externally visable function prototypes
   extern void  vFlashCode();
   extern void     vProcessEvents(void);
   extern FLT_STAT gGetFaultStatus(ubyte ubEventIndex);
   extern boolean  gfHandleEvent(ubyte ubEventIndex, boolean fSetEvent);
   extern void  vInitEventHandler(void);

   #endif
#endif

