/************************************************************************************************
 *  File:       Events.h
 *
 *  Purpose:    Defines the constants of Event Reporting system
 *
 *  Project:    C1103
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 11/30/2005
 *
 ************************************************************************************************
*/

#include "Truck_PDO.h"

#ifndef Event_Num_Registration
    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/

    typedef struct
    {
        uword uwEventCode;
        uword uwEventMask;
    } EVENT_TABLE_TYPE;

    // constants

    #define EVNT_NONE                                    0
    #define EVNT_LOST_DISPLAY_PDO                        1
    #define EVNT_LOST_COMMAND_PDO                        2
    #define EVNT_FRAM_Failure                            3
    #define EVNT_FRAM_CRC_Error_Event_History            4
    #define EVNT_HOUR_METERS_CORRUPT                     5
    #define EVNT_LOST_CALIBRATION                        6
    #define EVNT_NO_POWER_DOWN_AFTER_INTERRUPT           7
    #define EVNT_OVER_VOLTAGE                            8
    #define EVNT_INVALID_EVENT                          98  /* keep this last of the events */
    #define EVNT_INVALID_ADVISE_SRO_EVENT               99  /* keep this last in the list */


    #define NUMBER_OF_EVENTS (EVNT_INVALID_EVENT + 1)
    #define EVENT_LIST_SIZE  (EVNT_INVALID_ADVISE_SRO_EVENT + 1)

#endif

#ifdef EventNumberFile

    #ifndef EventNumLocalReg
    #define EventNumLocalReg 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Definitions

    // Funciton prototypes
    const EVENT_TABLE_TYPE gEvent_Table[EVENT_LIST_SIZE] =
    {
       /*EVNT_NONE                                    0 */ {  0, (uword)eDISABLE_NONE},
       /*EVNT_LOST_DISPLAY_PDO                        1 */ {570, (uword)eDISABLE_NONE},
       /*EVNT_LOST_COMMAND_PDO                        2 */ {571, (uword)eDISABLE_NONE},
       /*EVNT_FRAM_Failure                            3 */ {572, (uword)eDISABLE_NONE},
       /*EVNT_FRAM_CRC_Error_Event_History            4 */ {573, (uword)eDISABLE_NONE},
       /*EVNT_HOUR_METERS_CORRUPT                     5 */ {574, (uword)eDISABLE_NONE},
       /*EVNT_LOST_CALIBRATION                        6 */ {575, (uword)eDISABLE_NONE},
       /*EVNT_NO_POWER_DOWN_AFTER_INTERRUPT           7 */ {576, (uword)eDISABLE_ALL_LOCAL},
       /*EVNT_OVER_VOLTAGE                            8 */ {577, (uword)eDISABLE_ALL_LOCAL},
       /*EVNT_INVALID_EVENT                          98 */ {578, (uword)eDISABLE_ALL_LOCAL},  /* keep this last of the events */
       /*EVNT_INVALID_ADVISE_SRO_EVENT               99 */ {579, (uword)eDISABLE_NONE}
    };

    // Note:  Second module does a Plus 20 on these event codes.

    // Global Variables

    #endif


#else
    #ifndef Event_Num_Registration
    #define Event_Num_Registration 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Externaly visable function prototypes

    // Externally visable global variables
    extern const EVENT_TABLE_TYPE gEvent_Table[];

    #endif
#endif

