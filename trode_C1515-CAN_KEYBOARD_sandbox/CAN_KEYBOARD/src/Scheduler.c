/************************************************************************************************
 *  File:       Scheduler.c
 *
 *  Purpose:    Implements the data structures and variables used in the simple task scheduler for
 *              the TI F2800 series DSP
 *
 *  Project:    C1103
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
#include "types.h"
#define SchedulerFile 1
#include "Scheduler.h"
#undef SchedulerFile
#include "string.h"
#include "DSP2803x_CpuTimers.h"
#include "datalog.h"
#include "DSP2803x_Device.h"
#include "watchdog.h"
#include "Ev.h"

#define PAD_E3  GpioDataRegs.GPADAT.bit.GPIO28
extern void vTickleExternalWatchdog();

/************************************************************************************************
 *  Function:   vScheduler_Init
 *
 *  Purpose:    Sets up the scheduler system.
 *              CPU timer 2 (normally reserved for DSP/BIOS) is used as the IRQ event.
 *              The IRQ is set to interrupt the processor every 250uS for task service.
 *              The CAN, Event manager, and A/D interrupts all preempt this IRQ.
 *
 *              This module fills the gap between the hard real-time tasks of the event
 *              manager and the idle loop that runs the CANopen stack.
 *
 *              Jobs scheduled by this scheduler have a resolution of 250uS.  Up to two
 *              jobs can be run on a tick if time permits.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
void vScheduler_Init(void)
{
    CpuTimer2Regs.TCR.all = 0x8810; // stop the timer

    CpuTimer2Regs.PRD.half.LSW = 0x3A97; // set reload value to 250uS
    CpuTimer2Regs.PRD.half.MSW = 0x0;

    CpuTimer2Regs.TPR.all = 0;
    CpuTimer2Regs.TPRH.all = 0;
    CpuTimer2Regs.TCR.all = 0x4820; // start the timer
    memset(&Tasks, 0, sizeof(TASKS_STUCT));

}

/************************************************************************************************
 *  Function:   vStart_Task
 *
 *  Purpose:    Starts the specified task
 *
 *  Input:      uword i -- index number of the task.
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
void vStart_Task(uword i)
{
    if (Tasks.Jobs[i].swTickCount == 0)
    {
        Tasks.Jobs[i].swTickCount = Tasks.Jobs[i].swTimePeriod;
    }
    Tasks.Jobs[i].eState = SCHEDULER_WAIT;
}

/************************************************************************************************
 *  Function:   vScheduler_StartTasks
 *
 *  Purpose:    Starts all the tasks defined to be active
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
void vScheduler_StartTasks(void)
{
    uword i;

    for (i=0; i<Tasks.uwNum; i++)
    {
        if (Tasks.Jobs[i].fTaskActive)
        {
            vStart_Task(i);
        }
    }
    StopCpuTimer2();
    ReloadCpuTimer2();
    StartCpuTimer2();
}

/************************************************************************************************
 *  Function:   vScheduler_StopTasks
 *
 *  Purpose:    Stops all the tasks defined to be active
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
void vScheduler_StopTasks(void)
{
}

/************************************************************************************************
 *  Function:   vScheduler_Setup_Task
 *
 *  Purpose:    Defines a task for the task manager
 *
 *  Input:      uword uwPosition -- Position in the job list, 0 to MAX_SCHEDULED_JOBS
 *              uword uwID -- The task ID of the job
 *              uword uwPriorityLevel -- Service priority, 0 to MAX_PRIORITY_LEVELS.  0 = top priority
 *              sword swTimePeriod -- The period of service in ticks of 250uS/tick
 *              sword swTickCount -- The starting point or "preload" for time before first run.  A value of
 *                                   zero will cause the task to be marked to run on the first pass
 *                                   of the scheduler.
 *
 *              boolean fTaskActive -- Must be true for the task to be run.  False creates the task but does
 *                                     not schedule it.
 *              void *ServiceRoutine -- Pointer to the service function for the task
 *
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
void vScheduler_Setup_Task(uword uwPosition,
                           uword uwID,
                           uword uwPriorityLevel,
                           sword swTimePeriod,
                           sword swTickCount,
                           boolean fTaskActive,
                           void (*ServiceRoutine) ())
{
    if (uwPosition < MAX_SCHEDULED_JOBS)
    {
        if (Tasks.Jobs[uwPosition].uwID == 0)
        {
            // this is a new job
            Tasks.uwNum++;
        }
        Tasks.Jobs[uwPosition].uwID = uwID;
        Tasks.Jobs[uwPosition].eState = SCHEDULER_IDLE;
        Tasks.Jobs[uwPosition].uwPriorityLevel = uwPriorityLevel;
        Tasks.Jobs[uwPosition].uwActivePriorityLevel = uwPriorityLevel;
        Tasks.Jobs[uwPosition].swTimePeriod = swTimePeriod;
        Tasks.Jobs[uwPosition].swTickCount = swTickCount;
        Tasks.Jobs[uwPosition].fTaskActive = fTaskActive;
        Tasks.Jobs[uwPosition].ServiceRoutine = ServiceRoutine;
        Tasks.Jobs[uwPosition].ulLastRunTime = 0;
        Tasks.Jobs[uwPosition].ulAverageRunTime = 0;
    }
}

/************************************************************************************************
 *  Function:   vScheduler_Activate_Task
 *
 *  Purpose:    Activates an already defined task in the task manager
 *
 *  Input:      uword uwPosition -- Position in the job list, 0 to MAX_SCHEDULED_JOBS
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
void vScheduler_Activate_Task(uword uwPosition)
{
    if (uwPosition < MAX_SCHEDULED_JOBS)
    {
        if (Tasks.Jobs[uwPosition].uwID != 0)
        {
            Tasks.Jobs[uwPosition].fTaskActive = TRUE;
        }
    }
    vStart_Task(uwPosition);
}

/************************************************************************************************
 *  Function:   vScheduler_Activate_Task
 *
 *  Purpose:    Deactivates an already defined task in the task manager
 *
 *  Input:      uword uwPosition -- Position in the job list, 0 to MAX_SCHEDULED_JOBS
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
void vScheduler_Deactivate_Task(uword uwPosition)
{
    if (uwPosition < MAX_SCHEDULED_JOBS)
    {
        if (Tasks.Jobs[uwPosition].uwID != 0)
        {
            Tasks.Jobs[uwPosition].fTaskActive = FALSE;
        }
    }
}

/************************************************************************************************
 *  Function:   vScheduler_Delete_Task
 *
 *  Purpose:    Destroys an already defined task in the task manager
 *
 *  Input:      uword uwPosition -- Position in the job list, 0 to MAX_SCHEDULED_JOBS
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
void vScheduler_Delete_Task(uword uwPosition)
{
    if (uwPosition < MAX_SCHEDULED_JOBS)
    {
        if (Tasks.Jobs[uwPosition].uwID != 0)
        {
            Tasks.Jobs[uwPosition].fTaskActive = FALSE;
            Tasks.Jobs[uwPosition].uwID = 0;
            Tasks.uwNum--;
            if (Tasks.uwNum > MAX_SCHEDULED_JOBS)
            {
                Tasks.uwNum = 0;    // fix going below zero
            }
        }
    }
}

/************************************************************************************************
 *  Function:   vMarkTick
 *
 *  Purpose:    Marks a time tick on all the active tasks.
 *              Any task whose tick count reaches zero will be marked pending.
 *              Any already pending jobs will get a priority boost.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vMarkTick, "ramfuncs");
void vMarkTick()
{
    uword i;

    for (i=0; i<MAX_SCHEDULED_JOBS; i++)
    {
        if (Tasks.Jobs[i].fTaskActive)
        {
            if (Tasks.Jobs[i].eState == SCHEDULER_PENDING)
            {
                Tasks.Jobs[i].swTickCount--;    // this is ticks of delay past scheduled time
                // perform priority boost on delays
                Tasks.Jobs[i].uwActivePriorityLevel--;
                // check for underflow in the unsigned values by seeing if they get too large
                if (Tasks.Jobs[i].uwActivePriorityLevel > MAX_PRIORITY_LEVELS)
                {
                    Tasks.Jobs[i].uwActivePriorityLevel = 0;
                }
            }
            else
            {
                if ((Tasks.Jobs[i].eState == SCHEDULER_WAIT) ||
                    (Tasks.Jobs[i].fTaskActive))
                {
                    Tasks.Jobs[i].swTickCount--;
                    if (Tasks.Jobs[i].swTickCount <= 0)
                    {
                        Tasks.Jobs[i].swTickCount = 0;
                        Tasks.Jobs[i].eState = SCHEDULER_PENDING;
                    }
                }
            }
        }
    }
}

/************************************************************************************************
 *  Function:   swFindJob
 *
 *  Purpose:    Searches for the highest active priority job.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 *  Notes:
 *              There is a slight bias to the end of the list.  If two jobs are both pending and
 *              the same priority level, the job in the higher numbered slot will execute first.
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(swFindJob, "ramfuncs");
sword swFindJob()
{
    sword swJob = -1;   // mark no job found
    uword uwPriorityLevel = MAX_PRIORITY_LEVELS;
    uword i;

    for (i=0; i<MAX_SCHEDULED_JOBS; i++)
    {
        if (Tasks.Jobs[i].fTaskActive)
        {
            if (Tasks.Jobs[i].eState == SCHEDULER_PENDING)
            {
                if (Tasks.Jobs[i].uwActivePriorityLevel <= uwPriorityLevel)
                {
                    swJob = (sword)i;   // mark this location
                    uwPriorityLevel = Tasks.Jobs[i].uwActivePriorityLevel;
                }
            }
        }
    }
    return swJob;
}

/************************************************************************************************
 *  Function:   vRecycleJob
 *
 *  Purpose:    Reschedules a job.
 *
 *  Input:      uword uwPosition -- Position in the job list, 0 to MAX_SCHEDULED_JOBS
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vRecycleJob, "ramfuncs");
void vRecycleJob(uword uwPosition)
{
    Tasks.Jobs[uwPosition].eState = SCHEDULER_WAIT;
    Tasks.Jobs[uwPosition].swTickCount = Tasks.Jobs[uwPosition].swTimePeriod;
    Tasks.Jobs[uwPosition].uwActivePriorityLevel = Tasks.Jobs[uwPosition].uwPriorityLevel;
}

/************************************************************************************************
 *  Function:   vRunJob
 *
 *  Purpose:    Executes a job.
 *              The last and average run times of the function are tracked for use in determining
 *              what jobs can be run at the end of a time tick.
 *
 *  Input:      uword uwJob -- Position in the job list, 0 to MAX_SCHEDULED_JOBS
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vRunJob, "ramfuncs");
void vRunJob(uword uwJob)
{
    ulong ulStartTime;
    ulong ulEndTime;
    void (*fnPtr)();

    ulStartTime = ((ulong)CpuTimer2Regs.TIM.half.MSW * 65536L) + (ulong)CpuTimer2Regs.TIM.half.LSW;
    Tasks.Jobs[uwJob].eState = SCHEDULER_RUNNING;
    fnPtr = Tasks.Jobs[uwJob].ServiceRoutine;
    if (fnPtr != NULL)
    {
        fnPtr();
        vRecycleJob(uwJob);
    }
    ulEndTime = ((ulong)CpuTimer2Regs.TIM.half.MSW * 65536L) + (ulong)CpuTimer2Regs.TIM.half.LSW;
    Tasks.Jobs[uwJob].ulLastRunTime = ulStartTime - ulEndTime;
    if (Tasks.Jobs[uwJob].ulAverageRunTime == 0)
    {
        Tasks.Jobs[uwJob].ulAverageRunTime =  Tasks.Jobs[uwJob].ulLastRunTime;
    }
    else
    {
        Tasks.Jobs[uwJob].ulAverageRunTime = ((Tasks.Jobs[uwJob].ulAverageRunTime * 3) +
                                               Tasks.Jobs[uwJob].ulLastRunTime) >> 2;
    }
}

/************************************************************************************************
 *  Function:   Scheduler_Service
 *
 *  Purpose:    IRQ service for the CPU timer.  Invokes the job service.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(Scheduler_Service, "ramfuncs");
interrupt void Scheduler_Service(void)
{
    sword swJob;
    ulong ulTimeNow;
    uword i;


    vTickleExternalWatchdog();         // call the service for checking ADC IRQ's
   // PAD_E3 = 0;    // E3 (Scheduler Utilization)
    asm(" CLRC INTM");               // Enable global interrupts
    vMarkTick();
    if (mCheckBufferedDatalog(DL1_SCHEDULER_LOG_1))
    {
        // log the first 32 task states
        for (i=0; i<8; i++)
        {
            Can1.ubData[i] = Tasks.Jobs[i].eState & 0xFF;
            Can2.ubData[i] = Tasks.Jobs[i+8].eState & 0xFF;
            Can3.ubData[i] = Tasks.Jobs[i+16].eState & 0xFF;
            Can4.ubData[i] = Tasks.Jobs[i+24].eState & 0xFF;
        }
        vSaveDatalogExtended(&Can1, &Can2, &Can3, &Can4);
    }
#if MAX_SCHEDULED_JOBS > 32
    if (mCheckDatalog(DL2_SCHEDULER_LOG_2))
    {
        // log the second 32 task states
        for (i=0; i<8; i++)
        {
            Can1.ubData[i] = Tasks.Jobs[i+32].eState & 0xFF;
            Can2.ubData[i] = Tasks.Jobs[i+40].eState & 0xFF;
            Can3.ubData[i] = Tasks.Jobs[i+48].eState & 0xFF;
            Can4.ubData[i] = Tasks.Jobs[i+56].eState & 0xFF;
        }
        vSaveDatalogExtended(&Can1, &Can2, &Can3, &Can4);
    }
#endif
    swJob = swFindJob();
    if (swJob >= 0)
    {
        vRunJob((uword)swJob);

        ulTimeNow = ((ulong)CpuTimer2Regs.TIM.half.MSW * 65536L) + (ulong)CpuTimer2Regs.TIM.half.LSW;
        if (ulTimeNow > (ulong)MIN_IDLE_TIME)
        {
            swJob = swFindJob();
            if (swJob >= 0)
            {
                if (Tasks.Jobs[swJob].ulAverageRunTime != 0)
                {
                    if ((ulTimeNow - Tasks.Jobs[swJob].ulAverageRunTime) > (ulong)MIN_IDLE_TIME)
                    {
                        vRunJob((uword)swJob);
                    }
                }
            }
        }
    }
    //PAD_E3 = 1;    // E3 (Scheduler Utilization)
}




