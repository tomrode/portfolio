/**
 *  @file               InputSwitches.c
 *  @brief              Input Switch interface layer 
 *  @copyright          2014-2015 Crown Equipment Corp., New Bremen, OH  45869
 *  @date               11/19/2014
 *
 *  @remark Author:     Tom Rode
 *  @remark Project:    C1515
 *
 */

#include "DSP2803x_Device.h"
#include "utf_8.h"
#include "types.h"
#include "Truck_PDO.h"
#include "param.h"
#include "IO_interface.h"
#include "InputSwitches.h"
/*========================================================================*
 *  SECTION - Local variables                                             *
 *========================================================================*
 */
/** @brief This type is used for an indication for a switch in a given row for Stuck or shorted scenario 
 */
static MultiRow_Sw_Presses_t MultiRow_Sw_PressesRw[CNT6];

/** @brief This Bitfeld array is used for an indication for a switch position open or closed within a given row upto 6 rows 
 */
BitFieldRwSw_t  BitFieldRwSw[CNT6];

/*========================================================================*
 *  SECTION - Local functions                                             *
 *========================================================================*
 */



/**
 * @fn       uwMultiSwitchPress(uint16_t)
 *
 * @brief    This function returns a valid Switch position if there is a faulted switch within the row  
 *
 * @param    uwMultipleSwitches       = Amount of switches in a given row
 * @param    ubRow                    = Specific HW Row
 *
 * @return   returns an unstuck value
 *
 * @note     This will be called every 5 ms
 *
 * @author   Tom Rode
 *
 */
uint16_t uwMultiSwitchPress(uint16_t uwMultipleSwitches, uint8_t ubRow)
{

    uint16_t uwUpdateSwPrs;
    uint16_t uwRowColStatThrsh;   

    /** ###Functional overview: */
    
    /** - Update row state */
    uwRowColStatThrsh = mGET_ROW(gUwRowColStat);

     /** - Fault detect only up to what parameter max value */
    if ( uwRowColStatThrsh <= gParams.Kt.ubRows )      
    {  
        /** - Place current state into current row */   
        MultiRow_Sw_PressesRw[ubRow].uwNewSwVal = uwMultipleSwitches;
  
        /** - Nothing Shorted */
        if( FALSE == MultiRow_Sw_PressesRw[ubRow].ubSwShorted ) 
        {
            /** - Check for switch change */
            if( MultiRow_Sw_PressesRw[ubRow].uwOldSwVal != MultiRow_Sw_PressesRw[ubRow].uwNewSwVal )
            {
                /** - new value return working switch - Old value latched */
                uwUpdateSwPrs = (MultiRow_Sw_PressesRw[ubRow].uwNewSwVal - MultiRow_Sw_PressesRw[ubRow].uwOldSwVal);

                /** - Reset Null Counter */
                MultiRow_Sw_PressesRw[ubRow].ubNullCnt = CNT0;
            }
            /** - Has fault matured */
            else if( MultiRow_Sw_PressesRw[ubRow].ubNullCnt > gParams.Kt.uwFaultMatureTime ) 
            {
                /** - Which switch or switches shorted */
                MultiRow_Sw_PressesRw[ubRow].ubSwShorted = uwMultipleSwitches;
       
                /** - Output */
                uwUpdateSwPrs = CNT0;

                /** - Reset Null Counter TRIAL */
                MultiRow_Sw_PressesRw[ubRow].ubNullCnt = CNT0;
            }
            else
            {
                /** - Increment the null counter for stuck switches */
                MultiRow_Sw_PressesRw[ubRow].ubNullCnt++;

                /** - Output */
                uwUpdateSwPrs = CNT0;
            }

        /** - Update the old value for next time around */
        MultiRow_Sw_PressesRw[ubRow].uwOldSwVal = MultiRow_Sw_PressesRw[ubRow].uwNewSwVal;

        }
        /** - There is a shorted value */
        else if ( MultiRow_Sw_PressesRw[ubRow].ubSwShorted > 0u)
        {
             /** - Negate the shorted switches from a current read */
             MultiRow_Sw_PressesRw[ubRow].uwNewSwValSht = ( MultiRow_Sw_PressesRw[ubRow].uwNewSwVal - MultiRow_Sw_PressesRw[ubRow].ubSwShorted);

            /** - Check for switch change */
            if( MultiRow_Sw_PressesRw[ubRow].uwOldSwValSht != MultiRow_Sw_PressesRw[ubRow].uwNewSwValSht )
            {
                /** - Output new value return working switch - Old value latched */
                uwUpdateSwPrs = ( MultiRow_Sw_PressesRw[ubRow].uwNewSwValSht - MultiRow_Sw_PressesRw[ubRow].uwOldSwValSht );

                /** - Reset Null Counter */
                MultiRow_Sw_PressesRw[ubRow].ubNullCnt = CNT0;
            }
            /** - Has fault matured */
            else if( MultiRow_Sw_PressesRw[ubRow].ubNullCnt > gParams.Kt.uwFaultMatureTime ) 
            {
                /** - Which switch or switches shorted */
                MultiRow_Sw_PressesRw[ubRow].ubSwShorted = uwMultipleSwitches;

                /** - Output */
                uwUpdateSwPrs = CNT0;

                /** - Reset Null Counter TRIAL */
                MultiRow_Sw_PressesRw[ubRow].ubNullCnt = CNT0;
            }
            else
            {
                /** - Increment the null counter for stuck switches */
                MultiRow_Sw_PressesRw[ubRow].ubNullCnt++;

                /** - Output */
                uwUpdateSwPrs = CNT0;
            }

            /** - Negate the shorted switches from a good read */
            MultiRow_Sw_PressesRw[ubRow].uwOldSwValSht = ( MultiRow_Sw_PressesRw[ubRow].uwNewSwVal - MultiRow_Sw_PressesRw[ubRow].ubSwShorted) ;
        }
        else
        {
            /* Do nothing */
        }
    }

    /** - Updated Switch status */
    return uwUpdateSwPrs;
}


/**
 * @fn       ubGetRowSW(void)
 *
 * @brief    This function returns Switch position 
 *
 * @param    uwColumnval              =  row bit position to be converted 
 *
 * @return   Switch position corresponding to the column 
 *
 * @note     26 Jan 2015 Placed a FOR loop and cycled through column switches. 
 *
 * @author   Tom Rode
 *
 */
uint8_t ubGetRowSW(uint32_t uwColumnval)
{
    uint8_t ubShiftIndex;
    uint8_t ubRetVal;
    uint8_t ubExitVal;
 
    /* - ###Functional overview: */


    /** - Get the column parameter */  
    ubExitVal = gParams.Kt.ubColumns;

    /** - Cycle through until detected column switch then exit loop */
    for( ubShiftIndex = COL1_RCV; ubShiftIndex <= ubExitVal; ubShiftIndex++ )
    {
         /** - No switches detected at this time */
         if( CNT0 == uwColumnval )
         {
             /** - There is no Column switch hit */
             ubRetVal = CNT0;
             
             /** - Exit loop */
             ubExitVal = CNT0;
         }
         /** - Run through mask to determine column key hit */
         else if( COL1_RCV == uwColumnval )
         {
             /** - Return index which correlates to column key hit stop for loop*/
             ubRetVal = ubShiftIndex;
  
             /** - Make to exit loop */
             ubExitVal = CNT0;
         } 
         /** - Right shift one until value and mask equal */
         else
         {
             uwColumnval = uwColumnval >> COL1_RCV;     
         }
    }
     
    /** - Update switch position */
    return ubRetVal;
}

/**
 * @fn       GetDebRowSw(void)
 *
 * @brief    This function returns Debounced Switch state
 *
 * @param    None
 *
 * @return   N/A
 *
 * @note     Debounced Switch state after five consecutive 1-ms reads
 *
 * @author   Tom Rode
 *
 */
void vGetDebRowSw(void)
{
    uint8_t ubCurRowSt;   
    static uint8_t ubOldRowSt;
     
    /** ###Functional overview: */

    /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
    if( TRUE == gParams.Kt.ubDSP28035 )
    {    
        /** - Read in masked GPIO Register value */  
        gKey_Switches.ubHistory[gKey_Switches.ubPointer] = vReadKeySwitched28035DSP();
    } 

    /** - Current Row from status */
    ubCurRowSt = mGET_ROW(gUwRowColStat);   
    
    /** - Capture five same state readings */
    if ((gKey_Switches.ubHistory[CNT0] == gKey_Switches.ubHistory[CNT1]) &&
        (gKey_Switches.ubHistory[CNT1] == gKey_Switches.ubHistory[CNT2]) &&
        (gKey_Switches.ubHistory[CNT2] == gKey_Switches.ubHistory[CNT3]) &&
        (gKey_Switches.ubHistory[CNT3] == gKey_Switches.ubHistory[CNT4]) )
        
    {   
        /** - Five of the same state readings place into respective byte in switches array, index is correct now*/       
        gKey_Switches.uwSwitches[ubOldRowSt] = gKey_Switches.ubHistory[CNT0]; 

        /** - Update Old row status */
        ubOldRowSt = ubCurRowSt;   
    }
    /** - increment the count */
    gKey_Switches.ubPointer++;

    /** - Five of the same read state for state latching purposes*/
    if (gKey_Switches.ubPointer > CNT4)    
    {
        /** - reset the count */          
        gKey_Switches.ubPointer = CNT0;
    }
}

/**
 * @fn       ubKeyDepressed(uint16_t)
 *
 * @brief    This function checks for a rising edge and outputs if parameter set or output on a falling edge if parameter set.   
 *
 * @param    ubCheckTransition            = Logic level of a given switch
 * @param    Row                          = Specific HW Row

 * @return   returns a row when depressed
 *
 * @note     This will be called every 5 ms
 *
 * @author  Tom Rode
 *
 */
uint8_t ubKeyDepressed(uint8_t ubCheckTransition, uint8_t ubRow)
{
    uint8_t ubKeySel;

    /** ###Functional overview: */
    
    /** - Keys parameter switches read on a press */
    if( TRUE == gParams.Kt.ubCharOnPressed) 
    {     
        /** - High edge check SWITCH 1 less than row parameter */         
        if( KEYSW1 == ubCheckTransition ) 
        {  
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw1 = TRUE;
    
            /** - Output on Rising edge  */
            ubKeySel = KEYSW1;            
        }

        /** - Check if SWITCH 1 in a particular Row pressed and see if de-bounced masked value low and transition less than row parameter */                 
        if( ( TRUE == BitFieldRwSw[ubRow].sw1 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL1_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw1 = FALSE;

            /** - Output zero */ 
            ubKeySel = 0u;                    
        }

        /** - Hardware test check */
        if( SWITCH_TEST > gRpdoCanLanguageSelectInput.ubData ) 
        {     
            /** - High edge check SWITCH 2 and not special function key row normal scenario */
            if(( KEYSW2 == ubCheckTransition ) && (ubRow < 4u))
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw2 = TRUE;
            
                /** - Output on Rising edge */
                ubKeySel = KEYSW2;                    
            }

            /** - Check if SWITCH 2 in a particular Row pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[ubRow].sw2 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL2_RCV )) && (ubRow < 4u) )
            { 
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw2 = FALSE;
 
                /** - Output zero */
                ubKeySel = 0u;                          
            }
               
            /** - High edge check SWITCH 2 Row 4 Special function Key */ 
            if( ( KEYSW2 == ubCheckTransition ) && (ubRow == 4u))
            {    
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw2 = TRUE;

                /** - Output zero to prohibit sending out another character */  
                ubKeySel = 0u;                   
            }

            /** - Check if SWITCH 2 in Row 4 pressed and see if de-bounced masked value low transition */                
            if( ( TRUE == BitFieldRwSw[4u].sw2 ) && (0u == (gKey_Switches.uwSwitches[4u] & COL2_RCV )) && (ubRow == 4u) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw2 = FALSE;

                /** - Output zero to prohibit sending out another character */ 
                ubKeySel = 0u;
            }
        } 
        /** This is the hardware test */
        else
        {
            /** - High edge check SWITCH 2 less than row parameter */         
            if( KEYSW2 == ubCheckTransition ) 
            {  
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw2 = TRUE;
                
                /** - Output on Rising edge */
                ubKeySel = KEYSW2;            
            }

            /** - Check if SWITCH 2 in a particular Row pressed and see if de-bounced masked value low and transition less than row parameter */                 
            if( ( TRUE == BitFieldRwSw[ubRow].sw2 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL2_RCV )) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw2 = FALSE;
            
                /** - Output zero */ 
                ubKeySel = 0u;                    
            }   
        }

        /** - Hardware test check  */
        if( gRpdoCanLanguageSelectInput.ubData < SWITCH_TEST ) 
        { 
            /** - High edge check SWITCH 3 and not special function key row normal scenario */ 
            if( ( KEYSW3 == ubCheckTransition ) && (ubRow < 4u))
            { 
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw3 = TRUE; 
          
                /** - Output on Rising edge  */
                ubKeySel = KEYSW3;        
            }

            /** - Check if SWITCH 3 in a particular Row pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[ubRow].sw3 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL3_RCV )) && (ubRow < 4u) ) 
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw3 = FALSE; 
      
                /** - Output zero */ 
                ubKeySel = 0u;          
            }

            /** - High edge check SWITCH 3 Row 4 Special function Key */ 
            if( ( KEYSW3 == ubCheckTransition ) && (ubRow == 4u))
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw3 = TRUE;

                /** - Output zero */ 
                ubKeySel = 0u;       
            }

            /** - Check if SWITCH 3 in Row 4 pressed and see if de-bounced masked value low transition */                
            if( ( TRUE == BitFieldRwSw[4u].sw3 ) && ( 0u == ( gKey_Switches.uwSwitches[4u] & COL3_RCV )) && (ubRow == 4u) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw3 = FALSE;

                /** - Output zero */ 
                ubKeySel = 0u;       
            } 
        }  
        /** This is the hardware test */
        else
        {
            /** - High edge check SWITCH 3 and de-bounce is true and not locked out */ 
            if ( KEYSW3 == ubCheckTransition ) 
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw3 = TRUE;
                
                /** - Output on Rising edge  */
                ubKeySel = KEYSW3;       
            }

            /** - Check if SWITCH 3 in a particular Row pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[ubRow].sw3 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL3_RCV )) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw3 = FALSE;
                
                /** - Output zero */ 
                 ubKeySel = 0u;           
            }   
        }

        /** - High edge check SWITCH 4 and de-bounce is true and not locked out*/ 
        if ( KEYSW4 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw4 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW4;       
        }

        /** - Check if SWITCH 4 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw4 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL4_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw4 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;           
        }

        /** - High edge check SWITCH 5 and de-bounce is true and not locked out*/ 
        if ( KEYSW5 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw5 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW5;        
        }

        /** - Check if SWITCH 5 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw5 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL5_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw5 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;           
        }
   
        /** - High edge check SWITCH 6 and de-bounce is true and not locked out */ 
        if ( KEYSW6 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw6 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW6;       
        }

        /** - Check if SWITCH 6 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw6 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL6_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw6 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - Hardware test check */
        if( gRpdoCanLanguageSelectInput.ubData < SWITCH_TEST ) 
        { 
            /** - High edge check SWITCH 7 and not special function key row normal scenario */ 
            if ( ( KEYSW7 == ubCheckTransition ) && (ubRow < 4u) )
            { 
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw7 = TRUE; 

                /** - Output on Rising edge  */
                ubKeySel = KEYSW7;        
            }

            /** - Check if  SWITCH 7 in a particular Row pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[ubRow].sw7 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL7_RCV )) && (ubRow < 4u) )
            { 
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw7 = FALSE; 

                /** - Output zero */ 
                ubKeySel = 0u;           
            }

            /** - High edge check SWITCH 7 Row4 Special function Key */ 
            if ( ( KEYSW7 == ubCheckTransition ) && (ubRow == 4u))
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw7 = TRUE;
 
                /** - Output zero */ 
                ubKeySel = 0u; 
            }
            /** - Check if  SWITCH 7 in Row 4 pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[4u].sw7 ) && ( 0u == ( gKey_Switches.uwSwitches[4u] & COL7_RCV )) && (ubRow == 4u) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw7 = FALSE;

                /** - Output zero */ 
                ubKeySel = 0u; 
            } 
        }  
        /** This is the hardware test*/
        else
        {
            /** - High edge check SWITCH 7 and de-bounce is true and not locked out */ 
            if ( KEYSW7 == ubCheckTransition ) 
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw7 = TRUE;
 
                /** - Output on Rising edge  */
                ubKeySel = KEYSW7;       
            }

            /** - Check if SWITCH 7 in a particular Row pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[ubRow].sw7 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL7_RCV )) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw7 = FALSE;
 
                /** - Output zero */ 
                ubKeySel = 0u;          
            }
        }

        /** - Hardware test check  */
        if( gRpdoCanLanguageSelectInput.ubData < SWITCH_TEST ) 
        {
            /** - High edge check SWITCH 8 and not special function key row normal scenario */ 
            if ( ( KEYSW8 == ubCheckTransition ) && (ubRow < 4u) )
            { 
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw8 = TRUE; 

                /** - Output on Rising edge  */
                ubKeySel = KEYSW8;        
            }

            /** - Check if SWITCH 8 in a particular Row pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[ubRow].sw8 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL8_RCV )) && (ubRow < 4u) )
            { 
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw8 = FALSE; 

                /** - Output zero */ 
                ubKeySel = 0u;           
            }

            /** - High edge check SWITCH 8 Row4 Special function Key */
            if ( ( KEYSW8 == ubCheckTransition ) && (ubRow == 4u))
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw8 = TRUE;

                /** - Output zero */ 
                ubKeySel = 0u; 
            }

            /** - Check if SWITCH 8 in Row 4 pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[4u].sw8 ) && ( 0u == ( gKey_Switches.uwSwitches[4u] & COL8_RCV )) && (ubRow == 4u) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw8 = FALSE;

                /** - Output zero */ 
                ubKeySel = 0u;  
            }  
        }
        /** This is the hardware test*/
        else
        {
            /** - High edge check SWITCH 8 and de-bounce is true and not locked out */ 
            if ( KEYSW8 == ubCheckTransition ) 
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw8 = TRUE;
 
                /** - Output on Rising edge  */
                ubKeySel = KEYSW8;       
            }

            /** - Check if SWITCH 8 in a particular Row pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[ubRow].sw8 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL8_RCV )) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw8 = FALSE;
 
                /** - Output zero */ 
                ubKeySel = 0u;          
            }
        }

        /** - Hardware test check  */
        if( gRpdoCanLanguageSelectInput.ubData < SWITCH_TEST ) 
        {
            /** - High edge check SWITCH 9 and not special function key row normal scenario */ 
            if ( ( KEYSW9 == ubCheckTransition ) && (ubRow < 4u) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw9 = TRUE;  

                /** - Output on Rising edge */
                ubKeySel = KEYSW9;        
            }

            /** - Check if SWITCH 9 in a particular Row pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[ubRow].sw9 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL9_RCV )) && (ubRow < 4))
            { 
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw9 = FALSE;   

                /** - Output zero */ 
                ubKeySel = 0u;          
            }

            /** - High edge check SWITCH 9 Row4 Special function Key */
            if ( ( KEYSW9 == ubCheckTransition ) && (ubRow == 4u))
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw9 = TRUE;

                /** - Output zero */ 
                ubKeySel = 0u; 
            }

            /** - Check if SWITCH 9 in Row 4 pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[4u].sw9 ) && ( 0u == ( gKey_Switches.uwSwitches[4u] & COL9_RCV )) && (ubRow == 4u) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[4u].sw9 = FALSE;

                /** - Output zero */ 
                ubKeySel = 0u; 
            }  
        }    
        /** This is the hardware test*/
        else
        {
            /** - High edge check SWITCH 9 and de-bounce is true and not locked out */ 
            if ( KEYSW9 == ubCheckTransition ) 
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw9 = TRUE;
               
                /** - Output on Rising edge */
                ubKeySel = KEYSW9;       
            }

            /** - Check if SWITCH 9 in a particular Row pressed and see if de-bounced masked value low transition */                
            if ( ( TRUE == BitFieldRwSw[ubRow].sw9 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL9_RCV )) )
            {
                /** - Update switch currently pressed */
                BitFieldRwSw[ubRow].sw9 = FALSE;
               
                /** - Output zero */ 
                ubKeySel = 0u;          
            }
        }

        /** - High edge check SWITCH 10 and de-bounce is true and not locked out */ 
        if ( KEYSW10 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw10 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW10;        
        }

        /** - Check if SWITCH 10 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw10 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL10_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw10 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - High edge check SWITCH 11 and de-bounce is true and not locked out */ 
        if ( KEYSW11 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw11 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW11;        
        }

        /** - Check if SWITCH 11 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw11 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL11_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw11 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - High edge check SWITCH 12 and de-bounce is true and not locked out */ 
        if ( KEYSW12 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw12 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW12;        
        }

        /** - Check if SWITCH 12 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw12 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL12_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw12 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** High edge check SWITCH 13 and de-bounce is true and not locked out*/ 
        if ( KEYSW13 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw13 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW13;        
        }
        /** - Check if SWITCH 13 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw13 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL13_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw13 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }
 
        /** High edge check SWITCH 14 and de-bounce is true and not locked out*/ 
        if ( KEYSW14 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw14 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW14;        
        }

        /** - Check if SWITCH 14 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw14 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL14_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw14 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - High edge check SWITCH 15 and de-bounce is true and not locked out*/
        if ( KEYSW15 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw15 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW15;        
        }

        /** - Check if SWITCH 15 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw15 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL15_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw15 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** High edge check SWITCH 16 and de-bounce is true and not locked out*/
        if ( KEYSW16 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw16 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW16;        
        }

        /** - Check if SWITCH 16 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw16 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL16_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw16 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - High edge check SWITCH 17 and de-bounce is true and not locked out*/ 
        if ( KEYSW17 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw17 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW17;        
        }

        /** - Check if SWITCH 17 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw17 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL17_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw17 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - High edge check SWITCH 18 and de-bounce is true and not locked out*/ 
        if ( KEYSW18 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw18 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW18;        
        }

        /** - Check if SWITCH 18 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw18 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL18_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw18 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - High edge check SWITCH 19 and de-bounce is true and not locked out*/ 
        if ( KEYSW19 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw19 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW19;        
        }

        /** - Check if SWITCH 19 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw19 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL19_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw19 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - High edge check SWITCH 20 and de-bounce is true and not locked out*/ 
        if ( KEYSW20 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw20 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW20;        
        }

        /** - Check if SWITCH 20 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw20 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL20_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw20 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - High edge check SWITCH 21 and de-bounce is true and not locked out*/ 
        if ( KEYSW21 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw21 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW21;        
        }

        /** - Check if SWITCH 21 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw21 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL21_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw21 = FALSE;
 
            /** - Output zero */ 
            ubKeySel = 0u;          
        }

        /** - High edge check SWITCH 22 and de-bounce is true and not locked out*/ 
        if ( KEYSW22 == ubCheckTransition ) 
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw22 = TRUE;
 
            /** - Output on Rising edge  */
            ubKeySel = KEYSW22;        
        }

        /** - Check if SWITCH 22 in a particular Row pressed and see if de-bounced masked value low transition */                
        if ( ( TRUE == BitFieldRwSw[ubRow].sw22 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL22_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw22 = FALSE;
 
           /** - Output zero */ 
           ubKeySel = 0u;          
        }
    }
    /** - Keys parameter switches read on a Press then Release*/
    else
    {
        /** - High edge check SWITCH 1*/
        if ( KEYSW1 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw1 = TRUE;

            /** - Output zero */
            ubKeySel =  0u;
        }
        /** - Check if Switch in a particular Row pressed and see if de-bounced masked value low transition */                
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw1 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL1_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw1 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW1;               
        }

        /** - High edge check SWITCH 2*/ 
        if ( KEYSW2 == ubCheckTransition)
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw2 = TRUE;

            /** - Output zero */
            ubKeySel =  0u; 
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw2 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL2_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw2 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW2;               
        }
    
        /** - High edge check SWITCH 3*/ 
        if ( KEYSW3 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw3 = TRUE;

            /** - Output zero */
            ubKeySel =  0u;
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw3 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL3_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw3 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW3;               
        }

        /** - High edge check SWITCH 4*/ 
        if ( KEYSW4 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw4 = TRUE;   

            /** - Output zero */
            ubKeySel =  0u;
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw4 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL4_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw4 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW4;               
        }

        /** - High edge check SWITCH 5*/ 
        if ( KEYSW5 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw5 = TRUE;  

            /** - Output zero */
            ubKeySel =  0u;       
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw5 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL5_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw5 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW5;               
        }

        /** - High edge check SWITCH 6*/ 
        if ( KEYSW6 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw6 = TRUE;  

            /** - Output zero */
            ubKeySel =  0u;        
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw6 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL6_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw6 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW6;               
        }    

        /** - High edge check SWITCH 7*/ 
        if ( KEYSW7 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw7 = TRUE;

            /** - Output zero */
            ubKeySel =  0u;        
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw7 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL7_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw7 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW7;               
        }    

        /** - High edge check SWITCH 8*/ 
        if ( KEYSW8 == ubCheckTransition)
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw8 = TRUE;

            /** - Output zero */
            ubKeySel =  0u;     
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw8) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL8_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw8 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW8;               
        }    

        /** - High edge check SWITCH 9*/ 
        if ( KEYSW9 == ubCheckTransition)
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw9 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;    
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw9 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL9_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw9 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW9;               
        }    

        /** - High edge check SWITCH 10*/ 
        if ( KEYSW10 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw10 = TRUE;  
  
            /** - Output zero */
            ubKeySel =  0u;    
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw10 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL10_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw10 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW10;               
        }    

        /** - High edge check SWITCH 11*/ 
        if ( KEYSW11 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw11 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;         
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw11 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL11_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw11 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW11;               
        }    

        /** - High edge check SWITCH 12 All below */ 
        if ( KEYSW12 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw12 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u; 
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw12 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL12_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw12 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW12;               
        }    

        /** - High edge check SWITCH 13 All below */ 
        if ( KEYSW13 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw13 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;        
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw13 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL13_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw13 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW13;               
        }    

        /** - High edge check SWITCH 14 All below */ 
        if ( KEYSW14 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw14 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;    
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw14 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL14_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw14 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW14;               
        }    

        /** - High edge check SWITCH 15 All below */ 
        if ( KEYSW15 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw15 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;     
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw15 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL15_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw15 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW15;               
        }    

        /** - High edge check SWITCH 16 All below */ 
        if ( KEYSW16 == ubCheckTransition)
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw16 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;       
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw16 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL16_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw16 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW16;               
        }    

        /** - High edge check SWITCH 17 All below */ 
        if ( KEYSW17 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw17 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;      
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw17 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL17_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw17 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW17;               
        }    

        /** - High edge check SWITCH 18 All below */ 
        if ( KEYSW18 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw18 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;        
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw18 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL18_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw18 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW17;               
        }    

        /** - High edge check SWITCH 19 All below */ 
        if ( KEYSW19 == ubCheckTransition)
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw19 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;        
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw19 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL19_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw19 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW19;               
        }    

        /** - High edge check SWITCH 20 All below */ 
        if ( KEYSW20 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw20 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;        
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw20 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL20_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw20 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW20;               
        } 

        /** - High edge check SWITCH 21 All below */ 
        if ( KEYSW21 == ubCheckTransition )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw21 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;      
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw21 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL21_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw21 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW21;               
        }    

        /** - High edge check SWITCH 22 All below */ 
        if ( KEYSW22 == ubCheckTransition)
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw22 = TRUE; 

            /** - Output zero */
            ubKeySel =  0u;        
        }
        /** - Update Switch and de-bounced masked value low transition */                 
        else if ( ( TRUE == BitFieldRwSw[ubRow].sw22 ) && ( 0u == ( gKey_Switches.uwSwitches[ubRow] & COL22_RCV )) )
        {
            /** - Update switch currently pressed */
            BitFieldRwSw[ubRow].sw22 = FALSE;
 
            /** - Output switch on falling edge */ 
            ubKeySel =  KEYSW22;               
        } 
    }

    /** - return the selected key with parameter value */
    return ubKeySel;
}
