/************************************************************************************************
 *  File:       AdcData.h
 *
 *  Purpose:    Defines the data structures for the ADC scaled data
 *
 *  Project:    C1103
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 9/29/2005
 *
 ************************************************************************************************
*/
#include "IQmathLib.h"

#ifndef _AdcData_h
#ifndef _AdcData_h_local
    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/

    #define ZERO_CURRENT_TICKS  50
    #define ADC_PASS_COUNT  10
    #define ADC_CAL_FILTER  1

    typedef struct
    {
       _iq19 slSV1_I_mA_q19;
       _iq19 slSV2_I_mA_q19;
       _iq19 slInput_AN1_Volts_q19;
       _iq19 slInput_AN2_Volts_q19;
       _iq19 slBattSense_Volts_q19;
       _iq19 slN5_Volts_q19;
       _iq19 slP12_Volts_q19;
       _iq19 slLAS1_Supply_Volts_q19;
       _iq19 slLAS2_Supply_Volts_q19;
       _iq19 slCAM1_Supply_Volts_q19;
       _iq19 slCAM2_Supply_Volts_q19;
       _iq19 slCAM3_Supply_Volts_q19;
       _iq19 slSpare_CH9_Volts_q19;
       _iq19 slSpare_CH12_Volts_q19;

       _iq19 slSV1_I_mA_Average_q19;
       _iq19 slSV2_I_mA_Average_q19;
    } ADC_SCALED_DATA_TYPE;

    typedef struct
    {

       _iq19 slSV1_I_mA_Count_Average_q19;
       _iq19 slSV2_I_mA_Count_Average_q19;
       _iq19 slInput_AN1_Volts_Count_Average_q19;
       _iq19 slInput_AN2_Volts_Count_Average_q19;
       _iq19 slBattSense_Volts_Count_Average_q19;
       _iq19 slRef_2_5_Volts_Count_Average_q19;
       _iq19 slRef_227_mV_Count_Average_q19;
       _iq19 slN5_Volts_Count_Average_q19;
       _iq19 slP12_Volts_Count_Average_q19;
       _iq19 slLAS1_Supply_Volts_Count_Average_q19;
       _iq19 slLAS2_Supply_Volts_Count_Average_q19;
       _iq19 slCAM1_Supply_Volts_Count_Average_q19;
       _iq19 slCAM2_Supply_Volts_Count_Average_q19;
       _iq19 slCAM3_Supply_Volts_Count_Average_q19;
       _iq19 slSpare_CH9_Volts_Count_Average_q19;
       _iq19 slSpare_CH12_Volts_Count_Average_q19;

       uword uwSV1_I_mA_Average;
       uword uwSV2_I_mA_Average;
       uword uwInput_AN1_Volts_Average;
       uword uwInput_AN2_Volts_Average;
       uword uwBattSense_Volts_Average;
       uword uwRef_2_5_Volts_Average;
       uword uwRef_227_mV_Average;
       uword uwN5_Volts_Average;
       uword uwP12_Volts_Average;
       uword uwLAS1_Supply_Volts_Average;
       uword uwLAS2_Supply_Volts_Average;
       uword uwCAM1_Supply_Volts_Average;
       uword uwCAM2_Supply_Volts_Average;
       uword uwCAM3_Supply_Volts_Average;
       uword uwSpare_CH9_Volts_Average;
       uword uwSpare_CH12_Volts_Average;
    } ADC_CAL_DATA_TYPE;

    // Macros
    #define mScale_Linear( X, M, B) (((slong)M * (slong)X) + (slong)B)



#endif
#endif

#ifdef ADCDATA_MODULE

    #ifndef _AdcData_h_local
    #define _AdcData_h_local 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Defines

    // Delay Times

    //Funciton prototypes
    void vInit_ADC_Data(void);
    void vFilterADC_Channel(_iq19 *pq19Average, uword uwCountsNew, uword *puwCountsOut, uword uwPercent);
    slong slScalePoly(uword uwCounts, slong slSqrRootP1, slong slP2, slong P3);

    // Global variables
    ADC_SCALED_DATA_TYPE gADC_Scaled;
    ADC_CAL_DATA_TYPE    gADC_Cal;
    boolean gfADC_Blanking;             // used in startup of ADC system
    uword guwADC_Pass_Count;            // used in startup of ADC system
    // Macros



    #endif


#else
    #ifndef _AdcData_h
    #define _AdcData_h 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/

    // Externaly visable function prototypes

    extern void vInit_ADC_Data(void);
    extern void vFilterADC_Channel(_iq19 *pq19Average, uword uwCountsNew, uword *puwCountsOut, uword uwPercent);
    extern slong slScalePoly(uword uwCounts, slong slSqrRootP1, slong slP2, slong P3);


    // Externaly visable global variables
    extern ADC_SCALED_DATA_TYPE gADC_Scaled;
    extern ADC_CAL_DATA_TYPE    gADC_Cal;
    extern boolean gfADC_Blanking;             // used in startup of ADC system
    extern uword guwADC_Pass_Count;            // used in startup of ADC system

    #endif
#endif

