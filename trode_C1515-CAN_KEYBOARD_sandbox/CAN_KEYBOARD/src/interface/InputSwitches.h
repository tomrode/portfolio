
/************************************************************************************************
 *  File:       InputSwitches.h
 *
 *  Purpose:    Handles the wetting current drive on the input switches
 *
 *  Project:    C1103
 *
 *  Copyright:  2005-2006 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey, Tom Rode
 *
 *  Revision History:
 *              Written 6/5/2006
 *              Revised 11/19/2014 
 *
 ************************************************************************************************
*/
#ifndef INPUT_SWITCHES_H
#define INPUT_SWITCHES_H
#include "types.h"
#include "utf_8.h"

/*========================================================================*
 *  SECTION - Global definitions
 *========================================================================*
 */
 
/** @def   PAD_E2 
 *  @brief Measure CPU utilization
 */  
#define PAD_E2              GpioDataRegs.GPADAT.bit.GPIO7

/** @def   MODULE_LED 
 *  @brief Module debug pin
 */  
#define MODULE_LED          GpioDataRegs.GPBDAT.bit.GPIO39
 
/** @def   COL1_RCV 
 *  @brief Mask value for sw1
 */ 
#define COL1_RCV            1u

/** @def   COL2_RCV 
 *  @brief Mask value for sw2
 */ 
#define COL2_RCV            2u

/** @def   COL3_RCV 
 *  @brief Mask value for sw3
 */ 
#define COL3_RCV            4u

/** @def   COL4_RCV 
 *  @brief Mask value for sw4
 */ 
#define COL4_RCV            8u

/** @def   COL5_RCV 
 *  @brief Mask value for sw5
 */ 
#define COL5_RCV            16u

/** @def   COL6_RCV 
 *  @brief Mask value for sw6
 */ 
#define COL6_RCV            32u

/** @def   COL7_RCV 
 *  @brief Mask value for sw7
 */ 
#define COL7_RCV            64u

/** @def   COL8_RCV 
 *  @brief Mask value for sw8
 */ 
#define COL8_RCV            128u

/** @def   COL9_RCV 
 *  @brief Mask value for sw9
 */ 
#define COL9_RCV            256u

/** @def   COL10_RCV 
 *  @brief Mask value for sw10
 */ 
#define COL10_RCV           512u

/** @def   COL11_RCV 
 *  @brief Mask value for sw11
 */ 
#define COL11_RCV           1024u

/** - Below, These values may never be used */

/** @def   COL12_RCV 
 *  @brief Mask value for sw12
 */ 
#define COL12_RCV           2048u

/** @def   COL13_RCV 
 *  @brief Mask value for sw13
 */        
#define COL13_RCV           4096u

/** @def   COL14_RCV 
 *  @brief Mask value for sw14
 */ 
#define COL14_RCV           8192u

/** @def   COL15_RCV 
 *  @brief Mask value for sw15
 */ 
#define COL15_RCV           16384u

/** @def   COL16_RCV 
 *  @brief Mask value for sw16
 */ 
#define COL16_RCV           32768u

/** @def   COL17_RCV 
 *  @brief Mask value for sw17
 */ 
#define COL17_RCV           65536u

/** @def   COL18_RCV 
 *  @brief Mask value for sw18
 */ 
#define COL18_RCV           131072u

/** @def   COL19_RCV 
 *  @brief Mask value for sw19
 */ 
#define COL19_RCV           262144u

/** @def   COL20_RCV 
 *  @brief Mask value for sw20
 */       
#define COL20_RCV           524288u

/** @def   COL21_RCV 
 *  @brief Mask value for sw21
 */         
#define COL21_RCV           1048576u

/** @def   COL22_RCV 
 *  @brief Mask value for sw22
 */       
#define COL22_RCV           2097152u

/** @def   HW_GPADAT_MSK 
 *  @brief GPIO register contents
 */
//#define HW_GPADAT_MSK       0x07FF

/** @def  CNT0  
 *  @brief Values used in InputSwitches module
 */
#define CNT0                0u

/** @def  CNT1  
 *  @brief Values used in InputSwitches module
 */
#define CNT1                1u

/** @def  CNT2  
 *  @brief Values used in InputSwitches module
 */
#define CNT2                2u

/** @def  CNT3  
 *  @brief Values used in InputSwitches module
 */
#define CNT3                3u

/** @def  CNT4  
 *  @brief Values used in InputSwitches module
 */
#define CNT4                4u

/** @def  CNT5  
 *  @brief Values used in InputSwitches module
 */
#define CNT5                5u

/** @def  CNT6  
 *  @brief Values used in InputSwitches module
 */
#define CNT6                6u
/*******************************************************************************
*                                                                             *
*  E x t e r n a l   S c o p e   S e c t i o n                                *
*                                                                             *
*******************************************************************************/


/** @struct  utf8
 *  @brief   Key board key structure column row reading
 *
 *  @typedef column_sw_val_t
 *  @brief   row and column read out conditioned value 
 */
typedef struct column_sw_val_cond
{
    uint8_t ubColumnSwRwPos;     /**< Switch position in the row */
    uint8_t ubColumnSwRwFal;     /**< Output on a rising to falling transition */
    uint16_t uwColumnSwRw;       /**< Which row switch */
    uint32_t uwColumnSwRwSht;    /**< Test for Stuck or Shorted Switch */ 
}column_sw_val_cond_t;

/** @struct  utf16
 *  @brief   Bit field to check if particular switch active upto 22 column switches
 *
 *  @typedef Multiple Key switch 
 *  @brief   
 */
typedef struct
{
     uint32_t sw1  : 1;          /**< Column Switch1 */
     uint32_t sw2  : 1;          /**< Column Switch2 */
     uint32_t sw3  : 1;          /**< Column Switch3 */
     uint32_t sw4  : 1;          /**< Column Switch4 */   
     uint32_t sw5  : 1;          /**< Column Switch5 */
     uint32_t sw6  : 1;          /**< Column Switch6 */
     uint32_t sw7  : 1;          /**< Column Switch7 */
     uint32_t sw8  : 1;          /**< Column Switch8 */
     uint32_t sw9  : 1;          /**< Column Switch9 */
     uint32_t sw10 : 1;          /**< Column Switch10 */
     uint32_t sw11 : 1;          /**< Column Switch11 */
     uint32_t sw12 : 1;          /**< Column Switch12 */
     uint32_t sw13 : 1;          /**< Column Switch13 */
     uint32_t sw14 : 1;          /**< Column Switch14 */
     uint32_t sw15 : 1;          /**< Column Switch15 */
     uint32_t sw16 : 1;          /**< Column Switch16 */
     uint32_t sw17 : 1;          /**< Column Switch17 */
     uint32_t sw18 : 1;          /**< Column Switch18 */
     uint32_t sw19 : 1;          /**< Column Switch19 */
     uint32_t sw20 : 1;          /**< Column Switch20 */
     uint32_t sw21 : 1;          /**< Column Switch21 */
     uint32_t sw22 : 1;          /**< Column Switch22 */
     uint32_t pad  : 10;         /**< Padding the bit field */ 
} BitFieldRwSw_t;

/** - test data remove when comlete*/
typedef struct TestVar
{
     uint8_t SeegUwRowColStat;
     uint8_t SeeRow;
     Key_Switches_t SeeDeB; 
     uint8_t SeeFlatIndex;
} TestVar_t;

/*******************************************************************************
*  External function Prototype section                                         *                                                                           
*******************************************************************************/
extern void vGetDebRowSw(void);
extern uint16_t uwMultiSwitchPress(uint16_t uwMultipleSwitches, uint8_t ubRow);
extern uint8_t ubGetRowSW(uint32_t uwColumnval);
extern uint8_t ubKeyDepressed(uint8_t ubCheckTransition, uint8_t ubRow);
extern boolean fGetSwitch(uword uwSwitch);
/*******************************************************************************
*  External Scope Variable section                                                           *                                                                           
*******************************************************************************/
extern volatile boolean gfWC_Output_Update;      // flag on when to update the output
extern BitFieldRwSw_t  BitFieldRwSw[6u];

#endif /*INPUT_SWITCHES_H*/

