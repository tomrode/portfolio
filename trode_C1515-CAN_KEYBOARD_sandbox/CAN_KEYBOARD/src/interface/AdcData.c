/************************************************************************************************
 *  File:       AdcData.c
 *
 *  Purpose:    Implements the data structures for the ADC scaled data
 *
 *  Project:    C1103
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 9/29/2005
 *
 ************************************************************************************************
*/
//****************************************************************************
// @Project Includes
//****************************************************************************
#include "types.h"
#include "string.h"
#include "fram.h"
#include "IQmathLib.h"
#include "math.h"

#define ADCDATA_MODULE 1
#include "AdcData.h"
#undef ADCDATA_MODULE



//****************************************************************************
// @Global Variables
//****************************************************************************

//****************************************************************************
// @Local Variables
//****************************************************************************

//****************************************************************************
// @External Prototypes
//****************************************************************************

//****************************************************************************
// @Prototypes Of Local Functions
//****************************************************************************


void vInit_ADC_Data(void)
{
    memset((void *)&gADC_Scaled, 0, sizeof(gADC_Scaled));
    memset((void *)&gADC_Cal, 0, sizeof(gADC_Cal));
    gfADC_Blanking = TRUE;
    guwADC_Pass_Count = 0;

}

#pragma CODE_SECTION(vFilterADC_Channel, "ramfuncs");
void vFilterADC_Channel(_iq19 *pq19Average, uword uwCountsNew, uword *puwCountsOut, uword uwPercent)
{
    _iq19 slOldReading;
    _iq19 slNewReading;
    _iq19 slPercentNew;

    if (!gfADC_Blanking)
    {
        slPercentNew = _IQ19mpy(((slong)uwPercent << 19), _IQ19(0.01));
        slOldReading = _IQ19mpy(*pq19Average, (524288L - slPercentNew));
        slNewReading = _IQ19mpy(((slong)uwCountsNew << 19), slPercentNew);
        *pq19Average = slOldReading + slNewReading;
        *puwCountsOut = *pq19Average >> 19;
    }
    else
    {
        *pq19Average = (slong)uwCountsNew << 19;
        *puwCountsOut = uwCountsNew;
    }
}

#pragma CODE_SECTION(slScalePoly, "ramfuncs");
slong slScalePoly(uword uwCounts, slong slSqrRootP1, slong slP2, slong slP3)
{
    boolean fNegFlag = FALSE;
    _iq19 X;
    _iq19 T1;
    _iq19 T2;
    _iq19 Result;

    // check the sign of the P1 term, if negative then flag it and make it positive
    if (slSqrRootP1 < 0)
    {
        fNegFlag = TRUE;
        slSqrRootP1 = labs(slSqrRootP1);
    }

    X = (slong)uwCounts << 19;
    T1 = _IQ19mpy(slSqrRootP1, X);
    T1 = _IQ19mpy(T1, T1);
    if (fNegFlag)
    {
        T1 *= -1;   // put sign back in
    }

    T2 = _IQ19mpy(slP2, X);

    Result = T1 + T2 + slP3;

    return Result;
}
