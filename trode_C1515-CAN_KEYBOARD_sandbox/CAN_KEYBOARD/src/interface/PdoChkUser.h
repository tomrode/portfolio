/******************************************************************************
 *  File:       PdoChkUser.h
 *
 *  Purpose:    Defines the interface and constants of the main hoist throttle
 *              system
 *
 *  Project:    C584
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Walter Conley
 *
 *  Revision History:
 *              Written 5/14/2003
 *              Revised
 *
 ******************************************************************************
*/
#ifndef pdochkuser_registration

  /*******************************************************************************
   *                                                                             *
   *  G l o b a l   S c o p e   S e c t i o n                                    *
   *                                                                             *
   *******************************************************************************/
   #include "types.h"

   // Defines
   #define PDO_CHECK_DATALOG  DL210_PDO_RX_CHECK

   // Indexes for RxPdoList[], , must match order of table
   #define DM_PDO1  0
   #define VCM_PDO3 1

   // Time definitions for loss of PDOs
   #define CK_PDO_UPDATE_MS   10
   #define LOST_DM_PDO1_MS    500 // 1/2 second
   #define LOST_CMD_PDO_MS    250 // 1/4 second

   // Bits in uwPdoStatus
   #define PDOCHK_DM_PDO1_RX  0x0001
   #define PDOCHK_VCM_PDO_RX  0x0002

#endif

#ifdef PDOCHKUSER_MODULE

   #ifndef pdochkuser_registration_local
   #define pdochkuser_registration_local 1

  /*******************************************************************************
   *                                                                             *
   *  I n t e r n a l   S c o p e   S e c t i o n                                *
   *                                                                             *
   *******************************************************************************/

   // Defines
   typedef struct
   {
      PDO_CHK *pPdo;
      uword   uwPdoBit;
      ubyte   ubEventIndex;
      uword   uwTimeOutMs;
   }PDO_CHECKS;

   // Define state machine states
   typedef enum
   {
      PDO_INIT = 0,
      PDO_TEST
   }ST_ENC_DIAG;

   // Function prototypes
   void vInitRxPdoTimeouts(void);
   void vCheckRxPdoTimouts(void);
   boolean fSeenFirstPdo(ubyte ubPdoIndex);

   // PdoChkUser variables
   PDO_CHK DmPdo;
   PDO_CHK VcmPdo;
   PDO_CHK NmPdo;
   uword   guwPdoStatus    = 0;
   ubyte   ubPdoCheckState = PDO_INIT;
   uword   guwPdoStatus;

   // Constants (must match indexes defined in global section)
   const PDO_CHECKS RxPdoList[] =
   {
      { (PDO_CHK *)&DmPdo,  PDOCHK_DM_PDO1_RX, EVNT_LOST_DISPLAY_PDO, LOST_DM_PDO1_MS },
      { (PDO_CHK *)&VcmPdo,  PDOCHK_VCM_PDO_RX,  EVNT_LOST_COMMAND_PDO, LOST_CMD_PDO_MS },
   };
   #define NUM_RX_PDOS (sizeof(RxPdoList)/sizeof(PDO_CHECKS))

   // Macros

   #endif


#else
   #ifndef pdochkuser_registration
   #define pdochkuser_registration 1
  /*******************************************************************************
   *                                                                             *
   *  E x t e r n a l   S c o p e   S e c t i o n                                *
   *                                                                             *
   *******************************************************************************/

   // Externaly visable PdoChkUser variables
   extern uword guwPdoStatus;

   // Externaly visable function prototypes
   extern void vInitRxPdoTimeouts(void);
   extern void    vCheckRxPdos(void);
   extern boolean fSeenFirstPdo(ubyte ubPdoIndex);
   extern ubyte   ubGetPdoCheckState(void);
   #endif
#endif


