/************************************************************************************************
 *  File:       Truck_PDO.c
 *
 *  Purpose:    Implements handling of truck PDO data
 *
 *  Project:    C584
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 7/2/2003
 *
 ************************************************************************************************
*/
#include "types.h"
#include "portab.h"
#include "cos_main.h"
#include "target.h"
#include "OsmSlave.h"
#include "ramtron.h"
#define TruckPDOFile 1
#include "Truck_PDO.h"
#undef TruckPDOFile
#include "param.h"
#include "fram.h"
#include "string.h"
#include "adc.h"
#include "AdcData.h"
#include "util.h"
#include "InputSwitches.h"
#include "ev.h"

/************************************************************************************************
 *  Function:   vInit_Object_Dictionary
 *
 *  Purpose:    Initializes the PDO and SDO structures
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Sets up the data structures.
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 11/1/2006
 *
 ************************************************************************************************
*/
void vInit_Object_Dictionary( void)
{
    ubyte i;

    // Init the outbound PDO
    for (i=0; i<8; i++)
    {
        gDatalog1.ubData[i] = 0;
        gDatalog2.ubData[i] = 0;
        gDatalog3.ubData[i] = 0;
        gDatalog4.ubData[i] = 0;
    }

    // Init the inbound PDO
    gDatalog_Req.ubDatalog = 0;
    gDatalog_Req.ubModule = 0;
    gDatalog_Req.ubOption = 0;

    memset((void *)&gDM_Data, 0 , sizeof(dm_data_t));
    gDM_Data.ubOSM_State = OSM_LEVEL1;

    gfDatalog1_TX = FALSE;
    gfDatalog2_TX = FALSE;
    gfDatalog3_TX = FALSE;
    gfDatalog4_TX = FALSE;
    gfSendPDO1    = FALSE;
    gfSendPDO2    = FALSE;
    gfSendPDO3    = FALSE;
    gfError_TX = FALSE;
    gfError2_TX = FALSE;
    guwMyCAN_ID = ReturnModuleId();

    gubLastFreshnessCounter = 0;
    gDM_Data.uwControlWord = (uword)eDISABLE_NONE;
    guwLocalControlWord = (uword)eDISABLE_NONE;
    gubAsmReadyForOperation = 0;
    gulSetup_Requests[DR_GROUP1] = REQ_FTR_TRK_VOLT;
    gulSetup_Requests[DR_GROUP2] = 0;
    gulSetup_Requests[DR_GROUP3] = 0;
    gulSetup_Requests[DR_GROUP4] = 0;
    gulSetup_Requests[DR_GROUP5] = 0;
    gulSetup_Requests[DR_GROUP6] = 0;
    gulSetup_Requests[DR_GROUP7] = 0;

}

/************************************************************************************************
 *  Function:   vSend_Datalog_Pkt1
 *
 *  Purpose:    Sends this packet out the CAN stack
 *
 *  Input:      Array of 8 bytes
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Causes trasmission of the packet.
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gDatalog1 defined in Truck_PDO.h
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 7/7/2005
 *
 ************************************************************************************************
*/
void vSend_Datalog_Pkt1( DATA_LOG_TYPE *ubdata)
{
    int i;

    for (i=0; i<8; i++)
    {
        gDatalog1.ubData[i] = ubdata->ubData[i];
    }
    gfDatalog1_TX = TRUE;
}

/************************************************************************************************
 *  Function:   vSend_Datalog_Pkt2
 *
 *  Purpose:    Sends this packet out the CAN stack
 *
 *  Input:      Array of 8 bytes
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Causes trasmission of the packet.
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gDatalog2 defined in Truck_PDO.h
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 7/7/2005
 *
 ************************************************************************************************
*/
void vSend_Datalog_Pkt2( DATA_LOG_TYPE *ubdata)
{
    int i;

    for (i=0; i<8; i++)
    {
        gDatalog2.ubData[i] = ubdata->ubData[i];
    }
    gfDatalog2_TX = TRUE;
}

/************************************************************************************************
 *  Function:   vSend_Datalog_Pkt3
 *
 *  Purpose:    Sends this packet out the CAN stack
 *
 *  Input:      Array of 8 bytes
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Causes trasmission of the packet.
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gDatalog3 defined in Truck_PDO.h
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 7/7/2005
 *
 ************************************************************************************************
*/
void vSend_Datalog_Pkt3( DATA_LOG_TYPE *ubdata)
{
    int i;

    for (i=0; i<8; i++)
    {
        gDatalog3.ubData[i] = ubdata->ubData[i];
    }
    gfDatalog3_TX = TRUE;
}

/************************************************************************************************
 *  Function:   vSend_Datalog_Pkt4
 *
 *  Purpose:    Sends this packet out the CAN stack
 *
 *  Input:      Array of 8 bytes
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Causes trasmission of the packet.
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gDatalog4 defined in Truck_PDO.h
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 7/7/2005
 *
 ************************************************************************************************
*/
void vSend_Datalog_Pkt4( DATA_LOG_TYPE *ubdata)
{
    int i;

    for (i=0; i<8; i++)
    {
        gDatalog4.ubData[i] = ubdata->ubData[i];
    }
    gfDatalog4_TX = TRUE;
}


/************************************************************************************************
 *  Function:   vSend_Error_TX
 *
 *  Purpose:    Sends this packet out the CAN stack
 *
 *  Input:      fActive -- true = set error, false = clear error
 *              uwEvent -- the event code to send
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Side Effect:
 *              Causes trasmission of the packet.
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gError_Data defined in Truck_PDO.h
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 1/6/2004
 *
 ************************************************************************************************
*/
void vSend_Error_TX( boolean fActive, uword uwEvent)
{
    if (fActive)
    {
        gError_Data.uwErrorCode = 0x1000;
    }
    else
    {
        gError_Data.uwErrorCode = 0x0000;
    }
    gError_Data.ubErrorRegister = 0x81;
    gError_Data.uwEventNumber = uwEvent;
    gError_Data.ubSpare[0] = 0;
    gError_Data.ubSpare[1] = 0;
    gError_Data.ubSpare[2] = 0;
    if (guwMyCAN_ID == CIM1_ID)
    {
        gfError_TX = TRUE;           // mark that this needs to go out the stack
    }
    else
    {
        gError_Data.uwEventNumber += 20;
        gfError2_TX = TRUE;
    }
}

void vSend_Pdo1()
{
}

void vSend_Pdo2()
{
}


