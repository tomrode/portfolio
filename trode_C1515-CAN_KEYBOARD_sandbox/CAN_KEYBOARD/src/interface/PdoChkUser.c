/*******************************************************************
 *
 * Filename:    PdoChkUser.c
 *
 * Copyright (C) 2006 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains functions and variables of PdoChkUser
 *              scope that are included as part of the infrastructure.
 *
 **********************************************************************/

//****************************************************************************
// @Project Includes
//****************************************************************************
#include "portab.h"
#include "cos_main.h"
#include "PdoChk.h"
#include "Evhandler.h"
#include "datalog.h"
#define PDOCHKUSER_MODULE 1
#include "PdoChkUser.h"
#undef PDOCHKUSER_MODULE
#include "watchdog.h"

extern vModInfo XDATA ModuleInfo; /* module information */

/*
 ** vCheckRxPdos
 *
 *  FILENAME:     PdoChkUser.c
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  Check for loss of receive PDO traffic
 *
 *  RETURNS:      Nothing
 *
 */
void vCheckRxPdos(void)
{
   // Process current state
   switch ( ubPdoCheckState )
   {
   case PDO_TEST:
      vCheckRxPdoTimouts();
      break;

   default:
   case PDO_INIT:
      vInitRxPdoTimeouts();
      break;
   }

   // Find next state
   switch ( ubPdoCheckState )
   {
   case PDO_TEST:
      if (ModuleInfo.bCommState != OPERATIONAL )
      {
         ubPdoCheckState = PDO_INIT;
      }
      break;

   default:
   case PDO_INIT:
      if ( ModuleInfo.bCommState == OPERATIONAL )
      {
         ubPdoCheckState = PDO_TEST;
      }
      break;
   }
   mWatchdogRecordTask(PDO_CHECK_RAN_FLAG);
}

/*
 ** vInitRxPdoTimeouts
 *
 *  FILENAME:     PdoChkUser.c
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  Initialize each receive PDO timeout in list
 *
 *  RETURNS:      Nothing
 *
 */
void vInitRxPdoTimeouts(void)
{
   ubyte ubPdo;

   // Walk though list of Rx PDOs and initialize
   for ( ubPdo = 0; ubPdo < NUM_RX_PDOS; ubPdo++ )
   {
      vPdoTimeoutInit(RxPdoList[ubPdo].pPdo,
                      CK_PDO_UPDATE_MS,
                      RxPdoList[ubPdo].uwTimeOutMs
                     );
   }
}

/*
 ** vCheckRxPdoTimeouts
 *
 *  FILENAME:     PdoChkUser.c
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  Step through each PDO in list and check if any have
 *                timed out.
 *
 *  RETURNS:      Nothing
 *
 */
void vCheckRxPdoTimouts(void)
{
   ubyte   ubPdo;

   // Walk though list of Rx PDOs and make sure there are no timeouts
   for ( ubPdo = 0; ubPdo < NUM_RX_PDOS; ubPdo++ )
   {
      // Perform diagnostic
      if ( fPdoTimeout(RxPdoList[ubPdo].pPdo,
                       RxPdoList[ubPdo].uwPdoBit,
                       guwPdoStatus,
                       (mCheckDatalog(DL200_PDO_CHECK_DATALOG) && mCheckDatalogOption(ubPdo))
                      )
         )
      {
         // Log a fault since diagnostic failed
         gfHandleEvent(RxPdoList[ubPdo].ubEventIndex, TRUE);
      }

      // Show PDO has been processed
      guwPdoStatus &= ~RxPdoList[ubPdo].uwPdoBit;
   }
}

/*
 ** fSeenFirstPdo
 *
 *  FILENAME:     PdoChkUser.c
 *
 *  PARAMETERS:   ubPdoIndex = index into RxPdoList[]
 *
 *  DESCRIPTION:  Check if the specified PDO has been seen yet.
 *
 *  RETURNS:      TRUE if the PDO has been received yet, otherwise FALSE.
 *
 */
boolean fSeenFirstPdo(ubyte ubPdoIndex)
{
   boolean fSeenFirst = FALSE;

   if ( ubPdoIndex < NUM_RX_PDOS )
   {
      fSeenFirst = RxPdoList[ubPdoIndex].pPdo->fFirstPdoSeen;
   }
   return(fSeenFirst);
}

ubyte ubGetPdoCheckState(void)
{
   return(ubPdoCheckState);
}

