 /************************************************************************************************
 *  File:       datalog.c
 *
 *  Purpose:    Implements the datalog functions
 *
 *  Project:    C584
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 4/21/2003
 *
 ************************************************************************************************
*/

//#include "main.h"
#include "portab.h"
#include "cos_main.h"
#define  DATALOG_MODULE 1
#include "datalog.h"
#undef DATALOG_MODULE
#include "Truck_CAN.h"
#include "stddef.h"
#include "ramtron.h"
#include "fram.h"

#define DL_STATE_INIT         0
#define DL_STATE_IDLE         1
#define DL_STATE_SEND_PACKET1 2
#define DL_STATE_SEND_PACKET2 3
#define DL_STATE_SEND_PACKET3 4
#define DL_STATE_SEND_PACKET4 5
#define DL_STATE_UPDATE_DATA  6


/************************************************************************************************
 *  Function:   vInit_Datalog
 *
 *  Purpose:    Sets up the datalog system
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 4/22/2003
 *
 ************************************************************************************************
*/
void vInit_Datalog(void)
{

    gubDatalogState = DL_STATE_INIT;
    gubDatalogPacket = 0;
    gubNumOfDatalogPackets = 0;
    gubData_log_number = 0;
    gubData_log_option = 0;
    gubData_log_module = 0;
    gDatalog_Req.ubDatalog = 0;
    gDatalog_Req.ubOption = 0;
    gDatalog_Req.ubModule = 0;
    gfDatalogDataPresent = FALSE;
    gfDatalogNewData = FALSE;
    gfLogToFRAM = FALSE;
    eLogState = LOG_STATE_IDLE;
}

/************************************************************************************************
 *  Function:   vStartDatalog
 *
 *  Purpose:    Sets up for a datalog
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gBufferedLog1 -- from datalog.h
 *              gBufferedLog2 -- from datalog.h
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 4/22/2003
 *
 ************************************************************************************************
*/
void vStartDatalog(void)
{
    if ((gubData_log_number != gDatalog_Req.ubDatalog) || (gubData_log_module != gDatalog_Req.ubModule))
    {
        if ( (gDatalog_Req.ubDatalog == 0) ||
             (
               ((gDatalog_Req.ubModule != CIM1_ID) && (guwMyCAN_ID == CIM1_ID)) ||
               ((gDatalog_Req.ubModule != CIM2_ID) && (guwMyCAN_ID == CIM2_ID))
             )
           )
        {
            gubData_log_option = 0;
            gubData_log_number = 0;
            gubData_log_module = 0;
            gDatalog_Req.ubModule = 0;
            gDatalog_Req.ubDatalog = 0;
            gubNumOfDatalogPackets = 0;
            if (gfLogToFRAM)
            {
                FRAM_Log_End();
            }
            gfLogToFRAM = FALSE;
            eLogState = LOG_STATE_IDLE;
        }
        else
        {
            gubNumOfDatalogPackets = gubDatalogLength[gDatalog_Req.ubDatalog] & 0x0F;  //get from table
            gfLogToFRAM = (gubDatalogLength[gDatalog_Req.ubDatalog] & 0x10) >> 4;       // get flag from table
            if (gubNumOfDatalogPackets > 0)
            {
                gubData_log_option = gDatalog_Req.ubOption;
                gubData_log_number = gDatalog_Req.ubDatalog;
                gubData_log_module = gDatalog_Req.ubModule;
                if (gfLogToFRAM)
                {
                    eLogState = LOG_STATE_START;
                    if (FRAM_Log_Start(FRAM_LOG_ADDR))
                    {
                        eLogState = LOG_STATE_LOGGING;
                        uwFRAM_Log_Count = 0;
                    }
                    else
                    {
                        eLogState = LOG_STATE_IDLE;
                        gubData_log_option = 0;
                        gubData_log_number = 0;
                        gubData_log_module = 0;
                    }
                }
            }
        }
    }
}

/************************************************************************************************
 *  Function:   vServiceDatalog
 *
 *  Purpose:    Handles datalog functions
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gBufferedLog1 -- from datalog.h
 *              gBufferedLog2 -- from datalog.h
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 4/22/2003
 *
 ************************************************************************************************
*/
void vServiceDatalog(void)
{
    ubyte ubCount;

    vStartDatalog();

    // Find Next State
    switch (gubDatalogState)
    {
        case DL_STATE_INIT:
            gubDatalogState = DL_STATE_IDLE;
            break;

        case DL_STATE_IDLE:
            if ((gubNumOfDatalogPackets > 0) && (gfDatalogNewData))
            {
                gubDatalogState = DL_STATE_UPDATE_DATA;
            }
            break;

        case DL_STATE_SEND_PACKET1:
            if ( (gubDatalogPacket == 2) && (gubNumOfDatalogPackets >= 2) )
            {
                gubDatalogState = DL_STATE_SEND_PACKET2;
            }
            else
            {
                if ( (gubDatalogPacket == 2) && (gubNumOfDatalogPackets == 1) )
                {
                    gubDatalogState = DL_STATE_UPDATE_DATA;
                }
                else
                {
                    if (gubNumOfDatalogPackets == 0)
                    {
                        gubDatalogState = DL_STATE_IDLE;
                    }
                }
            }
            break;

        case DL_STATE_SEND_PACKET2:
            if ( (gubDatalogPacket == 3) && (gubNumOfDatalogPackets >= 3) )
            {
                gubDatalogState = DL_STATE_SEND_PACKET3;
            }
            else
            {
                if ( (gubDatalogPacket == 3) && (gubNumOfDatalogPackets == 2) )
                {
                    gubDatalogState = DL_STATE_UPDATE_DATA;
                }
                else
                {
                    if (gubNumOfDatalogPackets == 0)
                    {
                        gubDatalogState = DL_STATE_IDLE;
                    }
                }
            }
            break;

        case DL_STATE_SEND_PACKET3:
            if ( (gubDatalogPacket == 4) && (gubNumOfDatalogPackets == 4) )
            {
                gubDatalogState = DL_STATE_SEND_PACKET4;
            }
            else
            {
                if ( (gubDatalogPacket == 4) && (gubNumOfDatalogPackets == 3) )
                {
                    gubDatalogState = DL_STATE_UPDATE_DATA;
                }
                else
                {
                    if (gubNumOfDatalogPackets == 0)
                    {
                        gubDatalogState = DL_STATE_IDLE;
                    }
                }
            }
            break;

        case DL_STATE_SEND_PACKET4:
            if (gubDatalogPacket == 5)
            {
                gubDatalogState = DL_STATE_UPDATE_DATA;
            }
            break;

        case DL_STATE_UPDATE_DATA:
            if (gubNumOfDatalogPackets == 0)
            {
                gubDatalogState = DL_STATE_IDLE;
                gfDatalogNewData = FALSE;
            }
            else
            {
                gubDatalogPacket = 1;
                gubDatalogState = DL_STATE_SEND_PACKET1;
                gfDatalogNewData = FALSE;
            }
            break;

        default:
            gubDatalogState = DL_STATE_INIT;
    }

    // Find State Output
    switch (gubDatalogState)
    {
        case DL_STATE_INIT:
            break;

        case DL_STATE_IDLE:
            break;

        case DL_STATE_SEND_PACKET1:
            if (gfDatalogNewData)
            {
                vSend_Datalog_Pkt1((DATA_LOG_TYPE *)&gSavedLog[0].ubData[0]);
                gubDatalogPacket++;
            }
            break;

        case DL_STATE_SEND_PACKET2:
            vSend_Datalog_Pkt2((DATA_LOG_TYPE *)&gSavedLog[1].ubData[0]);
            gubDatalogPacket++;
            break;

        case DL_STATE_SEND_PACKET3:
            vSend_Datalog_Pkt3((DATA_LOG_TYPE *)&gSavedLog[2].ubData[0]);
            gubDatalogPacket++;
            break;

        case DL_STATE_SEND_PACKET4:
            vSend_Datalog_Pkt4((DATA_LOG_TYPE *)&gSavedLog[3].ubData[0]);
            gubDatalogPacket++;
            break;

        case DL_STATE_UPDATE_DATA:
            for (ubCount = 0; ubCount < 8; ubCount++)
            {
                gSavedLog[0].ubData[ubCount] = gBufferedLog[0].ubData[ubCount];
                gSavedLog[1].ubData[ubCount] = gBufferedLog[1].ubData[ubCount];
                gSavedLog[2].ubData[ubCount] = gBufferedLog[2].ubData[ubCount];
                gSavedLog[3].ubData[ubCount] = gBufferedLog[3].ubData[ubCount];
            }
            break;

        default:
            break;
    }

}

/************************************************************************************************
 *  Function:   vSaveDatalog
 *
 *  Purpose:    Saves a datalog to the buffers.  The buffered data is put out the can bus
 *              at the next available slot.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gBufferedLog[] -- from datalog.h
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 4/22/2003
 *
 ************************************************************************************************
*/
void vSaveDatalog( volatile DATA_LOG_TYPE *C1, volatile DATA_LOG_TYPE *C2)
{
    ubyte ubCount;

    for (ubCount = 0; ubCount < 8; ubCount++)
    {
        if (C1 != NULL)
        {
            gBufferedLog[0].ubData[ubCount] = C1->ubData[ubCount];
        }
        if (C2 != NULL)
        {
            gBufferedLog[1].ubData[ubCount] = C2->ubData[ubCount];
        }
    }
    gfDatalogNewData = TRUE;
    gfDatalogDataPresent = TRUE;

}

/************************************************************************************************
 *  Function:   vSaveDatalogExtended
 *
 *  Purpose:    Saves an Extended datalog to the buffers.  The buffered data is put out the can bus
 *              at the next available slot.
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *              gBufferedLog[] -- from datalog.h
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 5/21/2004
 *
 ************************************************************************************************
*/
void vSaveDatalogExtended( volatile DATA_LOG_TYPE *C1,
                           volatile DATA_LOG_TYPE *C2,
                           volatile DATA_LOG_TYPE *C3,
                           volatile DATA_LOG_TYPE *C4)
{
    ubyte ubCount;

    if (!gfLogToFRAM)
    {
        for (ubCount = 0; ubCount < 8; ubCount++)
        {
            if (C1 != NULL)
            {
                gBufferedLog[0].ubData[ubCount] = C1->ubData[ubCount];
            }
            if (C2 != NULL)
            {
                gBufferedLog[1].ubData[ubCount] = C2->ubData[ubCount];
            }
            if (C3 != NULL)
            {
                gBufferedLog[2].ubData[ubCount] = C3->ubData[ubCount];
            }
            if (C4 != NULL)
            {
                gBufferedLog[3].ubData[ubCount] = C4->ubData[ubCount];
            }
        }
        gfDatalogNewData = TRUE;
        gfDatalogDataPresent = TRUE;
    }
    else
    {
        if (eLogState == LOG_STATE_LOGGING)
        {
            for (ubCount = 0; ubCount < 8; ubCount+=2)
            {
                if (C1 != NULL)
                {
                    uwFRAM_Buffer[(ubCount>>1)] = C1->ubData[ubCount] << 8 | C1->ubData[ubCount + 1];
                }
                if (C2 != NULL)
                {
                    uwFRAM_Buffer[(ubCount>>1)+4] = C2->ubData[ubCount] << 8 | C2->ubData[ubCount + 1];
                }
                if (C3 != NULL)
                {
                    uwFRAM_Buffer[(ubCount>>1)+8] = C3->ubData[ubCount] << 8 | C3->ubData[ubCount + 1];
                }
                if (C4 != NULL)
                {
                    uwFRAM_Buffer[(ubCount>>1)+12] = C4->ubData[ubCount] << 8 | C4->ubData[ubCount + 1];
                }
            }
            FRAM_Log_Write(&uwFRAM_Buffer[0], 16);
            uwFRAM_Log_Count += 16;
            if (uwFRAM_Log_Count >= guwFRAM_Log_Length)
            {
                eLogState = LOG_STATE_ENDING;
                FRAM_Log_End();
                uwFRAM_Log_Count = 0;
            }
        }
    }
}


void vFRAM_Datalog_Service(void)
{
    // Check for service on FRAM datalogs
    if (gfLogToFRAM && (eLogState == LOG_STATE_ENDING))
    {
        eLogState = LOG_STATE_SENDING;
    }

    if (eLogState == LOG_STATE_SENDING)
    {
        gfLogToFRAM = FALSE;
        ReadFram(&uwFRAM_Buffer[0], (FRAM_LOG_ADDR + uwFRAM_Log_Count), 16);
        uwFRAM_Log_Count += 16;
        Can1.ubData[0] = (uwFRAM_Buffer[0] >> 8) & 0xFF;
        Can1.ubData[1] = (uwFRAM_Buffer[0] & 0xFF);
        Can1.ubData[2] = (uwFRAM_Buffer[1] >> 8) & 0xFF;
        Can1.ubData[3] = (uwFRAM_Buffer[1] & 0xFF);
        Can1.ubData[4] = (uwFRAM_Buffer[2] >> 8) & 0xFF;
        Can1.ubData[5] = (uwFRAM_Buffer[2] & 0xFF);
        Can1.ubData[6] = (uwFRAM_Buffer[3] >> 8) & 0xFF;
        Can1.ubData[7] = (uwFRAM_Buffer[3] & 0xFF);

        Can2.ubData[0] = (uwFRAM_Buffer[4] >> 8) & 0xFF;
        Can2.ubData[1] = (uwFRAM_Buffer[4] & 0xFF);
        Can2.ubData[2] = (uwFRAM_Buffer[5] >> 8) & 0xFF;
        Can2.ubData[3] = (uwFRAM_Buffer[5] & 0xFF);
        Can2.ubData[4] = (uwFRAM_Buffer[6] >> 8) & 0xFF;
        Can2.ubData[5] = (uwFRAM_Buffer[6] & 0xFF);
        Can2.ubData[6] = (uwFRAM_Buffer[7] >> 8) & 0xFF;
        Can2.ubData[7] = (uwFRAM_Buffer[7] & 0xFF);

        Can3.ubData[0] = (uwFRAM_Buffer[8] >> 8) & 0xFF;
        Can3.ubData[1] = (uwFRAM_Buffer[8] & 0xFF);
        Can3.ubData[2] = (uwFRAM_Buffer[9] >> 8) & 0xFF;
        Can3.ubData[3] = (uwFRAM_Buffer[9] & 0xFF);
        Can3.ubData[4] = (uwFRAM_Buffer[10] >> 8) & 0xFF;
        Can3.ubData[5] = (uwFRAM_Buffer[10] & 0xFF);
        Can3.ubData[6] = (uwFRAM_Buffer[11] >> 8) & 0xFF;
        Can3.ubData[7] = (uwFRAM_Buffer[11] & 0xFF);

        Can4.ubData[0] = (uwFRAM_Buffer[12] >> 8) & 0xFF;
        Can4.ubData[1] = (uwFRAM_Buffer[12] & 0xFF);
        Can4.ubData[2] = (uwFRAM_Buffer[13] >> 8) & 0xFF;
        Can4.ubData[3] = (uwFRAM_Buffer[13] & 0xFF);
        Can4.ubData[4] = (uwFRAM_Buffer[14] >> 8) & 0xFF;
        Can4.ubData[5] = (uwFRAM_Buffer[14] & 0xFF);
        Can4.ubData[6] = (uwFRAM_Buffer[15] >> 8) & 0xFF;
        Can4.ubData[7] = (uwFRAM_Buffer[15] & 0xFF);

        vSaveDatalogExtended(&Can1, &Can2, &Can3, &Can4);

        if (uwFRAM_Log_Count >= guwFRAM_Log_Length)
        {
            guwBuffered_Flush_Counts = 0;
            // End the datalog
            eLogState = LOG_STATE_FLUSHING_BUFFER;
            gfDatalogNewData = FALSE;
        }
    }

    if (eLogState == LOG_STATE_FLUSHING_BUFFER)
    {
        guwBuffered_Flush_Counts++;
        if (guwBuffered_Flush_Counts >= LOG_FLUSH_TICKS)
        {
            guwBuffered_Flush_Counts = 0;
            gubData_log_option = 0;
            gubData_log_number = 0;
            gDatalog_Req.ubDatalog = 0;
            gubData_log_module = 0;
            gDatalog_Req.ubModule = 0;
            gubNumOfDatalogPackets = 0;
            eLogState = LOG_STATE_IDLE;
        }
    }
}
