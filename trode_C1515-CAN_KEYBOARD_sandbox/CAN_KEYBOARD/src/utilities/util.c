/*********************************************************************
 *
 * Filename:    util.c
 *
 * Copyright (C) 2002 Crown Equipment Corporation. All rights reserved.
 *
 * Description: 
 *
 **********************************************************************/

//****************************************************************************
// @Project Includes
//****************************************************************************
//#include "MAIN.H"
#include "types.h"
#include <stdlib.h>
#define UTIL_MODULE 1
#include "util.h"

//****************************************************************************
// @Global Variables
//****************************************************************************

//****************************************************************************
// @Local Variables
//****************************************************************************

//****************************************************************************
// @External Prototypes
//****************************************************************************

//****************************************************************************
// @Prototypes Of Local Functions
//****************************************************************************

/**********************************************************************
 *
 * Function:    delay_uS(delayval)
 *
 * Description: This function is used to delay a specific # of microseconds (uS).
 *              The argument "delayval" represents a specific # of uS. It is determined by
 *              the following formula:
 *
 *              delayval = 3(desired_delay_in_uS - 9.34) + 20  
 *
 *              Note: This function will be accurate for a clock speed of 24MHz and a desired_delay_in_uS
 *                    value > = 9.34uS.
 *
 * Argument(s): delayval - see above
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void delay_uS(uword delayval)
{
    uword delay_counter;
    for (delay_counter = 0; delay_counter < delayval; delay_counter++)
    {
    }
}

sword swClip(sword min, sword val, sword max)
{
  if ( val < min ) return min;
  if ( val > max ) return max;

  return val;
}

uword uwClip(uword min, uword val, uword max)
{
  if ( val < min ) return min;
  if ( val > max ) return max;

  return val;
}

ulong ulClip(ulong min, ulong val, ulong max)
{
  if ( val < min ) return min;
  if ( val > max ) return max;

  return val;
}

slong slClip(slong min, slong val, slong max)
{
  if ( val < min ) return min;
  if ( val > max ) return max;

  return val;
}

sword swMin( sword a, sword b )
{
    if ( a < b )
    {
        return( a );
    }
    else
    {   
        return( b );
    }
}

sword swMax( sword a, sword b )
{
    if ( a > b )
    {
        return( a );
    }
    else
    {   
        return( b );
    }
}

uword uwMin( uword a, uword b )
{
    if ( a < b )
    {
        return( a );
    }
    else
    {   
        return( b );
    }
}

uword uwMax( uword a, uword b )
{
    if ( a > b )
    {
        return( a );
    }
    else
    {   return( b );
    }
}

slong slMin( slong a, slong b )
{
    if ( a < b )
    {
        return( a );
    }
    else
    {   return( b );
    }
}

slong slMax( slong a, slong b )
{
    if ( a > b )
    {
        return( a );
    }
    else
    {   return( b );
    }
}

ulong ulMin( ulong a, ulong b )
{
    if ( a < b )
    {
        return( a );
    }
    else
    {   
        return( b );
    }
}

ulong ulMax( ulong a, ulong b )
{
    if ( a > b )
    {
        return( a );
    }
    else
    {   
        return( b );
    }
}

/*----------------------------------------------------------------------

   Functions: init_history
              update_history
              prev_history_equal_to
              prev_history_abs_greater_than
              prev_history_less_than
              sum_history
              history_within_dev

   Author: John Wilkinson
   Revised by: Bill McCroskey for C584 SCM

   Description:
   These functions manage FIFO buffers that are used for
   testing the input in hydraulics state transitions, and other stuff

 ---------------------------------------------------------------------*/
void vInit_History( sword *pswOldData,  sword swNum,  sword swValue )
{
    sword swHi;

    for ( swHi=0; swHi<swNum; swHi++ )
    {
        pswOldData[swHi] = swValue;    /*   Initialize buffer to value   */
    }
}

void vUpdate_History( sword *pswOldData, sword swNum, sword swValue)
{
    sword swHi;

    for ( swHi=0; swHi<(swNum-1); swHi++ )
    {
        pswOldData[swHi] = pswOldData[swHi+1];   /*  Add swValue to buffer    */
    }

    pswOldData[swNum-1] = swValue;
}

boolean fPrev_History_Equal_To( sword *pswOldData, sword swNum, sword swValue)
{
    sword swHi;
    boolean fResult = TRUE;

    for ( swHi=0; swHi<swNum; swHi++ )
    {                          /* Test if swValue = data in buffer  */
        if (pswOldData[swHi] != swValue)
        {
            fResult = FALSE;
        }
    }
    return (fResult);
}

boolean fPrev_History_Greater_Than( sword *pswOldData, sword swNum, sword swValue)
{
    sword swHi;
    boolean fResult = TRUE;

    for ( swHi=0; swHi<swNum; swHi++ )
    {                          /*  Test if swValue > data in buffer */
        if (pswOldData[swHi] <= swValue)
        {
            fResult = FALSE;
        }
    }
    return (fResult);
}

boolean fPrev_History_Abs_Greater_Than( sword *pswOldData, sword swNum, sword swValue)
{
    sword swHi;
    boolean fResult = TRUE;

    swValue = abs(swValue);

    for ( swHi=0; swHi<swNum; swHi++ )
    {
        if (abs(pswOldData[swHi]) <= swValue)
        {
            fResult = FALSE;
        }
    }
    return (fResult);
}

boolean fPrev_History_Less_Than( sword *pswOldData, sword swNum, sword swValue)
{
    sword swHi;
    boolean fResult = TRUE;

    for ( swHi=0; swHi<swNum; swHi++ )
    {                          /* Test if swValue < data in buffer  */
        if (pswOldData[swHi] >= swValue)
        {
            fResult = FALSE;
        }
    }
    return (fResult);
}

sword swAdd_History( sword *pswOldData, sword swNum )
{
    sword swHi;

    sword swAccum = 0;
                                             /*  Sum the buffer   */
    for ( swHi=0; ((swHi<swNum) && (swAccum < 32767)); swHi++ )
    {
       swAccum += pswOldData[swHi];
    }
    return (swAccum);
}

boolean fGreater_Than_History_Avg( sword *pswOldData, sword swNum, sword swValue )
{
    sword swHi;
    boolean fResult;

    sword swAccum = 0;
    for( swHi=0; swHi<(swNum-2); swHi++ )
    {
        swAccum += pswOldData[swHi];
    }
    swAccum /=(swNum-2);
                            /* Test if latest data is greater by  */
                            /*     'swValue' than the average       */
    if ( pswOldData[swNum-1] > ( swAccum + swValue ) )
        fResult = TRUE;
    else
        fResult = FALSE;
    return (fResult);
}

boolean fHistory_Within_Dev( sword *pswOldData, sword swNum, sword swValue )
{
    sword swHi;
    boolean fResult = TRUE;

    slong slAccum = 0;

    for ( swHi=0; swHi<swNum; swHi++ )
    {
        slAccum += pswOldData[swHi];
    }
    slAccum /= swNum;
                             /* Test if data in buffer is within  */
                             /*           'swValue' of average      */
    for ( swHi=0; swHi<swNum; swHi++ )
    {
        if ( labs( (slong)pswOldData[swHi] - slAccum ) > (slong)swValue )
        {
            fResult = FALSE;
        }
    }
    return (fResult);
}

/**
 * @fn       gvDelay(uint16_t uwDelay)
 *
 * @brief    This function adds in a small delay in "NOP's".
 *
 * @param    uwDelay = Amount of delay cycles
 *
 * @return   None
 *
 * @note     N/A
 *
 * @author   Tom Rode
 *
 */
void gvDelay(uint16_t uwDelay)
{
    uint16_t uwDelayCnt = 0;
	
    /** ###Functional overview: */
   
    while( uwDelayCnt < uwDelay )
    {
	   	/** - Delay count*/
	    uwDelayCnt++; 	
	    asm(" NOP ");        /* wait a cpu cycle */ 
    }
}


#undef UTIL_MODULE
