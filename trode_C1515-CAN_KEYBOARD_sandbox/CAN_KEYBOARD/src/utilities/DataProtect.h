/************************************************************************************************
 *	File:		DataProtect.h
 *
 *	Purpose:	Defines the DataProtection system -- Gives ability to suspend the tasks but not
 *              the hardware.
 *
 *	Project:    C584
 *
 *	Copyright:	2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 *	Author:		Bill McCroskey
 *
 *	Revision History:
 *				Written 1/8/2004
 *
 ************************************************************************************************
*/

#ifdef DATAPROTECT_MODULE
    #ifndef DATAPROTECT_LOCAL_REG
    #define DATAPROTECT_LOCAL_REG
    void vEnableTasks(void);
    void vDisableTasks(void);
    #endif
#else
    #ifndef DATAPROTECT_EXT_REG
    #define DATAPROTECT_EXT_REG
    extern void vEnableTasks(void);
    extern void vDisableTasks(void);
    #endif
#endif
