 /************************************************************************************************
 *  File:       datalog.h
 *
 *  Purpose:    Defines data and structures of the data log function
 *
 *  Project:    C584
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 4/21/2003
 *
 ************************************************************************************************
*/

//#include "main.h"
#include "types.h"
#include "Truck_PDO.h"

#ifndef datalog_registration

    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/
     // define the datalog structures

    // Defines

    #define DL1_SCHEDULER_LOG_1         1
    #define DL2_SCHEDULER_LOG_2         2

    #define DL4_EVENTMGR_DATALOG        4
    #define DL5_WATCHDOG_DATALOG        5

    #define DL15_INPUT_SWITCHES         15

    #define DL23_OSM_DATALOG            23
    #define DL24_FRAM_HOURS_LOG         24
    #define DL25_FRAM_MAP_LOG           25
    #define DL26_SV_CURRENT_LOG         26
    #define DL27_BRAKE_CURRENT_LOG      27

    #define DL72_EMGY_PROC              72

    #define DL80_BLINK_EVENTS           80

    #define DL107_FAULT_STATUS          107

    #define DL154_EV_HANDLER            154

    #define DL200_PDO_CHECK_DATALOG     200

    typedef enum
    {
        LOG_STATE_IDLE = 0,
        LOG_STATE_START = 1,
        LOG_STATE_LOGGING = 2,
        LOG_STATE_ENDING = 3,
        LOG_STATE_SENDING = 4,
        LOG_STATE_FLUSHING_BUFFER = 5
    } FRAM_LOG_STATE_TYPE;

    #define mCheckDatalog(n) (gubData_log_number == n)
    #define mCheckDatalogOption(n) (gubData_log_option == n)
    #define mCheckBufferedDatalog(n) ((gubData_log_number == n) && (gfLogToFRAM))

#endif

#ifdef DATALOG_MODULE

    #ifndef datalog_registration_local
    #define datalog_registration_local 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Defines

    #define LOG_FLUSH_TICKS  100        /* .5 second at 5ms per tick */

    //Funciton prototypes
    void vInit_Datalog(void);
    void vServiceDatalog(void);
    void vStartDatalog(void);
    void vSaveDatalog( volatile DATA_LOG_TYPE *, volatile DATA_LOG_TYPE *);
    void vSaveDatalogExtended(volatile DATA_LOG_TYPE *, volatile DATA_LOG_TYPE *, volatile DATA_LOG_TYPE *, volatile DATA_LOG_TYPE *);
    void vFRAM_Datalog_Service(void);

    // Global variables
    // Format of number is lower nibble is number of packets, 0x10 in upper nibble flags log to FRAM
    const ubyte gubDatalogLength[256] =
        {
            /*   0       */  0x00,
            /*                  1     2     3     4     5     6     7     8     9    10 */
            /*   1 - 10  */  0x14, 0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00,
            /*  11 - 20  */  0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00,
            /*  21 - 30  */  0x00, 0x00, 0x01, 0x02, 0x04, 0x04, 0x04, 0x00, 0x00, 0x00,
            /*  31 - 40  */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /*  41 - 50  */  0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /*  51 - 60  */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /*  61 - 70  */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /*  71 - 80  */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /*  81 - 90  */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /*  91 - 100 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 101 - 110 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00,
            /* 111 - 120 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 121 - 130 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 131 - 140 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 141 - 150 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 151 - 160 */  0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 161 - 170 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 171 - 180 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 181 - 190 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 191 - 200 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
            /* 201 - 210 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 211 - 220 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 221 - 230 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 231 - 240 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 241 - 250 */  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            /* 251 - 255 */  0x00, 0x00, 0x00, 0x00, 0x00

        };
    volatile ubyte gubData_log_number;
    volatile ubyte gubData_log_option;
    volatile ubyte gubData_log_module;
    volatile ubyte gfDatalogDataPresent;
    volatile ubyte gubDatalogState;
    volatile ubyte gubDatalogPacket;
    volatile ubyte gubNumOfDatalogPackets;
    volatile boolean gfLogToFRAM;
    FRAM_LOG_STATE_TYPE eLogState;
    uword uwFRAM_Buffer[16];
    uword uwFRAM_Log_Count;

    ubyte gfDatalogNewData;
    volatile DATA_LOG_TYPE  Can1;   // used for datalogging
    volatile DATA_LOG_TYPE  Can2;
    volatile DATA_LOG_TYPE  Can3;
    volatile DATA_LOG_TYPE  Can4;
    volatile uword guwBuffered_Flush_Counts;

    volatile DATA_LOG_TYPE gSavedLog[4];
    volatile DATA_LOG_TYPE gBufferedLog[4];

    // Macros
    #endif


#else
    #ifndef datalog_registration
    #define datalog_registration 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/

    // Externaly visable global variables
    extern volatile ubyte gubData_log_number;           // used by other modules for log number
    extern volatile ubyte gubData_log_option;
    extern volatile ubyte gubData_log_module;
    extern FRAM_LOG_STATE_TYPE eLogState;

    extern volatile ubyte gubDatalogState;
    extern volatile ubyte gubNumOfDatalogPackets;
    extern volatile boolean gfLogToFRAM;
    extern volatile DATA_LOG_TYPE  Can1;   // used for datalogging
    extern volatile DATA_LOG_TYPE  Can2;
    extern volatile DATA_LOG_TYPE  Can3;
    extern volatile DATA_LOG_TYPE  Can4;

    // Externaly visable function prototypes
    extern void vInit_Datalog(void);
    extern void vServiceDatalog(void);
    extern void vSaveDatalog( volatile DATA_LOG_TYPE *, volatile DATA_LOG_TYPE *);
    extern void vSaveDatalogExtended(volatile DATA_LOG_TYPE *, volatile DATA_LOG_TYPE *, volatile DATA_LOG_TYPE *, volatile DATA_LOG_TYPE *);
    extern void vFRAM_Datalog_Service(void);



    #endif
#endif
