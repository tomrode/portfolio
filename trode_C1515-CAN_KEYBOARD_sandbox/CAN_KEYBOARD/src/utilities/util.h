/************************************************************************************************
 *  File:       util.h
 *
 *  Purpose:    Defines the functions and data of the utility function module
 *
 *  Project:    C584
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 4/15/2003
 *
 ************************************************************************************************
*/

//#include "main.h"
#include "types.h"

#ifndef util_registration

    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/
    #define DELAY10uS   22
    #define DELAY20uS   52
    #define DELAY50uS   142
    #define DELAY100uS  292
    #define DELAY250uS  742
    #define DELAY500uS  1492
    #define DELAY5000uS 14992

    #define  fToBoolean(expression) ( (boolean)( (expression) != 0 ) )

#endif

#ifdef UTIL_MODULE

    #ifndef util_registration_local
    #define util_registration_local 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Defines

    //Funciton prototypes
    uword uwClip(uword , uword , uword );
    ulong ulClip(ulong , ulong , ulong );
    slong slClip(slong , slong , slong );
    sword swClip(sword , sword , sword );

    sword swMin( sword , sword  );
    sword swMax( sword , sword  );
    uword uwMin( uword , uword  );
    uword uwMax( uword , uword  );
    slong slMin( slong , slong  );
    slong slMax( slong , slong  );
    ulong ulMin( ulong , ulong  );
    ulong ulMax( ulong , ulong  );
    void vInit_History( sword *pswOldData,  sword swNum,  sword swValue );
    void vUpdate_History( sword *pswOldData, sword swNum, sword swValue);
    boolean fPrev_History_Equal_To( sword *pswOldData, sword swNum, sword swValue);
    boolean fPrev_History_Greater_Than( sword *pswOldData, sword swNum, sword swValue);
    boolean fPrev_History_Abs_Greater_Than( sword *pswOldData, sword swNum, sword swValue);
    boolean fPrev_History_Less_Than( sword *pswOldData, sword swNum, sword swValue);
    sword swAdd_History( sword *pswOldData, sword swNum );
    boolean fGreater_Than_History_Avg( sword *pswOldData, sword swNum, sword swValue );
    boolean fHistory_Within_Dev( sword *pswOldData, sword swNum, sword swValue );

    // Global variables

    // Macros



    #endif


#else
    #ifndef util_registration
    #define util_registration 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/

    // Externaly visable global variables

    // Externaly visable function prototypes
    extern uword uwClip(uword , uword , uword );
    extern ulong ulClip(ulong , ulong , ulong );
    extern slong slClip(slong , slong , slong );
    extern sword swClip(sword , sword , sword );
    extern sword swMin( sword , sword  );
    extern sword swMax( sword , sword  );
    extern uword uwMin( uword , uword  );
    extern uword uwMax( uword , uword  );
    extern slong slMin( slong , slong  );
    extern slong slMax( slong , slong  );
    extern ulong ulMin( ulong , ulong  );
    extern ulong ulMax( ulong , ulong  );
    extern slong slAbs( slong );
    extern void vInit_History( sword *pswOldData,  sword swNum,  sword swValue );
    extern void vUpdate_History( sword *pswOldData, sword swNum, sword swValue);
    extern boolean fPrev_History_Equal_To( sword *pswOldData, sword swNum, sword swValue);
    extern boolean fPrev_History_Greater_Than( sword *pswOldData, sword swNum, sword swValue);
    extern boolean fPrev_History_Abs_Greater_Than( sword *pswOldData, sword swNum, sword swValue);
    extern boolean fPrev_History_Less_Than( sword *pswOldData, sword swNum, sword swValue);
    extern sword swAdd_History( sword *pswOldData, sword swNum );
    extern boolean fGreater_Than_History_Avg( sword *pswOldData, sword swNum, sword swValue );
    extern boolean fHistory_Within_Dev( sword *pswOldData, sword swNum, sword swValue );
    extern void gvDelay(uint16_t uwDelay);
    #endif
#endif

