/************************************************************************************************
 *	File:		DataProtect.c
 *
 *	Purpose:	Implements the DataProtection system -- Gives ability to suspend the tasks but not
 *              the hardware.
 *
 *	Project:    C584
 *
 *	Copyright:	2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 *	Author:		Bill McCroskey
 *
 *	Revision History:
 *				Written 1/8/2004
 *
 ************************************************************************************************
*/

#define DATAPROTECT_MODULE
#include "DataProtect.h"
#undef DATAPROTECT_MODULE


void vDisableTasks(void)
{
}

void vEnableTasks(void)
{
}
