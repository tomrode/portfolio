#include "DSP2803x_Device.h"


#ifndef types_h_reg
#define types_h_reg

typedef unsigned char  ubyte;    // 1 byte unsigned; prefix: ub
typedef unsigned char  Uint8;    // 1 byte unsigned;
typedef unsigned char  uint8_t;  // 1 byte unsigned;
typedef signed char    sbyte;    // 1 byte signed;   prefix: sb
typedef signed char    sint8_t;  // 1 byte signed;  

typedef Uint16         uword;    // 2 byte unsigned; prefix: uw
typedef unsigned int   uint16_t; // 2 byte unsigned;
typedef unsigned int   sint16_t; // 2 byte signed; 
typedef signed int     sword;    // 2 byte signed;   prefix: sw

typedef unsigned long  uint32_t; // 4 byte unsigned; 
typedef unsigned long  Uint32_t; // 4 byte unsigned; 
typedef Uint32         ulong;    // 4 byte unsigned; prefix: ul
typedef signed long    sint32_t; // 4 byte signed
typedef signed long    slong;    // 4 byte signed;   prefix: sl


//#ifndef boolean
    typedef unsigned char  boolean;
//#endif
#ifndef FALSE
 #define FALSE 0
 #define TRUE  1
#endif

/** @def   NULL_PTR
 *  @brief Value of a null or invalid pointer
 */
#ifndef NULL_PTR
#define NULL_PTR 0U
#endif

#endif
