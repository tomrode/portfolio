/************************************************************************************************
 * File:       OsmSlaveUser.h
 *
 * Purpose:    Defines the interface for the user configurable part of the
 *             slave version of the operational state machine.
 *
 * Project:    C1101
 *
 * Copyright:  2006 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 07/14/2006
 *
 ************************************************************************************************
*/
//#include "main.h"

#ifndef  osmslaveuser_registration

   /*******************************************************************************
    *                                                                             *
    *  G l o b a l   S c o p e   S e c t i o n                                    *
    *                                                                             *
    *******************************************************************************/

   // Defines

#endif

#ifdef OSMSLAVEUSER_MODULE

   #ifndef osmslaveuser_registration_local
   #define osmslaveuser_registration_local 1

   /*******************************************************************************
    *                                                                             *
    *  I n t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Defines

   // Must receive all PDO within 15 seconds or fault(s) will be logged
   #define STARTUP_PDO_TIMEOUT_US 15000000

   // Function prototypes
   ulong ulGetTickCount(void);
   ulong ulGetTickUsDelta(void);
   boolean fReceivingAllPdos(ulong ulTimeoutUs);
   void vNullTaskProcesses(void);
   void vOperationalProcesses(void);
   void vPreOperationalProcesses(void);
   void vTestOutputProcesses(void);
   void vHardwareInit(void);
   void vStartupInit(void);
   ubyte gubGetTestOutputNumber(void);

   // Global variables
   ulong gulLastTimeCount;

   // Macros

   #endif


#else
   #ifndef osmslaveuser_registration
   #define osmslaveuser_registration 1
   /*******************************************************************************
    *                                                                             *
    *  E x t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Externaly visable global variables
   extern ulong gulLastTimeCount;

   // Externally visable function prototypes
   extern void    vStartupInit(void);
   extern void    vHardwareInit(void);
   extern void    vOsmInit(void);
   extern void    vOsmStandby(void);
   extern void    vNullTaskProcesses(void);
   extern void    vOperationalProcesses(void);
   extern void    vPreOperationalProcesses(void);
   extern void    vForcePdoTx(void);
   extern void    gvParameterBasedCalculations(void);
   extern boolean fReceivingAllPdos(ulong ulTimeoutUs);
   extern ulong   ulGetTickCount(void);
   extern ulong   ulGetTickUsDelta(void);
   extern void    vInitRxPdoTimeouts(void);
   extern ubyte   gubGetTestOutputNumber(void);

   #endif
#endif

