/************************************************************************************************
 *  File:       EventMgrUser.h
 *
 *  Purpose:    Defines the data structure sizes used for the Event Reporting system
 *
 *  Project:    C584
 *
 *  Copyright:  2004 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 3/10/2004
 *
 ************************************************************************************************
*/
#include "DataProtect.h"
#include "util.h"
#include "Events.h"

#ifndef Event_Mgr_Usr_Reg
#define Event_Mgr_Usr_Reg

    #define EVENT_PENDING_SIZE       4
    #define EVENT_REPORTED_SIZE      4
    #define EVENT_ACKED_SIZE         4
    #define EVENT_ACK_TIMEOUT_TICKS  40
    #define EVENT_RETRY_LIMIT        5     /* use zero for no retry limit */
    #define mDataLogNumber           gubData_log_number
    #define mEventMgrDataLog         DL4_EVENTMGR_DATALOG
    #define gfUpdateLocalEvents      gubEventsUpdated


#endif
