/************************************************************************************************
 *  File:       fram.h
 *
 *  Purpose:    Defines the non-volitile ram parameters for the truck
 *
 *  Project:    C1103
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 9/26/2005
 *
 ************************************************************************************************
*/
#include "HrMeter.h"
#include "SetupMaps.h"
#include "InputSwitches.h"
#include "IQmathLib.h"

#ifndef _fram_h
#ifndef _fram_h_local
    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/
    #define LOCAL_EVENT_LIST_SIZE   16
    #define CALIBRATION_VERSION     1


    typedef enum
    {
        Cal_Backup_State_Blank = 0x000,
        Cal_Backup_State_Written = 0x3333,
        Cal_Backup_State_Lockout = (int)0xCCCC
    } CAL_BACKUP_STATES;

    typedef struct
    {
        uword uwEventsLogged;
        uword uwEvent[LOCAL_EVENT_LIST_SIZE];
    } EVENT_HISTORY_TYPE;

    typedef struct
    {
        uword uwCRC;
        uword uwCS;
        EVENT_HISTORY_TYPE History;
    } LOCAL_EVENTS_TYPE ;

    typedef enum
    {
        FRAM_PROT_CAL_NO_COMMAND = 0,
        FRAM_PROT_CAL_CLEAR      = 1,
        FRAM_PROT_CAL_CLONE      = 2
    } FRAM_PROT_CAL_COMMANDS;

    typedef struct
    {
        HR_METER MotorHours;
        HR_METER ModHours;
    } HOUR_METER_TYPE;

    typedef struct
    {
        uword uwCRC;
        uword uwCS;
        HOUR_METER_TYPE HourMeters;
    } LOCAL_HOUR_METER_TYPE;


    typedef struct
    {
        uword uwVersion;
        _iq19 slAN0_Gain_q19;
        _iq19 slAN0_Offset_q19;
        _iq19 slAN1_Gain_q19;
        _iq19 slAN1_Offset_q19;
        _iq19 slAN2_Gain_q19;
        _iq19 slAN2_Offset_q19;
        _iq19 slAN3_Gain_q19;
        _iq19 slAN3_Offset_q19;
        _iq19 slAN4_Gain_q19;
        _iq19 slAN4_Offset_q19;
        _iq19 slAN5_Gain_q19;
        _iq19 slAN5_Offset_q19;
        _iq19 slAN6_Gain_q19;
        _iq19 slAN6_Offset_q19;
        _iq19 slAN7_Gain_q19;
        _iq19 slAN7_Offset_q19;
        _iq19 slAN8_Gain_q19;
        _iq19 slAN8_Offset_q19;
        _iq19 slAN9_Gain_q19;
        _iq19 slAN9_Offset_q19;
        _iq19 slAN10_Gain_q19;
        _iq19 slAN10_Offset_q19;
        _iq19 slAN11_Gain_q19;
        _iq19 slAN11_Offset_q19;
        _iq19 slAN12_Gain_q19;
        _iq19 slAN12_Offset_q19;
        _iq19 slAN13_Gain_q19;
        _iq19 slAN13_Offset_q19;
        _iq19 slAN14_Gain_q19;
        _iq19 slAN14_Offset_q19;
        _iq19 slAN15_Gain_q19;
        _iq19 slAN15_Offset_q19;
        _iq19 slAutoGain_q19;
        slong slAutoOffsetCounts;
        uword uwUnused[8];
    } FRAM_CAL_TABLE_TYPE;

    typedef struct
    {
        uword uwVsel_Laser1;
        uword uwVsel_Camera3;
        char szPartNumber[20];
    } FRAM_SETUP_TABLE_TYPE;

    // Macros
    #define fprm_Laser1_Voltage                gFram_Setup_Data.uwEncoderConfig.Bits.fEncr1VoltageSel
    #define fprm_Camera3_Voltage                gFram_Setup_Data.uwEncoderConfig.Bits.fEncr2VoltageSel

    #define fprm_SV1_Current_Gain            gFram_Cal_Data.slAN0_Gain_q19
    #define fprm_SV1_Current_Offset          gFram_Cal_Data.slAN0_Offset_q19
    #define fprm_SV2_Current_Gain            gFram_Cal_Data.slAN1_Gain_q19
    #define fprm_SV2_Current_Offset          gFram_Cal_Data.slAN1_Offset_q19
    #define fprm_Input_AN1_Gain              gFram_Cal_Data.slAN2_Gain_q19
    #define fprm_Input_AN1_Offset            gFram_Cal_Data.slAN2_Offset_q19
    #define fprm_Input_AN2_Gain              gFram_Cal_Data.slAN3_Gain_q19
    #define fprm_Input_AN2_Offset            gFram_Cal_Data.slAN3_Offset_q19
    #define fprm_BattSense_Gain              gFram_Cal_Data.slAN4_Gain_q19
    #define fprm_BattSense_Offset            gFram_Cal_Data.slAN4_Offset_q19
    #define fprm_N5_Volts_Gain               gFram_Cal_Data.slAN7_Gain_q19
    #define fprm_N5_Volts_Offset             gFram_Cal_Data.slAN7_Offset_q19
    #define fprm_P12_Volts_Gain              gFram_Cal_Data.slAN8_Gain_q19
    #define fprm_P12_Volts_Offset            gFram_Cal_Data.slAN8_Offset_q19
    #define fprm_LAS1_Supply_Gain            gFram_Cal_Data.slAN10_Gain_q19
    #define fprm_LAS1_Supply_Offset          gFram_Cal_Data.slAN10_Offset_q19
    #define fprm_LAS2_Supply_Gain            gFram_Cal_Data.slAN11_Gain_q19
    #define fprm_LAS2_Supply_Offset          gFram_Cal_Data.slAN11_Offset_q19
    #define fprm_CAM1_Supply_Gain            gFram_Cal_Data.slAN13_Gain_q19
    #define fprm_CAM1_Supply_Offset          gFram_Cal_Data.slAN13_Offset_q19
    #define fprm_CAM2_Supply_Gain            gFram_Cal_Data.slAN14_Gain_q19
    #define fprm_CAM2_Supply_Offset          gFram_Cal_Data.slAN14_Offset_q19
    #define fprm_CAM3_Supply_Gain            gFram_Cal_Data.slAN15_Gain_q19
    #define fprm_CAM3_Supply_Offset          gFram_Cal_Data.slAN15_Offset_q19
    #define fprm_Spare_CH9_Gain              gFram_Cal_Data.slAN9_Gain_q19
    #define fprm_Spare_CH9_Offset            gFram_Cal_Data.slAN9_Offset_q19
    #define fprm_Spare_CH12_Gain             gFram_Cal_Data.slAN12_Gain_q19
    #define fprm_Spare_CH12_Offset           gFram_Cal_Data.slAN12_Offset_q19

    #define fprm_AutoCal_Gain                gFram_Cal_Data.slAutoGain_q19
    #define fprm_AutoCal_Offset              gFram_Cal_Data.slAutoOffsetCounts
    #define fprm_Factory_Cal_Version         gFram_Cal_Data.uwVersion

    // Define the locations within the fram chip for user storage
    #define FRAM_SIZE_CHECK_ADDR    0x70
    #define FRAM_SIZE_CHECK_LEN     1

    #define FRAM_SIZE_CHECK_HIGH_ADDR (FRAM_SIZE_CHECK_ADDR + FRAM_SIZE/2)
    #define FRAM_SIZE_CHECK_HIGH_LEN 1

    #define CAL_CRC_ADDR    0x71       /* uses 1 word */
    #define CAL_CRC_LEN     1

    #define CAL_TABLE_ADDR  0x72
    #define CAL_TABLE_LEN (sizeof(FRAM_CAL_TABLE_TYPE))

    #define EVENTS_ADDR            (CAL_TABLE_ADDR + CAL_TABLE_LEN + 2)
    #define EVENTS_LEN             (sizeof(LOCAL_EVENTS_TYPE))

    #define RESET_COUNTS_ADDR      EVENTS_ADDR + EVENTS_LEN + 2
    #define RESET_COUNTS_LEN       (sizeof(ulong))

    #define SETUP_CRC_ADDR          RESET_COUNTS_ADDR + RESET_COUNTS_LEN + 2       /* uses 1 word */
    #define SETUP_CRC_LEN           1

    #define SETUP_TABLE_ADDR       SETUP_CRC_ADDR + SETUP_CRC_LEN + 2
    #define SETUP_TABLE_LEN        (sizeof(FRAM_SETUP_TABLE_TYPE))

    //#define MONITORS_ADDR          SETUP_TABLE_ADDR + SETUP_TABLE_LEN + 2
    //#define MONITORS_LEN           (sizeof(LOCAL_CYCLE_COUNTERS_TYPE))

    #define HOUR_METERS_ADDR        SETUP_TABLE_ADDR + SETUP_TABLE_LEN + 2
    #define HOUR_METERS_LEN         (sizeof(LOCAL_HOUR_METER_TYPE))

    #define SAVED_HOUR_METERS_ADDR  (HOUR_METERS_ADDR + HOUR_METERS_LEN + 20)
    #define SAVED_HOUR_METERS_LEN   (sizeof(LOCAL_HOUR_METER_TYPE))

    #define CAL_BACKUP_STATE_ADDR_8K   0x0C00
    #define CAL_BACKUP_STATE_ADDR_32K  0x3000
    #define CAL_BACKUP_STATE_LEN       1

    #define CAL_BACKUP_CRC_ADDR_8K    0x0C01       /* uses 1 word */
    #define CAL_BACKUP_CRC_ADDR_32K   0x3001
    #define CAL_BACKUP_CRC_LEN        1

    #define CAL_BACKUP_ADDR_8K        (CAL_BACKUP_CRC_ADDR_8K + CAL_BACKUP_CRC_LEN)
    #define CAL_BACKUP_ADDR_32K       (CAL_BACKUP_CRC_ADDR_32K + CAL_BACKUP_CRC_LEN)
    #define CAL_BACKUP_LEN          (sizeof(FRAM_CAL_TABLE_TYPE))

    #define FRAM_LOG_ADDR  SAVED_HOUR_METERS_ADDR + SAVED_HOUR_METERS_LEN + 2
    #define FRAM_LOG_LEN_8K   (CAL_BACKUP_STATE_ADDR_8K - FRAM_LOG_ADDR - 128)
    #define FRAM_LOG_LEN_32K  (CAL_BACKUP_STATE_ADDR_32K - FRAM_LOG_ADDR - 128)

    #define STRING_SIZE 16u

#endif
#endif

#ifdef FRAM_MODULE

    #ifndef _fram_h_local
    #define _fram_h_local 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Defines

    // Delay Times

    //Funciton prototypes
    void vInit_Fram_Data(void);

    void vServiceFram( void );
    uword uwGetFRAM_Cal_CRC(void);
    void vPutFRAM_Cal_CRC(uword CRC);
    void vGetFRAM_Cal_Data(volatile FRAM_CAL_TABLE_TYPE *);
    void vPutFRAM_Cal_Data(volatile FRAM_CAL_TABLE_TYPE *);

    uword uwGetFRAM_Setup_CRC(void);
    void vPutFRAM_Setup_CRC(uword CRC);
    void vGetFRAM_Setup_Data(volatile FRAM_SETUP_TABLE_TYPE *);
    void vPutFRAM_Setup_Data(volatile FRAM_SETUP_TABLE_TYPE *);

    void vGetFRAM_Events(volatile LOCAL_EVENTS_TYPE *);
    void vWriteFRAM_Events(volatile LOCAL_EVENTS_TYPE *);
    LOCAL_EVENTS_TYPE buf;
    volatile uword address;
    volatile uword num_words;
    ulong  length;
    uword  uwCRC;
    uword  uwCS;
    uword uwStartCRC;
    uword uwChecksum;

    void vGetHourMeters(volatile LOCAL_HOUR_METER_TYPE *);
    void vWriteHourMeters(volatile LOCAL_HOUR_METER_TYPE *);

    uword uwFRAM_Size_Check(void);
    void vGetResetCounts(ulong *Value);
    void vPutResetCounts(ulong Value);

    // Global variables
    ubyte gubCalUpdated;
    ubyte gubSetupUpdated;

    volatile FRAM_CAL_TABLE_TYPE gFram_Cal_Data;
    volatile FRAM_SETUP_TABLE_TYPE gFram_Setup_Data;
    LOCAL_HOUR_METER_TYPE gHourMeters;
    LOCAL_HOUR_METER_TYPE gCorruptedHourMeters;         // saved meters if CRC and CS is bad on primary

    volatile ubyte gubEventsUpdated;
    volatile ubyte gubHourMeterSaveFlag;
    ubyte gubVoidTheCalFRAM;
    ubyte gubLoadTheCalFRAM;
    ubyte gubVoidTheSetupFRAM;
    ubyte gubLoadTheSetupFRAM;

    uword guwFRAM_CRC;
    uword guwFRAM_Setup_CRC;
    uword guwFRAM_Log_Length;
    uword guwFRAM_CAL_Backup_State_Addr;
    uword guwFRAM_CAL_Bakup_CRC_Addr;
    uword guwFRAM_CAL_Backup_Addr;
    boolean gfReportCalibrationTableFault;

    ulong gulResetCounts;
    volatile TRUCK_VOLT_TYPE geTruckVoltage;

    volatile boolean gfPowerDownLock;
    volatile ubyte gszModulePartNumber[STRING_SIZE];
    volatile ubyte gszHardwarePartNumber[STRING_SIZE];
    volatile FRAM_PROT_CAL_COMMANDS geFRAM_Prot_Cal_Cmd;
    // Macros



    #endif


#else
    #ifndef _fram_h
    #define _fram_h 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/

    // Externaly visable function prototypes


    extern void vInit_Fram_Data(void);
    extern void vServiceFram( void );
    extern uword uwGetFRAM_Cal_CRC(void);
    extern void vPutFRAM_Cal_CRC(uword CRC);
    extern void vGetFRAM_Cal_Data(volatile FRAM_CAL_TABLE_TYPE *);
    extern void vPutFRAM_Cal_Data(volatile FRAM_CAL_TABLE_TYPE *);
    extern void vGetFRAM_Events(volatile LOCAL_EVENTS_TYPE *);
    extern void vWriteFRAM_Events(volatile LOCAL_EVENTS_TYPE *);
    extern void vGetResetCounts(ulong *Value);
    extern void vPutResetCounts(ulong Value);
    extern uword uwGetFRAM_Setup_CRC(void);
    extern void vPutFRAM_Setup_CRC(uword CRC);
    extern void vGetFRAM_Setup_Data(volatile FRAM_SETUP_TABLE_TYPE *);
    extern void vPutFRAM_Setup_Data(volatile FRAM_SETUP_TABLE_TYPE *);
    void vClearCalBackup(void);
    void vCloneCalibration(void);

    // Externaly visable global variables
    extern ubyte gubPerfLevel;
    extern ubyte gubCalUpdated;
    extern ubyte gubSetupUpdated;
    extern ubyte gubPerfLevelUpdated;
    extern ubyte gubVoidTheSetupFRAM;
    extern ubyte gubLoadTheSetupFRAM;

    extern ubyte gubVoidTheCalFRAM;
    extern ubyte gubLoadTheCalFRAM;

    extern volatile FRAM_CAL_TABLE_TYPE gFram_Cal_Data;
    extern volatile FRAM_SETUP_TABLE_TYPE gFram_Setup_Data;
    extern LOCAL_HOUR_METER_TYPE gHourMeters;
    extern LOCAL_HOUR_METER_TYPE gCorruptedHourMeters;         // saved meters if CRC and CS is bad on primary

    extern volatile ubyte gubEventsUpdated;
    extern volatile ubyte gubHourMeterSaveFlag;
    extern ubyte gubHourMetersUpdated;
    extern uword guwFRAM_CRC;
    extern uword guwFRAM_Log_Length;
    extern uword guwFRAM_CAL_Backup_State_Addr;
    extern uword guwFRAM_CAL_Bakup_CRC_Addr;
    extern uword guwFRAM_CAL_Backup_Addr;
    extern boolean gfReportCalibrationTableFault;
    extern ulong gulResetCounts;
    extern volatile TRUCK_VOLT_TYPE geTruckVoltage;
    extern volatile boolean gfPowerDownLock;
    extern volatile ubyte gszModulePartNumber[STRING_SIZE];
    extern volatile ubyte gszHardwarePartNumber[STRING_SIZE];
    extern volatile FRAM_PROT_CAL_COMMANDS geFRAM_Prot_Cal_Cmd;

    #endif
#endif


