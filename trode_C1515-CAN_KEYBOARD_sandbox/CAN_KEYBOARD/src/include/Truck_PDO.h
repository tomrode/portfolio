/**
 *  @file                   Truck_PDO.h
 *  @brief                  Defines the data structures and variables exchanged via CANopen PDO
 *  @copyright              2003 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   7/02/2003
 *
 *  @remark Author:         Bill McCroskey, Tom Rode
 *  @remark Project Tree:   C1515
 *
  *  @note                  Updated for CAN Keyboard project 11/15/2014
 */

#include "types.h"
#include "SetupMaps.h"
#include "Truck_CAN.h"
#include "utf_8.h"
#ifndef TruckPDORegistration

/*========================================================================*
 *  SECTION - Global definitions
 *========================================================================*
 */
 
/** - RX PDO's */
/** @def   DATALOG_REQ_IDX 
 *  @brief IDX DATALOG log request
 */
#define DATALOG_REQ_IDX      0u

/** @def   DM_PDO_IDX
 *  @brief Dm DATALOG Log request
 */
#define DM_PDO_IDX           1u

/** @def   SVCM_PDO3_IDX
 *  @brief SVCM DATALOG Log request
 */
#define SVCM_PDO3_IDX        2u

/** @def   CAN_KYBD_RX_IDX 
 *  @brief Keyboard DATALOG Log request
 */
#define CAN_KYBD_RX_IDX      3u

/** - TX PDO's */
/** @def   DATALOG_PKT4_IDX 
 *  @brief IDX DATALOG packet 4 
 */
#define DATALOG_PKT4_IDX     1u

/** @def   DATALOG_PKT3_IDX 
 *  @brief IDX DATALOG packet 4 
 */
#define DATALOG_PKT3_IDX     3u

/** @def   DATALOG_PKT2_IDX 
 *  @brief IDX DATALOG packet 2 
 */
#define DATALOG_PKT2_IDX     3u

/** @def   DATALOG_PKT1_IDX 
 *  @brief IDX DATALOG packet 1 
 */
#define DATALOG_PKT1_IDX     4u

/** @def   ERROR_PDO_IDX
 *  @brief IDX Error DATALOG  
 */
#define ERROR_PDO_IDX        5u

/** @def   CAN_KYBD_TXMSG
 *  @brief Keyboard PDO receive callback  
 */
#define CAN_KYBD_TXMSG       6u

 /** @def   STRING_SIZE
 *  @brief  FRAM Manufacturing date string size    
 */   
#define STRING_SIZE          16u   /**< Event control bits in contol word */

/** @struct  datalog_req 
 *  @brief   data log 
 *
 *  @typedef datalog_req_t
 *  @brief   data log capture 
 */
typedef struct datalog_req
{
    ubyte ubModule;
    ubyte ubDatalog;
    ubyte ubOption;
} datalog_req_t;

typedef struct
{
    ubyte ubData[8];
} DATA_LOG_TYPE;

/** @struct  keyboard_data 
 *  @brief   Transmit PDO payload
 *
 *  @typedef keyboard_data_t
 *  @brief   Keyboard transmit PDO payload  
 */
typedef struct keyboard_data
{
    ubyte ubData[7];
} keyboard_data_t;
   
/** @struct  keyboard_data_rcv 
 *  @brief   Recieve PDO payload
 *
 *  @typedef keyboard_data_rcv_t
 *  @brief   Keyboard recieve PDO payload  
 */
typedef struct keyboard_data_rcv
{
    ubyte ubData;
} keyboard_data_rcv_t; 

/** @struct  error 
 *  @brief   error events
 *
 *  @typedef error_t
 *  @brief   Error objects  
 */
typedef struct error
{
    uword uwErrorCode;
    uword uwEventNumber;
    ubyte ubErrorRegister;
    ubyte ubSpare[3];
} error_t;

/** @struct  dm_data
 *  @brief   dm data 
 *
 *  @typedef dm_data_t
 *  @brief   dm data
 */
typedef struct dm_data
{
    ubyte ubFreshnessCounter;
    ubyte ubOSM_State;
    ubyte ubByte1;
    ubyte ubTestOutputCmd;
    ubyte ubAppSpecificFlags;
    uword uwControlWord;
} dm_data_t;

/** @struct  event_control
 *  @brief   event list
 *
 *  @typedef event_control_t
 *  @brief   Event control bits in contol word
 */

typedef enum event_control
{
    eDISABLE_FULL_SPEED_HOIST      = (int)0xFFFD,
    eDISABLE_LOWER                 = (int)0xFF7F,
    eDISABLE_ACCY1                 = (int)0xFFFB,
    eDISABLE_ACCY2                 = (int)0xFFF7,
    eDISABLE_FULL_TRACTION_E_STEER = (int)0xFFEF,
    eDISABLE_TILT                  = (int)0xFFDF,
    eDISABLE_LIMP_TRAVEL_E_STEER   = (int)0xFFBF,
    eDISABLE_RAISE                 = (int)0xFEFF,
    eDISABLE_FULL_TRACTION_H_STEER = (int)0xFDFF,
    eDISABLE_LIMP_TRAVEL_H_STEER   = (int)0xFBFF,
    eDISABLE_STEERING              = (int)0xF7FF,
    eSTOP_AND_DROP_ED              = (int)0xDFFF,
    eDISABLE_ED                    = (int)0xBFFF,
    eDISABLE_MODULE                = (int)0x7FFF,
    eDISABLE_NONE                  = (int)0xFFFF,
    eDISABLE_ACCY123               = (int)(eDISABLE_ACCY1 & eDISABLE_ACCY2),
    eDISABLE_ALL_ACCY              = (int)(eDISABLE_ACCY1 & eDISABLE_ACCY2 & eDISABLE_TILT),
    eDISABLE_HOIST                 = (int)( eDISABLE_RAISE & eDISABLE_LOWER ),
    eDISABLE_ALL_LOCAL      = (int)( eDISABLE_HOIST & eDISABLE_ALL_ACCY & eDISABLE_ED & eDISABLE_STEERING & eDISABLE_LIMP_TRAVEL_H_STEER & eDISABLE_FULL_TRACTION_H_STEER & eDISABLE_FULL_TRACTION_E_STEER & eDISABLE_LIMP_TRAVEL_E_STEER),
    eDISABLE_ALL_BUT_ED     = (int)( eDISABLE_HOIST & eDISABLE_ALL_ACCY & eDISABLE_LIMP_TRAVEL_H_STEER & eDISABLE_FULL_TRACTION_H_STEER & eDISABLE_FULL_TRACTION_E_STEER & eDISABLE_LIMP_TRAVEL_E_STEER),
    eDISABLE_H_STEER_ALL_BUT_ED     = (int)( eDISABLE_HOIST & eDISABLE_ALL_ACCY & eDISABLE_LIMP_TRAVEL_H_STEER & eDISABLE_FULL_TRACTION_H_STEER),
    eDISABLE_ALL_AND_STOP   = (int)( eDISABLE_ALL_BUT_ED & eSTOP_AND_DROP_ED),
    eDISABLE_ALL_BUT_STEER_AND_STOP = (int)(eDISABLE_LIMP_TRAVEL_H_STEER & eDISABLE_FULL_TRACTION_H_STEER & eDISABLE_FULL_TRACTION_E_STEER & eDISABLE_LIMP_TRAVEL_E_STEER & eSTOP_AND_DROP_ED),
    eDISABLE_TRACTION       = (int)( eDISABLE_LIMP_TRAVEL_H_STEER & eDISABLE_FULL_TRACTION_H_STEER & eDISABLE_FULL_TRACTION_E_STEER & eDISABLE_LIMP_TRAVEL_E_STEER ),
    eDISABLE_FULL_TRACTION  = (int)( eDISABLE_FULL_TRACTION_H_STEER & eDISABLE_FULL_TRACTION_E_STEER ),
    eDISABLE_STEERING_TRACTION = (int)( eDISABLE_STEERING & eDISABLE_LIMP_TRAVEL_H_STEER & eDISABLE_FULL_TRACTION_H_STEER & eDISABLE_FULL_TRACTION_E_STEER & eDISABLE_LIMP_TRAVEL_E_STEER )
} event_control_t; 
 
/** @struct  vcm_pdo3
 *  @brief   vcm pdo's
 *
 *  @typedef vcm_pdo3_t
 *  @brief   Pdo's sent to VCM
 */
typedef struct
{
    ubyte ubStatus;
    uword uwLoadWeight;
    uword uwForkHeight;
    sbyte sbSpeed;
    ubyte ubStatus2;
    ubyte ubBDI;
} vcm_pdo3_t;

  
    /*
     * Place to store unused PDO data
     */
/** @struct  junk
 *  @brief   unused pdo's
 *
 *  @typedef junk_t
 *  @brief  All unused pdo's
 */
typedef struct
{
    ubyte ubJunk;
    uword uwJunk;
    ulong ulJunk;
}junk_t;

#endif

#ifdef TruckPDOFile

    #ifndef TruckPDOLocalReg
    #define TruckPDOLocalReg 1

/*========================================================================*
 *  SECTION - Internal Function prototypes                                *
 *========================================================================*
 */
    /** - Definitions */

   
void vInit_Object_Dictionary(void);
void vSend_Datalog_Pkt1( DATA_LOG_TYPE *ubdata);
void vSend_Datalog_Pkt2( DATA_LOG_TYPE *ubdata);
void vSend_Datalog_Pkt3( DATA_LOG_TYPE *ubdata);
void vSend_Datalog_Pkt4( DATA_LOG_TYPE *ubdata);
void vSend_Error_TX(boolean fActive, uword uwEvent );

/*========================================================================*
 *  SECTION - global variables                                            *
 *========================================================================*
 */

volatile datalog_req_t  gDatalog_Req;
volatile DATA_LOG_TYPE  gDatalog1;
volatile DATA_LOG_TYPE  gDatalog2;
volatile DATA_LOG_TYPE  gDatalog3;
volatile DATA_LOG_TYPE  gDatalog4;
volatile boolean        gfDatalog1_TX;
volatile boolean        gfDatalog2_TX;
volatile boolean        gfDatalog3_TX;
volatile boolean        gfDatalog4_TX;
volatile boolean        gfError_TX;
volatile boolean        gfError2_TX;
volatile boolean        gfSendPDO1;
volatile boolean        gfSendPDO2;
volatile boolean        gfSendPDO3;
volatile boolean        gfSendPDO4;
volatile uword          guwOsmErrors;
volatile char           szOdmSfwrPn[STRING_SIZE];
volatile uword          guwMyCAN_ID;
volatile junk_t         gUnused;                 /* For unused bytes in receive  PDOs */
volatile junk_t         gZeros;                  /* For unused bytes in transmit PDOs */
volatile dm_data_t      gDM_Data;
volatile vcm_pdo3_t     gVcm_Data;
error_t gError_Data;
volatile ulong ulDataRequestBits[DR_MAX];
volatile uword guwLocalControlWord;
volatile ubyte gubLastFreshnessCounter;           /* tracks change in PDO byte */ 
volatile ubyte gubOsmLocalState = LOSM_STARTUP_INIT;
volatile ubyte gubAsmReadyForOperation = FALSE;
volatile uword guwTickMs;
volatile ubyte gubOsmMasterState = OSM_INIT;
volatile ubyte gubWatchdogCmd = 0;
ulong    gulSetup_Requests[DR_MAX];

#endif


#else
    #ifndef TruckPDORegistration
    #define TruckPDORegistration 1
/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */
  
extern void vInit_Object_Dictionary(void);
extern void vSend_Datalog_Pkt1( DATA_LOG_TYPE *ubdata);
extern void vSend_Datalog_Pkt2( DATA_LOG_TYPE *ubdata);
extern void vSend_Datalog_Pkt3( DATA_LOG_TYPE *ubdata);
extern void vSend_Datalog_Pkt4( DATA_LOG_TYPE *ubdata);
extern void vSend_Error_TX(boolean fActive, uword uwEvent );

/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */
extern volatile datalog_req_t       gDatalog_Req;
extern volatile DATA_LOG_TYPE       gDatalog1;
extern volatile DATA_LOG_TYPE       gDatalog2;
extern volatile DATA_LOG_TYPE       gDatalog3;
extern volatile DATA_LOG_TYPE       gDatalog4;
extern volatile keyboard_data_t     TpdoCanTableOutput;
extern volatile keyboard_data_rcv_t gRpdoCanLanguageSelectInput;
extern volatile boolean             gfDatalog1_TX;
extern volatile boolean             gfDatalog2_TX;
extern volatile boolean             gfDatalog3_TX;
extern volatile boolean             gfDatalog4_TX;
extern volatile boolean             gfSendPDO1;
extern volatile boolean             gfSendPDO2;
extern volatile boolean             gfSendPDO3;
extern volatile boolean             gfSendPDO4;
extern volatile boolean             gfPWM_Debug;
extern volatile boolean             gfError_TX;
extern volatile boolean             gfError2_TX;
extern volatile ulong               ulDataRequestBits[DR_MAX];
extern volatile uword               guwMyCAN_ID;
extern volatile junk_t              gUnused;                      /* For unused bytes in receive  PDOs */
extern volatile junk_t              gZeros;                       /* For unused bytes in transmit PDOs */
extern volatile dm_data_t           gDM_Data;
extern volatile vcm_pdo3_t          gVcm_Data;
extern error_t                      gError_Data;
extern volatile uword               guwLocalControlWord;
extern volatile ubyte               gubLastFreshnessCounter;      /* tracks change in PDO byte */
extern volatile ubyte               gubOsmLocalState;
extern volatile ubyte               gubAsmReadyForOperation;
extern volatile uword               guwTickMs;
extern volatile ubyte               gubOsmMasterState;
extern volatile uword               guwOsmErrors;
extern volatile char                szOdmSfwrPn[STRING_SIZE];
extern volatile ubyte               gubWatchdogCmd;
extern ulong                        gulSetup_Requests[DR_MAX];

    #endif
#endif

