/******************************************************************************
 *  File:       SetupMaps.h
 *
 *  Purpose:    Defines truck setup values shared between truck modules. 
 *
 *  Project:    C584
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     David J. Obringer
 *
 *  Revision History:
 *              Written 02/08/2005
 *              Revised 
 *
 ******************************************************************************
*/
#ifndef setupmaps_registration
   /****************************************************************************
    *                                                                          *
    *  G l o b a l   S c o p e   S e c t i o n                                 *
    *                                                                          *
    ***************************************************************************/
   
    // Defines
   
    typedef enum
    {
        DR_GROUP1  = 0,
        DR_GROUP2  = 1,
        DR_GROUP3  = 2,
        DR_GROUP4  = 3,
        DR_GROUP5  = 4,
        DR_GROUP6  = 5,
        DR_GROUP7  = 6,
        DR_MAX     = 7
    }DR_TYPE;

    /*  Request bits used to request data from OSM master. This is used as an
     *  index into the ulDataRequestBits[] array which is mapped to object
     *  OBJ_V2_SETUP_DATA_REQ in the object dictionary. If this section changes
     *  be sure to update the object range table in OsmMasterUser.h.
     */

    /* Request bits for features data subindex 1 - DR_GROUP1 */
    #define REQ_GROUP1_SPARE1                0x00000001 /*    1   */  
    #define REQ_FTR_TRK_VOLT                 0x00000002 /*    2   */  
    #define REQ_FTR_MAST_TYPE                0x00000004 /*    3   */  
    #define REQ_FTR_CLPSD_HGT                0x00000008 /*    4   */  
    #define REQ_FTR_LIFT_HGT                 0x00000010 /*    5   */  
    #define REQ_FTR_LANGUAGE                 0x00000020 /*    6   */  
    #define REQ_FTR_MODEL_TYPE               0x00000040 /*    7   */  
    #define REQ_FTR_TRK_UNITS                0x00000080 /*    8   */  
    #define REQ_FTR_USER_PERF                0x00000100 /*    9   */  
    #define REQ_GROUP1_SPARE2                0x00000200 /*   10   */  
    #define REQ_GROUP1_SPARE3                0x00000400 /*   11   */  
    #define REQ_FTR_BATT_BOX                 0x00000800 /*   12   */  
    #define REQ_FTR_TRK_WGT                  0x00001000 /*   13   */  
    #define REQ_FTR_MAX_LOAD                 0x00002000 /*   14   */  
    #define REQ_FTR_LIFT_MOTOR               0x00004000 /*   15   */  
    #define REQ_FTR_WIRE_GUIDE               0x00008000 /*   16   */  
    #define REQ_FTR_EAC_SENSE                0x00010000 /*   17   */  
    #define REQ_FTR_BATT_RETAIN              0x00020000 /*   18   */  
    #define REQ_FTR_TRVL_ALARM               0x00040000 /*   19   */  
    #define REQ_FTR_TRVL_ALRM_HGT            0x00080000 /*   20   */  
    #define REQ_FTR_LOWER_ALARM              0x00100000 /*   21   */  
    #define REQ_FTR_TRK_LOCKOUT              0x00200000 /*   22   */  
    #define REQ_FTR_PIN_TIMEOUT              0x00400000 /*   23   */  
    #define REQ_FTR_HOUR_MTR_MSG             0x00800000 /*   24   */  
    #define REQ_FTR_OPT_COLD_COND            0x01000000 /*   25   */  
    #define REQ_FTR_OPT_ROLLOFF              0x02000000 /*   26   */  
    #define REQ_FTR_OPT_PED_DET              0x04000000 /*   27   */  
    #define REQ_FTR_MOTOR_MONITOR            0x08000000 /*   28   */  
    #define REQ_FTR_WIRE_SENSE               0x10000000 /*   29   */  
    #define REQ_FTR_MAN_SENSE_FT             0x20000000 /*   30   */  
    #define REQ_FTR_MSG_MODE                 0x40000000 /*   31   */  
    #define REQ_FTR_REGION                   0x80000000 /*   32   */  
    
    /* Request bits for features data subindex 1 - DR_GROUP2  */
    #define REQ_FTR_ZONESW                   0x00000001 /*    1   */   
    #define REQ_P1_TRVL_ACCEL                0x00000002 /*    2   */   
    #define REQ_P1_GUIDE_SPD_CURV            0x00000004 /*    3   */   
    #define REQ_P1_GUIDE_MAX_SPD_FF          0x00000008 /*    4   */   
    #define REQ_P1_GUIDE_MAX_SPD_PUF         0x00000010 /*    5   */   
    #define REQ_P1_FR_SPD_CURV               0x00000020 /*    6   */   
    #define REQ_P1_FR_MAX_SPD_FF             0x00000040 /*    7   */   
    #define REQ_P1_FR_MAX_SPD_PUF            0x00000080 /*    8   */   
    #define REQ_P2_TRVL_ACCEL                0x00000100 /*    9   */   
    #define REQ_P2_GUIDE_SPD_CURV            0x00000200 /*   10   */   
    #define REQ_P2_GUIDE_MAX_SPD_FF          0x00000400 /*   11   */   
    #define REQ_P2_GUIDE_MAX_SPD_PUF         0x00000800 /*   12   */   
    #define REQ_P2_FR_SPD_CURV               0x00001000 /*   13   */   
    #define REQ_P2_FR_MAX_SPD_FF             0x00002000 /*   14   */   
    #define REQ_P2_FR_MAX_SPD_PUF            0x00004000 /*   15   */   
    #define REQ_P3_TRVL_ACCEL                0x00008000 /*   16   */   
    #define REQ_P3_GUIDE_SPD_CURV            0x00010000 /*   17   */   
    #define REQ_P3_GUIDE_MAX_SPD_FF          0x00020000 /*   18   */   
    #define REQ_P3_GUIDE_MAX_SPD_PUF         0x00040000 /*   19   */   
    #define REQ_P3_FR_SPD_CURV               0x00080000 /*   20   */   
    #define REQ_P3_FR_MAX_SPD_FF             0x00100000 /*   21   */   
    #define REQ_P3_FR_MAX_SPD_PUF            0x00200000 /*   22   */   
    #define REQ_PERF_PLUG                    0x00400000 /*   23   */   
    #define REQ_PERF_COAST_THROTTLE          0x00800000 /*   24   */   
    #define REQ_PERF_BRAKE_FF                0x01000000 /*   25   */   
    #define REQ_PERF_BRAKE_PUF               0x02000000 /*   26   */   
    #define REQ_PERF_FREE_HGT_1MPH           0x04000000 /*   27   */   
    #define REQ_PERF_FREE_HGT_0MPH           0x08000000 /*   28   */   
    #define REQ_PERF_GUIDE_HGT_1MPH          0x10000000 /*   29   */   
    #define REQ_PERF_GUIDE_HGT_0MPH          0x20000000 /*   30   */   
    #define REQ_PERF_GUIDE_ACQ_SPD           0x40000000 /*   31   */   
    #define REQ_PERF_BDI                     0x80000000 /*   32   */   

    /* Request bits for features data subindex 1 - DR_GROUP3 */
    #define REQ_PERF_EAC_MODE                0x00000001  /*   1   */   
    #define REQ_PERF_EAC_FF_SLOW_SPD         0x00000002  /*   2   */    
    #define REQ_PERF_EAC_FF_D_RATE           0x00000004  /*   3   */    
    #define REQ_PERF_EAC_FF_RES_DST          0x00000008  /*   4   */    
    #define REQ_PERF_EAC_FF_STOP_DST         0x00000010  /*   5   */    
    #define REQ_PERF_EAC_PUF_SLOW_SPD        0x00000020  /*   6   */    
    #define REQ_PERF_EAC_PUF_D_RATE          0x00000040  /*   7   */    
    #define REQ_PERF_EAC_PUF_RES_DST         0x00000080  /*   8   */    
    #define REQ_PERF_EAC_PUF_STOP_DST        0x00000100  /*   9   */    
    #define REQ_PERF_MAINR_MAX_SPD           0x00000200  /*  10   */    
    #define REQ_PERF_MAINR_ACCEL             0x00000400  /*  11   */    
    #define REQ_PERF_MAINR_DECEL             0x00000800  /*  12   */    
    #define REQ_PERF_MAINL_MAX_SPD           0x00001000  /*  13   */    
    #define REQ_PERF_MAINL_ACCEL             0x00002000  /*  14   */    
    #define REQ_PERF_MAINL_DECEL             0x00004000  /*  15   */    
    #define REQ_PERF_AUXR_MAX_SPD            0x00008000  /*  16   */    
    #define REQ_PERF_AUXR_ACCEL              0x00010000  /*  17   */    
    #define REQ_PERF_AUXR_DECEL              0x00020000  /*  18   */    
    #define REQ_PERF_AUXL_MAX_SPD            0x00040000  /*  19   */    
    #define REQ_PERF_AUXL_ACCEL              0x00080000  /*  20   */    
    #define REQ_PERF_AUXL_DECEL              0x00100000  /*  21   */    
    #define REQ_PERF_PDS_FF_MAX_SPD          0x00200000  /*  22   */    
    #define REQ_PERF_PDS_FF_SLOW_SPD         0x00400000  /*  23   */    
    #define REQ_PERF_PDS_FF_D_RATE           0x00800000  /*  24   */    
    #define REQ_PERF_PDS_PUF_MAX_SPD         0x01000000  /*  25   */    
    #define REQ_PERF_PDS_PUF_SLOW_SPD        0x02000000  /*  26   */    
    #define REQ_PERF_PDS_PUF_D_RATE          0x04000000  /*  27   */    
    #define REQ_GROUP3_SPARE1                0x08000000  /*  28   */
    #define REQ_GROUP3_SPARE2                0x10000000  /*  29   */
    #define REQ_GROUP3_SPARE3                0x20000000  /*  30   */
    #define REQ_GROUP3_SPARE4                0x40000000  /*  31   */
    #define REQ_GROUP3_SPARE5                0x80000000  /*  32   */
    
    /* Request bits for features data subindex 1 - DR_GROUP4 */
    #define REQ_GROUP4_SPARE1                0x00000001  /*   1    */     
    #define REQ_CUTOUT1_HEIGHT               0x00000002  /*   2    */  
    #define REQ_CUTOUT1_TYPE                 0x00000004  /*   3    */  
    #define REQ_CUTOUT1_USAGE                0x00000008  /*   4    */  
    #define REQ_CUTOUT1_ZONE                 0x00000010  /*   5    */  
    #define REQ_CUTOUT2_HEIGHT               0x00000020  /*   6    */  
    #define REQ_CUTOUT2_TYPE                 0x00000040  /*   7    */  
    #define REQ_CUTOUT2_USAGE                0x00000080  /*   8    */  
    #define REQ_CUTOUT2_ZONE                 0x00000100  /*   9    */  
    #define REQ_CUTOUT3_HEIGHT               0x00000200  /*  10    */  
    #define REQ_CUTOUT3_TYPE                 0x00000400  /*  11    */  
    #define REQ_CUTOUT3_USAGE                0x00000800  /*  12    */  
    #define REQ_CUTOUT3_ZONE                 0x00001000  /*  13    */  
    #define REQ_CUTOUT4_HEIGHT               0x00002000  /*  14    */  
    #define REQ_CUTOUT4_TYPE                 0x00004000  /*  15    */  
    #define REQ_CUTOUT4_USAGE                0x00008000  /*  16    */  
    #define REQ_CUTOUT4_ZONE                 0x00010000  /*  17    */  
    #define REQ_CUTOUT5_HEIGHT               0x00020000  /*  18    */  
    #define REQ_CUTOUT5_TYPE                 0x00040000  /*  19    */  
    #define REQ_CUTOUT5_USAGE                0x00080000  /*  20    */  
    #define REQ_CUTOUT5_ZONE                 0x00100000  /*  21    */  
    #define REQ_CUTOUT6_HEIGHT               0x00200000  /*  22    */  
    #define REQ_CUTOUT6_TYPE                 0x00400000  /*  23    */  
    #define REQ_CUTOUT6_USAGE                0x00800000  /*  24    */  
    #define REQ_CUTOUT6_ZONE                 0x01000000  /*  25    */  
    #define REQ_FEATURES_CRC                 0x02000000  /*  26    */
    #define REQ_FEATURES_REV                 0x04000000  /*  27    */
    #define REQ_PERFS_CRC                    0x08000000  /*  28    */
    #define REQ_PERFS_REV                    0x10000000  /*  29    */
    #define REQ_CUTOUTS_CRC                  0x20000000  /*  30    */
    #define REQ_CUTOUTS_REV                  0x40000000  /*  31    */
    #define REQ_ALL_SETUPS_CRC               0x80000000  /*  32    */
    
    /* Request bits for features data subindex 1 - DR_GROUP5 */
    #define REQ_ALL_SETUPS_REV               0x00000001  /*   1    */
    #define REQ_FTR_CDM_SELECT               0x00000002  /*   2    */
    #define REQ_FTR_CDM_ZONES                0x00000004  /*   3    */  
    #define REQ_FTR_CDM_HGT1                 0x00000008  /*   4    */  
    #define REQ_FTR_CDM_WGT1                 0x00000010  /*   5    */  
    #define REQ_FTR_CDM_HGT2                 0x00000020  /*   6    */  
    #define REQ_FTR_CDM_WGT2                 0x00000040  /*   7    */  
    #define REQ_FTR_CDM_HGT3                 0x00000080  /*   8    */  
    #define REQ_FTR_CDM_WGT3                 0x00000100  /*   9    */  
    #define REQ_FTR_CDM_HGT4                 0x00000200  /*  10    */  
    #define REQ_FTR_CDM_WGT4                 0x00000400  /*  11    */  
    #define REQ_FTR_USR_CODES                0x00000800  /*  12    */  
    #define REQ_FTR_USR_CODE1_ID             0x00001000  /*  13    */  
    #define REQ_FTR_USR_CODE1_LEV            0x00002000  /*  14    */  
    #define REQ_FTR_USR_CODE2_ID             0x00004000  /*  15    */  
    #define REQ_FTR_USR_CODE2_LEV            0x00008000  /*  16    */  
    #define REQ_FTR_USR_CODE3_ID             0x00010000  /*  17    */  
    #define REQ_FTR_USR_CODE3_LEV            0x00020000  /*  18    */  
    #define REQ_FTR_USR_CODE4_ID             0x00040000  /*  19    */  
    #define REQ_FTR_USR_CODE4_LEV            0x00080000  /*  20    */  
    #define REQ_FTR_USR_CODE5_ID             0x00100000  /*  21    */  
    #define REQ_FTR_USR_CODE5_LEV            0x00200000  /*  22    */  
    #define REQ_FTR_USR_CODE6_ID             0x00400000  /*  23    */  
    #define REQ_FTR_USR_CODE6_LEV            0x00800000  /*  24    */  
    #define REQ_FTR_USR_CODE7_ID             0x01000000  /*  25    */  
    #define REQ_FTR_USR_CODE7_LEV            0x02000000  /*  26    */
    #define REQ_FTR_USR_CODE8_ID             0x04000000  /*  27    */
    #define REQ_FTR_USR_CODE8_LEV            0x08000000  /*  28    */
    #define REQ_FTR_USR_CODE9_ID             0x10000000  /*  29    */
    #define REQ_FTR_USR_CODE9_LEV            0x20000000  /*  30    */
    #define REQ_FTR_USR_CODE10_ID            0x40000000  /*  31    */
    #define REQ_FTR_USR_CODE10_LEV           0x80000000  /*  32    */
    
    /* Request bits for features data subindex 1 - DR_GROUP6 */
    #define REQ_FTR_USR_CODE11_ID            0x00000001  /*   1    */
    #define REQ_FTR_USR_CODE11_LEV           0x00000002  /*   2    */
    #define REQ_FTR_USR_CODE12_ID            0x00000004  /*   3    */  
    #define REQ_FTR_USR_CODE12_LEV           0x00000008  /*   4    */  
    #define REQ_FTR_USR_CODE13_ID            0x00000010  /*   5    */  
    #define REQ_FTR_USR_CODE13_LEV           0x00000020  /*   6    */  
    #define REQ_FTR_USR_CODE14_ID            0x00000040  /*   7    */  
    #define REQ_FTR_USR_CODE14_LEV           0x00000080  /*   8    */  
    #define REQ_FTR_USR_CODE15_ID            0x00000100  /*   9    */  
    #define REQ_FTR_USR_CODE15_LEV           0x00000200  /*  10    */  
    #define REQ_FTR_USR_CODE16_ID            0x00000400  /*  11    */  
    #define REQ_FTR_USR_CODE16_LEV           0x00000800  /*  12    */  
    #define REQ_FTR_USR_CODE17_ID            0x00001000  /*  13    */  
    #define REQ_FTR_USR_CODE17_LEV           0x00002000  /*  14    */  
    #define REQ_FTR_USR_CODE18_ID            0x00004000  /*  15    */  
    #define REQ_FTR_USR_CODE18_LEV           0x00008000  /*  16    */  
    #define REQ_FTR_USR_CODE19_ID            0x00010000  /*  17    */  
    #define REQ_FTR_USR_CODE19_LEV           0x00020000  /*  18    */  
    #define REQ_FTR_USR_CODE20_ID            0x00040000  /*  19    */  
    #define REQ_FTR_USR_CODE20_LEV           0x00080000  /*  20    */  
    #define REQ_FTR_USR_CODE21_ID            0x00100000  /*  21    */  
    #define REQ_FTR_USR_CODE21_LEV           0x00200000  /*  22    */  
    #define REQ_FTR_USR_CODE22_ID            0x00400000  /*  23    */  
    #define REQ_FTR_USR_CODE22_LEV           0x00800000  /*  24    */  
    #define REQ_FTR_USR_CODE23_ID            0x01000000  /*  25    */  
    #define REQ_FTR_USR_CODE23_LEV           0x02000000  /*  26    */
    #define REQ_FTR_USR_CODE24_ID            0x04000000  /*  27    */
    #define REQ_FTR_USR_CODE24_LEV           0x08000000  /*  28    */
    #define REQ_FTR_USR_CODE25_ID            0x10000000  /*  29    */
    #define REQ_FTR_USR_CODE25_LEV           0x20000000  /*  30    */
    #define REQ_GROUP6_SPARE30               0x40000000  /*  31    */
    #define REQ_GROUP6_SPARE31               0x80000000  /*  32    */
    
    /* Request bits for features data subindex 1 - DR_GROUP7 */
    #define REQ_GROUP7_SPARE1                0x00000001  /*   1    */
    #define REQ_GROUP7_SPARE2                0x00000002  /*   2    */
    #define REQ_GROUP7_SPARE3                0x00000004  /*   3    */  
    #define REQ_GROUP7_SPARE4                0x00000008  /*   4    */  
    #define REQ_GROUP7_SPARE5                0x00000010  /*   5    */  
    #define REQ_GROUP7_SPARE6                0x00000020  /*   6    */  
    #define REQ_GROUP7_SPARE7                0x00000040  /*   7    */  
    #define REQ_GROUP7_SPARE8                0x00000080  /*   8    */  
    #define REQ_GROUP7_SPARE9                0x00000100  /*   9    */  
    #define REQ_GROUP7_SPARE10               0x00000200  /*  10    */  
    #define REQ_GROUP7_SPARE11               0x00000400  /*  11    */  
    #define REQ_GROUP7_SPARE12               0x00000800  /*  12    */  
    #define REQ_GROUP7_SPARE13               0x00001000  /*  13    */  
    #define REQ_GROUP7_SPARE14               0x00002000  /*  14    */  
    #define REQ_GROUP7_SPARE15               0x00004000  /*  15    */  
    #define REQ_GROUP7_SPARE16               0x00008000  /*  16    */  
    #define REQ_GROUP7_SPARE17               0x00010000  /*  17    */  
    #define REQ_GROUP7_SPARE18               0x00020000  /*  18    */  
    #define REQ_GROUP7_SPARE19               0x00040000  /*  19    */  
    #define REQ_GROUP7_SPARE20               0x00080000  /*  20    */  
    #define REQ_GROUP7_SPARE21               0x00100000  /*  21    */  
    #define REQ_GROUP7_SPARE22               0x00200000  /*  22    */  
    #define REQ_GROUP7_SPARE23               0x00400000  /*  23    */  
    #define REQ_GROUP7_SPARE24               0x00800000  /*  24    */  
    #define REQ_GROUP7_SPARE25               0x01000000  /*  25    */  
    #define REQ_GROUP7_SPARE26               0x02000000  /*  26    */
    #define REQ_GROUP7_SPARE27               0x04000000  /*  27    */
    #define REQ_GROUP7_SPARE28               0x08000000  /*  28    */
    #define REQ_GROUP7_SPARE29               0x10000000  /*  29    */
    #define REQ_GROUP7_SPARE30               0x20000000  /*  30    */
    #define REQ_GROUP7_SPARE31               0x40000000  /*  31    */
    #define REQ_GROUP7_SPARE32               0x80000000  /*  32    */

    /* Feature setups mapping definitions*/
    typedef enum
    {
        NO_ALARM = 0,  
        FWD_ALARM,
        REV_ALARM,
        BOTH_ALARM
    } ALARM_TYPE;
    
    typedef enum
    {
        LOWER_ALARM_OFF = 0,
        LOWER_ALARM_ON
    } TRVL_ALRM_LWR_TYPE;
    
    typedef enum
    {
        CDM_DISABLED = 0,   
        CDM_ZONE_1,   
        CDM_ZONE_2,   
        CDM_ZONE_3,   
        CDM_ZONE_4   
    } CDM_CONFIG_TYPE;
    
    typedef enum
    {
        WIRE_GUIDE_DISABLED = 0,   
        WIRE_GUIDE_ENABLED
    } WIRE_GUIDE_TYPE;
    
    typedef enum
    {
        SENSE_DISABLED = 0,   
        SENSE_STD,   
        SENSE_SLOW,
        SENSE_STOP
    } MAN_WIRE_SENSE_TYPE;
    
    typedef enum
    {
        EAC_DISABLE = 0,
        EAC_NORTH,
        EAC_SOUTH
    } END_AISLE_TYPE;
    
    typedef enum
    {
        BATT_SW_NO = 0,  
        BATT_SW_YES
    } BATTERY_SWITCH_TYPE;
    
    typedef enum
    {
        ENGLISH = 0,    
        GERMAN,
        FRENCH,
        SPANISH,
        DUTCH,
        ITALIAN
    } LANGUAGE_TYPE;
    
    typedef enum
    {
        USER_PERF_DISABLED = 0,    
        USER_PERF_ENABLED =  1
    } USER_PERF_TYPE;
    
    typedef enum
    {
        USER_CODES_DISABLED = 0,   
        USER_CODES_ENABLED  = 1
    } USER_CODES_TYPE;
    
    typedef enum
    {
        COLD_COND_DISABLED = 0,   
        COLD_COND_ENABLED
    } COLD_CONDITION_TYPE;
    
    typedef enum
    {
        OBS_DETECT_DISABLED = 0,   
        OBS_DETECT_ENABLED
    } OBSTACLE_DETECT_TYPE;
    
    typedef enum
    {
        ROLL_LOADER_DISABLED = 0,   
        ROLL_LOADER_ENABLED
    } ROLL_LOADER_TYPE;
    
    typedef enum
    {
        MOTOR_MONITOR_DISABLED = 0,   
        MOTOR_MONITOR_ENABLED
    } MTR_MONITOR_TYPE;
    
    typedef enum
    {
        ZONE_SW_NO = 0,   
        ZONE_SW_YES
    } ZONE_SWITCH_TYPE;
    
    typedef enum
    {
        DMM_NONE = 0,   
        DMM_TIMER,
        DMM_TRIP,
        DMM_BDI,   
        DMM_HOURS,
        DMM_ODOMETER,
        DMM_SPD,
        DMM_STEER,
        DMM_MODEL,
        DMM_ALL
    } MSG_MODE_TYPE;
    
    typedef enum
    {
        TRK_OP_DISABLED = 0,   
        TRK_OP_ENABLED
    } TRUCK_OPERATION_TYPE;
    
    typedef enum
    {
        HR_MTR_RTIME1 = 0,   
        HR_MTR_RTIME2,
        HR_MTR_ACCESS1,
        HR_MTR_ACCESS3,
        HR_MTR_ACCESS4,
        HR_MTR_ACCESS5,
        HR_MTR_ACCESS6,
        HR_MTR_TRV_MOTOR,
        HR_MTR_LIFT_MOTOR,
        HR_MTR_PLAT_LOWER,
        HR_MTR_ODOMETER
    } HR_METER_SHOW_TYPE;
    
    typedef enum
    {
        BOX_B = 0,      
        BOX_C,
        BOX_D
    } BATTERY_BOX_TYPE;
    
    typedef enum
    {
        MAST_TT = 0,         
        MAST_TL
    } MAST_TYPE;
    
    typedef enum REGION_ENUM
    {
        NORTH_AMERICAN = 0,     /* Default  */  
        EUROPEAN       = 1,
        AUSTRALIA      = 2
    } REGION_TYPE;
    
    typedef enum
    {
        VOLT_24 = 0,
        VOLT_36,
        VOLT_48
    } TRUCK_VOLT_TYPE;

    /**********************************************************************************************/
    /********************************* Truck Model Numbers ****************************************/
    /**********************************************************************************************/
    /* NOTE: If this is changed make sure the macros below are still valid and adjust accordingly */
    /**********************************************************************************************/
    typedef enum        
    {
        SP3505_30_NA = 0,       /* Default  */
        SP3510_30_NA,           /*  1 */
        SP3520_30_NA,           /*  2 */
        SP3522_22_NA,           /*  3 */
        SP3550_30_NA,           /*  4 */
        SP3570_30_NA,           /*  5 */
        SP3580_15_NA,           /*  6 */
        SP3511_1_25_EU,         /*  7 */
        SP3512_1_00_EU,         /*  8 */
        SP3521_1_25_EU,         /*  9 */
        SP3522_1_00_EU,         /* 10 */
        SP3571_1_25_EU,         /* 11 */
        SP3581_62_EU,           /* 12 */
        SP3510_30_AU,           /* 13 */
        SP3520_30_AU,           /* 14 */
        SP3522_22_AU,           /* 15 */
        SP3550_30_AU,           /* 16 */
        SP3570_30_AU,           /* 17 */
        SP3580_15_AU,           /* 18 */
        NUM_OF_TRUCK_MODELS     /* 19 */  /*Last entry keeps total count, first enum must = 0*/
    } TRUCK_MODEL_TYPE;

    // Truck model number macros
    #define gfModel_NorthAmerican() (fToBoolean(   (gDataSet.Data.Features.ubTruckModel == SP3505_30_NA)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3510_30_NA)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3520_30_NA)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3522_22_NA)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3550_30_NA)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3570_30_NA)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3580_15_NA)   \
                                               )                                                           \
                                    )
    #define gfModel_European()      (fToBoolean(   (gDataSet.Data.Features.ubTruckModel == SP3511_1_25_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3512_1_00_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3521_1_25_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3522_1_00_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3571_1_25_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3581_62_EU)   \
                                               )                                                           \
                                    )                                                                      
    #define gfModel_Australian()    (fToBoolean(   (gDataSet.Data.Features.ubTruckModel == SP3510_30_AU)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3520_30_AU)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3522_22_AU)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3550_30_AU)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3570_30_AU)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3580_15_AU)   \
                                               )                                                           \
                                    )                                                                        
    #define gfModel_HiSpeedRaise()  (fToBoolean(   (gDataSet.Data.Features.ubTruckModel == SP3520_30_NA  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3522_22_NA  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3550_30_NA  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3570_30_NA  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3580_15_NA  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3521_1_25_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3522_1_00_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3571_1_25_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3581_62_EU  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3520_30_AU  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3522_22_AU  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3550_30_AU  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3570_30_AU  ) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3580_15_AU  ) \
                                               )                                                           \
                                    )                                                                        
    #define gfModel_LiftingForks()  (fToBoolean(   (gDataSet.Data.Features.ubTruckModel == SP3522_22_NA)   \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3512_1_00_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3522_1_00_EU) \
                                                || (gDataSet.Data.Features.ubTruckModel == SP3522_22_AU)   \
                                               )                                                           \
                                    )

    typedef enum
    {
        FORKS_TELESCOPIC = 0,
        FORKS_FIXED,
        FORKS_TILTING
    } FORK_TYPE;
    
    typedef enum
    {
        MOTOR_STD = 0,  
        HIGH_PERF = 1,
        STD_W_FAN = 2
    } LIFT_MOTOR_TYPE;
    
    typedef enum
    {
        METRIC = 0,     
        ENG_MEASURE
    } MEASURE_TYPE;
    
    typedef enum
    {
        FULL = 0,       
        NARROW
    } FRONT_RAIL_TYPE;
    
    typedef enum
    {
        AUX_69 = 0,     
        AUX_114
    } AUX_MAST_TYPE;
    
    typedef enum
    {
        DISABLED = 0,   
        ENABLED
    } OFF_ON_TYPE;
    
    typedef enum            /*Saved as a feature used under utility */
    {
        PVT_TYPE_D2 = 0,   
        PVT_TYPE_D
    } PVT_VALVE_TYPE;
    
    typedef enum            /*   CUTOUTS   */
    {
        CUTOUT_NONE = 0,
        CUTOUT_PCT,
        CUTOUT_PST,
        CUTOUT_NCT
    } CUTOUT_TYPE_TYPE;
    
    typedef enum
    {
        CUTOUT_ALWAYS = 0,
        CUTOUT_FR
    } CUTOUT_WHEN_TYPE;
    
    typedef enum
    {
        CUTOUT_ZONEA = 0,
        CUTOUT_ZONEB,
        CUTOUT_ZONEC,
        CUTOUT_ALL
    } CUTOUT_ZONE_TYPE;
    
    typedef enum
    {
        EAC_SLOWDOWN = 0,
        EAC_SLOWRESUME = 1,
        EAC_SLOWAUTOSTOP = 2,
        EAC_STOPMOTOR = 3,
        EAC_STOPBRAKE = 4
    }PERF_EACMODE_TYPE;
    
#endif  // ifndef _SETUPMAPS_H_

#ifdef SETUPMAPS_MODULE

    #ifndef setupmaps_registration_local
    #define setupmaps_registration_local 1
    
   /****************************************************************************
    *                                                                          *
    *  I n t e r n a l   S c o p e   S e c t i o n                             *
    *                                                                          *
    ****************************************************************************/
   
    // Defines
   
    // Constants
   
    // Global variables
   
    // Macros
   
    #endif

#else

    #ifndef setupmaps_registration
    #define setupmaps_registration 1
   
   /****************************************************************************
    *                                                                          *
    *  E x t e r n a l   S c o p e   S e c t i o n                             *
    *                                                                          *
    ****************************************************************************/
   
    // Externaly visable global variables
   
    // Externaly visable function prototypes
   
    #endif

#endif






