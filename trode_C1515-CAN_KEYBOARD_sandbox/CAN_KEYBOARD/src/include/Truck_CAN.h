/************************************************************************************************
 *  File:       Truck_CAN.h
 *
 *  Purpose:    Defines the data structures and variables exchanged via CANopen
 *
 *  Project:    C1103
 *
 *  Copyright:  2003 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 7/7/2005
 ************************************************************************************************
*/


#ifndef TruckCANRegistration
#define TruckCANRegistration 1

// Definitions for the CAN ID areas
    #define NMT_ID              0
    #define SYNC_ID             0x80
    #define TIME_STAMP_ID       0x100
    #define EMERGENCY_ID        0x80
    #define PDO1_TX             0x180
    #define PDO1_RX             0x200
    #define PDO2_TX             0x280
    #define PDO2_RX             0x300
    #define PDO3_TX             0x380
    #define PDO3_RX             0x400
    #define PDO4_TX             0x480
    #define PDO4_RX             0x500
    #define SDO_TX_ID           0x580
    #define SDO_RX_ID           0x600
    #define NMT_ERROR_CONT_ID   0x700
    #define KYBD_PDO1_TX        0x1F6
    #define KYBD_PDO1_RX        0x276

    // Definitions for CAN IDs for the modules
    #define CIM1_ID    0x4A
    #define CIM2_ID    0x4C
    #define ODM_ID     0x18
    #define SVCM_ID    0x0c
    #define KYBD_ID    0x76

    // Define the PDO ID's
    #define DM_PDO1_TX      (PDO1_TX + ODM_ID)

    // Define the SDO ID's
    #define CIM_SDO_TX  (SDO_TX_ID + CIM_ID)
    #define CIM_SDO_RX  (SDO_RX_ID + CIM_ID)

    #define SVCM_PDO3_TX  (PDO3_TX + SVCM_ID)

    #define CIM_PDO1  (PDO1_TX + CIM_ID)
    #define CIM_PDO2  (PDO2_TX + CIM_ID)
    #define CIM_PDO3  (PDO3_TX + CIM_ID)
    #define CIM_PDO4  (PDO4_TX + CIM_ID)
    #define CIM_RPDO1 (PDO1_RX + CIM_ID)
    #define CIM_RPDO2 (PDO2_RX + CIM_ID)
    #define CIM_RPDO3 (PDO3_RX + CIM_ID)

#endif

