/************************************************************************************************
 * File:       OsmSlave.h
 *
 * Purpose:    Defines the interface for the slave version of the operational
 *             state machine.
 *
 * Project:    C1101
 *
 * Copyright:  2006 Crown Equipment Corp., New Bremen, OH  45869
 *
 * Author:     Walter Conley
 *
 * Revision History:
 *             Written 07/14/2006
 *
 ************************************************************************************************
*/

#include "OsmSlaveUser.h"

#ifndef  osmslave_registration

   /*******************************************************************************
    *                                                                             *
    *  G l o b a l   S c o p e   S e c t i o n                                    *
    *                                                                             *
    *******************************************************************************/

   // Defines
   // Global OSM states from OSM master
   typedef enum
   {
      OSM_VOID    = 0,
      OSM_INIT    = 1,
      OSM_STANDBY = 2,
      OSM_LEVEL1  = 3,
      OSM_LEVEL2  = 4,
      OSM_LEVEL3  = 5,
      OSM_TIMEOUT = 6
   }OSM_STATES;

   // Local OSM states Order is important change with caution
   typedef enum
   {
      LOSM_STARTUP_INIT        =  0,
      LOSM_WAIT_CAN_OPEN       =  1,
      LOSM_START_TASKS         =  2,
      LOSM_WAIT_MASTER_INIT    =  3,
      LOSM_MASTER_INIT         =  4,
      LOSM_WAIT_MASTER_STBY    =  5,
      LOSM_MASTER_STBY         =  6,
      LOSM_STANDBY_WAIT_PDO    =  7,
      LOSM_WAIT_MASTER_LEVEL   =  8,
      LOSM_LEVEL1              =  9,
      LOSM_LEVEL2              = 10,
      LOSM_LEVEL3              = 11,
      LOSM_TIMEOUT             = 12
   }LOSM_STATES;

#endif

#ifdef OSMSLAVE_MODULE

   #ifndef osmslave_registration_local
   #define osmslave_registration_local 1

   /*******************************************************************************
    *                                                                             *
    *  I n t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Defines


   // Function prototypes
   void  vProcessCanOpenStack(void);
   void  vNullTaskProcesses(void);
   boolean gfOsmCommunicationEstablished(void );

   // Global variables
   ulong ulDeltaUs;
   ulong ulPdoTimeoutUs;
   volatile ubyte ubOsmGlobalState;
   volatile boolean gfOSM_AutoAdvanceMode = TRUE;
   // Macros

   #endif


#else
   #ifndef osmslave_registration
   #define osmslave_registration 1
   /*******************************************************************************
    *                                                                             *
    *  E x t e r n a l   S c o p e   S e c t i o n                                *
    *                                                                             *
    *******************************************************************************/

   // Externaly visable global variables

   // Externally visable function prototypes
   extern void    vOsmStateMachine(void);
   extern boolean gfOsmOperationAllowed(void);
   extern boolean gfOsmCommunicationEstablished(void);
   extern boolean gfOsmTestOutputsAllowed(void);
   extern boolean gfOsmNetworkOperational(void);
   extern volatile boolean gfOSM_AutoAdvanceMode;

   #endif
#endif

