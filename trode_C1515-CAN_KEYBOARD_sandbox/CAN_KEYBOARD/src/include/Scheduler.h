/************************************************************************************************
 *  File:       Scheduler.h
 *
 *  Purpose:    Defines the data structures and variables used in the simple task scheduler for
 *              the TI F2800 series DSP
 *
 *  Project:    C1103
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/8/2005
 ************************************************************************************************
*/
// dwt #include "main.h"
#include "types.h"

#ifndef SchedulerRegistration
    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/
    // Definitions


    


#endif

#ifdef SchedulerFile

    #ifndef SchedulerLocalReg
    #define SchedulerLocalReg 1
    
    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Definitions
    #define MAX_SCHEDULED_JOBS 32
    #define MAX_PRIORITY_LEVELS 32
    #define MIN_IDLE_TIME (60L * 75L)  /* 60 counts per uS * 75uS */
    
    typedef enum
    {
        SCHEDULER_IDLE = 0,
        SCHEDULER_WAIT = 1,
        SCHEDULER_PENDING = 2,
        SCHEDULER_RUNNING = 3
    } SCHEDULER_STATES;

    typedef struct SCHEDULE_STRUCT
    {
        uword uwID;
        SCHEDULER_STATES eState;
        uword uwPriorityLevel;
        uword uwActivePriorityLevel;
        sword swTimePeriod;
        sword swTickCount;
        ulong ulLastRunTime;
        ulong ulAverageRunTime;
        boolean fTaskActive;
        void (*ServiceRoutine) ();
    } SCHEDULE_TYPE;

    typedef struct
    {
        uword uwNum;
        SCHEDULE_TYPE Jobs[MAX_SCHEDULED_JOBS];
    } TASKS_STUCT;
    
    // Function prototypes
    void vScheduler_Init(void);
    void vScheduler_Setup_Task(uword uwPosition, 
                               uword uwID, 
                               uword uwPriorityLevel,
                               sword swTimePeriod,
                               sword swTickCount,
                               boolean fTaskActive,
                               void (*ServiceRoutine) ());
                               
    void vScheduler_Activate_Task(uword uwPosition);
    void vScheduler_Deactivate_Task(uword uwPosition);
    void vScheduler_Delete_Task(uword uwPosition);
    void vScheduler_StartTasks(void);
    void vScheduler_StopTasks(void);
    void vMarkTick();
    sword swFindJob();
    void vRunJob(uword uwJob);
    
    void vRecycleJob(uword uwPosition);
    
    void vStart_Task(uword uwPosition);
    
    interrupt void Scheduler_Service(void);

    // define the global variables 
    TASKS_STUCT Tasks;
    
    #endif


#else
    #ifndef SchedulerRegistration
    #define SchedulerRegistration 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Externaly visable function prototypes
    extern void vScheduler_Init(void);
    extern void vScheduler_StartTasks(void);
    extern void vScheduler_StopTasks(void);
    extern void vScheduler_Setup_Task(uword uwPosition, 
                               uword uwID, 
                               uword uwPriorityLevel,
                               sword swTimePeriod,
                               sword swTickCount,
                               boolean fTaskActive,
                               void (*ServiceRoutine) ());

    extern void vScheduler_Activate_Task(uword uwPosition);
    extern void vScheduler_Deactivate_Task(uword uwPosition);
    extern void vScheduler_Delete_Task(uword uwPosition);
    extern interrupt void Scheduler_Service(void);

    // Externally visable global variables

    #endif
#endif


