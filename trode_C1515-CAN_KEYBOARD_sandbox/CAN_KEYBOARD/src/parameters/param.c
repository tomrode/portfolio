/************************************************************************************************
 *  File:       parm.c
 *
 *  Purpose:    Implements the settable parameters for the truck
 *
 *  Project:    C1515
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey, Tom Rode
 *
 *  Revision History:
 *              Written 11/30/2005
 *              Revised 01/29/2015 
 *
 ************************************************************************************************
*/

#include "DSP2803x_Device.h"
#define PARAM_MODULE
#include "Param.h"
#undef PARAM_MODULE
#pragma DATA_SECTION(gParams, "parameters");
const param_type_t gParams =
{
    { 
         {    /* Language table set one */            
              /* ulBasicUtf8EnglishLowerCase */
           { /*    SW1      SW2,     SW3,     SW4,     SW5,           */
                   0x0031u, 0x0071u, 0x0061u, 0x001Bu, 0x1221u,
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY, */
                   0x0032u, 0x0077u, 0x0073u, 0x007Au, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,  */
                   0x0033u, 0x0065u, 0x0064u, 0x0078u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,          */
                   0x0034u, 0x0072u, 0x0066u, 0x0063u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,          */
                   0x0035u, 0x0074u, 0x0067u, 0x0076u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,          */
                   0x0036u, 0x0079u, 0x0068u, 0x0062u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,  */
                   0x0037u, 0x0075u, 0x006Au, 0x006Eu, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY, */
                   0x0038u, 0x0069u, 0x006Bu, 0x006Du, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,   */
                   0x0039u, 0x006Fu, 0x006Cu, 0x002Cu, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,          */
                   0x0030u, 0x0070u, 0x0000u, 0x002Eu, 0x000Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,          */
                   0x002Du, 0x0000u, 0x0000u, 0x0008u, 0x000Du
           },

             /* ulBasicUtf8EnglishUpperCase */
            { /*   SW1      SW2,     SW3,     SW4,     SW5,           */
		           0x0031u, 0x0051u, 0x0041u, 0x001Bu, 0x1221u,
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY, */
                   0x0032u, 0x0057u, 0x0053u, 0x005Au, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,  */
                   0x0033u, 0x0045u, 0x0044u, 0x0058u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,          */
                   0x0034u, 0x0052u, 0x0046u, 0x0043u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,          */
                   0x0035u, 0x0054u, 0x0047u, 0x0056u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,          */
                   0x0036u, 0x0059u, 0x0048u, 0x0042u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,  */
                   0x0037u, 0x0055u, 0x004Au, 0x004Eu, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY, */
                   0x0038u, 0x0049u, 0x004Bu, 0x004Du, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,   */
                   0x0039u, 0x004Fu, 0x004Cu, 0x0000u, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,          */
                   0x0030u, 0x0050u, 0x0000u, 0x0000u, 0x000Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,          */
                   0x002Du, 0x0000u, 0x0000u, 0x0008u, 0x000Du
           }, 
             /* ulBasicUtf8EnglishAlternateChar */
           { /*    SW1      SW2,     SW3,     SW4,     SW5,           */
		           0x0021u, 0x007Eu, 0x007Bu, 0x001Bu, 0x1221u,  
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY, */
                   0x0040u, 0x0000u, 0x1203u, 0x007Du, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,  */
                   0x0023u, 0x1201u, 0x1204u, 0x1202u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,          */
                   0x0024u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,          */
                   0x0025u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,          */
                   0x005Eu, 0x0000u, 0xC397u, 0x0000u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,  */
                   0x0026u, 0x002Bu, 0xC3B7u, 0x002Du, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY, */
                   0x002Au, 0x003Bu, 0x003Au, 0x003Cu, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,   */
                   0x0028u, 0x005Bu, 0x0022u, 0x003Eu, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,          */
                   0x0029u, 0x005Du, 0x0027u, 0x003Fu, 0x003Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,          */
                   0x002Du, 0x005Cu, 0x002Fu, 0x0008u, 0x003Du
            },   
             /* ulBasicUtf8EnglishFunctionsChar */
           { /*    SW1      SW2,     SW3,     SW4,     SW5,            */
   	               0x1101u, 0x0000u, 0x0000u, 0x001Bu, 0x1221u, 
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY,  */
                   0x1102u, 0x0000u, 0x1213u, 0x0000u, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,   */
                   0x1103u, 0x1211u, 0x1214u, 0x1212u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,           */
                   0x1104u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,           */
                   0x1105u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,           */
                   0x1106u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,   */
                   0x1107u, 0x0000u, 0x0000u, 0x0000u, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY,  */
                   0x1108u, 0x0000u, 0x0000u, 0x0000u, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,    */
                   0x1109u, 0x0000u, 0x0000u, 0x0000u, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,           */
                   0x1100u, 0x0000u, 0x0000u, 0x0000u, 0x000Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,           */
                   0x002Du, 0x0000u, 0x0000u, 0x0008u, 0x000Du
           }, 
          /** -  End of First language set */
         }, 
      
         {        /* Language table set two */ 
                 /* ulBasicUtf8GermanLowerCase */
            { /*    SW1      SW2,     SW3,     SW4,     SW5,           */
                   0x0031u, 0x0071u, 0x0061u, 0x001Bu, 0x1221u,
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY,  */
                   0x0032u, 0x0077u, 0x0073u, 0x0079u, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,   */
                   0x0033u, 0x0065u, 0x0064u, 0x0078u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,           */
                   0x0034u, 0x0072u, 0x0066u, 0x0063u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,           */
                   0x0035u, 0x0074u, 0x0067u, 0x0076u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,           */
                   0x0036u, 0x007Au, 0x0068u, 0x0062u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,   */
                   0x0037u, 0x0075u, 0x006Au, 0x006Eu, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY,  */
                   0x0038u, 0x0069u, 0x006Bu, 0x006Du, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,    */
                   0x0039u, 0x006Fu, 0x006Cu, 0x002Cu, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,           */
                   0x0030u, 0x0070u, 0xC3B6u, 0x002Eu, 0x000Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,           */
                   0x0000u, 0xC3BCu, 0xC3A4u, 0x0008u, 0x000Du
             /*             SW52=�   SW48=�                            */
             /*                      SW53=�                            */ 
            },
           
                 /* ulBasicUtf8GermanUpperCase */
            { /*   SW1      SW2,     SW3,     SW4,     SW5,            */
		           0x0031u, 0x0051u, 0x0041u, 0x001Bu, 0x1221u,
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY,  */
                   0x0032u, 0x0057u, 0x0053u, 0x0059u, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,   */
                   0x0033u, 0x0045u, 0x0044u, 0x0058u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,           */
                   0x0034u, 0x0052u, 0x0046u, 0x0043u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,           */
                   0x0035u, 0x0054u, 0x0047u, 0x0056u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,           */
                   0x0036u, 0x005Au, 0x0048u, 0x0042u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,   */
                   0x0037u, 0x0055u, 0x004Au, 0x004Eu, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY,  */
                   0x0038u, 0x0049u, 0x004Bu, 0x004Du, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,    */
                   0x0039u, 0x004Fu, 0x004Cu, 0x0000u, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,           */
                   0x0030u, 0x0050u, 0xC396u, 0x0000u, 0x000Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,           */
                   0xC39Fu, 0xC39Cu, 0xC384u, 0x0008u, 0x000Du
             /*    SW51=�   SW52=�   SW48=�                            */
             /*                      SW53=�                            */ 
           },
  
               /* ulBasicUtf8GermanAlternateChar */
           { /*    SW1      SW2,     SW3,     SW4,     SW5,            */
		           0x0021u, 0x007Eu, 0x007Bu, 0x001Bu, 0x1221u,  
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY,  */
                   0x0040u, 0x0000u, 0x1203u, 0x007Du, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,   */
                   0x0023u, 0x1201u, 0x1204u, 0x1202u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,           */
                   0x0024u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,           */
                   0x0025u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,           */
                   0x005Eu, 0x0000u, 0xC397u, 0x0000u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,   */
                   0x0026u, 0x002Bu, 0xC3B7u, 0x002Du, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY,  */
                   0x002Au, 0x003Bu, 0x003Au, 0x003Cu, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,    */
                   0x0028u, 0x005Bu, 0x0022u, 0x003Eu, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,           */
                   0x0029u, 0x005Du, 0x0027u, 0x003Fu, 0x003Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,           */
                   0x005Fu, 0x005Cu, 0x002Fu, 0x0008u, 0x003Du
            },   

             /* ulBasicUtf8GermanFunctionsChar */
           { /*    SW1      SW2,     SW3,     SW4,     SW5,            */
   		           0x1101u, 0x0000u, 0x0000u, 0x001Bu, 0x1221u, 
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY,  */
                   0x1102u, 0x0000u, 0x1213u, 0x0000u, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,   */
                   0x1103u, 0x1211u, 0x1214u, 0x1212u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,           */
                   0x1104u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,           */
                   0x1105u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,           */
                   0x1106u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,   */
                   0x1107u, 0x0000u, 0x0000u, 0x0000u, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY,  */
                   0x1108u, 0x0000u, 0x0000u, 0x0000u, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,    */
                   0x1109u, 0x0000u, 0x0000u, 0x0000u, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,           */
                   0x1100u, 0x0000u, 0x0000u, 0x0000u, 0x000Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,           */
                   0x0000u, 0x0000u, 0x0000u, 0x0008u, 0x000Du
           },          
         /** -  End of Second language set */
         }, 

         { 
             /* ulBasicUtf8SpanishLowerCase */
           { /*    SW1      SW2,     SW3,     SW4,     SW5,            */
                   0x0031u, 0x0071u, 0x0061u, 0x001Bu, 0x1221u,
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY,  */
                   0x0032u, 0x0077u, 0x0073u, 0x007Au, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,   */
                   0x0033u, 0x0065u, 0x0064u, 0x0078u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,           */
                   0x0034u, 0x0072u, 0x0066u, 0x0063u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,           */
                   0x0035u, 0x0074u, 0x0067u, 0x0076u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,           */
                   0x0036u, 0x0079u, 0x0068u, 0x0062u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,   */
                   0x0037u, 0x0075u, 0x006Au, 0x006Eu, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY,  */
                   0x0038u, 0x0069u, 0x006Bu, 0x006Du, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,    */
                   0x0039u, 0x006Fu, 0x006Cu, 0x002Cu, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,           */
                   0x0030u, 0x0070u, 0xC2BFu, 0x002Eu, 0x000Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,           */
                   0xC3A7,  0xC3B1u, 0xC2A1u, 0x0008u, 0x000Du
             /*    SW51=�   SW52=�   SW48=�                            */
             /*                      SW53=�                            */
           },
         
           
              /* ulBasicUtf8SpanishUpperCase */
           {  /*   SW1      SW2,     SW3,     SW4,     SW5,            */
		           0x0031u, 0x0051u, 0x0041u, 0x001Bu, 0x1221u,
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY,  */
                   0x0032u, 0x0057u, 0x0053u, 0x005Au, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,   */
                   0x0033u, 0x0045u, 0x0044u, 0x0058u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,           */
                   0x0034u, 0x0052u, 0x0046u, 0x0043u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,           */
                   0x0035u, 0x0054u, 0x0047u, 0x0056u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,           */
                   0x0036u, 0x0059u, 0x0048u, 0x0042u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,   */
                   0x0037u, 0x0055u, 0x004Au, 0x004Eu, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY,  */
                   0x0038u, 0x0049u, 0x004Bu, 0x004Du, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,    */
                   0x0039u, 0x004Fu, 0x004Cu, 0x0000u, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,           */
                   0x0030u, 0x0050u, 0x0000u, 0x0000u, 0x000Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,           */
                   0xC387u, 0xC391u, 0x0000u, 0x0008u, 0x000Du
             /*    SW51=�   SW52=�                                     */
           },
   
             /* ulBasicUtf8SpanishAlternateChar */
           { /*    SW1      SW2,     SW3,     SW4,     SW5,            */
		           0x0021u, 0x007Eu, 0x007Bu, 0x001Bu, 0x1221u,  
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY,  */
                   0x0040u, 0x0000u, 0x1203u, 0x007Du, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,   */
                   0x0023u, 0x1201u, 0x1204u, 0x1202u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,           */
                   0x0024u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,           */
                   0x0025u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,           */
                   0x005Eu, 0x0000u, 0xC397u, 0x0000u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,   */
                   0x0026u, 0x002Bu, 0xC3B7u, 0x002Du, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY,  */
                   0x002Au, 0x003Bu, 0x003Au, 0x003Cu, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,    */
                   0x0028u, 0x005Bu, 0x0022u, 0x003Eu, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,           */
                   0x0029u, 0x005Du, 0x0027u, 0x003Fu, 0x003Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,           */
                   0x005Fu, 0x005Cu, 0x002Fu, 0x0008u, 0x003Du
            },

             /* ulBasicUtf8SpanishFunctionsChar */
           { /*    SW1      SW2,     SW3,     SW4,     SW5,            */
   	   	           0x1101u, 0x0000u, 0x0000u, 0x001Bu, 0x1221u, 
             /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY,  */
                   0x1102u, 0x0000u, 0x1213u, 0x0000u, 0x0000u,
             /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,   */
                   0x1103u, 0x1211u, 0x1214u, 0x1212u, 0x0000u,
             /*    SW16,    SW17,    SW18,    SW19,    SW20,           */
                   0x1104u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW21,    SW22,    SW23,    SW24,    SW25,           */
                   0x1105u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW26,    SW27,    SW28,    SW29,    SW30,           */
                   0x1106u, 0x0000u, 0x0000u, 0x0000u, 0x0020u,
             /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,   */
                   0x1107u, 0x0000u, 0x0000u, 0x0000u, 0x0000u,
             /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY,  */
                   0x1108u, 0x0000u, 0x0000u, 0x0000u, 0x0000u,
             /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,    */
                   0x1109u, 0x0000u, 0x0000u, 0x0000u, 0x0000u,
             /*    SW46,    SW47,    SW48,    SW49,    SW50,           */
                   0x1100u, 0x0000u, 0x0000u, 0x0000u, 0x000Du,
             /*    SW51,    SW52,    SW53,    SW54,    SW55,           */
                   0x0000u, 0x0000u, 0x0000u, 0x0008u, 0x000Du
           },
        },    /** End of Third language tables */                               

		{ 
			/* ulBasicUtf8Lang4LowerCase */
		{ /*    SW1          SW2,         SW3,         SW4,         SW5,            */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
          /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
	      /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
				0xFFFFFFFF,  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu

		 },
			
			
			/* ulBasicUtf8Lang4UpperCase */
		{  /*   SW1          SW2,         SW3,         SW4,         SW5,            */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		},
			
			/* ulBasicUtf8Lang4AlternateChar */
		{ /*    SW1          SW2,         SW3,         SW4,         SW5,            */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,  
	   	  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
        },
		
			/* ulBasicUtf8Lang4FunctionsChar */
		{ /*    SW1          SW2,         SW3,         SW4,         SW5,            */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 
		  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
	  	  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
		  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
				0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
	    },
      },       /** End of Fourth language tables */                             
		
	  
	  { 
		  /* ulBasicUtf8Lang5LowerCase */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFF,  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
	  
		  },
		  
		  
		  /* ulBasicUtf8Lang5UpperCase */
		  {  /*   SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang5AlternateChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,  
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang5FunctionsChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
      },      /** End of Fifth language tables */ 
		
	  { 
		  /* ulBasicUtf8Lang6LowerCase */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFF,  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
			  
		  },
		  
		  
		  /* ulBasicUtf8Lang6UpperCase */
		  {  /*   SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang6AlternateChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,  
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang6FunctionsChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
      },      /** End of Sixth language tables */ 
	  
	  { 
		  /* ulBasicUtf8Lang7LowerCase */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFF,  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
			  
		  },
		  
		  
		  /* ulBasicUtf8Lang7UpperCase */
		  {  /*   SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang7AlternateChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,  
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang7FunctionsChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
      },      /** End of Seventh language tables */ 
	  
	  { 
		  /* ulBasicUtf8Lang8LowerCase */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFF,  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
			  
		  },
		  
		  
		  /* ulBasicUtf8Lang8UpperCase */
		  {  /*   SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang8AlternateChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,  
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang8FunctionsChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
      },      /** End of Eigth language tables */ 
	  
	  { 
		  /* ulBasicUtf8Lang9LowerCase */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFF,  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
			  
		  },
		  
		  
		  /* ulBasicUtf8Lang9UpperCase */
		  {  /*   SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang9AlternateChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,  
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
		  
		  /* ulBasicUtf8Lang9FunctionsChar */
		  { /*    SW1          SW2,         SW3,         SW4,         SW5,            */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 
			  /*    SW6,         SW7,         SW8,         SW9,         SW10 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW11,        SW12,        SW13,        SW14,        SW15 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW16,        SW17,        SW18,        SW19,        SW20,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW21,        SW22,        SW23,        SW24,        SW25,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW26,        SW27,        SW28,        SW29,        SW30,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW31,        SW32,        SW33,        SW34,        SW35 ALT_KEY,   */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW36,        SW37,        SW38,        SW39,        SW40 SHFT_KEY,  */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW41,        SW42,        SW43,        SW44,        SW45 FN_KEY,    */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW46,        SW47,        SW48,        SW49,        SW50,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu,
			  /*    SW51,        SW52,        SW53,        SW54,        SW55,           */
			  0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu
		  },
      },      /** End of Nineth language tables */ 
	  
    },
	
	/** Hardware Switch Table */
	/* ulHardwareSwCheckTable */
	{ /*    SW1      SW2,     SW3,     SW4,     SW5,           */
		    1u,      2u,      3u,      4u,      5u,
	  /*    SW6,     SW7,     SW8,     SW9,     SW10 SHFT_KEY, */
	        6u,      7u,      8u,      9u,      10u,
	  /*    SW11,    SW12,    SW13,    SW14,    SW15 ALT_KEY,  */
		    11u,     12u,     13u,     14u,     15u,
	  /*    SW16,    SW17,    SW18,    SW19,    SW20,          */
		    16u,     17u,     18u,     19u,     20u,
	  /*    SW21,    SW22,    SW23,    SW24,    SW25,          */
		    21u,     22u,     23u,     24u,     25u,
	  /*    SW26,    SW27,    SW28,    SW29,    SW30,          */
		    26u,     27u,     28u,     29u,     30u,
	  /*    SW31,    SW32,    SW33,    SW34,    SW35 ALT_KEY,  */
		    31u,     32u,     33u,     34u,     35u,
	  /*    SW36,    SW37,    SW38,    SW39,    SW40 SHFT_KEY, */
		    36u,     37u,     38u,     39u,     40u,
	  /*    SW41,    SW42,    SW43,    SW44,    SW45 FN_KEY,   */
		    41u,     42u,     43u,     44u,     45u,
	  /*    SW46,    SW47,    SW48,    SW49,    SW50,          */
		    46u,     47u,     48u,     49u,     50u,
	  /*    SW51,    SW52,    SW53,    SW54,    SW55,          */
		    51u,     52u,     53u,     54u,     55u
	},
	
     /* Keyboard driver parameters*/
    {
             (uint16_t)120u,                      /* uwFaultMatureTime  Ticks / 25ms = 3seconds */
             (uint8_t)5u,                         /* ubRows, The amount of hardware rows */
             (uint8_t)11u,                        /* ubColumns, The amount of hardware switch columns */
             (uint8_t)1u,                         /* ubCharOnPressed, Output character on pressed =1 or released =0 */
             (uint8_t)9u,                         /* ubLangTablesCnt, This is the amount of language tables sets, used for protection of in case 0x276 message is wrong */ 
             (uint8_t)1u,                         /* ubDSP28035, This is for hardware abstarction using 28035DSP micro. */  
    },
    


};




