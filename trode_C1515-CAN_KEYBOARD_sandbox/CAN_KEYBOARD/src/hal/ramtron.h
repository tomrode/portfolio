/**********************************************************************
 *
 * Filename:    ramtron.h
 *
 * Copyright (C) 2001 Crown Equipment Corporation. All rights reserved.
 *
 * Description: Header file for Ramtron FRAM device
 *
 * Notes:       Device support is RM25640 only
 *
 **********************************************************************/

#include "types.h"
//#include "SPI.h"

#ifndef _ramtron_h
#define _ramtron_h

/*
 * Definitions
 */
#define FRAM_SIZE  0x2000

// Opcode commands
#define FRAM_WREN 0x06
#define FRAM_WRDI 0x04
#define FRAM_RDSR 0x05
#define FRAM_WRSR 0x01
#define FRAM_READ 0x03
#define FRAM_WRIT 0x02

// Status register bit definitions
#define BIT_WPEN  0x80
#define CLEAR_BPS 0xf3
#define RANGE0    0x00
#define RANGE25   0x04
#define RANGE50   0x08
#define RANGE100  0x0c


#define FRAM_REV_ADDR       0x00    // 1 word long  at 0x00 in FRAM
#define MODULE_ID_ADDR      0x01    // 1 word long  at 0x02 in FRAM
#define BAUD_CAN_ADDR       0x02    // 1 word long  at 0x04 in FRAM
#define EXT_MEM_ADDR        0x03    // 1 word long  at 0x06 in FRAM
#define RS232_ENABLE_ADDR   0x04    // 1 word long  at 0x08 in FRAM
#define BAUD_232_ADDR       0x05    // 1 word long  at 0x0A in FRAM
#define SWITCH_ID_ENABLE_ADDR 0x06  // 1 word long  at 0x0C in FRAM
#define MODULE_PN_ADDR      0x10    // 16 words max including null termination, data in lsb at 0x20 in FRAM
#define HDWR_PN_ADDR        0x20    // 16 words max including null termination, data in lsb at 0x40 in FRAM
#define HDWR_REV_ADDR       0x30    // 16 words max including null termination, data in lsb at 0x60 in FRAM
#define SERIAL_NUMBER_ADDR  0x40    // 16 words max including null termination, data in lsb at 0x80 in FRAM
#define MODULE_TYPE_ADDR    0x50    // 16 words max including null termination, data in lsb at 0xA0 in FRAM
#define IMM_LANGUAGE_ADDR   0x60    // 16 words max including null termination, data in lsb at 0xB0 in FRAM


boolean fFramProtect(uword uwRange);
boolean fFramControlWpEn(uword uwState);
uword ReturnFramRevision(void);
uword ReturnModuleId(void);
uword ReturnCANBaudRate(void);
uword *ReturnModuleType(uword *);
uword *ReturnModulePN(uword *);
uword *ReturnHardwarePN(uword *);
uword *ReturnHardwareRev(uword *);
uword *ReturnSerialNumber(uword *);

boolean WriteFram(uword *data, uword address, uword num_words, boolean verify);
boolean FRAM_Log_Start(uword address);
void FRAM_Log_Write(uword *data, uword num_words);
void FRAM_Log_End();
boolean ReadFram(uword *data, uword address, uword num_words);
uword   uwReturnExternalMemoryVersion(void);
void    vWriteExternalMemoryVersion(uword uwVersion);
uword   uwReturnRs232Enable(void);
void    vWriteRs232Enable(uword uwEnable);
uword   uwReturnSwitchID_Enable(void);
void    vWriteSwitchID_Enable(uword uwEnable);

#endif /* _ramtron_h */
