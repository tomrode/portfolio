//****************************************************************************
// Module        High-Speed Synchronous Serial Interface (SPI)
// Filename      SPI.H
//----------------------------------------------------------------------------
// Controller    TI DSP2803x
//
// Compiler      Code Composer Studio
//
//
// Description   This file contains all function for the SPI module.
//
//----------------------------------------------------------------------------
// Date          09/04/2009 AM
//
//****************************************************************************

// USER CODE BEGIN (SPI_General,1)

// USER CODE END



//****************************************************************************
// @Project Includes
//****************************************************************************

#include "DSP2803x_Device.h"     // DSP281x Headerfile Include File
#include "DSP2803x_Examples.h"   // DSP281x Examples Include File
#include "types.h"
#include "SPI.h"
#include "DSP2803x_Sci.h"
#include "DSP2803x_Gpio.h"

// USER CODE BEGIN (SPI_General,2)

// USER CODE END


//****************************************************************************
// @Macros
//****************************************************************************

// USER CODE BEGIN (SPI_General,3)

// USER CODE END


//****************************************************************************
// @Defines
//****************************************************************************

// USER CODE BEGIN (SPI_General,4)
#define TIMEOUT 57142
// USER CODE END


//****************************************************************************
// @Typedefs
//****************************************************************************

// USER CODE BEGIN (SPI_General,5)

// USER CODE END


//****************************************************************************
// @Imported Global Variables
//****************************************************************************

// USER CODE BEGIN (SPI_General,6)

// USER CODE END


//****************************************************************************
// @Global Variables
//****************************************************************************

// USER CODE BEGIN (SPI_General,7)

// USER CODE END


//****************************************************************************
// @External Prototypes
//****************************************************************************

// USER CODE BEGIN (SPI_General,8)

// USER CODE END


//****************************************************************************
// @Prototypes Of Local Functions
//****************************************************************************

// USER CODE BEGIN (SPI_General,9)

// USER CODE END


//****************************************************************************
// @Function      void SPI_vInit(void)
//
//----------------------------------------------------------------------------
// @Description   This is the initialization function of the SPI function
//                library. It is assumed that the SFRs used by this library
//                are in its reset state.
//
//----------------------------------------------------------------------------
// @Returnvalue   None
//
//----------------------------------------------------------------------------
// @Parameters    None
//
//----------------------------------------------------------------------------
// @Date          9/30/2004
//
//****************************************************************************

// USER CODE BEGIN (Init,1)

// USER CODE END

void SPI_vInit(void)
{
    // USER CODE BEGIN (Init,2)

    // USER CODE END
    // Setup only the GP I/O only for SPI functionality
    EALLOW;
    GpioCtrlRegs.GPAMUX2.bit.GPIO27 = 0;    // Chip select
    GpioCtrlRegs.GPADIR.bit.GPIO27  = 1;    // Make it an output
    GpioDataRegs.GPADAT.bit.GPIO27 = 1;     // Set High

    GpioCtrlRegs.GPAMUX2.bit.GPIO24 = 3;    // SIMO
    GpioCtrlRegs.GPAMUX2.bit.GPIO25 = 3;    // SOMI
    GpioCtrlRegs.GPAMUX2.bit.GPIO26 = 3;    // SPI CLK


    SpibRegs.SPICCR.all =0x0004F;                // Reset on, falling edge, 8-bit char bits
    SpibRegs.SPICTL.all =0x0006;                 // Enable master mode, normal phase,
                                                 // enable talk, and SPI int disabled.
    SpibRegs.SPIBRR =0x0008;                     // 1.875 MHz operation
    SpibRegs.SPICCR.all =0x00CF;                 // Relinquish SPI from Reset
    SpibRegs.SPIPRI.bit.FREE = 1;                // Set so breakpoints don't disturb xmission
    EDIS;

    // USER CODE BEGIN (Init,3)

    // USER CODE END

} //  End of function SPI_vInit


//****************************************************************************
// @Function      void SPI_vSendData(uword uwData)
//
//----------------------------------------------------------------------------
// @Description   The master device can initiate the first data transfer by
//                writing the transmit data into transmit buffer. This value
//                is copied into the shift register (which is assumed to be
//                empty at this time), and the selected first bit of the
//                transmit data is placed onto the MTSR line on the next
//                clock from the baud rate generator.
//                A slave device immediately outputs the selected first bit
//                (MSB or LSB of the transfer data) at pin MRST, when the
//                contents of the transmit buffer are copied into the slave's
//                shift register.
//
//----------------------------------------------------------------------------
// @Returnvalue   None
//
//----------------------------------------------------------------------------
// @Parameters    uwData:
//                Data to be send
//
//----------------------------------------------------------------------------
// @Date          9/30/2004
//
//****************************************************************************

// USER CODE BEGIN (SendData,1)

// USER CODE END

void SPI_vSendData(uword uwData)
{
    SpibRegs.SPITXBUF = uwData;   //  load transmit buffer register

} //  End of function SPI_vSendData


//****************************************************************************
// @Function      uword SPI_uwGetData(void)
//
//----------------------------------------------------------------------------
// @Description   This function returns the contents of the receive buffer.
//                When the receive interrupt request flag is set this implies
//                that data is available for reading in the receive buffer.
//
//----------------------------------------------------------------------------
// @Returnvalue   Received data
//
//----------------------------------------------------------------------------
// @Parameters    None
//
//----------------------------------------------------------------------------
// @Date          9/30/2004
//
//****************************************************************************

// USER CODE BEGIN (GetData,1)

// USER CODE END

uword SPI_uwGetData(void)
{
    return(SpibRegs.SPIRXBUF);     // return receive buffer register

} //  End of function SPI_uwGetData




// USER CODE BEGIN (SPI_General,10)
/**********************************************************************
 *
 * Function:    SPI_MySendData()
 *
 * Description: This function handles all the steps required to send
 *              a byte of data using the SPI port.
 *
 * Argument(s): uwData = contains the byte to be sent
 *
 * Returns:     True if no timeouts
 *
 **********************************************************************/
boolean SPI_fMySendData(uword uwData, volatile uword *puwDataOut, uword uwLength)
{
    boolean fResult = TRUE;
    ulong ulRetry;
    uword uwBuffer;
    uword uwMask = 0xFFFF;

    uwBuffer = uwData << (16 - uwLength);
    // Set the length of the transfer
    SpibRegs.SPICCR.bit.SPICHAR = ((uwLength - 1) & 0x0F);

    ulRetry = 0;
    while ( !SPI_ubTxBufFree() )  //  Wait till the transmit buffer  is free
    {
        ulRetry++;
        if ( ulRetry > TIMEOUT )
        {
            fResult = FALSE;
            break;
        }
    }
    SPI_vSendData(uwBuffer);            //  load transmit buffer register
    ulRetry = 0;
    while ( !SPI_ubRxDataReady() )  //   Wait till the receiver is ready
    {
        ulRetry++;
        if ( ulRetry > TIMEOUT )
        {
            fResult = FALSE;
            break;
        }
    }
    uwMask >>= (16 - uwLength);
    *puwDataOut = SPI_uwGetData() & uwMask;                   //  read receive buffer register
    return(fResult);
}
// USER CODE END

