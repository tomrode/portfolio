/**
 *  @file               Adc.c
 *  @brief              ADC setup and drive service on the ADC hardware
 *  @copyright          2015 Crown Equipment Corp., New Bremen, OH  45869
 *  @date               03/24/2015 
 *
 *  @remark Author:     Bill McCroskey
 *
 *  @note               Updated by Tom Rode
 *
 */

/**********************************************************************
* File: Adc.c
* Devices: TMS320F28235
* Author: David M. Alter, Texas Instruments Inc.
* History:
*   09/08/03 - original (based on DSP281x header files v1.00, D. Alter)
*   03/18/04 - fixed comment field for ADCTRL1.5 bit
**********************************************************************/

#include "DSP2803x_Device.h"     /* DSP2803x Headerfile Include File */
#include "DSP2803x_Examples.h"   /* DSP2803x Examples Include File */
#include "InputSwitches.h"



/** - EXAMPLE_BIOS or EXAMPLE_NONBIOS are defined in the CCS project build options */
#ifdef EXAMPLE_BIOS
    #include "example_BIOS.h"
#endif

#ifdef EXAMPLE_NONBIOS
    #include "example_nonBIOS.h"
#endif

#include "ev.h"
#include "AdcData.h"
#include "ev.h"
#include "util.h"
#include "string.h"
#include "types.h"
#define ADC_MODULE 1
#include "Adc.h"
#undef ADC_MODULE

/**
 *  @fn         InitAdc
 *
 *  @brief      Sets up the ADC hardware sub system.
 *
 *  @param      N/A
 *
 *  @return     N/A
 *
 *  @note       Initially written: 08/07/2007, updated: 02/10/2015
 *      
 *  @author     Bill McCroskey, Tom Rode
 *
 */
void InitAdc(void)
{
     /** ###Functional overview: */ 
 
    /** -  Blank the ADC data*/
    vInit_ADC_Data();

    /** - Set Raw ADC */
    memset((void *)&gRaw_Adc, 0, sizeof(RAW_ADC_COUNT_TYPE));


    /** - IMPORTANT*/
    /** - The Device_cal function, which copies the ADC calibration values from TI reserved */
    /** - OTP into the ADCREFSEL and ADCOFFTRIM registers, occurs automatically in the */
    /** - Boot ROM. If the boot ROM code is bypassed during the debug process, the */
    /** - following function MUST be called for the ADC to function according */
    /** - to specification. The clocks to the ADC MUST be enabled before calling this */
    /** - function. */
    /** - See the device data manual and/or the ADC Reference */
    /** - Manual for more information. */
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.ADCENCLK = 1;
    (*Device_cal)();
    EDIS;

    /** - Reset the ADC module */
    /** - Must wait 12-cycles (worst-case) for */
    /** - ADC reset to take effect */
    AdcRegs.ADCCTL1.bit.RESET = 1;   
    asm(" RPT #10 || NOP");           
                                     

    /** - To powerup the ADC the ADCENCLK bit should be set first to enable */
    /** - clocks, followed by powering up the bandgap, reference circuitry, and ADC core. */
    /** - Before the first conversion is performed a 5ms delay must be observed */
    /** - after power up to give all analog circuits time to power up and settle */

    /** - Please note that for the delay function below to operate correctly the */
    /** - CPU_RATE define statement in the DSP2803x_Examples.h file must */
    /** - contain the correct CPU clock period in nanoseconds. */

    EALLOW;
    AdcRegs.ADCCTL1.bit.ADCENABLE = 1;      /** Enable ADC */
    AdcRegs.ADCCTL1.bit.ADCPWDN   = 1;      /** Power ADC */
    AdcRegs.ADCCTL1.bit.ADCBGPWD  = 1;      /** Power ADC BG */
    AdcRegs.ADCCTL1.bit.ADCREFPWD = 1;      /** Power reference */
    AdcRegs.ADCCTL1.bit.ADCREFSEL = 1;      /** External reference */
    AdcRegs.ADCCTL1.bit.INTPULSEPOS = 1;    /** INT at end of conversion */
    AdcRegs.ADCCTL1.bit.VREFLOCONV  = 0;    /** Pass ADCINB5 through as normal */
    AdcRegs.ADCCTL1.bit.TEMPCONV    = 0;    /** Pass ADCINA5 through as normal */
    EDIS;

    /** - Delay before converting ADC channels */ 
    DELAY_US(ADC_usDELAY);         


    /** - Setup interrupts */
    EALLOW;
    AdcRegs.INTSEL1N2.bit.INT2CONT  = 0;    /** Disable ADCINT2 Continuous mode */
    AdcRegs.INTSEL1N2.bit.INT2E     = 1;    /** Enabled ADCINT2 */
    AdcRegs.INTSEL1N2.bit.INT2SEL   = 15;   /** Setup EOC15 to trigger ADCINT2 to fire */
    AdcRegs.INTSEL1N2.bit.INT1CONT  = 0;    /** Disable ADCINT1 Continuous mode */
    AdcRegs.INTSEL1N2.bit.INT1E     = 1;    /** Enabled ADCINT1 */
    AdcRegs.INTSEL1N2.bit.INT1SEL   = 8;    /** Setup EOC8 to trigger ADCINT1 to fire */
    EDIS;

    /** - Configure ADC pins */
    EALLOW;

    /** - This specifies which of the possible AIO pins will be Analog input pins.*/
    /** - NOTE: AIO1,3,5,7-9,11,13,15 are analog inputs in all AIOMUX1 configurations. */
    GpioCtrlRegs.AIOMUX1.bit.AIO2 = 0;    /** Configure AIO2 for A2 (analog input) operation */
    GpioCtrlRegs.AIOMUX1.bit.AIO4 = 0;    /** Configure AIO4 for A4 (analog input) operation */
    GpioCtrlRegs.AIOMUX1.bit.AIO6 = 0;    /** Configure AIO6 for A6 (analog input) operation */
    GpioCtrlRegs.AIOMUX1.bit.AIO10 = 0;   /** Configure AIO10 for B2 (analog input) operation */
    GpioCtrlRegs.AIOMUX1.bit.AIO12 = 0;   /** Configure AIO12 for B4 (analog input) operation */
    GpioCtrlRegs.AIOMUX1.bit.AIO14 = 0;   /** Configure AIO14 for B6 (analog input) operation */

    EDIS;

    EALLOW;
    
    /** - Configure SOC settings */
    AdcRegs.SOCPRICTL.bit.SOCPRIORITY = 0;  /** SOC0 and 1 are high priority, 2 to 15 are round robin */
    AdcRegs.ADCSOC0CTL.bit.CHSEL  = 4;      /** Set SOC0 channel select to ADCINA4 */
    AdcRegs.ADCSOC1CTL.bit.CHSEL  = 5;      /** Set SOC1 channel select to ADCINA5 */
    AdcRegs.ADCSOC2CTL.bit.CHSEL  = 6;      /** Set SOC0 channel select to ADCINA6 */
    AdcRegs.ADCSOC3CTL.bit.CHSEL  = 8;      /** Set SOC1 channel select to ADCINB0 */
    AdcRegs.ADCSOC4CTL.bit.CHSEL  = 4;      /** Set SOC0 channel select to ADCINA4 */
    AdcRegs.ADCSOC5CTL.bit.CHSEL  = 5;      /** Set SOC1 channel select to ADCINA5 */
    AdcRegs.ADCSOC6CTL.bit.CHSEL  = 6;      /** Set SOC0 channel select to ADCINA6 */
    AdcRegs.ADCSOC7CTL.bit.CHSEL  = 7;      /** Set SOC1 channel select to ADCINA7 */
    AdcRegs.ADCSOC8CTL.bit.CHSEL  = 8;      /** Set SOC0 channel select to ADCINB0 */
    AdcRegs.ADCSOC9CTL.bit.CHSEL  = 9;      /** Set SOC1 channel select to ADCINB1 */
    AdcRegs.ADCSOC10CTL.bit.CHSEL = 10;     /** Set SOC0 channel select to ADCINB2 */
    AdcRegs.ADCSOC11CTL.bit.CHSEL = 11;     /** Set SOC1 channel select to ADCINB3 */
    AdcRegs.ADCSOC12CTL.bit.CHSEL = 12;     /** Set SOC0 channel select to ADCINB4 */
    AdcRegs.ADCSOC13CTL.bit.CHSEL = 13;     /** Set SOC1 channel select to ADCINB5 */
    AdcRegs.ADCSOC14CTL.bit.CHSEL = 14;     /** Set SOC0 channel select to ADCINB6 */
    AdcRegs.ADCSOC15CTL.bit.CHSEL = 15;     /** Set SOC1 channel select to ADCINB7 */

    AdcRegs.ADCSOC0CTL.bit.TRIGSEL  = 5;    /** Set SOC0 start trigger on EPWM1A, High priority on 0 then round-robin converts SOC2 to 8 */
    AdcRegs.ADCSOC1CTL.bit.TRIGSEL  = 8;    /** Set SOC1 start trigger on EPWM2B, High priority on 1 then round-robin converts SOC10 to 15 */
    AdcRegs.ADCSOC2CTL.bit.TRIGSEL  = 5;    /** Set SOC2 start trigger on EPWM1A, High priority on 0 then round-robin converts SOC2 to 8 */
    AdcRegs.ADCSOC3CTL.bit.TRIGSEL  = 5;    /** Set SOC3 start trigger on EPWM1A, High priority on 0 then round-robin converts SOC2 to 8 */
    AdcRegs.ADCSOC4CTL.bit.TRIGSEL  = 0;    /** Set SOC4 start trigger on EPWM1A, High priority on 0 then round-robin converts SOC2 to 8 */
    AdcRegs.ADCSOC5CTL.bit.TRIGSEL  = 5;    /** Set SOC5 start trigger on EPWM1A, High priority on 0 then round-robin converts SOC2 to 8 */
    AdcRegs.ADCSOC6CTL.bit.TRIGSEL  = 5;    /** Set SOC6 start trigger on EPWM1A, High priority on 0 then round-robin converts SOC2 to 8 */
    AdcRegs.ADCSOC7CTL.bit.TRIGSEL  = 5;    /** Set SOC7 start trigger on EPWM1A, High priority on 0 then round-robin converts SOC2 to 8 */
    AdcRegs.ADCSOC8CTL.bit.TRIGSEL  = 0;    /** Set SOC8 start trigger on EPWM1A, High priority on 0 then round-robin converts SOC2 to 8 */
    AdcRegs.ADCSOC9CTL.bit.TRIGSEL  = 5;    /** Set SOC9 start trigger on software */
    AdcRegs.ADCSOC10CTL.bit.TRIGSEL = 8;    /** Set SOC10 start trigger on EPWM2B, High priority on 1 then round-robin converts SOC10 to 15 */
    AdcRegs.ADCSOC11CTL.bit.TRIGSEL = 8;    /** Set SOC11 start trigger on EPWM2B, High priority on 1 then round-robin converts SOC10 to 15 */
    AdcRegs.ADCSOC12CTL.bit.TRIGSEL = 8;    /** Set SOC12 start trigger on software */
    AdcRegs.ADCSOC13CTL.bit.TRIGSEL = 8;    /** Set SOC13 start trigger on EPWM2B, High priority on 1 then round-robin converts SOC10 to 15 */
    AdcRegs.ADCSOC14CTL.bit.TRIGSEL = 8;    /** Set SOC14 start trigger on EPWM2B, High priority on 1 then round-robin converts SOC10 to 15 */
    AdcRegs.ADCSOC15CTL.bit.TRIGSEL = 8;    /** Set SOC15 start trigger on EPWM2B, High priority on 1 then round-robin converts SOC10 to 15 */

    AdcRegs.ADCSOC0CTL.bit.ACQPS    = 6;    /** Set SOC0 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC1CTL.bit.ACQPS    = 6;    /** Set SOC1 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC2CTL.bit.ACQPS    = 6;    /** Set SOC2 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC3CTL.bit.ACQPS    = 6;    /** Set SOC3 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC4CTL.bit.ACQPS    = 6;    /** Set SOC4 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC5CTL.bit.ACQPS    = 6;    /** Set SOC5 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC6CTL.bit.ACQPS    = 6;    /** Set SOC6 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC7CTL.bit.ACQPS    = 6;    /** Set SOC7 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC8CTL.bit.ACQPS    = 6;    /** Set SOC8 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC9CTL.bit.ACQPS    = 63;   /** Set SOC9 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC10CTL.bit.ACQPS   = 6;    /** Set SOC10 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC11CTL.bit.ACQPS   = 6;    /** Set SOC11 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC12CTL.bit.ACQPS   = 6;    /** Set SOC12 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC13CTL.bit.ACQPS   = 6;    /** Set SOC13 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC14CTL.bit.ACQPS   = 6;    /** Set SOC14 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    AdcRegs.ADCSOC15CTL.bit.ACQPS   = 6;    /** Set SOC15 S/H Window to 7 ADC Clock Cycles, (6 ACQPS plus 1) */
    EDIS;

    /** - Set up Peripheral Interrupt vector table */
    EALLOW;
    PieVectTable.ADCINT1 = &ADCINT_ISR;
    PieVectTable.ADCINT2 = &ADCINT_ISR;
    EDIS;

    EALLOW;
    PieCtrlRegs.PIEIER1.bit.INTx1 = 1;      /** Enable INT 1.1 in the PIE -- ADCINT1 */
    PieCtrlRegs.PIEIER1.bit.INTx2 = 1;      /** Enable INT 1.2 in the PIE -- ADCINT2 */
    IER != M_INT1;                          /** Enable CPU Interrupt 1 */
    EINT;                                   /** Enable Global interrupt INTM */
    ERTM;                                   /** Enable Global realtime interrpt DBGM */
    EDIS;

     /** - Calibrate once more for ADC stability */
    (*Device_cal)();

}
/**
 *  @fn         SwADC_KeyBoard_Conversion(void)
 *
 *  @brief      Copies data out of the ADC results buffer for specified ADC channels
 *
 *  @param      None
 *
 *  @return:    None
 *
 *  @note       The trigger source for SOCx is configured by a combination of the TRIGSEL field in the ADCSOCxCTL
 *              register and the appropriate bits in the ADCINTSOCSEL1 or ADCINTSOCSEL2 register. Software can
 *              also force an SOC event with the ADCSOCFRC1 register. The channel and sample window size for SOCx
 *              are configured with the CHSEL and ACQPS fields of the ADCSOCxCTL register.
 *
 *  @author:    Tom Rode 02/10/2015
 *
 */
void vSwAdc_Keyboard_Conversion(void)
{
    /** ###Functional overview: */

    /** - Force a software SOC event +INV_SENSE V_REF*/ 
    AdcRegs.ADCSOCFRC1.bit.SOC4 = TRUE;

     /** - Place +INV_SENSE V_REF result*/  
    gRaw_Adc.AdcData[4].uwData = (AdcResult.ADCRESULT4);  

    /** - unForce a software SOC event +INV_SENSE V_REF*/ 
    AdcRegs.ADCSOCFRC1.bit.SOC4 = FALSE;  


    /** - Force a software SOC event 2.5 V_REF*/ 
    AdcRegs.ADCSOCFRC1.bit.SOC5 = TRUE;

    /** - Place 2.5V_REF result*/ 
    gRaw_Adc.AdcData[5].uwData = (AdcResult.ADCRESULT5);  

    /** - unForce a software SOC event 2.5 V_REF*/ 
    AdcRegs.ADCSOCFRC1.bit.SOC5 = FALSE;


    /** - Force a software SOC event 227MV_REF*/ 
    AdcRegs.ADCSOCFRC1.bit.SOC6 = TRUE;

    /** - Place 227MV_REF result*/ 
    gRaw_Adc.AdcData[6].uwData = (AdcResult.ADCRESULT6);  

    /** - unForce a software SOC event 227MV_REF */ 
    AdcRegs.ADCSOCFRC1.bit.SOC6 = FALSE;

    /** - Force a software SOC event +12V_Sense*/ 
    AdcRegs.ADCSOCFRC1.bit.SOC8 = TRUE;

    /** - Place +12V_Sense result*/ 
    gRaw_Adc.AdcData[8].uwData = (AdcResult.ADCRESULT8);

    /** - unForce a software SOC event +12V_Sense*/ 
    AdcRegs.ADCSOCFRC1.bit.SOC8 = FALSE;

}





/**
 *  @fn         vCopySeq1_Data
 *
 *  @brief      Copies data out of the ADC results buffer for ADC Sequence 1 channels
 *
 *  @param      None
 *
 *  @return     None
 * 
 *  @note       Data is copied to gRaw_Adc structure     
 *  
 *  @author:    Bill McCroskey, 08/07/2007
 *
 */
#pragma CODE_SECTION(vCopySeq1_Data, "ramfuncs");
void vCopySeq1_Data(void)
{
    /** - Put back into channel order from sample order */
    gRaw_Adc.AdcData[0].uwData = (AdcResult.ADCRESULT0);

    gRaw_Adc.AdcData[2].uwData = (AdcResult.ADCRESULT2);

    gRaw_Adc.AdcData[3].uwData = (AdcResult.ADCRESULT3);

    gRaw_Adc.AdcData[4].uwData = (AdcResult.ADCRESULT4);

    gRaw_Adc.AdcData[5].uwData = (AdcResult.ADCRESULT5);

    gRaw_Adc.AdcData[6].uwData = (AdcResult.ADCRESULT6);

    gRaw_Adc.AdcData[7].uwData = (AdcResult.ADCRESULT7);

    gRaw_Adc.AdcData[8].uwData = (AdcResult.ADCRESULT8);

}

/**
 *  @fn         vCopySeq2_Data
 *
 *  @brief      Copies data out of the ADC results buffer for ADC Sequence 2 channels
 *
 *  @param      None
 *
 *  @return     None
 * 
 *  @note       Data is copied to gRaw_Adc structure     
 *  
 *  @author:    Bill McCroskey, 08/07/2007
 *
 */
#pragma CODE_SECTION(vCopySeq2_Data, "ramfuncs");
void vCopySeq2_Data(void)
{
    /** - Put back into channel order from sample order */
    gRaw_Adc.AdcData[1].uwData = (AdcResult.ADCRESULT1);

    gRaw_Adc.AdcData[9].uwData = (AdcResult.ADCRESULT9);

    gRaw_Adc.AdcData[10].uwData = (AdcResult.ADCRESULT10);

    gRaw_Adc.AdcData[11].uwData = (AdcResult.ADCRESULT11);

    gRaw_Adc.AdcData[12].uwData = (AdcResult.ADCRESULT12);

    gRaw_Adc.AdcData[13].uwData = (AdcResult.ADCRESULT13);

    gRaw_Adc.AdcData[14].uwData = (AdcResult.ADCRESULT14);

    gRaw_Adc.AdcData[15].uwData = (AdcResult.ADCRESULT15);
}

/**
 *  @fn         vProcess8k_Seq1_ADC
 *
 *  @brief      Processes the group of channels on the 8kHz sample block.  Filtering and Eng. Units
 *
 *  @param      None
 *
 *  @return     None
 * 
 *  @note       Data is copied to gRaw_Adc structure
 *
 *  @author:     Bill McCroskey, 08/07/2007
 *
 */
#pragma CODE_SECTION(vProcess8k_Seq1_ADC, "ramfuncs");
void vProcess8k_Seq1_ADC(void)
{
    _iq19 slTemp_q19;

    /** - Process SV1 */
    vFilterADC_Channel( &gADC_Cal.slSV1_I_mA_Count_Average_q19,
                        gRaw_Adc.AdcData[0].uwData,
                        &gADC_Cal.uwSV1_I_mA_Average, ADC_CAL_FILTER);
    slTemp_q19 = mScale_Linear(fprm_SV1_Current_Gain,
                               gRaw_Adc.AdcData[0].uwData,
                               fprm_SV1_Current_Offset);
    slTemp_q19 = slClip(MIN_SV_MA, slTemp_q19, MAX_SV_MA);
    gADC_Scaled.slSV1_I_mA_q19 = slTemp_q19;

    slTemp_q19 = mScale_Linear(fprm_SV1_Current_Gain,
                               gADC_Cal.uwSV1_I_mA_Average,
                               fprm_SV1_Current_Offset);
    slTemp_q19 = slClip(MIN_SV_MA, slTemp_q19, MAX_SV_MA);
    gADC_Scaled.slSV1_I_mA_Average_q19 = slTemp_q19;

    /** - Process Input AN1 */
    vFilterADC_Channel( &gADC_Cal.slInput_AN1_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[2].uwData,
                        &gADC_Cal.uwInput_AN1_Volts_Average, ADC_CAL_FILTER);
    slTemp_q19 = mScale_Linear(fprm_Input_AN1_Gain,
                               gADC_Cal.uwInput_AN1_Volts_Average,
                               fprm_Input_AN1_Offset);
    slTemp_q19 = slClip(MIN_IN_VOLTS, slTemp_q19, MAX_IN_VOLTS);
    gADC_Scaled.slInput_AN1_Volts_q19 = slTemp_q19;

    /** - Process Input AN2 */
    vFilterADC_Channel( &gADC_Cal.slInput_AN2_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[3].uwData,
                        &gADC_Cal.uwInput_AN2_Volts_Average, ADC_CAL_FILTER);
    slTemp_q19 = mScale_Linear(fprm_Input_AN2_Gain,
                               gADC_Cal.uwInput_AN2_Volts_Average,
                               fprm_Input_AN2_Offset);
    slTemp_q19 = slClip(MIN_IN_VOLTS, slTemp_q19, MAX_IN_VOLTS);
    gADC_Scaled.slInput_AN2_Volts_q19 = slTemp_q19;

    /** - Process Battery Volts */
    vFilterADC_Channel( &gADC_Cal.slBattSense_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[4].uwData,
                        &gADC_Cal.uwBattSense_Volts_Average, ADC_CAL_FILTER);
    slTemp_q19 = mScale_Linear(fprm_BattSense_Gain,
                               gADC_Cal.uwBattSense_Volts_Average,
                               fprm_BattSense_Offset);
    slTemp_q19 = slClip(MIN_IN_VOLTS, slTemp_q19, MAX_IN_VOLTS);
    gADC_Scaled.slBattSense_Volts_q19 = slTemp_q19;

    /** - Process 2.5 V reference */
    vFilterADC_Channel( &gADC_Cal.slRef_2_5_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[5].uwData,
                        &gADC_Cal.uwRef_2_5_Volts_Average, ADC_CAL_FILTER);

    /** - Process 227mV reference */
    vFilterADC_Channel( &gADC_Cal.slRef_227_mV_Count_Average_q19,
                        gRaw_Adc.AdcData[6].uwData,
                        &gADC_Cal.uwRef_227_mV_Average, ADC_CAL_FILTER);

    /** - Process -5V supply sense */
    vFilterADC_Channel( &gADC_Cal.slN5_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[7].uwData,
                        &gADC_Cal.uwN5_Volts_Average, ADC_CAL_FILTER);

    slTemp_q19 = mScale_Linear(fprm_AutoCal_Gain,
                               (gADC_Cal.uwN5_Volts_Average - fprm_AutoCal_Offset),
                               0);
    /** - Divide by 4096 */
    slTemp_q19 = slTemp_q19 >> 12;          

    /** - Input Voltage */
    slTemp_q19 = _IQ19mpy(slTemp_q19, ADC_REF_VOLTS); 

    slTemp_q19 = _IQ19mpy(slTemp_q19, fprm_N5_Volts_Gain);
    slTemp_q19 += fprm_N5_Volts_Offset;

    slTemp_q19 = -slTemp_q19;
    slTemp_q19 = slClip(MIN_NEG_IN_VOLTS, slTemp_q19, MAX_NEG_IN_VOLTS);

    gADC_Scaled.slN5_Volts_q19 = slTemp_q19;

    /** - Process +12V supply sense */
    vFilterADC_Channel( &gADC_Cal.slP12_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[8].uwData,
                        &gADC_Cal.uwP12_Volts_Average, ADC_CAL_FILTER);

    slTemp_q19 = mScale_Linear(fprm_AutoCal_Gain,
                               (gADC_Cal.uwP12_Volts_Average - fprm_AutoCal_Offset),
                               0);

    slTemp_q19 = slTemp_q19 >> 12;          // divide by 4096
    slTemp_q19 = _IQ19mpy(slTemp_q19, ADC_REF_VOLTS);  // input voltage

    slTemp_q19 = _IQ19mpy(slTemp_q19, fprm_P12_Volts_Gain);
    slTemp_q19 += fprm_P12_Volts_Offset;

    slTemp_q19 = slClip(MIN_IN_VOLTS, slTemp_q19, MAX_IN_VOLTS);
    gADC_Scaled.slP12_Volts_q19 = slTemp_q19;
}

/************************************************************************************************
 *  Function:   vProcess8k_Seq2_ADC
 *
 *  Purpose:    Processes the group of channels on the 2kHz sample block.  Filtering and Eng. Units
 *
 *  Input:      N/A
 *
 *  Output:     Data is copied to gRaw_Adc structure
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(vProcess8k_Seq2_ADC, "ramfuncs");
void vProcess8k_Seq2_ADC(void)
{
    _iq19 slTemp_q19;

    /** - Process SV2 */
    vFilterADC_Channel( &gADC_Cal.slSV2_I_mA_Count_Average_q19,
                        gRaw_Adc.AdcData[1].uwData,
                        &gADC_Cal.uwSV2_I_mA_Average, ADC_CAL_FILTER);
    slTemp_q19 = mScale_Linear(fprm_SV2_Current_Gain,
                               gRaw_Adc.AdcData[1].uwData,
                               fprm_SV2_Current_Offset);
    slTemp_q19 = slClip(MIN_SV_MA, slTemp_q19, MAX_SV_MA);
    gADC_Scaled.slSV2_I_mA_q19 = slTemp_q19;

    slTemp_q19 = mScale_Linear(fprm_SV2_Current_Gain,
                               gADC_Cal.uwSV2_I_mA_Average,
                               fprm_SV2_Current_Offset);
    slTemp_q19 = slClip(MIN_SV_MA, slTemp_q19, MAX_SV_MA);
    gADC_Scaled.slSV2_I_mA_Average_q19 = slTemp_q19;

    /** - Process Laser 1 supply volts */
    /** - Two point cal. -- supply can be adjusted */
    vFilterADC_Channel( &gADC_Cal.slLAS1_Supply_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[10].uwData,
                        &gADC_Cal.uwLAS1_Supply_Volts_Average, ADC_CAL_FILTER);

    slTemp_q19 = mScale_Linear(fprm_LAS1_Supply_Gain,
                               gADC_Cal.uwLAS1_Supply_Volts_Average,
                               fprm_LAS1_Supply_Offset);
    slTemp_q19 = slClip(MIN_IN_VOLTS, slTemp_q19, MAX_IN_VOLTS);
    gADC_Scaled.slLAS1_Supply_Volts_q19 = slTemp_q19;

    /** - Process Laser 2 supply volts */
    /** - single point cal. -- fixed supply */
    vFilterADC_Channel( &gADC_Cal.slLAS2_Supply_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[11].uwData,
                        &gADC_Cal.uwLAS2_Supply_Volts_Average, ADC_CAL_FILTER);
    slTemp_q19 = mScale_Linear(fprm_AutoCal_Gain,
                               (gADC_Cal.uwLAS2_Supply_Volts_Average - fprm_AutoCal_Offset),
                               0);

    slTemp_q19 = slTemp_q19 >> 12;
    
    /** - Input Voltage */
    slTemp_q19 = _IQ19mpy(slTemp_q19, ADC_REF_VOLTS);  

    slTemp_q19 = _IQ19mpy(slTemp_q19, fprm_LAS2_Supply_Gain);
    slTemp_q19 += fprm_LAS2_Supply_Offset;

    slTemp_q19 = slClip(MIN_IN_VOLTS, slTemp_q19, MAX_IN_VOLTS);
    gADC_Scaled.slLAS2_Supply_Volts_q19 = slTemp_q19;

   
    /** - single point cal. -- fixed supply */
    vFilterADC_Channel( &gADC_Cal.slCAM1_Supply_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[13].uwData,
                        &gADC_Cal.uwCAM1_Supply_Volts_Average, ADC_CAL_FILTER);
    slTemp_q19 = mScale_Linear(fprm_AutoCal_Gain,
                               (gADC_Cal.uwCAM1_Supply_Volts_Average - fprm_AutoCal_Offset),
                               0);
     /** - divide by 4096 */
    slTemp_q19 = slTemp_q19 >> 12;          

    /** - Input voltage */
    slTemp_q19 = _IQ19mpy(slTemp_q19, ADC_REF_VOLTS); 

    slTemp_q19 = _IQ19mpy(slTemp_q19, fprm_CAM1_Supply_Gain);
    slTemp_q19 += fprm_CAM1_Supply_Offset;

    slTemp_q19 = slClip(MIN_IN_VOLTS, slTemp_q19, MAX_IN_VOLTS);
    gADC_Scaled.slCAM1_Supply_Volts_q19 = slTemp_q19;

    /** - single point cal. -- fixed supply */
    vFilterADC_Channel( &gADC_Cal.slCAM2_Supply_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[14].uwData,
                        &gADC_Cal.uwCAM2_Supply_Volts_Average, ADC_CAL_FILTER);
    slTemp_q19 = mScale_Linear(fprm_AutoCal_Gain,
                               (gADC_Cal.uwCAM2_Supply_Volts_Average - fprm_AutoCal_Offset),
                               0);
     /** divide by 4096 */
    slTemp_q19 = slTemp_q19 >> 12;         

     /** input voltage */
    slTemp_q19 = _IQ19mpy(slTemp_q19, ADC_REF_VOLTS);  

    slTemp_q19 = _IQ19mpy(slTemp_q19, fprm_CAM2_Supply_Gain);
    slTemp_q19 += fprm_CAM2_Supply_Offset;

    slTemp_q19 = slClip(MIN_IN_VOLTS, slTemp_q19, MAX_IN_VOLTS);
    gADC_Scaled.slCAM2_Supply_Volts_q19 = slTemp_q19;


    /** Two point cal. -- supply can be adjusted */
    vFilterADC_Channel( &gADC_Cal.slCAM3_Supply_Volts_Count_Average_q19,
                        gRaw_Adc.AdcData[15].uwData,
                        &gADC_Cal.uwCAM3_Supply_Volts_Average, ADC_CAL_FILTER);

    slTemp_q19 = mScale_Linear(fprm_CAM3_Supply_Gain,
                               gADC_Cal.uwCAM3_Supply_Volts_Average,
                               fprm_CAM3_Supply_Offset);
    slTemp_q19 = slClip(MIN_IN_VOLTS, slTemp_q19, MAX_IN_VOLTS);
    gADC_Scaled.slCAM3_Supply_Volts_q19 = slTemp_q19;

}

/************************************************************************************************
 *  Function:   ADCINT_ISR
 *
 *  Purpose:    Interrupt service for the ADC system.  This function runs via interrupt after
 *              the ADC finishes a sequencer run of conversions.  The source of the IRQ (1 or 2)
 *              is checked, and then service for the correct sequence is called.
 *
 *              NOTE:  This function does not run under the DSPbios system.  All code called from
 *              this function can not make use of DSPbios services.  This is one of the highest
 *              priority services on the DSP.  Only power fail response is higher.
 *
 *  Input:      N/A
 *
 *  Output:     Event Manager and ADC control
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
#pragma CODE_SECTION(ADCINT_ISR, "ramfuncs");
interrupt void ADCINT_ISR(void)           // 0x000D4A  ADCINT (ADC)
{
    boolean fSeq1, fSeq2;

    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;     // Must acknowledge the PIE group

    // Save IRQ flags
    fSeq1 = AdcRegs.ADCINTFLG.bit.ADCINT1;   // save bit

    if (fSeq1)
    {
        PAD_E2 = 0; // measure interrupt cpu utilization

        vCopySeq1_Data();
        AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;       // Clear ADC SEQ1 interrupt flag
        vProcess8k_Seq1_ADC();
        vSV_PWM_8K_Seq1_Service();
        PAD_E2 = 1; // measure interrupt cpu utilization
    }

    asm(" NOP");
    fSeq2 = AdcRegs.ADCINTFLG.bit.ADCINT2;   // save bit

    if (fSeq2)
    {
        PAD_E2 = 0; // measure interrupt cpu utilization
        vCopySeq2_Data();
        AdcRegs.ADCINTFLGCLR.bit.ADCINT2 = 1;       // Clear ADC SEQ2 interrupt flag
        vProcess8k_Seq2_ADC();
        vSV_PWM_8K_Seq2_Service();
        PAD_E2 = 1; // measure interrupt cpu utilization
    }
}

/************************************************************************************************
 *  Function:   vADC_Reality_Check
 *
 *  Purpose:    This function is called by the operational state machine to check if the ADC system
 *              has paused.  When debugging with JTAG, DSP halt sequences can cause the ADC system
 *              to end up with both sequencers pending with no pending IRQ flag.  This situation is
 *              checked for and cleared if found.
 *
 *  Input:      N/A
 *
 *  Output:     ADC sequencer reset
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 08/07/2007
 *
 ************************************************************************************************
*/
void vADC_Reality_Check(void)
{
    // reality check on the ADC
    if (( AdcRegs.ADCINTFLG.bit.ADCINT1 == 1 ) &&
        ( AdcRegs.ADCINTFLG.bit.ADCINT1 == 1 ) &&
        ( PieCtrlRegs.PIEIER1.bit.INTx1 == 0) &&
        ( PieCtrlRegs.PIEIER1.bit.INTx2 == 0)
       )
    {
        AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;       // Clear ADC SEQ1 interrupt flag
        AdcRegs.ADCINTFLGCLR.bit.ADCINT1 = 1;       // Clear ADC SEQ2 interrupt flag
    }
}

/*********************************************************************
* Function: DelayUs()                                                *
* Description: Call the function for the micro in use                *
*********************************************************************/
#pragma CODE_SECTION(DelayUs, "ramfuncs");
void DelayUs( volatile Uint16 Usec )
{
    DSP28x_usDelay((Uint32)Usec);

} // end DelayUs()
/*** end of file *****************************************************/
