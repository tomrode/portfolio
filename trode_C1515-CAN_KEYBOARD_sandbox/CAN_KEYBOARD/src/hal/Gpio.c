/**
 *  @file                   Gpio.c
 *  @brief                  This file contains initializations for general purpose pins
 *  @copyright              2012 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   10/10/2014
 *
 *  @remark Author:         Tom Rode
 *  @remark Project Tree:   C1515
 *
 *  @note                   N/A
 *
 */
#include "DSP2803x_Device.h"
#include "DSP2803x_XIntrupt.h"
#include "types.h"
#include "param.h"
#include "gpio.h"
/** - EXAMPLE_BIOS or EXAMPLE_NONBIOS are defined in the CCS project build options */
#ifdef EXAMPLE_BIOS
    #include "example_BIOS.h"
#endif

#ifdef EXAMPLE_NONBIOS
    #include "example_nonBIOS.h"
#endif

/*========================================================================*
*  SECTION - Local variables                                             *
*========================================================================*
*/
/** @brief Row 1 Driver*//*
    @@ INSTANCE  = gRow1Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gRow1Drv;

/** @brief Row 2 Driver*//*
    @@ INSTANCE  = gRow2Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gRow2Drv;

/** @brief Row 3 Driver*//*
    @@ INSTANCE  = gRow3Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gRow3Drv;

/** @brief Row 4 Driver*//*
    @@ INSTANCE  = gRow4Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gRow4Drv;

/** @brief Row 5 Driver*//*
    @@ INSTANCE  = gRow5Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gRow5Drv;

/** @brief Col 1 Input *//*
    @@ INSTANCE  = gCol1Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol1Rcv;

/** @brief Col 2 Input *//*
    @@ INSTANCE  = gCol2Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol2Rcv;

/** @brief Col 3 Input *//*
    @@ INSTANCE  = gCol3Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol3Rcv;

/** @brief Col 4 Input *//*
    @@ INSTANCE  = gCol4Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol4Rcv;

/** @brief Col 5 Input *//*
    @@ INSTANCE  = gCol5Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol5Rcv;

/** @brief Col 6 Input *//*
    @@ INSTANCE  = gCol6Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol6Rcv;

/** @brief Col 7 Input *//*
    @@ INSTANCE  = gCol7Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol7Rcv;

/** @brief Col 8 Input *//*
    @@ INSTANCE  = gCol8Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol8Rcv;

/** @brief Col 9 Input *//*
    @@ INSTANCE  = gCol9Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol9Rcv;

/** @brief Col 10 Input *//*
    @@ INSTANCE  = gCol10Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol10Rcv;

/** @brief Col 11 Input *//*
    @@ INSTANCE  = gCol11Rcv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
gpio_object_t gCol11Rcv;

/*========================================================================*
*  SECTION - Local function prototypes                                   *
*========================================================================*
*/
boolean gfGPIO_Initialize_ROW1_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_ROW2_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_ROW3_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_ROW4_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_ROW5_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL1_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL2_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL3_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL4_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL5_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL6_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL7_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL8_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL9_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL10_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
boolean gfGPIO_Initialize_COL11_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState);
/**
 * @fn     InitGpio()
 *
 * @brief   General Purpose I/O pin configuration for F2803x.
 *  
 * @param   N/A
 *
 * @return  N/A 
 *  
 * @note    Ported over from CIM project, reconfigured for C1515 CAN_KEYBOARD H/W.
 *  
 * @author  Tom Rode
 *
 * @date    10/10/2014  
 *
 */
void InitGpio(void)
{
    /** ###Functional overview: */
    /** - Enable EALLOW protected register access */
    asm(" EALLOW"); 
    
    /** - Set up each port for intended function on this board  */             
    /** - COL1_RCV */
    gfGPIO_Initialize_COL1_RCV(&gCol1Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW); 

    /** - COL2_RCV */
    gfGPIO_Initialize_COL2_RCV(&gCol2Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW); 

    /** - COL3_RCV */
    gfGPIO_Initialize_COL3_RCV(&gCol3Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW); 

    /** - COL4_RCV */
    gfGPIO_Initialize_COL4_RCV(&gCol4Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW); 
 
    /** - COL5_RCV */
    gfGPIO_Initialize_COL5_RCV(&gCol5Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW);  

    /** - COL6_RCV */
    gfGPIO_Initialize_COL6_RCV(&gCol6Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW);  

    /** - COL7_RCV */   
    gfGPIO_Initialize_COL7_RCV(&gCol7Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW);

    /** - COL8_RCV */
    gfGPIO_Initialize_COL8_RCV(&gCol8Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW);

    /** - COL9_RCV */
    gfGPIO_Initialize_COL9_RCV(&gCol9Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW);

    /** - COL10_RCV */
    gfGPIO_Initialize_COL10_RCV(&gCol10Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW);

    /** - COL11_RCV */
    gfGPIO_Initialize_COL11_RCV(&gCol11Rcv, GPIO_MODE_INPUT, GPIO_STATE_LOW);

    /** - GPIO11 -- unused */
    GpioCtrlRegs.GPAMUX1.bit.GPIO11 = 0u;  
    /** - Set as Input */           
    GpioCtrlRegs.GPADIR.bit.GPIO11  = 0u;
    
    /** - GPIO12 -- PFIN */
    GpioCtrlRegs.GPAMUX1.bit.GPIO12 = 0u;  
    /** - Set as Input */           
    GpioCtrlRegs.GPADIR.bit.GPIO12  = 0u;

    /** - GPIO13 -- unused */
    GpioCtrlRegs.GPAMUX1.bit.GPIO13 = 0u;  
    /** - Set as Input */           
    GpioCtrlRegs.GPADIR.bit.GPIO13  = 0u;
    
    /** - GPIO14 -- E6 */
    GpioCtrlRegs.GPAMUX1.bit.GPIO14 = 0u;  
    /** - Set as Output */           
    GpioCtrlRegs.GPADIR.bit.GPIO14  = 1u;
   
    /** - GPIO15 -- E5 */
    GpioCtrlRegs.GPAMUX1.bit.GPIO15 = 0u;  
    /** - Set as Output */           
    GpioCtrlRegs.GPADIR.bit.GPIO15  = 1u;
       
    /** - ROW1_DRV Row 1 Driver */
    gfGPIO_Initialize_ROW1_DRV(&gRow1Drv, GPIO_MODE_OUTPUT, GPIO_STATE_LOW); 
 
    /** - ROW2_DRV Row 2 Driver */
    gfGPIO_Initialize_ROW2_DRV(&gRow2Drv, GPIO_MODE_OUTPUT, GPIO_STATE_LOW); 

    /** - ROW3_DRV Row 3 Driver */
    gfGPIO_Initialize_ROW3_DRV(&gRow3Drv, GPIO_MODE_OUTPUT, GPIO_STATE_LOW); 

    /** - ROW4_DRV Row 4 Driver */
    gfGPIO_Initialize_ROW4_DRV(&gRow4Drv, GPIO_MODE_OUTPUT, GPIO_STATE_LOW);

    /** - ROW5_DRV Row 5 Driver */
    gfGPIO_Initialize_ROW5_DRV(&gRow5Drv, GPIO_MODE_OUTPUT, GPIO_STATE_LOW); 

    EALLOW;
    /** - GPIO21 --  WDOG_EN */
    GpioCtrlRegs.GPAMUX2.bit.GPIO21 = 0u;  
    /** - Set as Output */           
    GpioCtrlRegs.GPADIR.bit.GPIO21  = 0u;

    /** - GPIO22 --  WDOG_TIC */
    GpioCtrlRegs.GPAMUX2.bit.GPIO22 = 0u;  
    /** - Set as Output */           
    GpioCtrlRegs.GPADIR.bit.GPIO22  = 0u;

    /** - GPIO23 --  KEY_EN */
    GpioCtrlRegs.GPAMUX2.bit.GPIO23 = 0u;  
    /** - Set as Output */           
    GpioCtrlRegs.GPADIR.bit.GPIO23  = 1u;
    GpioDataRegs.GPADAT.bit.GPIO23  = 1u;

    /** - GPIO24 --  MOSI (FRAM) */
    GpioCtrlRegs.GPAMUX2.bit.GPIO24 = 3u; 
     
    /** - GPIO25 --  MISO (FRAM) */      
    GpioCtrlRegs.GPAMUX2.bit.GPIO25 = 3u;            

    /** - GPIO26 --  SCLK (FRAM) */ 
    GpioCtrlRegs.GPAMUX2.bit.GPIO26 = 3u; 

    /** - GPIO27 --  FCS (FRAM) */       
    GpioCtrlRegs.GPAMUX2.bit.GPIO27 = 0u;            
    /** - Set as Output */  
    GpioCtrlRegs.GPADIR.bit.GPIO27  = 1u;  
    /** - Set it to on (inactive state) */          
    GpioDataRegs.GPADAT.bit.GPIO27  = 1u;

    /** - GPIO28 --  E3 */    
    GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 0u;           
    /** - Set as Output */  
    GpioCtrlRegs.GPADIR.bit.GPIO28  = 1u;
	/** - Set it to off (inactive state) */
    GpioDataRegs.GPADAT.bit.GPIO28  = 0u;  // E3

    /** - GPIO29 --  E2 */    
    GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 0u;           
    /** - Set as Output */  
    GpioCtrlRegs.GPADIR.bit.GPIO29  = 1u;
       
    /** - GPIO30 --  CANRXD */    
    GpioCtrlRegs.GPAMUX2.bit.GPIO30 = 1u;           

    /** - GPIO31 --  CANTXD */  
    GpioCtrlRegs.GPAMUX2.bit.GPIO31 = 1u;            

    /** - GPIO32 --  unused */ 
    GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 0u;
    /** - Set as Input */            
    GpioCtrlRegs.GPBDIR.bit.GPIO32  = 0u;           
    
    /** - GPIO33 --  unused */ 
    GpioCtrlRegs.GPBMUX1.bit.GPIO33 = 0u;
    /** - Set as Input */            
    GpioCtrlRegs.GPBDIR.bit.GPIO33  = 0u;
  
    /** - GPIO34 --  E1 */ 
    GpioCtrlRegs.GPBMUX1.bit.GPIO34 = 0u;
    /** - Set as Output */            
    GpioCtrlRegs.GPBDIR.bit.GPIO34  = 1u;
   
    /** - GPIO35 - 38 are JTAG */ 
 
    /** - GPIO39 --  LED */ 
    GpioCtrlRegs.GPBMUX1.bit.GPIO39 = 0u;            
    /** - Set as Output */ 
    GpioCtrlRegs.GPBDIR.bit.GPIO39  = 1u;            
    /** - Set it to off (inactive state) */
    GpioDataRegs.GPBDAT.bit.GPIO39  = 0u;  

    /** - GPIO40 --  unused */     
    GpioCtrlRegs.GPBMUX1.bit.GPIO40 = 0u;            
    /** - Set as Input */ 
    GpioCtrlRegs.GPBDIR.bit.GPIO40  = 0u;           

    /** - GPIO41 --  unused */
    GpioCtrlRegs.GPBMUX1.bit.GPIO41 = 0u; 
    /** - Set as Input */            
    GpioCtrlRegs.GPBDIR.bit.GPIO41  = 0u;            

    /** - GPIO42 --  unused */
    GpioCtrlRegs.GPBMUX1.bit.GPIO42 = 0u; 
    /** - Set as Input */            
    GpioCtrlRegs.GPBDIR.bit.GPIO42  = 0u;           

    /** - GPIO43 --  unused */
    GpioCtrlRegs.GPBMUX1.bit.GPIO43 = 0u; 
    /** - Set as Input */            
    GpioCtrlRegs.GPBDIR.bit.GPIO43  = 0u;

    /** - GPIO44 --  BOOT_DETECT (CAN_ID address) */
    GpioCtrlRegs.GPBMUX1.bit.GPIO44 = 0u; 
    /** - Set as Input */        
    GpioCtrlRegs.GPBDIR.bit.GPIO44  = 0u;           

    /** - Disable EALLOW protected register access */     
    asm(" EDIS");     

}
/**
 * @fn      boolean gfGPIO_Initialize_ROW1_DRV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_ROW1_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
               

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - GPIO16 --  ROW1_DRV */
                    GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 0u;  
                    
                    /** - Set as Output */           
                    GpioCtrlRegs.GPADIR.bit.GPIO16 = 1u;

                    /** - Configure pin GPIO16 on TMS320F28035 */
                    pObject->uwPin = GPIO16_28035;

                    /** - Set this port to output */
                    pObject->eMode = GPIO_MODE_OUTPUT;

                }  
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
  
        /** - Set pin to an initial state*/
        if ( GPIO_STATE_HIGH == eInitialState)
        {
            /* Do nothing */
        }
        else if ( GPIO_STATE_LOW == eInitialState)
        {
             EALLOW;    
             /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
             if ( TRUE == gParams.Kt.ubDSP28035 )
             { 
                 /** - Set it to off (inactive state) */
                 GpioDataRegs.GPADAT.bit.GPIO16 = 0u; 
             } 
             EDIS; 
        }
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_ROW2_DRV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_ROW2_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
               

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - GPIO17 --  ROW2_DRV */
                    GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 0u;
  
                    /** - Set as Output */           
                    GpioCtrlRegs.GPADIR.bit.GPIO17 = 1u;

                    /** - Configure pin GPIO17 on TMS320F28035 */
                    pObject->uwPin = GPIO17_28035;

                    /** - Set this port to output */
                    pObject->eMode = GPIO_MODE_OUTPUT;
                }  
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
  
        /** - Set pin to an initial state*/
        if ( GPIO_STATE_HIGH == eInitialState)
        {
            /* Do nothing */
        }
        else if ( GPIO_STATE_LOW == eInitialState)
        {
             EALLOW;    
             /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
             if ( TRUE == gParams.Kt.ubDSP28035 )
             { 
                 /** - Set it to off (inactive state) */
                 GpioDataRegs.GPADAT.bit.GPIO17 = 0u; 
             } 
             EDIS; 
        }
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_ROW3_DRV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_ROW3_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
               

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - GPIO18 --  ROW3_DRV */
                    GpioCtrlRegs.GPAMUX2.bit.GPIO18 = 0u;
  
                    /** - Set as Output */           
                    GpioCtrlRegs.GPADIR.bit.GPIO18 = 1u;

                    /** - Configure pin GPIO18 on TMS320F28035 */
                    pObject->uwPin = GPIO18_28035;

                    /** - Set this port to output */
                    pObject->eMode = GPIO_MODE_OUTPUT; 
                }  
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
  
        /** - Set pin to an initial state*/
        if ( GPIO_STATE_HIGH == eInitialState)
        {
            /* Do nothing */
        }
        else if ( GPIO_STATE_LOW == eInitialState)
        {
             EALLOW;    
             /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
             if ( TRUE == gParams.Kt.ubDSP28035 )
             { 
                 /** - Set it to off (inactive state) */
                 GpioDataRegs.GPADAT.bit.GPIO18 = 0u; 
             } 
             EDIS; 
        }
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_ROW4_DRV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_ROW4_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
               

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - GPIO19 --  ROW4_DRV */
                    GpioCtrlRegs.GPAMUX2.bit.GPIO19 = 0u;
  
                    /** - Set as Output */           
                    GpioCtrlRegs.GPADIR.bit.GPIO19 = 1u;

                    /** - Configure pin GPIO19 on TMS320F28035 */
                    pObject->uwPin = GPIO19_28035;

                    /** - Set this port to output */
                    pObject->eMode = GPIO_MODE_OUTPUT; 

                }  
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
  
        /** - Set pin to an initial state*/
        if ( GPIO_STATE_HIGH == eInitialState)
        {
            /* Do nothing */
        }
        else if ( GPIO_STATE_LOW == eInitialState)
        {
             EALLOW;    
             /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
             if ( TRUE == gParams.Kt.ubDSP28035 )
             { 
                 /** - Set it to off (inactive state) */
                 GpioDataRegs.GPADAT.bit.GPIO19 = 0u; 
             } 
             EDIS; 
        }
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_ROW5_DRV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_ROW5_DRV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                   
                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - GPIO20 --  ROW5_DRV */
                    GpioCtrlRegs.GPAMUX2.bit.GPIO20 = 0u; 
 
                    /** - Set as Output */           
                    GpioCtrlRegs.GPADIR.bit.GPIO20 = 1u;

                    /** - Configure pin GPIO20 on TMS320F28035 */
                    pObject->uwPin = GPIO20_28035;

                    /** - Set this port to output */
                    pObject->eMode = GPIO_MODE_OUTPUT; 
                }  
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
  
        /** - Set pin to an initial state*/
        if ( GPIO_STATE_HIGH == eInitialState)
        {
            /* Do nothing */
        }
        else if ( GPIO_STATE_LOW == eInitialState)
        {
             EALLOW;    
             /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
             if ( TRUE == gParams.Kt.ubDSP28035 )
             { 
                 /** - Set it to off (inactive state) */
                 GpioDataRegs.GPADAT.bit.GPIO20 = 0u; 
             } 
             EDIS; 
        }
    }

    /** - Return status to caller */
    return fSuccess;
}
/**
 * @fn      boolean gfGPIO_Initialize_COL1_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL1_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - COL1_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 0u;
    
                    /** - Set as Input */
                    GpioCtrlRegs.GPADIR.bit.GPIO0  = 0u;
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL2_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL2_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - COL2_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO1  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL3_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL3_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - COL3_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO2  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL4_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL4_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - COL4_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO3  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL5_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL5_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /** - COL5_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO4 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO4  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL6_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL6_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /**  COL6_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO5 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO5  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL7_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL7_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /**  COL6_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO6  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL8_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL8_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /**  COL6_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO7  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL9_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL9_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /**  COL6_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO8 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO8  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL10_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL10_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /**  COL6_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO9 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO9  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}

/**
 * @fn      boolean gfGPIO_Initialize_COL11_RCV(gpio_object_t *pObject, gpio_mode_t eMode, gpio_states_t eInitialState)
 *
 * @brief   This function configures the specified pin for the specified GPIO function
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eMode   = Desired mode
 * @param   eInitialState = Initial output state
 *
 * @return  If initialization was successful or not
 *          - TRUE  = Successful
 *          - FALSE = Failed
 *
 * @author  Tom Rode
 *
 * @note    This function assumes the specified pin is valid and capable of providing the requested
 *          mode of operation.
 */
boolean gfGPIO_Initialize_COL11_RCV(gpio_object_t *pObject,gpio_mode_t eMode,gpio_states_t eInitialState)
{
    boolean fSuccess = TRUE;

    /** ###Functional overview: */

    /** - Check object for null pointer or pin number for null */
    if ( NULL_PTR == pObject )
    {
        fSuccess = FALSE;
    }
    else
    {
        /** - Configure pin mode */
        pObject->eMode = eMode;

        /** - Configuration is mode dependent */
        switch (eMode)
        {
            case GPIO_MODE_INPUT:

                EALLOW;

                /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for input</table> */
                if ( TRUE == gParams.Kt.ubDSP28035 )
                { 
                    /** <table border="0"><tr><td width="35"><td>Pin Configuration Register initialization for output</table> */
                    /**  COL6_RCV */
                    GpioCtrlRegs.GPAMUX1.bit.GPIO10 = 0u;  
    
                    /** - Set as Input */           
                    GpioCtrlRegs.GPADIR.bit.GPIO10  = 0u; 
                }  

                EDIS;

                break;

            case GPIO_MODE_OUTPUT:

                EALLOW;
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
               
                EDIS;

                break;

            default:
                pObject->eMode = GPIO_MODE_NONE;
                fSuccess = FALSE;
                break;
        }
 
    }

    /** - Return status to caller */
    return fSuccess;
}


/**
 * @fn      boolean gfGPIO_SetOutputState(gpio_object_t *pObject, gpio_states_t eState)
 *
 * @brief   This function writes to a GPIO port
 *
 * @param   pObject = Pointer to digital input/output object
 * @param   eState  = State to output
 *
 * @return  If the output was written or not
 *          - TRUE  = Output was written
 *          - FALSE = Output was not written
 *
 * @author  Walter Conley
 *
 * @note    Output is not written if the object pointer is null or the pin is configured
 *          as an input.
 */
boolean gfGPIO_SetOutputState(gpio_object_t *pObject, gpio_states_t eState)
{
    boolean fWriteSuccessful = FALSE;

    /** ###Functional overview: */

    /** - Check object for null pointer */
    if ( NULL_PTR != pObject )
    {
        /** - Check configured mode of operation */
        switch ( pObject->eMode )
        {
            case GPIO_MODE_OUTPUT:
            case GPIO_MODE_BIDIRECTIONAL:
                /** - Write the state to the gpio pin if configured as an output or bidirectional */
                
                /** - Get parameter for specific microprocessor to retrieve contents for switch inputs  */    
                if ( TRUE == gParams.Kt.ubDSP28035 )
                {
                     /** - ROW1_DRV */   
                     if( GPIO16_28035 == pObject->uwPin )
                     { 
                         if ( GPIO_STATE_HIGH == eState )
                         { 
                             /** - SET Hardware True  */
                             GpioDataRegs.GPASET.bit.GPIO16 = TRUE;
                         }
                         else
                         {
                             /** - CLEAR Hardware True  */
                             GpioDataRegs.GPACLEAR.bit.GPIO16 = TRUE;
                         }
                     }
                     /** - ROW2_DRV */   
                     else if( GPIO17_28035 == pObject->uwPin )
                     { 
                         if ( GPIO_STATE_HIGH == eState )
                         { 
                             /** - SET Hardware True  */
                             GpioDataRegs.GPASET.bit.GPIO17 = TRUE;
                         }
                         else
                         {
                             /** - CLEAR Hardware True  */
                             GpioDataRegs.GPACLEAR.bit.GPIO17 = TRUE;
                         }
                     }
                     /** - ROW3_DRV */   
                     else if( GPIO18_28035 == pObject->uwPin )
                     { 
                         if ( GPIO_STATE_HIGH == eState )
                         { 
                             /** - SET Hardware True  */
                             GpioDataRegs.GPASET.bit.GPIO18 = TRUE;
                         }
                         else
                         {
                             /** - CLEAR Hardware True  */
                             GpioDataRegs.GPACLEAR.bit.GPIO18 = TRUE;
                         }
                     }
                     /** - ROW4_DRV */   
                     else if( GPIO19_28035 == pObject->uwPin )
                     { 
                         if ( GPIO_STATE_HIGH == eState )
                         { 
                             /** - SET Hardware True  */
                             GpioDataRegs.GPASET.bit.GPIO19 = TRUE;
                         }
                         else
                         {
                             /** - CLEAR Hardware True  */
                             GpioDataRegs.GPACLEAR.bit.GPIO19 = TRUE;
                         }
                     }
                     /** - ROW5_DRV */   
                     else if( GPIO20_28035 == pObject->uwPin )
                     { 
                         if ( GPIO_STATE_HIGH == eState )
                         { 
                             /** - SET Hardware True  */
                             GpioDataRegs.GPASET.bit.GPIO20 = TRUE;
                         }
                         else
                         {
                             /** - CLEAR Hardware True  */
                             GpioDataRegs.GPACLEAR.bit.GPIO20 = TRUE;
                         }
                     }
                }
                /** - Save state in object */
                pObject->eState = eState;

                /** - Show the write was successful */
                fWriteSuccessful = TRUE;
                break;

            default:
                /** - Skip write if not configured as an output or bidirectional */
                break;
        }
    }

    /** - Return status to caller */
    return fWriteSuccessful;
}

/*** end of file *****************************************************/
