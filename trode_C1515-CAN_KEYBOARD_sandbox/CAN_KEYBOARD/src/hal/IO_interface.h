/**
 *  @file                   IO_interface.h
 *  @brief                  This file contains all Hardware interfacing 
 *  @copyright              2015 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   4/1/2015
 *
 *  @remark Author:         Tom Rode
 *  @remark Project Tree:   C1515
 *
 *  @note                   N/A
 *
 */
#include "gpio.h"
#ifndef  IO_INTERFACE_H
#define  IO_INTERFACE_H 1

/** @def   HW_GPADAT_MSK
 *  @brief GPIO register contents
 */
#define HW_GPADAT_MSK       0x07FF

/** @def   mROW1_DRV 
 *  @brief Hardware output for row 1 
 */   
#define mROW1_DRV(x)           gfGPIO_SetOutputState(&gRow1Drv,(gpio_states_t)(x));  

/** @def   mROW2_DRV 
 *  @brief Hardware output for row 2 
 */   
#define mROW2_DRV(x)           gfGPIO_SetOutputState(&gRow2Drv,(gpio_states_t)(x));

/** @def   mROW3_DRV 
 *  @brief Hardware output for row 3 
 */   
#define mROW3_DRV(x)           gfGPIO_SetOutputState(&gRow3Drv,(gpio_states_t)(x));

/** @def   mROW4_DRV 
 *  @brief Hardware output for row 4 
 */   
#define mROW4_DRV(x)           gfGPIO_SetOutputState(&gRow4Drv,(gpio_states_t)(x));

/** @def   mROW5_DRV 
 *  @brief Hardware output for row 5 
 */   
#define mROW5_DRV(x)           gfGPIO_SetOutputState(&gRow5Drv,(gpio_states_t)(x));
/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */
extern uint16_t vReadKeySwitched28035DSP(void);
#endif /* #ifndef GPIO_H */
