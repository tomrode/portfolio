/************************************************************************************************
 *  File:       Adc.h
 *
 *  Purpose:    Defines the data structures and functions for the ADC I/O module
 *
 *  Project:    C1314
 *
 *  Copyright:  2007 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 8/03/2007
 *
 ************************************************************************************************
*/
#include "types.h"
#include "IQmathLib.h"

#define ADC_usDELAY  1000L
#define PAD_E1 GpioDataRegs.GPBDAT.bit.GPIO32
#ifndef _Adc_h
#ifndef _Adc_h_local
    /*******************************************************************************
     *                                                                             *
     *  G l o b a l   S c o p e   S e c t i o n                                    *
     *                                                                             *
     *******************************************************************************/


    #define MAX_SV_MA   _IQ19(2000)
    #define MIN_SV_MA   _IQ19(-100)
    #define MAX_IN_VOLTS _IQ19(105)
    #define MIN_IN_VOLTS _IQ19(0)
    #define MIN_NEG_IN_VOLTS _IQ19(-10)
    #define MAX_NEG_IN_VOLTS _IQ19(0)
    #define ADC_REF_VOLTS    _IQ19(3.3)

    typedef struct
    {
        uword uwData;
    } ADC_CH_DATA_TYPE;


    typedef struct
    {
        ADC_CH_DATA_TYPE AdcData[16];
    } RAW_ADC_COUNT_TYPE;


#endif
#endif

#ifdef ADC_MODULE

    #ifndef _Adc_h_local
    #define _Adc_h_local 1

    /*******************************************************************************
     *                                                                             *
     *  I n t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/
    // Defines

    // Delay Times

    // Function prototypes
    void InitAdc(void);
    void ADCINT_ISR(void);     // 0x000D4A  ADCINT (ADC)
    void vCopySeq1_Data(void);
    void vCopySeq2_Data(void);
    void vProcess8k_Seq1_ADC(void);      // processes the group of channels on the 8kHz 0 phase sample block
    void vProcess8k_Seq2_ADC(void);      // processes the group of channels on the 8kHz 189 phase sample block
    void vADC_Reality_Check(void);

    // Global variables
    volatile RAW_ADC_COUNT_TYPE gRaw_Adc;   // ADC Results Snapshot Allocation
    volatile uword guwPassCount;
    volatile uword guwADC_Mux_Ch;               // channel selected

    // Macros



    #endif


#else
    #ifndef _Adc_h
    #define _Adc_h 1
    /*******************************************************************************
     *                                                                             *
     *  E x t e r n a l   S c o p e   S e c t i o n                                *
     *                                                                             *
     *******************************************************************************/

    // Externaly visable function prototypes
    extern void InitAdc(void);
    extern void ADCINT_ISR(void);     // 0x000D4A  ADCINT (ADC)
    extern void vADC_Reality_Check(void);
    extern void vSwAdc_Keyboard_Conversion(void);

    // Externaly visable global variables
    extern volatile RAW_ADC_COUNT_TYPE gRaw_Adc;   // ADC Results Snapshot Allocation
    extern volatile uword guwPassCount;
    extern void DSP28x_usDelay(Uint32_t Count);
    #endif
#endif

