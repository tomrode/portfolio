/**
 *  @file                   gpio.h
 *  @brief                  Interface for general purpose inputs and outputs
 *  @copyright              2012 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   4/1/2015
 *
 *  @remark Author:         Walter Conley, Tom Rode
 *  @remark Project Tree:   C1515
 *
 */

#ifndef  GPIO_H
#define  GPIO_H 1

/*========================================================================*
 *  SECTION - Global definitions
 *========================================================================*
 */

/** @def   GPIO16_28035 
 *  @brief TMS320F28035 GPIO pin used for triggering row 1 
 */
#define  GPIO16_28035 16u

/** @def   GPIO17_28035 
 *  @brief TMS320F28035 GPIO pin used for triggering row 2 
 */
#define  GPIO17_28035 17u

/** @def   GPIO18_28035 
 *  @brief TMS320F28035 GPIO pin used for triggering row 3 
 */
#define  GPIO18_28035 18u

/** @def   GPIO19_28035 
 *  @brief TMS320F28035 GPIO pin used for triggering row 4 
 */
#define  GPIO19_28035 19u

/** @def   GPIO20_28035 
 *  @brief TMS320F28035 GPIO pin used for triggering row 5 
 */
#define  GPIO20_28035 20u

/** @enum  gpio_states
 *  @brief General purpose digital input/output pin states
 *
 *  @typedef gpio_states_t
 *  @brief   gpio_states type definition
 */
typedef enum gpio_states
{
    GPIO_STATE_LOW  = 0,    /**< Logic low  (logic 0) read or written to a port pin */
    GPIO_STATE_HIGH = 1     /**< Logic high (logic 1) read or written to a port pin */
}gpio_states_t;

/** @enum  gpio_mode
 *  @brief Modes that a general purpose digital input/output pin can be configured to
 *
 *  @typedef gpio_mode_t
 *  @brief   gpio_mode type definition
 */
typedef enum gpio_mode
{
    GPIO_MODE_NONE = 0,     /**< Pin not configured */
    GPIO_MODE_INPUT,        /**< Pin configured as a digital input */
    GPIO_MODE_OUTPUT,       /**< Pin configured as a digital output */
    GPIO_MODE_BIDIRECTIONAL /**< Pin configured to be bidirectional */
}gpio_mode_t;

/** @struct gpio_object
 *  @brief  Control structure for a general purpose input/output pin
 *
 *  @typedef gpio_object_t
 *  @brief   gpio_object type definition
 */
typedef struct gpio_object
{
    /*
    @@ ELEMENT = eMode
    @@ STRUCTURE = gpio_object_t
    @@ A2L_TYPE = MEASURE
    @@ DATA_TYPE = ULONG
    @@ CONVERSION = TABLE 0 "None" 1 "Input" 2 "Output" 3 "Bidirectional"
    @@ DESCRIPTION = "Mode of operation"
    @@ END
    */
    gpio_mode_t   eMode;    /**< Mode of operation */

    /*
    @@ ELEMENT = uwPin
    @@ STRUCTURE = gpio_object_t
    @@ A2L_TYPE = MEASURE
    @@ DATA_TYPE = UWORD
    @@ DESCRIPTION = "Pin number"
    @@ END
    */
    uint16_t      uwPin;    /**< Pin number */

    /*
    @@ ELEMENT = eState
    @@ STRUCTURE = gpio_object_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = ULONG
    @@ CONVERSION = TABLE 0 "Low" 1 "High"
    @@ DESCRIPTION = "Input or output state"
    @@ END
    */
    gpio_states_t eState;   /**< Input or output state */
}gpio_object_t;

/*========================================================================*
 *  SECTION - extern global variables (minimize global variable use)      *
 *========================================================================*
 */
/** @brief Row 1 Driver*//*
    @@ INSTANCE  = gRow1Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
extern gpio_object_t gRow1Drv;

/** @brief Row 2 Driver*//*
    @@ INSTANCE  = gRow2Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
extern gpio_object_t gRow2Drv;

/** @brief Row 3 Driver*//*
    @@ INSTANCE  = gRow3Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
extern gpio_object_t gRow3Drv;

/** @brief Row 4 Driver*//*
    @@ INSTANCE  = gRow4Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
extern gpio_object_t gRow4Drv;

/** @brief Row 5 Driver*//*
    @@ INSTANCE  = gRow5Drv
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
extern gpio_object_t gRow5Drv;

/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */
extern boolean gfGPIO_Initialize(gpio_object_t *pObject, gpio_mode_t eMode, uint16_t uwPin, gpio_states_t eInitialState);
extern gpio_states_t geGPIO_GetInputState(gpio_object_t *pObject);
extern boolean gfGPIO_SetOutputState(gpio_object_t *pObject, gpio_states_t eState);

#endif /* #ifndef GPIO_H */

