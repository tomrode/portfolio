/**
 *  @file                   IO_interface.c
 *  @brief                  This file contains all Hardware interfacing 
 *  @copyright              2015 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   30/03/2015
 *
 *  @remark Author:         Tom Rode
 *  @remark Project Tree:   C1515
 *
 *  @note                   N/A
 *
 */
#include "DSP2803x_Device.h"
#include "types.h"
#include "IO_interface.h"


/**
 * @fn       vReadKeySwitched28035DSP(void)
 *
 * @brief    This function returns GPADAT from HW register on micro DSP28035 
 *
 * @param    None
 *
 * @return  16-bit value, Only first eleven bits correlating to keyboard switch matrix  
 *
 * @note
 *
 * @author  Tom Rode
 *
 */
uint16_t vReadKeySwitched28035DSP(void)
{
    /** - Hardware register assigned for switch matrix feedback */
    return (GpioDataRegs.GPADAT.all & HW_GPADAT_MSK);
}

/*** end of file *****************************************************/
