/*--------------------------------------------------------------------
       TIMER.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Implementation of timer dependent routines
  --------------------------------------------------------------------*/
                                


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#ifdef WIN32
# define STRICT
# include <windows.h>
#endif
#include "target.h"
#include "DSP2803x_Device.h"     // DSP281x Headerfile Include File

/*--------------------------------------------------------------------*/
/*  Service Scheduling                                                */
/*--------------------------------------------------------------------*/

#include "portab.h"
#include "cos_main.h"
#ifndef __18CXX
# include <stdio.h>
#endif /* __18CXX */
#include "objdict.h"
#if CLIENT_ENABLE == 1
# include "sdoclnt.h"
#endif /* CLIENT_ENABLE == 1 */
#include "sdoserv.h"
#include "timerdef.h"
#include "emcyhndl.h"
#include "target.h"
#include "cancntrl.h"
#include "usrclbck.h"


#if NMT_MASTER_ENABLE == 1
# include "nmtms.h"
#endif /* NMT_MASTER_ENABLE == 1 */

#if ENABLE_LAYER_SERVICES == 1
# if ENABLE_LSS_MASTER == 1
#  include "lss_mst.h"
# else
#  include "lss_slv.h"
# endif /* ENABLE_LSS_MASTER == 1 */
#endif /* ENABLE_LAYER_SERVICES == 1 */

#if STATUS_LEDS_USED == 1
# include "co_led.h"
#endif /* STATUS_LEDS_USED == 1 */



/*--------------------------------------------------------------------*/
/*  variable definitions                                              */
/*--------------------------------------------------------------------*/
STATIC WORD DATA wSystemTime;      /*!< basic period of the timer   */
STATIC WORD DATA wTimerIncrement;  /*!< incremented every interrupt */
STATIC WORD DATA wActivateCounter; /*!< activation time for handler */
STATIC WORD DATA wLastTime;        /*!< last time event for handler */

#if NMT_GUARDING_ENABLE == 1
STATIC BYTE DATA bGuardCnt = 0;    /*!< counter for guarding timer  */
#endif /* NMT_GUARDING_ENABLE == 1 */

#if STATUS_LEDS_USED == 1
# if LED_FLICKER_MODE_ENABLED == 1
STATIC BYTE bLedFlickerTime = 0;
# endif /* LED_FLICKER_MODE_ENABLED == 1 */
STATIC BYTE bLedBasisTime = 0;
#endif /* STATUS_LEDS_USED == 1 */



/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern VComParaArea XDATA gStkGlobDat; /*!< Storage block for non-volatile data. */
extern vModInfo XDATA ModuleInfo;   /* module information */
#if ENABLE_EMCYMSG == 1
extern vEmcyManage XDATA EmcyTable; /* EMCY information */
#endif /* ENABLE_EMCYMSG == 1 */

/* externals from cos_main.c                                            */
#if MAX_TX_PDOS > 0
extern TPDOparameter XDATA mTPDO[MAX_TX_PDOS];   /* TX PDO par.  */
extern CONST VTPDOComParData CODE mTPDOAttr[MAX_TX_PDOS]; /*!< default parameters */
#endif /* MAX_TX_PDOS > 0 */
extern DWORD XDATA lLifeTime;           /* node guarding life time      */
extern BOOLEAN DATA fNodeGuarding;      /* node guarding on/off         */
extern BOOLEAN DATA fMasterGuard;       /* master guarding on/off       */
extern BOOLEAN DATA fGuardingFailure;   /* master guarding alarm        */
extern BOOLEAN DATA fHeartbeatPro;      /* producer heartbeat event     */
extern BOOLEAN DATA fHeartbeatConErr;   /* consumer heartbeat event     */

#if SYNC_PRODUCER == 1
extern PRO_SYNC tSyncPro;               /* SYNC producer information    */
#endif /* SYNC_PRODUCER == 1 */

#if HEARTBEAT_CONSUMER == 1
extern CON_HEART XDATA atHeartbeatCon[]; /* consumer heartbeat info      */
#endif /* HEARTBEAT_CONSUMER == 1 */

#if EXAMPLE_OBJECTS_USED == 1
# if DELAYED_SDO_ENABLE == 1
extern WORD wDlySdoUpTimer;
extern WORD wDlySdoDownTimer;
extern BYTE bDlyUpEvent;
extern BYTE bDlyDownEvent;
# endif /* DELAYED_SDO_ENABLE == 1 */
#endif /* EXAMPLE_OBJECTS_USED == 1 */

#if STATUS_LEDS_USED == 1
# if LED_FLICKER_MODE_ENABLED == 1
extern BYTE fFlickerModeEntered;
# endif /* LED_FLICKER_MODE_ENABLED == 1 */
#endif /* STATUS_LEDS_USED == 1 */


/*--------------------------------------------------------------------*/
/*  function definitions                                              */
/*--------------------------------------------------------------------*/

/*!
  \brief Increment system timer.

  This function is called from the hardware timer interrupt service routine.
  The pending interrupt is cleared, the system timer is incremented by the
  basic period and the hardware timer is reloaded again.
*/
#if TIME_FROM_OS == 1
#else
#ifdef __MWERKS__
# ifdef __m56800__
#  pragma interrupt called
# endif /* __m56800__ */
#endif /* __MWERKS__ */
void Tim_PIT(void)
{
  Timer_ClearPendingInterrupt();
  Tgt_EnableInterrupts();
  wTimerIncrement = (WORD)(wTimerIncrement + wSystemTime);
  Tim_ReloadTimer();
} /* Tim_PIT */
#endif /* TIME_FROM_OS == 1 */


/*!
  \brief Initialize system timer.

  This function is called from the protocol stack to set up the system timer.
  An optional hardware timer is configured.

  \param wTime - basic period of the system timer
*/
void Tim_InitTimer(WORD wTime)
{
  wSystemTime = wTime;
#if TIME_FROM_OS == 1
#else
  Tim_ConfigureTimer();
#endif /* TIME_FROM_OS == 1 */
} /* Tim_InitTimer */


/*!
  \brief Start system timer.

  This function is called from the protocol stack to start the system timer.
  An optional hardware timer is enabled.

  \retval TRUE - timer is running
  \retval FALSE - timer not successful started
*/
BOOLEAN Tim_StartTimer(void)
{
  wActivateCounter = 1;
  wLastTime = 0;
#if TIME_FROM_OS == 1
#else
  Tim_EnableTimer();
#endif /* TIME_FROM_OS == 1 */
  return TRUE;
} /* Tim_StartTimer */


/*!
  \brief Stop system timer.

  This function is called from the protocol stack to stop the system timer.
  An optional hardware timer is disabled.

  \retval TRUE - timer has been stopped
  \retval FALSE - timer not stopped
*/
BOOLEAN Tim_StopTimer(void)
{
#if TIME_FROM_OS == 1
#else
    Tim_DisableTimer();
#endif /* TIME_FROM_OS == 1 */
  return TRUE;
} /* Tim_StopTimer */


/*!
  \brief Get the current time.

  This function is called from the protocol stack to get the current value
  of the system timer.

  \note To prevent mixed bytes of old and new values in wTimerIncrement
  on eight bit machines ensure no interrupts are possible by the timer.

  \return current time
*/
WORD Tim_GetCurrentTime(void)
{
#if TIME_FROM_OS == 1
# ifdef WIN32
  return (WORD)GetTickCount();
# else
  return (WORD)GetOsTicks();
# endif /* WIN32 */
#else
# ifdef __18CXX
  WORD DATA retval;

  INTCON &= ~TMR0IE_ENABLED; /* disable TMR0 interrupts */
  retval = wTimerIncrement; /* get 16 bit timer value */
  INTCON |= TMR0IE_ENABLED;  /* enable TMR0 interrupts  */
  return retval; /* return consitent timer value */
# else
#  ifdef __C51__
  WORD DATA retval;

  Tim_DisableTimer();
  retval = wTimerIncrement;
  Tim_EnableTimer();
  return retval;
#  else
  return wTimerIncrement;
#  endif /* __C51__ */
# endif /* __18CXX*/
#endif /* else TIME_FROM_OS == 1 */
} /* Tim_GetCurrentTime */


/*!
  \brief Handle time controlled features.

  All time dependent things of the protocol stack are handled in this function.
  If the timer has been expired, the following has to be done:
  - monitoring of the inhibit time
  - monitoring of the PDO event time
  - guarding/heartbeat checking
  - SDO timeout checking
*/
void TimerHandler(void)
{
#if MAX_TX_PDOS > 0
  BYTE DATA i;                  /* counter */
#else
# if HEARTBEAT_CONSUMER == 1
  BYTE DATA i;                  /* counter */
# endif /* HEARTBEAT_CONSUMER == 1 */
#endif /* MAX_TX_PDOS > 0 */
  BYTE DATA f;                  /* flag */
  WORD DATA wLocalTime;         /* actual time sampled during function call */
  WORD DATA wRealDifference = 0;/* difference to last function call */

  /* Check, if there is at least one timer expiration expected */
  wLocalTime = Tim_GetCurrentTime();

  f = FALSE;                    /* timer not expired */

  /* Calculate time difference to last function call */
  if (wActivateCounter > wLastTime) {
    if (wLocalTime >= wActivateCounter) {
      /* get expired time span */
      wRealDifference = (WORD)(wLocalTime - wLastTime);
      f = TRUE;               /* timer expired */
    } /* if */
    if (wLocalTime < wLastTime) {
      /* get expired time span */
      wRealDifference = (WORD)(wLocalTime + ~wLastTime + 1);
      f = TRUE;               /* timer expired */
    } /* if */
  } /* if */
  else {
    if ((wLocalTime >= wActivateCounter)
        && (wLocalTime < wLastTime)) {
      /* get expired time span */
      wRealDifference = (WORD)(wLocalTime + ~wLastTime + 1);
      f = TRUE;               /* timer expired */
    } /* if */
  } /* else */

  if (f) {
    /* refresh last time */
    wLastTime = wLocalTime;
    /* This is the next time we expect the timer to expire */
#if BASIC_TIME == COMM_TIMER_CYCLE
    wActivateCounter = (WORD)(wLocalTime + COMM_TIMER_CYCLE);
#else
    wActivateCounter = (WORD)(wLocalTime + COMM_TIMER_CYCLE - BASIC_TIME);
#endif
  } /* if */
#if STATUS_LEDS_USED == 1
# if LED_FLICKER_MODE_ENABLED == 1
  /* flickering mode activated? */
  if (fFlickerModeEntered == TRUE) {
    if (bLedFlickerTime > wRealDifference) {
      /* decrease timer */
      bLedFlickerTime = bLedFlickerTime - wRealDifference;
    }
    else {
      /* flickering timer expired */
      bLedFlickerTime = FLICKER_TIME_INT; /* reload timer */
      gLED_ChkToggleTime();
    } /* else */
  } /* if */
  else
# endif /* LED_FLICKER_MODE_ENABLED == 1 */
  /* regular mode timing */
  {
    if (bLedBasisTime > wRealDifference) {
      /* decrease timer */
      bLedBasisTime = bLedBasisTime - wRealDifference;
    }
    else {
      /* LED timer expired: check toggle condition  */
      bLedBasisTime = LED_TIMER_INT; /* reload timer */
      gLED_ChkToggleTime();
    }
  } /* else */
#endif /* STATUS_LEDS_USED == 1 */

  if (ModuleInfo.bModuleState == STACK_ENABLED) {
    if (f) {                                /* timer expired */
#if MAX_TX_PDOS > 0
      for (i = 0; i < MAX_TX_PDOS; i++) {   /* TX PDO messages */
        /* control inhibit time */
        if (mTPDO[i].wInhibitTime > (wRealDifference * 10)) {
          mTPDO[i].wInhibitTime =
            (WORD)(mTPDO[i].wInhibitTime - (wRealDifference * 10));
        } /* if */
        else {
          mTPDO[i].wInhibitTime = 0;
        } /* else */
        /* control event time */
        if (TPDO_PARAM[i].wEventReload != 0) {
          if (mTPDO[i].wEventTimer > wRealDifference) {
            mTPDO[i].wEventTimer =
              (WORD)(mTPDO[i].wEventTimer - wRealDifference);
          } /* if */
          else {
            mTPDO[i].wEventTimer = TPDO_PARAM[i].wEventReload;
            mTPDO[i].bNewValue = TRUE;
          } /* else */
        } /* if */
      } /* for */
#endif /* MAX_TX_PDOS > 0 */
  
#if ENABLE_EMCYMSG == 1
      /* EMCY message */
      if (EmcyTable.wbEmcyInhTime.wc > (wRealDifference * 10)) {
        EmcyTable.wbEmcyInhTime.wc =
          (WORD)(EmcyTable.wbEmcyInhTime.wc - (wRealDifference * 10));
      } /* if */
      else {
        EmcyTable.wbEmcyInhTime.wc = 0;
      } /* else */
#endif /* ENABLE_EMCYMSG == 1 */

      /* handle master guarding */
#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
      if (fMasterGuard == ON) {
        /* guard message */
        if (lLifeTime > (DWORD)wRealDifference) {
          lLifeTime = lLifeTime - (DWORD)wRealDifference;
        } /* if */
        else {
          lLifeTime = 0;
          fMasterGuard = OFF;
          fGuardingFailure = TRUE;  /* node indication */
        } /* else */
      } /* if */
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */

      /* handle producer heartbeat */
#if ENABLE_NMT_ERROR_CTRL > 1
      if (fNodeGuarding == OFF) {
        if (gStkGlobDat.tHeartbeatPro.wTime > wRealDifference) {
          gStkGlobDat.tHeartbeatPro.wTime =
            (WORD)(gStkGlobDat.tHeartbeatPro.wTime - wRealDifference);
        } /* if */
        else {
          gStkGlobDat.tHeartbeatPro.wTime = gStkGlobDat.tHeartbeatPro.wReloadTime.wc;
          fHeartbeatPro = ON;
        } /* else */
      } /* if */
#endif /* ENABLE_NMT_ERROR_CTRL > 1 */

      /* handle SYNC producer functionality */
#if SYNC_PRODUCER == 1
      if ((tSyncPro.bRun == ON) && (tSyncPro.wReloadTime != 0)) {
        /* SYNC producer enabled */
        if (tSyncPro.wTime > wRealDifference) {
          tSyncPro.wTime = (WORD)(tSyncPro.wTime - wRealDifference);
        } /* if */
        else {
          gNmt_SyncTimer(); /* transmit SYNC */
          tSyncPro.wTime = tSyncPro.wReloadTime; /* next cycle */
        } /* else */
      } /* if */
#endif /* SYNC_PRODUCER == 1 */

      /* handle consumer heartbeat */
#if HEARTBEAT_CONSUMER == 1
      /* check all assigned slots */
      for (i = 0; i < NUM_OF_HB_CONSUMERS; i++) {
        if (atHeartbeatCon[i].bRun == ON) {
          if (atHeartbeatCon[i].wTime > wRealDifference) {
            atHeartbeatCon[i].wTime =
              (WORD)(atHeartbeatCon[i].wTime - wRealDifference);
          } /* if */
          else {
            atHeartbeatCon[i].bRun = OFF;
            /* set consumer heartbeat event */
            fHeartbeatConErr = ON;
            /* branch to callback funktion */
# if (SIGNAL_HEARTBEAT_EVENT==1) && (SIGNAL_STACK_EVENT==1)
            gSlvStackEventCallback(HB_EVT,
              gStkGlobDat.atHeartbeatCon[i].bNodeID);
# endif /* (SIGNAL_HEARTBEAT_EVENT==1) && (SIGNAL_STACK_EVENT==1) */
          } /* else */
        } /* if */
      } /* for */
#endif /* HEARTBEAT_CONSUMER == 1 */

      /* handle NMT master node guarding */
#if NMT_GUARDING_ENABLE == 1
      if (++bGuardCnt == GUARD_TIME_FACTOR) { 
        gNdm_GuardTimer(); /* check guarding timer */
        bGuardCnt = 0; /* reset counter for guarding timer */
      } /* if */
#endif /* NMT_GUARDING_ENABLE == 1 */

#if EXAMPLE_OBJECTS_USED == 1
# if DELAYED_SDO_ENABLE == 1
      /* delayed SDO data access example */
      if (bDlyUpEvent & 0x80) {
        if (wDlySdoUpTimer > (wRealDifference)) {
          wDlySdoUpTimer = (WORD)(wDlySdoUpTimer - (wRealDifference));
        } /* if */
        else {
          bDlyUpEvent = 0x01;
          wDlySdoUpTimer = 0;
        } /* else */
      } /* if */

      if (bDlyDownEvent & 0x80) {
        if (wDlySdoDownTimer > (wRealDifference)) {
          wDlySdoDownTimer = (WORD)(wDlySdoDownTimer - (wRealDifference));
        } /* if */
        else {
          bDlyDownEvent = 0x01;
          wDlySdoDownTimer = 0;
        } /* else */
      } /* if */
# endif /* DELAYED_SDO_ENABLE == 1 */
#endif /* EXAMPLE_OBJECTS_USED == 1 */

#if CLIENT_ENABLE == 1
      gSdoC_ChkTimeOut(wRealDifference); /* check client timeout */
#endif /* CLIENT_ENABLE == 1 */

      gSdoS_ChkTimeOut(wRealDifference); /* check server timeout */
    } /* if time elapsed */
  } /* if stack enabled */
#if ENABLE_LAYER_SERVICES == 1
# if LSS_IMPLEMENTATION_LVL_2 == 1
  else if (ModuleInfo.bModuleState == STACK_DEFERRED) {
    if (f) { /* timer expired */
      gLssActivateTimer(wRealDifference);
    } /* if */
  } /* else if */
# endif /* LSS_IMPLEMENTATION_LVL_2 == 1 */
# if ENABLE_LSS_MASTER == 1
  else {
    if (f) { /* timer expired */
      gLssTimer(wRealDifference);
    } /* if */    
  } /* else */
# endif /* ENABLE_LSS_MASTER == 1 */
#endif /* ENABLE_LAYER_SERVICES == 1 */
} /* TimerHandler */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Implementation of timer dependent routines.
  \par File name timer.c
  \version 34
  \date 1.08.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

