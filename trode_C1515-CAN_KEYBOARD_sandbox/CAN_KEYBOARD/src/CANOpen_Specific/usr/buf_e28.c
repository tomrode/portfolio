/*--------------------------------------------------------------------
       BUF_E28.C
  --------------------------------------------------------------------
       Copyright (c) 1998-2005 Vector Informatik GmbH, Stuttgart

       Function: Basic routines to filter and store CAN messages
                 using the TI TMS320F28x eCAN
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/

#include "portab.h"
#include "cos_main.h"
#include "target.h"
#include "cancntrl.h"
#include "buffer.h"
#include "InputSwitches.h"

/*--------------------------------------------------------------------*/
/*  #define statement validation section                              */
/*--------------------------------------------------------------------*/

#if MAX_CAN_BUFFER > 32
# error "Number of available CAN buffers exceeded !"
#endif /* MAX_CAN_BUFFER > 32 */

#if MAX_RX_PDOS > 15
# error "Number of configurable receive PDOs exceeded !"
#endif /* MAX_RX_PDOS > 15 */

#if MAX_TX_PDOS > 15
# error "Number of configurable transmit PDOs exceeded !"
#endif /* MAX_TX_PDOS > 15 */

/*--------------------------------------------------------------------*/
/*  local definitions                                                 */
/*--------------------------------------------------------------------*/
/** - Debug LED used for Passive error test */
//#define MOD_LED_EN 1

/*! CAN controller memory address */
#define CAN_BASIC_ADR   0x6000

/*! use workaround for TI errata SPRZ193H */
#define SPRZ193H        1

/*! use 32-bit only access to the eCAN module */
#define USE_32_BIT_ACCESS 1

/* CAN register control segment */
#define CAN_ME          0x00
#define CAN_MD          0x02
#define CAN_TRS         0x04
#define CAN_TRR         0x06
#define CAN_TA          0x08
#define CAN_AA          0x0A
#define CAN_RMP         0x0C
#define CAN_RML         0x0E
#define CAN_RFP         0x10
#define CAN_MC          0x14
#define CAN_BTC         0x16
#define CAN_ES          0x18
#define CAN_TEC         0x1A
#define CAN_REC         0x1C
#define CAN_GIF0        0x1E
#define CAN_GIM         0x20
#define CAN_GIF1        0x22
#define CAN_MIM         0x24
#define CAN_MIL         0x26
#define CAN_OPC         0x28
#define CAN_TIOC        0x2A
#define CAN_RIOC        0x2C
#define CAN_LNT         0x2E
#define CAN_TOC         0x30
#define CAN_TOS         0x32

/* CAN message buffer organisation */
#define CAN_MID         0x00
#define CAN_MCF         0x02
#define CAN_MDL         0x04
#define CAN_MDH         0x06
#define CAN_MSG_OFFSET  0x08 /*!< message object distance */
#define CAN_LAM         0x40
#define CAN_MOTS        0x80
#define CAN_MOTO        0xC0

/* CAN message buffer location */
#define CAN_FIRST_BUF   0x0100

/* CAN controller flags */
#define CAN_FLAG_SUSP   0x00010000UL
#define CAN_FLAG_MBCC   0x00008000UL
#define CAN_FLAG_TCC    0x00004000UL
#define CAN_FLAG_SCB    0x00002000UL
#define CAN_FLAG_CCR    0x00001000UL
#define CAN_FLAG_PDR    0x00000800UL
#define CAN_FLAG_DBO    0x00000400UL
#define CAN_FLAG_WUBA   0x00000200UL
#define CAN_FLAG_CDR    0x00000100UL
#define CAN_FLAG_ABO    0x00000080UL
#define CAN_FLAG_STM    0x00000040UL
#define CAN_FLAG_SRES   0x00000020UL

#define CAN_FLAG_FE     0x01000000UL
#define CAN_FLAG_BE     0x00800000UL
#define CAN_FLAG_SA1    0x00400000UL
#define CAN_FLAG_CRCE   0x00200000UL
#define CAN_FLAG_SE     0x00100000UL
#define CAN_FLAG_ACKE   0x00080000UL
#define CAN_FLAG_BO     0x00040000UL
#define CAN_FLAG_EP     0x00020000UL
#define CAN_FLAG_EW     0x00010000UL
#define CAN_FLAG_SMA    0x00000020UL
#define CAN_FLAG_CCE    0x00000010UL
#define CAN_FLAG_PDA    0x00000008UL
#define CAN_FLAG_RM     0x00000002UL
#define CAN_FLAG_TM     0x00000001UL

#define CAN_FLAG_IDE    0x80000000UL
#define CAN_FLAG_AME    0x40000000UL
#define CAN_FLAG_AAM    0x20000000UL

#define CAN_FLAG_BOFF   0x00000400UL
#define CAN_FLAG_PASV   0x00000200UL
#define CAN_FLAG_WARN   0x00000100UL

#define PIEACK          *((volatile int *)0x0CE1)

#if 0
/* values defined for 150 Mhz Vector Values*/
DWORD CONST tCCanBtr[9] = {
  0x00C70052, /*  10K bit/s, do not use */
  0x00C70052, /*  20K bit/s, do not use */
  0x00C70052, /*  50K bit/s */
  0x00630052, /* 100K bit/s */
  0x004F0052, /* 125K bit/s */
  0x00270052, /* 250K bit/s */
  0x00130052, /* 500K bit/s */
  0x000A0062, /* 800K bit/s */
  0x00090052  /*   1M bit/s */
};

#endif

/* values defined for 30 Mhz Crown Values */
/*
TI DSP 28035            Input Clock     60.0E+6
Rate    Clock PS    Tseg1   Tseg2   Jump Width  Ratio   BTR Value   Code    Baud
1M      1           10      4       2           73.33%  0x1014B     8       1.000E+6
800K    1           13      4       2           77.78%  0x10163     7       833.333E+3
500K    3           10      4       2           73.33%  0x3014B     6       500.000E+3
400K    4           10      4       2           73.33%  0x4014B     5       400.000E+3
250K    5           13      6       2           70.00%  0x50165     4       250.000E+3
200K    5           16      8       2           68.00%  0x5017F     3       200.000E+3
125K    9           16      7       2           70.83%  0x9017E     2       125.000E+3
100K    11          16      8       2           68.00%  0xB017F     1       100.000E+3
50K     23          16      8       2           68.00%  0x17017F    0       50.000E+3
*/

DWORD CONST tCCanBtr[9] = {
  0x0017017F, /*  50K bit/s */
  0x000B017F, /* 100K bit/s */
  0x0009017E, /* 125K bit/s */
  0x0005017F, /* 200K bit/s */
  0x00050165, /* 250K bit/s */
  0x0004014B, /* 400K bit/s */
  0x0003014B, /* 500K bit/s */
  0x00010163, /* 800K bit/s */
  0x0001014B, /*   1M bit/s */
};


/*--------------------------------------------------------------------*/
/*  macros                                                            */
/*--------------------------------------------------------------------*/

#if SPRZ193H == 0
/* read one word from memory */
#define ReadWord(adr)               ((WORD)(*(WORD volatile *)(adr)))
/* write one word to memory */
#define WriteWord(dst, src)         (*(WORD volatile *)(dst)=((WORD)src))
/* read one double word from memory */
#define ReadDword(adr)              ((DWORD)(*(DWORD volatile *)(adr)))
/* write one double word to memory */
#define WriteDword(dst, src)        (*(DWORD volatile *)(dst)=((DWORD)src))
/* set bit at memory location */
#define SetBitValueW(adr, val)      ((*(WORD volatile *)(adr)) |= (WORD)(val))
/* set bit at memory location */
#define SetBitValueD(adr, val)      {\
                                      register DWORD lIoData;\
                                      lIoData = *(DWORD volatile *)(adr);\
                                      lIoData |= (DWORD)(val);\
                                      *(DWORD volatile *)(adr) = lIoData;\
                                    }
/* reset bit at memory location */
#define ResetBitValueW(adr, val)    ((*(WORD volatile *)(adr)) &= ~((WORD)(val)))
/* reset bit at memory location */
#define ResetBitValueD(adr, val)    {\
                                      register DWORD lIoData;\
                                      lIoData = *(DWORD volatile *)(adr);\
                                      lIoData &= ~((DWORD)(val));\
                                      *(DWORD volatile *)(adr) = lIoData;\
                                    }
/* test bit at memory location */
#define TestBitValueW(adr, val)      (((*(WORD volatile *)(adr)) & (WORD)(val)) ? TRUE : FALSE)
/* test bit at memory location */
#define TestBitValueD(adr, val)      (((*(DWORD volatile *)(adr)) & (DWORD)(val)) ? TRUE : FALSE)
#endif /* SPRZ193H == 0 */


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/

extern vModInfo XDATA ModuleInfo;       /* module information */
extern BOOLEAN DATA fEmcyOverrun;       /* EMCY overrun flag */
#if SYNC_USED == 1
# if MAX_RX_PDOS > 0
extern VComParaArea XDATA gStkGlobDat; /* Storage block for non-volatile data */
extern CONST VRPDOComParData CODE mRPDOAttr[MAX_RX_PDOS]; /* default parameters */
# endif /* MAX_RX_PDOS > 0 */
#endif /* SYNC_USED == 1 */


/*--------------------------------------------------------------------*/
/*  private data                                                      */
/*--------------------------------------------------------------------*/

STATIC BYTE bFirstMsgTrans; /*!< after first transmission this is set to TRUE */
STATIC BYTE bCanStatus;     /*!< current CAN controller status */
#if SYNC_USED == 1
# if MAX_RX_PDOS > 16
STATIC DWORD SyncPdoRxSta;  /*!< status of synchronized receive PDOs          */
# else
#  if MAX_RX_PDOS > 8
STATIC WORD SyncPdoRxSta;   /*!< status of synchronized receive PDOs          */
#  else
#   if MAX_RX_PDOS > 0
STATIC BYTE SyncPdoRxSta;   /*!< status of synchronized receive PDOs          */
#   endif /* MAX_RX_PDOS > 0 */
#  endif /* MAX_RX_PDOS > 8 */
# endif /* MAX_RX_PDOS > 16 */
#endif /* SYNC_USED == 1 */

/*!
  We must ensure that our ISR is only executed after the init routine
  was called. Otherwise it is possible that we find not initalised structures.
  This will crash the stack. Imagine a reset of the microcontroller but NOT
  of the CAN controller.
*/
STATIC BYTE bISRInitiated = FALSE;

/*! CAN message buffer */
FAR CAN_BUF XDATA tCanMsgBuffer[MAX_CAN_BUFFER];


/*--------------------------------------------------------------------*/
/*  private functions                                                 */
/*--------------------------------------------------------------------*/

#if SPRZ193H == 1
#if USE_32_BIT_ACCESS == 0
/*!
  Read one word from memory.

  \param adr - address to read from
  \return value at given address
*/
#pragma CODE_SECTION(ReadWord, "ramfuncs");
WORD ReadWord(DWORD adr)
{
  WORD rval;

  asm(" PUSH  ST1 ");  /* save status register on stack */
  asm(" SETC  INTM "); /* disable all interrupts */
  /* get value */
  rval = *(WORD volatile *)adr;
  if (!rval) {
    rval = *(WORD volatile *)adr;
  } /* if */
  asm(" POP   ST1 ");  /* restore status register */
  return rval;
}
#endif

/*!
  Read one double word from memory.

  \param adr - address to read from
  \return value at given address
*/
#pragma CODE_SECTION(ReadDword, "ramfuncs");
DWORD ReadDword(DWORD adr)
{
  DWORD rval;

  asm(" PUSH  ST1 ");  /* save status register on stack */
  asm(" SETC  INTM "); /* disable all interrupts */
  /* get value */
  rval = *(DWORD volatile *)adr;
  if (!rval) {
    rval = *(DWORD volatile *)adr;
  } /* if */
  asm(" POP   ST1 ");  /* restore status register */
  return rval;
}

#if USE_32_BIT_ACCESS == 0
/*!
  Write one word to memory.

  \param dst - address to write to
  \param src - content to write
*/
#pragma CODE_SECTION(WriteWord, "ramfuncs");
void WriteWord(DWORD dst, WORD src)
{
  asm(" PUSH  ST1 ");  /* save status register on stack */
  asm(" SETC  INTM "); /* disable all interrupts */
  *(WORD volatile *)dst = src; /* set value first time */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  *(WORD volatile *)dst = src; /* set value second time */
  asm(" POP   ST1 ");  /* restore status register */
}
#endif

/*!
  Write one double word to memory.

  \param dst - address to write to
  \param src - content to write
*/
#pragma CODE_SECTION(WriteDword, "ramfuncs");
void WriteDword(DWORD dst, DWORD src)
{
  asm(" PUSH  ST1 ");  /* save status register on stack */
  asm(" SETC  INTM "); /* disable all interrupts */
  *(DWORD volatile *)dst = src; /* set value first time */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  asm(" NOP ");        /* wait a cpu cycle */
  *(DWORD volatile *)dst = src; /* set value second time */
  asm(" POP   ST1 ");  /* restore status register */
}

#if USE_32_BIT_ACCESS == 0
/*!
  Set bit at memory location (word).

  \param dst - address to write to
  \param val - bits to set
*/
static void SetBitValueW(DWORD dst, WORD val)
{
  WriteWord(dst, ReadWord(dst) | val);
}
#endif

/*!
  Set bit at memory location (double word).

  \param dst - address to write to
  \param val - bits to set
*/
static void SetBitValueD(DWORD dst, DWORD val)
{
  WriteDword(dst, ReadDword(dst) | val);
}

#if USE_32_BIT_ACCESS == 0
/*!
  Reset bit at memory location (word).

  \param dst - address to write to
  \param val - bits to clear
*/
static void ResetBitValueW(DWORD dst, WORD val)
{
  WriteWord(dst, ReadWord(dst) & ~val);
}
#endif

/*!
  Reset bit at memory location (double word).

  \param dst - address to write to
  \param val - bits to clear
*/
static void ResetBitValueD(DWORD dst, DWORD val)
{
  WriteDword(dst, ReadDword(dst) & ~val);
}

#if USE_32_BIT_ACCESS == 0
/*!
  Test bit at memory location (word).

  \param adr - address to read from
  \param val - bits to test
  \retval TRUE - bit is set
  \retval FALSE - bit is reset
*/
static WORD TestBitValueW(DWORD adr, WORD val)
{
  return (ReadWord(adr) & val) ? TRUE : FALSE;
}
#endif

/*!
  Test bit at memory location (double word).

  \param adr - address to read from
  \param val - bits to test
  \retval TRUE - bit is set
  \retval FALSE - bit is reset
*/
static WORD TestBitValueD(DWORD adr, DWORD val)
{
  return (ReadDword(adr) & val) ? TRUE : FALSE;
}
#endif /* SPRZ193H == 1 */


/*--------------------------------------------------------------------*/
/*  public functions                                                  */
/*--------------------------------------------------------------------*/

/*!
  Initialize the CAN management structures.
*/
void gCan_Init(void)
{
  bFirstMsgTrans = FALSE;  /* no transmission up to now */
  bCanStatus = CAN_STS_NORMAL; /* reset status information */
#if SYNC_USED == 1
# if MAX_RX_PDOS > 0
  SyncPdoRxSta = 0;            /* reset sync RX PDO status    */
# endif /* MAX_RX_PDOS > 0 */
#endif /* SYNC_USED == 1 */
} /* gCan_Init */


/*!
  \brief Set new mode of operation.

  \param bNewMode - new mode of operation
  \retval TRUE  - new mode established
  \retval FALSE - new mode refused
*/
BYTE gCan_ActivateMode(BYTE bNewMode)
{
  BYTE bChangeOk = TRUE;

  switch (bNewMode) {
    case MODE_CONFIG:
      asm(" eallow"); /* disable protection */
      /* configuration mode request */
      WriteDword(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_CCR);
      while (!TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_CCE));
      /* software reset */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_SRES);
      /* select HECC mode */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_SCB);
      asm(" edis"); /* enable protection */
      bCanStatus = CAN_STS_RESET;
      break;

    case MODE_NORMAL:
      asm(" eallow"); /* disable protection */
      /* CAN data stored in little-endian order */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_DBO);
      /* CAN in free run mode */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_SUSP);
      /* disable loop back self test mode */
      ResetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_STM);
      /* controller wake-up */
      ResetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_PDR);
      /* normal mode request */
      ResetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_CCR);
      while (TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_CCE));
      asm(" edis"); /* enable protection */
      bCanStatus &= ~CAN_STS_RESET;
      bCanStatus &= ~CAN_STS_SLEEPING;
      break;

    case MODE_SLEEP:
      asm(" eallow"); /* disable protection */
      /* controller in sleep mode */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_PDR);
      asm(" edis"); /* enable protection */
      bCanStatus |= CAN_STS_SLEEPING;
      break;

    case MODE_LOOPBK:
      asm(" eallow"); /* disable protection */
      /* enable loop back self test mode */
      SetBitValueD(CAN_MC + CAN_BASIC_ADR, CAN_FLAG_STM);
      asm(" edis"); /* enable protection */
      break;

    case MODE_LISTEN:
      bChangeOk = FALSE; /* not switched to listen only mode */
      break;

    default:
      bChangeOk = FALSE; /* unknown mode */
      break;
  }
  return bChangeOk;
} /* gCan_ActivateMode */


/*!
  Initialize the CAN controller.

  \param bIndex selects the bit timing
*/
void gCan_CntrlInit(BYTE bIndex)
{
  BYTE i;  /* counter */

  gCan_ActivateMode(MODE_CONFIG);

  asm(" eallow"); /* disable protection */

  /* set baud rate */
  WriteDword(CAN_BTC + CAN_BASIC_ADR, tCCanBtr[bIndex]);

  /* disable all message buffers */
  WriteDword(CAN_ME + CAN_BASIC_ADR, 0);

  /* set all message buffers to invalid */
  for (i = 0; i < 32; i++) {
    /* identifier zero, set AME */
    WriteDword(CAN_MID + CAN_FIRST_BUF + i * CAN_MSG_OFFSET + CAN_BASIC_ADR, 0x40000000UL);
    /* control off */
    WriteDword(CAN_MCF + CAN_FIRST_BUF + i * CAN_MSG_OFFSET + CAN_BASIC_ADR, 0);
    /* control off */
    WriteDword(CAN_MDL + CAN_FIRST_BUF + i * CAN_MSG_OFFSET + CAN_BASIC_ADR, 0);
    /* control off */
    WriteDword(CAN_MDH + CAN_FIRST_BUF + i * CAN_MSG_OFFSET + CAN_BASIC_ADR, 0);
    /* set mask to MUST MATCH */
    WriteDword(CAN_LAM + i * 2 + CAN_BASIC_ADR, 0);
  } /* for */

  asm(" edis"); /* enable protection */

  /* reset bus off flag */
  bCanStatus &= ~CAN_STS_BUS_OFF;

  gCan_ActivateMode(MODE_NORMAL);

  Can_Configure();

  asm(" eallow"); /* disable protection */

  /* enable status interrupts on interrupt line 0 */
  /* enable message interrupts on interrupt line 1 */
  /* for bus-off, error passiv and warning level  */
  WriteDword(CAN_GIM + CAN_BASIC_ADR, 0x00000703UL);

  /* enable message buffer interrupts */
  WriteDword(CAN_MIM + CAN_BASIC_ADR, 0xFFFFFFFFUL);

  /* connect message buffer interrupts to line 1 */
  WriteDword(CAN_MIL + CAN_BASIC_ADR, 0xFFFFFFFFUL);

  asm(" edis"); /* enable protection */

  bISRInitiated = TRUE;
} /* gCan_CntrlInit */


/*!
  \brief Enter sleep mode.

  With this call we go to sleep. The stack is suspended and no
  longer executed.

  \retval TRUE  - sleep mode established
  \retval FALSE - sleep mode refused
*/
BYTE gCan_InstallWakeUp(void)
{
  /* initiate sleeping modus */
  if (gCan_ActivateMode(MODE_SLEEP) == TRUE) {
    ModuleInfo.bModuleState = STACK_FREEZED;
    return TRUE;
  } /* if */
  else {
    return FALSE;
  } /* else */
} /* gCan_InstallWakeUp */


/*!
  \brief Wake up from sleep mode.

  This call controls the recover from sleep mode.
*/
void gCan_WakeUpOccurred(void)
{
  /* initiate normal mode */
  if (gCan_ActivateMode(MODE_NORMAL) == TRUE) {
    ModuleInfo.bModuleState = STACK_WAKEUP;
  } /* if */
} /* gCan_WakeUpOccurred */


/*!
  \brief Put a CAN message on the bus.

  \param  bBNr buffer number
  \return NO_ERR transmission in progress,
          WAIT_ERR no transmission cause of inhibit time,
          BUSY_ERR no transmission cause of other transmission in progress
*/
BYTE gCan_SendMsg(BYTE bBNr)
{
  BYTE bRet = NO_ERR;
  QBYTE lData; /* data frame content  */

  if (bBNr > (MAX_CAN_BUFFER - 1)) {
    return NO_ERR; /* CAN hardware buffer limit exceeded! */
  } /* if */

  /* reset TX status flag */
  bCanStatus &= ~CAN_STS_TX_OK;

  /* check for pending transmission request */
  if (!TestBitValueD(CAN_TRS + CAN_BASIC_ADR, (DWORD)1 << bBNr)) {
    /* disable TX message buffer */
    ResetBitValueD(CAN_ME + CAN_BASIC_ADR, (DWORD)1 << bBNr);
    /* set DLC and RTR flag */
    WriteDword(CAN_MCF + CAN_FIRST_BUF + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR,
               tCanMsgBuffer[bBNr].tMsg.Rtr ? tCanMsgBuffer[bBNr].tMsg.Dlc | 0x10
                                            : tCanMsgBuffer[bBNr].tMsg.Dlc);
    /* copy message data bytes */
    lData.fb.b0 = tCanMsgBuffer[bBNr].tMsg.bDb[0];
    lData.fb.b1 = tCanMsgBuffer[bBNr].tMsg.bDb[1];
    lData.fb.b2 = tCanMsgBuffer[bBNr].tMsg.bDb[2];
    lData.fb.b3 = tCanMsgBuffer[bBNr].tMsg.bDb[3];
    WriteDword(CAN_MDL + CAN_FIRST_BUF + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR, lData.dw);
    lData.fb.b0 = tCanMsgBuffer[bBNr].tMsg.bDb[4];
    lData.fb.b1 = tCanMsgBuffer[bBNr].tMsg.bDb[5];
    lData.fb.b2 = tCanMsgBuffer[bBNr].tMsg.bDb[6];
    lData.fb.b3 = tCanMsgBuffer[bBNr].tMsg.bDb[7];
    WriteDword(CAN_MDH + CAN_FIRST_BUF + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR, lData.dw);
    /* enable TX message buffer */
    SetBitValueD(CAN_ME + CAN_BASIC_ADR, (DWORD)1 << bBNr);
    /* set CAN controller transmission request */
    SetBitValueD(CAN_TRS + CAN_BASIC_ADR, (DWORD)1 << bBNr);

    /* set buffer to transmit */
    tCanMsgBuffer[bBNr].bSta = BUF_TRANSMIT;

    bRet = NO_ERR;  /* no error */
  }
  else {
    bRet = BUSY_ERR;  /* TX buffer busy */
  } /* else */

  return bRet;
} /* gCan_SendMsg */


/*!
  \brief The CAN interrupt service routine for the message buffer event.
*/
interrupt void gCan_MsgIntHandler(void)
{
  BYTE bBNr;   /* message buffer number */
  BYTE bDlc;   /* DLC */
  QBYTE lData; /* data frame content */

  asm(" CLRC INTM");
  /* get interrupt source */
  bBNr = (BYTE)(ReadDword(CAN_GIF1 + CAN_BASIC_ADR) & 0x0000001FUL);

  if (bISRInitiated)
  {
      switch (bBNr) {
        case NMT_ZERO_RX: /* buffer for NMT commands */
          /* get DLC */
          bDlc = (BYTE)(ReadDword(CAN_MCF + CAN_FIRST_BUF + NMT_ZERO_RX
                                  * CAN_MSG_OFFSET + CAN_BASIC_ADR) & 0x0F);

          /* check DLC */
          if (bDlc == 2) {
            /* copy message data bytes */
            lData.dw = ReadDword(CAN_MDL + CAN_FIRST_BUF + NMT_ZERO_RX
                                 * CAN_MSG_OFFSET + CAN_BASIC_ADR);
            /* check addressee */
            if ((lData.fb.b1 == 0) || (lData.fb.b1 == ModuleInfo.bBoardAdr)) {
              /* check overrun */
              if (tCanMsgBuffer[NMT_ZERO_RX].bSta == BUF_EMPTY) {
                /* copy 2 message data bytes */
                tCanMsgBuffer[NMT_ZERO_RX].tMsg.bDb[0] = lData.fb.b0;
                tCanMsgBuffer[NMT_ZERO_RX].tMsg.bDb[1] = lData.fb.b1;
                /* set status */
                tCanMsgBuffer[NMT_ZERO_RX].bSta = BUF_RECEIVED;
                tCanMsgBuffer[NMT_ZERO_RX].tMsg.RxOk = 1;
              } /* if */
              else {  /* set EMCY overrun flag */
                fEmcyOverrun = TRUE;
              } /* else */
            } /* if */
          } /* if */

          /* release receive buffer */
          WriteDword(CAN_RMP + CAN_BASIC_ADR, (DWORD)1 << NMT_ZERO_RX);
          break;

#if MAX_RX_PDOS > 0
        case PDO_RX + 0:          /* receive PDO 1  */
#endif /* MAX_RX_PDOS > 0 */

#if MAX_RX_PDOS > 1
        case PDO_RX + 1:          /* receive PDO 2  */
#endif /* MAX_RX_PDOS > 1 */

#if MAX_RX_PDOS > 2
        case PDO_RX + 2:          /* receive PDO 3  */
#endif /* MAX_RX_PDOS > 2 */

#if MAX_RX_PDOS > 3
        case PDO_RX + 3:          /* receive PDO 4  */
#endif /* MAX_RX_PDOS > 3 */

#if MAX_RX_PDOS > 4
    case PDO_RX + 4:            /* receive PDO 5 */
#endif /* MAX_RX_PDOS > 4 */

#if MAX_RX_PDOS > 5
    case PDO_RX + 5:            /* receive PDO 6 */
#endif /* MAX_RX_PDOS > 5 */

#if MAX_RX_PDOS > 6
    case PDO_RX + 6:            /* receive PDO 7 */
#endif /* MAX_RX_PDOS > 6 */

#if MAX_RX_PDOS > 7
    case PDO_RX + 7:            /* receive PDO 8 */
#endif /* MAX_RX_PDOS > 7 */

#if MAX_RX_PDOS > 8
    case PDO_RX + 8:            /* receive PDO 9 */
#endif /* MAX_RX_PDOS > 8 */

#if MAX_RX_PDOS > 9
    case PDO_RX + 9:            /* receive PDO 10 */
#endif /* MAX_RX_PDOS > 9 */

#if MAX_RX_PDOS > 10
    case PDO_RX + 10:            /* receive PDO 11 */
#endif /* MAX_RX_PDOS > 10 */

#if MAX_RX_PDOS > 11
    case PDO_RX + 11:            /* receive PDO 12 */
#endif /* MAX_RX_PDOS > 11 */

#if MAX_RX_PDOS > 12
    case PDO_RX + 12:            /* receive PDO 13 */
#endif /* MAX_RX_PDOS > 12 */

#if MAX_RX_PDOS > 13
    case PDO_RX + 13:            /* receive PDO 14 */
#endif /* MAX_RX_PDOS > 13 */

#if MAX_RX_PDOS > 14
    case PDO_RX + 14:            /* receive PDO 15 */
#endif /* MAX_RX_PDOS > 14 */

#if MAX_RX_PDOS > 0
# if SYNC_USED == 1
          /* check sync PDO */
          if (RPDO_PARAM[bBNr - PDO_RX].mTT < 241) {
            /* set sync RX PDO status */
            SyncPdoRxSta |= 0x01 << (bBNr - PDO_RX);

            /* release receive buffer */
            WriteDword(CAN_RMP + CAN_BASIC_ADR, (DWORD)1 << bBNr);
          } /* if */
          else {
            /* get DLC */
            bDlc = (BYTE)(ReadDword(CAN_MCF + CAN_FIRST_BUF + bBNr
                                    * CAN_MSG_OFFSET + CAN_BASIC_ADR) & 0x0F);

            /* check overrun */
            if (tCanMsgBuffer[bBNr].bSta == BUF_EMPTY) {
              /* check DLC */
              if (bDlc > 8) {
                bDlc = 8;
              } /* if */

              /* get the message data */
              lData.dw = ReadDword(CAN_MDL + CAN_FIRST_BUF + bBNr
                                   * CAN_MSG_OFFSET + CAN_BASIC_ADR);
              tCanMsgBuffer[bBNr].tMsg.bDb[0] = lData.fb.b0;
              tCanMsgBuffer[bBNr].tMsg.bDb[1] = lData.fb.b1;
              tCanMsgBuffer[bBNr].tMsg.bDb[2] = lData.fb.b2;
              tCanMsgBuffer[bBNr].tMsg.bDb[3] = lData.fb.b3;
              lData.dw = ReadDword(CAN_MDH + CAN_FIRST_BUF + bBNr
                                   * CAN_MSG_OFFSET + CAN_BASIC_ADR);
              tCanMsgBuffer[bBNr].tMsg.bDb[4] = lData.fb.b0;
              tCanMsgBuffer[bBNr].tMsg.bDb[5] = lData.fb.b1;
              tCanMsgBuffer[bBNr].tMsg.bDb[6] = lData.fb.b2;
              tCanMsgBuffer[bBNr].tMsg.bDb[7] = lData.fb.b3;

              /* set status */
              tCanMsgBuffer[bBNr].bSta = BUF_RECEIVED;
              tCanMsgBuffer[bBNr].tMsg.RxOk = 1;
              tCanMsgBuffer[bBNr].tMsg.Dlc = bDlc;
            } /* if */
            else {
              /* set EMCY overrun flag */
              fEmcyOverrun = TRUE;
            } /* else */

            /* release receive buffer */
            WriteDword(CAN_RMP + CAN_BASIC_ADR, (DWORD)1 << bBNr);
          } /* else */
          break;
# endif /* SYNC_USED == 1 */
#endif /* MAX_RX_PDOS > 0 */

#if TIME_STAMP_USED == 1
        case TIME_RX:             /* time stamp message */
#endif /* TIME_STAMP_USED == 1 */

#if HEARTBEAT_CONSUMER == 1
        case ERRCTRL_RX:          /* heartbeat consumer */
#endif /* HEARTBEAT_CONSUMER == 1 */

#if MULTI_SDO_SERVER == 1
# if NUM_SDO_SERVERS == 2
        case SDO_RX + 1:          /* second server's SDO request */
# endif /* NUM_SDO_SERVERS == 2 */
#endif /* MULTI_SDO_SERVER == 1 */

#if CLIENT_ENABLE == 1
        case CLSDO_RX:            /* client SDO response */
#endif /* CLIENT_ENABLE == 1*/

#if (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1))
        case LSS_RX:              /* receive layer services */
#endif /* (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1))*/

        case SDO_RX:              /* server SDO request */
          /* get DLC */
          bDlc = (BYTE)(ReadDword(CAN_MCF + CAN_FIRST_BUF + bBNr
                                  * CAN_MSG_OFFSET + CAN_BASIC_ADR) & 0x0F);

          /* check overrun */
          if (tCanMsgBuffer[bBNr].bSta == BUF_EMPTY) {
            /* check DLC */
            if (bDlc > 8) {
              bDlc = 8;
            } /* if */

            /* get the message data */
            lData.dw = ReadDword(CAN_MDL + CAN_FIRST_BUF + bBNr
                                 * CAN_MSG_OFFSET + CAN_BASIC_ADR);
            tCanMsgBuffer[bBNr].tMsg.bDb[0] = lData.fb.b0;
            tCanMsgBuffer[bBNr].tMsg.bDb[1] = lData.fb.b1;
            tCanMsgBuffer[bBNr].tMsg.bDb[2] = lData.fb.b2;
            tCanMsgBuffer[bBNr].tMsg.bDb[3] = lData.fb.b3;
            lData.dw = ReadDword(CAN_MDH + CAN_FIRST_BUF + bBNr
                                 * CAN_MSG_OFFSET + CAN_BASIC_ADR);
            tCanMsgBuffer[bBNr].tMsg.bDb[4] = lData.fb.b0;
            tCanMsgBuffer[bBNr].tMsg.bDb[5] = lData.fb.b1;
            tCanMsgBuffer[bBNr].tMsg.bDb[6] = lData.fb.b2;
            tCanMsgBuffer[bBNr].tMsg.bDb[7] = lData.fb.b3;

            /* set status */
            tCanMsgBuffer[bBNr].bSta = BUF_RECEIVED;
            tCanMsgBuffer[bBNr].tMsg.RxOk = 1;
            tCanMsgBuffer[bBNr].tMsg.Dlc = bDlc;
          } /* if */
          else {
            /* set EMCY overrun flag */
            fEmcyOverrun = TRUE;
          } /* else */

          /* release receive buffer */
          WriteDword(CAN_RMP + CAN_BASIC_ADR, (DWORD)1 << bBNr);
          break;

#if SYNC_USED == 1
        case SYNC_RX:             /* SYNC message */
          /* check SW overrun */
          if (tCanMsgBuffer[SYNC_RX].bSta == BUF_EMPTY) {
            /* set status */
            tCanMsgBuffer[SYNC_RX].bSta = BUF_RECEIVED;
            tCanMsgBuffer[SYNC_RX].tMsg.RxOk = 1;
          } /* if */
          else {
            /* set EMCY overrun flag */
            fEmcyOverrun = TRUE;
          } /* else */

          /* release receive buffer */
          WriteDword(CAN_RMP + CAN_BASIC_ADR, (DWORD)1 << SYNC_RX);
# if MAX_RX_PDOS > 0
          /* serve sync RX PDOs */
          {
            BYTE k;
            for (k = 0; k < MAX_RX_PDOS; k++) {
              if (SyncPdoRxSta & (0x01 << k)) {
                /* reset sync PDO receive flag */
                SyncPdoRxSta &= ~(0x01 << k);
                /* check buffer overrun */
                if (tCanMsgBuffer[PDO_RX + k].bSta == BUF_EMPTY) {
                  /* get DLC */
                  bDlc = (BYTE)(ReadDword(CAN_MCF + CAN_FIRST_BUF + (PDO_RX + k)
                                          * CAN_MSG_OFFSET + CAN_BASIC_ADR) & 0x0F);

                  /* check DLC */
                  if (bDlc > 8) {
                    bDlc = 8;
                  } /* if */

                  /* get the message data */
                  lData.dw = ReadDword(CAN_MDL + CAN_FIRST_BUF + (PDO_RX + k)
                                       * CAN_MSG_OFFSET + CAN_BASIC_ADR);
                  tCanMsgBuffer[PDO_RX + k].tMsg.bDb[0] = lData.fb.b0;
                  tCanMsgBuffer[PDO_RX + k].tMsg.bDb[1] = lData.fb.b1;
                  tCanMsgBuffer[PDO_RX + k].tMsg.bDb[2] = lData.fb.b2;
                  tCanMsgBuffer[PDO_RX + k].tMsg.bDb[3] = lData.fb.b3;
                  lData.dw = ReadDword(CAN_MDH + CAN_FIRST_BUF + (PDO_RX + k)
                                       * CAN_MSG_OFFSET + CAN_BASIC_ADR);
                  tCanMsgBuffer[PDO_RX + k].tMsg.bDb[4] = lData.fb.b0;
                  tCanMsgBuffer[PDO_RX + k].tMsg.bDb[5] = lData.fb.b1;
                  tCanMsgBuffer[PDO_RX + k].tMsg.bDb[6] = lData.fb.b2;
                  tCanMsgBuffer[PDO_RX + k].tMsg.bDb[7] = lData.fb.b3;

                  /* set status */
                  tCanMsgBuffer[PDO_RX + k].bSta =
                    (ModuleInfo.bCommState == OPERATIONAL) ? BUF_FULL : BUF_EMPTY;
                  tCanMsgBuffer[PDO_RX + k].tMsg.RxOk = 1;
                  tCanMsgBuffer[PDO_RX + k].tMsg.Dlc = bDlc;

                  /* release receive buffer */
                  WriteDword(CAN_RMP + CAN_BASIC_ADR, (DWORD)1 << (PDO_RX + k));
                } /* if */
                else {
                  /* set EMCY overrun flag */
                  fEmcyOverrun = TRUE;
                } /* else */
              } /* if */
            } /* for */
          } /* block */
# endif /* MAX_RX_PDOS > 0 */
          break;
#endif /* SYNC_USED == 1 */

#if MAX_TX_PDOS > 0
        case PDO_TX + 0:          /* transmit PDO 1  */

# if MAX_TX_PDOS > 1
        case PDO_TX + 1:          /* transmit PDO 2  */
# endif /* MAX_TX_PDOS > 1 */

# if MAX_TX_PDOS > 2
        case PDO_TX + 2:          /* transmit PDO 3  */
# endif /* MAX_TX_PDOS > 2 */

# if MAX_TX_PDOS > 3
        case PDO_TX + 3:          /* transmit PDO 4  */
# endif /* MAX_TX_PDOS > 3 */
#if MAX_TX_PDOS > 5
    case PDO_TX + 5:            /* transmit PDO 6 */
#endif /* MAX_TX_PDOS > 5 */

#if MAX_TX_PDOS > 6
    case PDO_TX + 6:            /* transmit PDO 7 */
#endif /* MAX_TX_PDOS > 6 */

#if MAX_TX_PDOS > 7
    case PDO_TX + 7:            /* transmit PDO 8 */
#endif /* MAX_TX_PDOS > 7 */

#if MAX_TX_PDOS > 8
    case PDO_TX + 8:            /* transmit PDO 9 */
#endif /* MAX_TX_PDOS > 8 */

#if MAX_TX_PDOS > 9
    case PDO_TX + 9:            /* transmit PDO 10 */
#endif /* MAX_TX_PDOS > 9 */

#if MAX_TX_PDOS > 10
    case PDO_TX + 10:            /* transmit PDO 11 */
#endif /* MAX_TX_PDOS > 10 */

#if MAX_TX_PDOS > 11
    case PDO_TX + 11:            /* transmit PDO 12 */
#endif /* MAX_TX_PDOS > 11 */

#if MAX_TX_PDOS > 12
    case PDO_TX + 12:            /* transmit PDO 13 */
#endif /* MAX_TX_PDOS > 12 */

#if MAX_TX_PDOS > 13
    case PDO_TX + 13:            /* transmit PDO 14 */
#endif /* MAX_TX_PDOS > 13 */

#if MAX_TX_PDOS > 14
    case PDO_TX + 14:            /* transmit PDO 15 */
#endif /* MAX_TX_PDOS > 14 */

          /* last transmission successful */
          bFirstMsgTrans = TRUE;
          bCanStatus |= CAN_STS_TX_OK;

          /* buffer has status sent */
          tCanMsgBuffer[bBNr].bSta = BUF_SENT;
          tCanMsgBuffer[bBNr].tMsg.TxOk = 1;

          /* acknowledge transmit interrupt */
          WriteDword(CAN_TA + CAN_BASIC_ADR, (DWORD)1 << bBNr);
          do
          {
            ;  // Wait for bit to clear
          } while (TestBitValueD(CAN_TA + CAN_BASIC_ADR, (DWORD)1 << bBNr));
          break;

#endif /* MAX_TX_PDOS > 0 */

#if ENABLE_EMCYMSG == 1
        case EMCY_TX:             /* emergency message */
#endif

#if MULTI_SDO_SERVER == 1
# if NUM_SDO_SERVERS == 2
        case SDO_TX + 1:          /* second server's SDO response */
# endif /* NUM_SDO_SERVERS == 2 */
#endif /* MULTI_SDO_SERVER == 1 */

#if CLIENT_ENABLE == 1
        case CLSDO_TX:            /* client SDO request */
#endif /* CLIENT_ENABLE == 1*/

#if (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1))
        case LSS_TX:              /* transmit layer services */
#endif /* (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1)) */

        case SDO_TX:              /* server SDO response */
          /* last transmission successful */
          bFirstMsgTrans = TRUE;
          bCanStatus |= CAN_STS_TX_OK;

          /* buffer has status sent */
          tCanMsgBuffer[bBNr].bSta = BUF_SENT;
          tCanMsgBuffer[bBNr].tMsg.TxOk = 1;

          /* acknowledge transmit interrupt */
          WriteDword(CAN_TA + CAN_BASIC_ADR, (DWORD)1 << bBNr);
          do
          {
            ;  // Wait for bit to clear
          } while (TestBitValueD(CAN_TA + CAN_BASIC_ADR, (DWORD)1 << bBNr));
          break;

#if ENABLE_NMT_ERROR_CTRL != 0
        case ERRCTRL_TX:          /* node guarding / heartbeat producer */
          /* RTR message received and response to RTR message sent (Guarding) */
          /* or cyclic message sent (Heartbeat): last transmission successful */
          /* acknowledge transmit interrupt */
          WriteDword(CAN_TA + CAN_BASIC_ADR, (DWORD)1 << ERRCTRL_TX);
          do
          {
            ;  // Wait for bit to clear
          } while (TestBitValueD(CAN_TA + CAN_BASIC_ADR, (DWORD)1 << bBNr));

# if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
          /* refresh remote buffer */
          lData.fb.b0 = NewGuardData();
          WriteDword(CAN_MDL + CAN_FIRST_BUF + ERRCTRL_TX * CAN_MSG_OFFSET
                     + CAN_BASIC_ADR, lData.dw);
# endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */

          bFirstMsgTrans = TRUE;
          bCanStatus |= CAN_STS_TX_OK;

          /* buffer has status sent */
          tCanMsgBuffer[ERRCTRL_TX].bSta = BUF_SENT;
          tCanMsgBuffer[ERRCTRL_TX].tMsg.TxOk = 1;
          break;
#endif /* ENABLE_NMT_ERROR_CTRL != 0 */

        default:
          /* release receive buffer */
          WriteDword(CAN_RMP + CAN_BASIC_ADR, (DWORD)1 << bBNr);
          /* acknowledge transmit interrupt */
          WriteDword(CAN_TA + CAN_BASIC_ADR, (DWORD)1 << bBNr);
          do
          {
            ;  // Wait for bit to clear
          } while (TestBitValueD(CAN_TA + CAN_BASIC_ADR, (DWORD)1 << bBNr));
          break;
      } /* switch */
   } /* if (bISRInitiated) */
  /* acknowledge interrupt */
  PIEACK = 0x0100;
} /* gCan_MsgIntHandler */


/*!
  \brief CAN error ISR.

  The CAN error interrupt service routine.
*/
interrupt void gCan_SysIntHandler(void)
{
  if (TestBitValueD(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_WARN)) {
    /* warning level interrupt */
    bCanStatus |= CAN_STS_WARNING;
    /* no bus-off detected */
    bCanStatus &= ~CAN_STS_BUS_OFF;
    /* clear error interrupt flag */
    WriteDword(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_WARN);
  } /* if */

  if (TestBitValueD(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_PASV)) {
//#if  MOD_LED_EN == 1
    /** Debug*/
//	MODULE_LED = TRUE;
//#endif
	  /* warning level interrupt */
    bCanStatus |= CAN_STS_WARNING;
    /* no bus-off detected */
    bCanStatus &= ~CAN_STS_BUS_OFF;
    /* clear error interrupt flag */
    WriteDword(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_PASV);
  } /* if */

  if (TestBitValueD(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_BOFF)) {
	/* bus-off interrupt */
    bCanStatus |= CAN_STS_BUS_OFF;
    /* leave warning level */
    bCanStatus &= ~CAN_STS_WARNING;
    /* clear error interrupt flag */
    WriteDword(CAN_GIF0 + CAN_BASIC_ADR, CAN_FLAG_BOFF);
  } /* if */

  /* acknowledge interrupt */
  PIEACK = 0x0100;
} /* gCan_SysIntHandler */


#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
/*!
  Prepare a CAN buffer to handle a given remote message.

  \param ptMsg pointer to buffer
  \return NO_ERR buffer established
*/
BYTE gCan_PrepareRemoteMessage(CAN_MSG MEM_AREA *ptMsg)
{
  QBYTE lData; /* data frame content */

  if (ptMsg == 0) {
    return NO_ERR;
  } /* if */

  /* set transmit buffer invalid */
  ResetBitValueD(CAN_ME + CAN_BASIC_ADR, (DWORD)1 << ERRCTRL_TX);

  /* update data to current state */
  lData.fb.b0 = ModuleInfo.bCommState;
  WriteDword(CAN_MDL + CAN_FIRST_BUF + ERRCTRL_TX * CAN_MSG_OFFSET
             + CAN_BASIC_ADR, lData.dw);

  /* set transmit buffer valid */
  SetBitValueD(CAN_ME + CAN_BASIC_ADR, (DWORD)1 << ERRCTRL_TX);

  return NO_ERR;
} /* gCan_PrepareRemoteMessage */
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */


#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
/*!
  \brief Refresh a CAN buffer to handle a given remote message.
*/
void gCan_RefreshRemoteMessage(void)
{
  QBYTE lData; /* data frame content */

  /* set transmit buffer invalid */
  ResetBitValueD(CAN_ME + CAN_BASIC_ADR, (DWORD)1 << ERRCTRL_TX);

  /* refresh remote buffer */
  lData.dw = ReadDword(CAN_MDL + CAN_FIRST_BUF + ERRCTRL_TX
                       * CAN_MSG_OFFSET + CAN_BASIC_ADR);
  lData.fb.b0 = (lData.fb.b0 & 0x80) | ModuleInfo.bCommState;
  WriteDword(CAN_MDL + CAN_FIRST_BUF + ERRCTRL_TX * CAN_MSG_OFFSET
             + CAN_BASIC_ADR, lData.dw);

  /* set transmit buffer valid */
  SetBitValueD(CAN_ME + CAN_BASIC_ADR, (DWORD)1 << ERRCTRL_TX);
} /* gCan_RefreshRemoteMessage */
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */


/*!
  Read the current status information of the CAN controller.
  The variable \e bCanStatus is local to the CAN module. Via this call
  we have an encapsulated access to this information.

  \return
  \link #CAN_STS_NORMAL CAN_STS_NORMAL \endlink
  or
  \link #CAN_STS_RESET CAN_STS_RESET \endlink
  | \link #CAN_STS_WARNING CAN_STS_WARNING \endlink
  | \link #CAN_STS_BUS_OFF CAN_STS_BUS_OFF \endlink
  | \link #CAN_STS_SLEEPING CAN_STS_SLEEPING \endlink
  | \link #CAN_STS_OVERRUN CAN_STS_OVERRUN \endlink
  | \link #CAN_STS_TX_OK CAN_STS_TX_OK \endlink
*/
BYTE gCan_GetCanStatus(void)
{
  /* update all status flags */
  WriteDword(CAN_ES + CAN_BASIC_ADR, 0x01BF0000);

  if (TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_EW)
      || TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_EP)) {
    /* warning level detected */
    bCanStatus |= CAN_STS_WARNING;
  } /* if */
  else {
    /* no warning level detected */
    bCanStatus &= ~CAN_STS_WARNING;
  } /* if */

  if (TestBitValueD(CAN_ES + CAN_BASIC_ADR, CAN_FLAG_BO)) {
    /* bus-off detected */
    bCanStatus |= CAN_STS_BUS_OFF;
  } /* if */
  else {
    /* no bus-off detected */
    bCanStatus &= ~CAN_STS_BUS_OFF;
  } /* else */
  return bCanStatus;
} /* gCan_GetCanStatus */


/*!
  This function signals that the first message (boot-up message) is transmitted
  successfully.

  \retval TRUE - First message successfully transmitted.
  \retval FALSE - First message not yet transmitted.
*/
BYTE gCan_TxOccurred(void)
{
  return bFirstMsgTrans;
} /* gCan_TxOccurred */


/*!
  \brief Initialize CAN message buffering.

  This function is called from the protocol stack to set up the
  message buffers and optional queues.
*/
void gCB_Init(void)
{
  ; /* nothing to do for buffered CAN driver */
} /* gCB_Init */


/*!
  \brief Set identifier arbitration entry.

  Configure the identifier arbitration entry and set the message
  object valid flag.

  \param bBNr buffer number of a flat structure
  \param fMsgValid TRUE to enable message object,
                   FALSE to disable message object
  \return NO_ERR if no error occured, VALUE_TOO_HIGH_ERR if identifier
          table exceeded
*/
STATIC BYTE sCB_ConfigureId(BYTE bBNr, BOOLEAN fMsgValid)
{
  DWORD lId; /* actually we support std/ext identifier */

  if (bBNr >= MAX_CAN_BUFFER) {
    return VALUE_TOO_HIGH_ERR;  /* OUT: ID table exceeded */
  } /* if */

  /* construct identifier */
#if EXTENDED_ID_USED == 1
  if (tCanMsgBuffer[bBNr].tMsg.Ext == 1) { /* extended frame */
    lId = tCanMsgBuffer[bBNr].tMsg.qbId.dw | 0x80000000UL;
  } /* if */
  else
#endif /* EXTENDED_ID_USED == 1 */
  {
    /* standard frame */
    lId = tCanMsgBuffer[bBNr].tMsg.qbId.dw << 18;
  } /* else */

  /* set message object invalid */
  ResetBitValueD(CAN_ME + CAN_BASIC_ADR, (DWORD)1 << bBNr);

  switch (bBNr) {
    case NMT_ZERO_RX:           /* receive buffer for NMT commands */

#if MAX_RX_PDOS > 0
    case PDO_RX + 0:            /* receive PDO 1 */
#endif /* MAX_RX_PDOS > 0 */

#if MAX_RX_PDOS > 1
    case PDO_RX + 1:            /* receive PDO 2 */
#endif /* MAX_RX_PDOS > 1 */

#if MAX_RX_PDOS > 2
    case PDO_RX + 2:            /* receive PDO 3 */
#endif /* MAX_RX_PDOS > 2 */

#if MAX_RX_PDOS > 3
    case PDO_RX + 3:            /* receive PDO 4 */
#endif /* MAX_RX_PDOS > 3 */

#if MAX_RX_PDOS > 4
    case PDO_RX + 4:            /* receive PDO 5 */
#endif /* MAX_RX_PDOS > 4 */

#if MAX_RX_PDOS > 5
    case PDO_RX + 5:            /* receive PDO 6 */
#endif /* MAX_RX_PDOS > 5 */

#if MAX_RX_PDOS > 6
    case PDO_RX + 6:            /* receive PDO 7 */
#endif /* MAX_RX_PDOS > 6 */

#if MAX_RX_PDOS > 7
    case PDO_RX + 7:            /* receive PDO 8 */
#endif /* MAX_RX_PDOS > 7 */

#if MAX_RX_PDOS > 8
    case PDO_RX + 8:            /* receive PDO 9 */
#endif /* MAX_RX_PDOS > 8 */

#if MAX_RX_PDOS > 9
    case PDO_RX + 9:            /* receive PDO 10 */
#endif /* MAX_RX_PDOS > 9 */

#if MAX_RX_PDOS > 10
    case PDO_RX + 10:            /* receive PDO 11 */
#endif /* MAX_RX_PDOS > 10 */

#if MAX_RX_PDOS > 11
    case PDO_RX + 11:            /* receive PDO 12 */
#endif /* MAX_RX_PDOS > 11 */

#if MAX_RX_PDOS > 12
    case PDO_RX + 12:            /* receive PDO 13 */
#endif /* MAX_RX_PDOS > 12 */

#if MAX_RX_PDOS > 13
    case PDO_RX + 13:            /* receive PDO 14 */
#endif /* MAX_RX_PDOS > 13 */

#if MAX_RX_PDOS > 14
    case PDO_RX + 14:            /* receive PDO 15 */
#endif /* MAX_RX_PDOS > 14 */

#if SYNC_USED == 1
    case SYNC_RX:               /* SYNC message  */
#endif /* SYNC_USED == 1 */

#if MULTI_SDO_SERVER == 1
# if NUM_SDO_SERVERS == 2
    case SDO_RX + 1:            /* second server's SDO request */
# endif /* NUM_SDO_SERVERS == 2 */
#endif /* MULTI_SDO_SERVER == 1 */

#if CLIENT_ENABLE == 1
    case CLSDO_RX:              /* client SDO response */
#endif /* CLIENT_ENABLE == 1*/

#if (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1))
    case LSS_RX:                /* receive layer services */
#endif /* (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1)) */

#if TIME_STAMP_USED == 1
    case TIME_RX:               /* time stamp message */
#endif /* TIME_STAMP_USED == 1 */

#if HEARTBEAT_CONSUMER == 1
    case ERRCTRL_RX:        /* heartbeat consumer */
#endif /* HEARTBEAT_CONSUMER == 1 */

    case SDO_RX:                /* server SDO request */
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[bBNr].tMsg.Dlc);
      /* set message object to receive */
      SetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << bBNr);
      /* enable overwrite protection */
      SetBitValueD(CAN_OPC + CAN_BASIC_ADR, (DWORD)1 << bBNr);
      if (fMsgValid == TRUE) {
        /* set message object valid */
        SetBitValueD(CAN_ME + CAN_BASIC_ADR, (DWORD)1 << bBNr);
      } /* if */
      break;

#if MAX_TX_PDOS > 0
    case PDO_TX + 0:            /* transmit PDO 1 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 0) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 0) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 0].tMsg.Dlc | (1 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 0));
      break;
#endif /* MAX_TX_PDOS > 0 */

#if MAX_TX_PDOS > 1
    case PDO_TX + 1:            /* transmit PDO 2 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 1) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 1) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 1].tMsg.Dlc | (2 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 1));
      break;
#endif /* MAX_TX_PDOS > 1 */

#if MAX_TX_PDOS > 2
    case PDO_TX + 2:            /* transmit PDO 3 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 2) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 2) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 2].tMsg.Dlc | (3 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 2));
      break;
#endif /* MAX_TX_PDOS > 2 */

#if MAX_TX_PDOS > 3
    case PDO_TX + 3:            /* transmit PDO 4 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 3) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 3) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 3].tMsg.Dlc | (4 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 3));
      break;
#endif /* MAX_TX_PDOS > 3 */

#if MAX_TX_PDOS > 4
    case PDO_TX + 4:            /* transmit PDO 5 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 4) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 4) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 4].tMsg.Dlc | (5 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 4));
      break;
#endif /* MAX_TX_PDOS > 4 */

#if MAX_TX_PDOS > 5
    case PDO_TX + 5:            /* transmit PDO 6 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 5) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 5) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 5].tMsg.Dlc | (6 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 5));
      break;
#endif /* MAX_TX_PDOS > 5 */

#if MAX_TX_PDOS > 6
    case PDO_TX + 6:            /* transmit PDO 7 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 6) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 6) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 6].tMsg.Dlc | (7 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 6));
      break;
#endif /* MAX_TX_PDOS > 6 */

#if MAX_TX_PDOS > 7
    case PDO_TX + 7:            /* transmit PDO 8 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 7) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 7) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 7].tMsg.Dlc | (8 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 7));
      break;
#endif /* MAX_TX_PDOS > 7 */

#if MAX_TX_PDOS > 8
    case PDO_TX + 8:            /* transmit PDO 9 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 8) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 8) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 8].tMsg.Dlc | (9 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 8));
      break;
#endif /* MAX_TX_PDOS > 8 */

#if MAX_TX_PDOS > 9
    case PDO_TX + 9:            /* transmit PDO 10 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 9) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 9) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 9].tMsg.Dlc | (10 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 9));
      break;
#endif /* MAX_TX_PDOS > 9 */

#if MAX_TX_PDOS > 10
    case PDO_TX + 10:            /* transmit PDO 11 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 10) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 10) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 10].tMsg.Dlc | (11 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 10));
      break;
#endif /* MAX_TX_PDOS > 10 */

#if MAX_TX_PDOS > 11
    case PDO_TX + 11:            /* transmit PDO 12 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 11) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 11) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 11].tMsg.Dlc | (12 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 11));
      break;
#endif /* MAX_TX_PDOS > 11 */

#if MAX_TX_PDOS > 12
    case PDO_TX + 12:            /* transmit PDO 13 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 12) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 12) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 12].tMsg.Dlc | (13 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 12));
      break;
#endif /* MAX_TX_PDOS > 12 */

#if MAX_TX_PDOS > 13
    case PDO_TX + 13:            /* transmit PDO 14 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 13) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 13) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 13].tMsg.Dlc | (14 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 13));
      break;
#endif /* MAX_TX_PDOS > 13 */

#if MAX_TX_PDOS > 14
    case PDO_TX + 14:            /* transmit PDO 15 */
      /* enable auto answer mode */
      lId |= 0x20000000UL;
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + (PDO_TX + 14) * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + (PDO_TX + 14) * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[PDO_TX + 14].tMsg.Dlc | (15 << 8));
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << (PDO_TX + 14));
      break;
#endif /* MAX_TX_PDOS > 14 */

#if ENABLE_NMT_ERROR_CTRL != 0
    case ERRCTRL_TX:            /* node guarding / heartbeat producer */
# if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
      /* enable auto answer mode */
      lId |= 0x20000000UL;
# endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[bBNr].tMsg.Dlc);
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << bBNr);
      break;
#endif /* ENABLE_NMT_ERROR_CTRL != 0 */

#if ENABLE_EMCYMSG == 1
    case EMCY_TX:               /* emergency message */
#endif /* ENABLE_EMCYMSG == 1 */

#if MULTI_SDO_SERVER == 1
# if NUM_SDO_SERVERS == 2
    case SDO_TX + 1:            /* second server's SDO response */
# endif /* NUM_SDO_SERVERS == 2 */
#endif /* MULTI_SDO_SERVER == 1 */

#if CLIENT_ENABLE == 1
    case CLSDO_TX:              /* client SDO request */
#endif /* CLIENT_ENABLE == 1*/

#if (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1))
    case LSS_TX:                /* transmit layer services */
#endif /* (ENABLE_LAYER_SERVICES == 1) && ((EXEC_LAYER_SERVICES_ALWAYS == 1) || (ENABLE_LSS_MASTER == 1)) */

    case SDO_TX:                /* server SDO response */
      /* set COB ID */
      WriteDword(CAN_MID + CAN_FIRST_BUF + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR, lId);
      /* set message object configuration: DLC */
      WriteDword(CAN_MCF + CAN_FIRST_BUF + bBNr * CAN_MSG_OFFSET + CAN_BASIC_ADR,
                 tCanMsgBuffer[bBNr].tMsg.Dlc);
      /* set message object to transmit */
      ResetBitValueD(CAN_MD + CAN_BASIC_ADR, (DWORD)1 << bBNr);
      break;
  } /* switch */

  return NO_ERR;  /* OUT: no error */
} /* sCB_ConfigureId */


/*!
  Configure a flat buffer for the reception of a message.

  \param bBNr - buffer number in flat buffer structure.
  \param dwId - Identifier to be used for this buffer.
  \param bDlc - Data length code to be used for message.
  \param bDir - #RX_DIR, #TX_DIR
*/
void gCB_BufSetId(BYTE bBNr, DWORD dwId, BYTE bDlc, BYTE bDir)
{
  /* configure buffer */
  if (dwId > CAN_MAX_STD_ID) {
    tCanMsgBuffer[bBNr].tMsg.Ext = TRUE; /* extended frame */
  } /* if */
  else {
    tCanMsgBuffer[bBNr].tMsg.Ext = FALSE; /* standard frame */
  } /* else */
  tCanMsgBuffer[bBNr].tMsg.qbId.dw = dwId;
  tCanMsgBuffer[bBNr].tMsg.Dlc     = bDlc;

  /* set identifier arbitration */
  sCB_ConfigureId(bBNr, FALSE);
} /* gCB_BufSetId */


/*!
  Change the identifier for a flat buffer location.

  \param bBNr - buffer number in flat buffer structure.
  \param dwId - New identifier.
  \retval FALSE - Identifier would overwrite filter array
  \retval TRUE - Changing accepted.
*/
BYTE gCB_BufChangeId(BYTE bBNr, DWORD dwId)
{
  BYTE bRet;  /* return value */

  /* set COB ID */
  tCanMsgBuffer[bBNr].tMsg.qbId.dw = dwId;
  if (dwId > CAN_MAX_STD_ID) {
    tCanMsgBuffer[bBNr].tMsg.Ext = TRUE; /* extended frame */
  } /* if */
  else {
    tCanMsgBuffer[bBNr].tMsg.Ext = FALSE; /* standard frame */
  } /* else */

  /* assign new identifier */
  if (sCB_ConfigureId(bBNr, TRUE) == NO_ERR) {
    bRet = TRUE;
  } /* if */
  else {
    bRet = FALSE;
  } /* else */

  return bRet;
} /* gCB_BufChangeId */


/*!
  Enable/disable a known identifier for reception.

  \param bBNr - buffer number in flat buffer structure.
  \param fSet - ON - message will be used / OFF - message ignored.
  \retval FALSE - Identifier would overwrite filter array
  \retval TRUE - Changing accepted.
*/
BYTE gCB_BufEnable(BOOLEAN fSet, BYTE bBNr)
{
  BYTE bRet;  /* return value */

  if (fSet) {  /* enable identifier */
    /* assign identifier */
    if (sCB_ConfigureId(bBNr, TRUE) == NO_ERR) {
      bRet = TRUE;
    } /* if */
    else {
      bRet = FALSE;
    } /* else */
  } /* if */
  else {  /* disable identifier */
    /* release identifier */
    if (sCB_ConfigureId(bBNr, FALSE) == NO_ERR) {
      bRet = TRUE;
    } /* if */
    else {
      bRet = FALSE;
    } /* else */
  } /* else */

  return bRet;
} /* gCB_BufEnable */


/*!
  Check receive message existance.

  \return buffer number or NO_MESSAGE.
*/
BYTE gCB_TestInMsg(void)
{
  STATIC BYTE DATA i = 1;  /* buffer counter */

  /* check for NMT 0 message first */
  if (tCanMsgBuffer[NMT_ZERO_RX].bSta == BUF_RECEIVED) {
    return NMT_ZERO_RX;  /* OUT: buffer number */
  } /* if */

  for (; i < MAX_CAN_BUFFER; i++) {
    /* message received ? */
    if (tCanMsgBuffer[i].bSta == BUF_RECEIVED) {
      return i;  /* OUT: buffer number */
    } /* if */
  } /* for */

  i = 1;              /* reset index     */
  return NO_MESSAGE;  /* OUT: no message */
} /* gCB_TestInMsg */


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Basic routines to filter and store CAN messages using the
         on-chip CAN interface of the Texas Instruments TMS320F28x eCAN.
  \par File name buf_e28.c
  \version 5
  \date 14.06.05
  \author (c) 1998-2005 by Vector Informatik GmbH, Stuttgart

  NOTE: This driver release includes some workarounds regarding the
        TI errata SPRZ193H CAN - CPU access to the eCAN registers.
*/

/*!
  \var tCCanBtr
  \brief CAN controller bit timing values.

  The bit timing values are defined for a 150 MHz clock in ascending order,
  first BTR0 then BTR1.
  - 10K bit/s, DO NOT USE
  - 20K bit/s, DO NOT USE
  - 50K bit/s
  - 100K bit/s
  - 125K bit/s
  - 250K bit/s
  - 500K bit/s
  - 800K bit/s
  - 1M bit/s
*/

