/*--------------------------------------------------------------------
       USRCLBCK.C
  --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: CANopen slave user callbacks functions
  --------------------------------------------------------------------*/


/*--------------------------------------------------------------------*/
/*  include files                                                     */
/*--------------------------------------------------------------------*/
#include <string.h>
#include "portab.h"
#include "cos_main.h"
#include "cancntrl.h"
#include "buffer.h"
#include "objdict.h"
#include "sdoserv.h"
#include "profil.h"
#include "ramtron.h"
#include "Truck_PDO.h"
#include "EventMgr.h"
#include "EvHandler.h"
#include "objects.h"
#include "fram.h"
#include "sdo_map_interface.h"
#include "ev.h"
#include "watchdog.h"
#include "SPI.h"

#include "PdoChkUser.h"
#include "InputSwitches.h"


#ifndef __18CXX
# include "stdio.h"
#endif /* __18CXX */

#include "emcyhndl.h"
#include "vdbgprnt.h"
#include "target.h"

#if NMT_MASTER_ENABLE == 1
# if NMT_GUARDING_ENABLE == 1
#  include "nmtms.h"
# endif /* NMT_GUARDING_ENABLE == 1 */
#endif /* NMT_MASTER_ENABLE == 1 */
#if STORE_PARAMETERS_SUPP == 1
# include "nonvolst.h"
#endif /* STORE_PARAMETERS_SUPP == 1 */

#if PROFILE_DS401 == 1
# include "ds401.h"
#endif /* PROFILE_DS401 == 1 */


/*--------------------------------------------------------------------*/
/*  external data                                                     */
/*--------------------------------------------------------------------*/
extern VComParaArea XDATA gStkGlobDat; /* Storage block for non-volatile data. */
#if EXAMPLE_OBJECTS_USED == 1
extern BYTE gDomainBuffer[EXAMPLE_BUFFER_SIZE]; /* example buffer for domain data */
#endif /* EXAMPLE_OBJECTS_USED == 1 */
#if SCOFLD_SUPPORT == 1
extern QBYTE gScofldConf; /* configuration of flash loader */
#endif /* SCOFLD_SUPPORT == 1 */

/* external from SDOSERV.C */
/* SDO abort codes table */
extern CONST DWORD CODE alSdoAbortCodes[];

extern void vSetAccelerometer_Sample_Rate(ubyte ubRate);

/*--------------------------------------------------------------------*/
/*  external functions                                                       */
/*--------------------------------------------------------------------*/
#if USE_CANOE_EMU == 1
void CANoeAPI_PutValueInt   (const char*, int);
void CANoeAPI_PutValueData  (const char*, BYTE*, int);
int  CANoeAPI_GetValueInt   (const char*);
void CANoeAPI_GetValueData  (const char*, BYTE*, int);
extern void tgtSignalOpState(void);
extern void tgtPdoReceived(void);
#endif /* USE_CANOE_EMU == 1 */


#if STORE_PARAMETERS_SUPP == 1
# if NV_SAVE_COMPAR == 1
void sStoreComPar(void);
BYTE sApplyComPar(void);
# endif /* NV_SAVE_COMPAR == 1 */

# if NV_SAVE_APPPAR == 1
STATIC void sStoreAppPar(void);
STATIC BYTE sApplyAppPar(void);
/*!
  \brief Non volatile storage for application relevant parameters
*/
struct {
  VStoreAreaHead Head; /*!< block management head */
  /*
  Add your application parameters to this area.
  */
} XDATA gAppPar;
# endif /* NV_SAVE_APPPAR == 1 */

# if NV_SAVE_MANPAR == 1
STATIC void sStoreManPar(void);
STATIC BYTE sApplyManPar(void);

/*!
  \brief Non volatile storage for manufacturer specific relevant parameters
*/
struct {
  VStoreAreaHead Head; /*!< block management head */
  /*
  Add your manufacturer parameters to this area.
  */
} XDATA gManPar;
# endif /* NV_SAVE_MANPAR == 1 */

CONST VStoreDesc CODE StoreBlks[] = {
# if NV_SAVE_COMPAR == 1
  /* Description of storage block for communication parameters. */
  STORE_BLOCK(
    PAR_SEL_COM,                       /* selector */
    "NV_COMM.BIN",                     /* file name for  file interface */
    "communication parameter",         /* name for debugging purposes */
    (VStoreAreaHead *)&gStkGlobDat,    /* pointer to storage block */
    sizeof(VComParaArea),              /* size of storage block */
    0,                                 /* offset in block selection */
    gStoreComPar,                      /* callback for "store" */
    gApplyComPar),                     /* callback for "apply" */
# endif /* NV_SAVE_COMPAR == 1 */
# if NV_SAVE_APPPAR == 1
  /* Description of storage block for application parameters. */
  STORE_BLOCK(
    PAR_SEL_APP,                       /* selector */
    "NV_APPL.BIN",                     /* file name for  file interface */
    "application parameter",           /* name for debugging purposes */
    (VStoreAreaHead *)&gAppPar,        /* pointer to storage block */
    sizeof(gAppPar),                   /* size of storage block */
    0,                                 /* offset in block selection */
    sStoreAppPar,                      /* callback for "store" */
    sApplyAppPar),                     /* callback for "apply" */
# endif /* NV_SAVE_APPPAR == 1 */
# if NV_SAVE_MANPAR == 1
  /* Description of storage block for manufacturer related parameters. */
  STORE_BLOCK(
    PAR_SEL_MAN,                       /* selector */
    "NV_MANU.BIN",                     /* file name for  file interface */
    "manufacturer parameter",          /* name for debugging purposes */
    (VStoreAreaHead *)&gManPar,        /* pointer to storage block */
    sizeof(gManPar),                   /* size of storage block */
    0,                                 /* offset in block selection */
    sStoreManPar,                      /* callback for "store" */
    sApplyManPar)                      /* callback for "apply" */
# endif /* NV_SAVE_MANPAR == 1 */
};

/*!
  Size of the global known object dictionary.
 */
CONST BYTE CODE gStoreBlkSz = sizeof(StoreBlks) / sizeof(StoreBlks[0]);

#endif /* STORE_PARAMETERS_SUPP == 1 */


/*--------------------------------------------------------------------*/
/*  function definitions                                              */
/*--------------------------------------------------------------------*/
#if STORE_PARAMETERS_SUPP == 1
static void sStoreAppPar(void)
{
# if USE_CANOE_EMU == 1
  CANoeAPI_PutValueInt("envStoreAppParClbck", TRUE);  /* set flag */
# endif /* USE_CANOE_EMU == 1 */
} /* sStoreAppPar */

static void sStoreManPar(void)
{
# if USE_CANOE_EMU == 1
  CANoeAPI_PutValueInt("envStoreManParClbck", TRUE);  /* set flag */
# endif /* USE_CANOE_EMU == 1 */
} /* sStoreManPar */
static BYTE sApplyAppPar(void)
{
# if USE_CANOE_EMU == 1
  CANoeAPI_PutValueInt("envApplyAppParClbck", TRUE);  /* set flag */
# endif /* USE_CANOE_EMU == 1 */
  return TRUE;
} /* sApplyAppPar */

static BYTE sApplyManPar(void)
{
# if USE_CANOE_EMU == 1
  CANoeAPI_PutValueInt("envApplyManParClbck", TRUE);  /* set flag */
# endif /* USE_CANOE_EMU == 1 */
  return TRUE;
} /* sApplyManPar */


#endif /* STORE_PARAMETERS_SUPP == 1 */

#if SDO_EXEC_USER_READ_CALLBACKS == 1
/*!
  \brief SDO server upload callback

  This callback is only executed for objects which are:
  - marked with #SDO_EXEC_CB_UP or #SDO_EXEC_CB_ALL

  Place specific handling for selected objects here.
  Separate the handling of the diverse indices by case-break blocks.
  For one given index further differentiation by the subindex is
  possible (ptServerSdoInst->bSubIndex).
  The length is available via ptServerSdoInst->SDO_DATASIZE.
  If the SDO should be aborted set the appropriate ABORT-CODE from
  the list (#sdoabortcodes) in <sdoserv.h> to bRet,
  otherwise leave the return value at it's default of zero.

  \param  ptServerSdoInst - pointer to the server SDO instance
  \retval 0 - no abort situation
  \retval #sdoabortcodes - reason for SDO abort
*/
BYTE gSdoS_UpCb(const VSdoAttrib XDATA *ptServerSdoInst)
{
  BYTE bRet = 0; /* return value (default: no abort situation) */

  switch (ptServerSdoInst->wbIndex.wc) {
# if EXAMPLE_OBJECTS_USED == 1
    case EXAMPLE_OBJECT1:
      break;
    case EXAMPLE_OBJECT2:
      break;
    case EXAMPLE_OBJECT3:
      break;
    case EXAMPLE_OBJECT4:
      break;
    case EXAMPLE_OBJECT5:
      break;
# endif /* EXAMPLE_OBJECTS_USED == 1 */
    /*
    Add your own handling here!
    */
# if SCOFLD_SUPPORT == 1
    case CONFIGURE_SCOFLD:
      Nvs_ReadChunk(0, (void *)&gScofldConf);
      break;
# endif /* SCOFLD_SUPPORT == 1 */
    default:
      bRet = 0; /* no special object dealing: return OK */
    break;
  } /* switch */
# if USE_CANOE_EMU == 1
  CANoeAPI_PutValueInt("envSdoSUpErrCode", bRet);
  CANoeAPI_PutValueInt("envSdoSUpClbck", TRUE); /* set flag */
# endif /* USE_CANOE_EMU == 1 */
  return bRet;
} /* gSdoS_UpCb */
#endif /* SDO_EXEC_USER_READ_CALLBACKS 1 */

#if SDO_EXEC_USER_WRITE_CALLBACKS == 1
/*!
  \brief SDO server callback

  This callback is only executed for objects which are
  - marked with #SDO_EXEC_CB_DOWN or #SDO_EXEC_CB_ALL
  - accessed with expedited transfer

  The length is available via ptServerSdoInst->SDO_DATASIZE.
  The user now can perform range checking or any other check.
  If no abort-code is provided via the return value bRet, a regular
  SDO response is created. If a specific abort-code
  (\ref sdoabortcodes in <sdoserv.h>) is set in bRet,
  the corresponding SDO abort message is created.

  Place specific handling for selected objects in this function.
  Separate the handling of the diverse indices by case-break blocks.
  For one given index further differentiation by the subindex is possible
  (ptServerSdoInst->bSubIndex).

  \param ptServerSdoInst - pointer to the server SDO instance
  \retval 0 - no abort situation
  \retval #sdoabortcodes - reason for SDO abort
*/
BYTE gSdoS_ExpDownCb(const VSdoAttrib XDATA *ptServerSdoInst)
{
  BYTE bRet = 0; /* return value (default: no abort situation) */

  switch (ptServerSdoInst->wbIndex.wc) {
# if EXAMPLE_OBJECTS_USED == 1
    case EXAMPLE_OBJECT1:
      break;
    case EXAMPLE_OBJECT2:
      break;
    case EXAMPLE_OBJECT3:
      break;
    case EXAMPLE_OBJECT4:
      break;
    case EXAMPLE_OBJECT5:
      break;
# endif /* EXAMPLE_OBJECTS_USED == 1 */
    /*
    Add your own handling here!
    */
# if SCOFLD_SUPPORT == 1
    case CONFIGURE_SCOFLD:
      break;
# endif /* SCOFLD_SUPPORT == 1 */

    default:
      bRet = 0; /* no special object dealing: return OK */
    break;
  } /* switch */
# if USE_CANOE_EMU == 1
  CANoeAPI_PutValueInt("envSdoSExpDownErrCode", bRet);
  CANoeAPI_PutValueInt("envSdoSExpDownClbck", TRUE);    /* set flag */
# endif /* USE_CANOE_EMU == 1 */
  return bRet;
} /* gSdoS_ExpDownCb */

/*!
  \brief SDO server callback

  This callback is only executed for objects which are
  - marked with #SDO_EXEC_CB_DOWN or #SDO_EXEC_CB_ALL
  - accessed with segemented/block transfer

  The data item itself is not provided since it is not available
  during that state of the transfer.
  If no abort-code is provided via the return value bRet, a regular
  SDO response is created. If a specific abort-code
  (\ref sdoabortcodes in <sdoserv.h>) is set in bRet, the corresponding
  SDO abort message is created.

  Place specific handling for selected objects in this function.
  Separate the handling of the diverse indices by case-break blocks.
  The length is available via ptServerSdoInst->SDO_DATASIZE.
  For one given index further differentiation by the subindex is possible
  (ptServerSdoInst->bSubIndex).

  \param ptServerSdoInst - pointer to the server SDO instance
  \retval 0 - no abort situation
  \retval #sdoabortcodes - reason for SDO abort
*/
BYTE gSdoS_DownCb(const VSdoAttrib XDATA *ptServerSdoInst)
{
  BYTE bRet = 0; /* return value (default: no abort situation) */

  switch (ptServerSdoInst->wbIndex.wc) {
# if EXAMPLE_OBJECTS_USED == 1
    case EXAMPLE_OBJECT1:
      break;
    case EXAMPLE_OBJECT2:
      break;
    case EXAMPLE_OBJECT3:
      break;
    case EXAMPLE_OBJECT4:
      break;
    case EXAMPLE_OBJECT5:
      break;
# endif /* EXAMPLE_OBJECTS_USED == 1 */
    /*
    Add your own handling here!
    */
    default:
      bRet = 0; /* no special object dealing: return OK */
    break;
  } /* switch */
  return bRet;
} /* gSdoS_DownCb() */

/*!
  \brief SDO server callback

  This callback is only executed for objects which are
  - marked with #SDO_EXEC_CB_DOWN or #SDO_EXEC_CB_ALL
  - accessed with expedited transfer

  Now the written data item is located at its final location.
  The application can now try to "apply" the parameter or whatever
  is planned to do with it.
  The length is available via ptServerSdoInst->SDO_DATASIZE.
  For one given index further differentiation by the subindex is possible
  (ptServerSdoInst->bSubIndex).

  \param ptServerSdoInst - pointer to the server SDO instance
  \returns nothing
*/
void gSdoS_ExpDownCbFinish(const VSdoAttrib XDATA *ptServerSdoInst)
{
  switch (ptServerSdoInst->wbIndex.wc) {
# if EXAMPLE_OBJECTS_USED == 1
    case EXAMPLE_OBJECT1:
      break;
    case EXAMPLE_OBJECT2:
      break;
    case EXAMPLE_OBJECT3:
      break;
    case EXAMPLE_OBJECT4:
      break;
    case EXAMPLE_OBJECT5:
      break;
# endif /* EXAMPLE_OBJECTS_USED == 1 */
  /*
  Add your own handling here!
  */
# if SCOFLD_SUPPORT == 1
    case CONFIGURE_SCOFLD:
      Nvs_WriteChunk(0, (void *)&gScofldConf);
      break;
# endif /* SCOFLD_SUPPORT == 1 */

    case OBJ_STATE_OSM:
        // gfOSM_AutoAdvanceMode = FALSE;
        break;

    case OBJ_LOCAL_EV_HIST:
        if ((ptServerSdoInst->bSubIndex == 1) && (gLocalEvents.History.uwEventsLogged == 0))
        {
            vClearLocalEventList();
        }
        if (gLocalEvents.History.uwEventsLogged > 15)
        {
            gLocalEvents.History.uwEventsLogged = 15;
        }
        break;

    case OBJ_RESET_COUNTS:
        vPutResetCounts(gulResetCounts);
        break;

    case OBJ_EMY_MSG_ACK:
        ubAckEvent(gulAckedEvent);
        break;

    default:
      break;
  } /* switch */
# if USE_CANOE_EMU == 1
  CANoeAPI_PutValueInt("envSdoSExpDownFinishClbck", TRUE);  /* set flag */
# endif /* USE_CANOE_EMU == 1 */
} /* gSdoS_ExpDownCbFinish */

/*!
  \brief SDO server callback

  This callback is only executed for objects which are
  - marked with #SDO_EXEC_CB_DOWN or #SDO_EXEC_CB_ALL
  - accessed with segemented/block transfer

  The transfer is completed at this point but the last answer from the server
  is not transmitted.
  Now there is the chance to evaluate the received data item.
  The data to be written is accessible via the buffer
  ptServerSdoInst->abSdoDataTmpBuffer[].
  The length is available via ptServerSdoInst->SDO_DATASIZE.

  If no abort-code is provided via the return value bRet, the data is written to
  the final location and a regular SDO response is created. If a specific
  abort-code (\ref sdoabortcodes in <sdoserv.h>) is set in bRet, the
  corresponding SDO abort message is created.

  Place specific handling for selected objects in this function.
  Separate the handling of the diverse indices by case-break blocks.
  For one given index further differentiation by the subindex is possible
  (ptServerSdoInst->bSubIndex).

  \param ptServerSdoInst - pointer to the server SDO instance
  \retval 0 - no abort situation
  \retval #sdoabortcodes - reason for SDO abort
*/
BYTE gSdoS_DownCbFinish(const VSdoAttrib XDATA *ptServerSdoInst)
{
  BYTE bRet = 0; /* return value (default: no abort situation) */

  switch (ptServerSdoInst->wbIndex.wc) {
# if EXAMPLE_OBJECTS_USED == 1
    case EXAMPLE_OBJECT1:
      break;
    case EXAMPLE_OBJECT2:
      break;
    case EXAMPLE_OBJECT3:
      break;
    case EXAMPLE_OBJECT4:
      break;
    case EXAMPLE_OBJECT5:
      break;
# endif /* EXAMPLE_OBJECTS_USED == 1 */
    /*
    Add your own handling here!
    */

    default:
      bRet = 0; /* no special object dealing: return OK */
    break;
  } /* switch */
  return bRet;
} /* gSdoS_DownCbFinish() */

#endif /* SDO_EXEC_USER_WRITE_CALLBACKS == 1 */


/*!
  \brief Get object's address

  If the address of an object is provided with the value 0
  this function is called. The user has to provide the
  valid address of the object during run-time.

  Firthermore this function takes care of a special feature
  of some micros. For these micros it is not possible
  to convert a pointer to ROM into a pointer to RAM.
  For that reason the data contents from ROM have to be copied
  to RAM in this function.
*/
BYTE XDATA * gSdoS_GetObjectAddress(const VSdoAttrib XDATA *ptServerSdoInst)
{

  switch (ptServerSdoInst->wbIndex.wc) {
#if EXAMPLE_OBJECTS_USED == 1
    case EXAMPLE_OBJECT2:
      if (ptServerSdoInst->bSubIndex == 0) {
        /* get the valid pointer to the data item */
        return((BYTE XDATA*)&gDomainBuffer[0]);
      } /* if */
      else {
        return(0);
      } /* else */

# ifdef NO_UNIVERSAL_POINTERS
    case EXAMPLE_OBJECT3:
      if (ptServerSdoInst->bSubIndex == 0) {
        /* provide the size of the structured object */
        ptServerSdoInst->bExpedBuf[0] = ExplCodeArea.mNrExample3[0];
      } /* if */
      return(0);
    case EXAMPLE_OBJECT5:
      if (ptServerSdoInst->bSubIndex == 0) {
        /* provide the size of the structured object */
        ptServerSdoInst->bExpedBuf[0] = 4;
      } /* if */
      return(0);
# endif /* NO_UNIVERSAL_POINTERS */

    case EXAMPLE_OBJECT4:
# ifdef NO_UNIVERSAL_POINTERS
      if (ptServerSdoInst->bSubIndex == 0) {
        /* provide the size of the structured object */
        ptServerSdoInst->bExpedBuf[0] = ExplCodeArea.mNrExample4[0];
        return(0);
      } /* if */
      else
# endif /* NO_UNIVERSAL_POINTERS */
      {
        if ((ptServerSdoInst->bSubIndex > 0)
          && (ptServerSdoInst->bSubIndex <= sizeof(gDomainBuffer)
              / ARRAY_ENTRY_SIZE4)) {
          /* get the valid pointer to example object 4 */
          return(&gDomainBuffer[(ptServerSdoInst->bSubIndex - 1) * ARRAY_ENTRY_SIZE4]);
        } /* if */
        else {
          return(0);
        } /* else */
      } /* else */

#endif /* EXAMPLE_OBJECTS_USED == 1 */

#if EXAMPLE_PROFILE_USED == 1
# ifdef NO_UNIVERSAL_POINTERS
    case DIGI_IN_8BIT:
      if (ptServerSdoInst->bSubIndex == 0) {
        /* provide the size of the structured object */
        ptServerSdoInst->bExpedBuf[0] = PrflCodeArea.mNr6000[0];
      } /* if */
      return(0);
    case DIGI_IN_16BIT:
      if (ptServerSdoInst->bSubIndex == 0) {
        /* provide the size of the structured object */
        ptServerSdoInst->bExpedBuf[0] = PrflCodeArea.mNr6100[0];
      } /* if */
      return(0);
    case DIGI_IN_32BIT:
      if (ptServerSdoInst->bSubIndex == 0) {
        /* provide the size of the structured object */
        ptServerSdoInst->bExpedBuf[0] = PrflCodeArea.mNr6120[0];
      } /* if */
      return(0);
    case DIGI_OUT_8BIT:
      if (ptServerSdoInst->bSubIndex == 0) {
        /* provide the size of the structured object */
        ptServerSdoInst->bExpedBuf[0] = PrflCodeArea.mNr6200[0];
      } /* if */
      return(0);
    case DIGI_OUT_16BIT:
      if (ptServerSdoInst->bSubIndex == 0) {
        /* provide the size of the structured object */
        ptServerSdoInst->bExpedBuf[0] = PrflCodeArea.mNr6300[0];
      } /* if */
      return(0);
    case DIGI_OUT_32BIT:
      if (ptServerSdoInst->bSubIndex == 0) {
        /* provide the size of the structured object */
        ptServerSdoInst->bExpedBuf[0] = PrflCodeArea.mNr6320[0];
      } /* if */
      return(0);
# endif /* NO_UNIVERSAL_POINTERS */
#endif /* EXAMPLE_PROFILE_USED == 1 */

#ifdef NO_UNIVERSAL_POINTERS
    case DEVICE_NAME:
      /*
      Be sure that the device's name is up to 4 bytes in length,
      then copy the data to the buffer bExpedBuf[].
      */
      ptServerSdoInst->bExpedBuf[0] =
        (*((QBYTE CONST_PTR *)PRODUCT_NAME)).fb.b0;
      ptServerSdoInst->bExpedBuf[1] =
        (*((QBYTE CONST_PTR *)PRODUCT_NAME)).fb.b1;
      ptServerSdoInst->bExpedBuf[2] =
        (*((QBYTE CONST_PTR *)PRODUCT_NAME)).fb.b2;
      ptServerSdoInst->bExpedBuf[3] =
        (*((QBYTE CONST_PTR *)PRODUCT_NAME)).fb.b3;
      return(0);

    case HW_VER:
      {
        /*
        Be sure that the device's name is up to longer than 4 bytes
        in length, then copy the data to the buffer abSdoDataTmpBuffer[].
        */
        BYTE i;
        if (ptServerSdoInst->SDO_DATASIZE > SDO_DATA_BUFFER_SIZE) {
          return(0);
        } /* if */
        else {
          for (i=0; i<ptServerSdoInst->SDO_DATASIZE; i++) {
            ptServerSdoInst->abSdoDataTmpBuffer[i] =
              ((BYTE CONST_PTR *)HW_VERSION)[i];
          } /* for */
          return(ptServerSdoInst->abSdoDataTmpBuffer);
        } /* else */
      }
      break;

    case SW_VER:
      {
        /*
        Be sure that the device's name is up to longer than 4 bytes
        in length, then copy the data to the buffer abSdoDataTmpBuffer[].
        */
        BYTE i;
        if (ptServerSdoInst->SDO_DATASIZE > SDO_DATA_BUFFER_SIZE) {
          return(0);
        } /* if */
        else {
          for (i=0; i<ptServerSdoInst->SDO_DATASIZE; i++) {
            ptServerSdoInst->abSdoDataTmpBuffer[i] =
              ((BYTE CONST_PTR *)SW_VERSION)[i];
          } /* for */
          return(ptServerSdoInst->abSdoDataTmpBuffer);
        } /* else */
      }
#endif /* NO_UNIVERSAL_POINTERS */

    /*
    Add your own handling here!
    */
    default:
      return(0); /* no special object dealing: return OK */
  } /* switch */
} /* gSdoS_DownCbFinish() */



/*!
  \brief provide object length

  For objects in the object dictionary which are
  defined with length 0 this function will be called during
  a dictionary access.
  If the user adds such an entry this function must be adapted.

  \param wIndex - object's index
  \param bSubIndex - object's subindex

  \retval == 0 - no entry for this object,
  \retval != 0 - length of object
*/
#if (SDO_WRITE_SEG_ALLOWED == 0) && (SDO_READ_SEG_ALLOWED == 0)
BYTE
#else
DWORD
#endif /* (SDO_WRITE_SEG_ALLOWED == 0) && (SDO_READ_SEG_ALLOWED == 0) */
gSdoS_GetObjLenCb(WORD wIndex, BYTE bSubIndex)
{
  switch (wIndex) {
#if EXAMPLE_OBJECTS_USED == 1
# if (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1)
    case EXAMPLE_OBJECT2: /* get length of example object 2 */
      if (bSubIndex == 0) {
        return sizeof(gDomainBuffer);
      } /* if */
      else {
        return 0;
      } /* else */

    case EXAMPLE_OBJECT4: /* get length of example object 4 */
      if ((bSubIndex > 0)
        && (bSubIndex <= sizeof(gDomainBuffer) / ARRAY_ENTRY_SIZE4)) {
        return ARRAY_ENTRY_SIZE4;
      } /* if */
      else {
        return 0;
      } /* else */
# endif /* (SDO_WRITE_SEG_ALLOWED == 1) || (SDO_READ_SEG_ALLOWED == 1) */
#endif /* EXAMPLE_OBJECTS_USED == 1 */
    case DEVICE_NAME:
#ifndef __18CXX
      return(strlen(PRODUCT_NAME));
#else
      return 4;
#endif /* __18CXX */
    case HW_VER:
#ifndef __18CXX
      return(strlen(HW_VERSION));
#else
      return 8;
#endif /* __18CXX */
    case SW_VER:
#ifndef __18CXX
      return(strlen(SW_VERSION));
#else
      return 11;
#endif /* __18CXX */

    /*
    Add your own handling to this section.
    Use wIndex and bSubIndex to identify the accessed object.
    If you have determined the object's length return it
    with
    return(your_length);
    */

    default:
      switch (bSubIndex) {
        case 0:
          return 0;
        default:
          return 0;
      } /* switch */
  } /* switch */
} /* gSdoS_GetObjLenCb */


#if CLIENT_ENABLE == 1
/*!
  \brief Signal client SDO transfer completion (to be filled by the user)

  When a client access is finished successfully or is failed
  this function will be called.
  \param bSuccess - TRUE/FALSE
*/
void gSdoC_Completed(BYTE bSuccess)
{
/* command state specifier */
#if USE_CANOE_EMU == 1
# define COMMAND_EXECUTED   0x00
# define COMMAND_VALID      0x01
# define UNKNOWN_SERVICE    0x02
# define NOT_EXECUTED       0x03
extern BYTE bEmuData[];
#endif /* USE_CANOE_EMU == 1 */

  if (bSuccess == TRUE) {
    /* SDO transfer successfully completed */
    VDEBUG_RPT0("Client transfer successfully completed\n");

#if USE_CANOE_EMU == 1
    CANoeAPI_PutValueData("envData", bEmuData, 4);               /* only 4 bytes supported */
    CANoeAPI_PutValueInt("envCommandState", COMMAND_EXECUTED); /* command was executed */
#endif /* USE_CANOE_EMU == 1 */
  } /* if */
  else {
    /* SDO transfer failed */
    VDEBUG_RPT0("Client transfer failed\n");

#if USE_CANOE_EMU == 1
    CANoeAPI_PutValueInt("envCommandState", NOT_EXECUTED); /* command was not executed */
#endif /* USE_CANOE_EMU == 1 */
  } /* else */
} /* gSdoC_Completed */
#endif /* CLIENT_ENABLE == 1 */


#if QUEUED_MODE == 1
# if SIGNAL_EMCYMSG == 1
/*!
  \brief Callback for EMCY events.

  This function is enabled if the code is compiled with
  #SIGNAL_EMCYMSG = 1 and #QUEUED_MODE = 1.
  The reception of a EMCY message is signalled to the application.
  \param bNodeId - module id of the module which transmits the EMCY message.
  \param pData - pointer to data of the EMCY message.
*/
void EmcyEventRcv(BYTE bNodeId, BYTE * pData)
{
  /*
  Add your own handling here!
  */
#  if USE_CANOE_EMU == 1
     CANoeAPI_PutValueInt("envEmcyEventRcv", TRUE);   /* set flag */
     CANoeAPI_PutValueInt("envNodeID", bNodeId);      /* set node ID */
     CANoeAPI_GetValueData("envEmcyData", pData, 8);  /* copy data */
#  endif /* USE_CANOE_EMU == 1 */
} /* EmcyEventRcv() */
# endif /* SIGNAL_EMCYMSG == 1 */

# if SIGNAL_BOOTUPMSG == 1
/*!
  \brief Callback for bootup events.

  This function is enabled if the code is compiled with
  #SIGNAL_BOOTUPMSG = 1 and #QUEUED_MODE = 1.
  The reception of an error control message is signalled to the application.
  \param bNodeId - module id of the module which transmits the boot up message.
*/
void BootUpEventRcv(BYTE bNodeId)
{
  /*
  Add your own handling here!
  */
#  if USE_CANOE_EMU == 1
     CANoeAPI_PutValueInt("envBootUpEventRcv", TRUE);   /* set flag */
     CANoeAPI_PutValueInt("envNodeID", bNodeId);   /* set flag */
#  endif /* USE_CANOE_EMU == 1 */
} /* BootUpEventRcv() */
# endif /* SIGNAL_BOOTUPMSG == 1 */
#endif /* QUEUED_MODE == 1 */


#if SIGNAL_SYNC == 1
/*!
  \brief Callback for SYNC events.

  This function is enabled if the code is compiled with
  #SIGNAL_SYNC = 1 and #SYNC_USED = 1.
  The reception of a SYNC message is signalled to the application.
*/
void SyncEventRcv(void)
{
  /*
  Add your own handling here!
  */
# if USE_CANOE_EMU == 1
    CANoeAPI_PutValueInt("envSyncEventRcv", TRUE);   /* set flag */
# endif /* USE_CANOE_EMU == 1 */
} /* SyncEventRcv() */
#endif /* SIGNAL_SYNC == 1 */


#if MAX_RX_PDOS > 0
# if PDO_EXEC_USER_RECEIVE_CALLBACKS == 1
/*!
  \brief Receive PDO callback function.

  The receive PDO callback function will be executed when
  a receive PDO has been analyzed. The callback functions
  parameter is the index in the PDO table (0..n).
  \param bPdoIndex - index in PDO table
*/
void gSlvRxPdoCallback(BYTE bPdoIndex)
{
    switch (bPdoIndex)
    {
        case DATALOG_REQ_IDX:
            break;

        case DM_PDO_IDX:

            vEventMaskReport();
            guwLocalControlWord = gDM_Data.uwControlWord & guwEventHandlerMask;
            gubLastFreshnessCounter = gDM_Data.ubFreshnessCounter;
            break;


        case SVCM_PDO3_IDX:
            guwPdoStatus |= PDOCHK_VCM_PDO_RX;
            break;

        case CAN_KYBD_RX_IDX:
          
             break;


        default:
            break;;
    }

} /* gSlvRxPdoCallback() */
# endif /* PDO_EXEC_USER_RECEIVE_CALLBACKS == 1 */
#endif /* MAX_RX_PDOS > 0 */


#if SIGNAL_STACK_EVENT == 1
/*!
  \brief Generic stack event callback

  \param eventtype - \ref eventcodes
  \param details - \ref detailledeventcodes
*/
void gSlvStackEventCallback(BYTE eventtype, BYTE details)
{

  switch (eventtype) {
    case STACK_STATE: /* handling of internal states */
      switch (details) {
      case EXECUTE_LSS:
        /* Add your event handling code here */
        break;
      case WAIT_FOR_STARTUP:
        /* Add your event handling code here */
        break;
      case STACK_ENABLED:
        /* Add your event handling code here */
        break;
      case STACK_DEFERRED:
        /* Add your event handling code here */
        break;
      default:
        /* will never occur */
        break;
      } /* switch */
    break;

# if SIGNAL_STATE_CHANGE == 1
    case COMM_STATE: /* handling of communication states */
      switch (details) {
        case BOOTUP:
          /* Add your event handling code here */
          break;
        case STOPPED:
          /* Add your event handling code here */
          break;
        case OPERATIONAL:
          /* Add your event handling code here */
#if USE_CANOE_EMU == 1
          /*
          This is example code for the adaptation to the
          osCAN environment
          */
          tgtSignalOpState();
#endif /* USE_CANOE_EMU == 1 */
          break;
        case PRE_OPERATIONAL:
          /* Add your event handling code here */
          break;
        default:
          /* will never occur */
          break;
      } /* switch */
      break;
# endif /* SIGNAL_STATE_CHANGE == 1 */

# if (HEARTBEAT_CONSUMER==1) && (SIGNAL_HEARTBEAT_EVENT==1)
    case HB_EVT: /* heartbeat consumer event */
      /* Add your event handling code here */
      break;
# endif /* (HEARTBEAT_CONSUMER==1) && (SIGNAL_HEARTBEAT_EVENT==1) */

# if SIGNAL_GUARDFAILURE == 1
    case GD_EVT: /* guarding failure event */
      /* Add your event handling code here */
      break;
# endif /* SIGNAL_GUARDFAILURE == 1 */

# if (TIME_STAMP_USED==1) && (EXEC_USER_TIME_STAMP_CALLBACKS==1)
    case TIME_STP: /* time stamp event */
      /* Add your event handling code here */
      break;
# endif /* (TIME_STAMP_USED==1) && (EXEC_USER_TIME_STAMP_CALLBACKS==1) */

    case CAN_INFO: /* CAN related information */
      switch (details) {
        case CAN_OVERRUN:
          /* Add your event handling code here */
          break;
        case CAN_WARNING:
          /* Add your event handling code here */
          break;
        case CAN_ACTIVE:
          /* Add your event handling code here */
          break;
        case BUS_OFF:
          /* Add your event handling code here */
          break;
        default:
          /* will never occur */
          break;
      } /* switch */
      break;

# if NMT_MASTER_ENABLE == 1
#  if NMT_GUARDING_ENABLE == 1
    case MM_GD_EVT: /* NMT master guarding event */
      switch (gNdm_CheckGuarding(details)) {
        case GD_MISSED:
          /* response is missed for Node-ID "details" */
          /* Add your event handling code here */
          break;
        case GD_DEAD:
          /* no response and life time elapsed for Node-ID "details" */
          /* Add your event handling code here */
          break;
        case GD_TOGGLE:
          /* toggle error for Node-ID "details" */
          /* Add your event handling code here */
          break;
        case GD_STATE:
          /* state changed for Node-ID "details" */
          /* Add your event handling code here */
          break;
        default:
          /* will never occur */
          break;
      } /* switch */
      break;
#  endif /* NMT_GUARDING_ENABLE == 1 */
# endif /* NMT_MASTER_ENABLE == 1 */

    default:

      break;
  } /* switch */
# if USE_CANOE_EMU == 1
    CANoeAPI_PutValueInt("envSlvStackEventClbck", TRUE);   /* set flag */
    CANoeAPI_PutValueInt("envSlvStackEventType", eventtype);   /* set event type */
    CANoeAPI_PutValueInt("envSlvStackEventDetails", details);   /* set details */
# endif /* USE_CANOE_EMU == 1 */
} /* gSlvStackEventCallback */
#endif /* SIGNAL_STACK_EVENT == 1 */


#if ACP_USED == 1
/*!
  \brief ACP idle loop callback

  This callback is executed every cycle in the ACP loop.
*/
void gAcpIdleCycleCb(void)
{
  ;
} /* gAcpIdleCycleCb */


/*!
  \brief ACP claim message receive callback

  This callback is executed every received claiming message.

  \param ptCanClaimBuffer pointer to CAN message
*/
void gAcpClaimRxCb(CAN_MSG XDATA * ptCanClaimBuffer)
{
  ;
} /* gAcpClaimRxCb */


/*!
  ACP number of claim messages callback

  \return number of claiming messages
*/
BYTE gAcpNumClaimFramesCb(void)
{
  return 3;
}

/*!
  \brief ACP claim message transmit callback

  This callback is executed before transmitting a claiming message.

  \param bNr claiming message number
  \param ptCanClaimBuffer pointer to CAN message
*/
void gAcpClaimTxCb(BYTE bNr, CAN_MSG XDATA * ptCanClaimBuffer)
{
  switch (bNr) {
    case 0: /* do not change the content of frame 0 */
      break;

    case 1:
      ptCanClaimBuffer->bDb[0] = 0x10;  /* frame type */
      ptCanClaimBuffer->bDb[1] = 1;     /* number of frames following */
      ptCanClaimBuffer->bDb[2] = (BYTE)VENDOR_ID_VAL;
      ptCanClaimBuffer->bDb[3] = (BYTE)(VENDOR_ID_VAL >> 8);
      ptCanClaimBuffer->bDb[4] = (BYTE)(VENDOR_ID_VAL >> 16);
      ptCanClaimBuffer->bDb[5] = (BYTE)(VENDOR_ID_VAL >> 24);
      ptCanClaimBuffer->bDb[6] = 0;
      ptCanClaimBuffer->bDb[7] = 0;
      break;

    case 2:
      ptCanClaimBuffer->bDb[0] = 0x10;  /* frame type */
      ptCanClaimBuffer->bDb[1] = 0;     /* number of frames following */
      ptCanClaimBuffer->bDb[2] = (BYTE)SERIAL_NUM_VAL;
      ptCanClaimBuffer->bDb[3] = (BYTE)(SERIAL_NUM_VAL >> 8);
      ptCanClaimBuffer->bDb[4] = (BYTE)(SERIAL_NUM_VAL >> 16);
      ptCanClaimBuffer->bDb[5] = (BYTE)(SERIAL_NUM_VAL >> 24);
      ptCanClaimBuffer->bDb[6] = 0;
      ptCanClaimBuffer->bDb[7] = 0;
      break;

    default:
      break;
  } /* switch */
} /* gAcpClaimTxCb */
#endif /* ACP_USED == 1 */


/*!
  \brief Process evaluation.

  This function is the right place to put code in, which deals with
  process data.
*/
void Process(void)
{
 /* EXAMPLE - copy inputs to outputs */
#if EXAMPLE_PROFILE_USED == 1
# if (MAX_RX_PDOS > 0) && (MAX_TX_PDOS > 0)
  BYTE XDATA i;                 /* counter                              */
  for (i = 0; i < INPUT_BUFFER_LEN; i++) {
    if (i < OUTPUT_BUFFER_LEN) {
#  if USE_CANOE_EMU == 1
#  else
      *INPUT_DATA(i) = *OUTPUT_DATA(i);
#  endif /* USE_CANOE_EMU == 1 */
    } /* if */
  } /* for */
# endif /* (MAX_RX_PDOS > 0) && (MAX_TX_PDOS > 0) */
#endif /* EXAMPLE_PROFILE_USED == 1 */

#if GENERIC_PROFILE_USED == 1
  PROFILE_TASK_OPERATIONAL();
#endif /* GENERIC_PROFILE_USED == 1 */
} /* Process */


/*!
  \brief Process evaluation in #PRE_OPERATIONAL state.

  This function is the right place to put code in, which deals with
  process data in #PRE_OPERATIONAL state.
*/
#if GENERIC_PROFILE_USED == 1
void ProcessPreoperational(void)
{
  PROFILE_TASK_PREOPERATIONAL();
} /* ProcessPreoperational */
#endif /* GENERIC_PROFILE_USED == 1 */


/*!
  \brief Determine BootUp behaviour.

  This function determines the format of the bootup message.
  - version 3 BootUp: emergency COB-ID with 0 data bytes
  - version 4 BootUp: error control COB-ID with 1 data byte (= 0)

  The source could be DIP switches, jumpers or nonvolatile memory.
  \retval TRUE  - version 3 BootUp
  \retval FALSE - version 4 BootUp
*/
BOOLEAN Version3BootUp(void)
{
  return FALSE;
} /* Version3BootUp */


/*!
  \brief Get the module ID.

  This function will determine the module ID. The source could be
  DIP switches, jumpers or nonvolatile memory.
  \return module ID (1..127)
*/
BYTE ReadBoardAdr(void)
{
    return (BYTE)ReturnModuleId();
#if 0
#if STORE_PARAMETERS_SUPP == 1
  return gStkGlobDat.mBoardAdr;
#else
  return 0xFF;
#endif /* STORE_PARAMETERS_SUPP == 1 */
#endif
} /* ReadBoardAdr */


/*!
  \brief Get the baud rate.

  This function will determine the baud rate. The source could be
  DIP switches, jumpers or nonvolatile memory.
  \return baud rate table index (0..8)
*/
BYTE ReadBaudRate(void)
{
    return (BYTE)ReturnCANBaudRate();
#if 0
#if STORE_PARAMETERS_SUPP == 1
  return gStkGlobDat.mBaudRate;
#else
  return 0xFF;
#endif /* STORE_PARAMETERS_SUPP == 1 */
#endif
} /* ReadBaudRate */


/*!
  Module initialization.
*/
void ModulInit(void)
{
#if (EXAMPLE_PROFILE_USED==1) || (GENERIC_PROFILE_USED==1)

#if (OUTPUT_BUFFER_LEN < 256) && (INPUT_BUFFER_LEN < 256)
  BYTE XDATA i;   /* counter */
#else
  WORD XDATA i;   /* counter */
#endif /* (OUTPUT_BUFFER_LEN < 256) && (INPUT_BUFFER_LEN < 256) */
#endif

#if EXAMPLE_PROFILE_USED==1
# if MAX_RX_PDOS > 0
  /* initialize process variables */
  for (i = 0; i < OUTPUT_BUFFER_LEN; i++) {
    *OUTPUT_DATA(i) = 0;
  }
# endif /* MAX_RX_PDOS > 0 */

# if MAX_TX_PDOS > 0
  for (i = 0; i < INPUT_BUFFER_LEN; i++) {
    *INPUT_DATA(i) = 0;
  }
# endif /* MAX_TX_PDOS > 0 */
#endif /* EXAMPLE_PROFILE_USED==1 */

#if GENERIC_PROFILE_USED == 1
# if PROFILE_DS401 == 1
#  if MAX_RX_PDOS > 0
  /* initialize process variables */
  for (i = 0; i < OUTPUT_BUFFER_LEN_DO; i++) {
    abOutDataDig[i] = 0;
  } /* for */

  for (i = 0; i < OUTPUT_BUFFER_LEN_AO; i++) {
    awOutDataAna[i] = 0;
  } /* for */
#  endif /* MAX_RX_PDOS > 0 */

#  if MAX_TX_PDOS > 0
  /* initialize process variables */
  for (i = 0; i < INPUT_BUFFER_LEN_DI; i++) {
    abInDataDig[i] = 0;
#   if DS401_TEST == 1
    abInRawDig[i] = 0;
#   endif /* DS401_TEST == 1 */
  } /* for */

  for (i = 0; i < INPUT_BUFFER_LEN_AI; i++) {
    awInDataAna[i] = 0;
#   if DS401_TEST == 1
    awInRawAna[i] = 0;
#   endif /* DS401_TEST == 1 */
  } /* for */
#  endif /* MAX_TX_PDOS > 0 */
# endif /* PROFILE_DS401 == 1 */
#endif /* GENERIC_PROFILE_USED == 1 */
} /* ModulInit */


/*!
  \brief Module reset.

  This function is called from the protocol stack to reset this module.
  A reason is for instance the NMT reset node service.
*/
void ModulReset(void)
{
  Tgt_Reset();
}


/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief CANopen slave user callbacks.
  \par File name usrclbck.c
  \version 26
  \date 3.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
