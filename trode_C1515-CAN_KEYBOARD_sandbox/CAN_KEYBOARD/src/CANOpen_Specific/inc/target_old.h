/*
 *  Copyright 2006 by Texas Instruments Incorporated.
 *  All rights reserved. Property of Texas Instruments Incorporated.
 *  Restricted rights to use, duplicate or disclose this code are
 *  granted through contract.
 *
 *  @(#) RTDX_Examples 5,3,1 08-30-2006 (rtdxtcEx-b14)
 */
/*************************************************************************/
/* initialization details                                                */
/*************************************************************************/
#ifndef __TARGET_H
#define __TARGET_H

#ifdef _TMS320C28X

/*  
    RTDX Init functions are called by C28x runtimes during PINIT.
    RTDX_Init_RT_Monitor will enable the DLOG interrupt.
    However, the user must enable the global interrupt flag.
*/

/* Enable global ints & disable WD Timer */
#define TARGET_INITIALIZE() \
    asm(" CLRC VMAP " ); \
    asm(" CLRC INTM " ); \
    asm(" EALLOW" ); \
    asm(" NOP"); \
    asm(" NOP"); \
    asm(" NOP"); \
    asm(" NOP"); \
    DISABLE_WD(); \
    asm(" EDIS");

/* Disable Watchdog Timer */
#define DISABLE_WD() \
    { \
        unsigned short* wdReg1 = (unsigned short*)0x7029; \
        unsigned short* wdReg2 = (unsigned short*)0x7025; \
        *wdReg1 |= 0x0068;               \
        *wdReg2  = 0x0055;               \
        *wdReg2  = 0x00AA;               \
    }

#endif // _TMS320C28X

#ifdef _TMS320C5XX

#define TARGET_INITIALIZE()

#endif // _TMS320C5XX

#ifdef __TMS320C55X__

/*  
    RTDX Init functions are called by C55x runtimes during PINIT.
    RTDX_Init_RT_Monitor will enable the DLOG interrupt.
    However, the user must enable the global interrupt flag.
*/
#define TARGET_INITIALIZE() \
asm(" .if .ALGEBRAIC\n" \
    " BIT (ST1, #ST1_INTM) = #0\n" \
    " .elseif .MNEMONIC\n" \
    " BCLR ST1_INTM, ST1_55\n" \
    " .endif"); /* Enable global ints */

#endif // __TMS320C55X__
 
#ifdef _TMS320C6200

#include <c6x.h>                /* IER,ISR,CSR registers                */

/*
    RTDX is interrupt driven on the C6x.
    So enable the interrupts now, or it won't work.
*/
#ifdef HSRTDX
#define IER_NMIE 0x0000180A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#else
#define IER_NMIE 0x0000000A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#endif //HSRTDX

#endif // _TMS320C6200

#ifdef _TMS320C6400

#include <c6x.h>                /* IER,ISR,CSR registers                */

/*
    RTDX is interrupt driven on the C6x.
    So enable the interrupts now, or it won't work.
*/
#ifdef HSRTDX
#define IER_NMIE 0x0000180A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#else
#define IER_NMIE 0x0000020A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#endif //HSRTDX

#endif // _TMS320C6400

#ifdef _TMS320C6400_PLUS

#include <c6x.h>                /* IER,ISR,CSR registers                */

/*
    RTDX is interrupt driven on the C6x.
    So enable the interrupts now, or it won't work.
*/
#ifdef HSRTDX
#define IER_NMIE 0x0000180A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#else
#define IER_NMIE 0x0000020A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#endif //HSRTDX

#endif // _TMS320C6400_PLUS

#ifdef _TMS320C6700

#include <c6x.h>                /* IER,ISR,CSR registers                */

/*
    RTDX is interrupt driven on the C6x.
    So enable the interrupts now, or it won't work.
*/
#ifdef HSRTDX
#define IER_NMIE 0x0000180A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#else
#define IER_NMIE 0x0000000A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#endif //HSRTDX

#endif // _TMS320C6700

#ifdef _TMS320C6700_PLUS

#include <c6x.h>                /* IER,ISR,CSR registers                */

/*
    RTDX is interrupt driven on the C6x.
    So enable the interrupts now, or it won't work.
*/
#ifdef HSRTDX
#define IER_NMIE 0x0000180A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#else
#define IER_NMIE 0x0000000A
#define CSR_GIE  0x00000001
#define TARGET_INITIALIZE() \
        IER |= 0x00000001 | IER_NMIE; \
        CSR |= CSR_GIE;
#endif //HSRTDX

#endif // _TMS320C6700_PLUS

#endif /*__TARGET_H */

