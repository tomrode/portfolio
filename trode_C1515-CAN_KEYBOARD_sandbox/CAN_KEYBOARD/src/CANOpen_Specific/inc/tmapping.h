/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/* --------------------------------------------------------------------*/

/*!
  \file
  \brief Transmit PDO mapping parameter.

  Be sure that the referenced variables are mappable, readable
  and that the objects exist in the object dictionary.

  \par File name tmapping.h
  \version 1
  \date 25.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

# if MAX_TX_PDOS > 0

{
    NUM_OF_MAP_ENTRIES(8),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 25, 8, (BYTE *)&gDatalog4.ubData[0]),     /* transmit PDO 1    */
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 26, 8, (BYTE *)&gDatalog4.ubData[1]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 27, 8, (BYTE *)&gDatalog4.ubData[2]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 28, 8, (BYTE *)&gDatalog4.ubData[3]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 29, 8, (BYTE *)&gDatalog4.ubData[4]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 30, 8, (BYTE *)&gDatalog4.ubData[5]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 31, 8, (BYTE *)&gDatalog4.ubData[6]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 32, 8, (BYTE *)&gDatalog4.ubData[7]),
}
# endif /* MAX_TX_PDOS > 0 */

# if MAX_TX_PDOS > 1
,
{
    NUM_OF_MAP_ENTRIES(8),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 17, 8, (BYTE *)&gDatalog3.ubData[0]),     /* transmit PDO 2   */
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 18, 8, (BYTE *)&gDatalog3.ubData[1]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 19, 8, (BYTE *)&gDatalog3.ubData[2]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 20, 8, (BYTE *)&gDatalog3.ubData[3]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 21, 8, (BYTE *)&gDatalog3.ubData[4]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 22, 8, (BYTE *)&gDatalog3.ubData[5]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 23, 8, (BYTE *)&gDatalog3.ubData[6]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 24, 8, (BYTE *)&gDatalog3.ubData[7]),
}
# endif /* MAX_TX_PDOS > 1 */

# if MAX_TX_PDOS > 2
,
{
    NUM_OF_MAP_ENTRIES(8),
    MAP_ENTRY(OBJ_DATALOG_BLOCK,  9, 8, (BYTE *)&gDatalog2.ubData[0]),     /* transmit PDO 3    */
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 10, 8, (BYTE *)&gDatalog2.ubData[1]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 11, 8, (BYTE *)&gDatalog2.ubData[2]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 12, 8, (BYTE *)&gDatalog2.ubData[3]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 13, 8, (BYTE *)&gDatalog2.ubData[4]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 14, 8, (BYTE *)&gDatalog2.ubData[5]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 15, 8, (BYTE *)&gDatalog2.ubData[6]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 16, 8, (BYTE *)&gDatalog2.ubData[7]),
}
# endif /* MAX_TX_PDOS > 2 */

# if MAX_TX_PDOS > 3
,
{
    NUM_OF_MAP_ENTRIES(8),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 1, 8, (BYTE *)&gDatalog1.ubData[0]),     /* transmit PDO 4    */
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 2, 8, (BYTE *)&gDatalog1.ubData[1]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 3, 8, (BYTE *)&gDatalog1.ubData[2]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 4, 8, (BYTE *)&gDatalog1.ubData[3]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 5, 8, (BYTE *)&gDatalog1.ubData[4]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 6, 8, (BYTE *)&gDatalog1.ubData[5]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 7, 8, (BYTE *)&gDatalog1.ubData[6]),
    MAP_ENTRY(OBJ_DATALOG_BLOCK, 8, 8, (BYTE *)&gDatalog1.ubData[7]),
}
# endif /* MAX_TX_PDOS > 3 */

#  if MAX_TX_PDOS > 4
,
{
    // Error_TX
    NUM_OF_MAP_ENTRIES(6),                                                /* Transmit PDO 4   */
    MAP_ENTRY(OBJ_EMERGENCY_PDO, 1, 16, (BYTE *)&gError_Data.uwErrorCode),
    MAP_ENTRY(OBJ_EMERGENCY_PDO, 2, 8,  (BYTE *)&gError_Data.ubErrorRegister),
    MAP_ENTRY(OBJ_EMERGENCY_PDO, 3, 16, (BYTE *)&gError_Data.uwEventNumber),
    MAP_ENTRY(OBJ_EMERGENCY_PDO, 4, 8,  (BYTE *)&gError_Data.ubSpare[0]),
    MAP_ENTRY(OBJ_EMERGENCY_PDO, 5, 8,  (BYTE *)&gError_Data.ubSpare[1]),
    MAP_ENTRY(OBJ_EMERGENCY_PDO, 6, 8,  (BYTE *)&gError_Data.ubSpare[2]),
    VOID_MAP_ENTRY,
    VOID_MAP_ENTRY,
}
# endif /* MAX_TX_PDOS > 4 */



#  if MAX_TX_PDOS > 5
,
{
    // Error_TX2
    NUM_OF_MAP_ENTRIES(7),                                                   /* Transmit PDO 5   */    
    MAP_ENTRY(OBJ_CAN_KEYBOARD, 1, 8, (BYTE *)&TpdoCanTableOutput.ubData[0]),
    MAP_ENTRY(OBJ_CAN_KEYBOARD, 2, 8, (BYTE *)&TpdoCanTableOutput.ubData[1]),
    MAP_ENTRY(OBJ_CAN_KEYBOARD, 3, 8, (BYTE *)&TpdoCanTableOutput.ubData[2]),
    MAP_ENTRY(OBJ_CAN_KEYBOARD, 4, 8, (BYTE *)&TpdoCanTableOutput.ubData[3]),
    MAP_ENTRY(OBJ_CAN_KEYBOARD, 5, 8, (BYTE *)&TpdoCanTableOutput.ubData[4]),
    MAP_ENTRY(OBJ_CAN_KEYBOARD, 6, 8, (BYTE *)&TpdoCanTableOutput.ubData[5]),
    MAP_ENTRY(OBJ_CAN_KEYBOARD, 7, 8, (BYTE *)&TpdoCanTableOutput.ubData[6]),
    VOID_MAP_ENTRY
}
# endif /* MAX_TX_PDOS > 5 */


