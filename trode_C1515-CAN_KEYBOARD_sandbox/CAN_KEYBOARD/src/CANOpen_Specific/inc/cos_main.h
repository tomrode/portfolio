/*--------------------------------------------------------------------
       COS_MAIN.H
   --------------------------------------------------------------------
       Copyright (C) 1998-2003 Vector Informatik GmbH, Stuttgart

       Function: Main header for CANopen slave
  --------------------------------------------------------------------*/


/*
START CONFIGURATOR CONTROL UNIT

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
MAX_RX_PDOS             | PDO Handling      | GENERAL |    INT  |   0 | 200
MAX_TX_PDOS             | PDO Handling      | GENERAL |    INT  |   0 | 200
MAX_RX_RMTPDOS          | PDO Handling      | GENERAL |    INT  |   0 |   4
MAX_RX_RMTPDOS          | PDO Handling      | GENERAL |    INT  |   0 |   4
RPDO_PAR_READONLY       | PDO Handling      |    BOOL |   BOOL  |     |
TPDO_PAR_READONLY       | PDO Handling      |    BOOL |   BOOL  |     |
RPDO_MAP_READONLY       | PDO Handling      |    BOOL |   BOOL  |     |
TPDO_MAP_READONLY       | PDO Handling      |    BOOL |   BOOL  |     |
RPDO_DIRECT_COPY        | PDO Handling      |    BOOL |   BOOL  |     |
TPDO_DIRECT_COPY        | PDO Handling      |    BOOL |   BOOL  |     |
USE_DUMMY_MAPPING       | PDO Handling      |    BOOL |   BOOL  |     |
OBJECTS_64BIT_USED      | PDO Handling      |    BOOL |   BOOL  |     |
ADD_NODE_ID_TO_PDO      | PDO Handling      |    BOOL |   BOOL  |     |
PDO_EXEC_USER_RECEIVE_CALLBACKS | PDO Handling | BOOL |   BOOL  |     |

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
MODULE_NODE_ID          | General           | GENERAL |   INT   |   1 | 127
USE_CANOE_EMU           | General           |    BOOL |   BOOL  |     |
INFINITE_LOOP           | General           |    BOOL |   BOOL  |     |
SIGNAL_STACK_EVENT      | General           |    BOOL |   BOOL  |     |
CHECK_CONSISTENCY       | General           |    BOOL |   BOOL  |     |
STATUS_LEDS_USED        | LED Indicator     |    BOOL |   BOOL  |     |
STARTUP_AUTONOMOUSLY    | Startup Behaviour |    BOOL |   BOOL  |     |
ENABLE_LAYER_MANAGEMENT | Layer Services    |    BOOL |   BOOL  |     |
ENABLE_LAYER_SERVICES   | Layer Services    |    BOOL |   BOOL  |     |
EXEC_LAYER_SERVICES_ALWAYS | Layer Services |    BOOL |   BOOL  |     |
ENABLE_LSS_MASTER       | Layer Services    |    BOOL |   BOOL  |     |
LSS_IMPLEMENTATION_LVL_2| Layer Services    |    BOOL |   BOOL  |     |
LSS_IMPLEMENTATION_LVL_3| Layer Services    |    BOOL |   BOOL  |     |
LSS_IMPLEMENTATION_LVL_4| Layer Services    |    BOOL |   BOOL  |     |
MODULE_BAUDRATE         | CAN Adaptation    | GENERAL |  TOKEN  |     |
QUEUED_MODE             | CAN Adaptation    |    BOOL |   BOOL  |     |
ID_FILTER               | CAN Adaptation    | GENERAL |   INT   |  0  |  8
MULTIPLE_RTR_USED       | CAN Adaptation    |    BOOL |   BOOL  |     |
_BIG_ENDIAN             | Target Adaptation |    BOOL |   BOOL  |     |
MACHINE_IS_16BIT_ONLY   | Target Adaptation |    BOOL |   BOOL  |     |
MemCpy                  | Target Adaptation | GENERAL |  TOKEN  |     |

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
HEARTBEAT_CONSUMER      | Error Control     |    BOOL |   BOOL  |     |
NUM_OF_HB_CONSUMERS     | Error Control     | GENERAL |    INT  |   1 | 127
SIGNAL_HEARTBEAT_EVENT  | Error Control     |    BOOL |   BOOL  |     |
ENABLE_NMT_ERROR_CTRL   | Error Control     | GENERAL |    INT  |   0 |   3
START_HEARTBEAT_TIME    | Error Control     | GENERAL |    INT  |  0  | 0xFFFF
REFRESH_ERRCTRL_TX_BUFFER | Error Control   |    BOOL |   BOOL  |     |
NMT_MASTER_ENABLE       | Master features   |    BOOL |   BOOL  |     |
NMT_GUARDING_ENABLE     | Master features   |    BOOL |   BOOL  |     |
NMT_NODES_TO_GUARD      | Master features   | GENERAL |    INT  |   1 | 127
SYNC_PRODUCER           | Master features   |    BOOL |   BOOL  |     |

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
CLIENT_ENABLE           | SDO Handling      |    BOOL |   BOOL  |     |
MULTI_SDO_SERVER        | SDO Handling      |    BOOL |   BOOL  |     |
SDO_SERVER2             | SDO Handling      | GENERAL |    INT  |   1 | 127
SDO_SERVER2_DEFAULT_ENABLE SDO Handling     |    BOOL |   BOOL  |     |
SDO_EXEC_USER_WRITE_CALLBACKS | SDO Handling|    BOOL |   BOOL  |     |
SDO_EXEC_USER_READ_CALLBACKS  | SDO Handling|    BOOL |   BOOL  |     |
SDO_WRITE_SEG_ALLOWED   | SDO Handling      |    BOOL |   BOOL  |     |
SDO_READ_SEG_ALLOWED    | SDO Handling      |    BOOL |   BOOL  |     |
SDO_BLOCK_ALLOWED       | SDO Handling      |    BOOL |   BOOL  |     |
SDO_BLOCK_USE_CRC       | SDO Handling      |    BOOL |   BOOL  |     |
CRC_LOOKUP_TABLE        | SDO Handling      |    BOOL |   BOOL  |     |
SDO_MAX_BLK_SIZE        | SDO Handling      | GENERAL |    INT  |   1 | 127
SDO_PST_VALUE           | SDO Handling      | GENERAL |    INT  |   0 | 0xFFFF
SDO_LOCAL_WRITE         | SDO Handling      |    BOOL |   BOOL  |     |
SDO_LOCAL_READ          | SDO Handling      |    BOOL |   BOOL  |     |
DELAYED_SDO_ENABLE      | SDO Handling      |    BOOL |   BOOL  |     |

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
DEVICE_TYPE_VAL         | Object Dictionary | GENERAL |    INT  |   0x0 | 0xFFFFFFFFUL
VENDOR_ID_VAL           | Object Dictionary | GENERAL |    INT  |   0x0 | 0xFFFFFFFFUL
PRODUCT_CODE_VAL        | Object Dictionary | GENERAL |    INT  |   0x0 | 0xFFFFFFFFUL
REVISION_NUM_VAL        | Object Dictionary | GENERAL |    INT  |   0x0 | 0xFFFFFFFFUL
SERIAL_NUM_VAL          | Object Dictionary | GENERAL |    INT  |   0x0 | 0xFFFFFFFFUL
PRODUCT_NAME            | Object Dictionary | GENERAL |  STRING |       |
HW_VERSION              | Object Dictionary | GENERAL |  STRING |       |
SW_VERSION              | Object Dictionary | GENERAL |  STRING |       |
OBJECT_1029_USED        | Object Dictionary |    BOOL |    BOOL |       |

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
SIGNAL_BOOTUPMSG        | Event Signalling  |    BOOL |   BOOL  |     |
SIGNAL_GUARDFAILURE     | Event Signalling  |    BOOL |   BOOL  |     |
ON_COMMERROR_GO_PREOPERATIONAL | Error Control | BOOL |   BOOL  |     |
SIGNAL_EMCYMSG          | Event Signalling  |    BOOL |   BOOL  |     |
SIGNAL_STATE_CHANGE     | Event Signalling  |    BOOL |   BOOL  |     |
ENABLE_EMCYMSG          | EMCY Handling     |    BOOL |   BOOL  |     |
TIME_STAMP_USED         | Time Stamp        |    BOOL |   BOOL  |     |
EXEC_USER_TIME_STAMP_CALLBACKS | Time Stamp |    BOOL |   BOOL  |     |
SYNC_USED               | SYNC Handling     |    BOOL |   BOOL  |     |
SIGNAL_SYNC             | SYNC Handling     |    BOOL |   BOOL  |     |
EXAMPLE_OBJECTS_USED    | Object Dictionary |    BOOL |   BOOL  |     |
EXAMPLE_PROFILE_USED    | Object Dictionary |    BOOL |   BOOL  |     |
HANDLE_UNKNOWN_OBJ_IN_APPL | Object Dictionary | BOOL |   BOOL  |     |
STORE_PARAMETERS_SUPP   | Non-volatile storage | BOOL |   BOOL  |     |
NV_SAVE_COMPAR          | Non-volatile storage | BOOL |   BOOL  |     |
NV_SAVE_APPPAR          | Non-volatile storage | BOOL |   BOOL  |     |
NV_SAVE_MANPAR          | Non-volatile storage | BOOL |   BOOL  |     |
EMULATE_ON_DISK         | Non-volatile storage | BOOL |   BOOL  |     |
EMULATE_FLASH           | Non-volatile storage | BOOL |   BOOL  |     |
NV_BLOCK_MODE           | Non-volatile storage | BOOL |   BOOL  |     |
SCOFLD_SUPPORT          | Non-volatile storage | BOOL |   BOOL  |     |

Macro Name              | Macro Group       |         | type    | limits
-------------------------------------------------------------------------------
GENERIC_PROFILE_USED    | Profiles          | BOOL    |   BOOL  |     |
PROFILE_PDO_TX_HANDLING | Profiles          | BOOL    |   BOOL  |     |
PROFILE_DS401           | Profiles          | BOOL    |   BOOL  |     |
OBJECT_6422_USED        | Profiles          | BOOL    |   BOOL  |     |
OUTPUT_BUFFER_LEN_DO    | Profiles          | GENERAL |   INT   |     |
OUTPUT_BUFFER_LEN_AO    | Profiles          | GENERAL |   INT   |     |
INPUT_BUFFER_LEN_DI     | Profiles          | GENERAL |   INT   |     |
INPUT_BUFFER_LEN_AI     | Profiles          | GENERAL |   INT   |     |
DS401_TEST              | Profiles          | BOOL    |   BOOL  |     |
CONF_GENERATION         | Target Adaptation | BOOL    |   BOOL  |     |

END CONFIGURATOR CONTROL UNIT
*/

#ifndef _COS_MAIN_H_
#define _COS_MAIN_H_

#include "portab.h"
/*--------------------------------------------------------------------*/
/*  basic definitions (do not change)                                 */
/*--------------------------------------------------------------------*/
#ifndef TRUE
# define TRUE       1 /*!< It's true in boolean operations. */
#endif

#ifndef FALSE
# define FALSE      0 /*!< It's false in boolean operations. */
#endif

#define ON          1 /*!< Use it to set flags. */
#define OFF         0 /*!< Use it to reset flags. */

#define OK          1 /*!< Return value for success in binary functions. */
#define FAULT       0 /*!< Return value for failure in binary functions. */

#define NO_ERR      0 /*!< Return value for success. */
#define BUSY_ERR    1 /*!< Return value if resource temporary not available. */
#define WAIT_ERR    2 /*!< Return value for service delayed. */
#define RANGE_ERR   3 /*!< Return value for out or range. */
#define VALUE_TOO_HIGH_ERR 100 /*!< Return value for parameter to high. */
#define ID_BUSY     200 /*!< Return value for allready used identifier. */

#define MAP_EXCEED     0x01 /*!< signal received PDO smaller than mapping */
#define PDO_EXCEED     0x02 /*!< signal received PDO larger than mapping */
#define ERR_CTRL_FAIL  0x04 /*!< error control failed */
#define MSG_OVERRUN    0x08 /*!< CAN message overrun */
#define COB_COLLISION  0x10 /*!< COB-ID collision */

#define PDO_INVALID  0x80 /*!< PDO invalidation bit. */
#define PDO_NO_RTR   0x40 /*!< PDO RTR functionality bit. */
#define PDO_EXTENDED 0x20 /*!< PDO extended identifier bit. */

#define RX_DIR 1 /*!< Direction: receive */
#define TX_DIR 0 /*!< Direction: transmit */

#define FIRST_HB    2   /*!< Wait for first heartbeat. */

/*!
  \defgroup eventcodes Event codes for callback
  @{
*/
#define STACK_STATE 1 /*!< stack state event */
#define COMM_STATE  2 /*!< communication state event */
#define HB_EVT      3 /*!< heartbeat event */
#define GD_EVT      4 /*!< guarding event */
#define TIME_STP    5 /*!< time stamp event */
#define CAN_INFO    6 /*!< CAN information event (\ref canevtcodes) */
#define MM_GD_EVT   7 /*!< NMT master guarding event */
/*!
  end of group eventcodes
  @}
*/

/*!
  \defgroup statecodes States of CANopen stack's statemachine
  @{
*/

/*!
  \addtogroup detailledeventcodes detailled event codes for gSlvStackEventCallback()
  @{
*/

/*!
  Initial state

  This is the initial state after RESET.
  The function ModulInit() is called
*/
#define LOCAL_INIT       0
/*!
  Preparing state

  The CAN is initialized and the a configuration is loaded and
  applied.
*/
#define PREPARE_STARTUP  1
/*!
  Wait for transmission of first message

  Here we poll for the first message to be transmitted.
  This first message is the bootup message.
*/
#define WAIT_FOR_STARTUP 2
/*!
  Communication functionality enabled.

  Only in this state it is possible to communicate according to CANopen.
*/
#define STACK_ENABLED    3
/*!
  The bootup message could not be transmitted.
*/
#define STACK_STOPPED    4
/*!
  Sleep mode reached.

  If the CAN controller supports a sleep mode the stack stays in this mode.
*/
#define STACK_FREEZED    5
/*!
  Wake up from sleep.
*/
#define STACK_WAKEUP     6
/*!
  Prepare the LSS procedure
*/
#define PREPARE_LSS      7
/*!
  LSS is actually running.
*/
#define EXECUTE_LSS      8
/*!
  New bitttime is adjusted during execution of LSS.
*/
#define STACK_DEFERRED   9

/*!
  @} end of group detailledeventcodes
*/

/*!
  @} end of group statecodes
*/

/*
  Default ID base values as defined in the CANopen specification.
 */

#define SYNC_ID            0x80    /*!< COB ID SYNC message */
#define NMT_ZERO_ID        0x00    /*!< COB ID 0 NMT service */
#define TIME_ID            0x100   /*!< COB ID time stamp */

#define SDORX_ID           0x600   /*!< SDO server side receive/client side transmit */
#define SDOTX_ID           0x580   /*!< SDO server side transmit/client side receive */

#define EMCY_ID            0x080   /*!< EMCY message */
#define ERRCTRL_ID         0x700   /*!< error control services */
#define CAN_MAX_STD_ID     0x7FF

/*! Maximum number of mapped objects for a PDO -> 8 (do not change). */
#define MAX_MAPPED_OBJECTS (8 + 1)

/*! Maximum number of mapped bits for a PDO -> 64 (do not change). */
#define MAX_MAPPED_BITS    64

/*!
  \defgroup nmtstatecodes NMT state codes
  @{
*/
#define BOOTUP               0 /*!< Signal initial bootup. */
#define STOPPED              4 /*!< Signal stopped state. */
#define OPERATIONAL          5 /*!< Signal operational state. */
#define PRE_OPERATIONAL    127 /*!< Signal pre-operational state. */
/*!
  @} end of group nmtstatecodes
*/

/*!
  \defgroup nmtcommands NMT command codes
  @{
*/
#define NMT_CS_START_NODE    1 /*!< NMT start remote node        */
#define NMT_CS_STOP_NODE     2 /*!< NMT stop remote node         */
#define NMT_CS_ENTER_PREOP 128 /*!< NMT enter preoperation state */
#define NMT_CS_RESET_NODE  129 /*!< NMT reset node               */
#define NMT_CS_RESET_COM   130 /*!< NMT reset communication      */
/*!
  @} end of group nmtcommands
*/

/*!
  \defgroup canevtcodes event codes for CAN
  @{
*/
#define CAN_OVERRUN 1 /*!< CAN overrun detected */
#define CAN_WARNING 2 /*!< CAN warning limit reached */
#define CAN_ACTIVE  3 /*!< CAN error active state reached */
#define BUS_OFF     4 /*!< CAN bus off reached */
/*!
  @} end of group canevtcodes
*/

/*!
  \defgroup errorclasscodes values for error class entries in object 1029h
  @{
*/
#define ERR_PREOPERATIONAL 0 /*!< on error go PRE-OPERATIONAL */
#define ERR_NO_CHANGE      1 /*!< no state change on error    */
#define ERR_STOPPED        2 /*!< on error go STOPPED         */
/*!
  @} end of group errorclasscodes
*/


/*--------------------------------------------------------------------*/
/*  basic definitions (may be changeable by the user)                 */
/*--------------------------------------------------------------------*/

/*!
  \defgroup profiledef1 Profile related define statements (user adaptable)
  @{
*/

/*!
  \def GENERIC_PROFILE_USED
  Switch for enabling generic profile support.

  With this switch the generic profile support is enabled. The extended
  functionality is implemented by additional functions called via following
  defines (set in <profil.h>):
  - #PROFILE_INIT
  - #PROFILE_TASK_PREOPERATIONAL
  - #PROFILE_TASK_OPERATIONAL
  - #PROFILE_PDO_ANALYZE
  - #PROFILE_PDO_CREATE
  - #PROFILE_SDO_DOWN
  - #PROFILE_SDO_UP
  - #PROFILE_PDO_TX (only if PROFILE_PDO_TX_HANDLING == 1)
  - #PROFILE_CHECK_ERRORS (only if OBJECT_1029_USED == 1)

  Further on a function ProcessPreoperational() is available
  (see module <usrclbck.c>).

  - 0  Disable Generic profile functionality.
  - 1  Enable generic profile functionality.
*/
#define GENERIC_PROFILE_USED 0

/*!
  \def PROFILE_PDO_TX_HANDLING
  Switch for enabling additional object handling when sent by PDO.

  With this switch further treatment of objects just sent by PDO is configurable.
  The function set by #PROFILE_PDO_TX (see <profil.h>) is called for every
  object mapped into the corresponding TPDO.

  - 0  Disable additional PDO tx object handling.
  - 1  Enable additional PDO tx object handling .
*/
#define PROFILE_PDO_TX_HANDLING 0

/*!
  \def PROFILE_DS401
  Switch for enabling DS401 I/O profile.

  With this switch the I/O functionality is activated.
  Associated settings are the input and output buffer length (see below) as
  well as further settings in the DS401 module.

  - 0  I/O profile DS401 not used.
  - 1  Include DS401 I/O profile functionality.

  \note To activate DS-401 functionality #GENERIC_PROFILE_USED has to be set
        additionally!
*/
#define PROFILE_DS401 0

/*!
  \def OBJECT_6422_USED
  Switch for enabling DS401 I/O profile object 6422h.

  With this switch the object 6422h is activated.
  - 0  Object 6422h not used.
  - 1  Include object 6422h in profile.

  \remarks Object 6422h only available when DS401 I/O profile is implemented!

  \note If this object is set up with the attribute "mappable" (#ROMAP, see
        <objentry.h>), the define #PROFILE_PDO_TX_HANDLING has to be set in
        order to reset the corresponding bits when transmitted by PDO
        (see CiA DS-401).
*/
#define OBJECT_6422_USED 0

/*!
  \def OUTPUT_BUFFER_LEN_DO
  Digital Output buffer size for DS-401 I/O profile (user adaptable).

  This value describes the number of 8-bit objects which have to be written to
  the process environment.
  The value directly influences the buffer #abOutDataDig.
  The values from this buffer are transferred via RPDO.
  According to the value of #RPDO_MAP_READONLY the location of data items
  transferred via RPDO is fixed or variable.
  If we use a fixed mapping every RPDO is assigned an area of exactly
  8 bytes in this buffer.
*/
#define OUTPUT_BUFFER_LEN_DO 16 /* number of digital (8-bit) output channels */

/*!
  \def OUTPUT_BUFFER_LEN_AO
  Analogue Output buffer size for DS-401 I/O profile (user adaptable).

  This value describes the number of 16-bit objects which have to be written to
  the process environment.
  The value directly influences the buffer #awOutDataAna.
  The values from this buffer are transferred via RPDO.
  According to the value of #RPDO_MAP_READONLY the location of data items
  transferred via RPDO is fixed or variable.
  If we use a fixed mapping every RPDO is assigned an area of exactly
  8 bytes in this buffer.
*/
#define OUTPUT_BUFFER_LEN_AO 4  /* number of analogue (16-bit) output channels */

/*!
  \def INPUT_BUFFER_LEN_DI
  Digital Input buffer size for DS-401 I/O profile (user adaptable).

  This value describes the number of 8-bit objects which have to be read from
  the process environment.
  The value directly influences the buffer #abInDataDig.
  The values from this buffer are transferred via TPDO.
  According to the value of #TPDO_MAP_READONLY the location of data items
  transferred via TPDO is fixed or variable.
  If we use a fixed mapping every TPDO is assigned an area of exactly
  8 bytes in this buffer.
*/
#define INPUT_BUFFER_LEN_DI 16 /* number of digital (8-bit) input channnels */

/*!
  \def INPUT_BUFFER_LEN_AI
  Analogue Input buffer size for DS-401 I/O profile (user adaptable).

  This value describes the number of 16-bit objects which have to be read from
  the process environment.
  The value directly influences the buffer #awInDataAna.
  The values from this buffer are transferred via TPDO.
  According to the value of #TPDO_MAP_READONLY the location of data items
  transferred via TPDO is fixed or variable.
  If we use a fixed mapping every TPDO is assigned an area of exactly
  8 bytes in this buffer.
*/
#define INPUT_BUFFER_LEN_AI 16 /* number of analogue (16-bit) input channels */

/* only for test purposes */
#define DS401_TEST 0

/*!
  @} end of group profiledef1
*/

/*!
  \defgroup pdodef1 PDO related define statements (user adaptable)
  @{
*/

/*!
  \def MAX_RX_PDOS
  Number of receive PDOs (user adaptable)

  The following values are allowed:
  - 0 - There are no receive PDOs available
  - 1 - 200 - Number of receive PDOs
*/
#define MAX_RX_PDOS 1

/*!
  \def MAX_TX_PDOS
  Number of transmit PDOs (user adaptable)

  The following values are allowed:
  - 0 - There are no transmit PDOs available
  - 1 - 200 - Number of transmit PDOs
*/
#define MAX_TX_PDOS 6

/*!
  \def MAX_RX_RMTPDOS
  Number of remote receive PDOs (user adaptable)

  \note This is only adjustable for the target T89C51CC01.

  The following values are allowed:
  - 0 - There are no remote receive PDOs available
  - 1 - 4 - Number of remote receive PDOs
*/
#define MAX_RX_RMTPDOS 0

/*!
  \def MAX_TX_RMTPDOS
  Number of remote transmit PDOs (user adaptable)

  \note This is only adjustable for the target T89C51CC01.

  The following values are allowed:
  - 0 - There are no remote transmit PDOs available
  - 1 - 4 - Number of remote transmit PDOs
*/
#define MAX_TX_RMTPDOS 0

/*!
  \def RPDO_PAR_READONLY
  Access restrictions for receive PDO communication parameters.

  The receive PDO parameters can have a fixed value in the object dictionary.
  This means that it is not possible to change their values via the
  object dictionary (entries #RECEIVE_PDO_1_COMMUNI_PAR).
  They are dependent on the module id only.

  The following values are allowed:
  - 1 receive PDO communication parameters are read only
  - 0 receive PDO communication parameters are read/write values
*/
#define RPDO_PAR_READONLY 1

/*!
  \def RPDO_MAP_READONLY
  Access restrictions for receive PDO mapping.

  The receive PDO mapping can have a fixed value in the object dictionary.
  This means that it is not possible to change the mapping during runtime
  via the object dictionary (entries #RECEIVE_PDO_1_MAPPING_PAR).
  The mapping is then dependent on the configuration in the file
  mapping.c.

  The following values are allowed:
  - 1 receive PDO mappings are read only
  - 0 receive PDO mappings are read/write values
*/
#define RPDO_MAP_READONLY 1

/*!
  \def RPDO_DIRECT_COPY
  Determine copy routine for RPDO data.

  The following values are allowed:
  - 1 The mapping (the data type) is not evaluated and the data from the
    RPDO are simply copied to the buffer area #OUTPUT_DATA().
    The bytes will be intel ordered in the buffer.
  - 0 The mapping is considered and according to the data type the data items
    copied.

  \remarks This define is only used if #RPDO_MAP_READONLY = 1.
*/
#define RPDO_DIRECT_COPY 0

/*!
  \def TPDO_PAR_READONLY
  Access restrictions for transmit PDO communication parameters.

  The transmit PDO parameters can have a fixed value in the object dictionary.
  This means that it is not possible to change their values via the
  object dictionary (entries #TRANSMIT_PDO_1_COMMUNI_PAR).
  They are dependent on the module id only.

  The following values are allowed:
  - 1 transmit PDO communication parameters are read only
  - 0 transmit PDO communication parameters are read/write values
*/
#define TPDO_PAR_READONLY 1

/*!
  \def TPDO_MAP_READONLY
  Access restrictions for transmit PDO mapping.

  The transmit PDO mapping can have a fixed value in the object dictionary.
  This means that it is not possible to change the mapping during runtime
  via the object dictionary (entries #TRANSMIT_PDO_1_MAPPING_PAR).
  The mapping is then dependent on the configuration in the file
  mapping.c.

  The following values are allowed:
  - 1 transmit PDO mappings are read only
  - 0 transmit PDO mappings are read/write values
*/
#define TPDO_MAP_READONLY 1

/*!
  \def TPDO_DIRECT_COPY
  Determine copy routine for TPDO data.

  The following values are allowed:
  - 1 The mapping (the data type) is not evaluated and the data from the
    buffer area #abInData are simply copied to the TPDO.
    The bytes are expected to be be intel ordered in the buffer.
  - 0 The mapping is considered and according to the data type the data items
    copied.

  \remarks This define is only used if #TPDO_MAP_READONLY = 1.
*/
#define TPDO_DIRECT_COPY 0

/*!
  \def USE_DUMMY_MAPPING
  Enable dummy mapping for RPDO.

  The dummy mapping feature can be switched off.
  - 0 Do not use dummy mapping.
  - 1 Enable dummy mapping.
 */
#define USE_DUMMY_MAPPING 1

/*!
  \def PDO_EXEC_USER_RECEIVE_CALLBACKS
  Determine if a user callback has to be executed on PDO reception.

  The Stack can execute the user callback gSlvRxPdoCallback() from <usrclbck.c>
  if a PDO is received and has been processed.

  - 0 Do not execute the user callback.
  - 1 Execute the user callback.
 */
#define PDO_EXEC_USER_RECEIVE_CALLBACKS 1

/*!
  \def OBJECTS_64BIT_USED
  Enable support for 64 bit objects mappable in PDOs.

  The stack handles 64 bit objects in receive and transmit PDOs.

  - 0 No 64 bit object support.
  - 1 Enable 64 bit object support.
 */
#define OBJECTS_64BIT_USED 1

/*!
  \def ADD_NODE_ID_TO_PDO
  Create PDO identifier according to predefined connection set (base ID + node ID).

  The entries in <rpdocomm.h> and <tpdocomm.h> represent the default communication
  parameters for the PDOs. Setting this define to '1' the configured ID values
  represent the base ID where the actual node ID of the CANopen device is added
  implicitly.
  If set to '0' the configured values represent the absolute CAN identifier of
  the individual PDOs.

  - 0 No offset added to configured CAN identifiers.
  - 1 Current node ID is added to configured CAN identifiers.
 */
#define ADD_NODE_ID_TO_PDO 0

/*!
  @} end of group pdodef1
*/

/*!
  \defgroup general1 General define statements (user adaptable)
  @{
*/
/*!
  \def MODULE_NODE_ID
  Determine the initial device NodeID.

  Every CANopen device needs a unique node id to be distinguished inside the
  network. The device will bootup first with this id (range 1..127) if the
  function ReadBoardAdr() - see module <usrclbck.c> - does not provide any
  different valid node id.
*/
#define MODULE_NODE_ID 0x76

/*!
  \def USE_CANOE_EMU
  Determine if the source code should be executed via CANoe emulation.

  If the source code should be used via CANoe emulation by osCAN,
  hardware accesses (e.g. LED's) are redirected through environment
  variables within CANoe. Also the user callbacks and so on are
  redirected by the same way.

  - 0 hardware accesses are not redirected trough environment variables.
  - 1 redirection is enabled.
*/
#define USE_CANOE_EMU 0

/*!
  \def INFINITE_LOOP
  Determine calling conventions for stack.

  We use a routine named main() as an entry point.
  From there we loop for ever.
  The following values are allowed for this define.

  - 0 Use the single functions of the stack from within of your application.
  - 1 The function main() is used.
*/
#define INFINITE_LOOP 0

/*!
  \def STARTUP_AUTONOMOUSLY
  Go to operational autonomously.

  In some networks it is required to go to OPERATIONAL autonomously.
  This switch enables the object 1F80 for the code.
  In this object the bits 2 and 3 are set (hardcoded).

  - 0 Behave according to specification.
  - 1 Go to OPERATIONAL automatically.
 */
#ifdef SURGE_TEST

    #define STARTUP_AUTONOMOUSLY 1
#else
    #define STARTUP_AUTONOMOUSLY 0
#endif

/*!
  \def SIGNAL_STACK_EVENT
  Create user callback.

  The function gSlvStackEventCallback() in <usrclbck.c> is the central point for
  adding functionality for user callback functions.

  - 0 user call back is not compiled
  - 1 user callback is compiled
*/
#define SIGNAL_STACK_EVENT 1

/*!
  \def CHECK_CONSISTENCY
  Enable run-time consistency checks.

  The ordering of the object dictionary entries and the
  consistency of the mapping tables is checked during bootup.

  - 0 do not use consistency checks during runtime (for tested version)
  - 1 use consistency check (debug option)
*/
#define CHECK_CONSISTENCY 1
/*!
  @} end of group general1
*/

/*!
  \def STATUS_LEDS_USED
  Enable CANopen LED signaling.

  This switch enables the CANopen LED signaling according to CiA DR-303-3
  Indicator Specification.
  This functionality requires the add on module <co_led.c>.

  - 0 Enable  LED status signaling.
  - 1 Disable LED status signaling.
*/
#define STATUS_LEDS_USED 0

/*!
  \def ENABLE_LAYER_MANAGEMENT
  Enable layer management.

  Layer management protocol is responsible for aquiring the node id and
  baud rate for the module by means of CAN messages.
  If you do not need LMT then better use LSS with the switch
  #ENABLE_LAYER_SERVICES below.
  LSS is an over-worked version of LMT and more powerful.

  LMT do not work without LSS module. Therefore  #ENABLE_LAYER_SERVICES
  and #ENABLE_LAYER_MANAGEMENT have to be set if LMT is required.

  The values 0 and 1 are allowed for this define.
  - 1 Use layer management.
*/
#define ENABLE_LAYER_MANAGEMENT 0

/*!
  \def ENABLE_LAYER_SERVICES
  Enable layer setting services.

  Layer setting services are responsible for aquiring the node id and
  baud rate for the module by means of CAN messages.

  The values 0 and 1 are allowed for this define.
  - 0 Use no layer setting service. The node id is provided via ReadBoardAdr().
  - 1 Use a layer setting service.
*/
#define ENABLE_LAYER_SERVICES 0

/*!
  \def EXEC_LAYER_SERVICES_ALWAYS
  Determine if the layer services have to be executed in any state of NMT state machine.

  There are two start up scenarios if layer services are used.
  - The stack starts up with a default node id (ReadBoardAdr()).
    If node id == LSS_INVALID_NODE_ID LSS init state will be reached
    to configure a valid node id.
    After configuration of a valid node id the node runs to
    preoperational state and LSS can't be executed again.
  - The stack starts with the layer services.
    LSS can be executed at any time.

  The values 0 and 1 are allowed for this define.
  - 0 run LSS if node id is invalid.
  - 1 run LSS always.
*/
#define EXEC_LAYER_SERVICES_ALWAYS 0

/*!
  \def ENABLE_LSS_MASTER
  Determine if the node works as LSS master or LSS slave.

  The values 0 and 1 are allowed for this define.
  - 0 node works as LSS slave.
  - 1 node works as LSS master.
*/
#define ENABLE_LSS_MASTER 0


/*!
  \def LSS_IMPLEMENTATION_LVL_2
  LSS implementation level 2

  Enable the services
    - configure bit timing parameters
    - activate bit timing parameters

  The values 0 and 1 are allowed for this define.
  - 0 LSS implementation level 2 disbled.
  - 1 LSS implementation level 2 enabled.
*/
#define LSS_IMPLEMENTATION_LVL_2 1

/*!
  \def LSS_IMPLEMENTATION_LVL_3
  LSS implementation level 3

  Enable the services
    - switch mode selective

  The values 0 and 1 are allowed for this define.
  - 0 LSS implementation level 3 disbled.
  - 1 LSS implementation level 3 enabled.
*/
#define LSS_IMPLEMENTATION_LVL_3 1

/*!
  \def LSS_IMPLEMENTATION_LVL_4
  LSS implementation level 4

  Enable the services
    - store configured parameters
    - inquiry services
    - identification services

  The values 0 and 1 are allowed for this define.
  - 0 LSS implementation level 4 disbled.
  - 1 LSS implementation level 4 enabled.
*/
#define LSS_IMPLEMENTATION_LVL_4 1

/*!
  \def ACP_USED
  This define enables the use of the specific ACP solution.

  Be sure that the ACP module is included in the project
  if you enable that feature
  - 0 ACP disabled (default)
  - 1 ACP enabled
*/
#define ACP_USED 0

/*!
  \defgroup canusage1 CAN/buffer management related define statements (user adaptable)
  @{
*/

/*!
  \def MODULE_BAUDRATE
  Determine the initial baudrate of the device.

  The device will bootup with this baudrate (BR_10K..BR_1M; see <cancntrl.h>) if the
  function ReadBaudRate() - see module <usrclbck.c> - does not provide any different
  valid baudrate.
*/
#define MODULE_BAUDRATE BR_500K

/*! Number of CAN channels. We support only one channel at the moment. */
#define NUM_CANCHANNELS 1

/*!
  \def QUEUED_MODE
  Select buffer management.

  Choose between queued and nonqueued protocol stack.
  - 0 Use buffers for message storing.
  - 1 Use queues for message storing.
*/
#define QUEUED_MODE 0

/*!
  \def FULLCAN_BUFFER_MODE
  Select full can buffer mode

  Choose between FullCAN mode or software buffer mode.
  - 0 Use software buffers for message storing.
  - 1 Use FullCAN buffers (hardware) for message storing.
*/

#if QUEUED_MODE == 1
/* Zero, because it is not possible to use the FullCAN mode with queues. */
# define FULLCAN_BUFFER_MODE 0
#else
# define FULLCAN_BUFFER_MODE 1
#endif /* QUEUED_MODE == 1 */

/*!
  \def ID_FILTER
  Use an identifier filter to select messages.

  Possible values:
  - 0 -> switch filter off
  - 1 -> support for 1 queue or buffer
  - 2 -> support for 3 queues or buffers
  - 4 -> support for 15 queues or buffers
  - 8 -> support for 255 queues or buffers
*/
#define ID_FILTER 1

/*!
  \def MULTIPLE_RTR_USED
  Support multiple remote requests.

  To use remote requests for transmit PDOs we need to know,
  if the hardware has support for receiving multiple remote requests.
  This affects the usage of a full CAN controller in buffer mode.
  The single remote request for node guarding is not affected
  by this definition.
  Set this definition to
  - 0 - disable support for multiple remote requests
  - 1 - enable support for multiple remote requests
*/
#define MULTIPLE_RTR_USED 1

/*!
  @} end of group canusage1
*/

/*!
  \defgroup nvstorage non-volatile storage (user adaptable)
  @{
*/

/*!
  \def STORE_PARAMETERS_SUPP
  Enable parameter storage support.

  The stored parameters in nonvolatile memory has always priority.
  So if node id changed it must be stored before execute the switch mode operation service.

  CANopen supports parameter storage through the entries 1010/1011 in the
  object dictionary.
  Refer to the section \ref nonvolpage for more details.
  With this define statement the storage handling entries are available:
  - 0 - disable storage handling
  - 1 - enable storage handling
*/
#define STORE_PARAMETERS_SUPP 0

/*!
  \def STORE_ON_COMMAND
  The store/restore commands are used for storage.

  On read access to the 1010 bit 0 is set to 1.
  If a sub-index is written a storage block is required for evaluation.
  You need the file <nonvolst.c> if this define is set.
  Refer to the section \ref nonvolpage for more details.
  With this define statement the storage command evaluation is enabled:
  - 0 - disable storage command
  - 1 - enable storage command
*/
#define STORE_ON_COMMAND 1

/*!
  \def STORE_AUTOMATICALLY
  The store/restore commands are used for storage.

  On read access to the 1010 bit 1 is set to 1.
  Refer to the section \ref nonvolpage for more details.
  With this define statement only the correct return value is enabled:
  - 0 - do not signal automatic storage
  - 1 - signal automatic storage
*/
#define STORE_AUTOMATICALLY 0

/*!
  \def NV_SAVE_COMPAR
  Enable sub-index 2 for storage objects
  (save/restore communication parameters).

  The following values are allowed:
  - 0 - disable sub-index 2 of objects 1010/1011
  - 1 - enable sub-index 2 of objects 1010/1011
*/
#define NV_SAVE_COMPAR 1

/*!
  \def NV_SAVE_APPPAR
  Enable sub-index 3 for storage objects (save/restore application parameters).
  This object takes care of the device profile relevant data during the
  storage process.

  The following values are allowed:
  - 0 - disable sub-index 3 of objects 1010/1011
  - 1 - enable sub-index 3 of objects 1010/1011
*/
#define NV_SAVE_APPPAR 1

/*!
  \def NV_SAVE_MANPAR
  Enable sub-index 4 for storage objects (save/restore manufacturer
  defined parameters).

  The following values are allowed:
  - 0 - disable sub-index 4 of objects 1010/1011
  - 1 - enable sub-index 4 of objects 1010/1011
*/
#define NV_SAVE_MANPAR 1

/*!
  \def EMULATE_ON_DISK
  Storage functionality is emulated with a file system.

  CANopen supports parameter storage through the entries 1010/1011 in the
  object dictionary.
  With this define statement the storage handling supported using a file
  system:
  - 0 - no disk emulation
  - 1 - disk emulation
*/
#define EMULATE_ON_DISK 0

/*!
  \def EMULATE_FLASH
  Storage functionality is not realised.

  CANopen supports parameter storage through the entries 1010/1011 in the
  object dictionary.
  With this define statement the storage handling can be compiled in
  but no storage is executed.
  - 0 - no emulation
  - 1 - stub functions compiled
*/
#define EMULATE_FLASH 0

/*!
  \def NV_BLOCK_MODE
  Store a nonvolatile parameters group as one block or in portions
  to 8/16/32 bits.

  The following values are allowed:
  - 0 - disable block mode
  - 1 - enable block mode
*/
#define NV_BLOCK_MODE 0

/*!
  \def SCOFLD_SUPPORT
  Enable support for the Small CANopen Flash Loader.

  The following values are allowed:
  - 0 - disable flash loader support
  - 1 - enable flash loader support
*/
#define SCOFLD_SUPPORT 0

/*!
  @} end of group nvstorage
*/



/*!
  \defgroup errctrl1 Error control related define statements (user adaptable)
  @{
*/

/*!
  \def HEARTBEAT_CONSUMER
  Heartbeat consumer support.

  The heartbeat consumer feature is selectable with this define.
  - 0 - disable heartbeat consumer
  - 1 - enable heartbeat consumer
*/
#define HEARTBEAT_CONSUMER 0

/*!
  \def NUM_OF_HB_CONSUMERS
  Number of entries in heartbeat consumer table.

  The value range is 1 .. 127.
  \remarks #HEARTBEAT_CONSUMER must be set to 1.
*/
#define NUM_OF_HB_CONSUMERS 2

/*!
  \def SIGNAL_HEARTBEAT_EVENT
  Signal heartbeat consmuer event

  The consumer heartbeat failure event is signalled to the application via the
  callback function gSlvStackEventCallback() from <usrclbck.c>.
 - 1 Use the callback.
 - 0 Do not signal consumer heartbeat failure event.

  \sa #HEARTBEAT_CONSUMER, #SIGNAL_HEARTBEAT_EVENT, #SIGNAL_STACK_EVENT
 */
#define SIGNAL_HEARTBEAT_EVENT 2

/*!
  \def ENABLE_NMT_ERROR_CTRL
  Select which kind of NMT error control is used by the protocol stack.

  Choose the NMT error control behaviour (user adaptable).
  - 0 No protocol activated.
  - 1 This slave uses the node guarding protocol (old-fashioned).
  - 2 This slave uses the heartbeat protocol (new-fashioned).
  - 3 This slave is able to use both protocols (self-adapting).

  \note
  CANopen Standard: The implementation of either guarding or
  heartbeat is mandatory.

  \note
  For the heartbeat producer it is recommended to set the
  initial heartbeat producer time #START_HEARTBEAT_TIME.
*/
#define ENABLE_NMT_ERROR_CTRL 3

/*!
  \def START_HEARTBEAT_TIME
  Select if heartbeat should be started initially with given value.

  If this value is set to zero the Heartbeat Producer Time has to be
  configured via SDO and the Heartbeat Protocol will not start immediately
  at bootup.

  This is the initial heartbeat producer time in ms (user adaptable).
*/
#define START_HEARTBEAT_TIME 100

/*!
  \def REFRESH_ERRCTRL_TX_BUFFER
  Refresh the error control TX buffer of the CAN controller.

  The function gCan_PrepareRemoteMessage() from can driver module is called in
  function CanCom() from <cos_main.c> every time a NMT command was received.

  This feature is only useful for CAN controller which answers remote frames
  automatically with the data which is stored at this moment in the buffer.
  Such a controller for example is the TouCAN which is used in the Motorola's
  MPC555 or 63376.
  If #REFRESH_ERRCTRL_TX_BUFFER 0 then the buffer is not updated if the node
  has a NMT command received. This results in an old communication state
  answer to a following remote guard request. Only the first guard request is
  answered with the false state, because of the refresh in the remote frame
  interrupt routine.

 - 1 Use the refresh of the buffer.
 - 0 Error control buffer will not be refreshed.
 */
#define REFRESH_ERRCTRL_TX_BUFFER 0

/*!
  \def OBJECT_1029_USED
  Support for configurable error behaviour.

  The implementation of object 1029h (Error Behaviour) is selectable with
  this define.
  - 0 - object 1029h disabled
  - 1 - object 1029h enabled
*/
#define OBJECT_1029_USED 0

/*!
  @} end of group errctrl1
*/

/*!
  \defgroup nmtmaster1 NMT master related define statements (user adaptable)
  @{
*/

/*!
  \def NMT_MASTER_ENABLE
  Enable master functionality.

  The following values are selectable (user adaptable).
  - 0 - master functionality disabled.
  - 1 - master functionality enabled.
*/
#define NMT_MASTER_ENABLE 0

/*!
  \def NMT_GUARDING_ENABLE
  Enable node guarding capability.

  The NMT master is able to guard selected nodes.
  This feature is only available
  if the master functionality (#NMT_MASTER_ENABLE) is enabled.
  The following values are selectable (user adaptable).
  - 0 - guarding functionality disabled.
  - 1 - guarding functionality enabled.
*/
#define NMT_GUARDING_ENABLE 0

/*! This value defines the number of nodes to guard (1..127). (user adaptable) */
/*!
  \def NMT_NODES_TO_GUARD
  Number of nodes in guarding table.

  The NMT master is able to guard selected nodes.
  The number of nodes which can be guarded simultaneously is limited
  by the size of an internal table (1..127).
  The table is only available if
  - the master functionality (#NMT_MASTER_ENABLE) and
  - the guarding functionality (#NMT_GUARDING_ENABLE)
  are enabled.

*/
#define NMT_NODES_TO_GUARD 5

/*!
  \def SYNC_PRODUCER
  Enable SYNC producer functionality.

  In addition to the SYNC consumer functionality, the producer feature is
  available (user adaptable).

  \note
  This feature is only available if the NMT master is enabled (#NMT_MASTER_ENABLE).
  (#NMT_MASTER_ENABLE).

  - 0 SYNC producer disabled.
  - 1 SYNC producer enabled.
*/
#define SYNC_PRODUCER 0

/*!
  @} end of group nmtmaster1
*/

/*!
  \defgroup sdohandler1 SDO related define statements (user adaptable)
  @{
*/

/*!
  \def CLIENT_ENABLE
  Enable SDO client functionality.

  With the client functionality it is possible to initiate SDO connections
  to all other modules in the network (with the additional module sdoclnt.c).

  The following values are allowed:
  - 1 client functionality enabled
  - 0 client functionality disabled
*/
#define CLIENT_ENABLE 0

/*!
  \def MULTI_SDO_SERVER
  Enable multiple SDO server handling.

  Normally a CANopen slave has only one SDO connection available
  for its internal object dictionary.
  With this define multiple server connections can be used
  simultaneously.

  The following values are allowed:
  - 1 multiple SDO servers are enabled.
  - 0 only the default SDO server is enabled.
*/
#define MULTI_SDO_SERVER 1

/*!
  \def NUM_SDO_SERVERS
  Number of used SDO servers.

  Here only the values 1 or 2 are allowed.
*/
#if MULTI_SDO_SERVER == 1
# define NUM_SDO_SERVERS 2
#else
# define NUM_SDO_SERVERS 1
#endif /* MULTI_SDO_SERVER == 1 */

/*!
  \def SDO_SERVER2
  Second SDO server node id.

  Every further SDO server needs a pair of CAN identifiers.
  We use a virtual node id to calculate them.
*/
#define SDO_SERVER2 (ModuleInfo.bBoardAdr + 1)

/*!
  \def SDO_SERVER2_DEFAULT_ENABLE
  Enable the second SDO server by default.

  According to the CANopen specification an additional SDO server has to
  be disabled by default. By setting this define to '1' the default behaviour
  can be bypassed though.

  The following values are allowed:
  - 0 the second SDO server is disabled after BootUp.
  - 1 the second SDO server is enabled  after BootUp.
*/
#define SDO_SERVER2_DEFAULT_ENABLE 1

/*!
  \def SDO_EXEC_USER_WRITE_CALLBACKS
  Determine if a user callback has to be executed on SDO write access.

  The SDO server can execute user callbacks from <usrclbck.c> if a download
  has been initiated but before the data is written to the object location
  respectively after the data has been written to the final object location
  (gSdoS_ExpDownCb(), gSdoS_DownCb(), gSdoS_ExpDownCbFinish(), and
  gSdoS_DownCbFinish()).

  - 0 Do not execute the user callbacks.
  - 1 Execute the user callbacks.

  \sa SDO_EXEC_USER_READ_CALLBACKS
 */
#define SDO_EXEC_USER_WRITE_CALLBACKS 1

/*!
  \def SDO_EXEC_USER_READ_CALLBACKS
  Determine if a user callback has to be executed on SDO read access.

  The SDO server can execute a user callback from <usrclbck.c> if an upload
  has been initiated but before the response is transmitted from the server
  (gSdoS_UpCb()).

  - 0 Do not execute the user callback.
  - 1 Execute the user callback.

  \sa SDO_EXEC_USER_WRITE_CALLBACKS
 */
#define SDO_EXEC_USER_READ_CALLBACKS 1

/*!
  \def SDO_WRITE_SEG_ALLOWED
  Restrict SDO write access.

  With this define the segmented write access to
  the local object dictionary is allowed.

  The SDO server can handle a segmented SDO write access
  to the local object dictionary (user adaptable).
  - 0 Segmented write access to the local server not allowed
      (only expedited access available).
  - 1 Segmented write access available.
 */
#define SDO_WRITE_SEG_ALLOWED 1

/*!
  \def SDO_READ_SEG_ALLOWED
  Restrict SDO read access.

  The SDO server can handle a segmented SDO read access
  from the local object dictionary (user adaptable).
  - 0 Segmented read access to the local server not allowed
      (only expedited access).
  - 1 Segmented read access available.
 */
#define SDO_READ_SEG_ALLOWED 1

/*!
  \def SDO_BLOCK_ALLOWED
  The block mode for SDO transfers is allowed.

  The block transfer is mainly intended for large data items.
  - 0 No block transfer support.
  - 1 Block transfer available.
*/
#define SDO_BLOCK_ALLOWED 0

/*!
  \def SDO_BLOCK_USE_CRC
  The generation of CRC for block transfer is supported.

  During initiation of the block transfer client and
  server negotiate the use of a CRC.
  - 0 Do not use CRC.
  - 1 Use CRC as specified in DS301.
*/
#define SDO_BLOCK_USE_CRC 0

/*!
  \def CRC_LOOKUP_TABLE
  Use a lookup table for CRC generation.

  There are two methods provided to generate the CRC.
  - 0 Generate CRC via function only (takes more time).
  - 1 Generate CRC via lookup table (takes more space).
*/
#define CRC_LOOKUP_TABLE 1

/*!
  \def SDO_MAX_BLK_SIZE
  The number of message sequences belonging to a block.

  Here a number in the range 1..127 can be specified.
  This number is used for the initial block size.
  It is a good idea to make this value less then the number
  of available buffer entries.
*/
#define SDO_MAX_BLK_SIZE 8

/*!
  \def SDO_PST_VALUE
  Protocol switch threshold.

  Here a number in the range 0..255 can be specified.
  If the number is 0 there is no switching to segmented transfer
  allowed if the client requests a block upload.
*/
#define SDO_PST_VALUE 28

/*!
  \def DELAYED_SDO_ENABLE
  Enable delayed SDO data access.

  The SDO server can handle delayed data access to an (external)
  location that needs some time to complete. At this time only access
  up to 4 byte objects (expedited transfer) is implemented.
  This functionality requires the add on module <sdodlyac.c>

  - 0 No delayed SDO data access needed.
  - 1 Use additional delayed SDO data access.
*/
#define DELAYED_SDO_ENABLE 0

/*!
  \def HANDLE_UNKNOWN_OBJ_IN_APPL
  Enable handling unknown objects in application.

  If any object access requested by SDO finds no object description
  in the standard object dictionary or the extended user object dictionary
  (mfctobj.h/objentry.h) an error is reported: #OBJ_NOT_EXIST. For separate
  handling of those requests by the application this define has to be set.
  Appropriate handling is already implemented for delayed SDO access enabled
  (#DELAYED_SDO_ENABLE == 1).

  - 0 Unknown objects cause SDO reply #OBJ_NOT_EXIST
  - 1 Unknown objects are handled by the application.
*/
#define HANDLE_UNKNOWN_OBJ_IN_APPL 0

/*!
  \def SDO_LOCAL_READ
  Enable local read access to the object dictionary.

  Via gReadObject() read access to the local object dictionary is possible.
  The caller does not need to know the address of the read object, the index
  and sub-index information is sufficient.
  The read access is restricted to objects with a maximum of 4 bytes.

  - 0 No local SDO read access.
  - 1 Use local SDO read access via gReadObject().
*/
#define SDO_LOCAL_READ 0

/*!
  \def SDO_LOCAL_WRITE
  Enable local write access to the object dictionary.

  Via gWriteObject() write access to the local object dictionary is possible.
  The caller does not need to know the address of the written object, the index
  and sub-index information is sufficient.
  The write access is restricted to objects with a maximum of 4 bytes.

  - 0 No local SDO write access.
  - 1 Use local SDO write access via gWriteObject().
*/
#define SDO_LOCAL_WRITE 0

/*!
  @} end of group sdohandler1
*/

/*!
  \def SIGNAL_BOOTUPMSG
  The reception of bootup messages is signalled to the application.

  The function BootUpEventRcv() from <usrclbck.c> is called
  upon reception.
  This is only possible in queued mode (user adaptable).
 - 1 Use the callback.
 - 0 Do not signal bootup messages.
 */
#define SIGNAL_BOOTUPMSG 0

/*!
  \def SIGNAL_STATE_CHANGE
  Setting the communication state is signalled to the application.

  The function gSlvStackEventCallback() from <usrclbck.c> is called
  every time the communication state is set.
 - 1 Use the callback.
 - 0 Do not signal communication state setting.

  \sa #SIGNAL_STACK_EVENT
 */
#define SIGNAL_STATE_CHANGE 1

/*!
  \def SIGNAL_GUARDFAILURE
  The guarding failure event is signalled to the application.

  The function initiated by gSlvStackEventCallback() from <usrclbck.c> is
  called upon occurrence of a guarding failure event.
 - 1 Use the callback.
 - 0 Do not signal guarding failure event.

 \sa #SIGNAL_STACK_EVENT
 */
#define SIGNAL_GUARDFAILURE 1

/*!
  \def ON_COMMERROR_GO_PREOPERATIONAL
  A communication error event causes a transition to PRE-OPERATIONAL state.

  In case of a communication error event (Nodeguarding/Heartbeat or BUS OFF)
  the device is automatically switching to the PRE_OPERATIONAL state.

 - 1 Automatic state transition enabled.
 - 0 No automatic state transition performed.

  \remark
  This switch shows effect only if object 1029h (Error Behaviour) is not
  implemented. If #OBJECT_1029_USED == 1 the corresponding behaviour can be
  configured via this object (subindex 1).
 */
#define ON_COMMERROR_GO_PREOPERATIONAL 0

/*!
  \defgroup emcygroup1 EMCY related configuration
  @{
*/

/*!
  \def SIGNAL_EMCYMSG
  The reception of emcy messages is signalled to the application.

  The function EmcyEventRcv() in the CAN driver module is called
  upon reception.
  This is only possible in queued mode (user adaptable).
  - 1 Use the callback.
  - 0 Do not signal EMCY messages.
 */
#define SIGNAL_EMCYMSG 0

/*!
  \def ENABLE_EMCYMSG
  The transmission of EMCY messages is enabled.

  The following values are allowed.
  - 0 Do not generate EMCY messages.
  - 1 Generate EMCY messages.
 */
#define ENABLE_EMCYMSG 1

/*!
  @} end of group emcygroup1
*/

/*!
  \def DEVICE_TYPE_VAL
  Value for the entry #DEVICE_TYPE in the object dictionary.
*/
#define DEVICE_TYPE_VAL 0x00000191UL

/*!
  \def VENDOR_ID_VAL
  Value for the entry #IDENTITY_OBJECT

  This value describes the vendor id part
  of the identity object (sub-index 1).
*/
#define VENDOR_ID_VAL    0x00000096UL

/*!
  \def PRODUCT_CODE_VAL
  Value for the entry #IDENTITY_OBJECT

  This value describes the product code part
  of the identity object (sub-index 2).
*/
#define PRODUCT_CODE_VAL 0x05060708UL

/*!
  \def REVISION_NUM_VAL
  Value for the entry #IDENTITY_OBJECT

  This value describes the revision number part
  of the identity object (sub-index 3).
*/
#define REVISION_NUM_VAL 0x090A0B0CUL

/*!
  \def SERIAL_NUM_VAL
  Value for the entry #IDENTITY_OBJECT

  This value describes the serial number part
  of the identity object (sub-index 4).
*/
#define SERIAL_NUM_VAL   0x0D0E0F10UL


/*!
  \def PRODUCT_NAME
  Object 1008: manufacturer device name

  \note If the name is longer than 4 bytes be sure that the segmented
  transfer is activated for read access (#SDO_READ_SEG_ALLOWED).
  This value is returned on a read access to #DEVICE_NAME.
*/
#define PRODUCT_NAME "Svcm"

/*!
  \def HW_VERSION
  Object 1009: manufacturer hardware version

  \note If the name is longer than 4 bytes be sure that the segmented
  transfer is activated for read access (#SDO_READ_SEG_ALLOWED).
  This value is returned on a read access to #HW_VER.
*/
#define HW_VERSION "134241"

/*!
  \def SW_VERSION
  Object 100A: manufacturer software version

  \note If the name is longer than 4 bytes be sure that the segmented
  transfer is activated for read access (#SDO_READ_SEG_ALLOWED).
  This value is returned on a read access to #SW_VER.
*/
#define SW_VERSION "Version 4.3.0"

/*!
  \def TIME_STAMP_USED
  Enable time stamp object.

  The time stamp object is known (user adaptable).
  - 0 time stamp object disabled.
  - 1 time stamp object enabled.
*/
#define TIME_STAMP_USED 0

/*!
  \def EXEC_USER_TIME_STAMP_CALLBACKS
  Determine if a user callback has to be executed on time stamp received.

  The stack can execute the user callback gSlvStackEventCallback() from
  <usrclbck.c> if a time stamp message is received and the device is not in
  #STOPPED state.
  - 0 Do not execute the user callback.
  - 1 Execute the user callback.

  \sa #SIGNAL_STACK_EVENT, #TIME_STAMP_USED
 */
#define EXEC_USER_TIME_STAMP_CALLBACKS 0

/*!
  \def SYNC_USED
  Enable SYNC object and SYNC consumer functionality.

  The SYNC object and functionality is known (user adaptable).
  - 0 SYNC object disabled.
  - 1 SYNC object enabled.
*/
#define SYNC_USED 1

/*!
  \def SIGNAL_SYNC
  The reception of the SYNC message is signalled to the application.

  The function SyncEventRcv() in the CAN driver module is called
  upon reception.
  - 1 Use the callback.
  - 0 Do not signal the SYNC message.
 \sa #SYNC_USED
 */
#define SIGNAL_SYNC 0

/*!
  \def EXAMPLE_OBJECTS_USED
  Enable the example object in the object dictionary.

  There are some example objects already provided in the object dictionary.
  - 0 example objects disabled
  - 1 example objects enabled

*/
#define EXAMPLE_OBJECTS_USED 0

/*!
  \def EXAMPLE_PROFILE_USED
  Enable the example profile in the object dictionary.

  There are some example profile entries already provided
  in the object dictionary.
  They form a very basic I/O device.
  - 0 example profile objects disabled
  - 1 example profile objects enabled

*/
#define EXAMPLE_PROFILE_USED 0



/*!
  \def CONF_GENERATION
  This value signals that the code was configured with the
  tool CANfigurator CANopen.

  \note This value should not be used by hand!

  - 0 default (code not configured with tool)
  - 1 code configured with tool
*/
#define CONF_GENERATION 0



/*!
  \def _BIG_ENDIAN
  Switch target machine support (user adaptable).

  Set this to 0 if the target machine has an intel byte ordering scheme.
  Set this to 1 if the target machine has a motorola byte ordering scheme.
*/
#define _BIG_ENDIAN 0

#define _LITTLE_ENDIAN (!_BIG_ENDIAN) /*!< little endian is set automatically */

/*!
  \def MACHINE_IS_16BIT_ONLY
  Switch target machine support (user adaptable).

  Set this to 0 if the target machine does support bytes (8 bit values),
  which is the normal case.
  Set this to 1 only if the target machine supports words (16 bit) as
  smallest value.
*/
#define MACHINE_IS_16BIT_ONLY 1


/*--------------------------------------------------------------------*/
/*  #define statement validation section                              */
/*--------------------------------------------------------------------*/
/* Check SYNC parameters */
#if SYNC_PRODUCER == 1
# if NMT_MASTER_ENABLE == 0
#  error "SYNC Producer is permitted for NMT Master Services only!"
# endif
# if SYNC_USED == 0
#  error "SYNC Producer needs SYNC Object!"
# endif
#endif
#if SIGNAL_SYNC == 1
# if SYNC_USED == 0
#  error "Signalling SYNC message needs SYNC Object!"
# endif
#endif

/* Check node guarding parameters */
#if NMT_GUARDING_ENABLE == 1
# if NMT_MASTER_ENABLE == 0
#  error "Node Guarding is permitted for NMT Master Services only!"
# endif
# if HEARTBEAT_CONSUMER == 1
#  error "Node Guarding cannot be used by a Heartbeat Consumer!"
# endif
#endif

/* Check NMT error control */
#if START_HEARTBEAT_TIME != 0
# if ENABLE_NMT_ERROR_CTRL == 0
#  error "No NMT error control available to initialize!"
# endif
# if ENABLE_NMT_ERROR_CTRL == 1
#  error "Heartbeat Producer expected but does not exist!"
# endif
#endif

/* Check boundary condition for I/O profile DS401 */
#if GENERIC_PROFILE_USED == 1
  /* RPDO */
# if (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1)
#  error "Profile needs evaluation of receive PDO mapping !"
# endif /* (RPDO_MAP_READONLY == 1) && (RPDO_DIRECT_COPY == 1) */

  /* TPDO */
# if (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1)
#  error "Profile needs evaluation of transmit PDO mapping !"
# endif /* (TPDO_MAP_READONLY == 1) && (TPDO_DIRECT_COPY == 1) */
#endif /* GENERIC_PROFILE_USED == 1 */

/* Check profile dependencies */
#if PROFILE_DS401 == 1
# if GENERIC_PROFILE_USED == 0
#  error "IO Profile DS-401 needs generic profile support! (GENERIC_PROFILE_USED == 1)"
# endif /* GENERIC_PROFILE_USED == 0 */
#endif /* PROFILE_DS401 == 1 */

/* check CANoe emulation environment */
#if (USE_CANOE_EMU == 1) && (INFINITE_LOOP == 1)
# error "#USE_CANOE_EMU and #INFINITE_LOOP must not be set at the same time!"
#endif


/*--------------------------------------------------------------------*/
/*  macros                                                            */
/*--------------------------------------------------------------------*/
/*! return maximum of A and B */
#define MAX(A, B)   ((A) > (B) ? (A) : (B))
/*! return first data byte of a message */
#define MESSAGE_DB0(index) (tCanMsgBuffer[(index)].tMsg.bDb[0])
/*! return pointer to a massage */
#define MESSAGE_PTR(index) (&tCanMsgBuffer[(index)].tMsg)
/*! return a data byte of a message */
#define MESSAGE_DB(index, i) (tCanMsgBuffer[(index)].tMsg.bDb[(i)])

/*!
  \def MemCpy
  Substitute memory copy.
  Platforms not supporting universal pointers (e.g. PIC18 with Microchip MPLAB
  compiler): limited to 256 byte and no ROM to RAM copying!
*/
#ifndef __18CXX
# define MemCpy memcpy
#else
# define MemCpy(dst, src, len) do {                               \
   BYTE bCopyLoop;                                                \
   BYTE *pDest;                                                   \
   CONST BYTE *pSrc;                                              \
   pDest = (BYTE *)dst;                                           \
   pSrc = (CONST BYTE *)src;                                      \
   for (bCopyLoop = 0; bCopyLoop < len; bCopyLoop++)              \
   {                                                              \
     *pDest++ = *pSrc++;                                          \
   }                                                              \
 } while(0)
#endif /* __18CXX */

/*!
  \def MemSet
  Substitute memory set.
*/
#ifndef __18CXX
# define MemSet memset
#else
# define MemSet(dst, val, len) do {                               \
   WORD wCopyLoop;                                                \
   BYTE *pDest;                                                   \
   pDest = (BYTE*)dst;                                            \
   for (wCopyLoop=0; wCopyLoop < len; wCopyLoop++)                \
   {                                                              \
     *pDest = (BYTE)val;                                          \
     pDest++;                                                     \
   }                                                              \
 } while(0)
#endif /* __18CXX */

/*!
  \def PDO_MAPPING_PTR
   Belongs to platforms not supporting unviversal pointers only!
   (e.g. PIC18 with Microchip MPLAB compiler) */
#ifndef __18CXX
# define PDO_MAPPING_PTR
#else
# if (RPDO_MAP_READONLY == 0) && (TPDO_MAP_READONLY == 0)
#  define PDO_MAPPING_PTR
# elif (RPDO_MAP_READONLY == 1) && (TPDO_MAP_READONLY == 1)
#   define PDO_MAPPING_PTR rom
# else
#   error "RX/TX PDO static/variable mapping setup (xPDO_MAP_READONLY) has to be identical!"
# endif
#endif /* __18CXX */

/*! lock the application data for exclusive read access */
#define LOCK_APPLICATION_RDATA
/*! unlock the application data for non-exclusive read access */
#define UNLOCK_APPLICATION_RDATA
/*! lock the application data for exclusive write access */
#define LOCK_APPLICATION_WDATA
/*! unlock the application data for non-exclusive write access */
#define UNLOCK_APPLICATION_WDATA

/*! set the receive PDO mapping data location accordingly */
#if RPDO_MAP_READONLY == 1
# define RPDO_MAPPING atRxPdoDefMap
#else
# define RPDO_MAPPING gStkGlobDat.atRxPdoActMap
#endif /* RPDO_MAP_READONLY == 1 */

/*! set the receive PDO parameter location accordingly */
#if RPDO_PAR_READONLY == 1
# define RPDO_PARAM mRPDOAttr
#else
# define RPDO_PARAM gStkGlobDat.mRPDOAttr
#endif /* RPDO_PAR_READONLY == 1 */

/*! set the transmit PDO mapping data location accordingly */
#if TPDO_MAP_READONLY == 1
# define TPDO_MAPPING atTxPdoDefMap
#else
# define TPDO_MAPPING gStkGlobDat.atTxPdoActMap
#endif /* TPDO_MAP_READONLY == 1 */

/*! set the transmit PDO parameter location accordingly */
#if TPDO_PAR_READONLY == 1
# define TPDO_PARAM mTPDOAttr
#else
# define TPDO_PARAM gStkGlobDat.mTPDOAttr
#endif /* RPDO_PAR_READONLY == 1 */



/*--------------------------------------------------------------------*/
/*  type definitions                                                  */
/*--------------------------------------------------------------------*/
typedef unsigned char BOOLEAN;                  /*!<  1 bit  basic type */
typedef unsigned char BYTE;                     /*!<  8 bits basic type */
typedef unsigned short WORD;                    /*!< 16 bits basic type */
typedef unsigned long DWORD;                    /*!< 32 bits basic type */

/*! 64 bits basic type */
typedef struct {
  unsigned long hw; /*!< most significant double  */
  unsigned long lw; /*!< least significant double */
}
QWORD;

/*!
  two bytes structure
*/
typedef struct {
#if MACHINE_IS_16BIT_ONLY == 1
# if _BIG_ENDIAN == 1
  unsigned int mb : 8; /*!< most significant byte  */
  unsigned int lb : 8; /*!< least significant byte */
# else
  unsigned int lb : 8; /*!< least significant byte */
  unsigned int mb : 8; /*!< most significant byte  */
# endif /* _BIG_ENDIAN == 1 */
#else
# if _BIG_ENDIAN == 1
  BYTE mb; /*!< most significant byte  */
  BYTE lb; /*!< least significant byte */
# else
  BYTE lb; /*!< least significant byte */
  BYTE mb; /*!< most significant byte  */
# endif /* _BIG_ENDIAN == 1 */
#endif /* MACHINE_IS_16BIT_ONLY == 1 */
}
DBYTE;


/*!
  two byte structure for machine independent word conversion
*/
typedef union {
  WORD  wc; /*!< word             */
  DBYTE bc; /*!< two single bytes */
}
WBYTE;


/*!
  four bytes structure
*/
typedef struct {
#if MACHINE_IS_16BIT_ONLY == 1
# if _BIG_ENDIAN == 1
  unsigned int b3 : 8; /*!< high word / most significant byte  */
  unsigned int b2 : 8; /*!< high word / least significant byte */
  unsigned int b1 : 8; /*!< low word / most significant byte   */
  unsigned int b0 : 8; /*!< low word / least significant byte  */
# else
  unsigned int b0 : 8; /*!< low word / least significant byte  */
  unsigned int b1 : 8; /*!< low word / most significant byte   */
  unsigned int b2 : 8; /*!< high word / least significant byte */
  unsigned int b3 : 8; /*!< high word / most significant byte  */
# endif /* _BIG_ENDIAN == 1 */
#else
# if _BIG_ENDIAN == 1
  BYTE b3; /*!< high word / most significant byte  */
  BYTE b2; /*!< high word / least significant byte */
  BYTE b1; /*!< low word / most significant byte   */
  BYTE b0; /*!< low word / least significant byte  */
# else
  BYTE b0; /*!< low word / least significant byte  */
  BYTE b1; /*!< low word / most significant byte   */
  BYTE b2; /*!< high word / least significant byte */
  BYTE b3; /*!< high word / most significant byte  */
# endif /* _BIG_ENDIAN == 1 */
#endif /* MACHINE_IS_16BIT_ONLY == 1 */
}
FOURBYTE;


/*!
  eight bytes structure
*/
#if OBJECTS_64BIT_USED == 1
typedef struct {
# if MACHINE_IS_16BIT_ONLY == 1
#  if _BIG_ENDIAN == 1
  unsigned int b7 : 8;
  unsigned int b6 : 8;
  unsigned int b5 : 8;
  unsigned int b4 : 8;
  unsigned int b3 : 8;
  unsigned int b2 : 8;
  unsigned int b1 : 8;
  unsigned int b0 : 8;
#  else
  unsigned int b0 : 8;
  unsigned int b1 : 8;
  unsigned int b2 : 8;
  unsigned int b3 : 8;
  unsigned int b4 : 8;
  unsigned int b5 : 8;
  unsigned int b6 : 8;
  unsigned int b7 : 8;
#  endif /* _BIG_ENDIAN == 1 */
# else
#  if _BIG_ENDIAN == 1
  BYTE b7;
  BYTE b6;
  BYTE b5;
  BYTE b4;
  BYTE b3;
  BYTE b2;
  BYTE b1;
  BYTE b0;
#  else
  BYTE b0;
  BYTE b1;
  BYTE b2;
  BYTE b3;
  BYTE b4;
  BYTE b5;
  BYTE b6;
  BYTE b7;
#  endif /* _BIG_ENDIAN == 1 */
# endif /* MACHINE_IS_16BIT_ONLY == 1 */
}
EIGHTBYTE;
#endif /* OBJECTS_64BIT_USED == 1 */


/*!
  four byte structure for machine independent long conversion
*/
typedef union {
  DWORD dw;     /*!< long value          */
  FOURBYTE fb;  /*!< splitted long value */
}
QBYTE;


/*!
  eight byte structure
*/
#if OBJECTS_64BIT_USED == 1
typedef union {
  EIGHTBYTE eb; /*!< splitted value */
}
OBYTE;
#endif /* OBJECTS_64BIT_USED == 1 */


/*!
  This structure holds the actual settings for the module.
*/
typedef struct _vModInfo {
  BYTE  bBoardAdr;    /*!< module's address 1..127      */
  BYTE  bBaudRate;    /*!< module's baudrate code       */
  BYTE  bModuleState; /*!< module's state               */
  BYTE  bCommState;   /*!< module's communication state */
}
vModInfo;


/*!
  producer heartbeat information
*/
typedef struct _vPRO_HEART {
  WORD  wTime;                  /*!< timer                             */
  WBYTE wReloadTime;            /*!< timer reload value                */
}
PRO_HEART;


/*!
  \brief Heartbeat consumer information

  This structure holds the actual consumer information.
*/
typedef struct _vCON_HEART {
  WORD  wTime;                  /*!< Actual timer value.               */
  BYTE  bStatus;                /*!< status - holds actual communication status */
  BYTE  bRun;                   /*!< enable flag [#OFF|#ON]            */
}
CON_HEART;


/*!
  \brief Heartbeat consumer parameters for non volatile storage.

  These heartbeat consumer related parameetrs have to be stored
  in non-volatile memory.
*/
typedef struct _VHbAttrib {
  WBYTE wReloadTime;            /*!< timer reload value                */
  BYTE  bNodeID;                /*!< node identifier                   */
}
VHbAttrib;


/*!
  PDO mapping entry
 */
typedef struct _vMAPPING {
  BYTE  bObjLength;             /*!< CANopen object length     */
  BYTE  bSubIndex;              /*!< CANopen subindex          */
  BYTE  bIndexLsb;              /*!< CANopen index LSB         */
  BYTE  bIndexMsb;              /*!< CANopen index MSB         */
  BYTE  FCONST * pData;          /*!< location of data value    */
}
MAPPING;


/*!
  \brief transmit PDO communication attributes.

  These are the actual control values for our TPDO.
  The values are changing during runtime.
*/
typedef struct _vTPDOparameter {
#if SYNC_USED == 1
  BYTE  bSyncCounter;           /*!< sync counter for transmission type */
#endif /* SYNC_USED == 1 */
  WORD wEventTimer;             /*!< event timer                        */
  WORD wInhibitTime;            /*!< inhibit time                       */
  /*!
    \brief Enable/disable PDO

    Values (RPDO/TPDO):
    - 1 - enabled
    - 0 - disabled
  */
  unsigned int bMode: 1;
  /*!
    \brief Transmission requested/not requested.

    Values (TPDO only):
    - 1 - requested
    - 0 - not requested
  */
  unsigned int bTransRequest: 1;
  /*!
    \brief Remote request allowed on this PDO.

    Values (TPDO only):
    - 1 - allowed
    - 0 - not allowed
  */
  unsigned int bRemoteAllowed: 1;
  /*!
    \brief Mapped data have been updated.

    Values (TPDO only):
    - 1 - updated
    - 0 - not updated
  */
  unsigned int bNewValue: 1;
}
TPDOparameter;


/*!
  \brief TPDO parameter structure for non volatile storage.

  These attributes are intended to be stored in non-volatile memory.
*/
typedef struct _VTPDOComParData {
  QBYTE mCOB;                   /*!< COB-ID            */
  WORD  wEventReload;           /*!< event timer reload value           */
  WORD  wInhibitReload;         /*!< inhibit reload                     */
  BYTE  mTT;                    /*!< transmission type                  */
}
VTPDOComParData;


/*!
  \brief receive PDO communication attributes.

  For every PDO we need some managing values.
*/
typedef struct _vRPDOparameter {
#if SYNC_USED == 1
  BYTE  bSyncCounter;     /*!< sync counter for transmission type */
#endif /* SYNC_USED == 1 */
  /*!
    \brief Enable/disable PDO

    Values (RPDO/TPDO:
    - 1 - enabled
    - 0 - disabled
  */
  unsigned int bMode: 1;
}
RPDOparameter;


/*!
  \brief RPDO parameter structure for non volatile storage.

  These attributes are intended to be stored in non-volatile memory.
*/
typedef struct _VRPDOComParData {
  QBYTE mCOB;                   /*!< COB-ID            */
  BYTE  mTT;                    /*!< transmission type                  */
}
VRPDOComParData;


/*!
  CANopen time of day.
*/
typedef struct _vTIME_OF_DAY {
  QBYTE ms;                     /*!< milliseconds after midnight        */
  WBYTE days;                   /*!< number of days since 1984/01/01.   */
}
TIME_OF_DAY;


/*!
  \brief Second SDO server parameter structure for non volatile storage.

  This structure provides place to store the SDO attributes.
*/
typedef struct _VSSDOParData {
  DWORD mCobRx; /*!< receive COB-ID */
  DWORD mCobTx; /*!< transmit COB-ID */
  BYTE mNodeId; /*!< client Node-ID */
}
VSSDOParData;


/*!
  \brief Storage block header

  Every storage block is preceded by a header of this format.
  With the provided information it is possible to validate
  the storage block.
*/
typedef struct _VStoreAreaHead {
  WORD wMagic;    /*!< Holds the value #CONFIG_SAVED if a configuration is stored. */
  WORD wCheckSum; /*!< Checksum over storage block without header. */
  WORD wBlockLen; /*!< Overall length of storage block. */
} VStoreAreaHead;


/*!
  \brief Non volatile storage for communication relevant parameters
*/
typedef struct _VComParaArea {

#if STORE_PARAMETERS_SUPP==1
  VStoreAreaHead Head;                 /*!< block management head */
#endif /* STORE_PARAMETERS_SUPP==1 */

  BYTE mBoardAdr; /*!< store last board address */
  BYTE mBaudRate; /*!< store last baudrate */
#if (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3)
  WORD mGuardTime;                     /*!< guard time */
  BYTE mLifeTimeFactor;                /*!< life time factor */
#endif /* (ENABLE_NMT_ERROR_CTRL == 1) || (ENABLE_NMT_ERROR_CTRL == 3) */

#if ENABLE_NMT_ERROR_CTRL > 1
  PRO_HEART tHeartbeatPro;             /*!< producer heartbeat information    */
#endif /* ENABLE_NMT_ERROR_CTRL > 1 */

#if SYNC_USED == 1
  QBYTE mSyncId;                       /*!< SYNC identifier                   */
  QBYTE lComCyclePeriod;               /*!< communication cycle period        */
  QBYTE lSyncWinLength;                /*!< synchronous window length         */
#endif /* SYNC_USED == 1 */

#if ENABLE_EMCYMSG == 1
  WBYTE mEmcyInhReloadTime;            /*!< EMCY inhibit reload */
#endif /* ENABLE_EMCYMSG == 1 */

#if HEARTBEAT_CONSUMER == 1
  VHbAttrib atHeartbeatCon[NUM_OF_HB_CONSUMERS]; /*!< heartbeat consumer table */
#endif /* HEARTBEAT_CONSUMER == 1 */

#if (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1)
  VSSDOParData mSSdoParameter[NUM_SDO_SERVERS - 1];
#endif /* (MULTI_SDO_SERVER == 1) && (NUM_SDO_SERVERS > 1) */

#if MAX_RX_PDOS > 0
# if RPDO_PAR_READONLY == 0
  VRPDOComParData mRPDOAttr[MAX_RX_PDOS]; /*!< RPDO parameter   */
# endif /* RPDO_PAR_READONLY == 0 */
# if RPDO_MAP_READONLY == 0
  /*!
    \brief Actual used mapping for receive PDOs.

    This mapping is placed in RAM where it can be changed.
    If the mapping of receive PDOs is not changeable
    this variable does not exist.
  */
  MAPPING atRxPdoActMap[MAX_RX_PDOS][MAX_MAPPED_OBJECTS];
# endif /* RPDO_MAP_READONLY == 0 */
#endif /* MAX_RX_PDOS > 0 */

#if MAX_TX_PDOS > 0
#if TPDO_PAR_READONLY == 0
  VTPDOComParData mTPDOAttr[MAX_TX_PDOS]; /*!< TPDO parameter   */
#endif /* TPDO_PAR_READONLY == 0 */
# if TPDO_MAP_READONLY == 0
  /*!
    \brief Actual used mapping for transmit PDOs.

    This mapping is placed in RAM where it can be changed.
    If the mapping of tarnsmit PDOs is not changeable
    this variable does not exist.
  */
  MAPPING atTxPdoActMap[MAX_TX_PDOS][MAX_MAPPED_OBJECTS]; /*!< TPDO mapping     */
# endif /* TPDO_MAP_READONLY == 0 */
#endif /* MAX_TX_PDOS > 0 */

} VComParaArea;


/*--------------------------------------------------------------------*/
/*  prototype declarations                                            */
/*--------------------------------------------------------------------*/
BYTE NewGuardData(void);          /* obtain last guarding information */
BYTE GetModulStatus(void);        /* get actual status */
BYTE gSlvGetMapLen(CONST MAPPING PDO_MAPPING_PTR *, BYTE); /* get mapping length */
void gEvalEmcy(void); /* evaluate new emcy errors */
void gSlvForceTxPdoEvent(BYTE); /* force a TX PDO to be transmitted */
#if HEARTBEAT_CONSUMER == 1
BYTE gSlvSetErrCtrlRxNode(BYTE, WORD); /* set heartbeat checking */
#endif /* HEARTBEAT_CONSUMER == 1 */
#if CHECK_CONSISTENCY==1
void gVerifyMapping(void);
BYTE gCheckODOrdering(void);
#endif /* CHECK_CONSISTENCY==1 */
void InitMapping(void);
void InitComPars(void);
void gGoOperational(void);
void gGoPreOperational(void);
void gGoStopped(void);
void gGoResetComm(void);
void gGoResetNode(void);
void gStoreComPar(void);
BYTE gApplyComPar(void);
void gSlvSetIdent(void);

#endif /* _COS_MAIN_H_ */



/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Main header for CANopen slave.
  \par File name cos_main.h
  \version 83
  \date 4.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

