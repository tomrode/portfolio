/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/* --------------------------------------------------------------------*/

/*!
  \file
  \brief Receive PDO mapping parameter.

  Be sure that the referenced variables are mappable, writeable
  and that the objects exist in the object dictionary.

  \par File name rmapping.h
  \version 1
  \date 25.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/



# if MAX_RX_PDOS > 0

{
    NUM_OF_MAP_ENTRIES(1),                                                   /* Receive PDO 4   */    
    MAP_ENTRY(OBJ_CAN_KEYBOARD_RCV, 0, 8, (BYTE *)&gRpdoCanLanguageSelectInput.ubData),
    VOID_MAP_ENTRY, 
    VOID_MAP_ENTRY,
    VOID_MAP_ENTRY,
    VOID_MAP_ENTRY,
    VOID_MAP_ENTRY,
    VOID_MAP_ENTRY, 
    VOID_MAP_ENTRY 
} 
# endif 

