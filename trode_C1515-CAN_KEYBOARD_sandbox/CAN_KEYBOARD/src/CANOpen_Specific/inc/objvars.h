/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/* --------------------------------------------------------------------*/

/*!
  \file
  \brief User's object dictionary variables.
  \par File name objvars.h
  \version 2
  \date 25.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

/*!
  \def EXAMPLE_OBJECTS
  Number of manufacturer specific definitions

  Fill in the number of user specific definitions made in the file
  - mfctobj.h (10 entries)
*/

/*!
  \def PROFILE_OBJECTS
  Number of manufacturer specific definitions

  Fill in the number of user specific definitions made in the file
  - objentry.h (12 entries)
*/

/*!
  \def NUM_DS401_OBJECTS
  Number of I/O profile DS-401 specific definitions

  This reflects the number of definitions made for the I/O profile
  DS-401 in the file - objentry.h (39/41 entries)
*/


#if EXAMPLE_OBJECTS_USED == 1

# define EXAMPLE_OBJECTS 10

/*!
  \brief Buffer for domain data.
  This is our example buffer for   the example objects.
  It is intended for demonstration purposes only !
 */
BYTE gDomainBuffer[EXAMPLE_BUFFER_SIZE];

/*!
  \brief Example object 5

  This is the physical representation of this example object.
*/
struct {
  DWORD Obj1; /*!< long object */
  WORD  Obj3; /*!< short object */
  BYTE  Obj4; /*!< byte object */
} gExmpl5;

/*!
  \brief Fixed array length structure.

 Some of the entries in the object dictionary are
 arrays with a fixed number of entries behind.
 The "number of entries" information could be placed
 in ROM if the subindex 0 is a member of this structure.
 */
CONST struct {
  BYTE mNrExample3 [1]; /*!< number of entries for example3   */
  BYTE mNrExample4 [1]; /*!< number of entries for example4   */
} CODE ExplCodeArea =
{
  { sizeof(gDomainBuffer) / ARRAY_ENTRY_SIZE3 },
  { sizeof(gDomainBuffer) / ARRAY_ENTRY_SIZE4 },
};
#else
# define EXAMPLE_OBJECTS 0
#endif /* EXAMPLE_OBJECTS_USED == 1 */



#if EXAMPLE_PROFILE_USED==1

# define PROFILE_OBJECTS 12

BYTE XDATA abOutData[8 * 4]; /*!< output buffer - 32 bytes    */
BYTE XDATA abInData [8 * 4];  /*!< input buffer - 32 bytes    */

/*!
  \brief Fixed array length structure.

 Some of the entries in the object dictionary are
 arrays with a fixed number of entries behind.
 The "number of entries" information could be placed
 in ROM if the subindex 0 is a member of this structure.
 */
CONST struct {
  BYTE mNr6000     [1]; /*!< digital inputs 8 bit wide        */
  BYTE mNr6100     [1]; /*!< digital inputs 16 bit wide       */
  BYTE mNr6120     [1]; /*!< digital inputs 32 bit wide       */
  BYTE mNr6200     [1]; /*!< digital outputs 8 bit wide       */
  BYTE mNr6300     [1]; /*!< digital outputs 16 bit wide      */
  BYTE mNr6320     [1]; /*!< digital outputs 32 bit wide      */
} CODE PrflCodeArea =
{
  { sizeof(abInData) },
  { sizeof(abInData) / 2 },
  { sizeof(abInData) / 4 },
  { sizeof(abOutData) },
  { sizeof(abOutData) / 2 },
  { sizeof(abOutData) / 4 },
};
#else
# define PROFILE_OBJECTS 0
#endif /* EXAMPLE_PROFILE_USED==1 */



#if PROFILE_DS401 == 1

# if DS401_TEST == 1
BYTE XDATA abInRawDig[INPUT_BUFFER_LEN_DI]; /*!< raw input buffer digital  */
WORD XDATA awInRawAna[INPUT_BUFFER_LEN_AI]; /*!< raw input buffer analogue */
# endif /* DS401_TEST == 1 */

# if OBJECT_6422_USED == 1
#  define NUM_DS401_OBJECTS 42
# else
#  define NUM_DS401_OBJECTS 40
# endif /* OBJECT_6422_USED == 1 */

/*
  The variables that hold the values of the object dictionary entries
  for DS401 are declared here. The initialization is according to the
  default values in the specification DS401 version 2.0
*/

/**************************************************************/
/* variables for digital input                                */
/**************************************************************/

/*! location of polarity values (Index 6002h) */
BYTE abDI_PolarityInput[INPUT_BUFFER_LEN_DI];

/*! location of filter constant values (Index 6003h) */
BYTE abDI_FilterConstantInput[INPUT_BUFFER_LEN_DI];

/*! global interrupt enable digital (Index 6005h) */
BOOLEAN fDI_GlobalIntEnable;

/*! location of any change mask values (Index 6006h) */
BYTE abDI_IntMaskAnyChange[INPUT_BUFFER_LEN_DI];

/*! location of low to high mask values (Index 6007h) */
BYTE abDI_IntMaskLowToHigh[INPUT_BUFFER_LEN_DI];

/*! location of high to low mask values (Index 6008h) */
BYTE abDI_IntMaskHighToLow[INPUT_BUFFER_LEN_DI];


/**************************************************************/
/* variables for digital output                               */
/**************************************************************/

/*! location of polarity values (Index 6202h) */
BYTE abDO_ChangePolarityOutput[OUTPUT_BUFFER_LEN_DO];

/*! location of filter mask values (Index 6208h) */
BYTE abDO_FilterConstantOutput[OUTPUT_BUFFER_LEN_DO];


/**************************************************************/
/* variables for analogue input                               */
/**************************************************************/

/*! location of interrupt trigger selection (Index 6421h) */
BYTE abAI_IntTriggerSelection[INPUT_BUFFER_LEN_AI];

/*! location of interrupt source (Index 6422h) */
# if OBJECT_6422_USED == 1
DWORD adwAI_IntSource[NUM_AI_INTSOURCE];
# endif /* OBJECT_6422_USED == 1 */

/*! global interrupt enable (Index 6423h) */
BOOLEAN fAI_GlobalIntEnable;

/*! location of interrupt upper limit integer (Index 6424h) */
DWORD adwAI_IntUpperLimitInteger[INPUT_BUFFER_LEN_AI];

/*! location of interrupt lower limit integer (Index 6425h) */
DWORD adwAI_IntLowerLimitInteger[INPUT_BUFFER_LEN_AI];

/*! location of interrupt delta unsigned (Index 6426h) */
DWORD adwAI_IntDeltaUnsigned[INPUT_BUFFER_LEN_AI];

/*! location of interrupt negative delta unsigned (Index 6427h) */
DWORD adwAI_IntNegDeltaUnsigned[INPUT_BUFFER_LEN_AI];

/*! location of interrupt positive delta unsigned (Index 6428h) */
DWORD adwAI_IntPosDeltaUnsigned[INPUT_BUFFER_LEN_AI];


/**************************************************************/
/* variables for analogue output                              */
/**************************************************************/

/*! location of error mode (Index 6443h) */
BYTE abAO_ErrorMode[OUTPUT_BUFFER_LEN_AO];

/*! location of error value integer (Index 6444h) */
DWORD adwAO_ErrorValueInteger[OUTPUT_BUFFER_LEN_AO];


/**************************************************************/
/* variables for general device profile                       */
/**************************************************************/

# if MAX_RX_PDOS > 0
BYTE XDATA abOutDataDig[OUTPUT_BUFFER_LEN_DO]; /*!< output buffer digital  */
WORD XDATA awOutDataAna[OUTPUT_BUFFER_LEN_AO]; /*!< output buffer analogue */
# endif /* MAX_RX_PDOS > 0 */

# if MAX_TX_PDOS > 0
BYTE XDATA abInDataDig[INPUT_BUFFER_LEN_DI];   /*!< input buffer digital   */
WORD XDATA awInDataAna[INPUT_BUFFER_LEN_AI];   /*!< input buffer analogue  */
# endif /* MAX_TX_PDOS > 0 */

# if DS401_TEST == 1
BYTE XDATA abInRawDig[INPUT_BUFFER_LEN_DI]; /*!< raw input buffer digital  */
WORD XDATA awInRawAna[INPUT_BUFFER_LEN_AI]; /*!< raw input buffer analogue */
# endif /* DS401_TEST == 1 */

#else
# define NUM_DS401_OBJECTS 0
#endif /* PROFILE_DS401 == 1 */


#if OBJECT_1029_USED == 1
/*! location of error behavior (Index 1029h) */
/* SUBSTITUTION OF OBJECT 67FE ! (see errata sheet for DS-401 / 2001-03-05) */
BYTE abErrorBehavior[NUM_ERROR_CLASSES + 1];
#endif /* OBJECT_1029_USED == 1 */

#if SCOFLD_SUPPORT == 1
# define NUM_SCOFLD_OBJECTS 1
/*! configuration of flash loader (SCOFLD) */
QBYTE gScofldConf;
#else
# define NUM_SCOFLD_OBJECTS 0
#endif /* SCOFLD_SUPPORT == 1 */

/*!
  \brief Number of user objects in object dictionary.

  This define value must correspond with the number of object dictionary
  entries provided via the files
  objentry.h and mfctobj.h.
  This value can also be generated automatically by a tool.
*/
#define USER_MAPPED_OBJECTS 188

#if CONF_GENERATION == 1
#define NUM_USER_OBJECTS USER_MAPPED_OBJECTS
#else
#define NUM_USER_OBJECTS (EXAMPLE_OBJECTS + PROFILE_OBJECTS + NUM_DS401_OBJECTS + NUM_SCOFLD_OBJECTS + USER_MAPPED_OBJECTS)
#endif /* CONF_GENERATION == 1 */
