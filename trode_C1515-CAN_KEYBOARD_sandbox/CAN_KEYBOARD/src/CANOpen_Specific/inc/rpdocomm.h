/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/* --------------------------------------------------------------------*/

/*!
  \file
  \brief Receive PDO communication parameter.
  \par File name rpdocomm.h
  \version 1
  \date 25.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
#include "Truck_CAN.h"

#if MAX_RX_PDOS > 0
 RPDO_COMM(KYBD_PDO1_RX, 0xFF) /* receive PDO 3 */
#endif /* MAX_RX_PDOS > 2 */




