#include "Truck_CAN.h"
#include "Truck_PDO.h"
#include "fram.h"
#include "objects.h"
#include "EventMgr.h"
#include "AdcData.h"
#include "InputSwitches.h"

extern const char partnumber[17];

#define MAP_DEVICE_NAME  SINGLE_OBJ(DEVICE_NAME, 16, (BYTE *)gszModulePartNumber,   RO, SDO_EXEC_CB_NOT),
#define MAP_HW_VER       SINGLE_OBJ(HW_VER,      16, (BYTE *)gszHardwarePartNumber, RO, SDO_EXEC_CB_NOT),
#define MAP_SW_VERSION   SINGLE_OBJ(SW_VER,      16, (BYTE *)partnumber,            RO, SDO_EXEC_CB_NOT),


