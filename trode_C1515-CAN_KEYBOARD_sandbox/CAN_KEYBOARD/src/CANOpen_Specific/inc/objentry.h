/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/*--------------------------------------------------------------------*/

/*!
  \file
  \brief Example entries for the profile's part of the object dictionary.
  \par File name objentry.h
  \version 2
  \date 2.09.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

#if EXAMPLE_PROFILE_USED == 1
  /* digital inputs 6000 - 8 bit access type */
  ARRAY_HEAD(DIGI_IN_8BIT, 1, CONST_ADR((BYTE FCONST *)PrflCodeArea.mNr6000),
            RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(DIGI_IN_8BIT, sizeof(abInData), 1, &abInData[0],
            ROMAP, SDO_EXEC_CB_NOT),
  /* digital inputs 6100 - 16 bit access type */
  ARRAY_HEAD(DIGI_IN_16BIT, 1, CONST_ADR((BYTE FCONST *)PrflCodeArea.mNr6100),
            RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(DIGI_IN_16BIT, (sizeof(abInData) / 2), 2, &abInData[0],
            ROMAP, SDO_EXEC_CB_NOT),
  /* digital inputs 6120 - 32 bit access type */
  ARRAY_HEAD(DIGI_IN_32BIT, 1, CONST_ADR((BYTE FCONST *)PrflCodeArea.mNr6120),
            RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(DIGI_IN_32BIT, (sizeof(abInData) / 4), 4, &abInData[0],
            ROMAP, SDO_EXEC_CB_NOT),

  /* digital outputs 6200 - 8 bit access type */
  ARRAY_HEAD(DIGI_OUT_8BIT, 1, CONST_ADR((BYTE FCONST *)PrflCodeArea.mNr6200),
            RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(DIGI_OUT_8BIT, sizeof(abOutData), 1, &abOutData[0],
            RWWMAP, SDO_EXEC_CB_NOT),
  /* digital outputs 6300 - 16 bit access type */
  ARRAY_HEAD(DIGI_OUT_16BIT, 1, CONST_ADR((BYTE FCONST *)PrflCodeArea.mNr6300),
            RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(DIGI_OUT_16BIT, (sizeof(abOutData) / 2), 2, &abOutData[0],
            RWWMAP, SDO_EXEC_CB_NOT),
  /* digital outputs 6320 - 32 bit access type */
  ARRAY_HEAD(DIGI_OUT_32BIT, 1, CONST_ADR((BYTE FCONST *)PrflCodeArea.mNr6320),
            RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(DIGI_OUT_32BIT, (sizeof(abOutData) / 4), 4, &abOutData[0],
            RWWMAP, SDO_EXEC_CB_NOT),
#endif /* EXAMPLE_PROFILE_USED == 1 */



#if PROFILE_DS401 == 1
/*--------------------------------------------------------------------*/
/*  local constant definitions                                        */
/*--------------------------------------------------------------------*/
#define NUM_DIG_INPUT8        "\x10"  /*!< number of digital input 8-bit channels    */
#define NUM_DIG_OUTPUT8       "\x10"  /*!< number of digital output 8-bit channels   */
#define NUM_ANA_INPUT16       "\x10"  /*!< number of analogue input 16-bit channels  */
#define NUM_ANA_OUTPUT16      "\x04"  /*!< number of analogue output 16-bit channels */
#define ADR_ANA_INPUT_INTSRC  "\x01"  /*!< number of analogue input interrupt source */
#define NUM_ANA_INPUT_INTSRC  0x01    /*!< number of analogue input interrupt source */


/*--------------------------------------------------------------------*/
/*  constants                                                         */
/*--------------------------------------------------------------------*/

/*
  Digital Input: Read Input 8-Bit.
  index: 6000
  sub-index 0
  type: array[INPUT_BUFFER_LEN_DI] of unsigned8
  mappable: yes (to TPDOs only)
  location: abInDataDig[]
*/
  ARRAY_HEAD(DIGI_IN_8BIT, 1, CONST_ADR((BYTE FCONST *)NUM_DIG_INPUT8), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(DIGI_IN_8BIT, INPUT_BUFFER_LEN_DI, 1, (BYTE *) &abInDataDig, ROMAP, SDO_EXEC_CB_NOT),

/*
  Digital Input: Polarity Input 8-Bit.
  index: 6002
  sub-index 0
  type: array[INPUT_BUFFER_LEN_DI] of unsigned8
  mappable: optional
  location: abDI_PolarityInput[]
*/
  ARRAY_HEAD(POLARITY_IN_8BIT, 1, CONST_ADR((BYTE FCONST *)NUM_DIG_INPUT8), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(POLARITY_IN_8BIT, INPUT_BUFFER_LEN_DI, 1, (BYTE *) &abDI_PolarityInput, RW, SDO_EXEC_CB_NOT),

/*
  Digital Input: Filter Constant Input 8-Bit.
  index: 6003
  sub-index 0
  type: array[INPUT_BUFFER_LEN_DI] of unsigned8
  mappable: optional
  location: abDI_FilterConstantInput[]
*/
  ARRAY_HEAD(FILTER_IN_8BIT, 1, CONST_ADR((BYTE FCONST *)NUM_DIG_INPUT8), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(FILTER_IN_8BIT, INPUT_BUFFER_LEN_DI, 1, (BYTE *) &abDI_FilterConstantInput, RW, SDO_EXEC_CB_NOT),

/*
  Digital Input: Global Interrupt Enable Digital 8-Bit
  index: 6005
  sub-index 0
  type: variable of boolean
  mappable: no
  location: fDI_GlobalIntEnable
*/
  SINGLE_OBJ(GLOB_INT_ENABLE, 1, (BYTE *)&fDI_GlobalIntEnable, RW, SDO_EXEC_CB_NOT),

/*
  Digital Input: Interrupt Mask Any Change 8-Bit.
  index: 6006
  sub-index 0
  type: array[INPUT_BUFFER_LEN_DI] of unsigned8
  mappable: optional
  location: abDI_IntMaskAnyChange[]
*/
  ARRAY_HEAD(INT_MASK_ANY_CHANGE, 1, CONST_ADR((BYTE FCONST *)NUM_DIG_INPUT8), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(INT_MASK_ANY_CHANGE, INPUT_BUFFER_LEN_DI, 1, (BYTE *) &abDI_IntMaskAnyChange, RW, SDO_EXEC_CB_NOT),

/*
  Digital Input: Interrupt Mask Low-To-High 8-Bit.
  index: 6007
  sub-index 0
  type: array[INPUT_BUFFER_LEN_DI] of unsigned8
  mappable: optional
  location: abDI_IntMaskLowToHigh[]
*/
  ARRAY_HEAD(INT_MASK_LOW_TO_HIGH, 1, CONST_ADR((BYTE FCONST *)NUM_DIG_INPUT8), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(INT_MASK_LOW_TO_HIGH, INPUT_BUFFER_LEN_DI, 1, (BYTE *) &abDI_IntMaskLowToHigh, RW, SDO_EXEC_CB_NOT),

/*
  Digital Input: Interrupt Mask High-To-Low 8-Bit.
  index: 6008
  sub-index 0
  type: array[INPUT_BUFFER_LEN_DI] of unsigned8
  mappable: optional
  location: abDI_IntMaskHighToLow[]
*/
  ARRAY_HEAD(INT_MASK_HIGH_TO_LOW, 1, CONST_ADR((BYTE FCONST *)NUM_DIG_INPUT8), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(INT_MASK_HIGH_TO_LOW, INPUT_BUFFER_LEN_DI, 1, (BYTE *) &abDI_IntMaskHighToLow, RW, SDO_EXEC_CB_NOT),

/*
  Digital Output: Write Output 8-Bit.
  index: 6200
  sub-index 0
  type: array[OUTPUT_BUFFER_LEN_DO] of unsigned8
  mappable: optional
  location: abOutDataDig[]
*/
  ARRAY_HEAD(DIGI_OUT_8BIT, 1, CONST_ADR((BYTE FCONST *)NUM_DIG_OUTPUT8), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(DIGI_OUT_8BIT, OUTPUT_BUFFER_LEN_DO, 1, (BYTE *) &abOutDataDig, WOMAP/*RW*/, SDO_EXEC_CB_NOT),

/*
  Digital Output: Change Polarity Output 8-Bit.
  index: 6202
  sub-index 0
  type: array[OUTPUT_BUFFER_LEN_DO] of unsigned8
  mappable: optional
  location: abDO_ChangePolarityOutput[]
*/
  ARRAY_HEAD(POLARITY_OUT_8BIT, 1, CONST_ADR((BYTE FCONST *)NUM_DIG_OUTPUT8), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(POLARITY_OUT_8BIT, OUTPUT_BUFFER_LEN_DO, 1, (BYTE *) &abDO_ChangePolarityOutput, RW, SDO_EXEC_CB_NOT),

/*
  Digital Output: Filter Mask Output 8-Bit.
  index: 6208
  sub-index 0
  type: array[OUTPUT_BUFFER_LEN_DO] of unsigned8
  mappable: optional
  location: abDO_FilterConstantOutput[]
*/
  ARRAY_HEAD(FILTER_OUT_8BIT, 1, CONST_ADR((BYTE FCONST *)NUM_DIG_OUTPUT8), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(FILTER_OUT_8BIT, OUTPUT_BUFFER_LEN_DO, 1, (BYTE *) &abDO_FilterConstantOutput, RW, SDO_EXEC_CB_NOT),

/*
  Analogue Input: Read Analogue Input 16-Bit.
  index: 6401
  sub-index 0
  type: array[INPUT_BUFFER_LEN_AI] of signed16
  mappable: optional
  location: awInDataAna[]
*/
  ARRAY_HEAD(ANA_IN_16BIT, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_INPUT16), ROMAP, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_IN_16BIT, INPUT_BUFFER_LEN_AI, 2, (BYTE *) &awInDataAna, ROMAP, SDO_EXEC_CB_NOT),

/*
  Analogue Output: Write Analog Output 16-Bit.
  index: 6411
  sub-index 0
  type: array[OUTPUT_BUFFER_LEN_AO] of signed16
  mappable: optional
  location: awOutDataAna[]
*/
  ARRAY_HEAD(ANA_OUT_16BIT, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_OUTPUT16), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_OUT_16BIT, OUTPUT_BUFFER_LEN_AO, 2, (BYTE *) &awOutDataAna, WOMAP, SDO_EXEC_CB_NOT),

/*
  Analogue Input: Analogue Input Interrupt Trigger Selection.
  index: 6421
  sub-index 0
  type: array[INPUT_BUFFER_LEN_AI] of unsigned8
  mappable: optional
  location: abAI_IntTriggerSelection[]
*/
  ARRAY_HEAD(ANA_IN_INT_TRIGGER_SELECT, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_INPUT16), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_IN_INT_TRIGGER_SELECT, INPUT_BUFFER_LEN_AI, 1, (BYTE *) &abAI_IntTriggerSelection, RW, SDO_EXEC_CB_NOT),

/*
  Analogue Input: Analogue Input Interrupt Source.
  index: 6422
  sub-index 0
  type: array[NUM_ANA_INPUT_INTSRC] of unsigned32
  mappable: optional
  location: adwAI_IntSource[]
*/
#if OBJECT_6422_USED == 1
  ARRAY_HEAD(ANA_IN_INT_SOURCE, 1, CONST_ADR((BYTE FCONST *)ADR_ANA_INPUT_INTSRC), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_IN_INT_SOURCE, NUM_ANA_INPUT_INTSRC, 4, (BYTE *) &adwAI_IntSource, RO, SDO_EXEC_CB_NOT),
#endif /* OBJECT_6422_USED == 1 */

/*
  Analogue Input: Analogue Global Interrupt Enable.
  index: 6423
  sub-index 0
  type: variable of boolean
  mappable: no
  location: fAI_GlobalIntEnable
*/
  SINGLE_OBJ(ANA_IN_GLOBAL_INT_ENABLE, 1, (BYTE *) &fAI_GlobalIntEnable, RW, SDO_EXEC_CB_NOT),

/*
  Analogue Input: Analogue Input Interrupt Upper Limit Integer.
  index: 6424
  sub-index 0
  type: array[INPUT_BUFFER_LEN_AI] of signed32
  mappable: optional
  location: adwAI_IntUpperLimitInteger[]
*/
  ARRAY_HEAD(ANA_IN_INT_UPPER_LIMIT, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_INPUT16), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_IN_INT_UPPER_LIMIT, INPUT_BUFFER_LEN_AI, 4, (BYTE *) &adwAI_IntUpperLimitInteger, RW, SDO_EXEC_CB_NOT),

/*
  Analogue Input: Analogue Input Interrupt Lower Limit Integer.
  index: 6425
  sub-index 0
  type: array[INPUT_BUFFER_LEN_AI] of signed32
  mappable: optional
  location: adwAI_IntLowerLimitInteger[]
*/
  ARRAY_HEAD(ANA_IN_INT_LOWER_LIMIT, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_INPUT16), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_IN_INT_LOWER_LIMIT, INPUT_BUFFER_LEN_AI, 4, (BYTE *) &adwAI_IntLowerLimitInteger, RW, SDO_EXEC_CB_NOT),

/*
  Analogue Input: Analogue Input Interrupt Delta Unsigned.
  index: 6426
  sub-index 0
  type: array[INPUT_BUFFER_LEN_AI] of unsigned32
  mappable: optional
  location: adwAI_IntDeltaUnsigned[]
*/
  ARRAY_HEAD(ANA_IN_INT_DELTA_UNSIG, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_INPUT16), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_IN_INT_DELTA_UNSIG, INPUT_BUFFER_LEN_AI, 4, (BYTE *) &adwAI_IntDeltaUnsigned, RW, SDO_EXEC_CB_NOT),

/*
  Analogue Input: Analogue Input Interrupt Negative Delta Unsigned.
  index: 6427
  sub-index 0
  type: array[INPUT_BUFFER_LEN_AI] of unsigned32
  mappable: optional
  location: adwAI_IntNegDeltaUnsigned[]
*/
  ARRAY_HEAD(ANA_IN_INT_NEG_DELTA_UNSIG, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_INPUT16), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_IN_INT_NEG_DELTA_UNSIG, INPUT_BUFFER_LEN_AI, 4, (BYTE *) &adwAI_IntNegDeltaUnsigned, RW, SDO_EXEC_CB_NOT),

/*
  Analogue Input: Analogue Input Interrupt Positive Delta Unsigned.
  index: 6428
  sub-index 0
  type: array[INPUT_BUFFER_LEN_AI] of unsigned32
  mappable: optional
  location: adwAI_IntPosDeltaUnsigned[]
*/
  ARRAY_HEAD(ANA_IN_INT_POS_DELTA_UNSIG, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_INPUT16), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_IN_INT_POS_DELTA_UNSIG, INPUT_BUFFER_LEN_AI, 4, (BYTE *) &adwAI_IntPosDeltaUnsigned, RW, SDO_EXEC_CB_NOT),

/*
  Analogue Output: Analogue Output Error Mode.
  index: 6443
  sub-index 0
  type: array[OUTPUT_BUFFER_LEN_AO] of unsigned8
  mappable: optional
  location: abAO_ErrorMode[]
*/
  ARRAY_HEAD(ANA_OUT_ERROR_MODE, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_OUTPUT16), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_OUT_ERROR_MODE, OUTPUT_BUFFER_LEN_AO, 1, (BYTE *) &abAO_ErrorMode, RW, SDO_EXEC_CB_NOT),

/*
  Analogue Output: Analogue Output Error Value Integer.
  index: 6444
  sub-index 0
  type: array[OUTPUT_BUFFER_LEN_AO] of signed32
  mappable: optional
  location: adwAO_ErrorValueInteger[]
*/
  ARRAY_HEAD(ANA_OUT_ERROR_VALUE, 1, CONST_ADR((BYTE FCONST *)NUM_ANA_OUTPUT16), RO, SDO_EXEC_CB_NOT),
  ARRAY_BODY(ANA_OUT_ERROR_VALUE, OUTPUT_BUFFER_LEN_AO, 4, (BYTE *) &adwAO_ErrorValueInteger, RW, SDO_EXEC_CB_NOT),
#endif /* PROFILE_DS401 == 1 */
