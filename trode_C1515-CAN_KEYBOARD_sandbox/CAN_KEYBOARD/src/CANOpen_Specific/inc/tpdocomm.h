/*--------------------------------------------------------------------*/
/*  documentation                                                     */
/* --------------------------------------------------------------------*/

/*!
  \file
  \brief Transmit PDO communication parameter.
  \par File name tpdocomm.h
  \version 1
  \date 25.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/

/* NOTE:  Highest PDO number = highest priority */


#if MAX_TX_PDOS > 0
TPDO_COMM(0x7F4UL, 0xFE, 0, 0)
#endif /* MAX_TX_PDOS > 0 */

#if MAX_TX_PDOS > 1
, TPDO_COMM(0x7F3UL, 0xFE, 0, 0)
#endif /* MAX_TX_PDOS > 1 */

#if MAX_TX_PDOS > 2
, TPDO_COMM(0x7F2UL, 0xFE, 0, 0)
#endif /* MAX_TX_PDOS > 2 */

#if MAX_TX_PDOS > 3
, TPDO_COMM(0x7F1UL, 0xFE, 0, 0)
#endif /* MAX_TX_PDOS > 3 */

#if MAX_TX_PDOS > 4
, TPDO_COMM(CIM1_ID, 0xFE, 0, 0)
#endif /* MAX_TX_PDOS > 4 */

#if MAX_TX_PDOS > 5
, TPDO_COMM(KYBD_PDO1_TX, 0xFE, 0, 0)
#endif /* MAX_TX_PDOS > 5 */


