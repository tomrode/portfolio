/*!
  \file
  \brief Fill in references to variables used in the user's mapping.
  \par File name mapvars.h
  \version 1
  \date 25.07.03
  \author (c) 1998-2003 by Vector Informatik GmbH, Stuttgart
*/
#include "DSP2803x_Device.h"
#include "objects.h"
#include "EventMgr.h"
#include "Truck_PDO.h"


