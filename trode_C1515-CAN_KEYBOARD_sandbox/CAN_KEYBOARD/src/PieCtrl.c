/**********************************************************************
* File: PieCtrl.c -- File for Labs 5, 6, 7, 8, and 9
* Devices: TMS320F2812, TMS320F2811, TMS320F2810
* Author: Technical Training Organization (TTO), Texas Instruments
* History:
*   11/10/03 - original (based on DSP281x header files v1.00)
**********************************************************************/

#include "DSP2803x_Device.h"


/**********************************************************************
* Function: InitPieCtrl()
*
* Description: Initializes and enables the PIE interrupts on the F281x
**********************************************************************/
void InitPieCtrl(void)
{

/*** Disable interrupts ***/
    asm(" SETC INTM");          // Disable global interrupts

/*** Disable all PIE interrupts ***/
    PieCtrlRegs.PIEIER1.all =  0x0000;
    PieCtrlRegs.PIEIER2.all =  0x0000;
    PieCtrlRegs.PIEIER3.all =  0x0000;
    PieCtrlRegs.PIEIER4.all =  0x0000;
    PieCtrlRegs.PIEIER5.all =  0x0000;
    PieCtrlRegs.PIEIER6.all =  0x0000;
    PieCtrlRegs.PIEIER7.all =  0x0000;
    PieCtrlRegs.PIEIER8.all =  0x0000;
    PieCtrlRegs.PIEIER9.all =  0x0000;
    PieCtrlRegs.PIEIER10.all = 0x0000;
    PieCtrlRegs.PIEIER11.all = 0x0000;
    PieCtrlRegs.PIEIER12.all = 0x0000;

/*** Clear any potentially pending PIEIFR flags ***/
    PieCtrlRegs.PIEIFR1.all  = 0x0000;
    PieCtrlRegs.PIEIFR2.all  = 0x0000;
    PieCtrlRegs.PIEIFR3.all  = 0x0000;
    PieCtrlRegs.PIEIFR4.all  = 0x0000;
    PieCtrlRegs.PIEIFR5.all  = 0x0000;
    PieCtrlRegs.PIEIFR6.all  = 0x0000;
    PieCtrlRegs.PIEIFR7.all  = 0x0000;
    PieCtrlRegs.PIEIFR8.all  = 0x0000;
    PieCtrlRegs.PIEIFR9.all  = 0x0000;
    PieCtrlRegs.PIEIFR10.all = 0x0000;
    PieCtrlRegs.PIEIFR11.all = 0x0000;
    PieCtrlRegs.PIEIFR12.all = 0x0000;

/*** Acknowlege all PIE interrupt groups ***/
    PieCtrlRegs.PIEACK.all = 0xFFFF;

/*** Enable the PIE ***/
    PieCtrlRegs.PIECTRL.bit.ENPIE = 1;      // Enable the PIE

} //end of InitPieCtrl()


/*** end of file *****************************************************/
