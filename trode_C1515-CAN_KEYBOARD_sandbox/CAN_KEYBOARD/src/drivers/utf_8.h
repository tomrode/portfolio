/**
 *  @file                   utf_8.h
 *  @brief                  Defines the data structures and variables
 *  @copyright              2014 Crown Equipment Corp., New Bremen, OH  45869
 *  @date                   10/15/2014    
 *
 *  @remark Author:         Tom Rode
 *
 *  @remark Project:        C1515
 *              
 */

#ifndef UTF_8
#define UTF_8 1
#include "types.h"


/*==============================================================*
 *  SECTION - Global type definitions
 *========================================================================*
 */
/** @def    mGET_ROW 
 *  @brief Extract the row from input parameter gUwRowColStat 
 */
#define mGET_ROW(x)       ((uint16_t)((x) & 0x0007))

/** @def    mSET_ROW 
 *  @brief Input the row  into gUwRowColStat with upto 8 row bits
 */
#define mSET_ROW(x,y)    ((uint16_t)((x) & 0xFFF8) | (uint8_t)(y) )

/** @def    mGET_COLUMN 
 *  @brief Extract the column from gUwRowColStat
 */
#define mGET_COLUMN(x)    ((uint16_t)((x) & 0x00F8) >> 3u) 

 /** @def    mSET_COLUMN 
 *  @brief Input the Column into gUwRowColStat with upto 32 column bits
 */
#define mSET_COLUMN(x,y)    ((uint16_t)((x) & 0xFF07) | ((uint8_t)(y) << 3u))

/** @def    mGET_SHIFT 
 *  @brief Extract the shift special function key from gUwRowColStat
 */ 
#define mGET_SHIFT(x)     ((uint16_t)((x) & 0x0100) >> 8u)   

 /** @def    mSET_SHIFT 
 *  @brief Input the Shift bit into gUwRowColStat with on off state
 */
#define mSET_SHIFT(x,y)   ((uint16_t)((x) & 0xFEFF) | ((uint8_t)(y) << 8u) )

/** @def    mGET_ALTERNATE 
 *  @brief Extract the alternate special function key from gUwRowColStat
 */  
#define mGET_ALTERNATE(x) ((uint16_t)((x) & 0x0200) >> 9u)

/** @def    mSET_ALTERNATE 
 *  @brief Input the alternate bit into gUwRowColStat with on off state
 */ 
#define mSET_ALTERNATE(x,y) ((uint16_t)((x) & 0xFDFF) | ((uint8_t)(y) << 9u) )

/** @def    mGET_FUNCTION 
 *  @brief Extract the function special function key from gUwRowColStat
 */ 
#define mGET_FUNCTION(x)  ((uint16_t)((x) & 0x0400) >> 10u)

/** @def    mSET_FUNCTION 
 *  @brief Input the function bit into gUwRowColStat with on off state
 */ 
#define mSET_FUNCTION(x,y)  ((uint16_t)((x) & 0xFBFF) | ((uint8_t)(y) << 10u) )

/** @def    mGET_LANGUAGE 
 *  @brief Extract the language from gUwRowColStat
 */     
#define mGET_LANGUAGE(x)  ((uint16_t)((x) & 0xF000) >> 12u)   

/** @def    mSET_LANGUAGE 
 *  @brief Input the language into gUwRowColStat with upto 16 language possibilities
 */
#define mSET_LANGUAGE(x,y)  ((uint16_t)((x) & 0x0FFF) | ((uint8_t )(*y) << 12u) )

/** @def   TXPDO_MSG_DLY 
 *  @brief Delay time used in transmitting CAN PDO
 */
#define  TXPDO_MSG_DLY 16u

#define MAX_CHARS_PER_PDO 4u  /**< Maximum number of characters per CAN PDO */

#define BITS_PER_CHAR 8u  /**< Number of bits in each keyboard character */

#define NUM_KEYBOARD_DATA_BYTES 7u

/** @def   DECODE_1uS_DELAY 
 *  @brief Delay time in 200 nanosecond increments used after the get switch position due to Space bar issue
 */
#define DECODE_1uS_DELAY  5u

/** @enum    utf8_types
 *  @brief   Key board key structure data types
 *
 *  @typedef keyboard_switch_num_t
 *  @brief   @ref utf8 type definition
 */
typedef enum keyboard_switch_num
{
    KEYSWNONE = 0u,
    KEYSW1 = 1u,
    KEYSW2 = 2u,
    KEYSW3 = 3u,
    KEYSW4 = 4u,
    KEYSW5 = 5u,
    KEYSW6 = 6u,
    KEYSW7 = 7u,
    KEYSW8 = 8u,
    KEYSW9 = 9u,
    KEYSW10 = 10u,
    KEYSW11 = 11u,
    KEYSW12 = 12u,
    KEYSW13 = 13u,
    KEYSW14 = 14u,
    KEYSW15 = 15u,
    KEYSW16 = 16u,
    KEYSW17 = 17u,
    KEYSW18 = 18u,
    KEYSW19 = 19u,
    KEYSW20 = 20u,
    KEYSW21 = 21u,
    KEYSW22 = 22u,
    KEYSW23 = 23u,
    KEYSW24 = 24u,
    KEYSW25 = 25u,
    KEYSW26 = 26u,
    KEYSW27 = 27u,
    KEYSW28 = 28u,
    KEYSW29 = 29u,
    KEYSW30 = 30u,
    KEYSW31 = 31u,
    KEYSW32 = 32u,
    KEYSW33 = 33u,
    KEYSW34 = 34u,
    KEYSW35 = 35u,
    KEYSW36 = 36u,
    KEYSW37 = 37u,
    KEYSW38 = 38u,
    KEYSW39 = 39u,
    KEYSW40 = 40u,
    KEYSW41 = 41u,
    KEYSW42 = 42u,
    KEYSW43 = 43u,
    KEYSW44 = 44u,
    KEYSW45 = 45u,
    KEYSW46 = 46u,
    KEYSW47 = 47u,
    KEYSW48 = 48u,
    KEYSW49 = 49u,
    KEYSW50 = 50u,
    KEYSW51 = 51u,
    KEYSW52 = 52u,
    KEYSW53 = 53u,
    KEYSW54 = 54u,
    KEYSW55 = 55u,
    KEYSWMAX = 56u
} keyboard_switch_num_t;
   
/** @struct  utf8
 *  @brief   Row driver States
 *
 *  @typedef keyboard_hardware_decode_sm_t
 *  @brief   Key assignment 
 */  
typedef enum keyboard_hardware_decode_sm
{
    ROW1_DRV_ST = 0u,     /**< Row1 Excitation */
    ROW2_DRV_ST = 1u,     /**< Row2 Excitation */ 
    ROW3_DRV_ST = 2u,     /**< Row3 Excitation */
    ROW4_DRV_ST = 3u,     /**< Row4 Excitation */  
    ROW5_DRV_ST = 4u,     /**< Row5 Excitation */
    ROW6_DRV_ST = 5u,     /**< Row6 Excitation */
    ROW7_DRV_ST = 6u,   
    ROW_RESET_ST = 99u  /**< Reset cycle     */ 
} keyboard_hardware_decode_sm_t;

/** @struct  utf8
 *  @brief  Keyboard functions key select
 *
 *  @typedef keyboard_function_sm_t
 *  @brief   Function key combinations 
 */  
typedef enum keyboard_function_sm
{
    STANDARD = 0u,        /**< Lower case selection */
    SHIFT    = 1u,        /**< Upper case selection */ 
    ALT      = 2u,        /**< Alternate keys , arrows, symbols, etc.  */
    FN       = 3u         /**< Function  */  
   
}keyboard_function_sm_t; 

/** @enum  keyboard_language
 *  @brief  Keyboard language select
 *
 *  @typedef keyboard_language_t
 *  @brief   Specific language set via IMM 
 */  
typedef enum keyboard_language
{
    DEFAULT_LANG    = 0u,        /**< No language selected */
    ENGLISH_LANG    = 1u,        /**< IMM select English */ 
    GERMAN_LANG     = 2u,        /**< IMM select German  */
    SPANISH_LANG    = 3u,        /**< IMM select Spanish  */ 
    FOURTH_LANG     = 4u,        /**< IMM tbd  */
    FIFTH_LANG      = 5u,        /**< IMM tbd  */
    SIXTH_LANG      = 6u,        /**< IMM tbd  */
    SEVENTH_LANG    = 7u,        /**< IMM tbd  */
    EIGHTH_LANG      = 8u,        /**< IMM tbd  */
    NINTH_LANG     = 9u,        /**< IMM tbd  */
 	SWITCH_TEST     = 0xFF       /**< Hardware switch test*/
}keyboard_language_t; 
/** @struct  utf8
 *  @brief   Unfiltered key switches States
 *
 *  @typedef Key switch history
 *  @brief   Key Debounce 
 */
typedef struct Key_Switches
{
	uint32_t ubHistory[5];  
	uint16_t ubPointer;
	uint32_t uwSwitches[6];
} Key_Switches_t;

/** @struct  utf8
 *  @brief   check for stuck of shoted Switch condition
 *
 *  @typedef Multiple Key switch 
 *  @brief   
 */
typedef struct MultiRow_Sw_Presses
{
    uint8_t  ubNullCnt;
    uint8_t  ubSwShorted;
    uint16_t uwNewSwVal;
    uint16_t uwNewSwValSht;
    uint16_t uwOldSwVal;
    uint16_t uwOldSwValSht;
    uint16_t uwUpdateSwPrs;
}MultiRow_Sw_Presses_t;


/** @struct  utf16
 *  @brief   Language table indexing control  
 *
 *  @typedef Language table index
 *  @brief   
 */
typedef struct RowubColumnIndexControl
{
    uint8_t ubNewBtnPrsdRw1;
    uint8_t ubNewBtnPrsdRw2;
    uint8_t ubNewBtnPrsdRw3;
    uint8_t ubNewBtnPrsdRw4;
    uint8_t ubNewBtnPrsdRw5;
    uint8_t ubNewBtnPrsdRw6; 
    uint8_t ubOldBtnPrsdRw1;
    uint8_t ubOldBtnPrsdRw2;
    uint8_t ubOldBtnPrsdRw3;
    uint8_t ubOldBtnPrsdRw4;
    uint8_t ubOldBtnPrsdRw5;
    uint8_t ubOldBtnPrsdRw6; 
    uint8_t ubColumnIndex;
    uint8_t ubRowIndex;
}RowColumnIndexControl_t;


/*========================================================================*
 *  SECTION - extern global variables                                     *
 *========================================================================*
 */
extern uint16_t gUwRowColStat; 
extern volatile Key_Switches_t gKey_Switches;
extern uint8_t ubRowTriggerSm;
extern uint8_t ubAllowTx;
/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */
extern void gHwKeyDecodeInit(void);
extern void gScanKeyDecodeManager(void);


#endif /* #ifndef UTF_8_H */
