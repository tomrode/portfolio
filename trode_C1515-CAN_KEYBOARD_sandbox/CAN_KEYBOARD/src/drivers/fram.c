/************************************************************************************************
 *  File:       fram.c
 *
 *  Purpose:    Implements the non-volitile ram parameters for the truck
 *
 *  Project:    C1103
 *
 *  Copyright:  2005 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 9/26/2005
 *
 *  Notes:      ######### DO NOT USE level -o3 optimization on this file ######
 ************************************************************************************************
*/
//****************************************************************************
// @Project Includes
//****************************************************************************
#include "types.h"
#include "string.h"
#include "ramtron.h"
#include "Events.h"
#include "EventMgr.h"
#include "EvHandler.h"
#include "datalog.h"
//#include "Ev.h"
//#include "Monitors.h"
#include "Truck_PDO.h"
#include "Watchdog.h"

#define FRAM_MODULE 1
#include "fram.h"
#undef FRAM_MODULE
//#include "AdcData.h"


//****************************************************************************
// @Global Variables
//****************************************************************************

//****************************************************************************
// @Local Variables
//****************************************************************************

//****************************************************************************
// @External Prototypes
//****************************************************************************
extern boolean ReadFram(uword *, uword, uword);
extern boolean WriteFram(uword *, uword, uword, boolean);
extern uword crc16_range(ulong length, uword crc, uword *ptr);
extern uword cks16_range(ulong length, uword cks, uword *ptr);
extern const char partnumber[17];

//****************************************************************************
// @Prototypes Of Local Functions
//****************************************************************************
/**********************************************************************
 *
 * Function:    vLoad_Fram_Defaults()
 *
 * Description: Loads the data structures of FRAM data with default values
 *
 * Argument(s): None
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void vLoad_Fram_Defaults(void)
{
    // set default values of the cal table entries
    memset((void *)&gFram_Cal_Data.slAN0_Gain_q19, 0 , CAL_TABLE_LEN);

    gFram_Cal_Data.slAN0_Gain_q19           = 323989L    ;
    gFram_Cal_Data.slAN0_Offset_q19         = -124542770L;
    gFram_Cal_Data.slAN1_Gain_q19           = 319152L    ;
    gFram_Cal_Data.slAN1_Offset_q19         = -123508843L;
    gFram_Cal_Data.slAN2_Gain_q19           = 322172L    ;
    gFram_Cal_Data.slAN2_Offset_q19         = -124093097L;
    gFram_Cal_Data.slAN3_Gain_q19           = 319422L    ;
    gFram_Cal_Data.slAN3_Offset_q19         = -116507718L;
    gFram_Cal_Data.slAN4_Gain_q19           = 319567L    ;
    gFram_Cal_Data.slAN4_Offset_q19         = -116265755L;
    gFram_Cal_Data.slAN5_Gain_q19           = 320728L    ;
    gFram_Cal_Data.slAN5_Offset_q19         = -125262093L;
    gFram_Cal_Data.slAN6_Gain_q19           = 321259L    ;
    gFram_Cal_Data.slAN6_Offset_q19         = -124816515L;
    gFram_Cal_Data.slAN7_Gain_q19           = 2113L      ;
    gFram_Cal_Data.slAN7_Offset_q19         = 171135L    ;
    gFram_Cal_Data.slAN8_Gain_q19           = 870L       ;
    gFram_Cal_Data.slAN8_Offset_q19         = 4303L      ;
    gFram_Cal_Data.slAN9_Gain_q19           = 87639L     ;
    gFram_Cal_Data.slAN9_Offset_q19         = -55539113L ;
    gFram_Cal_Data.slAN10_Gain_q19          = 3238395L   ;
    gFram_Cal_Data.slAN10_Offset_q19        = 0L         ;
    gFram_Cal_Data.slAN11_Gain_q19          = 749L       ;
    gFram_Cal_Data.slAN11_Offset_q19        = -9143L     ;
    gFram_Cal_Data.slAN12_Gain_q19          = 7528L      ;
    gFram_Cal_Data.slAN12_Offset_q19        = -86651L    ;
    gFram_Cal_Data.slAN13_Gain_q19          = 3086L      ;
    gFram_Cal_Data.slAN13_Offset_q19        = -49315L    ;
    gFram_Cal_Data.slAutoGain_q19           = 511740L    ;
    gFram_Cal_Data.slAutoOffsetCounts       = 12L        ;

}

/**********************************************************************
 *
 * Function:    vLoad_Fram_Setup_Defaults()
 *
 * Description: Loads the data structures of FRAM data with default values
 *
 * Argument(s): None
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void vLoad_Fram_Setup_Defaults(void)
{
    strcpy((char *)gFram_Setup_Data.szPartNumber, partnumber);
}

/**********************************************************************
 *
 * Function:    vServiceFram()
 *
 * Description: Called by the idle task.  Checks for changes in the
 *              RAM copy of FRAM contents.  If changes are required,
 *              the changed area of FRAM is updated.
 *
 * Argument(s): None
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void vServiceFram( void )
{

    volatile boolean fUpdate = FALSE;
    volatile uword uwCRC;

    if (!gfLogToFRAM)
    {
        // determine if the fram needs to be updated
        if (gubCalUpdated > 0)
        {
            fUpdate = TRUE;
            gubCalUpdated = 0;
        }

        if (gubSetupUpdated > 0)
        {
            fUpdate = TRUE;
            gubSetupUpdated = 0;
        }

        if (gubVoidTheCalFRAM > 0)
        {
            fUpdate = TRUE;
            gubVoidTheCalFRAM = 0;
            vLoad_Fram_Defaults();
        }

        if (gubVoidTheSetupFRAM > 0)
        {
            fUpdate = TRUE;
            gubVoidTheSetupFRAM = 0;
            vLoad_Fram_Setup_Defaults();

        }

#if 0    // Handle Monitors
        if (gubMonitor_Void_Flag)
        {
            gubMonitor_Void_Flag = 0;
            vVoidMonitors();
        }

        if (gubMonitor_Update_Flag)
        {
            gubMonitor_Update_Flag = 0;
            vWriteFRAM_Monitors(&gMonitors_Data);
        }
#endif

        if (fUpdate)
        {
            if (!gfPowerDownLock)
            {
                vPutFRAM_Cal_Data(&gFram_Cal_Data);
                uwCRC = crc16_range(sizeof(FRAM_CAL_TABLE_TYPE), 0xFFFF, (uword *)&gFram_Cal_Data);
                vPutFRAM_Cal_CRC(uwCRC);
                guwFRAM_CRC = uwCRC;
            }
            vServiceWatchdog();

            if (!gfPowerDownLock)
            {
                vPutFRAM_Setup_Data(&gFram_Setup_Data);
                uwCRC = crc16_range(sizeof(FRAM_SETUP_TABLE_TYPE), 0xFFFF, (uword *)&gFram_Setup_Data);
                vPutFRAM_Setup_CRC(uwCRC);
                guwFRAM_Setup_CRC = uwCRC;
            }
            vServiceWatchdog();

        }

        if (gubEventsUpdated > 0)
        {
            if (!gfPowerDownLock)
            {
                vWriteFRAM_Events(&gLocalEvents);
                gubEventsUpdated = 0;
                vServiceWatchdog();
            }

        }
        if (geFRAM_Prot_Cal_Cmd == FRAM_PROT_CAL_CLEAR)
        {
            geFRAM_Prot_Cal_Cmd = FRAM_PROT_CAL_NO_COMMAND;
            vClearCalBackup();
        }

        if (geFRAM_Prot_Cal_Cmd == FRAM_PROT_CAL_CLONE)
        {
            geFRAM_Prot_Cal_Cmd = FRAM_PROT_CAL_NO_COMMAND;
            vCloneCalibration();
        }

#if 0   // TODO handle Monitors
        if (gubHourMeterSaveFlag > 0)
        {
            fHrMeterTimeToSave(&gHourMeters.HourMeters.ModHours, 1000);      // keep the save counters from overflowing
            fHrMeterTimeToSave(&gHourMeters.HourMeters.MotorHours,  1000);

            gubHourMeterSaveFlag = 0;
            vServiceMonitors();
            vServiceWatchdog();
            if (!gfPowerDownLock)
            {
                vWriteHourMeters(&gHourMeters);
            }

        }
#endif
    }
    if (mCheckDatalog(DL25_FRAM_MAP_LOG))
    {
        Can1.ubData[0] = (ubyte)((CAL_TABLE_ADDR) >> 8);
        Can1.ubData[1] = (ubyte)(CAL_TABLE_ADDR);
        Can1.ubData[2] = (ubyte)((CAL_TABLE_LEN) >> 8);
        Can1.ubData[3] = (ubyte)(CAL_TABLE_LEN);
        Can1.ubData[4] = (ubyte)((EVENTS_ADDR) >> 8);
        Can1.ubData[5] = (ubyte)(EVENTS_ADDR);
        Can1.ubData[6] = (ubyte)((EVENTS_LEN) >> 8);
        Can1.ubData[7] = (ubyte)(EVENTS_LEN);

        Can2.ubData[0] = (ubyte)((RESET_COUNTS_ADDR) >> 8);
        Can2.ubData[1] = (ubyte)(RESET_COUNTS_ADDR);
        Can2.ubData[2] = (ubyte)((RESET_COUNTS_LEN) >> 8);
        Can2.ubData[3] = (ubyte)(RESET_COUNTS_LEN);
        Can2.ubData[4] = (ubyte)((SETUP_CRC_ADDR) >> 8);
        Can2.ubData[5] = (ubyte)(SETUP_CRC_ADDR);
        Can2.ubData[6] = (ubyte)((SETUP_CRC_LEN) >> 8);
        Can2.ubData[7] = (ubyte)(SETUP_CRC_LEN);

        Can3.ubData[0] = (ubyte)((SETUP_TABLE_ADDR) >> 8);
        Can3.ubData[1] = (ubyte)(SETUP_TABLE_ADDR);
        Can3.ubData[2] = (ubyte)((SETUP_TABLE_LEN) >> 8);
        Can3.ubData[3] = (ubyte)(SETUP_TABLE_LEN);
        Can3.ubData[4] = (ubyte)0;
        Can3.ubData[5] = (ubyte)0;
        Can3.ubData[6] = (ubyte)0;
        Can3.ubData[7] = (ubyte)0;

        Can4.ubData[0] = (ubyte)((HOUR_METERS_ADDR) >> 8);
        Can4.ubData[1] = (ubyte)(HOUR_METERS_ADDR);
        Can4.ubData[2] = (ubyte)((HOUR_METERS_LEN) >> 8);
        Can4.ubData[3] = (ubyte)(HOUR_METERS_LEN);
        Can4.ubData[4] = (ubyte)((FRAM_LOG_ADDR) >> 8);
        Can4.ubData[5] = (ubyte)(FRAM_LOG_ADDR);
        Can4.ubData[6] = (ubyte)((FRAM_LOG_LEN_8K) >> 8);
        Can4.ubData[7] = (ubyte)(FRAM_LOG_LEN_8K);

        vSaveDatalogExtended(&Can1, &Can2, &Can3, &Can4);
    }
}

/**********************************************************************
 *
 * Function:    uwFRAM_Size_Check()
 *
 * Description: Determines the size of the FRAM chip installed on the board.
 *
 * Argument(s): None
 *
 * Returns:     Datalog buffer space (in words) for the given part.
 *              0x400 for 8K and 0x3400 for 32K
 *
 * Side Effects:  Computes the location of the Calibration backup information
 **********************************************************************/
uword uwFRAM_Size_Check(void)
{
    uword uwSize = 0;
    volatile uword uwLow_Word;
    volatile uword uwHigh_Word;
    volatile uword uwSaved_Low_Word;
    volatile uword uwSaved_High_Word;

    // Check the size of FRAM
    ReadFram((uword *)&uwSaved_Low_Word,FRAM_SIZE_CHECK_ADDR,FRAM_SIZE_CHECK_LEN);
    ReadFram((uword *)&uwSaved_High_Word,FRAM_SIZE_CHECK_HIGH_ADDR,FRAM_SIZE_CHECK_HIGH_LEN);

    uwLow_Word = 0xAA;
    uwHigh_Word = 0x55;

    // Write patterns
    WriteFram((uword *) &uwLow_Word, FRAM_SIZE_CHECK_ADDR,FRAM_SIZE_CHECK_LEN, 0);
    WriteFram((uword *) &uwHigh_Word, FRAM_SIZE_CHECK_HIGH_ADDR,FRAM_SIZE_CHECK_HIGH_LEN, 0);

    // Read back
    ReadFram((uword *)&uwLow_Word,FRAM_SIZE_CHECK_ADDR,FRAM_SIZE_CHECK_LEN);
    ReadFram((uword *)&uwHigh_Word,FRAM_SIZE_CHECK_HIGH_ADDR,FRAM_SIZE_CHECK_HIGH_LEN);

    if ((uwLow_Word == 0xAA) && (uwHigh_Word == 0x55))
    {
        // 32K part
        uwSize = FRAM_LOG_LEN_32K;
        WriteFram((uword *)&uwSaved_Low_Word, FRAM_SIZE_CHECK_ADDR, FRAM_SIZE_CHECK_LEN, 0);
        WriteFram((uword *)&uwSaved_High_Word,FRAM_SIZE_CHECK_HIGH_ADDR,FRAM_SIZE_CHECK_HIGH_LEN, 0);
        guwFRAM_CAL_Backup_State_Addr = CAL_BACKUP_STATE_ADDR_32K;
        guwFRAM_CAL_Bakup_CRC_Addr = CAL_BACKUP_CRC_ADDR_32K;
        guwFRAM_CAL_Backup_Addr = CAL_BACKUP_ADDR_32K;
    }
    else
    {
        // 8K part
        uwSize = FRAM_LOG_LEN_8K;
        WriteFram((uword *)&uwSaved_Low_Word, FRAM_SIZE_CHECK_ADDR, FRAM_SIZE_CHECK_LEN, 0);
        guwFRAM_CAL_Backup_State_Addr = CAL_BACKUP_STATE_ADDR_8K;
        guwFRAM_CAL_Bakup_CRC_Addr = CAL_BACKUP_CRC_ADDR_8K;
        guwFRAM_CAL_Backup_Addr = CAL_BACKUP_ADDR_8K;
    }
    return uwSize;
}

/**********************************************************************
 *
 * Function:    vInit_Fram_Data()
 *
 * Description: Loads the data structures of FRAM data.  A recovery
 *              process is run if the FRAM is corrupt.
 *
 * Argument(s): None
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void vInit_Fram_Data(void)
{
    volatile uword uwFram_CRC;
    volatile uword uwCRC;
    volatile ulong length;
    uword uwStartCRC = 0xFFFF;
    volatile uword uwChecksum = 0;
    volatile boolean  fPartNumberOK = TRUE;
    volatile uword address;
    volatile uword num_words;
    // recovery process varables
    volatile boolean fWorkingIsValid;
    volatile boolean fProtectedIsBlank;
    volatile boolean fProtectedIsValid;
    volatile CAL_BACKUP_STATES eProtectedState;
    volatile uword uwCalMode;
    volatile boolean fResult;

    gfReportCalibrationTableFault = FALSE;
    gubCalUpdated = 0;
    gubVoidTheCalFRAM = 0;
    gubLoadTheCalFRAM = 0;
    gfPowerDownLock = FALSE;

    guwFRAM_Log_Length = uwFRAM_Size_Check();
    // Note, the location of the protected block is now set
    // based on 8K part or 32K part

    // get the crc and data stored in the working area of fram
    uwFram_CRC = uwGetFRAM_Cal_CRC();

    vGetFRAM_Cal_Data( &gFram_Cal_Data);
    vServiceWatchdog();

    // now compute crc for what was read from the fram
    length = (ulong)CAL_TABLE_LEN;
    uwCRC = crc16_range(length, uwStartCRC, (uword *)&gFram_Cal_Data);
    uwChecksum = cks16_range(length, uwChecksum, (uword *)&gFram_Cal_Data);

    if ((uwFram_CRC != uwCRC) || ((uwCRC == 0) && (uwChecksum == 0)))
    {
        fWorkingIsValid = FALSE;
    }
    else
    {
        fWorkingIsValid = TRUE;
    }

    // get the crc and data stored in the protected area of fram

    address = guwFRAM_CAL_Bakup_CRC_Addr;
    num_words = CAL_BACKUP_CRC_LEN;

    ReadFram((uword *)&uwFram_CRC,address,num_words);

    address = guwFRAM_CAL_Backup_Addr;
    num_words = CAL_BACKUP_LEN;

    ReadFram((uword *)&gFram_Cal_Data,address,num_words);
    vServiceWatchdog();

    // now compute crc for what was read from the fram
    length = (ulong)CAL_TABLE_LEN;
    uwCRC = crc16_range(length, uwStartCRC, (uword *)&gFram_Cal_Data);
    uwChecksum = cks16_range(length, uwChecksum, (uword *)&gFram_Cal_Data);

    if (uwFram_CRC != uwCRC)
    {
        fProtectedIsValid = FALSE;
    }
    else
    {
        fProtectedIsValid = TRUE;
    }
    // Get the state from the protected FRAM
    address = guwFRAM_CAL_Backup_State_Addr;
    num_words = CAL_BACKUP_STATE_LEN;

    ReadFram((uword *)&eProtectedState, address, num_words);
    if ((eProtectedState == Cal_Backup_State_Blank) ||
        ((uwCRC == 0) && (uwChecksum == 0)))
    {
        fProtectedIsBlank = TRUE;
    }
    else
    {
        fProtectedIsBlank = FALSE;
    }

    // Now, set the mode of recovery
    // See chart in design doc
    if (fWorkingIsValid && fProtectedIsBlank)
    {
        uwCalMode = 1;
    }
    else if (fWorkingIsValid &&
             (fProtectedIsValid && (eProtectedState == Cal_Backup_State_Written)))
    {
        uwCalMode = 2;
    }
    else if (!fWorkingIsValid &&
             (fProtectedIsValid && (eProtectedState == Cal_Backup_State_Written)))
    {
        uwCalMode = 3;
    }
    else if (!fWorkingIsValid &&
             (!fProtectedIsValid &&
              ((eProtectedState == Cal_Backup_State_Written) ||
               (eProtectedState == Cal_Backup_State_Blank))))
    {
        uwCalMode = 4;
    }
    else if (eProtectedState == Cal_Backup_State_Lockout)
    {
        uwCalMode = 5;
    }
    else
    {
        uwCalMode = 5;
    }

    switch (uwCalMode)
    {
        case 1:
            // get the crc and data stored in the working area of fram
            uwFram_CRC = uwGetFRAM_Cal_CRC();

            vGetFRAM_Cal_Data( &gFram_Cal_Data);
            vServiceWatchdog();
            // try to clone the FRAM region from working to protected
            if (fFramProtect(RANGE0))
            {
                // store the crc and data in the protected area of fram
                address = guwFRAM_CAL_Bakup_CRC_Addr;
                num_words = CAL_BACKUP_CRC_LEN;

                fResult = WriteFram((uword *)&uwFram_CRC,address,num_words, 1);

                if (fResult)
                {
                    address = guwFRAM_CAL_Backup_Addr;
                    num_words = CAL_BACKUP_LEN;

                    fResult = WriteFram((uword *)&gFram_Cal_Data,address,num_words, 1);
                    vServiceWatchdog();
                    if (fResult)
                    {
                        eProtectedState = Cal_Backup_State_Written;
                        // write the state to the protected FRAM
                        address = guwFRAM_CAL_Backup_State_Addr;
                        num_words = CAL_BACKUP_STATE_LEN;

                        fResult = WriteFram((uword *)&eProtectedState, address, num_words, 1);
                        if (fResult)
                        {
                            fFramProtect(RANGE25);  // re-protect the FRAM
                        }
                    }
                }
            }
            break;

        case 2:
            // get the crc and data stored in the working area of fram
            uwFram_CRC = uwGetFRAM_Cal_CRC();

            vGetFRAM_Cal_Data( &gFram_Cal_Data);
            vServiceWatchdog();
            break;

        case 3:
            // clone from protected to working
            address = guwFRAM_CAL_Bakup_CRC_Addr;
            num_words = CAL_BACKUP_CRC_LEN;

            ReadFram((uword *)&uwFram_CRC,address,num_words);

            address = guwFRAM_CAL_Backup_Addr;
            num_words = CAL_BACKUP_LEN;

            ReadFram((uword *)&gFram_Cal_Data,address,num_words);
            vServiceWatchdog();

            vPutFRAM_Cal_CRC(uwFram_CRC);
            vPutFRAM_Cal_Data(&gFram_Cal_Data);
            break;

        case 4:
        case 5:
        default:
            vLoad_Fram_Defaults();         // The fram is corrupt, load defaults
            vPutFRAM_Cal_Data(&gFram_Cal_Data);
            length = (ulong)CAL_TABLE_LEN;
            uwStartCRC = 0xFFFF;
            uwCRC = crc16_range(length,uwStartCRC, (uword *)&gFram_Cal_Data);
            vPutFRAM_Cal_CRC(uwCRC);
            // Mark the protected region as locked
            if (fFramProtect(RANGE0))
            {
                // store the crc and data in the protected area of fram
                address = guwFRAM_CAL_Bakup_CRC_Addr;
                num_words = CAL_BACKUP_CRC_LEN;
                uwCRC = (uword)Cal_Backup_State_Lockout;
                WriteFram((uword *)&uwCRC,address,num_words, 1);

                // write the state to the protected FRAM
                address = guwFRAM_CAL_Backup_State_Addr;
                num_words = CAL_BACKUP_STATE_LEN;
                uwCRC = (uword)Cal_Backup_State_Lockout;

                fResult = WriteFram((uword *)&uwCRC, address, num_words, 1);

                address = guwFRAM_CAL_Backup_Addr;
                num_words = CAL_BACKUP_LEN;
                for (num_words = 0; num_words < CAL_BACKUP_LEN; num_words++)
                {
                    WriteFram((uword *)&uwCRC,address,1, 0);
                    address++;
                }
                vServiceWatchdog();
                fFramProtect(RANGE25);  // re-protect the FRAM
            }
            //gfReportCalibrationTableFault = TRUE;  // mark that the fault needs reported once operational
            break;
    } // end of switch (uwCalMode)

    vGetFRAM_Events(&gLocalEvents);
    guwFRAM_CRC = uwCRC;
    gubEventsUpdated = 0;
    vGetResetCounts(&gulResetCounts);
    gulResetCounts++;
    vPutResetCounts(gulResetCounts);

    // get the setup info

    // get the crc and data stored in the fram
    uwFram_CRC = uwGetFRAM_Setup_CRC();

    vGetFRAM_Setup_Data( &gFram_Setup_Data);

    if (strcmp((const char *)gFram_Setup_Data.szPartNumber, partnumber) == 0)
    {
        fPartNumberOK = TRUE;
    }
    else
    {
        fPartNumberOK = FALSE;
    }

     // Get partnumbers from Boot region of FRAM
    ReturnModulePN((uword *)gszModulePartNumber);
    ReturnHardwarePN((uword *)gszHardwarePartNumber);

    // now compute crc for what was read from the fram
    length = (ulong)SETUP_TABLE_LEN;
    uwStartCRC = 0xFFFF;
    uwChecksum = 0;
    uwCRC = crc16_range(length, uwStartCRC, (uword *)&gFram_Setup_Data);
    uwChecksum = cks16_range(length, uwChecksum, (uword *)&gFram_Setup_Data);

    if ((uwFram_CRC != uwCRC) || ((uwCRC == 0) && (uwChecksum == 0)) || !fPartNumberOK )
    {
        vLoad_Fram_Setup_Defaults();   // The fram setup is corrupt, load defaults

        vPutFRAM_Setup_Data(&gFram_Setup_Data);
        uwCRC = crc16_range(length,uwStartCRC, (uword *)&gFram_Setup_Data);
        vPutFRAM_Setup_CRC(uwCRC);

    }
    gubHourMeterSaveFlag = 0;
    vGetHourMeters(&gHourMeters);
}

/**********************************************************************
 *
 * Function:    GetFRAM_Cal_CRC()
 *
 * Description:
 *
 * Argument(s): None
 *
 * Returns:     CRC from Fram
 *
 **********************************************************************/
uword uwGetFRAM_Cal_CRC(void)
{
    volatile uword address;
    volatile uword num_words;
    volatile uword crc;

    // get the crc and data stored in the fram
    address = CAL_CRC_ADDR;
    num_words = CAL_CRC_LEN;

    ReadFram((uword *)&crc,address,num_words);

    return crc;
}

void vPutFRAM_Cal_CRC(uword CRC)
{
    volatile uword address;
    volatile uword num_words;
    volatile boolean Status;

    address = CAL_CRC_ADDR;
    num_words = CAL_CRC_LEN;

    Status = WriteFram((uword *)&CRC,address,num_words, 1);
    if (Status == FALSE)
    {
       gfHandleEvent(EVNT_FRAM_Failure, TRUE);
    }

}

void vGetFRAM_Cal_Data(volatile FRAM_CAL_TABLE_TYPE * Fram_Data)
{
    volatile uword address;
    volatile uword num_words;

    address = CAL_TABLE_ADDR;
    num_words = CAL_TABLE_LEN;

    ReadFram((uword *)Fram_Data,address,num_words);

}

void vPutFRAM_Cal_Data(volatile FRAM_CAL_TABLE_TYPE * Fram_Data)
{
    volatile uword address;
    volatile uword num_words;
    volatile boolean Status;

    address = CAL_TABLE_ADDR;
    num_words = CAL_TABLE_LEN;

    Status = WriteFram((uword *)Fram_Data, address, num_words, 1);
    if (Status == FALSE)
    {
      gfHandleEvent(EVNT_FRAM_Failure, TRUE);
    }

}



/**********************************************************************
 *
 * Function:    vGetFRAM_Events()
 *
 * Description:  Try to read past events.  On failure, blank the list
 *
 * Argument(s): Pointer to the event structure to read from FRAM
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void vGetFRAM_Events(volatile LOCAL_EVENTS_TYPE *Events)
{
    volatile LOCAL_EVENTS_TYPE buf;
    volatile uword address;
    volatile uword num_words;
    ulong  length;
    uword  uwCRC;
    uword  uwCS;
    uword uwStartCRC;
    uword uwChecksum;

    address = EVENTS_ADDR;
    num_words = EVENTS_LEN;
    uwStartCRC = 0xFFFF;
    uwChecksum = 0x80;  // use non zero to catch all zeros in blank part

    ReadFram((uword *)&buf,address,num_words);
    // now compute crc for what was read from the fram
    length = (ulong)sizeof(EVENT_HISTORY_TYPE);
    uwCRC = crc16_range(length, uwStartCRC, (uword *) &buf.History);
    uwCS = cks16_range(length, uwChecksum, (uword *) &buf.History);
    if ((uwCRC == buf.uwCRC) && (uwCS == buf.uwCS))
    {
        memcpy((void *)Events, (const void *)&buf, sizeof(LOCAL_EVENTS_TYPE));
    }
    else
    {
        memset((void *)Events, 0, sizeof(LOCAL_EVENTS_TYPE));
        vWriteFRAM_Events(Events);
        gfHandleEvent(EVNT_FRAM_CRC_Error_Event_History, TRUE);
    }

}

/**********************************************************************
 *
 * Function:    vWriteFRAM_Events()
 *
 * Description:  Put the Event list in FRAM
 *
 * Argument(s): Pointer to the event structure to write to FRAM
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void vWriteFRAM_Events(volatile LOCAL_EVENTS_TYPE *Events)
{
    boolean fResult;
    volatile uword address;
    volatile uword num_words;
    ulong  length;
    uword uwStartCRC;
    uword uwChecksum;

    address = EVENTS_ADDR;
    num_words = EVENTS_LEN;
    uwStartCRC = 0xFFFF;
    uwChecksum = 0x80;  // use non zero to catch all zeros in blank part

    // now compute crc for the data portion of the packet
    length = (ulong)sizeof(EVENT_HISTORY_TYPE);
    Events->uwCRC = crc16_range(length, uwStartCRC, (uword *) &Events->History);
    Events->uwCS = cks16_range(length, uwChecksum, (uword *) &Events->History);
    fResult = WriteFram((uword *)Events,address,num_words, 1);
    if (fResult == FALSE)
    {
        gfHandleEvent(EVNT_FRAM_Failure, TRUE);
    }
}

void vGetResetCounts(ulong *Value)
{
    volatile uword address;
    volatile uword num_words;
    volatile ulong ulLocalValue = 0L;

    address = RESET_COUNTS_ADDR;
    num_words = RESET_COUNTS_LEN;

    ReadFram((uword *)ulLocalValue, address, num_words);
    *Value = ulLocalValue;

}

void vPutResetCounts(ulong Value)
{
    volatile uword address;
    volatile uword num_words;

    address = RESET_COUNTS_ADDR;
    num_words = RESET_COUNTS_LEN;

    WriteFram((uword *)&Value, address, num_words, 0);
}


/**********************************************************************
 *
 * Function:    GetFRAM_Setup_CRC()
 *
 * Description:
 *
 * Argument(s): None
 *
 * Returns:     CRC from Fram
 *
 **********************************************************************/
uword uwGetFRAM_Setup_CRC(void)
{
    volatile uword address;
    volatile uword num_words;
    volatile uword crc;

    // get the crc and data stored in the fram
    address = SETUP_CRC_ADDR;
    num_words = SETUP_CRC_LEN;

    ReadFram((uword *)&crc,address,num_words);

    return crc;
}

void vPutFRAM_Setup_CRC(uword CRC)
{
    volatile uword address;
    volatile uword num_words;
    volatile boolean Status;

    address = SETUP_CRC_ADDR;
    num_words = SETUP_CRC_LEN;

    Status = WriteFram((uword *)&CRC,address,num_words, 1);
    if (Status == FALSE)
    {
       gfHandleEvent(EVNT_FRAM_Failure, TRUE);
    }

}

void vGetFRAM_Setup_Data(volatile FRAM_SETUP_TABLE_TYPE * Fram_Data)
{
    volatile uword address;
    volatile uword num_words;

    address = SETUP_TABLE_ADDR;
    num_words = SETUP_TABLE_LEN;

    ReadFram((uword *)Fram_Data,address,num_words);

}

void vPutFRAM_Setup_Data(volatile FRAM_SETUP_TABLE_TYPE * Fram_Data)
{
    volatile uword address;
    volatile uword num_words;
    volatile boolean Status;

    address = SETUP_TABLE_ADDR;
    num_words = SETUP_TABLE_LEN;

    Status = WriteFram((uword *)Fram_Data, address, num_words, 1);
    if (Status == FALSE)
    {
      gfHandleEvent(EVNT_FRAM_Failure, TRUE);
    }
}

void vGetHourMeters(volatile LOCAL_HOUR_METER_TYPE *Meters)
{
    volatile LOCAL_HOUR_METER_TYPE buf;
    volatile uword address;
    volatile uword num_words;
    ulong  length;
    uword  uwCRC;
    uword  uwCS;
    uword uwStartCRC;
    uword uwChecksum;

    address = HOUR_METERS_ADDR;
    num_words = HOUR_METERS_LEN;
    uwStartCRC = 0xFFFF;
    uwChecksum = 0x80;  // use non zero to catch all zeros in blank part

    ReadFram((uword *)&buf,address,num_words);
    // now compute crc for what was read from the fram
    length = (ulong)sizeof(HOUR_METER_TYPE);
    uwCRC = crc16_range(length, uwStartCRC, (uword *) &buf.HourMeters);
    uwCS = cks16_range(length, uwChecksum, (uword *) &buf.HourMeters);
    if ((uwCRC == buf.uwCRC) && (uwCS == buf.uwCS))
    {
        memcpy((void *)Meters, (const void *)&buf, sizeof(LOCAL_HOUR_METER_TYPE));
        // pull in the copy of any saved copy of corrupted values
        address = SAVED_HOUR_METERS_ADDR;
        num_words = SAVED_HOUR_METERS_LEN;
        ReadFram((uword *)&gCorruptedHourMeters, address, num_words);
    }
    else
    {
        address = SAVED_HOUR_METERS_ADDR;
        num_words = SAVED_HOUR_METERS_LEN;
        WriteFram((uword *)&buf, address, num_words, 0);    // save what was read
        memcpy(&gCorruptedHourMeters, (const void *)&buf, sizeof(LOCAL_HOUR_METER_TYPE));
        memset((void *)Meters, 0, sizeof(LOCAL_HOUR_METER_TYPE));
        vWriteHourMeters(Meters);
        gfHandleEvent(EVNT_HOUR_METERS_CORRUPT, TRUE);
    }

}

void vWriteHourMeters(volatile LOCAL_HOUR_METER_TYPE *Meters)
{
    volatile uword address;
    volatile uword num_words;
    volatile boolean Status;
    ulong  length;
    uword uwStartCRC;
    uword uwChecksum;

    address = HOUR_METERS_ADDR;
    num_words = HOUR_METERS_LEN;
    uwStartCRC = 0xFFFF;
    uwChecksum = 0x80;  // use non zero to catch all zeros in blank part

    // now compute crc for the data portion of the packet
    length = (ulong)sizeof(HOUR_METER_TYPE);
    Meters->uwCRC = crc16_range(length, uwStartCRC, (uword *) &Meters->HourMeters);
    Meters->uwCS = cks16_range(length, uwChecksum, (uword *) &Meters->HourMeters);

    Status = WriteFram((uword *)Meters, address, num_words, 1);
    if (Status == FALSE)
    {
      gfHandleEvent(EVNT_FRAM_Failure, TRUE);
    }
}

/**********************************************************************
 *
 * Function:    vClearCalBackup()
 *
 * Description: Erases top part of data structure for protected cal backup
 *
 * Argument(s): None
 *
 * Returns:     N/A
 *
 **********************************************************************/
void vClearCalBackup(void)
{
    volatile uword address;
    volatile uword num_words;
    volatile boolean Status;
    uword uwData[] = {0x0000, 0x0000, 0x0000, 0x0000};

    if (!fFramProtect(RANGE0))
    {
        fFramProtect(RANGE0);
    }
    address = guwFRAM_CAL_Backup_State_Addr;
    num_words = 4;
    WriteFram(uwData, address, num_words, FALSE);
    fFramProtect(RANGE25);

}

/**********************************************************************
 *
 * Function:    vCloneCalibration()
 *
 * Description: Copys the working cal data to the backup region.
 *
 * Argument(s): None
 *
 * Returns:     N/A
 *
 **********************************************************************/
void vCloneCalibration(void)
{
    volatile uword uwFram_CRC;
    volatile uword uwCRC;
    volatile ulong length;
    volatile uword uwChecksum = 0;
    volatile CAL_BACKUP_STATES eProtectedState;
    volatile uword uwCalMode;
    volatile boolean fResult;
    volatile uword address;
    volatile uword num_words;
    volatile boolean Status;

    // get the crc and data stored in the working area of fram
    uwFram_CRC = uwGetFRAM_Cal_CRC();

    vGetFRAM_Cal_Data( &gFram_Cal_Data);
    vServiceWatchdog();
    // try to clone the FRAM region from working to protected
    if (fFramProtect(RANGE0))
    {
        // store the crc and data in the protected area of fram
        address = guwFRAM_CAL_Bakup_CRC_Addr;
        num_words = CAL_BACKUP_CRC_LEN;

        fResult = WriteFram((uword *)&uwFram_CRC,address,num_words, 1);

        if (fResult)
        {
            address = guwFRAM_CAL_Backup_Addr;
            num_words = CAL_BACKUP_LEN;

            fResult = WriteFram((uword *)&gFram_Cal_Data,address,num_words, 1);
            vServiceWatchdog();
            if (fResult)
            {
                eProtectedState = Cal_Backup_State_Written;
                // write the state to the protected FRAM
                address = guwFRAM_CAL_Backup_State_Addr;
                num_words = CAL_BACKUP_STATE_LEN;

                fResult = WriteFram((uword *)&eProtectedState, address, num_words, 1);
                if (fResult)
                {
                    fFramProtect(RANGE25);  // re-protect the FRAM
                }
            }
        }
    }
}

