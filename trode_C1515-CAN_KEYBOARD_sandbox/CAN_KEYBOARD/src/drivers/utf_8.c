/************************************************************************************************
 *  File:       utf_8.c
 *
 *  Purpose:    Defines the data structures and variables exchanged via CANopen
 *
 *  Project:    C1515
 *
 *  Copyright:  2014 Crown Equipment Corp., New Bremen, OH  45869
 *
 *  Author:     Tom Rode
 *
 *  Revision History:
 *              
 ************************************************************************************************/

#include "param.h"
#include "types.h"
#include "InputSwitches.h"
#include "Truck_PDO.h"
#include "tmapping.h"
#include "rmapping.h"
#include "fram.h"
#include "ramtron.h"
#include "gpio.h"
#include "IO_interface.h"
#include "util.h"
#include "utf_8.h"


/*========================================================================*
 *  SECTION - extern global variabbles                                    *
 *========================================================================*
 */

/** @brief this will hold the status for the currently language, where to index into and the special function keys  
 */
uint16_t gUwRowColStat; 

/** @brief this variable flag will Not Allow CAN to transmit
*/ 
uint8_t ubAllowTx;

/*========================================================================*
 *  SECTION - Local variables                                             *
 *========================================================================*
 */

/** @brief this type is the check for in bounds indexing into CAN message parsing
 */
static uint8_t ubFlatIndexCheck;

/** @brief This pointer points to a specifc place from desired hardcoded table for indexing purposes for CAN Tx
 */
static const uint32_t *pLangTblPtr;

/** @brief De-bounced switch key value for a given row  
 */
static uint16_t uwColumnSwValRaw[6u];

/** @brief The signal are de-bounced, not shorted or stuck,will have switch position with in the row and send a hi to low transition
 */
volatile column_sw_val_cond_t column_sw_val_cond[6u];

/** @brief This is the variable that will contain a rows de-bounced value 
 */
volatile Key_Switches_t gKey_Switches;

/** @brief Transmit CAN Pdo. 
 */
volatile keyboard_data_t TpdoCanTableOutput;

/** @brief Receive CAN Pdo
 */
volatile keyboard_data_rcv_t gRpdoCanLanguageSelectInput;

/** @brief this type is the control for indexing into hardcoded table
 */
static RowColumnIndexControl_t RwColIndxCnt;



/*========================================================================*
 *  SECTION - Local functions                                             *
 *========================================================================*
 */
uint32_t RowTableIndx(uint8_t RowSwitch, uint8_t Row);
void CANTXMsg(uint32_t ulIndxVal);
 /*========================================================================*
  *  SECTION - extern global functions                                     *
  *========================================================================*/

/**
 * @fn       HwKeyDecodeInit
 *
 * @brief    This function Initializes Hardware decode manager at start up.
 *
 * @param    None
 *
 * @return   None
 *
 * @note
 *
 * @author  Tom Rode
 *
 */
void gHwKeyDecodeInit(void)
{
    static uint8_t FramRead;

    /** ###Functional overview: */
   
    /** - FRAM read for stored language */
    ReadFram((uword *)&FramRead, IMM_LANGUAGE_ADDR, 1u);
 
     /** - Place stored language into global status */
    gUwRowColStat = mSET_LANGUAGE(gUwRowColStat, (uint8_t *)&FramRead);     
     
}

/**
 * @fn       HwKeyDecode(void)
 *
 * @brief    This function Decodes the Hardware key buttons, and indexes into language table
 *
 * @param    None
 *
 * @return   None
 *
 * @note    Called every 5ms
 *
 * @author  Tom Rode
 *
 */
void gScanKeyDecodeManager(void)
{
    uint8_t uwRowColInxd;  
    static uint8_t uwRowColStat;     
    static uint32_t CANinIdx;
   
    /** ###Functional overview: */
    
    /** - index correction */ 
    if( 0u == uwRowColStat)
    {
        /** - No  Overflow allowed */
        uwRowColInxd = 0u; 
    }
    else
    {
        /** - index corrected */
        uwRowColInxd = (uwRowColStat - 1u); 
    }
    /** - De-bounced switches in the respective row that was toggled */
    uwColumnSwValRaw[uwRowColInxd] = gKey_Switches.uwSwitches[uwRowColInxd];
        
    /** - Test for Stuck or Shorted Switch */
    column_sw_val_cond[uwRowColInxd].uwColumnSwRwSht = uwMultiSwitchPress(uwColumnSwValRaw[uwRowColInxd], uwRowColInxd);
        
    /** - Get switch position in the row */
    column_sw_val_cond[uwRowColInxd].ubColumnSwRwPos = ubGetRowSW(column_sw_val_cond[uwRowColInxd].uwColumnSwRwSht); 
	
	/** - Adding delay due to Space bar issue*/
	gvDelay(DECODE_1uS_DELAY);
	
    /** - Output on a rising to falling transition or on Rising edge using a parameter value*/
    column_sw_val_cond[uwRowColInxd].ubColumnSwRwFal = ubKeyDepressed(column_sw_val_cond[uwRowColInxd].ubColumnSwRwPos, uwRowColInxd); 
    
    /** - Index into table */
    CANinIdx = RowTableIndx(column_sw_val_cond[uwRowColInxd].ubColumnSwRwFal, uwRowColInxd); 

    /** - Send out CANTx PDO */
    CANTXMsg(CANinIdx);
    
        
    /** - Electrical Row Excitation Section */
    /** - Enable Row 1 */
    if( 0u == uwRowColStat )        
    {
        /* Disable Row six Not assigned */
        //ROW6_DRV_CLEAR = TRUE;
            
        /** - Enable Row 1 abstracted output */
        mROW1_DRV(GPIO_STATE_HIGH); 

        /** - Set the row status */
        gUwRowColStat = mSET_ROW(gUwRowColStat,(uwRowColStat + 1u));  
    }
    /** - Enable Row 2 */
    else if( 1u == uwRowColStat )
    {
        /** - Disable Row 1 abstracted output */
        mROW1_DRV(GPIO_STATE_LOW);   
    
        /** - Enable Row 2 abstracted output */
        mROW2_DRV(GPIO_STATE_HIGH);  

        /** - Set the row status */
        gUwRowColStat = mSET_ROW(gUwRowColStat,(uwRowColStat + 1u)); 
    }
    /** - Enable Row 3 */
    else if( 2u == uwRowColStat )
    {
        /** - Disable Row 2 abstracted output */
        mROW2_DRV(GPIO_STATE_LOW); 

        /** - Enable Row 3 abstracted output */
        mROW3_DRV(GPIO_STATE_HIGH);     

        /** - Set the row status */
        gUwRowColStat = mSET_ROW(gUwRowColStat,(uwRowColStat + 1u)); 
    }
    /** - Enable Row 4 */ 
    else if( 3u == uwRowColStat )
    {
        /** - Disable Row 3 abstracted output */
        mROW3_DRV(GPIO_STATE_LOW);      

        /** - Enable Row 4 abstracted output */
        mROW4_DRV(GPIO_STATE_HIGH);

        /** - Set the row status */
        gUwRowColStat = mSET_ROW(gUwRowColStat,(uwRowColStat + 1u)); 
    }
    /** - Enable Row 5 */
    else if( 4u == uwRowColStat )
    {    
        /** - Disable Row 4 abstracted output */
        mROW4_DRV(GPIO_STATE_LOW);      
    
        /** - Enable Row 5 abstracted output */
        mROW5_DRV(GPIO_STATE_HIGH);

        /** - Set the row status */
        gUwRowColStat = mSET_ROW(gUwRowColStat,(uwRowColStat + 1u));    
    }
    /** - Enable Row 6 */   
    else if( 5u == uwRowColStat )
    {
        /** - Disable Row 5 abstracted output */
        mROW5_DRV(GPIO_STATE_LOW); 
        
        /** - Enable Row six not assigned */
        //ROW6_DRV = TRUE;
    }
    else
    {   
        /** - Do Nothing*/  
    }    
        
    /** - Cycle through up to what parameter max value */
    if( uwRowColStat == gParams.Kt.ubRows )
    {
        /** - Set the global row status */
        gUwRowColStat = mSET_ROW(gUwRowColStat,0); 
        
        /** - Reset the row column status*/
        uwRowColStat = 0u;  
        
        /** - Enable Row one restart trigger cycle*/
        /** - Enable abstracted Row 1 output */
        mROW1_DRV(GPIO_STATE_HIGH);     
        
        /** - Disable Row 2 abstracted output */
        mROW2_DRV(GPIO_STATE_LOW); 
                
        /** - Disable Row 3 abstracted output */
        mROW3_DRV(GPIO_STATE_LOW);       

        /** - Disable Row 4 abstracted output */
        mROW4_DRV(GPIO_STATE_LOW);        

        /** - Disable Row 5 abstracted output */
        mROW5_DRV(GPIO_STATE_LOW);  

        /* Disable Row six*/
        //ROW6_DRV_CLEAR = TRUE;     
    }
    else
    {
        /** - Increment overall status*/
        uwRowColStat++;
    }           
}

/**
 * @fn       RowTableIndx(Uint8 Rowswitch, uint8 Row )
 *
 * @brief    This function takes in the conditioned switch within the row 
 *
 * @param    Row number
 *
 * @return   32bit key-pad switch derived table value
 *
 * @note     This will be called every 5 ms, If there is a valid value then index into language table 
 *
 * @author  Tom Rode
 *
 */
uint32_t RowTableIndx(uint8_t RowSwitch, uint8_t Row)
{
    uint8_t i;
    uint8_t ubFlatIndex; 
    static uint8_t ubFlatIndexCurr, ubFlatIndexOld;
    uint32_t ulCanOut;
    const uint32_t ulNull = 0u;
  
    /** ###Functional overview: */

    /** - Set the index initially zero*/
    ubFlatIndex = 0u;
    
    /** - Which row state is it in */
    /** - This is the Row read for Row 1 */ 
    if(Row == ROW1_DRV_ST)
    {    
        /** - Update the last value */
        RwColIndxCnt.ubNewBtnPrsdRw1 = RowSwitch;              
        
        /** - If there is a switch pressed while in this row */        
        if( RwColIndxCnt.ubNewBtnPrsdRw1 != RwColIndxCnt.ubOldBtnPrsdRw1 ) 
        {   
            /** - Which Row */
            RwColIndxCnt.ubRowIndex = 1u;

            /** - Which column */   
            RwColIndxCnt.ubColumnIndex = RwColIndxCnt.ubOldBtnPrsdRw1;

            /** - Update old value */
            RwColIndxCnt.ubOldBtnPrsdRw1 = RwColIndxCnt.ubNewBtnPrsdRw1;
        }  
    }

    /** - ADDED This is the Row read for Row 2 */
    else if(Row == ROW2_DRV_ST)
    {
        /** - Update the last value */
        RwColIndxCnt.ubNewBtnPrsdRw2 = RowSwitch;
           
        /** - If there is a switch pressed while in this row */        
        if( RwColIndxCnt.ubNewBtnPrsdRw2 != RwColIndxCnt.ubOldBtnPrsdRw2 ) 
        {           
            /** - Which Row */
            RwColIndxCnt.ubRowIndex = 2u;

            /** - Which column */   
            RwColIndxCnt.ubColumnIndex = RwColIndxCnt.ubOldBtnPrsdRw2;
 
            /** - Update old value */
            RwColIndxCnt.ubOldBtnPrsdRw2 = RwColIndxCnt.ubNewBtnPrsdRw2;
        }
    }

    /** - This is the Row read for Row 3 */
    else if(Row == ROW3_DRV_ST)
    {
        /** - Update the last value */
        RwColIndxCnt.ubNewBtnPrsdRw3 = RowSwitch;
           
        /** - If there is a switch pressed while in this row */        
        if( RwColIndxCnt.ubNewBtnPrsdRw3 != RwColIndxCnt.ubOldBtnPrsdRw3 ) 
        {           
            /** - Which Row */
            RwColIndxCnt.ubRowIndex = 3u;

            /** - Which column */   
            RwColIndxCnt.ubColumnIndex = RwColIndxCnt.ubOldBtnPrsdRw3;
 
            /** - Update old value */
            RwColIndxCnt.ubOldBtnPrsdRw3 = RwColIndxCnt.ubNewBtnPrsdRw3;
                
        }  
    }

    /** - This is the Row read for Row 4 */
    else if(Row == ROW4_DRV_ST)
    {
        /** - Update the last value */
        RwColIndxCnt.ubNewBtnPrsdRw4 = RowSwitch;              
        
        /** - If there is a switch pressed while in this row */        
        if( RwColIndxCnt.ubNewBtnPrsdRw4 != RwColIndxCnt.ubOldBtnPrsdRw4 ) 
        {
            /** - Which Row */
            RwColIndxCnt.ubRowIndex = 4u;

            /** - Which column */   
            RwColIndxCnt.ubColumnIndex = RwColIndxCnt.ubOldBtnPrsdRw4;

            /** - Update old value */
            RwColIndxCnt.ubOldBtnPrsdRw4 = RwColIndxCnt.ubNewBtnPrsdRw4;
        }     
    }

    /** - This is the Row read for Row 5 */
    else if(Row == ROW5_DRV_ST)
    { 
        /** - Update the last value */
        RwColIndxCnt.ubNewBtnPrsdRw5 = RowSwitch;              
        
        /** - If there is a switch pressed while in this row */        
        if( RwColIndxCnt.ubNewBtnPrsdRw5 != RwColIndxCnt.ubOldBtnPrsdRw5 ) 
        {   
            /** - Which Row */
            RwColIndxCnt.ubRowIndex = 5u;

            /** - Which column */   
            RwColIndxCnt.ubColumnIndex = RwColIndxCnt.ubOldBtnPrsdRw5;

            /** - Update old value */
            RwColIndxCnt.ubOldBtnPrsdRw5 = RwColIndxCnt.ubNewBtnPrsdRw5;
        }                  
    }

    /** - This is the Row read for Row 6*/
    else if( Row == ROW6_DRV_ST )
    {      
        /** - Update the last value */
        RwColIndxCnt.ubNewBtnPrsdRw6 = RowSwitch;              
        
        /** - If there is a switch pressed while in this row */        
        if( RwColIndxCnt.ubNewBtnPrsdRw6 != RwColIndxCnt.ubOldBtnPrsdRw6 ) 
        {
            /** - Which Row */
            RwColIndxCnt.ubRowIndex = 6u;

            /** - Which column */   
            RwColIndxCnt.ubColumnIndex = RwColIndxCnt.ubOldBtnPrsdRw6;

            /** - Update old value */
            RwColIndxCnt.ubOldBtnPrsdRw6 = RwColIndxCnt.ubNewBtnPrsdRw6;
        }  
    }

    /** - Updated indexing according to design review 15 Dec 2014 */
    /** - 1. Multiplier offset = current column switch  * Row parameter  */
    /** - 2. Column Subtractor = Multiplier offset - Row parameter */
    /** - 3. Desired Character = Column subtractor + Current Row */
    /** - 4. Index correction Desired Character - 1 to index correct */

    /** - Flat index simplified expression */ 
    ubFlatIndex = ( ((RwColIndxCnt.ubColumnIndex - 1u) * (gParams.Kt.ubRows) ) + (RwColIndxCnt.ubRowIndex - 1u) );

    /** - Make language table pointer point first byte on correct table */
    /** - Keys switches read on a release parameter*/
    if( TRUE != gParams.Kt.ubCharOnPressed) 
    {      
        /** - Check to see if Hardware test */ 
        if( SWITCH_TEST == gRpdoCanLanguageSelectInput.ubData ) 
        {
            /** - Table Hardware switch test Picked from Tester */
            pLangTblPtr = &gParams.HwSwChk.ulHardwareSwCheckTable[0];     
        }  
        else
        {       
            /** - SHIFT TEST Read in the Rising edge of Switch 10 or 30 */
            if(( TRUE == BitFieldRwSw[4].sw2 ) || ( TRUE == BitFieldRwSw[4].sw8 )) 
            {     
                /**  - CAN Message for language selection in Range*/
                if(( NINTH_LANG >= gRpdoCanLanguageSelectInput.ubData ) && ( DEFAULT_LANG != gRpdoCanLanguageSelectInput.ubData ) 
                && ( gParams.Kt.ubLangTablesCnt >= gRpdoCanLanguageSelectInput.ubData ))
                {
                    /** - Upper case tables */
                    if( ENGLISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    { 
                        /** - Table set one Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[SHIFT][0];           
                    }
                    else if ( GERMAN_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set two Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulGermanTables[SHIFT][0];
                    }
                    else if ( SPANISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    { 
                        /** - Table set three Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulSpanishTables[SHIFT][0];
                    }
                    else if ( FOURTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set four Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang4Tables[SHIFT][0];     
                    }
                    else if ( FIFTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set five Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang5Tables[SHIFT][0];     
                    }
                    else if ( SIXTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Six Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang6Tables[SHIFT][0];     
                    }
                    else if ( SEVENTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Seventh Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang7Tables[SHIFT][0];     
                    }
                    else if ( EIGHTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Eigth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang8Tables[SHIFT][0];     
                    }
                    else if ( NINTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Ninth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang9Tables[SHIFT][0];     
                    }
                } 
                /** - CAN message out of range */ 
                else
                {  
                    /** - Default Table set */
                    pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[SHIFT][0];           
                } 
            }  
            /** - ALT TEST Read in the Rising edge of Switch 3 or 7 */
            /** - ALT table */
            else if(( TRUE == BitFieldRwSw[4].sw3 ) || ( TRUE == BitFieldRwSw[4].sw7 ))
            {
                /**  - CAN Message for language selection in Range*/
                if(( NINTH_LANG >= gRpdoCanLanguageSelectInput.ubData ) && ( DEFAULT_LANG != gRpdoCanLanguageSelectInput.ubData )
                && ( gParams.Kt.ubLangTablesCnt >= gRpdoCanLanguageSelectInput.ubData ))
                {
                    if( ENGLISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {  
                        /** - Table set one Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[ALT][0]; 
                    } 
                    else if( GERMAN_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                       /** - Table set two Picked from IMM */
                       pLangTblPtr = &gParams.LangTableAr.ulGermanTables[ALT][0];
                    }
                    else if( SPANISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set three Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulSpanishTables[ALT][0];
                    }
                    else if ( FOURTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set four Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang4Tables[ALT][0];    
                    }
                    else if ( FIFTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set five Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang5Tables[ALT][0];    
                    }
                    else if ( SIXTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Six Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang6Tables[ALT][0];    
                    }
                    else if ( SEVENTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Seventh Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang7Tables[ALT][0];    
                    }
                    else if ( EIGHTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Eigth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang8Tables[ALT][0];    
                    }
                    else if ( NINTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Ninth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang9Tables[ALT][0];    
                    }
                }
                /** - CAN message out of range */ 
                else
                {  
                    /** - Default Table set */
                    pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[ALT][0];                 
                }
            }
            /** - FUNCTION TEST Read in the Rising edge of Switch 9 */
            else if( TRUE == BitFieldRwSw[4].sw9 ) 
            {
                /**  - CAN Message for language selection in Range*/
                if(( NINTH_LANG >= gRpdoCanLanguageSelectInput.ubData ) && ( DEFAULT_LANG != gRpdoCanLanguageSelectInput.ubData )
                && ( gParams.Kt.ubLangTablesCnt >= gRpdoCanLanguageSelectInput.ubData )) 
                {
                    /** - FUNCTION table */
                    if( ENGLISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {  
                        /** - Table set one Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[FN][0];  
                    }
                    else if( GERMAN_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set two Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulGermanTables[FN][0];
                    }
                    else if( SPANISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set three Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulSpanishTables[FN][0];
                    }
                    else if ( FOURTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set four Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang4Tables[FN][0];     
                    }
                    else if ( FIFTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set five Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang5Tables[FN][0];     
                    }
                    else if ( SIXTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Six Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang6Tables[FN][0];     
                    }
                    else if ( SEVENTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Seventh Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang7Tables[FN][0];     
                    }
                    else if ( EIGHTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Eigth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang8Tables[FN][0];     
                    }
                    else if ( NINTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Ninth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang9Tables[FN][0];     
                    }
                }
                /** - CAN message out of range */ 
                else
                {  
                    /** - Default Table set */
                    pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[FN][0];                 
                }
            }  
            /** - No SHIFT or ALT or FUNCTION keys Pressed */
            else 
            {
                /**  - CAN Message for language selection in Range*/
                if(( NINTH_LANG >= gRpdoCanLanguageSelectInput.ubData ) && ( DEFAULT_LANG != gRpdoCanLanguageSelectInput.ubData )
                && ( gParams.Kt.ubLangTablesCnt >= gRpdoCanLanguageSelectInput.ubData ))
                { 
                    /** - lower case table */
                    if( ENGLISH_LANG == gRpdoCanLanguageSelectInput.ubData ) 
                    {     
                        /** - Table set one Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[STANDARD][0]; 
                    } 
                    else if( GERMAN_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set two Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulGermanTables[STANDARD][0];
                    }  
                    else if( SPANISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set three Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulSpanishTables[STANDARD][0];
                    }
                    else if ( FOURTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set four Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang4Tables[STANDARD][0];      
                    }
                    else if ( FIFTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set fifth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang5Tables[STANDARD][0];      
                    }
                    else if ( SIXTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Six Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang6Tables[STANDARD][0];     
                    }
                    else if ( SEVENTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Seventh Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang7Tables[STANDARD][0];      
                    }
                    else if ( EIGHTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Eigth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang8Tables[STANDARD][0];      
                    }
                    else if ( NINTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Ninth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang9Tables[STANDARD][0];      
                    }
               }
                   /** - CAN message out of range */ 
                   else
                   {  
                       /** - Default Table set */
                       pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[STANDARD][0];                 
                   }
             }     
         }
         /** - Scan through table*/
         for(i = 0u; i < ubFlatIndex; i++)
         {
              /** - move the pointer to specified index */
              pLangTblPtr++;
         } 
    }     
    /** - Keys switches read on a PRESSED parameter*/
    else if( TRUE == gParams.Kt.ubCharOnPressed) 
    {
        /** - Get most current index value */ 
        ubFlatIndexCurr = ubFlatIndex;
        
        /** - Check to see if Hardware test */   
        if( SWITCH_TEST == gRpdoCanLanguageSelectInput.ubData ) 
        {
            /** - Table Hardware switch test Picked from tester */
            pLangTblPtr = &gParams.HwSwChk.ulHardwareSwCheckTable[0];     
        }    
        else
        {           
            /** - All SHIFT buttons TRUE and a difference in index occurred */
            if( (( TRUE == BitFieldRwSw[4].sw2 ) || ( TRUE == BitFieldRwSw[4].sw8 )) && (ubFlatIndexCurr != ubFlatIndexOld ))
            {
                /**  - CAN Message for language selection in Range*/
                if(( NINTH_LANG >= gRpdoCanLanguageSelectInput.ubData ) && ( DEFAULT_LANG != gRpdoCanLanguageSelectInput.ubData )
                && ( gParams.Kt.ubLangTablesCnt >= gRpdoCanLanguageSelectInput.ubData )) 
                { 
                    /** - Upper case tables */
                    if( ENGLISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {                         
                        /** - Table set one Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[SHIFT][0];                                         
                    }
                    else if ( GERMAN_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set two Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulGermanTables[SHIFT][0];
                    }
                    else if ( SPANISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set three Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulSpanishTables[SHIFT][0];
                    }
                    else if ( FOURTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set four Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang4Tables[SHIFT][0];    
                    }
                    else if ( FIFTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set fifth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang5Tables[SHIFT][0];    
                    }
                    else if ( SIXTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Six Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang6Tables[SHIFT][0];    
                    }
                    else if ( SEVENTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Seventh Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang7Tables[SHIFT][0];    
                    }
                    else if ( EIGHTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Eigth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang8Tables[SHIFT][0];    
                    }
                    else if ( NINTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Ninth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang9Tables[SHIFT][0];    
                    }
                }    
                /** - CAN message out of range */ 
                else
                {  
                    /** - Default Table set */
                    pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[SHIFT][0];                 
                }
            } 
            /** - ALT TEST Read in the Rising edge of Switch 3 or 7 and a difference in index occured */
            else if( (( TRUE == BitFieldRwSw[4].sw3 ) || ( TRUE == BitFieldRwSw[4].sw7 )) && (ubFlatIndexCurr != ubFlatIndexOld ))
            {
                /**  - CAN Message for language selection in Range*/
                if(( NINTH_LANG >= gRpdoCanLanguageSelectInput.ubData ) && ( DEFAULT_LANG != gRpdoCanLanguageSelectInput.ubData )
                && ( gParams.Kt.ubLangTablesCnt >= gRpdoCanLanguageSelectInput.ubData ))
                { 
                    /** - ALT table */
                    if( ENGLISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {                     
                        /** - Table set one Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[ALT][0]; 
                    } 
                    else if( GERMAN_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {   
                        /** - Table set two Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulGermanTables[ALT][0];
                    }
                    else if( SPANISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set three Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulSpanishTables[ALT][0];
                    }
                    else if ( FOURTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set four Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang4Tables[ALT][0];     
                    }
                    else if ( FIFTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set fifth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang5Tables[ALT][0];     
                    }
                    else if ( SIXTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Six Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang6Tables[ALT][0];     
                    }    
                    else if ( SEVENTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set seventh Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang7Tables[ALT][0];     
                    }    
                    else if ( EIGHTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Eigth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang8Tables[ALT][0];     
                    }
                    else if ( NINTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Ninth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang9Tables[ALT][0];     
                    }
                }
                /** - CAN message out of range */ 
                else
                {  
                    /** - Default Table set */
                    pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[ALT][0];                 
                }
            }
            /** - FUNCTION TEST Read in the Rising edge of Switch 9 and a difference in index occured */
            else if (( TRUE == BitFieldRwSw[4].sw9 ) && (ubFlatIndexCurr != ubFlatIndexOld ))
            {
                /**  - CAN Message for language selection in Range*/
                if(( NINTH_LANG >= gRpdoCanLanguageSelectInput.ubData ) && ( DEFAULT_LANG != gRpdoCanLanguageSelectInput.ubData )
                && ( gParams.Kt.ubLangTablesCnt >= gRpdoCanLanguageSelectInput.ubData ))  
                {
                    /** - FUNCTION table */
                    if( ENGLISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {                     
                        /** - Table set one Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[FN][0];  
                    }
                    else if( GERMAN_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set two Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulGermanTables[FN][0];
                    }
                    else if( SPANISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set three Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulSpanishTables[FN][0];
                    }
                    else if ( FOURTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set four Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang4Tables[FN][0];       
                    }
                    else if ( FIFTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set fifth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang5Tables[FN][0];       
                    }
                    else if ( SIXTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Six Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang6Tables[FN][0];       
                    }
                    else if ( SEVENTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Seven Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang7Tables[FN][0];       
                    }
                    else if ( EIGHTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Eigth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang8Tables[FN][0];       
                    }
                    else if ( NINTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Ninth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang9Tables[FN][0];       
                    }
                }
                /** - CAN message out of range */ 
                else
                {  
                    /** - Default Table set */
                    pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[FN][0];                 
                }
            }  
            /** - Lower case, No SHIFT still need ALT and FN and a difference index changed */   
            else if ( (( TRUE != BitFieldRwSw[4].sw2 ) || ( TRUE != BitFieldRwSw[4].sw8 ) || 
                       ( TRUE != BitFieldRwSw[4].sw3 ) || ( TRUE != BitFieldRwSw[4].sw7 ) ||
                       ( TRUE != BitFieldRwSw[4].sw9 )) && (ubFlatIndexCurr != ubFlatIndexOld ))
            {
                /**  - CAN Message for language selection in Range*/
                if(( NINTH_LANG >= gRpdoCanLanguageSelectInput.ubData ) && ( DEFAULT_LANG != gRpdoCanLanguageSelectInput.ubData )
                && ( gParams.Kt.ubLangTablesCnt >= gRpdoCanLanguageSelectInput.ubData ))
                {
                    /** - lower case table */
                    if( ENGLISH_LANG == gRpdoCanLanguageSelectInput.ubData ) 
                    {   
                        /** - Table set one Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[STANDARD][0];
                    }     
                    else if( GERMAN_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set two Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulGermanTables[STANDARD][0];
                    }  
                    else if( SPANISH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set three Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulSpanishTables[STANDARD][0];
                    }  
                    else if ( FOURTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set four Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang4Tables[STANDARD][0];    
                    }
                    else if ( FIFTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set fifth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang5Tables[STANDARD][0];    
                    }
                    else if ( SIXTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Six Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang6Tables[STANDARD][0];    
                    }
                    else if ( SEVENTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Seven Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang7Tables[STANDARD][0];    
                    }
                    else if ( EIGHTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Eigth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang8Tables[STANDARD][0];    
                    }
                    else if ( NINTH_LANG == gRpdoCanLanguageSelectInput.ubData )
                    {
                        /** - Table set Ninth Picked from IMM */
                        pLangTblPtr = &gParams.LangTableAr.ulLang9Tables[STANDARD][0];    
                    }
                }
                /** - CAN message out of range */ 
                else
                {  
                    /** - Default Table set */
					pLangTblPtr = &gParams.LangTableAr.ulEnglishTables[STANDARD][0];
                }
            }
              /** No character */ 
              else
              {
                   /** - zero out index NEVER cycle through */          
                   ubFlatIndex = 0;          
 
                   /** - Make pointer null*/
                   pLangTblPtr = &ulNull;   
              }
        }

        /** Scan through table until index reached */
        for(i = 0u; i < ubFlatIndex; i++)
        {
            /** - move the pointer to specified index */
            pLangTblPtr++;
        }
            
        /** - maintain index */
        ubFlatIndexOld = ubFlatIndexCurr;     
    }  

    /** - FOR CAN RESET (FUNCTION+ALT+SHIFT) SHIFT TEST Read in the Rising edge of Switch 10 or 30 */
    if(( TRUE == BitFieldRwSw[4].sw2 ) || ( TRUE == BitFieldRwSw[4].sw8 ))
    {  
        /**-  CAN Status  */
        gUwRowColStat = mSET_SHIFT(gUwRowColStat,TRUE);
    }
    else
    {
        /**-  CAN Status  */  
        gUwRowColStat = mSET_SHIFT(gUwRowColStat,FALSE);
    }

    /** - ALT TEST Read in the Rising edge of Switch 3 or 7 */
    if(( TRUE == BitFieldRwSw[4].sw3 ) || ( TRUE == BitFieldRwSw[4].sw7 ))
    {    
        /**-  CAN Status  */
        gUwRowColStat = mSET_ALTERNATE(gUwRowColStat,TRUE);
    }
    else
    {
        /**-  CAN Status  */
        gUwRowColStat = mSET_ALTERNATE(gUwRowColStat,FALSE);
    }

    /** - FUNCTION TEST Read in the Rising edge of Switch 9 */
    if( TRUE == BitFieldRwSw[4].sw9 ) 
    {       
        /**-  CAN Status  */
        gUwRowColStat = mSET_FUNCTION(gUwRowColStat,TRUE);
    }  
    else
    {
        /**-  CAN Status  */
        gUwRowColStat = mSET_FUNCTION(gUwRowColStat,FALSE);
    }

    /** - Test if flat index in bounds or not due to issue with special function key and space bar*/
    if( ubFlatIndex <= (gParams.Kt.ubRows * gParams.Kt.ubColumns))
    {    
        /** - pointer is pointing correct*/
        ulCanOut = *pLangTblPtr;               
    }
    else 
    {
        /** - pointer is out of bounds this iteration*/
        ulCanOut = 0;   
    }
    
    /** - This value used in CAN parsing as a filter */
    ubFlatIndex = ubFlatIndexCheck;
    
    /** - set the indexing to zero*/
    ubFlatIndex = 0;     

    /** - Make pointer null*/
    pLangTblPtr = &ulNull; 
                          
    /** - return correctly indexed value for CAN transmitting */
    return ulCanOut;
}

/**
 * @fn       void CANTXMsg(uint32_t ulIndxVal)
 *
 * @brief    This function takes in indexed table value and formats the input 32 bit value into the CAN frame correctly 
 *           according to UTF8 CAN Protocol
 * @param    ulIndxVal = Row number
 *
 * @return   None
 *
 * @note     This will be called every 5 ms.  
 *
 * @author   Tom Rode, Mike Corbett
 *
 */
void CANTXMsg(uint32_t ulIndxVal)
{
    uint8_t ubIdx = 0u;
    uint32_t ulValMask = 0x000000FF;
    static uint8_t ubMsgCnts = 0u;
    static uint8_t ubOldFramLangSel;
    static uint32_t ulOldIndxRead;

  
    /** ###Functional overview: */
    
    /** - Test if flat index in bounds or not due to issue with special function key and space bar*/
    if( ubFlatIndexCheck >= (gParams.Kt.ubRows * gParams.Kt.ubColumns))
    {   
        /** - Input value is bogus */
        ulIndxVal = 0;
    }
    
    /** - Was PDO transmit timer reset? */
    if( 0u == ubMsgCnts )
    {
        /** - Yes, clear PDO data */
        for ( ubIdx = 0u; ubIdx < NUM_KEYBOARD_DATA_BYTES; ubIdx++ )
        {
            TpdoCanTableOutput.ubData[ubIdx] = 0u;
        }
    }
    
    /** - Update the PDO when new data is read from the keyboard */
    if( (ulOldIndxRead != ulIndxVal) && (0u != ulIndxVal) )
    {   
        /** - CAN Message byte 0 the byte count into for UTF-8 Characters zero initially */
        TpdoCanTableOutput.ubData[0u] = 0u;

        /** - Transmit PDO Interface CAN Frame parsing  */
        for(ubIdx = 0u; ubIdx < MAX_CHARS_PER_PDO; ubIdx++)
        {
            /** - Shift mask over after first loop iteration complete*/
            if( 0u != ubIdx)
            {
                /** - Mask shifts each iteration by 8 */
                ulValMask <<= (BITS_PER_CHAR);
            }  

            /** - Place a LSB value into byte 1 in PDO TX buffer */
            TpdoCanTableOutput.ubData[ubIdx + 1u] = ((ulIndxVal & ulValMask) >> (BITS_PER_CHAR * ubIdx));
          
            /** - If a key hit, which value then add to count */  
            if( TpdoCanTableOutput.ubData[ubIdx + 1u] > 0u)
            {
                /** - CAN Message byte 0 the byte count into for UTF-8 Characters */
                TpdoCanTableOutput.ubData[0u]++;
            }
        }       
    }
  
    /** - CAN message byte 5 with Reset status (Function+Alt+Shift) */
    if( (TRUE == mGET_FUNCTION(gUwRowColStat)) && (TRUE == mGET_ALTERNATE(gUwRowColStat)) && (TRUE == mGET_SHIFT(gUwRowColStat)) )
    {
        /** - (Function+Alt+Shift) All pressed send this out CAN */
        TpdoCanTableOutput.ubData[5u] = TRUE;  
    }
    else
    {
        /** - (Function+Alt+Shift) not pressed send this out CAN */
        TpdoCanTableOutput.ubData[5u] = FALSE;  
    }

    /** - IMM has prompted a change to language table set so store to Non volatile memory and nothing beyond language range */ 
    if( (ubOldFramLangSel != gRpdoCanLanguageSelectInput.ubData) && (NINTH_LANG >= gRpdoCanLanguageSelectInput.ubData) )
    {
        /** - There has been a language table set change from IMM to store in FRAM */ 
        WriteFram((uword *)&gRpdoCanLanguageSelectInput.ubData , IMM_LANGUAGE_ADDR, 1u, 0u);

        /** - Update gloabal status */
        gUwRowColStat = mSET_LANGUAGE(gUwRowColStat, (uint8_t *)&gRpdoCanLanguageSelectInput.ubData);  
    }
    
    /** - Hardware test check */
    if( SWITCH_TEST == gRpdoCanLanguageSelectInput.ubData ) 
    {
        /** - CAN message byte 6 with (Temporary hardware status) */
        TpdoCanTableOutput.ubData[6u] = SWITCH_TEST;
    }
    else    
    {
        /** - CAN message byte 6 with (Current Language status) */
        TpdoCanTableOutput.ubData[6u] = mGET_LANGUAGE(gUwRowColStat);
    }
    /** - Update old reading to see if a different value next time */
    ulOldIndxRead = ulIndxVal;

    /** - Update old FRAM value if a change from Imm occurs */
    ubOldFramLangSel = gRpdoCanLanguageSelectInput.ubData;
    
    /** - Increment Transmit PDO counter */
    ubMsgCnts++;

    /** - Is it time to send PDO? */
    if( ubMsgCnts >= TXPDO_MSG_DLY )
    {
        /** - Yes, set flag to send PDO, and clear counter */
        ubAllowTx = TRUE;
        ubMsgCnts = 0u;
    }

    /** - Insuring no byte count if byte 1 is 0 issue on truck*/
    if( 0u == TpdoCanTableOutput.ubData[1u])
    {
        /** Force a zero*/
        TpdoCanTableOutput.ubData[0u] = 0u;
    }
}
