;// TI File $Revision: /main/1 $
;// Checkin $Date: December 5, 2008   18:00:42 $
;//###########################################################################
;//
;// FILE:  DSP2803x_CodeStartBranch.asm
;//
;// TITLE: Branch for redirecting code execution after boot.
;//
;// For these examples, code_start is the first code that is executed after
;// exiting the boot ROM code.
;//
;// The codestart section in the linker cmd file is used to physically place
;// this code at the correct memory location.  This section should be placed
;// at the location the BOOT ROM will re-direct the code to.  For example,
;// for boot to FLASH this code will be located at 0x3f7ff6.
;//
;// In addition, the example DSP2803x projects are setup such that the codegen
;// entry point is also set to the code_start label.  This is done by linker
;// option -e in the project build options.  When the debugger loads the code,
;// it will automatically set the PC to the "entry point" address indicated by
;// the -e linker option.  In this case the debugger is simply assigning the PC,
;// it is not the same as a full reset of the device.
;//
;// The compiler may warn that the entry point for the project is other then
;//  _c_init00.  _c_init00 is the C environment setup and is run before
;// main() is entered. The code_start code will re-direct the execution
;// to _c_init00 and thus there is no worry and this warning can be ignored.
;//
;//###########################################################################
;// $TI Release: DSP2803x C/C++ Header Files V1.10 $
;// $Release Date: July 27, 2009 $
;//###########################################################################

***********************************************************************
    .def    _MarkFault
    .global _MarkFault

WD_DISABLE      .set    1               ;set to 1 to disable WD, else set to 0

    .ref _c_int00
    .global code_start

***********************************************************************
* Function: codestart section
*
* Description: Branch to code starting point
***********************************************************************

    .sect "codestart"

code_start:
    .if WD_DISABLE == 1
        LB wd_disable       ;Branch to watchdog disable code
    .else
        LB _c_int00         ;Branch to start of boot.asm in RTS library
    .endif

;end codestart section

***********************************************************************
* Function: wd_disable
*
* Description: Disables the watchdog timer
***********************************************************************
    .if WD_DISABLE == 1

    .text
wd_disable:
    SETC OBJMODE        ;Set OBJMODE for 28x object code
    EALLOW              ;Enable EALLOW protected register access
    MOVZ DP, #7029h>>6  ;Set data page for WDCR register
    MOV @7029h, #0068h  ;Set WDDIS bit in WDCR to disable WD
    EDIS                ;Disable EALLOW protected register access
    LB _c_int00         ;Branch to start of boot.asm in RTS library

    .endif

;end wd_disable

_MarkFault:
    ; Generate a fault
    EALLOW
    MOVL  XAR4,#0x6F80    ; Set GPIO B Mux1 reg for GPIO39 on Output
    MOVL  XAR5,XAR4
    ADDB  XAR5,#22
    AND   *+XAR5[0],#0x3fff
        MOVL  XAR4,#0x6F80
        MOVL  XAR5,XAR4
        ADDB  XAR5,#26
        OR    *+XAR5[0],#0x0080      ; Set GPIO B DIR reg for GPIO39 as Output
        MOVL  XAR4,#0x6FC8
        MOVL  XAR5,XAR4
        AND   *+XAR5[0],#0xFF7F  ; Turn Off Fault LED
    EDIS
MarkFaultRepeat:
    MOVL  XAR2,#0x70000
MarkFault1:
        MOVL  XAR4,#0x6FCA
        MOVL  XAR5,XAR4
        OR    *+XAR5[0],#0x0080          ; Turn On Fault LED
    SUBB  XAR2, #1
    MOV   AL, #0
    MOV   AH, #0
    CMPL  ACC, XAR2
    BF    MarkFault1, NEQ
    MOVL  XAR2,#0x70000
MarkFault2:
        MOVL  XAR4,#0x6FCC
        MOVL  XAR5,XAR4
        OR    *+XAR5[0],#0x0080          ; Turn Off Fault LED
    SUBB  XAR2, #1
    MOV   AL, #0
    MOV   AH, #0
    CMPL  ACC, XAR2
    BF    MarkFault2, NEQ
    B     MarkFaultRepeat, UNC
    .end


;//===========================================================================
;// End of file.
;//===========================================================================
