/*********************************************************************
 *
 * Filename:    appload.c
 *
 * Copyright (C) 2002 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains the functions to communicate with
 *              application download utility. It will call functions
 *              to program code into FLASH memory.
 *
 **********************************************************************/

//****************************************************************************
// @Project Includes
//****************************************************************************
#include <string.h>
#include <stdio.h>
#include "main.h"
#include "appload.h"
#include "flash.h"
#include "types.h"

#include "DSP2803x_GlobalPrototypes.h"
#include "Flash2803x_API_Library.h"

// Get info about the flashrel section 

//****************************************************************************
// @Global Veriables
//****************************************************************************

//****************************************************************************
// @Local Veriables
//****************************************************************************

//****************************************************************************
// @External Prototypes
//****************************************************************************
extern void vEraseLastSector(void);
extern uword crc16_range(ulong length, uword crc, uword *ptr);
extern boolean fEraseBootSector(void);
extern void vEraseLastSector(void);

//****************************************************************************
// @Prototypes Of Local Functions
//****************************************************************************
boolean fCopyBoot(void);
extern void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr);

/**********************************************************************
 *
 * Function:    FlashUtilities2Ram()
 *
 * Description: This function copies the sections of code compiled to
 *              run from RAM to their RAM locations from FLASH.
 *
 * Argument(s): None
 *
 * Returns:     Nothing
 *
 ***********************************************************************/
void FlashUtilities2Ram(void)
{

    // We must also copy required user interface functions to RAM. 
    MemCopy(&RamfuncsLoadStart, &RamfuncsLoadEnd, &RamfuncsRunStart);

}

/*------------------------------------------------------------------
  Simple memory copy routine to move code out of flash into SARAM
-----------------------------------------------------------------*/
void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr)
{
    while ( SourceAddr < SourceEndAddr )
    {
        *DestAddr++ = *SourceAddr++;
    }
    return;
}

boolean fUpdateBoot(void)
{
   ubyte   ubTryCount  = 0;
   boolean fSuccess    = FALSE;
   boolean fKeepTrying = TRUE;

   while ( fKeepTrying )
   {
      fSuccess = fEraseBootSector();
      if ( fSuccess )
      {
       fSuccess = fCopyBoot();
      }
      if ( fSuccess )
      {
         fKeepTrying = FALSE;
      }
      else
      {
         ubTryCount++;
         if ( ubTryCount >= 3 )
         {
            fKeepTrying = FALSE;
         }
      }
   }
   return(fSuccess);
}

boolean fCopyBoot(void)
{
   uword   uwPage;
   boolean fSuccess;
   static uword *pSrc = (uword *)0x3F2000; // source target is in sector C
   ulong   ulProgAddr =          0x3F6000; // destination is in sector A

   for ( uwPage = 0; uwPage < 64; uwPage++ )
   {
      vFillBuffer(pSrc, FLASH_BUFFER_SIZE);
      fSuccess = fProgramBlock(ulProgAddr);
      pSrc       += FLASH_BUFFER_SIZE;
      ulProgAddr += FLASH_BUFFER_SIZE;
      if ( !fSuccess )
      {
         break;
      }
   }
   return(fSuccess);
}

boolean fCheckCrc(void)
{
   uword   uwSrcCrc;
   uword   uwDestCrc;
   boolean fSuccess = FALSE;
      
   uwSrcCrc  = crc16_range(0x2000, 0xffff, (uword *)0x3F2000);
   uwDestCrc = crc16_range(0x2000, 0xffff, (uword *)0x3F6000);

   if ( uwSrcCrc == uwDestCrc )
   {
      fSuccess = TRUE;
   }

   return(fSuccess);
}

void vInvalidateApp(void)
{
   vEraseLastSector();
}
