***********************************************************************
* File: CodeStartBranch.asm
* Devices: TMS320F2812, TMS320F2811, TMS320F2810
* Author: David M. Alter, Texas Instruments Inc.
* History:
*   09/08/03 - original (D. Alter)
***********************************************************************

***********************************************************************
    .def    _MarkFault
    .global _MarkFault

WD_DISABLE      .set    1               ;set to 1 to disable WD, else set to 0

    .ref _c_int00
    .global code_start

***********************************************************************
* Function: codestart section
*
* Description: Branch to code starting point
***********************************************************************

    .sect "codestart"

code_start:
    .if WD_DISABLE == 1
        LB wd_disable       ;Branch to watchdog disable code
    .else
        LB clr_ram_mem      ;Branch ram clearing routine
    .endif

;end codestart section


***********************************************************************
* Function: wd_disable
*
* Description: Disables the watchdog timer
***********************************************************************
    .if WD_DISABLE == 1

    .text
wd_disable:
    SETC OBJMODE        ;Set OBJMODE for 28x object code
    EALLOW              ;Enable EALLOW protected register access
    MOVZ DP, #7029h>>6  ;Set data page for WDCR register
    MOV @7029h, #0068h  ;Set WDDIS bit in WDCR to disable WD
    EDIS                ;Disable EALLOW protected register access
    LB clr_ram_mem      ;Branch ram clearing routine

    .endif

;end wd_disable

***********************************************************************
* Function: clr_ram_mem
*
* Description: clears the RAM banks
***********************************************************************

    .text
clr_ram_mem:
    SETC OBJMODE        ;Set OBJMODE for 28x object code

    ; first test memory by shifting a bit through
    ; Registers XAR4 = count of memory problems
    ;           XAR2, XAR3 = pointers to memory for operation
    ;           AR6 = bit pass loops
    ;           AR7 = contents to put in RAM

    MOV  AR6, #15           ; Do 16 bits
    MOV  AR7, #1            ; Start with bit 0
    MOVL XAR4, #0           ; Set failed count to zero
MemCheckLoop:
    MOVL XAR2,#00008000h    ; Load XAR2 with start address of L0 - L3 RAM
    MOV  AR0,#01FFFh        ; Load AR0 with loop count for RAM size
CheckLoop1:
    ; fill memory
    MOV  *XAR2, AR7        ; Load test pattern to location pointed to by XAR2,

    MOV  AL, AR7           ; Copy the pattern to AL for compare
    CMP  AL, *XAR2++       ; Check written value in L0-L3 RAM
    BF   Good1, EQ
    MOV  AL, #1               ; Failed, bump count
    MOV  AH, #0
    ADDL XAR4, ACC
    MOV  AL, AR7
Good1:
    BANZ CheckLoop1,AR0--         ; Loop until AR0 == 0, post-decrement AR0

    MOVL XAR2,#000000h       ; Load XAR2 with start address of Vector/M0/M1 RAM
    MOV  AR0,#07FFh          ; Load AR0 with loop count for RAM size
CheckLoop2:                  ; Skip string for boot mode at end of M1
    MOV  *XAR2, AR7        ; Load 0 to location pointed to by XAR2,
    MOV  AL, AR7           ; Copy the pattern to AL for compare
    CMP  AL, *XAR2++       ; Check written value in Vector/MO/M1 RAM
    BF   Good3, EQ
    MOV  AL, #1               ; Failed, bump count
    MOV  AH, #0
    ADDL XAR4, ACC
    MOV  AL, AR7
Good3:
    BANZ CheckLoop2,AR0--         ; Loop until AR0 == 0, post-decrement AR0
    MOV  AL, AR7                  ; Do next power of two
    LSL  AL, #1
    MOV  AR7, AL
    BANZ MemCheckLoop, AR6--


    ; Clear the memory
    MOVL XAR2,#00008000h    ; Load XAR2 with start address of L0 - L3 RAM
    MOV  AR0,#01FFFh        ; Load AR0 with loop count for RAM size

ClearLoop1:
    MOV  *XAR2++, #0        ; Load 0 to location pointed to by XAR2,
                            ; post-increment XAR2


    BANZ ClearLoop1,AR0--   ; Loop until AR0 == 0, post-decrement AR0

    MOVL XAR2,#000000h      ; Load XAR2 with start address of Vector/M0/M1 RAM
    MOV  AR0,#07FFh         ; Load AR0 with loop count for RAM size
ClearLoop2:
    MOV  *XAR2++, #0        ; Load 0 to location pointed to by XAR2,
                            ; post-increment XAR2
    BANZ ClearLoop2,AR0--   ; Loop until AR0 == 0, post-decrement AR0

    ; Check if there were any bit errors on the RAM
    MOV  AH,#0
    MOV  AL,#0
    CMPL ACC, XAR4
    BF   _MarkFault, NEQ
    LB _c_int00             ;Branch to start of boot.asm in RTS library

_MarkFault:
    ; Generate a fault
    EALLOW
    MOVL  XAR4,#0x6F80    ; Set GPIO B Mux1 reg for GPIO39 on Output
    MOVL  XAR5,XAR4
    ADDB  XAR5,#22
    AND   *+XAR5[0],#0x3fff
	MOVL  XAR4,#0x6F80
	MOVL  XAR5,XAR4
	ADDB  XAR5,#26
	OR    *+XAR5[0],#0x0080      ; Set GPIO B DIR reg for GPIO39 as Output
	MOVL  XAR4,#0x6FC8
	MOVL  XAR5,XAR4
	AND   *+XAR5[0],#0xFF7F  ; Turn Off Fault LED
    EDIS
MarkFaultRepeat:
    MOVL  XAR2,#0x70000
MarkFault1:
	MOVL  XAR4,#0x6FCA
	MOVL  XAR5,XAR4
	OR    *+XAR5[0],#0x0080          ; Turn On Fault LED
    SUBB  XAR2, #1
    MOV   AL, #0
    MOV   AH, #0
    CMPL  ACC, XAR2
    BF    MarkFault1, NEQ
    MOVL  XAR2,#0x70000
MarkFault2:
	MOVL  XAR4,#0x6FCC
	MOVL  XAR5,XAR4
	OR    *+XAR5[0],#0x0080          ; Turn Off Fault LED
    SUBB  XAR2, #1
    MOV   AL, #0
    MOV   AH, #0
    CMPL  ACC, XAR2
    BF    MarkFault2, NEQ
    B     MarkFaultRepeat, UNC
    .end

; end of file CodeStartBranch.asm
