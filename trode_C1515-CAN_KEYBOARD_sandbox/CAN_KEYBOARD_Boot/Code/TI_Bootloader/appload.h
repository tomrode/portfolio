/**********************************************************************
 *
 * Filename:    appload.h
 *
 * Copyright (C) 2005 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains definitions used in CAN and RS-232
 *              communication protocol during application download.
 *
 **********************************************************************/

#ifndef APPLOAD_H
#define APPLOAD_H

#define CNTL_BYTES_PER_MSG (2)
#define DATA_BYTES_PER_MSG (6)

#define ADDR_INT_RAM_START  0x008000    // Start of internal RAM section

#define ADDR_CKS_FLAG       (0x3E8000)
#define ADDR_RESET_VECTOR   (0x3EA000)
#define ADDR_INFO_START     (0x3F5E00)
#define ADDR_TRADEMARK      (0x3F5E00)
#define ADDR_APP_SFWR_PN    (0x3F5E20)
#define ADDR_APP_FILL_TABLE (0x3F5E50)
#define ADDR_MEM_MAP        (0x3F5E60)
#define ADDR_COMPATIBILITY  (0x3F5EB0)
#define ADDR_APP_SFWR_CRC   (0x3F5FF0)
#define ADDR_APP_SFWR_CKS   (0x3F5FF2)

#define BOOTUP_TIMEOUT_MS        10L
#define BOOTUP_TIMEOUT_CNTS      ((ulong)BOOTUP_TIMEOUT_MS*(ulong)GPT1_TIMER_2_CPMS)

#define GET_OBJ_232_TIMEOUT_MS   1000L
#define GET_OBJ_232_TIMEOUT_CNTS ((ulong)GET_OBJ_232_TIMEOUT_MS*(ulong)GPT1_TIMER_2_CPMS)

#define PC_PRESENT_TIMEOUT_MS    1000L
#define PC_PRESENT_TIMEOUT_CNTS  ((ulong)PC_PRESENT_TIMEOUT_MS*(ulong)GPT1_TIMER_2_CPMS)

#define VENDOR_ID 0x96L

typedef enum
{
   PC_EMUL,
   MODULE
}Send_type;

typedef enum
{
   PORT_CAN = 0x01,
   PORT_232 = 0x02
}Port_type;

typedef enum
{
   C167CS  = 1,
   XC164CS = 2,
   M68HC16 = 3,
   TMS320F281x = 4
}PROCESSOR_TYPE;

typedef enum
{
   CKS,
   CRC
}CALC_TYPE;

typedef enum
{
   cmdReadData     = 0x40,
   cmdReadAnswer   = 0x42,
   cmdWriteData    = 0x22,
   cmdWriteAnswer  = 0x60,
   cmdOtherEvent   = 0x69,
   cmdOpInProgress = 0x30,
   cmdOpPassed     = 0x31,
   cmdOpFailed     = 0x32,
   cmdUnsupported  = 0x55
}Cmd_type;

typedef enum
{
   idxBootUpMessage              = 0x5000,
   idxPcPresent                  = 0x5001,
   idxSendBootUpMessage          = 0x5002,
   idxStoreData                  = 0x5010,
   idxRunBootLoadProgram         = 0x5011,
   idxEraseAppSection            = 0x5012,
   idxBufferData                 = 0x5013,
   idxEndSession                 = 0x5014,
   idxProgramBuffer              = 0x5015,
   idxConfigRxBuffer             = 0x5016,
   idxGetRxData                  = 0x5017,
   idxSendTxData                 = 0x5018,
   idxReturnModulesOnNetwork     = 0x5020,
   idxReturnManCode              = 0x5021,
   idxReturnDevCode              = 0x5022,
   idxReturnInfoStart            = 0x5023,
   idxFramRevision               = 0x5030,
   idxModuleId                   = 0x5031,
   idxModuleType                 = 0x5032,
   idxModulePN                   = 0x5033,
   idxSerialNumber               = 0x5034,
   idxHardwarePN                 = 0x5035,
   idxHardwareRev                = 0x5036,
   idxBootSoftwarePN             = 0x5037,
   idxCalcBootSoftwareCRC        = 0x5038,
   idxAppSoftwarePN              = 0x5039,
   idxStoredAppSoftwareCKS       = 0x503A,
   idxStoredAppSoftwareCRC       = 0x503B,
   idxCalculatedAppSoftwareCRC   = 0x503C,
   idxCalculatedAppSoftwareCKS   = 0x503D,
   idxCanBaud                    = 0x503E,
   idxUnused                     = 0x503F,
   idxRs232Baud                  = 0x5040,
   idxRs232Enable                = 0x5041,
   idxExternalMemoryMap          = 0x5042,
   idxSwitchIDEnable             = 0x5043
}Index_type;

typedef enum
{
   FIELD_START,
   FIELD_COMMAND,
   FIELD_INDEX,
   FIELD_NODEID,
   FIELD_CMDREG,
   FIELD_LENGTH,
   FIELD_DATA,
   FIELD_CHECKSUM,
   FIELD_END,
   FIELD_COMPLETE
}Field_232;

typedef struct
{
    Cmd_type   command;
    Index_type index;
    uword      module_id;
    uword      cmd_reg[4];
    uword      length;
    uword      data[32];
    Port_type  source;      // Used for pass-thru mode 
}Obj_type;

#endif                            

 

