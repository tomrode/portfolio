/**********************************************************************
 *
 * Filename:    utility.c
 *
 * Copyright (C) 2001 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains miscellaneous utility functions.
 *
 **********************************************************************/

//****************************************************************************
// @Project Includes
//****************************************************************************
#include <ctype.h>
#include "main.h"


//****************************************************************************
// @Macros
//****************************************************************************

//****************************************************************************
// @Prototypes Of Local Functions
//****************************************************************************
void vDelay_ms(uword uwDelayMs);

//****************************************************************************
// @Prototypes Of External Functions
//****************************************************************************
extern void  vFaultLedOn(boolean fTurnOn);


/**********************************************************************
 *
 * Function:    vDelay_ms()
 *
 * Description: This function waits for the specified number of 
 *              milli-seconds and returns.
 *              
 * Argument(s): uwDelay_ms-> 16-bit number of milli-seconds to delay
 *
 * Returns:     Nothing
 *
 * Comments:    Limits to 3.5 second delay
 *
 **********************************************************************/
void vDelay_ms(uword uwDelayMs)
{
   ulong ulTimeoutCnts;
   
   if ( uwDelayMs > 3500 )
   {
      uwDelayMs = 3500;
   }
   ulTimeoutCnts = (ulong)uwDelayMs * (ulong)GPT1_TIMER_2_CPMS;

   vResetIntervalTimer();
   while ( ulGetIntervalTimer() < ulTimeoutCnts ){};
}


ubyte ubClip(ubyte ubMin, ubyte ubValue, ubyte ubMax)
{
   if ( ubValue < ubMin )
   {
      ubValue = ubMin;
   }
   if ( ubValue > ubMax )
   {
      ubValue = ubMax;
   }
   return(ubValue);
}

