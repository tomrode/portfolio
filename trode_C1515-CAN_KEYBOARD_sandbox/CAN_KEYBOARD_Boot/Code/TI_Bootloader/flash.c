/**********************************************************************
 *
 * Filename:    flash.c
 *
 * Copyright (C) 2005 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains the functions to Program and Erase
 *              Several FLASH devices.
 *
 **********************************************************************/

#include <string.h>
#include "main.h"
#include "flash.h"
#include "DSP2803x_Device.h"     // DSP28 Headerfile Include File
#include "DSP2803x_Examples.h"   // DSP28 Examples Include File
#include "Flash2803x_API_Library.h"

//****************************************************************************
// @Global Veriables used in this file
//****************************************************************************
static uword uwFlashBuffer[FLASH_BUFFER_SIZE];
extern Uint32 Flash_CPUScaleFactor;
extern Uint16 CsmUnlockWithCode(int code);

FLASH_ST EraseStatus;
FLASH_ST ProgStatus;



void    vInit_Flash_API(void)
{
    /*------------------------------------------------------------------
      Initalize Flash_CPUScaleFactor.

       Flash_CPUScaleFactor is a 32-bit global variable that the flash
       API functions use to scale software delays. This scale factor
       must be initalized to SCALE_FACTOR by the user's code prior
       to calling any of the Flash API functions. This initalization
       is VITAL to the proper operation of the flash API functions.

       SCALE_FACTOR is defined in Example_Flash281x_API.h as
         #define SCALE_FACTOR  1048576.0L*( (200L/CPU_RATE) )

       This value is calculated during the compile based on the CPU
       rate, in nanoseconds, at which the algorithums will be run.
    ------------------------------------------------------------------*/
    EALLOW;
    Flash_CPUScaleFactor = SCALE_FACTOR;
    EDIS;
}



/*
 ** fProgramBlock
 *
 *  FILENAME:     flash.c
 *
 *  PARAMETERS:   ulAdr = Block start address
 *                pBuff = Address of data buffer to be written
 *
 *  DESCRIPTION:  Writes 64 words (128 bytes) to the block with the
 *                specified start address.
 *
 *  RETURNS:      TRUE if operation is successful otherwise FALSE
 *
 */
#pragma CODE_SECTION(fProgramBlock,"ramfuncs");
boolean fProgramBlock(ulong ulAdr)
{
    uword *puwFlashAddr;
    boolean fSuccess = TRUE;
    Uint16 UnlockStatus;
    uword uwResult;
    int code;
    
    for (code=0; code<3; code++)
    {
        UnlockStatus = CsmUnlockWithCode(code);
        if (UnlockStatus == 1)
        {
            break;
        }
    }
    
    puwFlashAddr = (uword *)ulAdr;
    uwResult = Flash_Program(puwFlashAddr, &uwFlashBuffer[0], FLASH_BUFFER_SIZE, &ProgStatus);
    if (uwResult != STATUS_SUCCESS)
    {
        fSuccess = FALSE;
    }
    return(fSuccess);
}

void vInitBuffer(uword uwValue)
{
   memset(&uwFlashBuffer[0], uwValue, FLASH_BUFFER_SIZE);
}

void vFillBuffer(uword *pSrcBuff, uword uwNumWords)
{
   if ( uwNumWords <= FLASH_BUFFER_SIZE )
   {
      memcpy(&uwFlashBuffer[0], pSrcBuff, uwNumWords);
   }
}

#pragma CODE_SECTION(fEraseBootSector,"ramfuncs");
boolean fEraseBootSector(void)
{
    boolean fSuccess = TRUE;
    uword uwEraseMask;
    uword uwResult;
    uword uwCount = 0;
    Uint16 UnlockStatus;
    int code;
    
    for (code=0; code<3; code++)
    {
        UnlockStatus = CsmUnlockWithCode(code);
        if (UnlockStatus == 1)
        {
            break;
        }
    }
    uwEraseMask = SECTORA;
    uwResult = Flash_Erase(uwEraseMask, &EraseStatus);
    if (uwResult != STATUS_SUCCESS)
    {
        fSuccess = FALSE;
    }
	if ((uwResult == STATUS_FAIL_PRECONDITION) ||
	    (uwResult == STATUS_FAIL_PRECOMPACT) ||
	    (uwResult == STATUS_FAIL_COMPACT)    ||
	    (uwResult == STATUS_FAIL_ERASE))  // depletion problem
	{
		do
		{
			uwResult = Flash_DepRecover();	 // try to recover
			uwCount++;
		} while((uwResult == STATUS_FAIL_COMPACT) && (uwCount < 2));
		fSuccess = TRUE;
	    uwEraseMask = SECTORA;
	    uwResult = Flash_Erase(uwEraseMask, &EraseStatus);
	    if (uwResult != STATUS_SUCCESS)
	    {
	        fSuccess = FALSE;
	    }
	}
    return (fSuccess);
}

#pragma CODE_SECTION(vEraseLastSector,"ramfuncs");
void vEraseLastSector(void)
{
    uword uwEraseMask;
    Uint16 UnlockStatus;
    int code;
    
    for (code=0; code<3; code++)
    {
        UnlockStatus = CsmUnlockWithCode(code);
        if (UnlockStatus == 1)
        {
            break;
        }
    }
    
    uwEraseMask = SECTORB | SECTORC;
    Flash_Erase(uwEraseMask, &EraseStatus);
}


