/**********************************************************************
 *
 * Filename:    flash.h
 *
 * Copyright (C) 2001 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains definitions used in FLASH programming.
 *              
 *              
 *
 **********************************************************************/

#ifndef FLASH_H
#define FLASH_H


#define FLASH_BUFFER_SIZE  (128)         // Size in bytes of FLASH buffer
#define FLASH_BUFFER_WORDS (64)
#define FLASH_ERASE_STATE  0xFF           // Erase state of flash data
#define ADDR_BOOT_START    0x3F6000
#define ADDR_BOOT_END      0x3F7FF7
#define ADDR_FLASH_END     0x3F7FFF
#define ADDR_INT_FL_START  0x3E8000    // Start of internal FLASH app section
#define ADDR_INT_FL_END    0x3F5FC1    // End of internal FLASH app section (end - 64 words + 1)
                             
void    TurnLedOn(void);
void    vInit_Flash_API(void);
boolean fProgramBlock(ulong ulAdr);
void    vFillBuffer(uword *pSrcBuff, uword uwNumWords);
void    vClearBuffer(void);

#endif                            

 

