;//###########################################################################
;//
;// FILE:  Info.asm     
;//
;// TITLE: Stores the parameter flag in flash. 
;//
;//###########################################################################


***********************************************************************


    .sect "trademark"

	.def	_trademark
_trademark:	.string "Crown Equipment Corp"	; Crown trademark
    .word 0
    
    .sect "partnumber"
    .def _partnumber
        
_partnumber:	.string "147915-901-01"		; Application software part number
    .word 0
    .word 0
    .word 0
    .word 0		; string must fill 16 bytes plus a null
            
    .sect "fillmap"
    .word 0ffffh					; Memory block 1 fill
    .word 0ffffh					; Memory block 2 fill
    .word 0ffffh					; Memory block 3 fill
    .word 0ffffh					; Memory block 4 fill
    .word 0ffffh					; Memory block 5 fill
    .word 0ffffh					; Memory block 6 fill
    .word 0ffffh					; Memory block 7 fill
    .word 0ffffh					; Memory block 8 fill
        
    .sect "memorymap"
    .word 1                         ; Number of table entries -- one memory region to check
    ;start of block one
    .long 03E8000h                  ; 0x3E8000 to 0x3F5FEF
    ;end of block one
    .long 03F5FEFh
        
    .sect "compatableboards"
    .word 1							; Number of table entries 
    .string "147923"				; compatible part numbers, use xxxx\0xxxxx\0 for multiple
    .word 0
        
        
    .sect "appcrc"
    .word 0ffffh
    
    .sect "appcs"
    .word 0ffffh
        
    .end
        
; end of file