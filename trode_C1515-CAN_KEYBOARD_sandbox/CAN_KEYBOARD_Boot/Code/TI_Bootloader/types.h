#include "DSP2803x_Device.h"

#ifndef types_h_reg
#define types_h_reg

typedef unsigned char  ubyte;    // 1 byte unsigned; prefix: ub 
typedef signed char    sbyte;    // 1 byte signed;   prefix: sb 
typedef Uint16         uword;    // 2 byte unsigned; prefix: uw 
typedef signed int     sword;    // 2 byte signed;   prefix: sw 
typedef Uint32         ulong;    // 4 byte unsigned; prefix: ul 
typedef signed long    slong;    // 4 byte signed;   prefix: sl 
#ifndef boolean
    typedef unsigned char  boolean;
#endif
#ifndef FALSE
 #define FALSE 0
 #define TRUE  1
#endif

#endif
