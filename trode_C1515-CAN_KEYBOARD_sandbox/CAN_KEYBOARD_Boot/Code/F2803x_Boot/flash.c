/**********************************************************************
 *
 * Filename:    flash.c
 *
 * Copyright (C) 2005 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains the functions to Program and Erase
 *              Several FLASH devices.
 *
 **********************************************************************/

#include <string.h>
#include "main.h"
#include "flash.h"
#include "DSP2803x_Device.h"     // DSP28 Headerfile Include File
#include "DSP2803x_Examples.h"   // DSP28 Examples Include File
#include "Flash2803x_API_Library.h"

//****************************************************************************
// @Global Veriables used in this file
//****************************************************************************
static uword uwFlashBuffer[FLASH_BUFFER_WORDS];
extern Uint32 Flash_CPUScaleFactor;
extern Uint16 CsmUnlock();

FLASH_ST EraseStatus;
FLASH_ST ProgStatus;



void    vInit_Flash_API(void)
{
    /*------------------------------------------------------------------
      Initalize Flash_CPUScaleFactor.

       Flash_CPUScaleFactor is a 32-bit global variable that the flash
       API functions use to scale software delays. This scale factor
       must be initalized to SCALE_FACTOR by the user's code prior
       to calling any of the Flash API functions. This initalization
       is VITAL to the proper operation of the flash API functions.

       SCALE_FACTOR is defined in Example_Flash281x_API.h as
         #define SCALE_FACTOR  1048576.0L*( (200L/CPU_RATE) )

       This value is calculated during the compile based on the CPU
       rate, in nanoseconds, at which the algorithums will be run.
    ------------------------------------------------------------------*/
    EALLOW;
    Flash_CPUScaleFactor = SCALE_FACTOR;
    EDIS;
}



/*
 ** fProgramBlock
 *
 *  FILENAME:     flash.c
 *
 *  PARAMETERS:   ulAdr = Block start address
 *                pBuff = Address of data buffer to be written
 *
 *  DESCRIPTION:  Writes 64 words (128 bytes) to the block with the
 *                specified start address.
 *
 *  RETURNS:      TRUE if operation is successful otherwise FALSE
 *
 */
#pragma CODE_SECTION(fProgramBlock,"ramfuncs");
boolean fProgramBlock(ulong ulAdr)
{
    uword *puwFlashAddr;

    boolean fSuccess = TRUE;
    uword uwResult;

    CsmUnlock();
    puwFlashAddr = (uword *)ulAdr;
    uwResult = Flash_Program(puwFlashAddr, &uwFlashBuffer[0], FLASH_BUFFER_WORDS, &ProgStatus);
    if (uwResult != STATUS_SUCCESS)
    {
        fSuccess = FALSE;
    }
    return(fSuccess);
}

#pragma CODE_SECTION(fEraseAppSectors,"ramfuncs");
boolean fEraseAppSectors(boolean fEraseParSectorOnly)
{
    boolean fSuccess = TRUE;
    uword uwEraseMask;
    uword uwResult;
    uword uwCount = 0;

    CsmUnlock();
    if ( fEraseParSectorOnly )
    {
        uwEraseMask = SECTORH;
        uwResult = Flash_Erase(uwEraseMask, &EraseStatus);
        if (uwResult != STATUS_SUCCESS)
        {
            fSuccess = FALSE;
        }
    }
    else
    {
        // erase B first to wack the info block in case the rest does not complete
        uwEraseMask = SECTORB;

        uwResult = Flash_Erase(uwEraseMask, &EraseStatus);
        if (uwResult != STATUS_SUCCESS)
        {
            fSuccess = FALSE;
        }
        if (fSuccess)  // if still OK,
        {
            uwEraseMask = SECTORC | SECTORD | SECTORE | SECTORF |
                          SECTORG | SECTORH;

            uwResult = Flash_Erase(uwEraseMask, &EraseStatus); // erase the rest
            if (uwResult != STATUS_SUCCESS)
            {
                fSuccess = FALSE;
            }
        }
    }
    if ((uwResult == STATUS_FAIL_PRECONDITION) ||
        (uwResult == STATUS_FAIL_PRECOMPACT) ||
        (uwResult == STATUS_FAIL_COMPACT)    ||
        (uwResult == STATUS_FAIL_ERASE))  // depletion problem
    {
        do
        {
            uwResult = Flash_DepRecover();   // try to recover
            uwCount++;
        } while((uwResult == STATUS_FAIL_COMPACT) && (uwCount < 2));
        fSuccess = FALSE;                    // mark the erase failed.  It should work on the next try
    }
    return(fSuccess);
}

void vClearBuffer(void)
{
    memset(&uwFlashBuffer[0], FLASH_ERASE_STATE, FLASH_BUFFER_WORDS);
}

void vFillBuffer(uword *pSrcBuff, uword uwDestIndex, uword uwNumBytes)
{
    uword uwSrcIndex;

    for ( uwSrcIndex = 0; uwSrcIndex < uwNumBytes; uwSrcIndex += 2 )
    {
        if ( (uwDestIndex) < FLASH_BUFFER_WORDS )
        {
            uwFlashBuffer[uwDestIndex] = ((uword)pSrcBuff[uwSrcIndex+1]<< 8) | pSrcBuff[uwSrcIndex];
            uwDestIndex++;
        }
    }
}

