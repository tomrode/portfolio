/**********************************************************************
 *
 * Filename:    ramtron.c
 *
 * Copyright (C) 2004 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains functions to Read, Write, and Control
 *              the Ramtron FRAM Nonvolatile Memory.
 *
 * Notes:       Device support is RM25640 only
 *
 **********************************************************************/

//****************************************************************************
// @Project Includes
//****************************************************************************
#include "DSP2803x_Device.h"     // DSP281x Headerfile Include File
#include "DSP2803x_Examples.h"   // DSP281x Examples Include File
#include "SPI.h"
#include "ramtron.h"
#include "errors.h"

//****************************************************************************
// @External Prototypes
//****************************************************************************

//****************************************************************************
// @Internal Prototypes
//****************************************************************************
static void EnableWriteFram(boolean);

//****************************************************************************
// @Macros
//****************************************************************************
#define FRAM_SIZE  0x2000
#define FRAM_CS_LO (GpioDataRegs.GPACLEAR.bit.GPIO27 = 1)
#define FRAM_CS_HI (GpioDataRegs.GPASET.bit.GPIO27 = 1)

uword   uwReadStatusFram(void);
static  void EnableWriteFram(boolean);
boolean fWriteStatusFram(uword uwStatusReg,  boolean fVerify);

/**********************************************************************
 ** uwReadStatusFram
 *
 *  FILENAME:    ramtron.c
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  Reads the FRAM status register
 *
 *  RETURNS:      Contents of FRAM status register
 *
 **********************************************************************/
uword uwReadStatusFram(void)
{
    volatile uword uwReadData;

    // Take CS low and wait at least 90ns before clocking
    FRAM_CS_LO;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");

    // Send read status command
    SPI_fMySendData((uword)FRAM_RDSR, &uwReadData, 8);

    SPI_fMySendData((uword)0x00, &uwReadData, 8);

    // Take CS High after tranmission is complete
    FRAM_CS_HI;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");

    return(ubyte)(uwReadData);
}

/**********************************************************************
 ** fFramControlWpEn
 *
 *  FILENAME:     ramtron.c
 *
 *  PARAMETERS:   uwState = 0 to clear bit
 *
 *  DESCRIPTION:  This function writes the WPEN bit to 1 or 0
 *                to control function of WP pin.
 *
 *  RETURNS:      TRUE if write was successful otherwise FALSE
 *
 **********************************************************************/
boolean fFramControlWpEn(uword uwState)
{
    volatile boolean fSuccess;
    volatile uword   uwStatusReg;

    uwStatusReg = uwReadStatusFram();

    if ( uwState )
    {
        uwStatusReg |= BIT_WPEN;
    }
    else
    {
        uwStatusReg &= ~BIT_WPEN;
    }
    fSuccess = fWriteStatusFram(uwStatusReg, TRUE);

    return(fSuccess);
}

/**********************************************************************
 ** fFramProtect
 *
 *  FILENAME:     S:\miniturret\boot\Src\ramtron.c
 *
 *  PARAMETERS:   RANGE0   = Unprotect all of FRAM
 *                RANGE25  = Protect upper  25% of FRAM
 *                RANGE50  = Protect upper  50% of FRAM
 *                RANGE100 = Protect upper 100% of FRAM
 *
 *  DESCRIPTION:  This function is used to protect/unprotect FRAM from write
 *                access
 *
 *  RETURNS:      TRUE if operation is successful otherwise FALSE
 *
 **********************************************************************/
boolean fFramProtect(uword uwRange)
{
    volatile uword   uwStatusReg;
    volatile boolean fSuccess = FALSE;

    if ( uwRange == RANGE0  || uwRange == RANGE25
         || uwRange == RANGE50 || uwRange == RANGE100
       )
    {
        /* Protect the specified area */
        uwStatusReg = (uwReadStatusFram() & CLEAR_BPS) | uwRange;

        fSuccess = fWriteStatusFram(uwStatusReg, TRUE);
    }
    return(fSuccess);
}

/**********************************************************************
 ** fWriteStatusFram
 *
 *  FILENAME:     S:\miniturret\boot\Src\ramtron.c
 *
 *  PARAMETERS:   ubStatusReg = Value to write to status register
 *                fVerify     = TRUE if value should be readback and verified
 *
 *  DESCRIPTION:  This function writes the specified value to the FRAM status
 *                register and will optionally verify the value was correctly
 *                written
 *
 *  RETURNS:      TRUE if value is correctly written or if verify option not
 *                requested, otherwise false.
 *
 **********************************************************************/
boolean fWriteStatusFram(uword uwStatusReg,  boolean fVerify)
{
    volatile boolean fSuccess = TRUE;
    volatile uword uwValue;
    volatile boolean fResult;

    // Enable writes to FRAM
    EnableWriteFram(TRUE);

    // Take CS low and wait at least 90ns before clocking
    FRAM_CS_LO;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");

    // Send write status register command
    fResult = SPI_fMySendData((uword)FRAM_WRSR, &uwValue, 8);
    if (!fResult)
    {
        fSuccess = FALSE;
    }
    fResult = SPI_fMySendData(uwStatusReg, &uwValue, 8);
    if (!fResult)
    {
        fSuccess = FALSE;
    }


    // Take CS High and wait at least 100ns before taking it low again
    FRAM_CS_HI;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");
    if ( fVerify )
    {
        // Take CS Low and want at least 90ns before clocking
        FRAM_CS_LO;
        asm(" RPT #7 || NOP");
        asm(" RPT #7 || NOP");

        // Send read status register command
        fResult = SPI_fMySendData((uword)FRAM_RDSR, &uwValue, 8);
        if (!fResult)
        {
            fSuccess = FALSE;
        }

        // Read status and verify same as status written
        fResult = SPI_fMySendData((uword)0x00, &uwValue, 8);
        if (!fResult)
        {
            fSuccess = FALSE;
        }

        if ( (ubyte)uwValue != (ubyte)uwStatusReg )
        {
            fSuccess = FALSE;
        }
        // Take CS High after tranmission is complete
        FRAM_CS_HI;
        asm(" RPT #7 || NOP");
        asm(" RPT #7 || NOP");
    }
    return(fSuccess);
}

/**********************************************************************
 ** ubReadStatusFram
 *
 *  FILENAME:     S:\miniturret\boot\xc164cs\ramtron.c
 *
 *  PARAMETERS:   None
 *
 *  DESCRIPTION:  Reads the FRAM status register
 *
 *  RETURNS:      Contents of FRAM status register
 *
 **********************************************************************/
ubyte ubReadStatusFram(void)
{
    volatile uword uwReadData;

    // Take CS low and wait at least 90ns before clocking
    FRAM_CS_LO;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");

    // Send read status command
    SPI_fMySendData((uword)FRAM_RDSR, &uwReadData, 8);

    SPI_fMySendData((uword)0x00, &uwReadData, 8);

    // Take CS High after tranmission is complete
    FRAM_CS_HI;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");

    return(ubyte)(uwReadData);
}

/**********************************************************************
 *
 * Function:    EnableWriteFram()
 *
 * Description: This function enables or disables the write feature
 *              of the FRAM depending on the argument passed.
 *
 * Notes:       Write will be automatically be disabled after a write
 *              cycle is completed.
 *
 * Returns:     Nothing
 *
 **********************************************************************/
static void EnableWriteFram(boolean enable)
{
    volatile uword uwResult;

    // Take CS low and wait at least 90ns before clocking
    FRAM_CS_LO;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");

    // Send write enable command, write command and address
    if ( enable )
    {
        SPI_fMySendData((uword)FRAM_WREN, &uwResult, 8);
    }
    else
    {
        SPI_fMySendData((uword)FRAM_WRDI, &uwResult, 8);
    }

    // Take CS High and wait at least 100ns before taking it low again
    FRAM_CS_HI;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");
}

/**********************************************************************
 *
 * Function:    WriteFram()
 *
 * Description: Writes up to FRAM_SIZE bytes of data to the FRAM
 *              device starting at the specified address.
 *
 * Notes:       If the caller passes arguments that attempts to write
 *              past the end of FRAM the function will return FALSE
 *              without writting any data. Otherwise the data will be
 *              written then optionally read back to verify each byte
 *              was written correctly. If the data is read back but
 *              does not match what was written a FALSE is returned.
 *
 * Returns:     TRUE  if the write succeeds.
 *              FALSE if the write fails.
 *
 **********************************************************************/
boolean WriteFram(uword *data, uword address, uword num_words, boolean verify)
{
    volatile uword   count;
    volatile boolean ret_value = TRUE;
    volatile boolean fResult;
    volatile uword   uwValue;

    // Make sure address is word aligned
    address <<= 1;

    // Make sure we are not trying to write past the end of FRAM
    if ( ( address + num_words ) > FRAM_SIZE )
    {
        return(FALSE);
    }

    // Enable writes to FRAM
    EnableWriteFram(TRUE);

    // Take CS low and wait at least 90ns before clocking
    FRAM_CS_LO;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");

    // Send write command and address
    fResult = SPI_fMySendData((uword)FRAM_WRIT, &uwValue, 8);
    if (!fResult)
    {
        ret_value = FALSE;
    }
    fResult = SPI_fMySendData((uword)address, &uwValue, 16);
    if (!fResult)
    {
        ret_value = FALSE;
    }

    // Send data to be written
    for ( count = 0; count < num_words; count++)
    {
        fResult = SPI_fMySendData((uword)data[count], &uwValue, 16);
         if (fResult == FALSE)
         {
            ret_value = FALSE;
            break;
         }
    }

    if (ret_value == FALSE)
    {
        // Take CS High after tranmission is complete
        FRAM_CS_HI;
        asm(" RPT #7 || NOP");
        asm(" RPT #7 || NOP");
        return(ret_value);
    }
    // Take CS High and wait at least 100ns before taking it low again
    FRAM_CS_HI;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");
    if ( verify )
    {
        // Take CS Low and want at least 90ns before clocking
        FRAM_CS_LO;
        asm(" RPT #7 || NOP");
        asm(" RPT #7 || NOP");

        // Send Read command and address
        fResult = SPI_fMySendData((uword)FRAM_READ, &uwValue, 8);
        if (!fResult)
        {
            ret_value = FALSE;
        }
        fResult = SPI_fMySendData((uword)address, &uwValue, 16);
        if (!fResult)
        {
            ret_value = FALSE;
        }

        // Read data and verify same as data that was written
        for ( count = 0; count < num_words; count++ )
        {
            fResult = SPI_fMySendData((uword) 0x00, &uwValue, 16);
            if (( uwValue != data[count] ) || (fResult == FALSE))
            {
                ret_value = FALSE;
                break;
            }
        }

    }
    // Take CS High after tranmission is complete
    FRAM_CS_HI;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");
    return(ret_value);
}

/**********************************************************************
 *
 * Function:    ReadFram()
 *
 * Description: Reads up to FRAM_SIZE bytes of data from the FRAM
 *              device starting at the specified address.
 *
 * Notes:       If the caller passes arguments that attempts to read
 *              past the end of FRAM the function will read to the end
 *              of FRAM then return.
 *
 **********************************************************************/
boolean ReadFram(uword *data, uword address, uword num_words)
{
    volatile uword count;
    volatile uword read_data;
    volatile boolean ret_value = TRUE;
    volatile boolean fResult;

    // Make sure address is word aligned
    address <<= 1;

    // Make sure we are not trying to write past the end of FRAM
    if ( address < FRAM_SIZE )
    {
        // Take CS low and wait at least 90ns before clocking
        FRAM_CS_LO;
        asm(" RPT #7 || NOP");
        asm(" RPT #7 || NOP");

        // Send Read command and address
        fResult = SPI_fMySendData((uword)FRAM_READ, &read_data, 8);
        if (!fResult)
        {
            ret_value = FALSE;
        }
        fResult = SPI_fMySendData((uword)address, &read_data, 16);
        if (!fResult)
        {
            ret_value = FALSE;
        }

        // Read data and store it in the buffer
        for (count = 0; ((count < num_words) && ((address + count) <= FRAM_SIZE));
              count++)
        {
             fResult = SPI_fMySendData((uword)0x00, &read_data, 16);
             if (!fResult)
             {
                ret_value = FALSE;
             }
             data[count] = (uword)read_data;
        }
    }
    else
    {
        ret_value = FALSE;
    }
    // Take CS High after tranmission is complete
    FRAM_CS_HI;
    asm(" RPT #7 || NOP");
    asm(" RPT #7 || NOP");
    return (ret_value);
}
/**********************************************************************
 *
 * Function:    ubReturnRs232Enable()
 *
 * Description: Reads the RS232 enable byte from FRAM and returns
 *              it to the caller.
 *
 **********************************************************************/
uword uwReturnRs232Enable(void)
{
    volatile uword   uwData;
    volatile boolean fResult;

    fResult = ReadFram((uword *)&uwData, RS232_ENABLE_ADDR, 1);

    if (!fResult)
    {
        boot_error_handler((uword)FRAM_ERROR);
    }
    return(uwData);
}

/**********************************************************************
 *
 * Function:    vWriteRs232Enable()
 *
 * Description: Writes the RS232 enable byte from FRAM
 *
 **********************************************************************/
void vWriteRs232Enable(uword uwEnable)
{
    volatile boolean fResult;

    fResult = WriteFram((uword *)&uwEnable, RS232_ENABLE_ADDR, 1, TRUE);

    if (!fResult)
    {
        boot_error_handler((uword)FRAM_ERROR);
    }
}

/**********************************************************************
 *
 * Function:    uwReturnSwitchID_Enable()
 *
 * Description: Reads the Switch ID Enable word from FRAM and returns
 *              it to the caller.
 *
 **********************************************************************/
uword   uwReturnSwitchID_Enable(void)
{
    volatile uword   uwData;
    volatile boolean fResult;

    fResult = ReadFram((uword *)&uwData, SWITCH_ID_ENABLE_ADDR, 1);

    if (!fResult)
    {
        boot_error_handler((uword)FRAM_ERROR);
    }
    return(uwData);
}

/**********************************************************************
 *
 * Function:    vWriteSwitchID_Enable()
 *
 * Description: Writes the Switch ID Enable word to FRAM
 *
 **********************************************************************/
void vWriteSwitchID_Enable(uword uwEnable)
{
    volatile boolean fResult;

    fResult = WriteFram((uword *)&uwEnable, SWITCH_ID_ENABLE_ADDR, 1, TRUE);

    if (!fResult)
    {
        boot_error_handler((uword)FRAM_ERROR);
    }
}

/**********************************************************************
 *
 * Function:    ubReturnExternalMemoryVersion()
 *
 * Description: Reads the external memory version from FRAM and returns
 *              it to the caller.
 *
 **********************************************************************/
uword uwReturnExternalMemoryVersion(void)
{
    volatile uword   uwData;
    volatile boolean fResult;

    fResult = ReadFram((uword *)&uwData, EXT_MEM_ADDR, 1);

    if (!fResult)
    {
        boot_error_handler((uword)FRAM_ERROR);
    }
    return(uwData);
}

/**********************************************************************
 *
 * Function:    vWriteExternalMemoryVersion()
 *
 * Description: Writes the external memory version from FRAM and returns
 *              it to the caller.
 *
 **********************************************************************/
void vWriteExternalMemoryVersion(uword uwVersion)
{
    volatile boolean fResult;

    fResult = WriteFram((uword *)&uwVersion, EXT_MEM_ADDR, 1, TRUE);

    if (!fResult)
    {
        boot_error_handler((uword)FRAM_ERROR);
    }
}

/**********************************************************************
 *
 * Function:    ReturnFramRevision()
 *
 * Description: Reads the Fram Revision from FRAM and returns it to
 *              the caller.
 *
 **********************************************************************/
uword ReturnFramRevision(void)
{
   volatile uword data;

   ReadFram((uword *)&data, FRAM_REV_ADDR, 1);
   return(data);
}

/**********************************************************************
 *
 * Function:    ReturnModuleId()
 *
 * Description: Reads the Module ID from FRAM and returns it to the
 *              caller.
 *
 **********************************************************************/
uword ReturnModuleId(void)
{
   volatile uword data;

   ReadFram((uword *)&data, MODULE_ID_ADDR, 1);
   if ((data == 0) || (data > 127))
   {
       data = 1;
   }
   return((uword)data);
}

/**********************************************************************
 *
 * Function:    ReturnCANBaudRate()
 *
 * Description: Reads the Module CAN baud rate from FRAM and returns it to the
 *              caller.
 *
 **********************************************************************/
uword ReturnCANBaudRate(void)
{
    volatile uword data;
    volatile uword uwRateCode;

    ReadFram((uword *)&data, BAUD_CAN_ADDR, 1);
    switch (data)
    {
        case 1000:
             uwRateCode = 8;
             break;

        case 800:
             uwRateCode = 7;
             break;

        case 0:
        case 500:
             uwRateCode = 6;
             break;

        case 400:
             uwRateCode = 5;
             break;

        case 250:
             uwRateCode = 4;
             break;

        case 200:
             uwRateCode = 3;
             break;

        case 125:
             uwRateCode = 2;
             break;

        case 100:
             uwRateCode = 1;
             break;

        case 50:
             uwRateCode = 0;
             break;

        default:
            uwRateCode = 6;
            break;
    }
    return(uwRateCode);
}

/**********************************************************************
 *
 * Function:    ReturnModuleType()
 *
 * Description: Reads the Module Type from FRAM and stores it in the
 *              provided buffer.
 *
 **********************************************************************/
uword *ReturnModuleType(uword *buffer)
{
   ReadFram(buffer, MODULE_TYPE_ADDR, 16);
   return(buffer);
}

/**********************************************************************
 *
 * Function:    ReturnModulePN()
 *
 * Description: Reads the Module Part number from FRAM and stores it
 *              in the provided buffer.
 *
 **********************************************************************/
uword *ReturnModulePN(uword *buffer)
{
   ReadFram(buffer, MODULE_PN_ADDR, 16);
   return(buffer);
}

/**********************************************************************
 *
 * Function:    ReturnHardwarePN()
 *
 * Description: Reads the Hardware Part number from FRAM and stores it
 *              in the provided buffer.
 *
 **********************************************************************/
uword *ReturnHardwarePN(uword *buffer)
{
   ReadFram(buffer, HDWR_PN_ADDR, 16);
   return(buffer);
}

/**********************************************************************
 *
 * Function:    ReturnHardwareRev()
 *
 * Description: Reads the Hardware Revison from FRAM and stores it
 *              in the provided buffer.
 *
 **********************************************************************/
uword *ReturnHardwareRev(uword *buffer)
{
   ReadFram(buffer, HDWR_REV_ADDR, 16);
   return(buffer);
}

/**********************************************************************
 *
 * Function:    ReturnHardwareSN()
 *
 * Description: Reads the Hardware Serial number from FRAM and stores
 *              it in the provided buffer.
 *
 **********************************************************************/
uword *ReturnSerialNumber(uword *buffer)
{
   ReadFram(buffer, SERIAL_NUMBER_ADDR, 16);
   return(buffer);
}

