@echo off
pushd ..\..\
setlocal


:process_arg
if "%1"=="" goto end_process_arg
set name=%1
set value=

:process_arg_value
if NOT "%value%"=="" set value=%value% %2
if "%value%"=="" set value=%2
shift
if "%2"=="!" goto set_arg
if "%2"=="" goto set_arg
goto process_arg_value

:set_arg
set %name%=%value%
shift
shift
goto process_arg
:end_process_arg

echo. > temp_postBuildStep_Debug.bat

echo C:\Program Files\Texas Instruments\C2000 Code Generation Tools 5.2.2\bin\hex2000.exe .\Debug\F2803x_Boot.out -i -memwidth 16 -order LS -romwidth 16 -o .\Debug\TI2803x_Boot.h86 -fill 0ffffh >> temp_postBuildStep_Debug.bat



call temp_postBuildStep_Debug.bat
del temp_postBuildStep_Debug.bat

endlocal
popd
