from tkinter import *
from tkinter import ttk
from tkinter.filedialog import *
import time
import tkinter
import shutil
import errno
import os
import glob
import re
import sys
import pdb

# This script is just non sense but it has useful items like gui and looping


# Code section

def CountCallBack():
   #tkMessageBox.showinfo( "Hello Python", "Hello World")

   count=0 
   for letter in 'Python':     # First Example
    print ('Current Letter :', letter)
    count = count+1 
    print ('Current count :', count)

    # Open a file
    fo = open("foo.txt", "w", 1)
    fo.write( "Python is a great language.\n");

    # Close opend file
    fo.close()

def LocalTimeCallBack():
    localtime = time.asctime( time.localtime(time.time()) )
    print ("Local current time :", localtime)

def OnOffFlagCallBack():
    print("On: %d,\n Off: %d" % (CheckVar3.get(), CheckVar4.get())) 
    if CheckVar3.get() == 1:
       print (" CheckVar3 is 1")
    if CheckVar4.get() == 1:
       print (" CheckVar4 is 1") 

def cmd_run():
    src = txt_src.get()                 # Read the Source folder
    dest = txt_wks.get()              # Read the work space folder (sand box)
    #------------Autogen update ----------------#
    #wrkdir = dest + '/F2803x_Boot' 
    #print(wrkdir)
    #os.chdir(wrkdir)
    #os.system('MakeH86FromOutfile.bat')
    #--------------------------------------------#


    
# Widgets section
top = Tk()

# Title area pop up box
top.title("MY Message box")

#CheckVar1 = IntVar()
#CheckVar2 = IntVar()
#CheckVar3 = IntVar()
#CheckVar4 = IntVar()

# This is the top ribbon area
var = StringVar()
label = Label(top, textvariable=var, width=20, height=1, anchor='w', relief='sunken').grid(row=0, column=0) 
var.set("Make a selection")

# Check box area
CheckVar1 = IntVar()
Checkbutton(top, text = "Time", variable = CheckVar1, \
                 onvalue = 1, offvalue = 0, height=1, \
                 width = 4, command = LocalTimeCallBack ).grid(row=1, column=0 ,sticky=W )

CheckVar2 = IntVar()
Checkbutton(top, text = "Count", variable = CheckVar2, \
                 onvalue = 1, offvalue = 0, height=1, \
                 width = 4, command = CountCallBack ).grid(row=2, column=0 ,sticky=W)

CheckVar3 = IntVar()
Checkbutton(top, text = "OnOffFlagOn", variable = CheckVar3, \
                 onvalue = 1, offvalue = 0, height=1, \
                 width = 10 ).grid(row=3, column=0 ,sticky=W)

CheckVar4 = IntVar()
Checkbutton(top, text = "OnOffFlagOff", variable = CheckVar4, \
                 onvalue = 1, offvalue = 0, height=1, \
                 width = 10 ).grid(row=4, column=0, sticky=W)

CheckVar5 = IntVar()
Checkbutton(top, text = "Make h86 from outfile", variable = CheckVar5, \
                 onvalue = 1, offvalue = 0, height=1, \
                 width = 20, command = cmd_run ).grid(row=5, column=0, sticky=W)

# Button area
Button(top, text='Show ON Off States', command=OnOffFlagCallBack).grid(row=6, sticky=S, pady=4)

#C1.pack()
#C2.pack()
#C3.pack()
#C4.pack()
#C5.pack()


top.mainloop()
