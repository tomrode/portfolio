/**********************************************************************
 *
 * Filename:    errors.h
 *
 * Copyright (C) 2001 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains definitions of error codes used in
 *              the boot code.
 *
 **********************************************************************/

#ifndef ERRORS_H
#define ERRORS_H

/* ERROR flags */
typedef enum
{
    NO_ERROR_PRESENT               =  0,
    BAD_FLASH_DEVICE_CODE          =  1,
    SECTOR_PROTECTED_ERROR         =  2,
    BAD_SECTOR_ERASE_NO            =  3,
    DATA_WRITE_OUT_OF_RANGE        =  4,
    DQ7_ERASE_FAILURE              =  5,
    ERASE_VERIFY_FAILED            =  6, 
    DQ7_PROGRAM_FAILURE            =  7,
    PROGRAM_VERIFY_FAILED          =  8,
    MISSING_DATA_MSGS              =  9,
    OUTSIDE_FLASH_ADDRESS_RANGE   = 10,
    RS232_TRANSMITTER_TIMEOUT     = 11,
    CAN_REQ_OBJ_TIMEOUT           = 12,
    FRAM_ERROR                    = 13
   
}Fl_Errs;

/* Prototype of global functions   */
void boot_error_handler(uword);
    
#endif

