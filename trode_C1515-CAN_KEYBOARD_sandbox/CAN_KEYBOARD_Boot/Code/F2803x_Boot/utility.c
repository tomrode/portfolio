/**********************************************************************
 *
 * Filename:    utility.c
 *
 * Copyright (C) 2001 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains miscellaneous utility functions.
 *
 **********************************************************************/

//****************************************************************************
// @Project Includes
//****************************************************************************
#include <ctype.h>
#include "main.h"
#include "ramtron.h"

// Defines
#define FLICKER_COUNTS 10

// Ticks at 5mS
#define FLICKER_ON      10
#define FLICKER_OFF     10
#define INTER_DIGIT     200
#define ON_TIME         20
#define OFF_TIME        80

typedef enum
{
    Idle = 0,
    Flicker_On = 1,
    Flicker_Off = 2,
    InterDigit1 = 3,
    Hund_On = 4,
    Hund_Off = 5,
    InterDigit2 = 6,
    Tens_On = 7,
    Tens_Off = 8,
    InterDigit3 = 9,
    Ones_On = 10,
    Ones_Off = 11,
    InterDigit4 = 12
} FLASH_STATE_TYPE;

volatile boolean  gfFaultActive = FALSE;
uword    uwFlashCode;
sword    swHdigit;
sword    swTdigit;
sword    swOdigit;
FLASH_STATE_TYPE eFlashState;
ubyte    ubFlickerCount;
ubyte    ubFlickerOnCount;
ubyte    ubFlickerOffCount;
uword    uwInterdigitCount;
uword    uwOnTimeCount;
uword    uwOffTimeCount;

//****************************************************************************
// @Macros
//****************************************************************************

//****************************************************************************
// @Prototypes Of Local Functions
//****************************************************************************
void vDelay_ms(uword uwDelayMs);
void  vFlashCode();
void  vComputeDigits();

//****************************************************************************
// @Prototypes Of External Functions
//****************************************************************************
extern void  vFaultLedOn(boolean fTurnOn);

/**********************************************************************
 *
 * Function:    boot_error_handler()
 *
 * Description: This function is called when an error occurs in the
 *              boot that will not allow transition of control to the
 *              application code.
 *
 * Notes:       Now this function just sits in a while loop and outputs
 *              the error to the LEDs.
 *
 * Returns:     Never returns
 *
 **********************************************************************/
void boot_error_handler(uword uwError)
{
    CAN_MESSAGE TxMsg;
    uword uwDelay = 0;

    while ( 1 )
    {
        if ( uwDelay == 0 )
        {
            TxMsg.id      = 0x80 + ReturnModuleId();
            TxMsg.size    = 2;
            TxMsg.uwData[0] = (ubyte)(uwError     );
            TxMsg.uwData[1] = (ubyte)(uwError >> 8);
            CAN_write(CAN_MB_TX, &TxMsg);
        }
        gfFaultActive = TRUE;
        uwFlashCode = uwError;
        vFlashCode();
        vDelay_ms(5);
        uwDelay += 5;
        if ( uwDelay >= 1000 )
        {
            uwDelay = 0;
        }
    }
}

/**********************************************************************
 *
 * Function:    vDelay_ms()
 *
 * Description: This function waits for the specified number of
 *              milli-seconds and returns.
 *
 * Argument(s): uwDelay_ms-> 16-bit number of milli-seconds to delay
 *
 * Returns:     Nothing
 *
 * Comments:    Limits to 3.5 second delay
 *
 **********************************************************************/
void vDelay_ms(uword uwDelayMs)
{
    ulong ulTimeoutCnts;
    volatile ulong ulPassCounts;

    if ( uwDelayMs > 3500 )
    {
        uwDelayMs = 3500;
    }
    ulTimeoutCnts = (ulong)uwDelayMs * (ulong)GPT1_TIMER_2_CPMS;

    vResetIntervalTimer();
    ulPassCounts = 0;
    while ( ulGetIntervalTimer() < ulTimeoutCnts )
    {
        ulPassCounts++;
    }
}

/**********************************************************************
 *
 * Function:    MyStrLen()
 *
 * Description: This function returns the length of the specified
 *              string up to the specified maximum. The first
 *              non-printable character, if any, will be converted to
 *              a null.
 *
 * Argument(s): buffer = character pointer specifying the start address
 *              of the string.
 *              max_len = 8-bit maximum length of the string.
 *
 * Returns:     Number of printable characters in buffer not including
 *              the NULL termination.
 *
 **********************************************************************/
ubyte MyStrLen(char *buffer, ubyte max_len)
{
    ubyte   count;
    boolean string_end = FALSE;

    for ( count = 0; count < max_len && string_end == FALSE; count++ )
    {
        if ( !isprint(buffer[count]) )
        {
            string_end = TRUE;
        }
    }

    buffer[count-1] = 0;
    return(count-1);
}

ubyte ubClip(ubyte ubMin, ubyte ubValue, ubyte ubMax)
{
    if ( ubValue < ubMin )
    {
        ubValue = ubMin;
    }
    if ( ubValue > ubMax )
    {
        ubValue = ubMax;
    }
    return(ubValue);
}

/************************************************************************************************
 *  Function:   vFlashCode
 *
 *  Purpose:    State machine for the LED fault code flash system
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 10/15/2004
 *
 ************************************************************************************************
*/
void vFlashCode()
{
    // find next state
    switch ( eFlashState )
    {
        case Idle:
            if ( gfFaultActive )
            {
                ubFlickerCount = 0;
                ubFlickerOnCount = 0;
                eFlashState = Flicker_On;
            }
            break;

        case Flicker_On:
            if ( ubFlickerOnCount >= FLICKER_ON )
            {
                ubFlickerOffCount = 0;
                ubFlickerCount++;
                eFlashState = Flicker_Off;
            }
            break;

        case Flicker_Off:
            if ( ubFlickerCount >= FLICKER_COUNTS )
            {
                uwInterdigitCount = 0;
                vComputeDigits();
                eFlashState = InterDigit1;
            }
            else
            {
                if ( ubFlickerOffCount >= FLICKER_OFF )
                {
                    ubFlickerOnCount = 0;
                    eFlashState = Flicker_On;
                }
            }
            break;

        case InterDigit1:
            if ( uwInterdigitCount >= INTER_DIGIT )
            {
                uwOnTimeCount = 0;
                eFlashState = Hund_On;
            }
            break;

        case Hund_On:
            if ( uwOnTimeCount >= ON_TIME )
            {
                uwOffTimeCount = 0;
                eFlashState = Hund_Off;
            }
            break;

        case Hund_Off:
            if ( uwOffTimeCount >= OFF_TIME )
            {
                swHdigit--;
                if ( swHdigit > 0 )
                {
                    uwOnTimeCount = 0;
                    eFlashState = Hund_On;
                }
                else
                {
                    uwInterdigitCount = 0;
                    eFlashState = InterDigit2;
                }
            }
            break;

        case InterDigit2:
            if ( uwInterdigitCount >= INTER_DIGIT )
            {
                uwOnTimeCount = 0;
                eFlashState = Tens_On;
            }
            break;

        case Tens_On:
            if ( uwOnTimeCount >= ON_TIME )
            {
                uwOffTimeCount = 0;
                eFlashState = Tens_Off;
            }
            break;

        case Tens_Off:
            if ( uwOffTimeCount >= OFF_TIME )
            {
                swTdigit--;
                if ( swTdigit > 0 )
                {
                    uwOnTimeCount = 0;
                    eFlashState = Tens_On;
                }
                else
                {
                    uwInterdigitCount = 0;
                    eFlashState = InterDigit3;
                }
            }
            break;

        case InterDigit3:
            if ( uwInterdigitCount >= INTER_DIGIT )
            {
                uwOnTimeCount = 0;
                eFlashState = Ones_On;
            }
            break;

        case Ones_On:
            if ( uwOnTimeCount >= ON_TIME )
            {
                uwOffTimeCount = 0;
                eFlashState = Ones_Off;
            }
            break;

        case Ones_Off:
            if ( uwOffTimeCount >= OFF_TIME )
            {
                swOdigit--;
                if ( swOdigit > 0 )
                {
                    uwOnTimeCount = 0;
                    eFlashState = Ones_On;
                }
                else
                {
                    uwInterdigitCount = 0;
                    eFlashState = InterDigit4;
                }
            }
            break;

        case InterDigit4:
            if ( uwInterdigitCount >= INTER_DIGIT )
            {
                eFlashState = Flicker_On;
                ubFlickerCount = 0;
                ubFlickerOnCount = 0;
                if ( gfFaultActive )
                {
                    eFlashState = Flicker_On;
                }
                else
                {
                    eFlashState = Idle;
                }
            }
            break;

        default:
            eFlashState = Idle;
            break;

    }

    // find state output
    switch ( eFlashState )
    {
        case Idle:
            LED_OFF();
            break;

        case Flicker_On:
            ubFlickerOnCount++;
            LED_ON();
            break;

        case Flicker_Off:
            ubFlickerOffCount++;
            LED_OFF();
            break;

        case InterDigit1:
        case InterDigit2:
        case InterDigit3:
        case InterDigit4:
            LED_OFF();
            uwInterdigitCount++;
            break;

        case Hund_On:
        case Tens_On:
        case Ones_On:
            uwOnTimeCount++;
            LED_ON();
            break;

        case Hund_Off:
        case Tens_Off:
        case Ones_Off:
            uwOffTimeCount++;
            LED_OFF();
            break;


        default:
            LED_OFF();
            break;

    }

}


/************************************************************************************************
 *  Function:   vComputeDigits
 *
 *  Purpose:    Computes the hundreds, tens, and ones digits of the code to flash
 *
 *  Input:      N/A
 *
 *  Output:     N/A
 *
 *  Return:     N/A
 *
 *  Globals:    Indicated by 'g' prefix on variable names
 *
 *  Author:     Bill McCroskey
 *
 *  Revision History:
 *              Written 10/15/2004
 *
 ************************************************************************************************
*/
void vComputeDigits()
{
    swHdigit = (sword) uwFlashCode / 100;
    swTdigit = ((sword)uwFlashCode - (swHdigit * 100)) / 10;
    swOdigit = (sword)uwFlashCode - (swHdigit * 100) - (swTdigit * 10);
    if ( swHdigit == 0 )
    {
        swHdigit = 10;
    }

    if ( swTdigit == 0 )
    {
        swTdigit = 10;
    }

    if ( swOdigit == 0 )
    {
        swOdigit = 10;
    }
}
