/*********************************************************************
 *
 * Filename:    appload.c
 *
 * Copyright (C) 2005 Crown Equipment Corporation. All rights reserved.
 *
 * Description: This file contains the functions to communicate with
 *              application download utility. It will call functions
 *              to program code into FLASH memory.
 *
 **********************************************************************/

//****************************************************************************
// @Project Includes
//****************************************************************************
#include <string.h>
#include <stdio.h>
#include "main.h"
#include "appload.h"
#include "flash.h"
#include "ramtron.h"

#include "DSP2803x_GlobalPrototypes.h"
#include "Flash2803x_API_Library.h"

#define RAM_START 0x009000L
#define RAM_END   0x009FFFL

// Get info about the flashrel section

//****************************************************************************
// @Global Veriables
//****************************************************************************
void *BootProgram;

//****************************************************************************
// @Local Veriables
//****************************************************************************
#if _ON_TESTER == 1
static const char BootSoftwarePartNumber[] = "147914-001-01D";
#else
static const char BootSoftwarePartNumber[] = "147914-001-01";
#endif

//****************************************************************************
// @External Variables
//****************************************************************************
extern boolean fRs232Enabled;
extern uword   uwMemoryVersion;
extern uword   uwFlashBuffer[FLASH_BUFFER_WORDS];

//****************************************************************************
// @External Prototypes
//****************************************************************************
extern void  vDelay_ms(uword uwDelayMs);
extern ubyte MyStrLen(char *, ubyte);
extern uword crc16_range(ulong, uword, uword *);
extern uword cks16_range(ulong, uword, uword *);
extern ubyte ubClip(ubyte ubMin, ubyte ubValue, ubyte ubMax);
extern void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr);

//****************************************************************************
// @Prototypes Of Local Functions
//****************************************************************************
static boolean AnswerCommand(Obj_type *);
static boolean GetRxObject(Obj_type *);
static boolean GetRxObjectCAN(Obj_type *);
void    SendTxObject(Obj_type *);
static void    SendTxObjectCAN(Obj_type *, Send_type);
void           ConfigureRxMailboxes(uword, Send_type);
uword          CalcAppSfwrCrcCks(CALC_TYPE);
void           SendBootupMessage(uword);
static void    vAnswerStoreData(Obj_type *obj);
static void    vAnswerReadData(Obj_type *obj);
static void    vAnswerRunBootLoadProgram(Obj_type *obj);
static void    vAnswerEraseAppSection(Obj_type *obj);
static void    vAnswerBufferData(Obj_type *obj);
static void    vAnswerProgramBuffer(Obj_type *obj);
static boolean fConfigureRxBuffer(Obj_type *obj);
static void    vCopyGenMbxToObj(Obj_type *obj);
static boolean fSendGenMbx(Obj_type *obj);
void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr);

uword *SiliconRev = (uword *)0x883;

/**********************************************************************
 *
 * Function:    ExecuteCommands()
 *
 * Description: This function receives commands from the PC via the
 *              RS-232 or CAN port. It then takes the appropriate
 *              action then in most cases send a message back to the
 *              PC. If a command is received via RS-232 and its
 *              destination is another module, the message is relayed
 *              over CAN to the intended target.
 *
 * Argument(s): None
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void ExecuteCommands(uword uwModuleId)
{
    Obj_type MyObject;
    boolean  done_prog  = FALSE;

    // Answer or relay commands until PC issues ends the session
    do
    {
        // Get a message from CAN
        if ( GetRxObject((Obj_type *)&MyObject) )
        {
            if ( MyObject.module_id == uwModuleId )
            {
                // Target is this module
                done_prog = AnswerCommand((Obj_type *)&MyObject);
            }
        }
    }  while ( !done_prog );
}

/******************************************************************************
 *
 * Function:    IdentifyModulesOnNetwork()
 *
 * Description: This function is called when a module receives a message from
 *              the PC with the index "idxReturnModulesOnNetwork". The module
 *              will only receive this message if it is the module that sends
 *              the boot-up message that the PC receives FIRST (either from
 *              CAN or RS-232).
 *
 *              The function will ask each possible module from ID 1 to 127 to
 *              send its boot up message allowing a maximum of 10ms to receive
 *              the response. The module will skip its own ID.
 *
 *              The IDs of all modules present on the network is sent to the
 *              PC using the "idxReturnModuleOnNetwork" index.
 *
 * Argument(s): obj -> Pointer to the Obj_type structure that is used to
 *              detect other modules on the network.
 *
 * Returns:     Nothing
 *
 ******************************************************************************/
static void IdentifyModulesOnNetwork(Obj_type *obj)
{
    Obj_type  tx_obj;
    uword     id_count;
    uword     Module_id;
    ulong     ulTimeoutCnt;

    // Get the module ID of this module.
    Module_id = ReturnModuleId();

    // Initialize the transmit message
    memset((void *)&tx_obj, 0, sizeof(Obj_type));
    tx_obj.command   = cmdReadAnswer;
    tx_obj.index     = idxReturnModulesOnNetwork;
    tx_obj.module_id = Module_id;
    tx_obj.source    = obj->source;

    // Try to detect all possible modules (ID = 1 - 127)
    for ( id_count = 1; id_count < 128; id_count++ )
    {
        // Check if the ID belongs to this module
        if ( Module_id == id_count )
        {
            // Yes, put module ID in list
            tx_obj.data[tx_obj.length++] = (ubyte)Module_id;
        }
        else
        {
            // No, Configure receive mailbox for the current ID
            ConfigureRxMailboxes(id_count, PC_EMUL);

            // Send the inquiry
            memset(obj, 0, sizeof(Obj_type));
            obj->module_id = id_count;
            obj->command   = cmdOtherEvent;
            obj->index     = idxSendBootUpMessage;
            obj->length    = 0;
            SendTxObjectCAN(obj, PC_EMUL);

            // Wait up to 10ms for a response
            ulTimeoutCnt = BOOTUP_TIMEOUT_CNTS;
            vResetIntervalTimer();
            do
            {
                if ( GetRxObject(obj) )
                {
                    if ( obj->index   == idxBootUpMessage
                         && obj->command == cmdOtherEvent
                       )
                    {
                        tx_obj.data[tx_obj.length++] = obj->module_id;
                    }
                }
            } while ( ulGetIntervalTimer() < ulTimeoutCnt );
        }
    }
    // Send the list of all modules on network to PC
    SendTxObject((Obj_type *)&tx_obj);

    // Re-configure the receive mailboxes for this module
    ConfigureRxMailboxes(Module_id, MODULE);
}

/******************************************************************************
 *
 * Function:    AnswerCommand()
 *
 * Description: After a command direct to this module has been received,
 *              this function is called to take the appropriate action which
 *              may include sending a response back to the PC.
 *
 * Argument(s): obj -> Pointer to the Obj_type structure that holds the command
 *              that will be responded to. This function also uses this space
 *              to hold the response data.
 *
 * Returns:     TRUE if the PC has informed the module to end the session
 *              otherwise FALSE.
 *
 ******************************************************************************/
static boolean AnswerCommand(Obj_type *obj)
{
    uword   uwTemp;
    ulong   ulTemp;
    uword   far *addr_ptr;
    boolean ret_value   = FALSE;

    // Check which index was received
    switch ( obj->index )
    {
        case idxSendBootUpMessage:                      // 0x5002
            /* Send the boot up message */
            if ( obj->command == cmdOtherEvent )
            {
                SendBootupMessage(ReturnModuleId());
            }
            break;

        case idxStoreData:                              // 0x5010
            /* Write data to RAM */
            if ( obj->command == cmdWriteData )
            {
                vAnswerStoreData(obj);
            }

            if ( obj->command == cmdReadData )
            {
                vAnswerReadData(obj);
            }
            break;

        case idxRunBootLoadProgram:                     // 0x5011
            /* Run program from specified address */
            if ( obj->command == cmdOtherEvent )
            {
                vAnswerRunBootLoadProgram(obj);
            }
            break;

        case idxEraseAppSection:                        // 0x5012
            /* Erase FLASH */
            if ( obj->command == cmdWriteData )
            {
                vAnswerEraseAppSection(obj);
            }
            break;

        case idxBufferData:                             // 0x5013
            /* Write Data to buffer */
            if ( obj->command == cmdWriteData )
            {
                vAnswerBufferData(obj);
            }
            break;

        case idxProgramBuffer:                          // 0x5015
            /* Write Data to buffer */
            if ( obj->command == cmdWriteData )
            {
                vAnswerProgramBuffer(obj);
            }
            break;

        case idxConfigRxBuffer:                         // 0x5016
            if ( obj->command == cmdOtherEvent )
            {
                if ( fConfigureRxBuffer(obj) )
                {
                    obj->command = cmdOpPassed;
                }
                else
                {
                    obj->command = cmdOpFailed;
                }
                SendTxObject(obj);
            }
            break;

        case idxGetRxData:                              // 0x5017
            if ( obj->command == cmdReadData )
            {
                vCopyGenMbxToObj(obj);
                SendTxObject(obj);
            }
            break;

        case idxSendTxData:                             // 0x5018
            if ( obj->command == cmdWriteData )
            {
                if ( fSendGenMbx(obj) )
                {
                    obj->command = cmdOpPassed;
                }
                else
                {
                    obj->command = cmdOpFailed;
                }
                obj->length = 0;
                SendTxObject(obj);
            }
            break;

        case idxEndSession:                             // 0x5014
            /* End communication session with PC */
            if ( obj->command == cmdOtherEvent )
            {
                obj->length = 0;
                SendTxObject(obj);
                vDelay_ms(10);
                ret_value = TRUE;
            }
            break;

        case idxReturnModulesOnNetwork:                 // 0x5020
            /* Identify modules on the network */
            if ( obj->command == cmdReadData )
            {
                IdentifyModulesOnNetwork(obj);
            }
            break;

        case idxReturnManCode:                          // 0x5021
            /* Read manufacturers code from FLASH */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                obj->data[0] = 0;
                obj->length  = 1;
                SendTxObject(obj);
            }
            break;

        case idxReturnDevCode:                          // 0x5022
            /* Read device code from FLASH */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                uwTemp   = *SiliconRev;
                obj->data[0] = (uwTemp     );
                obj->data[1] = (uwTemp >> 8);
                obj->length  = 2;
                SendTxObject(obj);
            }
            break;

        case idxReturnInfoStart:                        // 0x5023
            /* Return start address of the info block */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                obj->data[0] = (uword)(ADDR_INFO_START      );
                obj->data[1] = (uword)(ADDR_INFO_START >>  8);
                obj->data[2] = (uword)(ADDR_INFO_START >> 16);
                obj->data[3] = (uword)(ADDR_INFO_START >> 24);
                obj->length  = 4;
                SendTxObject(obj);
            }
            break;

        case idxFramRevision:                           // 0x5030
            /* Read FRAM mapping revision */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                uwTemp   = ReturnFramRevision();
                obj->data[0] = (uwTemp     );
                obj->data[1] = (uwTemp >> 8);
                obj->length  = 2;
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                WriteFram(&obj->data[0], FRAM_REV_ADDR, 1, FALSE);
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxModuleId:                               // 0x5031
            /* Return module's node ID */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                obj->data[0] = ReturnModuleId();
                obj->length  = 1;
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                uwTemp = (uword)obj->data[0];
                WriteFram(&uwTemp, MODULE_ID_ADDR, 1, FALSE);
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxModuleType:                             // 0x5032
            /* Return module type (Display etc) */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ReturnModuleType(&obj->data[0]);
                obj->length = MyStrLen((char *)&obj->data[0], sizeof(obj->data));
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                WriteFram(&obj->data[0],
                          MODULE_TYPE_ADDR,
                          ubClip(0, obj->length+1, 16),
                          FALSE
                         );
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxModulePN:                               // 0x5033
            /* Return module part number */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ReturnModulePN(&obj->data[0]);
                obj->length = MyStrLen((char *)&obj->data[0], sizeof(obj->data));
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                WriteFram(&obj->data[0],
                          MODULE_PN_ADDR,
                          ubClip(0, obj->length+1, 16),
                          FALSE
                         );
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxSerialNumber:                           // 0x5034
            /* Return module serial number */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ReturnSerialNumber(&obj->data[0]);
                obj->length = MyStrLen((char *)&obj->data[0], sizeof(obj->data));
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                WriteFram(&obj->data[0],
                          SERIAL_NUMBER_ADDR,
                          ubClip(0, obj->length+1, 16),
                          FALSE
                         );
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxHardwarePN:                             // 0x5035
            /* Return Hardware part number */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ReturnHardwarePN(&obj->data[0]);
                obj->length = MyStrLen((char *)&obj->data[0], sizeof(obj->data));
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                WriteFram(&obj->data[0],
                          HDWR_PN_ADDR,
                          ubClip(0, obj->length+1, 16),
                          FALSE
                         );
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxHardwareRev:                            // 0x5036
            /* Return Hardware part revision */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ReturnHardwareRev(&obj->data[0]);
                obj->length = MyStrLen((char *)&obj->data[0], sizeof(obj->data));
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                WriteFram(&obj->data[0],
                          HDWR_REV_ADDR,
                          ubClip(0, obj->length+1, 16),
                          FALSE
                         );
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxBootSoftwarePN:                         // 0x5037
            /* Return Boot sfwr part number */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                strcpy((char *)&obj->data[0], (char *)&BootSoftwarePartNumber);
                obj->length = MyStrLen((char *)&obj->data[0], sizeof(obj->data));
                SendTxObject(obj);
            }
            break;

        case idxCalcBootSoftwareCRC:                  // 0x5038
            /* Return Boot sfwr crc */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                uwTemp   = crc16_range((ulong )(ADDR_FLASH_END - ADDR_BOOT_START), 0xFFFF, (uword *)ADDR_BOOT_START);
                obj->data[0] = (uword)(uwTemp     );
                obj->data[1] = (uword)(uwTemp >> 8);
                obj->length  = 2;
                SendTxObject(obj);
            }
            break;

        case idxAppSoftwarePN:                          // 0x5039
            /* Return Application sfwr part number */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                memcpy((void *)&obj->data[0], (void *)ADDR_APP_SFWR_PN, sizeof(obj->data));
                obj->length = MyStrLen((char *)&obj->data[0], sizeof(obj->data));
                SendTxObject(obj);
            }
            break;

        case idxStoredAppSoftwareCKS:                   // 0x503A
            /* Return stored application sfwr checksum */
            /* (Return 0 if cks flag = 1234)           */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;

                if ( *((uword far *)ADDR_CKS_FLAG) == 0x1234 )
                {
                    obj->data[0] = 0;
                    obj->data[1] = 0;
                }
                else
                {
                    addr_ptr     =  (uword far *)ADDR_APP_SFWR_CKS;
                    obj->data[0] = *(addr_ptr) & 0xFF;
                    obj->data[1] = *(addr_ptr) >> 8;

                }
                obj->length  = 2;
                SendTxObject(obj);
            }
            break;

        case idxStoredAppSoftwareCRC:                   // 0x503B
            /* Return stored application sfwr crc    */
            /* (Return 0 if cks flag = 1234)         */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;

                if ( *((uword far *)ADDR_CKS_FLAG) == 0x1234 )
                {
                    obj->data[0] = 0;
                    obj->data[1] = 0;
                }
                else
                {
                    addr_ptr     =  (uword far *)ADDR_APP_SFWR_CRC;
                    obj->data[0] = *(addr_ptr) & 0xFF;
                    obj->data[1] = *(addr_ptr) >> 8;
                }
                obj->length  = 2;
                SendTxObject(obj);
            }
            break;

        case idxCalculatedAppSoftwareCRC:               // 0x503C
            /* Return calculated application sfwr crc */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                uwTemp   = CalcAppSfwrCrcCks(CRC);
                obj->data[0] = (uwTemp     );
                obj->data[1] = (uwTemp >> 8);
                obj->length  = 2;
                SendTxObject(obj);
            }
            break;

        case idxCalculatedAppSoftwareCKS:               // 0x503D
            /* Return calculated application sfwr checksum */
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                uwTemp   = CalcAppSfwrCrcCks(CKS);
                obj->data[0] = (uwTemp     );
                obj->data[1] = (uwTemp >> 8);
                obj->length  = 2;
                SendTxObject(obj);
            }
            break;

        case idxCanBaud:                              // 0x503E
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ReadFram(&uwTemp, BAUD_CAN_ADDR, 1);
                obj->data[0] = (uwTemp & 0xFF );
                obj->data[1] = (uwTemp >> 8);
                obj->length  = 2;
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                uwTemp = (obj->data[1] << 8) | obj->data[0];
                WriteFram(&uwTemp, BAUD_CAN_ADDR, 1, FALSE);
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxRs232Baud:                            // 0x5040
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ReadFram(&uwTemp, BAUD_232_ADDR, 1);
                obj->data[0] = (uwTemp  & 0xFF    );
                obj->data[1] = (uwTemp >>  8 );
                obj->length  = 2;
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                uwTemp = (obj->data[1] << 8) | obj->data[0];
                WriteFram(&uwTemp, BAUD_232_ADDR, 2, FALSE);
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxRs232Enable:                         // 0x5041
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ulTemp = (ulong)0;   // no RS-232 support on this module
                obj->data[0] = (uword)ulTemp & 0xFF;
                obj->length  = 1;
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                vWriteRs232Enable(0);  // no RS-232 support on this module
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxExternalMemoryMap:                   // 0x5042
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ulTemp = (ulong)uwReturnExternalMemoryVersion();
                obj->data[0] = (uword)(ulTemp & 0xFF);
                obj->length  = 1;
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                vWriteExternalMemoryVersion(obj->data[0]);
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        case idxSwitchIDEnable:                     // 0x5043
            if ( obj->command == cmdReadData )
            {
                obj->command = cmdReadAnswer;
                ulTemp = (ulong)uwReturnSwitchID_Enable();
                obj->data[0] = (uword)ulTemp & 0xFF;
                obj->length  = 1;
                SendTxObject(obj);
            }
            else if ( obj->command == cmdWriteData )
            {
                obj->command = cmdWriteAnswer;
                vWriteSwitchID_Enable((uword)obj->data[0]);
                obj->length  = 0;
                SendTxObject(obj);
            }
            break;

        default:
            obj->command = cmdUnsupported;
            obj->length  = 0;
            SendTxObject(obj);
            break;
    }
    return(ret_value);
}


/**********************************************************************
 *
 * Function:    GetRxObject()
 *
 * Description: This function is used to receive an object either by
 *              the RS-232 port or by the CAN port. The receiver is
 *              checked to see if data is waiting in the buffer. If so
 *              the function GetRxObject232() or GetRxObjectCAN() is
 *              called. The RS-232 port has higher priority since it
 *              is more likely to loose data if we do not receive it
 *              immediately.
 *
 * Argument(s): Object-> Pointer to the Obj_type structure where the
 *              receive data should be stored.
 *
 * Returns:     TRUE  if a complete object was received
 *              FALSE if a complete object was not received
 *
 **********************************************************************/
static boolean GetRxObject(Obj_type *Object)
{
    boolean ret_value = FALSE;

    if ( CAN_uwNewData(CAN_MB_RX_CMD) )
    {
        ret_value = GetRxObjectCAN(Object);
    }
    return(ret_value);
}

/**********************************************************************
 *
 * Function:    GetRxObjectCAN()
 *
 * Description: This function is called by GetRxObject() if the command
 *              message is received (CAN_MB_RX_CMD). This function will
 *              receive messages from all the receive mailboxes and
 *              place it in the specified object structure.
 *
 * Argument(s): Object-> Pointer to the Obj_type structure here the
 *              receive data should be stored.
 *
 * Returns:     TRUE  if a complete object was received
 *              FALSE if a complete object was not received
 *
 **********************************************************************/
static boolean GetRxObjectCAN(Obj_type *Object)
{
    CAN_MESSAGE msg;
    ubyte   msg_count;
    ubyte   obj_count        = 0;
    boolean msg_received     = FALSE;
    ubyte   mbx_data_array[] = { CAN_MB_RX_DATA1,
        CAN_MB_RX_DATA2,
        CAN_MB_RX_DATA3,
        CAN_MB_RX_DATA4};

    // Initialize the recive object
    memset(Object, 0, sizeof(Obj_type));

    // Get the command message
    if ( CAN_read(CAN_MB_RX_CMD, &msg) )
    {
        // Check if there are a 8 bytes in the command message
        if ( msg.size == 8 )
        {
            // Save contents of the command message to the object
            if ( msg.id < CAN_ID_RX_CMD )
            {  // Message came from another module to be relayed to PC
                Object->module_id = (ubyte)(msg.id - CAN_ID_TX_CMD);
            }
            else
            {  // Message came from PC
                Object->module_id = (ubyte)(msg.id - CAN_ID_RX_CMD);
            }
            Object->command = (Cmd_type)msg.uwData[0];
            Object->index   = (Index_type)(msg.uwData[1] + (msg.uwData[2] << 8));
            Object->length  = msg.uwData[3];
            memcpy((void *)&Object->cmd_reg[0], (void *)&msg.uwData[4], sizeof(Object->cmd_reg));

            // Check if there should be additional messages
            if ( Object->length )
            {
                // Pack object with data from up to 4 CAN messages
                for ( msg_count = 0; msg_count < 4; msg_count++ )
                {
                    if ( CAN_read(mbx_data_array[msg_count], &msg) )
                    {
                        memcpy((void *)&Object->data[obj_count], (void *)&msg.uwData[0], msg.size );
                        obj_count += msg.size;
                    }
                }
            }
        }
        // Check if the data field was packed with correct number of bytes
        if ( obj_count   == Object->length )
        {
            msg_received   = TRUE;
            Object->source = PORT_CAN;
        }
    }
    return(msg_received);
}

/**********************************************************************
 *
 * Function:    SendTxObject()
 *
 * Description: This function is used to transmit an object either by
 *              the RS-232 port or by the CAN port as specified by
 *              Obj->source.
 *
 * Argument(s): Obj-> Pointer to the Obj_type structure to be sent.
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void SendTxObject(Obj_type *Obj)
{
    SendTxObjectCAN(Obj, MODULE);
}

/******************************************************************************
 *
 * Function:    SendTxObjectCAN()
 *
 * Description: This function is called by SendTxObject() if obj->source is
 *              PORT_CAN. It is used to transmit an object using one or more
 *              CAN messages. The data message are sent first then the command
 *              message which indicates that the entire object has been sent.
 *
 * Argument(s): Obj      -> Pointer to the Obj_type structure to be sent.
 *              send_mode-> PC_EMUL = Relay message from PC to another module
 *                          MODULE  = Send message to PC
 *
 * Returns:     Nothing
 *
 **********************************************************************/
static void SendTxObjectCAN(Obj_type *obj, Send_type send_mode)
{
    ubyte msg_count;
    ubyte obj_count = 0;
    uword can_id_array[5];
    CAN_MESSAGE tx_msg;

    // Which IDs do we need to use
    if ( send_mode == MODULE )
    {
        // Message destination is the PC
        can_id_array[0] = CAN_ID_TX_DATA1;
        can_id_array[1] = CAN_ID_TX_DATA2;
        can_id_array[2] = CAN_ID_TX_DATA3;
        can_id_array[3] = CAN_ID_TX_DATA4;
        can_id_array[4] = CAN_ID_TX_CMD;
    }
    else
    {
        // Message destination is another module (relayed from PC)
        can_id_array[0] = CAN_ID_RX_DATA1;
        can_id_array[1] = CAN_ID_RX_DATA2;
        can_id_array[2] = CAN_ID_RX_DATA3;
        can_id_array[3] = CAN_ID_RX_DATA4;
        can_id_array[4] = CAN_ID_RX_CMD;
    }

    // Send the data messages ( for loop will pack and send up to 4 messages )
    for ( msg_count = 0; msg_count < 4; msg_count++ )
    {
        tx_msg.id   = can_id_array[msg_count] + obj->module_id;
        tx_msg.size = obj->length - obj_count;
        if ( tx_msg.size > 8 )
        {
            tx_msg.size = 8;
        }
        memcpy((void *)&tx_msg.uwData[0], (void *)&obj->data[obj_count], tx_msg.size);
        obj_count += tx_msg.size;
        if ( tx_msg.size )
        {
            CAN_write(CAN_MB_TX, &tx_msg);
            vDelay_ms(2);
        }
    }

    // Send the command message
    tx_msg.id      = can_id_array[4] + obj->module_id;
    tx_msg.size    = 8;
    tx_msg.uwData[0] = (uword)(obj->command);
    tx_msg.uwData[1] = (uword)(obj->index & 0xFF);
    tx_msg.uwData[2] = (uword)(obj->index >>8);
    tx_msg.uwData[3] = obj->length;
    memcpy((void *)&tx_msg.uwData[4], (void *)&obj->cmd_reg[0], sizeof(obj->cmd_reg));
    CAN_write(CAN_MB_TX, &tx_msg);
}

/******************************************************************************
 *
 * Function:    ConfigureRxMailboxes()
 *
 * Description: This function configures the 5 receive mailboxes used to
 *              transfer data over the CAN network.
 *
 * Argument(s): module_id-> Specifies the module's messages to be received
 *              send_mode-> PC_EMUL = Relay message from another module to PC
 *                          MODULE  = Receive messages from PC
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void ConfigureRxMailboxes(uword module_id, Send_type send_mode)
{
    TCAN_SWObj msg;
    uword      can_id_array[5];

    // How will mailboxes be used
    if ( send_mode == MODULE )
    {
        // Receive messages from PC
        can_id_array[0] = CAN_ID_RX_DATA1;
        can_id_array[1] = CAN_ID_RX_DATA2;
        can_id_array[2] = CAN_ID_RX_DATA3;
        can_id_array[3] = CAN_ID_RX_DATA4;
        can_id_array[4] = CAN_ID_RX_CMD;
    }
    else
    {
        // Relay messages from another module to PC
        can_id_array[0] = CAN_ID_TX_DATA1;
        can_id_array[1] = CAN_ID_TX_DATA2;
        can_id_array[2] = CAN_ID_TX_DATA3;
        can_id_array[3] = CAN_ID_TX_DATA4;
        can_id_array[4] = CAN_ID_TX_CMD;
    }

    // Configure the mailboxes
    msg.fTransmit = FALSE;
    msg.ulMSGID = can_id_array[0] + module_id;
    msg.uwLength = 8;
    CAN_vConfigMsgObj(CAN_MB_RX_DATA1, &msg);
    msg.ulMSGID = can_id_array[1] + module_id;
    CAN_vConfigMsgObj(CAN_MB_RX_DATA2, &msg);
    msg.ulMSGID = can_id_array[2] + module_id;
    CAN_vConfigMsgObj(CAN_MB_RX_DATA3, &msg);
    msg.ulMSGID = can_id_array[3] + module_id;
    CAN_vConfigMsgObj(CAN_MB_RX_DATA4, &msg);
    msg.ulMSGID = can_id_array[4] + module_id;
    CAN_vConfigMsgObj(CAN_MB_RX_CMD,   &msg);
}

/******************************************************************************
 *
 * Function:    CalcAppSfwrCrcCks()
 *
 * Description: This function calculates the checksum or crc for the area(s) of
 *              memory specified the table in the information block.
 *
 *              Table Format:
 *
 *              ADDR_MEM_MAP+0x00 num_blocks      0 to 8
 *              ADDR_MEM_MAP+0x02 start_address   block 1
 *              ADDR_MEM_MAP+0x06 end_address     block 1
 *              ADDR_MEM_MAP+0x0A start_address   block 2
 *              ADDR_MEM_MAP+0x0E end_address     block 2
 *                :
 *                :
 *              ADDR_MEM_MAP+0x3A start_address   block 8
 *              ADDR_MEM_MAP+0x3E end_address     block 8
 *
 * Argument(s): calc-> CKS = Calculate the checksum
 *                     CRC = Calculate the crc
 *
 * Returns:     16-bit checksum or crc
 *
 **********************************************************************/
uword CalcAppSfwrCrcCks(CALC_TYPE Calc)
{
    ubyte ubBlock;
    ubyte ubNumBlocks;
    ulong ulBlockStart;
    ulong ulBlockEnd;
    ulong ulBlockSize;
    uword uwResult = 0;
    ulong *pulAddrPtr = (ulong *)(ADDR_MEM_MAP + 2); // skip NumBlocks and alignment word

    // Initialize crc to 0xFFFF or cks to 0
    if ( Calc == CRC )
    {
        uwResult = 0xFFFF;
    }

    // Get number of blocks to use
    ubNumBlocks = *(uword *)ADDR_MEM_MAP;

    // Only do calculation if number of blocks is 1 - 8
    if ( ubNumBlocks >= 1 && ubNumBlocks <= 8 )
    {
        for ( ubBlock = 0; ubBlock < ubNumBlocks; ubBlock++ )
        {
            // Do calculation for each block seeding the result to the next block
            ulBlockStart = *(pulAddrPtr);
            pulAddrPtr++;
            ulBlockEnd   = *(pulAddrPtr);
            pulAddrPtr++;
            ulBlockSize  = (ulBlockEnd - ulBlockStart) + 1;

            if ( Calc == CRC )
            {
                uwResult = crc16_range(ulBlockSize, uwResult, (uword *)ulBlockStart);
            }
            else
            {
                uwResult = cks16_range(ulBlockSize, uwResult, (uword  *)ulBlockStart);
            }
        }
    }
    // Return result
    return(uwResult);
}

/**********************************************************************
 *
 * Function:    SendBootUpMessage()
 *
 * Description: This function sends the boot up message via both the
 *              CAN port and RS-232 port.
 *
 * Argument(s): module_id-> The ID of the source module
 *
 * Returns:     Nothing
 *
 **********************************************************************/
void SendBootupMessage(uword module_id)
{
    Obj_type Object;

    /* Configure the boot-up message */
    memset(&Object, 0, sizeof(Obj_type));
    Object.command    = cmdOtherEvent;
    Object.index      = idxBootUpMessage;
    Object.module_id  = module_id;
    Object.cmd_reg[0] = (uword)TMS320F281x;    // Processor type
    Object.cmd_reg[1] = FLASH_BUFFER_SIZE; // Flash page buffer size
    Object.cmd_reg[2] = FLASH_ERASE_STATE; // Flash erase level
    Object.cmd_reg[3] = (ubyte)VENDOR_ID;
    Object.length     = 0;

    /* Send via CAN port       */
    Object.source     = PORT_CAN;
    SendTxObject(&Object);

}

/**********************************************************************
 *
 * Function:    FlashUtilities2Ram()
 *
 * Description: This function uses Keil extensions located in srom.h
 *              to copy the FLASH programming utilities from FLASH to
 *              RAM. This is necessary since the FLASH device requires
 *              that no code executes while the device is erased or
 *              programmed. Refer to Keil APNT_138 for details.
 *
 * Argument(s): None
 *
 * Returns:     Nothing
 *
 *
 * Comments:    WARNING
 *
 *              The Keil project file reserves memory from 0xC000 to
 *              0xD7FF for the FLASH programming utilities. If the
 *              code in flashrel.c ever exceeds this space, this
 *              process will fail without warning. Reference the
 *              fixedbt.m66 file for the size of the FLASHREL section.
 *
 **********************************************************************/
void FlashUtilities2Ram(void)
{
#if 0
    // Copy the Flash API functions to SARAM
    MemCopy(&Flash28_API_LoadStart, &Flash28_API_LoadEnd, &Flash28_API_RunStart);
#endif

    // We must also copy required user interface functions to RAM.
    MemCopy(&RamfuncsLoadStart, &RamfuncsLoadEnd, &RamfuncsRunStart);
}

/*------------------------------------------------------------------
  Simple memory copy routine to move code out of flash into SARAM
-----------------------------------------------------------------*/
void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr)
{
    while ( SourceAddr < SourceEndAddr )
    {
        *DestAddr++ = *SourceAddr++;
    }
    return;
}

/**********************************************************************
 *
 * Function:    PCpresent()
 *
 * Description: This function watches for the idxPcPresent message from
 *              the PC which could appear on the CAN or RS-232 port. It
 *              will watch for a maximum of 1 seconds before giving up.
 *
 * Argument(s): None
 *
 * Returns:     TRUE if the PC was detected otherwise FALSE
 *
 * Comments:    The function provides for the possibility of receiving
 *              multiple messages and simply returns TRUE after the
 *              first one.
 *
 **********************************************************************/
boolean PCpresent(void)
{
    Obj_type obj;
    static   boolean ret_value = FALSE;
    ulong    timeout_count     = PC_PRESENT_TIMEOUT_CNTS;
    ulong    time_value;

    vResetIntervalTimer();

    if ( ret_value == FALSE )
    {
        do
        {
            if ( GetRxObject((Obj_type *)&obj)
                 && obj.command == cmdOtherEvent
                 && obj.index   == idxPcPresent
               )
            {
                ret_value = TRUE;
            }
            time_value = ulGetIntervalTimer();

        } while ( (ret_value == FALSE) && (time_value < timeout_count) );
    }

    return(ret_value);
}

static void vAnswerStoreData(Obj_type *obj)
{
    ubyte count;
    ulong seq_address;
    uword *puwData;

    seq_address = ((ulong)obj->cmd_reg[3] << 24)
                  + ((ulong)obj->cmd_reg[2] << 16)
                  + ((ulong)obj->cmd_reg[1] <<  8)
                  +  (ulong)obj->cmd_reg[0];

    // restrict this to unsecured RAM area only.
    // do not allow read of secure memory areas
    if ((seq_address >= RAM_START) && (seq_address <= RAM_END))
    {
        /* seq_address is in terms of bytes, compute in terms of words */
        puwData = (uword *)seq_address;

        for ( count = 0; count < obj->length; count+=2, puwData++ )
        {
            // count is in bytes, convert to words
            *puwData = ((uword)obj->data[count+1]<<8) | obj->data[count];
        }

        obj->command = cmdWriteAnswer;
        obj->length  = 0;
    }
    else
    {
        obj->command = cmdUnsupported;
        obj->length  = 0;
    }
    SendTxObject(obj);
}

static void vAnswerReadData(Obj_type *obj)
{
    ubyte ubCount;
    ubyte ubNumBytes;
    ulong ulSeqAddress;
    uword *puwData;


    ulSeqAddress = ((ulong)obj->cmd_reg[3] << 24)
                   + ((ulong)obj->cmd_reg[2] << 16)
                   + ((ulong)obj->cmd_reg[1] <<  8)
                   +  (ulong)obj->cmd_reg[0];

    // restrict this to unsecured RAM area only.
    // do not allow read of secure memory areas
    if ((ulSeqAddress >= RAM_START) && (ulSeqAddress <= RAM_END))
    {
        ubNumBytes   = obj->data[0];
        puwData = (uword *)ulSeqAddress;

        for ( ubCount = 0; ubCount < ubNumBytes; ubCount+=2, puwData++ )
        {
            obj->data[ubCount] = *puwData & 0xFF;
            obj->data[ubCount+1] = *puwData >> 8;
        }
        obj->command = cmdReadAnswer;
        obj->length  = ubNumBytes;
    }
    else
    {
        obj->command = cmdUnsupported;
        obj->length  = 0;
    }
    SendTxObject(obj);
}

static void vAnswerRunBootLoadProgram(Obj_type *obj)
{
    ulong seq_address;

    seq_address = ((ulong)obj->cmd_reg[3] << 24)
                  + ((ulong)obj->cmd_reg[2] << 16)
                  + ((ulong)obj->cmd_reg[1] <<  8)
                  +  (ulong)obj->cmd_reg[0];
    BootProgram = (void *)seq_address;
    obj->length = 0;
    SendTxObject(obj);
    vDelay_ms(10);
    asm("   MOVL    XAR6,#_BootProgram");
    asm("   MOVL    XAR7,*XAR6");
    asm("   LC  *XAR7");
}

static void vAnswerEraseAppSection(Obj_type *obj)
{
    boolean fInternalPassed        = FALSE;
    ubyte   ubEraseMode            = obj->cmd_reg[1];
    static  ubyte    ubTimesErased = 0;
    static  Cmd_type EraseStatus   = cmdOpFailed;

    /* Should we erase again */
    if ( obj->cmd_reg[0] == (ubTimesErased + 1) )
    {
        ubTimesErased++;
        EraseStatus = cmdOpInProgress;
    }

    /* Send erase status message */
    obj->command    = EraseStatus;
    obj->cmd_reg[0] = ubTimesErased;
    obj->cmd_reg[1] = ubEraseMode;
    obj->length     = 0;
    SendTxObject(obj);

    /* Perform the erase if necessary */
    if ( EraseStatus == cmdOpInProgress )
    {
        switch ( ubEraseMode )
        {
            case 0:
                // Erase all of primary FLASH
                if ( fEraseAppSectors(FALSE) )
                {
                    fInternalPassed = TRUE;
                }
                // Set erase status
                if ( fInternalPassed )
                {
                    EraseStatus = cmdOpPassed;
                }
                else
                {
                    EraseStatus = cmdOpFailed;
                }
                break;

            case 1:
                // Erase parameter section only of primary FLASH
                if ( fEraseAppSectors(TRUE) )
                {
                    EraseStatus  = cmdOpPassed;
                }
                else
                {
                    EraseStatus  = cmdOpFailed;
                }
                break;

            default:
                EraseStatus  = cmdOpFailed;
                break;
        }
    }
}

static void vAnswerBufferData(Obj_type *obj)
{
    uword uwBufferIndex;

    uwBufferIndex = ((uword)obj->cmd_reg[1] << 8)
                    + ((uword)obj->cmd_reg[0]     );

    vFillBuffer(&obj->data[0], uwBufferIndex, obj->length);

    obj->command = cmdWriteAnswer;
    obj->length  = 0;
    SendTxObject(obj);
}

static void vAnswerProgramBuffer(Obj_type *obj)
{
    ulong seq_address;

    // Initialize object for transmission
    obj->command = cmdOpFailed;
    obj->length  = 0;

    // Get start address
    seq_address = ((ulong)obj->cmd_reg[3] << 24)
                  + ((ulong)obj->cmd_reg[2] << 16)
                  + ((ulong)obj->cmd_reg[1] <<  8)
                  +  (ulong)obj->cmd_reg[0];

    // Check if address is with internal FLASH range
    if ( (seq_address >= ADDR_INT_FL_START)
         && (seq_address <= ADDR_INT_FL_END  )
       )
    {
        // Yes, program internal FLASH
        if ( fProgramBlock(seq_address) )
        {
            obj->command = cmdOpPassed;
        }
    }
    // Transmit object with pass/fail status
    SendTxObject(obj);
}

static boolean fConfigureRxBuffer(Obj_type *obj)
{
    TCAN_SWObj Msg;
    boolean fSuccess = FALSE;
    uword uwMsgId = ((uword)obj->cmd_reg[1] << 8) + obj->cmd_reg[0];

    if ( uwMsgId <= 0x7FF )
    {
        fSuccess = TRUE;
        Msg.fTransmit = FALSE;
        Msg.ulMSGID     = (ulong)uwMsgId;
        CAN_vConfigMsgObj(CAN_MB_RX_GEN, &Msg);
        CAN_vReleaseObj(CAN_MB_RX_GEN);
    }
    return(fSuccess);
}

static void vCopyGenMbxToObj(Obj_type *obj)
{
    CAN_MESSAGE Msg;

    obj->command    = cmdReadAnswer;
    obj->length     = 0;
    obj->cmd_reg[0] = 0;
    obj->cmd_reg[1] = 0;
    obj->cmd_reg[2] = 0;
    obj->cmd_reg[3] = 0;

    if ( CAN_read(CAN_MB_RX_GEN, &Msg) )
    {
        memcpy(&obj->data[0], &Msg.uwData[0], Msg.size);
        obj->cmd_reg[0] = 1;
        obj->length     = Msg.size;
    }
}

static boolean fSendGenMbx(Obj_type *obj)
{
    CAN_MESSAGE Msg;
    boolean fSuccess = FALSE;
    uword uwMsgId = ((uword)obj->cmd_reg[1] << 8) + obj->cmd_reg[0];

    if ( (obj->length <= 8) && (uwMsgId <= 0x7FF) )
    {
        Msg.id   = uwMsgId;
        Msg.size = obj->length;
        memcpy(&Msg.uwData[0], &obj->data[0], obj->length);
        CAN_write(CAN_MB_TX_GEN, &Msg);
        fSuccess = TRUE;
    }
    vDelay_ms(2);
    return(fSuccess);
}

