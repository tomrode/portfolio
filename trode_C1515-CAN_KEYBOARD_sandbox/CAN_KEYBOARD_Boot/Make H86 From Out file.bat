@echo off
cls

echo ********************************************
echo *   Conversion In Progress                 *
echo ********************************************

@rem Find the hex convertor executeable then use options for the conversion

c:\ti\ccsv6\tools\compiler\c2000_6.2.7\bin\hex2000.exe ^
-i ".\F2803x_Boot.out" ^
-o ".\F2803x_Boot.h86" ^
-order MS ^
-romwidth 16 ^ 

echo *******************************************
echo *           FINISHED                      *
echo *******************************************

@rem this paused it ;pause