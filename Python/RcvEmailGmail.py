# Name:      RcvEmailGmail.py
#
# Purpose:   This python script utilizes Gmail to get the Jazz build "TmlVcmMasterBuildDef00x to determine which developer
#            Delivered a change set.  
#
# Author:    Thomas Rode
#
# Options:   none
# Returns:   DevRecipients_Completelist: List of developers that submitted Change set From Gmail account.
#            RcvEmailErrorCode: which errors are present if any. 
#
# Note:      Could not place this function in EmailTestResults.py due to importing conflict
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#

import imaplib
import email

GmailSender = 'CrownTmlVcast@gmail.com'
GmailPassword = 'crown123'
Imap4SslGmail = 'imap.gmail.com'

def RcvEmailGmail():
                       
    Developer_list = []
    DevRecipients_list = []
    DevRecipients_Completelist = []
    GmailError = True
    RcvEmailErrorCode = 0
    try:
        #Connect over an encrypted SSL socket to gmail host
        mail = imaplib.IMAP4_SSL(Imap4SslGmail)
        mail.login(GmailSender, GmailPassword)
        mail.list()

        # Out: list of "folders" aka labels in gmail.
        mail.select("inbox") # connect to inbox.
        result, data = mail.search(None, "ALL")

        # data is a list.
        ids = data[0]

        # ids is a space separated string
        id_list = ids.split()
    
        # get the latest   
        latest_email_id = id_list[-1] 

        # fetch the email body (RFC822) for the given ID
        result, data = mail.fetch(latest_email_id, "(RFC822)") 

        # here's the body, which is raw text of the whole email, ncluding headers and alternate payloads
        raw_email = data[0][1] 
    
        #print raw_email
        msg = email.message_from_string(raw_email)

        #print msg['From']
        print (msg.get_payload(decode=True))

        # write this complete email to file 
        f = open('InDataGmail.txt', "w")
        f.writelines(msg.get_payload(decode=True))
        f.close()

        with open('InDataGmail.txt', 'r') as infile:
            for line in infile:
                if 'Jeff Campbell'in line or 'Linden R. Peterson'in line or 'Don Bartlett'in line or \
                   'Chris Graunke'in line or 'Mike Corbett'in line or 'Gupta, Subhanshu'in line or \
                   'Padsmanabha Manjunatha' in  line or 'Alexander Mueller'in line or 'Niels Franzen'in line or 'Tom Rode'in line: # remove me once
                    #print (line)
                    Developer_list.append(line)
            
            # write the developers from email to file 
            f = open('OutDataGmail.txt', "w")
            f.writelines(Developer_list)
            f.close()

            #Indicate No Error
            GmailError = False
            
    except:
        print("Error With Gmail")
        RcvEmailErrorCode = 1
        DevRecipients_Completelist = ['tom.rode@crown.com']
        return DevRecipients_Completelist, RcvEmailErrorCode
    
    try:
        # Add this in email developer function, remove \r\n and replace a " " with a "."witin list
        with open('OutDataGmail.txt', 'r') as infile:
            for line in infile:
                DevRecipients_list.append(line)
            DevRecipients_list = [x.replace("\r\n","") for x in DevRecipients_list]
            DevRecipients_list = [x.replace(" ",".") for x in DevRecipients_list]

            # add '@crown.com' to DevRecipients_Complete
            for line in DevRecipients_list:
                line = line + '@crown.com'
                DevRecipients_Completelist.append(line)

            # adjust email to lower case and non crown@.com recipients
            for line in DevRecipients_Completelist:
                if 'Jeff.Campbell@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Jeff.Campbell@crown.com", "jeff.campbell@crown.com") for x in DevRecipients_Completelist]
                if 'Linden.R..Peterson@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Linden.R..Peterson@crown.com", "linden.peterson") for x in DevRecipients_Completelist]
                if 'Don.Bartlett@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Don.Bartlett@crown.com", "don.bartlett@crown.com") for x in DevRecipients_Completelist]
                if 'Gupta,.Subhanshu@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Gupta,.Subhanshu@crown.com", "Subhanshu.G@lnttechservices.com") for x in DevRecipients_Completelist]
                if 'Padsmanabha.Manjunatha@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Padsmanabha.Manjunatha@crown.com", "Padmanabha.M@lnttechservices.com") for x in DevRecipients_Completelist]
                if 'Tom.Rode@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Tom.Rode@crown.com", "tom.rode@crown.com") for x in DevRecipients_Completelist]
                if 'Mike.Corbett@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Mike.Corbett@crown.com", "michael.corbett@crown.com") for x in DevRecipients_Completelist]
                if 'Niels.Franzen@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Niels.Franzen@crown.com", "niels.franzen@crown.com") for x in DevRecipients_Completelist]
                if 'Alexander.Mueller@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Alexander.Mueller@crown.com", "alexander.mueller@crown.com") for x in DevRecipients_Completelist]
                if 'Chris.Graunke@crown.com' in line:                   
                    DevRecipients_Completelist = [x.replace("Chris.Graunke@crown.com", "chris.graunke@crown.com") for x in DevRecipients_Completelist]


            print("Here is the Final DevRecipients_Completelist\n")
            print(DevRecipients_Completelist)

            
    except:
        print("Missing OutDataGmail.txt")
        RcvEmailErrorCode = 2
        DevRecipients_Completelist = ['tom.rode@crown.com']
        
    return DevRecipients_Completelist, RcvEmailErrorCode 
 
if __name__ == '__main__':
    Test = RcvEmailGmail()
    
    if RcvEmailGmail() == False:
        print("Main No Error")
    else:
        print("Main Error")
