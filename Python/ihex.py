#
# Purpose:   This python script will create an html file for the specified Intel hex record file
# Reference: https://www.python.org/ (open source)
#
import os
import sys

# This function will create a table with decoded index hex information
def convert_intel_hex_file(in_file, out_file):

    # Check if the input file exists
    if not os.path.isfile(in_file):
        print('File not found!')
        return

    # Read the intel hex file
    file_hnd = open(in_file, 'r')
    file_lines = file_hnd.read().splitlines()
    file_hnd.close()              

    # Initialize variables
    ext_addr = 0
    line_number = 0
    total_bytes = 0
    first_addr = 0xFFFFFF
    last_addr = 0
    dcolor = 'Wheat'
    Header_text = 'Intel Hex converter'
    Version = 'v1.0' 

    # HTML title and style information
    new_page = []
    new_page.append('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">')
    new_page.append('<html>')
    new_page.append('<head>')
    new_page.append('  <title>' + in_file + '</title>')
    new_page.append('</head>')
    new_page.append('')
    new_page.append('<body>')
    new_page.append('  <style type="text/css">')
    new_page.append('  body {')
    new_page.append('    background-color: PaleGoldenRod;')
    new_page.append('    margin-left: auto;')
    new_page.append('    margin-right: auto;')
    new_page.append('    width: 90%;')
    new_page.append('  }')
    new_page.append('  </style>')
    new_page.append('')
    new_page.append('<h2>' + Header_text + ' ' + Version + ' - ' + in_file + '</h2>')
    new_page.append('')
    new_page.append('<table>')
    new_page.append('  <style type="text/css">')
    new_page.append('  body {')
    new_page.append('    font-family: "Consolas" }')
    new_page.append('  table {')
    new_page.append('    border-collapse: collapse;')
    new_page.append('  }')
    new_page.append('')
    new_page.append('  table, th, td {')
    new_page.append('    border: 2px solid black;')
    new_page.append('    padding: 10px;')
    new_page.append('  }')
    new_page.append('')
    new_page.append('  th {')
    new_page.append('    height: 50px;')
    new_page.append('    background-color: Sienna;')
    new_page.append('    color: white;')
    new_page.append('  }')
    new_page.append('')
    new_page.append('  </style>')
    new_page.append('')
    new_page.append('<table>')
    new_page.append('<tr align="left"><th>Line</th><th>Intel Hex Record</th><th>Address</th><th>Data/Decription</th><th>Cks</th></tr>')

    try:
        # Step through each line of the file
        for file_line in file_lines:
            # remove leading/trailing whitespace
            file_line.strip()
            # Advance line number
            line_number += 1
            # Check for valid record
            if file_line[0] is ':':
                # Parse record
                byte_count = int(file_line[1:3], 16)
                base_addr  = int(file_line[3:7], 16)
                rec_type   = int(file_line[7:9], 16)

                # Data record
                if rec_type == 0:
                    full_addr = (ext_addr * 65536) + base_addr
                    line = '<tr bgcolor="' + dcolor + '"><td align="right">' + str(line_number) + '</td><td>' + file_line + '</td><td>' + str(hex(full_addr)).upper().replace('X', 'x') + '</td><td>'
                    for x in range(0, 2*byte_count, 2):
                        line += '0x' + file_line[9+x:11+x] + ' '
                    line += '</td><td align="center">0x' + file_line[-2:] + '</td></tr>'
                    new_page.append(line)
                    if ( full_addr < first_addr ): first_addr = full_addr
                    if ( full_addr > last_addr): last_addr = full_addr
                    total_bytes += byte_count
                    if dcolor is 'Wheat': dcolor = 'Peru' 
                    else: dcolor = 'Wheat'
                # End of file record
                elif rec_type == 1:
                    new_page.append('<tr bgcolor="Sienna"><td align="right">' + str(line_number) + '</td><td>' + file_line + '</td><td>' + str(hex(base_addr)).upper().replace('X', 'x') + '</td><td>EOF</td><td align="center">0x' + file_line[-2:] + '</td></tr>')
                # Currently unsupported
                elif rec_type == 2:
                    new_page.append('<tr bgcolor = "red"><td align="right">' + str(line_number) + '</td><td>' + file_line + '</td><td></td><td>rec_type 2 unsupported</td><td align="center">0x' + file_line[-2:] + '</td></tr>')
                elif rec_type == 3:
                    new_page.append('<tr bgcolor = "red"><td align="right">' + str(line_number) + '</td><td>' + file_line + '</td><td></td><td>rec_type 3 unsupported</td><td align="center">0x' + file_line[-2:] + '</td></tr>')
                # Extended address record
                elif rec_type == 4:
                    ext_addr = int(file_line[9:13], 16)
                    new_page.append('<tr bgcolor="Tan"><td align="right">' + str(line_number) + '</td><td>' + file_line + '</td><td>' + str(hex(base_addr)).upper().replace('X', 'x') + '</td><td>Extended Address = ' + str(hex(ext_addr * 65536)).upper().replace('X', 'x') + '</td><td align="center">0x' + file_line[-2:] + '</td></tr>')
                # Start linear address record
                elif rec_type == 5:
                    start_linear_addr = int(file_line[9:17], 16)
                    new_page.append('<tr bgcolor="Tan"><td align="right">' + str(line_number) + '</td><td>' + file_line + '</td><td>' + str(hex(base_addr)).upper().replace('X', 'x') + '</td><td>Start Linear Address = ' + str(hex(start_linear_addr)).upper().replace('X', 'x') + '</td><td align="center">0x' + file_line[-2:] + '</td></tr>')
            else:
                # Format error
                new_page.append('<tr bgcolor="red"><td align="right">' + str(line_number) + '</td><td>' + file_line + '</td><td></td><td>Invalid record</td><td></td></tr>')
    except:
        # Catch errors
        print('Error converting file!')
        return ''

    # End html file
    new_page.append('</table>' )
    new_page.append('')
    new_page.append('</body>')
    new_page.append('</html>')

    # Insert summary near top of page
    for x in range(0, len(new_page)):
        # Check if insert tag is found
        if ((-1) != new_page[x].find(Header_text)):
            # Yes, append the replace string
            new_page.insert(x+1, '<h3>First address = ' + str(hex(first_addr)).upper().replace('X', 'x') + '</h3>')
            new_page.insert(x+2, '<h3>Last address  = ' + str(hex(last_addr)).upper().replace('X', 'x') + '</h3>')
            new_page.insert(x+3, '<h3>Total bytes   = ' + str(total_bytes) + '</h3>')

    # Write to output file
    file_hnd = open(out_file, 'w')
    file_hnd.write("\n".join(new_page))
    file_hnd.close()

# When called by the command line
if (2 == len(sys.argv)):
    # Get the input filename  
    in_file = sys.argv[1]

    # Determine the output filename
    out_file = in_file[:in_file.find('.')] + '.html'

    # Call the main function
    convert_intel_hex_file(in_file, out_file)
else:
    # Show usage info
    print('Invalid script arguments!')
    print('Usage: ihex.py <filename>')

