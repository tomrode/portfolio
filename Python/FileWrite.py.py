# Python script to create and write to a file.

# Open ex text file
f = open('ex.txt', 'w')

# Write contents of string to the file
f.write('This is a test String \n')
f.write('This is a second test String \n')

# Close the file
f.close()
    
