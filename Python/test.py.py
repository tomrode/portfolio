# This is a comment My first Python examples

# This is a variable
Tom = 41 - 2;
print ('Hi Tom your age is, %d.' % Tom);

# Some string stuff 
print('C:\some\name')
print("""\
Usage: thingy [OPTIONS]
     -h                        Display this usage message
     -H hostname               Hostname to connect to
""")

# Lists
squares = [1, 4, 9, 16, 25]
print ('The value for squares[1] is, %d.' %squares[1]);

# Some small Programming
a, b = 0, 1
while (b < 10):
    
     print(b)
     a, b = b, a+b
     
# More flow control

words = ['cat', 'window', 'defenestrate']
for w in words:
    print(w, len(w))


var = int(input("Please enter an integer: "))
if var == 200:
   print ("1 - Got a true expression value")

elif var == 150:
   print ("2 - Got a true expression value")

elif var == 100:
   print ("3 - Got a true expression value")

else:
   print ("4 - Got a false expression value")


print ("Good bye!")
