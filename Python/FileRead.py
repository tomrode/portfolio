# Python script to create and write to a file.

# Open ex.txt file
f = open('ex.txt', 'r')

# Get user input
TRUE  = 1
FALSE = 0
STATE = TRUE

while STATE == TRUE:
      var = int(input("Please enter an '2' to read file '3' to close: "))

      # Write each line from file
      if var == 2:
         for line in f:
             print (line, end='')

      # Close file
      if var == 3:
         f.close()
         print(" Closed ")  
         STATE = FALSE
