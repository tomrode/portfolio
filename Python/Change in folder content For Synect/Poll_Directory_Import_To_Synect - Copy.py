# Name:      Poll_Directory_Import_To_Synect.py
#
# Purpose:   This python script run infinitely at a predetermined Periodicity. A base path with Server folder "MANUAL_SYNECT_TEST_PLAN" for PIL
#            for "Completed_Executions" folder established. 'subdir_listdic' list will contain dictionary elements used by directory polling method.
#            This list will update and be passed to file handle used by SYNECT. Next steps is to initialize/dispatch SYNECT, connect to database, Provide a server URL
#            open the desired workspace and desired Test Management project etc. 
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:      As of 16 August 2018 a pseudo explaination from dSPACE on accessing SYNECT Server.
#            Please make sure sub-folders match within "Completed_Executions"
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#
import os, time



# Server path on EngSTL1 
base_path_to_watch = "C:/Users/t0028919/Desktop/Python/Change in folder content For Synect/Completed_Execution"

# List of dictionary entries used for Polling algo for new/deleted files in a given folder 
subdir_listdic = [{'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"RM"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"C5"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"C5D"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"ESR"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"FC"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"LP"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"PC"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"RC"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"SC"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"SP"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"TC_AGV"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'before_init':"TSP"}
                 ]
# Files to be imported to SYNECT
files_to_synect = []

# Periodicity in seconds for entire event
SLEEP_PERIODICITY = 5 

# Enable flag to allow writng to files_to_synect list 
enable_synect_list_write = False

# Index count for each dictionary set in subdir_listdic list 
i = 0

# Main Loop to run as long as script is running 
while 1:
    
    # Time to recycle the whole evolution
    time.sleep (SLEEP_PERIODICITY)
 
    # Go through each sub-directory to see which has changed
    for subdir in subdir_listdic:
        
        # Use os.listdir to pull up a list of the files in a directory. Compare this list against the previous run to see which files have been added or removed
        subdir_listdic[i]["after"] = dict ([(f, None) for f in os.listdir (base_path_to_watch + '/' + subdir_listdic[i]["before_init"] )]) 
        subdir_listdic[i]["added"] = [f for f in subdir_listdic[i]["after"] if not f in subdir_listdic[i]["before"]]
        subdir_listdic[i]["removed"] = [f for f in subdir_listdic[i]["before"] if not f in subdir_listdic[i]["after"]]
        
        if subdir_listdic[i]["added"]:
            if enable_synect_list_write:
                
                # Clearout list contents then add in updated files to files_to_synec list 
                del files_to_synect[:]
                files_to_synect.append(subdir_listdic[i]["added"])
                print("Files added to enable_synect_import list", files_to_synect,'\n')
                
            print ("Added: ", ", ".join (subdir_listdic[i]["added"]), "To Folder", subdir_listdic[i]["before_init"] )
        if subdir_listdic[i]["removed"]:
            print ("Removed: ", ", ".join (subdir_listdic[i]["removed"]), "To Folder", subdir_listdic[i]["before_init"] ) 
        
        subdir_listdic[i]["before"] = subdir_listdic[i]["after"]
        
        i = i + 1
        
    # SYNECT Import testcase pseudo code section to be implemented from Chandu email

    #    2.	After you get the handle of what file was added to the folder, you need to pass that to SYNECT and then automate SYNECT via SYNECT API. I’m afraid this is the time consuming part and I couldn’t find a good demo that automates SYNECT from outside. However, you need to follow these steps via SYNECT API:

    #    a.	Open SYNECT via Dispatch Application from Python
    #    b.	Connect to the database. You will need to provide the Server URL and form of authentication (username/password or Windows). If windows, the connection will be automatic.
    #    c.	Once connected you need to open the desired workspace and desired Test Management project
    #    d.	Navigate to the pending execution and get the handle of the execution that is currently in processing state
    #    e.	Import the execution results using the import adapter api
    #    f.	Set the state of the execution to “Finished”

        
        
    # Reset the index, enable writing to synect
    i = 0
    enable_synect_list_write = True 
