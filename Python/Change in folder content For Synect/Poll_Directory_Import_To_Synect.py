# Name:      Poll_Directory_Import_To_Synect.py
#
# Purpose:   This python script run infinitely at a predetermined Periodicity. A base path with Server folder "MANUAL_SYNECT_TEST_PLAN" for PIL
#            for "Completed_Executions" folder established. 'subdir_listdic' list will contain dictionary elements used by directory polling method.
#            This list will update and be passed to file handle used by SYNECT. Next steps is to initialize/dispatch SYNECT, connect to database, Provide a server URL
#            open the desired workspace and desired Test Management project etc. 
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:      As of 16 August 2018 a pseudo explaination from dSPACE on accessing SYNECT Server.
#            Please make sure sub-folders match within "Completed_Executions"
#
# Python Version: 2.7.11
#          
# Reference: https://www.python.org/ (open source)
#
import os, time
#from win32com.client import Dispatch, DispatchWithEvents
from win32com.client import Dispatch


## Server-side ( myServerScript.py )
class myServerScript( object ):
      
      def get_data_from_server( self, ServerContext, args ):
            print(" ******* get_data_from_server ******* ")
            workspaceName, tmProjectName = args.split( ';' )

            try:
                  workspaceId = filter( lambda x: x.Name == workspaceName, ServerContext.Workspaces   )[0].UniqueID   
                  tmProjectId = filter( lambda x: x.Name == tmProjectName, workspace.Projects         )[0].UniqueID   
                  return '{};{}'.format( workspaceId, tmProjectId )
            except:
                  return ''

# SQL Server used for SYNECT information section for Development Environment
test_database = "Test"
test_server_url = "http://corp1862.us.crownlift.net:8081"
test_username = "demo"
test_password = "demo"

# SQL Server used for SYNECT information section for Production Environment
production_database = "T1515"
production_server_url = "http://engstl1.us.crownlift.net:8081"
production_username = "demo"
production_password = "demo"

# Server path on EngSTL1 
#base_path_to_watch = "Y:/MANUAL_SYNECT_TEST_PLAN/Completed_Execution"
base_path_to_watch = "C:/Users/t0028919.US.000/Desktop/Change in folder content For Synect/Completed_Execution"

# List of dictionary entries used for Polling algo for new/deleted files in a project folder 
subdir_listdic = [{'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"RM_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"C5_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"C5D_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"ESR_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"FC_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"LP_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"PC_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"RC_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"SC_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"SP_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"TC_AGV_Manual"},
                  {'after':None, 'added':None, 'removed':None,'before':'filler', 'sub_folder':"TSP_Manual"}
                 ]
# Files to be imported to SYNECT
files_to_synect = []

# Periodicity in seconds for entire event
SLEEP_PERIODICITY = 10#20 

# Enable flag to allow writng to files_to_synect list 
enable_synect_list_write = False

# Index count for each dictionary set in subdir_listdic list 
i = 0

# Main Loop to run as long as script is running 
while 1:
    
    # Time to recycle the whole evolution
    time.sleep (SLEEP_PERIODICITY)
 
    # Go through each sub-directory to see which has changed
    for subdir in subdir_listdic:
        
        # Use os.listdir to pull up a list of the files in a directory. Compare this list against the previous run to see which files have been added or removed
        subdir_listdic[i]["after"] = dict ([(f, None) for f in os.listdir (base_path_to_watch + '/' + subdir_listdic[i]["sub_folder"] )]) 
        subdir_listdic[i]["added"] = [f for f in subdir_listdic[i]["after"] if not f in subdir_listdic[i]["before"]]
        subdir_listdic[i]["removed"] = [f for f in subdir_listdic[i]["before"] if not f in subdir_listdic[i]["after"]]
        
        if subdir_listdic[i]["added"]:
            if enable_synect_list_write:
                
                # Clearout list contents then add in updated files to files_to_synec list 
                del files_to_synect[:]
                files_to_synect.append(subdir_listdic[i]["added"])
                files_to_synect.append(subdir_listdic[i]["sub_folder"]) # NEW because project in synect will have to match
                print('\n')      
                print("Files added to files_to_synect list", files_to_synect[0], "In folder", files_to_synect[1], "Length of file_to_synect", len(files_to_synect))

                # Open SYNECT, Connect with database, Open workspace & Test Management Project, goto pending executions, Import results, Set execution to finish
                try:
                    
                    ## a. Open SYNECT via Dispatch Application from Python
                    print(" ******* Dispatch SYNECT.Application.2.4 ******* ")
                    Application = Dispatch("SYNECT.Application")

                    ## b. Connect to the database. You will need to provide the Server URL and form of authentication (username/password or Windows). If windows, the connection will be automatic.
                    print(" ******* Connect to the database ******* ")
                    Application.DBConnection.Connect( True )  ## True => allow untrusted certificate
                    
                    ## Not sure what this does
                    print(" ******* server_script_name ******* ")
                    server_script_name = 'myServerScript'
                    #server_script_name = myServerScript()
                    #server_script_name.get_data_from_server()
                    
                    try:
                        script = filter( lambda x: x.Name == server_script_name, Application.Server.Scripts )[ 0 ]
                    except:
                        raise Exception( 'Failed to retrieve server script with name "{}"'.format( server_script_name ) )
                    
                    ## c. Once connected you need to open the desired workspace and desired Test Management project         
                    print(" ******* Open workspace and project ******* ")
                    Application.Navigator.OpenWorkspace( workspaceId )

                    print(" ******* Assign WorkspaceId and tmProjectId ******* ")
                    workspaceId, tmProjectId = Application.Server.CallMethod( script, 'get_data_from_server', 'myWorkspace;myTmProject' ).split( ';' )
                    if not workspaceId or not tmProjectId:
                        break
                        #return

                    ## e. Import the execution results using the import adapter API
                    plugin = filter( lambda x: x.DisplayName == 'my import plugin', Application.TM.Plugins.ExecutionsImportPlugins )
                    Application.TM.ImportExecutions( plugin, workspaceId, tmProjectId, 'file/to/import.ext', 'path/to/plugin.ecxml' )  ## not sure about this part, you will need to verify via API

                    # SYNECT Import testcase pseudo code section to be implemented from Chandu email
                    #    2.	After you get the handle of what file was added to the folder, you need to pass that to SYNECT and then automate SYNECT via SYNECT API. I�m afraid this is the time consuming part and I couldn�t find a good demo that automates SYNECT from outside. However, you need to follow these steps via SYNECT API:
                    #    a.	Open SYNECT via Dispatch Application from Python
                    #    b.	Connect to the database. You will need to provide the Server URL and form of authentication (username/password or Windows). If windows, the connection will be automatic.
                    #    c.	Once connected you need to open the desired workspace and desired Test Management project
                    #    d.	Navigate to the pending execution and get the handle of the execution that is currently in processing state
                    #    e.	Import the execution results using the import adapter api
                    #    f.	Set the state of the execution to �Finished�

                except:
                     print("Error With Synect")
                   
            #print ("Added: ", ", ".join (subdir_listdic[i]["added"]), "To Folder", subdir_listdic[i]["sub_folder"] )
        if subdir_listdic[i]["removed"]:
            print ("Removed: ", ", ".join (subdir_listdic[i]["removed"]), "To Folder", subdir_listdic[i]["sub_folder"] ) 
        
        subdir_listdic[i]["before"] = subdir_listdic[i]["after"]
        
        i = i + 1     
        
    # Reset the index, enable writing to synect
    i = 0
    enable_synect_list_write = True



