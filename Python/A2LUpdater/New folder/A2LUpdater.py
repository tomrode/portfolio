# Name:     A2LUpdater.py
#
# Purpose:   This python script will copy Most current 161720-xxx-xx.a2l from Truck download zip file to IOModel/database for HIL 
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:
#
# Python Version: 3.4
#          
# Reference: https://www.python.org/ (open source)
#

import os
import glob
from zipfile import ZipFile
import shutil
from tkinter import Tk, Label, Button

TruckDownLoadDir = 'C:/Users/t0028919/Desktop/Linker stuff'  
A2LFilePrefix = '161720-'
A2LPostFix = '.a2l'
CurrentA2LDst = 'C:/repos/HIL/Simulation/IOModel/database/'
DownLoadBundlePrefix = '154620'

class A2LUpdater:
    def __init__(self, master):
        self.master = master
        master.title("A2L Update")

        # New GUI dimensions
        root.geometry("200x200")

        # Set up GUI Label
        self.label = Label(master, text="Choose action!")
        self.label.pack()

        # Set up GUI Buttons
        self.greet_button = Button(master, text=" Change A2L ", command=self.change,  width = 15)
        self.greet_button.pack()

        self.close_button = Button(master, text="Remove A2L", command=self.same,   width = 15 )
        self.close_button.pack()

        self.close_button = Button(master, text="Quit", command=master.quit)
        self.close_button.pack()

    def change(self):

        # Change directory to the Truck bundle download folder 
        os.chdir(TruckDownLoadDir)
        
        # Get all zipped folders specified within directory 
        items = os.listdir(".")
        newlist = []
        for names in items:
            if names.endswith(".zip"):
                newlist.append(names)
        #print (newlist)
        
        # Check to see which folder is the newest
        latest_zipped_subdir = max(newlist, key=os.path.getmtime)

        # Mask off prefix (167720-) and the .zip extension, ex. 167720-821-02.zip
        latest_zipped_subdir_masked = latest_zipped_subdir[7:13]
        
        #print (latest_zipped_subdir_masked )
       
        # Opening the zip file in READ mode       
        with ZipFile(latest_zipped_subdir, 'r') as zip:
            
            # printing all the contents of the zip file
            #zip.printdir()
            
            # extracting all the files
            print('Extracting all the files now...')
            zip.extractall()
            

        #Open extracted folder, pick out A2L Src to dst
        shutil.copy('154620-821-03/' + (A2LFilePrefix + latest_zipped_subdir_masked + A2LPostFix), (A2LFilePrefix + latest_zipped_subdir_masked + A2LPostFix))

        print ( (A2LFilePrefix + latest_zipped_subdir_masked + A2LPostFix) , 'Is now copied at location ' + CurrentA2LDst) 
    
        # call mscript to be constructed
        #print("Call mscript!")
        #os.system("Test.py 1")
       

    def same(self):
        print("Keep Same A2L!")

root = Tk()
my_gui = A2LUpdater(root)
root.mainloop()
