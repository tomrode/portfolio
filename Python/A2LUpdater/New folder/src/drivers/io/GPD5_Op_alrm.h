 /**
 *  @file                   GPD5_Op_alrm.h
 *  @brief                  Interface for the GPD5 operator alarm driver
 *  @copyright              2015 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   07/6/2015
 * 
 *  @remark Author:         Paddy
 *  @remark Project Tree:   C1515
 *
 */

#ifndef  GPD5_OP_H
#define  GPD5_OP_H 1

  #include "typedefs.h"
/*========================================================================* 
 *  SECTION - Global definitions 
 *========================================================================* 
 */

#define GPD5_OPAL_ON 1
#define GPD5_OPAL_OFF 0


/** @struct gpd5opal_params
 *  @brief  Parameters used to configure a operator alarm
 *
 *  @typedef gpd5opal_params_t
 *  @brief   gpd5opal_params type definition
 */

typedef struct gpd5opal_params
{
    /*  
    @@ ELEMENT = ubAlarmMode 
    @@ STRUCTURE = gpd5opal_params_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = UBYTE
    @@ DESCRIPTION = "Operator alarm mode from IMM"
    @@ END 
    */
    uint8_t ubAlarmMode;           /**< Alram mode from IMM */

    /*  
    @@ ELEMENT = ubAlarmOnTicks
    @@ STRUCTURE = gpd5opal_params_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = UBYTE
    @@ DESCRIPTION = "Alarm ON time in ticks - 1 tick - 5ms "
    @@ END 
    */
    uint8_t ubAlarmOnTicks;       /**< Alarm ON time in ticks */

    /*  
    @@ ELEMENT = ubAlarmOffTicks
    @@ STRUCTURE = gpd5opal_params_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = UBYTE
    @@ DESCRIPTION = "Alarm OFF time in ticks - 1 tick - 5ms"
    @@ END 
    */
    uint8_t ubAlarmOffTicks;         /**< Alarm OFF time in ticks */

    /*  
    @@ ELEMENT = ubAlarmRepeat
    @@ STRUCTURE = gpd5opal_params_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = UWORD
    @@ DESCRIPTION = "Number of times the alarm to repeat"
    @@ END 
    */
    uint16_t ubAlarmRepeat;     /**< Number of times the alarm to repeat */

    /*  
    @@ ELEMENT = ubAlarmPriority
    @@ STRUCTURE = gpd5opal_params_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = UBYTE
    @@ DESCRIPTION = "Number of times the alarm to repeat"
    @@ END 
    */
//    uint8_t ubAlarmPriority;     /**< Number of times the alarm to repeat */

}gpd5opal_params_t;

extern int8_t gsbOperatorAlarmSel(const gpd5opal_params_t *pObj);

#endif /* #ifndef GPD5_OP_H */


