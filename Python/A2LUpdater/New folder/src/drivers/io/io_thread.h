/**
 *  @file                   io_thread.h
 *  @brief                  Interface for the master processors input/output handling
 *  @copyright              2013 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   1/10/2013
 *
 *  @remark Author:         Walter Conley
 *  @remark Project Tree:   C1515
 *
 */

#ifndef IO_THREAD_H
#define IO_THREAD_H 1

#include "OS_Interface.h"
#include "go_driver.h"
#include "switches.h"
#include "gpio.h"
#include "quadrature_absolute.h"

/*========================================================================*
 *  SECTION - Global definitions
 *========================================================================*
 */

/** @enum  tcm_gp_control
 *  @brief TCM GP driver control mode
 *
 *  @typedef tcm_gp_control_t
 *  @brief   tcm_gp_control type definition
 */
typedef enum tcm_gp_control
{
    TCM_GP_CTRLMODE_CURRENT = 0u,   /**< 0 = Current */
    TCM_GP_CTRLMODE_VOLTAGE = 1u,   /**< 1 = Voltage */
    TCM_GP_CTRLMODE_ON_OFF  = 2u    /**< 2 = B/W     */
}tcm_gp_control_t;

/** @def   IO_THREAD_NAME
 *  @brief Text name of the I/O thread
 */
#define IO_THREAD_NAME  "THD_IO"

/** @def   IO_STACK_SIZE
 *  @brief Define I/O thread stack size in bytes
 */
#define IO_STACK_SIZE 1536u

/** @def   IO_THREAD_PRIORITY
 *  @brief Define priority level for the I/O thread
 */
#define IO_THREAD_PRIORITY 8u

/** @def   IO_THREAD_ENTRY_INPUT
 *  @brief Value passed as argument into the entry function of the I/O thread
 */
#define IO_THREAD_ENTRY_INPUT 0u

/** @def   IO_THREAD_TIME_SLICE
 *  @brief Define thread time-slice in timer-ticks for the I/O thread
 *         - 1 - 0xFFFFFFFF if time-slicing should be used for this thread
 *         - 0 if time-slicing should be disabled for this thread
 */
#define IO_THREAD_TIME_SLICE 0u

/** @def   IO_SCHEDULE_TIMER_NAME
 *  @brief Text name of the I/O schedule timer
 */
#define IO_SCHEDULE_TIMER_NAME "TMR_IO_Schedule"

/** @def   ADCA_CONV_DONE_SEMA_NAME
 *  @brief Text name of the A/D channel A conversion done semaphore
 */
#define ADCA_CONV_DONE_SEMA_NAME "SEMA_ADCA_Conv_Complete"

/** @def   ADCB_CONV_DONE_SEMA_NAME
 *  @brief Text name of the A/D channel B conversion done semaphore
 */
#define ADCB_CONV_DONE_SEMA_NAME "SEMA_ADCB_Conv_Complete"

/** @def   ADC_DONE_SEMAPHORE_TIMEOUT
 *  @brief Timeout to get ADC(A/B)_Conversion_DoneSemaphore
 */
#define ADC_DONE_SEMAPHORE_TIMEOUT 2u

/** @def   NUM_QUADRATURE_ABSOLUTE
 *  @brief Define the number of quadrature absolute encoders used
 */
#define NUM_QUADRATURE_ABSOLUTE 1u

/** @def   LED_1SEC_TIME
 *  @brief Amber LED used in IMM Communication Error Indication
 *         blink rate
 */
#define LED_1SEC_TIME 1000u

/** @def   LED_1SEC_ON_OFF_CYCLE
 *  @brief Amber LED used in IMM Communication Error Indication
 *         blink state cycle
 */
#define LED_1SEC_ON_OFF_CYCLE 2u
/*========================================================================*
 *  SECTION - extern global variables (minimize global variable use)      *
 *========================================================================*
 */
/** @brief Command for caster brake enable *//*
    @@ SYMBOL = gsbOprAlrmState
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = UBYTE
    @@ CONVERSION = TABLE 0 "Enable" 1 "Disable"
    @@ DESCRIPTION = "Opr alarm"
    @@ END
 */
 int8_t gsbOprAlrmState;

extern quadrature_absolute_obj_t gQuadratureAbsoluteObject[NUM_QUADRATURE_ABSOLUTE];
/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */

extern uint32_t gulIO_Initialize(void);
extern os_thread_t *gpIO_GetThreadPointer(void);
extern void gvADC_ChannelA_ScanComplete(void);
extern void gvADC_ChannelB_ScanComplete(void);
extern void gvQUADAbsolute0ISR(uint32_t ulVectorNum);
extern void gvIO_UndervoltageISR(uint32_t ulVectorNum);
extern void gvIO_Main5VResetISR(uint32_t ulVectorNum);
extern void gvE13_control(boolean fSetHigh);
extern void gvE16_control(boolean fSetHigh);
extern void gvE87_control(boolean fSetHigh);
extern boolean gfIO_UndervoltageISR_Status(void);

#endif /* #ifndef IO_THREAD_H */

