/**
 *  @file                   io_thread.c
 *  @brief                  This file contains a thread that handles the I/O on the VCM master processor
 *  @copyright              2013 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   01/10/2013
 *
 *
 *  @remark Author:         Walter Conley
 *  @remark Project Tree:   C1515
 *
 */

/* Includes */
#include "typedefs.h"
#include "tx_api.h"
#include "param.h"
#include "XcpProf.h"
#include "debug_support.h"
#include "gpio.h"
#include "NvMemory.h"
#include "crc.h"
#include "EMIOS.h"
#include "Quadrature1.h"
#include "Quadrature3.h"
#include "switches.h"
#include "potentiometer.h"
#include "IRQ.h"
#include "adc.h"
#include "analog_interface.h"
#include "EventManager.h"
#include "4_20mA.h"
#include "test_api.h"
#include "supv_interface.h"
#include "imm_interface.h"
#include "SystemsImport.h"
#include "ag_common.h"
#include "CO_Application.h"
#include "INTC.h"
#include "switch_interface.h"
#include "xint_interface.h"
#include "GP_interface.h"
#include "LED.h"
#include "Info1.h"
#include "devrep_vcm_supv.h"
#include "devrep_Hdm.h"
#include "devrep_scm.h"
#include "watchdog.h"
#include "XcpUserFlash.h"
#include "pd_data.h"
#include "pd_data_test.h"
#include "AccumData.h"
#include "immcomm_driver.h"
#include "FileSysService.h"
#include "armrest_keyboard.h"
#include "instrument.h"
#include "GPD5_Op_alrm.h"
#include "timer.h"
#include "io_thread.h"

/*========================================================================*
 *  SECTION - External variables that cannot be defined in header files   *
 *========================================================================*
 */

/*========================================================================*
 *  SECTION - Local function prototypes                                   *
 *========================================================================*
 */
void vIO_ThreadEntry(uint32_t ulInput);
void vIO_Thread_TimerEntry(uint32_t ulInput);
static void vIO_Undervoltage_Init(void);
static void vIO_Poll_Undervoltage(void);
static void vIO_Main5VReset_Init(void);
  
/*========================================================================*
 *  SECTION - Local variables                                             *
 *========================================================================*
 */

/** @brief Thread control block for the thread used to handle the master processor's inputs and outputs
 */
static os_thread_t IO_Thread;

/** @brief Define memory used for the I/O thread stack
 */
static uint8_t ubIO_ThreadStack[IO_STACK_SIZE];

/** @brief Define a timer used to set the periodic execution rate
 */
static os_timer_t  IO_Timer;

/** @brief Enable for outer brake which is controlled by the supervisor processor *//*
    @@ INSTANCE  = Brake_outer_enable
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
static gpio_object_t Brake_outer_enable;

/** @brief Enable for inner brake which is controlled by the supervisor processor *//*
    @@ INSTANCE  = Brake_inner_enable
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
static gpio_object_t Brake_inner_enable;

/** @brief Enable for caster brake which is controlled by the supervisor processor *//*
    @@ INSTANCE  = Brake_caster_enable
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
static gpio_object_t Brake_caster_enable;

/** @brief Pin that controls PS5 shutdown *//*
    @@ INSTANCE  = sPS5_Shutdown
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
static gpio_object_t sPS5_Shutdown;

/** @brief Pin that controls PS5 voltage selection *//*
    @@ INSTANCE  = sPS5_VoltageSelect
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
static gpio_object_t sPS5_VoltageSelect;

/** @brief Pin that controls PS6 shutdown *//*
    @@ INSTANCE  = sPS6_Shutdown
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
static gpio_object_t sPS6_Shutdown;

/** @brief Pin that controls PS6 voltage selection *//*
    @@ INSTANCE  = sPS6_VoltageSelect
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
static gpio_object_t sPS6_VoltageSelect;

/** @brief Command for outer brake enable *//*
    @@ SYMBOL = eBrake_outer_enable_command
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = ULONG
    @@ CONVERSION = TABLE 0 "Enable" 1 "Disable"
    @@ DESCRIPTION = "Command for outer brake enable"
    @@ END
 */
static gpio_states_t eBrake_outer_enable_command = GPIO_STATE_LOW;

/** @brief Command for inner brake enable *//*
    @@ SYMBOL = eBrake_inner_enable_command
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = ULONG
    @@ CONVERSION = TABLE 0 "Enable" 1 "Disable"
    @@ DESCRIPTION = "Command for inner brake enable"
    @@ END
 */
static gpio_states_t eBrake_inner_enable_command = GPIO_STATE_LOW;

/** @brief Command for caster brake enable *//*
    @@ SYMBOL = eBrake_caster_enable_command
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = ULONG
    @@ CONVERSION = TABLE 0 "Enable" 1 "Disable"
    @@ DESCRIPTION = "Command for caster brake enable"
    @@ END
 */
static gpio_states_t eBrake_caster_enable_command = GPIO_STATE_LOW;

/** @brief EMIOS instance for monitoring undervoltage interrupt *//*
    @@ INSTANCE  = UV_Trip
    @@ STRUCTURE = emios_object_t
    @@ END
*/
static emios_object_t UV_Trip;

/** @brief LED driver instance for fault LED  *//*
    @@ INSTANCE  = Fault_LED
    @@ STRUCTURE = led_object_t
    @@ END
 */
static led_object_t Fault_LED;

/** @brief Define a semaphore used to indicate when a ADC channel A scan is complete
 */
static os_semaphore_t ADCA_Conversion_DoneSemaphore;

/** @brief Define a semaphore used to indicate when a ADC channel B scan is complete
 */
static os_semaphore_t ADCB_Conversion_DoneSemaphore;

/** @brief Signals when the A/D subsystem has been configured */
static boolean fADCInitialized = FALSE;

/** @brief Thread identifier for the watchdog
 */
static uint8_t ubWatchdogID;

/** @brief holds all the data needed for a quadrature absolute object*//*
    @@ INSTANCE  = gQuadratureAbsoluteObject[0]
    @@ STRUCTURE = quadrature_absolute_obj_t
    @@ END
 */
quadrature_absolute_obj_t gQuadratureAbsoluteObject[NUM_QUADRATURE_ABSOLUTE];

/** @brief Flag to indicate if the undervoltage condition is currently detected *//*
    @@ SYMBOL = fUndervoltageActive
    @@ A2L_TYPE = MEASURE
    @@ DATA_TYPE = UBYTE [0 ... 1]
    @@ CONVERSION = TABLE 0 "FALSE" 1 "TRUE"
    @@ DESCRIPTION = "Flag to indicate if the undervoltage condition is currently detected"
    @@ END
 */
static boolean fUndervoltageActive = FALSE;

/** @brief Flag to indicate if the Main 5V Reset undervoltage condition is currently detected *//*
    @@ SYMBOL = fMain5VResetUndervoltageActive
    @@ A2L_TYPE = MEASURE
    @@ DATA_TYPE = UBYTE [0 ... 1]
    @@ CONVERSION = TABLE 0 "FALSE" 1 "TRUE"
    @@ DESCRIPTION = "Flag to indicate if the Main 5V Reset undervoltage condition is currently detected"
    @@ END
 */
volatile static boolean fMain5VResetUndervoltageActive = FALSE;

/** @brief Flag to indicate the logic level at the IRQ 0 pin *//*
    @@ SYMBOL = sfLogicLevelIrq0
    @@ A2L_TYPE = MEASURE
    @@ DATA_TYPE = UBYTE [0 ... 1]
    @@ DESCRIPTION = "Flag to indicate the logic level at the IRQ 0 pin"
    @@ END
 */
volatile static boolean sfLogicLevelIrq0;

/** @brief Flag to indicate the logic level at the IRQ 1 pin *//*
    @@ SYMBOL = sfLogicLevelIrq1
    @@ A2L_TYPE = MEASURE
    @@ DATA_TYPE = UBYTE [0 ... 1]
    @@ DESCRIPTION = "Flag to indicate the logic level at the IRQ 1 pin"
    @@ END
 */
volatile static boolean sfLogicLevelIrq1;

/** @brief Flag to indicate the logic level at the IRQ 2 pin *//*
    @@ SYMBOL = sfLogicLevelIrq2
    @@ A2L_TYPE = MEASURE
    @@ DATA_TYPE = UBYTE [0 ... 1]
    @@ DESCRIPTION = "Flag to indicate the logic level at the IRQ 2 pin"
    @@ END
 */
volatile static boolean sfLogicLevelIrq2;

/** @brief I/O thread data *//*
    @@ INSTANCE  = sINST_Thread_IO
    @@ STRUCTURE = inst_thread_t
    @@ END
 */
static inst_thread_t sINST_Thread_IO;


/** @brief Pin used to synchronize the supervisor processor *//*
    @@ INSTANCE  = eSupv_sync
    @@ STRUCTURE = gpio_object_t
    @@ END
 */
static gpio_object_t eSupv_sync;


    /** @brief Command for caster brake enable *//*
     @@ SYMBOL = gsbOprAlrmMode
     @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = UBYTE [0 ... 10]
     @@ DESCRIPTION = "Opr alarm"
     @@ END
  */
  uint8_t gsbOprAlrmMode;

/**
 * @fn      uint32_t gulIO_Initialize(void)
 *
 * @brief   This function initializes the thread used to handle the master processor's inputs and outputs.
 *
 * @return  Status of thread creation service
 *   - @ref OS_SUCCESS
 *   - @ref OS_THREAD_ERROR
 *   - @ref OS_PTR_ERROR
 *   - @ref OS_SIZE_ERROR
 *   - @ref OS_PRIORITY_ERROR
 *   - @ref OS_THRESH_ERROR
 *   - @ref OS_START_ERROR
 *   - @ref OS_CALLER_ERROR
 *
 * @author  Walter Conley
 *
 * @note N/A
 *
 */
uint32_t gulIO_Initialize(void)
{
    uint32_t ulStatus;
    watchdog_thread_info_t WDThreadInfo;

    /** ###Functional overview: */

    /** Create the transmit thread and start automatically */
    ulStatus = gulOS_ThreadCreate(&IO_Thread,
                                  IO_THREAD_NAME,
                                  &vIO_ThreadEntry,
                                  IO_THREAD_ENTRY_INPUT,
                                  &ubIO_ThreadStack[0],
                                  IO_STACK_SIZE,
                                  IO_THREAD_PRIORITY,
                                  gParameters.ulIO_THREAD_PREEMPT_THRESH,
                                  IO_THREAD_TIME_SLICE,
                                  OS_AUTO_START
                                 );

    /** - Print status of thread creation using debug port */
    gvPrint_ThreadCreateStatus(IO_THREAD_NAME, ulStatus);

    /** - Register with the watchdog */
    WDThreadInfo.ulExecutionRate = gParameters.ulIO_THREAD_TICK_RATE;
    WDThreadInfo.uwEventID = DEMEV_VCM_IO_THREAD_WATCHDOG;
    WDThreadInfo.ubTimeoutTolerance = (uint8_t)gParameters.ulWD_IoTolerance;
    WDThreadInfo.ubConcernCountMax = (uint8_t)gParameters.ulWD_IoConcernCountMax;
    gvWatchdog_RegisterThread(&ubWatchdogID, &WDThreadInfo, IO_THREAD_NAME);

    /** - Return status to caller */
    return(ulStatus);
}

/**
 * @fn     void vIO_ThreadEntry(uint32_t ulInput)
 *
 * @brief  This function is the entry point for the I/O thread that handles all the master processor
 *         inputs and outputs
 *
 * @param  ulInput = Argument passed to this function specified by gulOS_ThreadCreate()
 *
 * @return N/A
 *
 * @author Walter Conley
 *
 * @note   N/A
 *
 */
void vIO_ThreadEntry(uint32_t ulInput)
{
    uint32_t  ulStatus;
    intc_interrupt_num_t InterruptNumber = {0u, 0u};
    boolean  fFirstTimeThrough = TRUE;
    uint32_t ulSync_rate = 0u;

    /** ###Functional overview: */

    /** Thread initialization code: */

    /** - Set up a semaphore to indicate that an A/D channel A scan is complete */
    ulStatus = gulOS_SemaphoreCreate(&ADCA_Conversion_DoneSemaphore, ADCA_CONV_DONE_SEMA_NAME, 0u);
    gvPrint_SemaphoreCreateStatus(ADCA_CONV_DONE_SEMA_NAME, ulStatus);

    /** - Set up a semaphore to indicate that an A/D channel B scan is complete */
    ulStatus = gulOS_SemaphoreCreate(&ADCB_Conversion_DoneSemaphore, ADCB_CONV_DONE_SEMA_NAME, 0u);
    gvPrint_SemaphoreCreateStatus(ADCB_CONV_DONE_SEMA_NAME, ulStatus);

    /** - Configure output to enable outer brake driver which is controlled by the supervisor processor */
    gfGPIO_Initialize(&Brake_outer_enable,  GPIO_MODE_OUTPUT, (uint16_t)gParameters.ulBRAKE_OUTER_ENABLE_PIN,  gParameters.eBrake_Outer_Enable_Initial_State);

    /** - Configure output to enable inner brake driver which is controlled by the supervisor processor */
    gfGPIO_Initialize(&Brake_inner_enable,  GPIO_MODE_OUTPUT, (uint16_t)gParameters.ulBRAKE_INNER_ENABLE_PIN,  gParameters.eBrake_Inner_Enable_Initial_State);

    /** - Configure output to enable caster brake driver which is controlled by the supervisor processor */
    gfGPIO_Initialize(&Brake_caster_enable, GPIO_MODE_OUTPUT, (uint16_t)gParameters.ulBRAKE_CASTER_ENABLE_PIN, gParameters. eBrake_Caster_Enable_Initial_State);

    /** - Configure output to control PS5 shutdown */
    gfGPIO_Initialize(&sPS5_Shutdown, GPIO_MODE_OUTPUT, (uint16_t)gParameters.ulPin_PS5_Shutdown, gParameters.ePS5_Shutdown);

    /** - Configure output to control PS5 voltage selection */
    gfGPIO_Initialize(&sPS5_VoltageSelect, GPIO_MODE_OUTPUT, (uint16_t)gParameters.ulPin_PS5_VoltageSelect, gParameters.ePS5_VoltageSelect);

    /** - Configure output to control PS6 shutdown */
    gfGPIO_Initialize(&sPS6_Shutdown, GPIO_MODE_OUTPUT, (uint16_t)gParameters.ulPin_PS6_Shutdown, gParameters.ePS6_Shutdown);

    /** - Configure output to control PS6 voltage selection */
    gfGPIO_Initialize(&sPS6_VoltageSelect, GPIO_MODE_OUTPUT, (uint16_t)gParameters.ulPin_PS6_VoltageSelect, gParameters.ePS5_VoltageSelect);

    /** - Configure steering crosscheck synchronization pin (should use parameters) */
    gfGPIO_Initialize(&eSupv_sync, GPIO_MODE_OUTPUT, (uint16_t)gParameters.ulPin_supv_synchronization, gParameters.eSupv_Synchronization);

    /** - Initialize the EMOS peripheral */
    gvEMIOS_Initialize(gParameters.ulPWM_CountsPerPeriod);

    /** - Initialize the analog to digital converter */
    fADCInitialized = gfANALOG_Initialize();

    /** - Initialize general purpose output #1 */
    gvGP_Initialize(GP_1);

    /** - Initialize general purpose output #2 */
    gvGP_Initialize(GP_2);

    /** - Initialize general purpose output #8 */
    gvGP_Initialize(GP_8);

    /** - Initialize general purpose output L1 */
    gvGP_Initialize(GP_L1);

    /** - Initialize Quadrature1 I/O */
    gvQUAD1_Initialize();

    /** - Initialize Quadrature3 I/O */
    gvQUAD3_Initialize();

    /** - Initialize Quadrature Absolute I/O and register interrupts */
    gfQUADAbsoluteInitialize( &(gQuadratureAbsoluteObject[0]), &(gFRAM_PowerDownInfo.Data.QuadratureAbsoluteWorkData[0]), &(gParameters.QuadratureAbsoluteParameters[0]), &(gParameters.QuadratureAbsoluteIOConfig[0]), "Q_Quad_Abs_1" );

    /** - Initialize Interrupts for Quadrature absolute encoder */
    InterruptNumber.uwVectorNum = (uint16_t)gQuadratureAbsoluteObject[0].QuadratureAbsoluteIOConfig.ulEMIOSVector_A;
    gvINTC_RegisterHandler(&gvQUADAbsolute0ISR, InterruptNumber);
    InterruptNumber.uwVectorNum = (uint16_t)gQuadratureAbsoluteObject[0].QuadratureAbsoluteIOConfig.ulEMIOSVector_B;
    gvINTC_RegisterHandler(&gvQUADAbsolute0ISR, InterruptNumber);

    /** - Initialize PS3 power supply I/O */
    gvPS3_Initialize();

    /** - Initialize all Potentiometers on the module: 1, 3, and 4 - additionally on the large VCM: 5-9 */
    gvANALOG_InitializePotentiometerInputs();

    /** - Initialize 4-20mA I/O */
    gv4_20mA_Initialize();

    /** - Initialize the undervoltage monitor */
    vIO_Undervoltage_Init();

    /** - Initialize the main 5V reset */
    vIO_Main5VReset_Init();

    /** - Initialize switch input drivers */
    gvSDI_Initialize();

    /** - Initialize external interrupts */
    gvXINT_Initialize();

    /** - Initialize Fault LED and turn off */
    gfLED_Initialize(&Fault_LED, (uint16_t)gParameters.ulFault_LED_Pin);

    /** - Initialize handler object for supervisor events */
    gvDEVREP_VCM_Supervisor_Initialize();

    /** - Initialize handler object for HDM events */
    gvDEVREP_HDM_Initialize();

    /** - Initialize handler object for SCM events */
    gvDEVREP_SCM_Initialize();

    /** - Initialize Infolink protocol */
    gvInfo1Initialize();
    
    /** - Initialize the accumulated data meters */
    gvAccumDataInitialize();
    
    /** - Initialize power management register, associated with FRAM power down write test */
    gvInitPowerManagementReg();

    /** - Call initialization of thread instrumentation function */
    gvINST_Thread_Init(&sINST_Thread_IO, gParameters.ulIO_THREAD_TICK_RATE * 1000u);

    /** - Set up timer to schedule task at desired periodic rate */
    ulStatus = gulOS_TimerCreate(&IO_Timer, IO_SCHEDULE_TIMER_NAME, &vIO_Thread_TimerEntry, 0u, gParameters.ulIO_THREAD_TICK_RATE, gParameters.ulIO_THREAD_TICK_RATE, TRUE);

    /** - Print status of the timer creation */
    gvPrint_TimerCreateStatus(IO_SCHEDULE_TIMER_NAME, ulStatus);

    /** - Suspend task until next time the scheduling timer expires */
    gulOS_ThreadSuspend(&IO_Thread);

    /** Thread body: */
    /* polyspace<RTE:NTL:Not a defect:No action planned> */
    for ( ;; )
    {
        /** - Trap thread if FLASH has been reflashed to force a power cycle */
        gvXCP_ReflashTrap();
        
        /** - Check if ADC has been initialized yet */
        if ( fADCInitialized == TRUE )
        {
            /** - Set supervisor synchronization signal to a high state if it's time */
            ulSync_rate += gParameters.ulIO_THREAD_TICK_RATE;
            if ( ulSync_rate >= gParameters.ulSupv_Synchronization_Rate )
            {
                ulSync_rate = 0u;
                gfGPIO_SetOutputState(&eSupv_sync, GPIO_STATE_HIGH);
            }

            /** - Trigger A/D Scan for ADC A/B0 */
            gvADC_TriggerModule0Scan();

            /** - Trigger A/D Scan for ADC A/B1 */
            gvADC_TriggerModule1Scan();

            /** - Suspend thread until the semaphore is set indicating the channel A0 scan is complete */
            ulStatus = gulOS_SemaphoreGet(&ADCA_Conversion_DoneSemaphore, ADC_DONE_SEMAPHORE_TIMEOUT);

            /** - If there was a failure, report @ref DEMEV_VCM_IO_ADCA_DONE_SEMAPHORE_GET_ERROR */
            if ( OS_SUCCESS != ulStatus )
            {
                gvDEM_ReportEventState(DEMEV_VCM_IO_ADCA_DONE_SEMAPHORE_GET_ERROR,
                                       DEM_STATE_ACTIVE1);
            }

            /** - Suspend thread until the semaphore is set indicating the channel B0 scan is complete */
            ulStatus = gulOS_SemaphoreGet(&ADCB_Conversion_DoneSemaphore, ADC_DONE_SEMAPHORE_TIMEOUT);

            /** - Set supervisor synchronization signal low */
            gfGPIO_SetOutputState(&eSupv_sync, GPIO_STATE_LOW);

            /** - If there was a failure, report @ref DEMEV_VCM_IO_ADCB_DONE_SEMAPHORE_GET_ERROR */
            if ( OS_SUCCESS != ulStatus )
            {
                gvDEM_ReportEventState(DEMEV_VCM_IO_ADCB_DONE_SEMAPHORE_GET_ERROR,
                                       DEM_STATE_ACTIVE1);
            }

            /** - Call start of thread instrumentation function (NOTE: Start after A/D conversions since thread is relinquished to other threads) */
            gvINST_Thread_Start(&sINST_Thread_IO);

            /** - Process the analog input interface */
            gvANALOG_Process();

            /** - Process switch input drivers */
            gvSDI_Process();

            /** - Process general purpose driver 1 */
            gvGP_Process(GP_1, grGP1_Command);

            /** - Process general purpose driver 2 */
            gvGP_Process(GP_2, grGP2_Command);

            /** - Process general purpose driver 8 */
            gvGP_Process(GP_8, grGP8_Command);

            /** - Process general purpose driver L1 */
            gvGP_Process(GP_L1, grGPL1_Command);

            /** - Send PDOs for this thread */
            gvAG_Trigger_Periodic_PDOS(PROCESS_IO);

            /** - Process Infolink services */
            gvInfo1ServiceSlow((uint16_t)gParameters.ulIO_THREAD_TICK_RATE);
            gvInfo1ServiceFast((uint16_t)gParameters.ulIO_THREAD_TICK_RATE);
        }

        /** - Enable/disable the outer brake driver which is controlled by the supervisor processor */
        gfGPIO_SetOutputState(&Brake_outer_enable, eBrake_outer_enable_command);

        /** - Enable/disable the inner brake driver which is controlled by the supervisor processor */
        gfGPIO_SetOutputState(&Brake_inner_enable, eBrake_inner_enable_command);

        /** - Enable/disable the caster brake driver which is controlled by the supervisor processor */
        gfGPIO_SetOutputState(&Brake_caster_enable, eBrake_caster_enable_command);
                               
        /** - Refresh PS5 shutdown pin */
        gfGPIO_SetOutputState(&sPS5_Shutdown, sPS5_Shutdown.eState);

        /** - Refresh PS5 voltage selection pin */
        gfGPIO_SetOutputState(&sPS5_VoltageSelect, sPS5_VoltageSelect.eState);

        /** - Refresh PS6 shutdown pin */
        gfGPIO_SetOutputState(&sPS6_Shutdown, sPS6_Shutdown.eState);

        /** - Refresh PS6 voltage selection pin */
        gfGPIO_SetOutputState(&sPS6_VoltageSelect, sPS6_VoltageSelect.eState);

        /** - Process Quadrature1 I/O */
        gvQUAD1_Process();

        /** - Process Quadrature3 I/O */
        gvQUAD3_Process();

        /** - Process Quadrature absolute encoder 0 */
        gvQUADAbsoluteProcess(&(gQuadratureAbsoluteObject[0]));

        /** - Process PS3 power supply I/O */
        gvPS3_Process();

        /** - Process nonvolatile memory functions */
        gvNvMemory_Process();

        /** - Process 4-20mA I/O */
        gv4_20mA_Process();

        /** - Process potentiometer I/O */
        gvPotentiometer_Process();
        
        /** - Refresh fault LED state */
        gfLED_SetState(&Fault_LED, FALSE);

        /** - Poll the undervoltage pin if we are in the undervoltage state */
        if ( TRUE == fUndervoltageActive )
        {
            vIO_Poll_Undervoltage();
        }

        /** Check to see if RS422 loopback testing is not enabled */
        if( TRUE == gfIMMCOMM_NetworkReady() )
        {
            /** - Send SRO messages to the IMM periodically */
            /*    @todo  Find a better place for this */
            gvIMM_SendSROMessage(gParameters.ulIO_THREAD_TICK_RATE);

            /** - Send truck operation messages to the IMM periodically */
            /*    @todo  Find a better place for this */
            gvIMM_SendTruckOperationMsg(gParameters.ulIO_THREAD_TICK_RATE);

            if ( TRUE == gfEnergySourceStepRun )
            {
                /** - Send slow data messages to the IMM periodically */
                /*    @todo  Find a better place for this */
                gvIMM_SendSlowDataMsg(gParameters.ulIO_THREAD_TICK_RATE);
            }

            /** - Send vehicle data messages to the IMM */
            /*    @todo  Find a better place for this */
            gvIMM_SendVehicleDataMsg(gParameters.ulIO_THREAD_TICK_RATE);

            /** - Send keyboard data message to the IMM */
            /*    @todo  Find a better place for this */
            gvIMM_ArmrestKeyboardDataMessage();
        }

        /** - Update accumulated data meters */
        gvAccumDataService(gParameters.ulIO_THREAD_TICK_RATE);

        /** - Read the number of bytes written during the FRAM power down write test */
        gvPDWTestReadBytesWritten();
        
        /** - Report execution to watchdog */
        gvWatchdog_ReportThreadExcecution(ubWatchdogID);

        /** - Check if this is the first pass through the loop */
        if ( TRUE == fFirstTimeThrough )
        {
            /** - Yes, clear flag */
            fFirstTimeThrough = FALSE;

            /** - Tell the watchdog to begin monitoring this thread */
            gvWatchdog_Begin(ubWatchdogID);
        }

        /** - Run NOR Flash write test, if requested */
        if ( TRUE == gTestMode_Parameters.fInitNorFlashWriteTest )
        {
            /** - Take control of the file system */
            gfFS_GetControl();

            /** - Queue the test command */
            gfFS_WriteTest();

            /** - Return control of the file system */
            gfFS_GiveControl();

            /** - Clear the test flag */
            gTestMode_Parameters.fInitNorFlashWriteTest = FALSE;
        }

        /** - Run NOR Flash read test, if requested */
        if ( TRUE == gTestMode_Parameters.fInitNorFlashReadTest )
        {
            /** - Take control of the file system */
            gfFS_GetControl();

            /** - Queue the test command */
            gfFS_ReadTest();

            /** - Return control of the file system */
            gfFS_GiveControl();

            /** - Clear the test flag */
            gTestMode_Parameters.fInitNorFlashReadTest = FALSE;
        }    

        gsbOprAlrmState = gsbOperatorAlarmSel(&gParameters.OperatorAlarm_params[gsbOprAlrmMode]);

        /** - Read the logic level at IRQ pins */
        sfLogicLevelIrq0 = gfIRQ_GetLogicLevel(0u);
        sfLogicLevelIrq1 = gfIRQ_GetLogicLevel(1u);
        sfLogicLevelIrq2 = gfIRQ_GetLogicLevel(2u);

        /** - Attach XCP driver to this thread */
        XcpEvent(2u);

        /** - Call end of thread instrumentation function */
        gvINST_Thread_End(&sINST_Thread_IO);

        /** - Suspend task until next time the scheduling timer expires */
        gulOS_ThreadSuspend(&IO_Thread);
    }
}

/**
 * @fn     os_thread_t *gpIO_GetThreadPointer(void)
 *
 * @brief  This function returns a pointer to the I/O thread control structure
 *
 * @return 32-bit address
 *
 * @author Walter Conley
 *
 * @note   N/A
 *
 */
os_thread_t *gpIO_GetThreadPointer(void)
{
    /** ###Functional overview: */

    /** - Return address of the I/O thread control structure */
    return(&IO_Thread);
}

/**
 * @fn      void vIO_Thread_TimerEntry(uint32_t ulInput)
 *
 * @brief   This function is called each time the scheduling timer expires
 *
 * @param   ulInput = Input value defined when the timer is created
 *
 * @return  N/A
 *
 * @author  Walter Conley
 *
 * @note    N/A
 *
 */
void vIO_Thread_TimerEntry(uint32_t ulInput)
{
    /** ###Functional overview: */

    /** - Wake the thread up for the next execution cycle */
    gulOS_ThreadResume(&IO_Thread);
}

/**
 * @fn     void gvADC_ChannelA_ScanComplete(void)
 *
 * @brief  This function is called when an A/D channel A scan is complete
 *
 * @return N/A
 *
 * @author Darrin Ihle
 *
 * @note   N/A
 *
 */
void gvADC_ChannelA_ScanComplete(void)
{
    /** ###Functional Overview: */

    /** - Resume thread after Channel A scan completes */
    gulOS_SemaphoreCeilingPut(&ADCA_Conversion_DoneSemaphore, 1u);
}

/**
 * @fn     void gvADC_ChannelB_ScanComplete(void)
 *
 * @brief  This function is called when an A/D channel B scan is complete
 *
 * @return N/A
 *
 * @author Darrin Ihle
 *
 * @note   N/A
 *
 */
void gvADC_ChannelB_ScanComplete(void)
{
    /** ###Functional Overview: */

    /** - Resume thread after Channel B scan completes */
    gulOS_SemaphoreCeilingPut(&ADCB_Conversion_DoneSemaphore, 1u);
}

/**
 * @fn     void gvQUADAbsolute0ISR(uint32_t ulVectorNum)
 *
 * @brief  This function is registered as the handler for quadrature absolute 0 interrupts
 *
 * @param  ulVectorNum = Interrupt vector that triggered this function call
 *  
 * @return N/A
 *
 * @author Chris Graunke
 *
 * @note   N/A
 *
 */
void gvQUADAbsolute0ISR(uint32_t ulVectorNum)
{
    /** ###Functional overview: */

    /** - Check if the interrupt source is channel A */
    if ( ulVectorNum == gQuadratureAbsoluteObject[0].QuadratureAbsoluteIOConfig.ulEMIOSVector_A )
    {
        /** - Clear the Interrupt source */
        gvEMIOS_IO_ClearFlag(&(gQuadratureAbsoluteObject[0].QuadratureAbsoluteInputObjects).EMIOS_A);

        /** call quadrature absolute callback */
        gvQUADAbsoluteCallback(&(gQuadratureAbsoluteObject[0]), QUAD_ABS_CH_A);
    }

    /** - Check if the interrupt source is channel B */
    if ( ulVectorNum == gQuadratureAbsoluteObject[0].QuadratureAbsoluteIOConfig.ulEMIOSVector_B )
    {
        /** - Clear the Interrupt source */
        gvEMIOS_IO_ClearFlag( &(gQuadratureAbsoluteObject[0].QuadratureAbsoluteInputObjects).EMIOS_B);

        /** call quadrature absolute callback */
        gvQUADAbsoluteCallback(&(gQuadratureAbsoluteObject[0]), QUAD_ABS_CH_B);
    }
}

/**
 * @fn     static void vIO_Undervoltage_Init(void)
 *
 * @brief  This function to initialize the undervoltage pin and interrupt
 *
 * @return N/A
 *
 * @author Chris Graunke
 *
 * @note   N/A
 *
 */
static void vIO_Undervoltage_Init(void)
{
    /** ###Functional overview: */

    /** - Initialize EMIOS pin*/
    gfEMIOS_Input_Initialize(&UV_Trip, (uint16_t)gParameters.ulUndervoltage_EMIOS_Channel, (uint16_t)gParameters.ulPin_Undervoltage, FALSE, FALSE);

    /** - Configure EMIOS as interrupt */
    gvEMIOS_Configure_interrupt(&UV_Trip, gParameters.UndervoltageIntNum.uwVectorNum);

    /** - Register interrupt handler */
    gvINTC_RegisterHandler(&gvIO_UndervoltageISR, gParameters.UndervoltageIntNum);
}

/**
 * @fn     void gvIO_UndervoltageISR(uint32_t ulVectorNum)
 *
 * @brief  This function is registered as the handler for the undervoltage interrupt
 *
 * @param  ulVectorNum = Interrupt vector that triggered this function call
 *  
 * @return N/A
 *
 * @author Chris Graunke
 *
 * @note   N/A
 *
 */
void gvIO_UndervoltageISR(uint32_t ulVectorNum)
{
    /** ###Functional overview: */

    /** - disable quadrature absolute encoder */
    gvQUADAbsoluteDisable(&(gQuadratureAbsoluteObject[0]));

    /** - Save power down test data */
    gvPowerDownTestFRAMWrite();

    /** - Clear the interrupt flag */
    gvEMIOS_IO_ClearFlag(&UV_Trip);

    /** - Toggle the status flag */
    fUndervoltageActive = TRUE;
}

/**
 * @fn     static void vIO_Poll_Undervoltage(void)
 *
 * @brief  This function polls the undervoltage pin to determine if we have 
 *         recovered from an undervoltage condition
 *  
 * @return N/A
 *
 * @author Chris Graunke
 *
 * @note   N/A
 *
 */
static void vIO_Poll_Undervoltage(void)
{
    static uint8_t ubVoltageNormalTics = 0u;

    /** ###Functional overview: */

    /** - Check the state of the pin */
    if ( TRUE == gfEMIOS_GetGpioState(&UV_Trip) )
    {
        /** - If high, increment the timer */
        ubVoltageNormalTics++;
    }
    else
    {
        /** - Otherwise, reset the timer */
        ubVoltageNormalTics = 0u;
    }

    /** - Check if the timer exceeds the timeout */
    if ( ubVoltageNormalTics > (gParameters.ulUndervoltageRecoverTimeMs / gParameters.ulIO_THREAD_TICK_RATE) )
    {
        /** - Toggle the status flag */
        fUndervoltageActive = FALSE;

        /** - Reset the timeout tics */
        ubVoltageNormalTics = 0u;
    }
}

/**
 * @fn     static void vIO_Main5VReset_Init(void)
 *
 * @brief  This function to initialize the main 5V reset interrupt
 *
 * @return N/A
 *
 * @author Chris Graunke
 *
 * @note   N/A
 *
 */
static void vIO_Main5VReset_Init(void)
{
    /** ###Functional overview: */

    /** - Register interrupt handler */
    gvINTC_RegisterHandler(&gvIO_Main5VResetISR, gParameters.Main5VResetIntNum);
}

/**
 * @fn     void gvIO_Main5VResetISR(uint32_t ulVectorNum)
 *
 * @brief  This function is registered as the handler for the main 5V reset interrupt
 *
 * @param  ulVectorNum = Interrupt vector that triggered this function call
 *  
 * @return N/A
 *
 * @author Chris Graunke
 *
 * @note   N/A
 *
 */
void gvIO_Main5VResetISR(uint32_t ulVectorNum)
{
    /** ###Functional overview: */

    /** - disable quadrature absolute encoder */
    gvQUADAbsoluteDisable(&(gQuadratureAbsoluteObject[0]));

    /** - Shut down the drivers */
    gvGP_ShutdownAll();

    /** - Toggle the status flag */
    fMain5VResetUndervoltageActive = TRUE;

    /** - Print out debug message */
    gvMyPrintf_b("5V Reset Tripped!\n");
}
