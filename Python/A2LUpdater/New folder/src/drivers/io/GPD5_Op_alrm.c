/**
 *  @file                   GPD5_Op_alrm.c
 *  @brief                  This file provides an interface for GPD5 operation alarm
 *  @copyright              2015 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   07/6/2015
 *  
 *  @remark Author:         Paddy
 *  @remark Project Tree:   C1515
 *  
 */

/* Includes */
#include "GPD5_Op_alrm.h"

/*========================================================================* 
 *  SECTION - External variables that cannot be defined in header files   * 
 *========================================================================* 
 */

/*========================================================================* 
 *  SECTION - Local function prototypes                                   * 
 *========================================================================* 
 */

/*========================================================================* 
 *  SECTION - Local variables                                             * 
 *========================================================================* 
 */
static int8_t BuzzState;

/** @brief GPD5 Buzzer *//*
    @@ INSTANCE  = Buzzer
    @@ STRUCTURE = gpd5opal_params_t
    @@ END
 */
static gpd5opal_params_t Buzzer;

#define BUZZOFF 0
#define BUZZON 1

int8_t gsbOperatorAlarmSel(const gpd5opal_params_t *pObj)
{
    static uint8_t subBuzzModelst;
    static uint8_t subOfftime = 0;
    static uint8_t subOntime = 0;
    static uint16_t ssbNooftime = 0;



    if ( subBuzzModelst != pObj->ubAlarmMode )
    {
        Buzzer.ubAlarmMode =  pObj->ubAlarmMode;
        Buzzer.ubAlarmOffTicks =  pObj->ubAlarmOffTicks;
        Buzzer.ubAlarmOnTicks =  pObj->ubAlarmOnTicks;
        Buzzer.ubAlarmRepeat =  pObj->ubAlarmRepeat;
        BuzzState = BUZZOFF;
        subOntime = 0;
        subOfftime = 0;
        ssbNooftime = 0;

    }
    else
    {
        if ( ssbNooftime < Buzzer.ubAlarmRepeat )
        {
            if ( BUZZOFF == BuzzState )
            {
                if ( subOfftime < Buzzer.ubAlarmOffTicks )
                {
                    subOfftime++;
                    BuzzState = BUZZOFF;
                }
                else
                {
                    BuzzState = BUZZON;
                    subOfftime = 0;
                }
            }
            else if ( BUZZON == BuzzState )
            {
                if ( subOntime < Buzzer.ubAlarmOnTicks )
                {
                    subOntime++;
                    BuzzState = BUZZON;
                }
                else
                {
                    BuzzState = BUZZOFF;
                    subOntime = 0;
                    ssbNooftime++;
                }
            }
           
        }
        else
        {
              BuzzState = BUZZOFF;
              subOntime = 0;
              subOfftime = 0;
              ssbNooftime = Buzzer.ubAlarmRepeat;
        }
    }
    subBuzzModelst = Buzzer.ubAlarmMode;
    return    BuzzState;
}
