/**
 *  @file                   ag_output_router.c
 *  @brief                  This file implements the systems integration model output bus routing feature based
 *                          on FLASH parameterization.
 *  @copyright              2013 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   09/25/2013
 *
 *  @remark Author:         Walter Conley
 *  @remark Project Tree:   C1515
 *
 *  @note                   - Block diagram of the output router @image html Output_routing.png
 *
 */

/* Includes */
#include "typedefs.h"
#include "param.h"
#include "SystemsExportVars.h"
#include "supv_interface.h"
#include "GP_interface.h"
#include "OBD_data.h"
#include "test_api.h"
#include "InfoUser1.h"
#include "ag_sys_integration.h"
#include "ag_output_router.h"
#include "io_thread.h"

/*========================================================================*
 *  SECTION - External variables that cannot be defined in header files   *
 *========================================================================*
 */

/*========================================================================*
 *  SECTION - Local function prototypes                                   *
 *========================================================================*
 */

/*========================================================================*
 *  SECTION - Local variables                                             *
 *========================================================================*
 */

/** @brief Provides an address to read zero for unused model bus outputs
 */
static uint32_t ulAG_Zero = 0u;

/** @brief This is an array of module outputs that can be routed to the model bus outputs.
 *
 *  @note  WARNING!!! The enumerated type @ref ag_module_output_index_t is used as an index into
 *         this array so the order of both must match exactly.
 */
static void *spvAG_ModuleOutputAddressArray[AG_NUMB_MODULE_OUTPUTS] =
{
    &ulAG_Zero,                                         /* AG_OUT_NOT_USED                       */
    &grGP1_Command,                                     /* AG_OUT_GP1_COMMAND                    */
    &gSupv_Data.IO_Interface.GP_Interface[0].rCommand,  /* AG_OUT_SUPV_GP1_COMMAND               */
    &gSupv_Data.IO_Interface.GP_Interface[1].rCommand,  /* AG_OUT_SUPV_GP2_COMMAND               */
    &gSupv_Data.IO_Interface.GP_Interface[2].rCommand,  /* AG_OUT_SUPV_GP3_COMMAND               */
    &gSupv_Data.IO_Interface.GP_Interface[3].rCommand,  /* AG_OUT_SUPV_GP4_COMMAND               */
    &gSupv_Data.IO_Interface.GP_Interface[4].rCommand,  /* AG_OUT_SUPV_GP5_COMMAND               */
    &gSupv_Data.IO_Interface.GP_Interface[5].rCommand,  /* AG_OUT_SUPV_GP6_COMMAND               */
    &gSupv_Data.IO_Interface.GP_Interface[6].rCommand,  /* AG_OUT_SUPV_GP7_COMMAND               */
    &gSupv_Data.IO_Interface.GP_Interface[7].rCommand,  /* AG_OUT_SUPV_GP8_COMMAND               */
    &gSupv_Data.IO_Interface.GP_Interface[8].rCommand,  /* AG_OUT_SUPV_GP9_COMMAND               */
    &gSupv_Data.IO_Interface.GP_Interface[9].rCommand,  /* AG_OUT_SUPV_GP10_COMMAND              */
    &gSupv_Data.IO_Interface.GP_Interface[10].rCommand, /* AG_OUT_SUPV_GP11_COMMAND              */
    &gSupv_Data.IO_Interface.GP_Interface[11].rCommand, /* AG_OUT_SUPV_GP12_COMMAND              */
    &USR_s_OV.swTcmSpeedSetpoint,                       /* AG_OUT_TXN_SPEED_SETPOINT             */
    &USR_s_OV.uwTcmGPDriverControl,                     /* AG_OUT_TCM_GP1_COMMAND                */
    &USR_s_OV.uwTcmGPDriverControl,                     /* AG_OUT_TCM_GP2_COMMAND                */
    &USR_s_OV.uwTcmGPDriverControl,                     /* AG_OUT_TCM_GP3_COMMAND                */
    &USR_s_OV.uwTcmGPDriverControl,                     /* AG_OUT_TCM_GP4_COMMAND                */
    &USR_s_OV.uwTcmGPDriverControl,                     /* AG_OUT_TCM_GP5_COMMAND                */
    &USR_s_OV.uwTcmGP1Setpoint,                         /* AG_OUT_TCM_GP1_SETPOINT               */
    &USR_s_OV.uwTcmGP2Setpoint,                         /* AG_OUT_TCM_GP2_SETPOINT               */
    &USR_s_OV.uwTcmGP3Setpoint,                         /* AG_OUT_TCM_GP3_SETPOINT               */
    &USR_s_OV.uwTcmGP4Setpoint,                         /* AG_OUT_TCM_GP4_SETPOINT               */
    &USR_s_OV.uwHcmCommandWord,                         /* AG_OUT_HCM_SWITCH_ON                  */
    &USR_s_OV.uwHcmCommandWord,                         /* AG_OUT_HCM_POWERSTAGE_ON              */
    &USR_s_OV.uwHcmCommandWord,                         /* AG_OUT_HCM_HOURMETERS_ON              */
    &USR_s_OV.uwHcmCommandWord,                         /* AG_OUT_HCM_RAISE_ACTIVE               */
    &USR_s_OV.uwHcmCommandWord,                         /* AG_OUT_HCM_LOWER_ACTIVE               */
    &USR_s_OV.uwScmCommandWord,                         /* AG_OUT_SCM_SWITCH_ON                  */
    &USR_s_OV.uwScmCommandWord,                         /* AG_OUT_SCM_POWERSTAGE_ON              */
    &USR_s_OV.uwScmCommandWord,                         /* AG_OUT_SCM_DEENERGIZE_STR             */
    &USR_s_OV.uwScmCommandWord,                         /* AG_OUT_SCM_SYNC_DU                    */
    &USR_s_OV.uwTcmCommandWord,                         /* AG_OUT_TCM_SWITCH_ON                  */
    &USR_s_OV.uwTcmCommandWord,                         /* AG_OUT_TCM_POWERSTAGE_ON              */
    &USR_s_OV.uwTcmCommandWord,                         /* AG_OUT_TCM_BUSLOAD_ON                 */
    &USR_s_OV.uwTcmCommandWord,                         /* AG_OUT_TCM_HOURMETERS_ON              */
    &USR_s_OV.swSteerCommandSetpoint1Deg100,            /* AG_OUT_SCM_COMMAND_SETPOINT1          */
    &USR_s_OV.swSteerCommandSetpoint2Deg100,            /* AG_OUT_SCM_COMMAND_SETPOINT2          */
    &USR_s_OV.ubMstrOkCrsChk,                           /* AG_OUT_SUPV_MSTR_OK                   */
    &USR_s_OV.ubFreshnessCounter_TCM_TX1,               /* AG_OUT_TCM_FRESHNESS1                 */
    &USR_s_OV.ubFreshnessCounter_SCM_TX1,               /* AG_OUT_SCM_FRESHNESS1                 */
    &USR_s_OV.ubFreshnessCounter_SCM_TX2,               /* AG_OUT_SCM_FRESHNESS2                 */
    &USR_s_OV.ubFreshnessCounter_HCM_TX1,               /* AG_OUT_HCM_FRESHNESS1                 */
    &USR_s_OV.swSteerSpeedMaxRPM,                       /* AG_OUT_SCM_MAX_STEER_SPEED            */
    &USR_s_OV.swHdmRefSpeed,                            /* AG_OUT_HDM_SPEED_COMMAND              */
    &USR_s_OV.uwHdmCommand,                             /* AG_OUT_HDM_COMMAND_WORD               */
    &USR_s_OV.ubIOM0807_1_GP_Enables,                   /* AG_OUT_IOM0807_1_GP1_ENABLE           */
    &USR_s_OV.ubIOM0807_1_GP_Enables,                   /* AG_OUT_IOM0807_1_GP2_ENABLE           */
    &USR_s_OV.ubIOM0807_1_GP_Enables,                   /* AG_OUT_IOM0807_1_GP3_ENABLE           */
    &USR_s_OV.ubIOM0807_1_GP_Enables,                   /* AG_OUT_IOM0807_1_GP4_ENABLE           */
    &USR_s_OV.ubIOM0807_1_GP_Enables,                   /* AG_OUT_IOM0807_1_GP5_ENABLE           */
    &USR_s_OV.ubIOM0807_1_GP_Enables,                   /* AG_OUT_IOM0807_1_GP6_ENABLE           */
    &USR_s_OV.ubIOM0807_1_GP_Enables,                   /* AG_OUT_IOM0807_1_GP7_ENABLE           */
    &USR_s_OV.ubIOM0807_1_GP_Commands,                  /* AG_OUT_IOM0807_1_GP1_BB_COMMAND       */
    &USR_s_OV.ubIOM0807_1_GP_Commands,                  /* AG_OUT_IOM0807_1_GP2_BB_COMMAND       */
    &USR_s_OV.ubIOM0807_1_GP_Commands,                  /* AG_OUT_IOM0807_1_GP3_BB_COMMAND       */
    &USR_s_OV.ubIOM0807_1_GP_Commands,                  /* AG_OUT_IOM0807_1_GP4_BB_COMMAND       */
    &USR_s_OV.ubIOM0807_1_GP_Commands,                  /* AG_OUT_IOM0807_1_GP5_BB_COMMAND       */
    &USR_s_OV.ubIOM0807_1_GP_Commands,                  /* AG_OUT_IOM0807_1_GP6_BB_COMMAND       */
    &USR_s_OV.ubIOM0807_1_GP_Commands,                  /* AG_OUT_IOM0807_1_GP7_BB_COMMAND       */
    &USR_s_OV.uwIOM0807_1_GP_CommandSetpoint[0],        /* AG_OUT_IOM0807_1_GP1_COMMAND_SETPOINT */
    &USR_s_OV.uwIOM0807_1_GP_CommandSetpoint[1],        /* AG_OUT_IOM0807_1_GP2_COMMAND_SETPOINT */
    &USR_s_OV.uwIOM0807_1_GP_CommandSetpoint[2],        /* AG_OUT_IOM0807_1_GP3_COMMAND_SETPOINT */
    &USR_s_OV.uwIOM0807_1_GP_CommandSetpoint[3],        /* AG_OUT_IOM0807_1_GP4_COMMAND_SETPOINT */
    &USR_s_OV.uwIOM0807_1_GP_CommandSetpoint[4],        /* AG_OUT_IOM0807_1_GP5_COMMAND_SETPOINT */
    &USR_s_OV.uwIOM0807_1_GP_CommandSetpoint[5],        /* AG_OUT_IOM0807_1_GP6_COMMAND_SETPOINT */
    &USR_s_OV.uwIOM0807_1_GP_CommandSetpoint[6],        /* AG_OUT_IOM0807_1_GP7_COMMAND_SETPOINT */
    &USR_s_OV.swHcmSpeedSetpoint,                       /* AG_OUT_HYD_SPEED_SETPOINT             */
    &USR_s_OV.uwHcmGPDriverControl,                     /* AG_OUT_HCM_GP1_COMMAND                */
    &USR_s_OV.uwHcmGPDriverControl,                     /* AG_OUT_HCM_GP2_COMMAND                */
    &USR_s_OV.uwHcmGPDriverControl,                     /* AG_OUT_HCM_GP3_COMMAND                */
    &USR_s_OV.uwHcmGPDriverControl,                     /* AG_OUT_HCM_GP4_COMMAND                */
    &USR_s_OV.uwHcmGPDriverControl,                     /* AG_OUT_HCM_GP5_COMMAND                */
    &USR_s_OV.uwHcmGP1Setpoint,                         /* AG_OUT_HCM_GP1_SETPOINT               */
    &USR_s_OV.uwHcmGP2Setpoint,                         /* AG_OUT_HCM_GP2_SETPOINT               */
    &USR_s_OV.uwHcmGP3Setpoint,                         /* AG_OUT_HCM_GP3_SETPOINT               */
    &USR_s_OV.uwHcmGP4Setpoint,                         /* AG_OUT_HCM_GP4_SETPOINT               */
    &grGP2_Command,                                     /* AG_OUT_GP2_COMMAND                    */
    &grGP8_Command,                                     /* AG_OUT_GP8_COMMAND                    */
    &grGPL1_Command,                                    /* AG_OUT_GPL1_COMMAND                   */
    &gVcmSlowData.rAccess2_1_ControllerTemp,            /* AG_OUT_HCM_CONTROLLER_TEMP            */
    &gVcmSlowData.rAccess2_2_ControllerTemp,            /* AG_OUT_HDM_CONTROLLER_TEMP            */
    &gVcmSlowData.rAccess3_1_ControllerTemp,            /* AG_OUT_TCM1_CONTROLLER_TEMP           */
    &gVcmSlowData.rAccess3_2_ControllerTemp,            /* AG_OUT_TCM2_CONTROLLER_TEMP           */
    &gVcmSlowData.rAccess5_1_ControllerTemp,            /* AG_OUT_SCM_CONTROLLER_TEMP            */
    &(gQuadratureAbsoluteObject[0].QuadratureAbsoluteIOData.uwDirectionInformation1Sig), /* AG_OUT_DIR_INFO_QUAD_ABS_1 */
    &USR_s_OV.swTractionMotorSpeedRPMTx,                /* AG_OUT_SCM_MOTOR_SPD                  */
    &USR_s_OV.swStrCmd1,                                /* AG_OUT_SCM_STRCMD1                    */                 
    &USR_s_OV.swTrxSpdReq,                              /* AG_OUT_TRC_SPD_CMD                    */
    &USR_s_OV.ubStp_trcOKChk                            /* AG_OUT_STPTRC_OKCHK                   */
};

/** @brief This is a list of model bus outputs that module outputs are routed to. Model bus output routing is done
 *         by adjusting [OutputRouting](@ref ag_matrix_parameters_t::OutputRouting) to the appropriate value of type
 *         @ref ag_module_output_index_t.
 *
 *  @note  When changing the length of this list, make sure to update @ref AG_NUMB_MODEL_BUS_OUTPUTS
 *
 *  @note  Use of @ref AG_NUMB_MODEL_BUS_OUTPUTS is not necessary here but will serve as a reminder to
 *         keep @ref AG_NUMB_MODEL_BUS_OUTPUTS up to date.
 *
 *  @note  @ref AG_NUMB_MODEL_BUS_OUTPUTS cannot be set using a precompiler calculation since it is also used to
 *         define space in the parameter table.
 */
static void *spvAG_ModelOutputBusAddressArray[AG_NUMB_MODEL_BUS_OUTPUTS] =
{
    &gEd_outputs.ulEdDriversOn,                                 /*   0 */
    &gEd_outputs.ulHcmCmd,                                      /*   1 */
    &gEd_outputs.ulHcmCmd,                                      /*   2 */
    &gEd_outputs.ulScmCmd,                                      /*   3 */
    &gEd_outputs.ulScmCmd,                                      /*   4 */
    &gEd_outputs.ulScmCmd,                                      /*   5 */
    &gEd_outputs.ulScmCmd,                                      /*   6 */
    &gEd_outputs.ulTcmCmd,                                      /*   7 */
    &gEd_outputs.ulTcmCmd,                                      /*   8 */
    &gEd_outputs.ulTcmCmd,                                      /*   9 */
    &gTraction_output.rBrake1Cmd,                               /*  10 */
    &gTraction_output.rBrake2Cmd,                               /*  11 */
    &gTraction_output.rBrake3Cmd,                               /*  12 */
    &gTraction_output.rTorqueCmd,                               /*  13 */
    &gTraction_output.ulTravelAlarmCmd,                         /*  14 */
    &gHyd_output.ulSVML,                                        /*  15 */
    &gHyd_output.ulSVMR,                                        /*  16 */
    &gHyd_output.ulLiftContactorCmd,                            /*  17 */
    &gHyd_output.slHDMSpeedCommand,                             /*  18 */
    &gHyd_output.slHCMSpeedCommand,                             /*  19 */
    &gHyd_output.ulSV_ACCY1_SELECT,                             /*  20 */
    &gHyd_output.ulSV_ACCY2_SELECT,                             /*  21 */
    &gHyd_output.ulSV_ACCY3_SELECT,                             /*  22 */
    &gHyd_output.ulSV_TILT_SELECT,                              /*  23 */
    &gHyd_output.ulPV_ACCY1_POS,                                /*  24 */
    &gHyd_output.ulPV_ACCY1_NEG,                                /*  25 */
    &gHyd_output.ulPV_ACCY2_POS,                                /*  26 */
    &gHyd_output.ulPV_ACCY2_NEG,                                /*  27 */
    &gHyd_output.ulPV_ACCY3_POS,                                /*  28 */
    &gHyd_output.ulPV_ACCY3_NEG,                                /*  29 */
    &gHyd_output.ulSV_ACCY1_POS,                                /*  30 */
    &gHyd_output.ulSV_ACCY1_NEG,                                /*  31 */
    &gHyd_output.ulSV_ACCY2_POS,                                /*  32 */
    &gHyd_output.ulSV_ACCY2_NEG,                                /*  33 */
    &gHyd_output.ulSV_ACCY3_POS,                                /*  34 */
    &gHyd_output.ulSV_ACCY3_NEG,                                /*  35 */
    &gHyd_output.ulSV_TILT_UP,                                  /*  36 */
    &gHyd_output.ulSV_TILT_DOWN,                                /*  37 */
    &gHyd_output.ulPV_TILT_UP,                                  /*  38 */
    &gHyd_output.ulPV_TILT_DOWN,                                /*  39 */
    &gHyd_output.ulPV_MAIN_LOWER,                               /*  40 */
    &gHyd_output.ulSV_ACCY_DIRECTION,                           /*  41 */
    &gHyd_output.ulPV_CYL1,                                     /*  42 */
    &gHyd_output.ulPV_CYL2,                                     /*  43 */
    &gHyd_output.ulPV_CYL3,                                     /*  44 */
    &gHyd_output.ulPV_ALL_ACCY,                                 /*  45 */
    &gHyd_output.ulSV_BYPASS,                                   /*  46 */
    &gHornBus.fHorn_on,                                         /*  47 */
    &gSteer_outputs.slMstrStrCmd,                               /*  48 */
    &gSteer_outputs.slSlvStrCmd,                                /*  49 */
    &gSteer_dx_out_bus.fStrU1OkCrossCk,                         /*  50 */
    &gSteer_outputs.slStrMtrSpdMax,                             /*  51 */
    &gFreshnessBus.ubFreshnessCounter[0],                       /*  52 */
    &gFreshnessBus.ubFreshnessCounter[1],                       /*  53 */
    &gFreshnessBus.ubFreshnessCounter[2],                       /*  54 */
    &gFreshnessBus.ubFreshnessCounter[3],                       /*  55 */
    &gFreshnessBus.ubFreshnessCounter[4],                       /*  56 */
    &gTractionIntegrationBus.Hourmeter_control.fActivateHours,  /*  57 */
    &gSteeringIntegrationBus.slSteerSpeedMax,                   /*  58 */
    &gED_HS_Bus.fED_HS_on,                                      /*  59 */
    &gHornBus.fHorn_on,                                         /*  60 */
    &gEd_outputs.ulHdmCmd,                                      /*  61 */
    &gFunction_active.fMainRaiseActive,                         /*  62 */
    &gFunction_active.fMainLowerActive,                         /*  63 */
    &gPowerbase_fb.Powerbase_fb_hcm.swControlTemp,              /*  64 */
    &gPowerbase_fb.Powerbase_fb_hdm.swControlTemp,              /*  65 */
    &gPowerbase_fb.Powerbase_fb_tcm1.swControlTemp,             /*  66 */ 
    &gPowerbase_fb.Powerbase_fb_tcm2.swControlTemp,             /*  67 */ 
    &gPowerbase_fb.Powerbase_fb_scm.swControlTemp,              /*  68 */
    &gSteer_outputs.rTfdCmd,                                    /*  69 */
    &gKPM_bus.rCommand,                                         /*  70 */
    &gFunction_active.eAccyVelDir,                              /*  71 */
    &gPowerbase_fb.Powerbase_fb_tcm1.rMotorSpeed,               /*  72 */ 
    &gSteer_inputs.slStrCmd1,                                   /*  73 */
    &gSteer_m2m_outputs.rTrxSpdLmtWhAngCmd,                     /*  74 */
    &gSteer_dx_out_bus.fIsStopTractCrossCkDetect,               /*  75 */
    &gHyd_output.ulSV_SCV,                                      /*  76 */
    &gsbOprAlrmState
};

/**
 * @fn      void gvAG_OutputRouter_Process(process_id_t eProcess)
 *
 * @brief   This function performs model output routing for the specified process using vehicle specific parameterization.
 *
 * @param   eProcess = Process ID of calling function
 *
 * @return  N/A
 *
 * @author  Walter Conley
 *
 * @note    N/A
 */
void gvAG_OutputRouter_Process(process_id_t eProcess)
{
    int32_t slOutput;

    /** ###Functional overview: */

    /** - Skip routing if CANape parameter disabled it */
    if ( FALSE != gTestMode_Parameters.fDoOutputRouting )
    {
        /** - Step through each model bus output */
        for ( slOutput = 0; slOutput < AG_NUMB_MODEL_BUS_OUTPUTS; slOutput++ )
        {
            /** - Make sure calling process is responsible for routing this entry */
            if ( gParameters.AG_MatrixConfiguration.OutputRouting[slOutput].eProcessID == eProcess )
            {
                /** - Route the correct signal to the specified model bus output  */
                gvAG_RouteMatrixEntry(spvAG_ModuleOutputAddressArray[gParameters.AG_MatrixConfiguration.OutputRouting[slOutput].eOutputIndex],
                                      &gParameters.AG_MatrixConfiguration.ModuleOutputConfig[gParameters.AG_MatrixConfiguration.OutputRouting[slOutput].eOutputIndex],
                                      spvAG_ModelOutputBusAddressArray[slOutput],
                                      &gParameters.AG_MatrixConfiguration.ModelOutputConfig[slOutput],
                                      gpAG_ScaleFactorArray);
            }
        }
    }
}
