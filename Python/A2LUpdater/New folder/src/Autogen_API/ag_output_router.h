/**
 *  @file                   ag_output_router.h
 *  @brief                  Interface for ag_output_router.c
 *  @copyright              2013 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   9/25/2013
 *
 *  @remark Author:         Walter Conley
 *  @remark Project Tree:   C1515
 *
 */

#ifndef AG_OUTPUT_ROUTER_H
#define AG_OUTPUT_ROUTER_H 1

/*========================================================================*
 *  SECTION - Global definitions
 *========================================================================*
 */

/** @def   AG_NUMB_MODEL_BUS_OUTPUTS
 *  @brief Define the number of nodes defined in the model output bus matrix
 *
 *  @note  This needs to be defined manually since it is needed before precompiler runs
 */
#define AG_NUMB_MODEL_BUS_OUTPUTS 78

/** @enum   ag_module_output_index
 *  @brief  These enumerations are used to route the associated model bus output to a module output. They are used to
 *          index into the module output matrix
 *
 *  @typedef ag_module_output_index_t
 *  @brief   ag_module_output_index type definition
 */
typedef enum ag_module_output_index
{
    /*
    @@ CONVERSION = module_output_index
    @@ A2L_TYPE = TABLE 0  "AG_OUT_NOT_USED" 
    @@                  1  "AG_OUT_GP1_COMMAND"
    @@                  2  "AG_OUT_SUPV_GP1_COMMAND"
    @@                  3  "AG_OUT_SUPV_GP2_COMMAND"
    @@                  4  "AG_OUT_SUPV_GP3_COMMAND"
    @@                  5  "AG_OUT_SUPV_GP4_COMMAND"
    @@                  6  "AG_OUT_SUPV_GP5_COMMAND"
    @@                  7  "AG_OUT_SUPV_GP6_COMMAND"
    @@                  8  "AG_OUT_SUPV_GP7_COMMAND"
    @@                  9  "AG_OUT_SUPV_GP8_COMMAND"
    @@                  10 "AG_OUT_SUPV_GP9_COMMAND"
    @@                  11 "AG_OUT_SUPV_GP10_COMMAND"
    @@                  12 "AG_OUT_SUPV_GP11_COMMAND"
    @@                  13 "AG_OUT_SUPV_GP12_COMMAND"
    @@                  14 "AG_OUT_TXN_SPEED_SETPOINT"
    @@                  15 "AG_OUT_TCM_GP1_COMMAND"
    @@                  16 "AG_OUT_TCM_GP2_COMMAND"
    @@                  17 "AG_OUT_TCM_GP3_COMMAND"
    @@                  18 "AG_OUT_TCM_GP4_COMMAND"
    @@                  19 "AG_OUT_TCM_GP5_COMMAND"
    @@                  20 "AG_OUT_TCM_GP1_SETPOINT"
    @@                  21 "AG_OUT_TCM_GP2_SETPOINT"
    @@                  22 "AG_OUT_TCM_GP3_SETPOINT"
    @@                  23 "AG_OUT_TCM_GP4_SETPOINT"
    @@                  24 "AG_OUT_HCM_SWITCH_ON"
    @@                  25 "AG_OUT_HCM_POWERSTAGE_ON"
    @@                  26 "AG_OUT_HCM_HOURMETER_ON"
    @@                  27 "AG_OUT_HCM_RAISE_ACTIVE"
    @@                  28 "AG_OUT_HCM_LOWER_ACTIVE"
    @@                  29 "AG_OUT_SCM_SWITCH_ON"
    @@                  30 "AG_OUT_SCM_POWERSTAGE_ON"
    @@                  31 "AG_OUT_SCM_DEENERGIZE_STR"
    @@                  32 "AG_OUT_SCM_SYNC_DU"
    @@                  33 "AG_OUT_TCM_SWITCH_ON"
    @@                  34 "AG_OUT_TCM_POWERSTAGE_ON"
    @@                  35 "AG_OUT_TCM_BUSLOAD_ON"
    @@                  36 "AG_OUT_TCM_HOURMETERS_ON"
    @@                  37 "AG_OUT_SCM_COMMAND_SETPOINT1"
    @@                  38 "AG_OUT_SCM_COMMAND_SETPOINT2"
    @@                  39 "AG_OUT_SUPV_MSTR_OK"
    @@                  40 "AG_OUT_TCM_FRESHNESS1"
    @@                  41 "AG_OUT_SCM_FRESHNESS1"
    @@                  42 "AG_OUT_SCM_FRESHNESS2"
    @@                  43 "AG_OUT_HCM_FRESHNESS1"
    @@                  44 "AG_OUT_SCM_MAX_STEER_SPEED"
    @@                  45 "AG_OUT_HDM_SPEED_COMMAND"
    @@                  46 "AG_OUT_COMMAND_WORD"
    @@                  47 "IOM0807-1 GP1 Enable"
    @@                  48 "IOM0807-1 GP2 Enable"
    @@                  49 "IOM0807-1 GP3 Enable"
    @@                  50 "IOM0807-1 GP4 Enable"
    @@                  51 "IOM0807-1 GP5 Enable"
    @@                  52 "IOM0807-1 GP6 Enable"
    @@                  53 "IOM0807-1 GP7 Enable"
    @@                  54 "IOM0807-1 GP1 BB Command"
    @@                  55 "IOM0807-1 GP2 BB Command"
    @@                  56 "IOM0807-1 GP3 BB Command"
    @@                  57 "IOM0807-1 GP4 BB Command"
    @@                  58 "IOM0807-1 GP5 BB Command"
    @@                  59 "IOM0807-1 GP6 BB Command"
    @@                  60 "IOM0807-1 GP7 BB Command"
    @@                  61 "IOM0807-1 GP1 Command Setpoint"
    @@                  62 "IOM0807-1 GP2 Command Setpoint"
    @@                  63 "IOM0807-1 GP3 Command Setpoint"
    @@                  64 "IOM0807-1 GP4 Command Setpoint"
    @@                  65 "IOM0807-1 GP5 Command Setpoint"
    @@                  66 "IOM0807-1 GP6 Command Setpoint"
    @@                  67 "IOM0807-1 GP7 Command Setpoint"
    @@                  68 "AG_OUT_HYD_SPEED_SETPOINT"
    @@                  69 "AG_OUT_HCM_GP1_COMMAND"
    @@                  70 "AG_OUT_HCM_GP2_COMMAND"
    @@                  71 "AG_OUT_HCM_GP3_COMMAND"
    @@                  72 "AG_OUT_HCM_GP4_COMMAND"
    @@                  73 "AG_OUT_HCM_GP5_COMMAND"
    @@                  74 "AG_OUT_HCM_GP1_SETPOINT"
    @@                  75 "AG_OUT_HCM_GP2_SETPOINT"
    @@                  76 "AG_OUT_HCM_GP3_SETPOINT"
    @@                  77 "AG_OUT_HCM_GP4_SETPOINT"
    @@                  78 "AG_OUT_GP2_COMMAND"
    @@                  79 "AG_OUT_GP8_COMMAND"
    @@                  80 "AG_OUT_GPL1_COMMAND"
    @@                  81 "AG_OUT_HCM_CONTROLLER_TEMP"
    @@                  82 "AG_OUT_HDM_CONTROLLER_TEMP"
    @@                  83 "AG_OUT_TCM1_CONTROLLER_TEMP"
    @@                  84 "AG_OUT_TCM2_CONTROLLER_TEMP"
    @@                  85 "AG_OUT_SCM_CONTROLLER_TEMP"
    @@                  86 "AG_OUT_DIR_INFO_QUAD_ABS_1"
    @@                  87 "AG_OUT_SCM_MOTOR_SPD"
    @@                  88 "AG_OUT_SCM_STRCMD1"
    @@                  89 "AG_OUT_TRC_SPD_CMD"
    @@                  90 "AG_OUT_STPTRC_OKCHK"
    @@ DESCRIPTION = "Module output enumeration used for model bus output routing"
    @@ END
    */
    AG_OUT_NOT_USED = 0,                    /**< Index for no connection to the output bus node */
    AG_OUT_GP1_COMMAND,                     /**< Index used to route model output bus to GP1 */
    AG_OUT_SUPV_GP1_COMMAND,                /**< Index used to route model output bus to supervisor's GP1 */
    AG_OUT_SUPV_GP2_COMMAND,                /**< Index used to route model output bus to supervisor's GP2 */
    AG_OUT_SUPV_GP3_COMMAND,                /**< Index used to route model output bus to supervisor's GP3 */
    AG_OUT_SUPV_GP4_COMMAND,                /**< Index used to route model output bus to supervisor's GP4 */
    AG_OUT_SUPV_GP5_COMMAND,                /**< Index used to route model output bus to supervisor's GP5 */
    AG_OUT_SUPV_GP6_COMMAND,                /**< Index used to route model output bus to supervisor's GP6 */
    AG_OUT_SUPV_GP7_COMMAND,                /**< Index used to route model output bus to supervisor's GP7 */
    AG_OUT_SUPV_GP8_COMMAND,                /**< Index used to route model output bus to supervisor's GP8 */
    AG_OUT_SUPV_GP9_COMMAND,                /**< Index used to route model output bus to supervisor's GP9 */
    AG_OUT_SUPV_GP10_COMMAND,               /**< Index used to route model output bus to supervisor's GP10 */
    AG_OUT_SUPV_GP11_COMMAND,               /**< Index used to route model output bus to supervisor's GP11 */
    AG_OUT_SUPV_GP12_COMMAND,               /**< Index used to route model output bus to supervisor's GP12 */
    AG_OUT_TXN_SPEED_SETPOINT,              /**< Index used to route model output bus to the traction speed set-point */
    AG_OUT_TCM_GP1_COMMAND,                 /**< Index used to route model output bus to the TCM GP1 command */
    AG_OUT_TCM_GP2_COMMAND,                 /**< Index used to route model output bus to the TCM GP2 command */
    AG_OUT_TCM_GP3_COMMAND,                 /**< Index used to route model output bus to the TCM GP3 command */
    AG_OUT_TCM_GP4_COMMAND,                 /**< Index used to route model output bus to the TCM GP4 command */
    AG_OUT_TCM_GP5_COMMAND,                 /**< Index used to route model output bus to the TCM GP5 command */
    AG_OUT_TCM_GP1_SETPOINT,                /**< Index used to route model output bus to the TCM GP1 setpoint */
    AG_OUT_TCM_GP2_SETPOINT,                /**< Index used to route model output bus to the TCM GP2 setpoint */
    AG_OUT_TCM_GP3_SETPOINT,                /**< Index used to route model output bus to the TCM GP3 setpoint */
    AG_OUT_TCM_GP4_SETPOINT,                /**< Index used to route model output bus to the TCM GP4 setpoint */
    AG_OUT_HCM_SWITCH_ON,                   /**< Index used to route model output bus to command the HCM to switch on */
    AG_OUT_HCM_POWERSTAGE_ON,               /**< Index used to route model output bus to command the HCM to enable the power base */
    AG_OUT_HCM_HOURMETERS_ON,               /**< Index used to route model output bus to command the HCM to enable the HCM hour meter timer */
    AG_OUT_HCM_RAISE_ACTIVE,                /**< Index used to route model output bus to show if raise is active */
    AG_OUT_HCM_LOWER_ACTIVE,                /**< Index used to route model output bus to show if lower is active */
    AG_OUT_SCM_SWITCH_ON,                   /**< Index used to route model output bus to command the SCM to switch on */
    AG_OUT_SCM_POWERSTAGE_ON,               /**< Index used to route model output bus to command the SCM to enable the power base */
    AG_OUT_SCM_DEENERGIZE_STR,              /**< Index used to route model output bus to command the SCM to de-energize steering */
    AG_OUT_SCM_SYNC_DU,                     /**< Index used to route model output bus to command the SCM to synchronize the drive unit */
    AG_OUT_TCM_SWITCH_ON,                   /**< Index used to route model output bus to command the TCM to switch on */
    AG_OUT_TCM_POWERSTAGE_ON,               /**< Index used to route model output bus to command the TCM to enable the power base */
    AG_OUT_TCM_BUSLOAD_ON,                  /**< Index used to route model output bus to command the TCM to enable the bus load */
    AG_OUT_TCM_HOURMETERS_ON,               /**< Index used to route model output bus to command the TCM to enable the TCM hour meter timer */
    AG_OUT_SCM_COMMAND_SETPOINT1,           /**< Index used to route model output bus to steer setpoint on the SCM Master */
    AG_OUT_SCM_COMMAND_SETPOINT2,           /**< Index used to route model output bus to steer setpoint on the SCM Slave */
    AG_OUT_SUPV_MSTR_OK,                    /**< Index used to route model output bus to Master Ok  on the Supervisor  */
    AG_OUT_TCM_FRESHNESS1,                  /**< Index used to route model output bus to freshness counter used in PDO1 to the TCM */
    AG_OUT_SCM_FRESHNESS1,                  /**< Index used to route model output bus to freshness counter used in PDO1 to the SCM */
    AG_OUT_SCM_FRESHNESS2,                  /**< Index used to route model output bus to freshness counter used in PDO2 to the SCM */
    AG_OUT_HCM_FRESHNESS1,                  /**< Index used to route model output bus to freshness counter used in PDO1 to the HCM */
    AG_OUT_SCM_MAX_STEER_SPEED,             /**< Index used to route model output bus to maximum steering rotational speed */
    AG_OUT_HDM_SPEED_COMMAND,               /**< Index used to route model output bus to HDM motor speed command */
    AG_OUT_HDM_COMMAND_WORD,                /**< Index used to route model output bus to HDM command word */
    AG_OUT_IOM0807_1_GP1_ENABLE,            /**< Index for IOM0807-1 GP1 Enable */
    AG_OUT_IOM0807_1_GP2_ENABLE,            /**< Index for IOM0807-1 GP2 Enable */
    AG_OUT_IOM0807_1_GP3_ENABLE,            /**< Index for IOM0807-1 GP3 Enable */
    AG_OUT_IOM0807_1_GP4_ENABLE,            /**< Index for IOM0807-1 GP4 Enable */
    AG_OUT_IOM0807_1_GP5_ENABLE,            /**< Index for IOM0807-1 GP5 Enable */
    AG_OUT_IOM0807_1_GP6_ENABLE,            /**< Index for IOM0807-1 GP6 Enable */
    AG_OUT_IOM0807_1_GP7_ENABLE,            /**< Index for IOM0807-1 GP7 Enable */
    AG_OUT_IOM0807_1_GP1_BB_COMMAND,        /**< Index for IOM0807-1 GP1 BB Command */
    AG_OUT_IOM0807_1_GP2_BB_COMMAND,        /**< Index for IOM0807-1 GP2 BB Command */
    AG_OUT_IOM0807_1_GP3_BB_COMMAND,        /**< Index for IOM0807-1 GP3 BB Command */
    AG_OUT_IOM0807_1_GP4_BB_COMMAND,        /**< Index for IOM0807-1 GP4 BB Command */
    AG_OUT_IOM0807_1_GP5_BB_COMMAND,        /**< Index for IOM0807-1 GP5 BB Command */
    AG_OUT_IOM0807_1_GP6_BB_COMMAND,        /**< Index for IOM0807-1 GP6 BB Command */
    AG_OUT_IOM0807_1_GP7_BB_COMMAND,        /**< Index for IOM0807-1 GP7 BB Command */
    AG_OUT_IOM0807_1_GP1_COMMAND_SETPOINT,  /**< Index for IOM0807-1 GP1 Command Setpoint */
    AG_OUT_IOM0807_1_GP2_COMMAND_SETPOINT,  /**< Index for IOM0807-1 GP2 Command Setpoint */
    AG_OUT_IOM0807_1_GP3_COMMAND_SETPOINT,  /**< Index for IOM0807-1 GP3 Command Setpoint */
    AG_OUT_IOM0807_1_GP4_COMMAND_SETPOINT,  /**< Index for IOM0807-1 GP4 Command Setpoint */
    AG_OUT_IOM0807_1_GP5_COMMAND_SETPOINT,  /**< Index for IOM0807-1 GP5 Command Setpoint */
    AG_OUT_IOM0807_1_GP6_COMMAND_SETPOINT,  /**< Index for IOM0807-1 GP6 Command Setpoint */
    AG_OUT_IOM0807_1_GP7_COMMAND_SETPOINT,  /**< Index for IOM0807-1 GP7 Command Setpoint */
    AG_OUT_HYD_SPEED_SETPOINT,              /**< Index used to route model output bus to the hydraulic speed set-point */
    AG_OUT_HCM_GP1_COMMAND,                 /**< Index used to route model output bus to the HCM GP1 command */
    AG_OUT_HCM_GP2_COMMAND,                 /**< Index used to route model output bus to the HCM GP2 command */
    AG_OUT_HCM_GP3_COMMAND,                 /**< Index used to route model output bus to the HCM GP3 command */
    AG_OUT_HCM_GP4_COMMAND,                 /**< Index used to route model output bus to the HCM GP4 command */
    AG_OUT_HCM_GP5_COMMAND,                 /**< Index used to route model output bus to the HCM GP5 command */
    AG_OUT_HCM_GP1_SETPOINT,                /**< Index used to route model output bus to the HCM GP1 setpoint */
    AG_OUT_HCM_GP2_SETPOINT,                /**< Index used to route model output bus to the HCM GP2 setpoint */
    AG_OUT_HCM_GP3_SETPOINT,                /**< Index used to route model output bus to the HCM GP3 setpoint */
    AG_OUT_HCM_GP4_SETPOINT,                /**< Index used to route model output bus to the HCM GP4 setpoint */
    AG_OUT_GP2_COMMAND,                     /**< Index used to route model output bus to GP2 */
    AG_OUT_GP8_COMMAND,                     /**< Index used to route model output bus to GP8 */
    AG_OUT_GPL1_COMMAND,                    /**< Index used to route model output bus to GPL1 */
    AG_OUT_HCM_CONTROLLER_TEMP,             /**< Index used to route model output bus to VCM slow data structure's HCM Controller Temperature */
    AG_OUT_HDM_CONTROLLER_TEMP,             /**< Index used to route model output bus to VCM slow data structure's HDM Controller Temperature */
    AG_OUT_TCM1_CONTROLLER_TEMP,            /**< Index used to route model output bus to VCM slow data structure's TCM1 Controller Temperature */
    AG_OUT_TCM2_CONTROLLER_TEMP,            /**< Index used to route model output bus to VCM slow data structure's TCM2 Controller Temperature */
    AG_OUT_SCM_CONTROLLER_TEMP,             /**< Index used to route model output bus to VCM slow data structure's SCM Controller Temperature */
    AG_OUT_DIR_INFO_QUAD_ABS_1,             /**< Index used to route model output bus to Quadrature absolute encoder direction change input */
    AG_OUT_SCM_MOTOR_SPD,                   /**< Index used to route model output bus to SCM motor speed */
    AG_OUT_SCM_STRCMD1,                     /**< Index used to route model output bus to Steer command 1 */
    AG_OUT_TRC_SPD_CMD,                     /**< Index used to route model output bus to Traction speed command */
    AG_OUT_STPTRC_OKCHK,                    /**< Index used to route model output bus to Stop Traction check ok */
    AG_NUMB_MODULE_OUTPUTS                  /**< Number of module outputs in matrix */
}ag_module_output_index_t;

/*========================================================================*
 *  SECTION - extern global variables (minimize global variable use)      *
 *========================================================================*
 */

/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */
extern void gvAG_OutputRouter_Process(process_id_t eProcess);

#endif /* #ifndef AG_OUTPUT_ROUTER_H */

