/**
 *  @file                   ag_common.c
 *  @brief                  This file contains the thread for the common integration model code
 *  @copyright              2013 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   07/23/2013
 *
 *
 *  @remark Author:         Walter Conley
 *  @remark Project Tree:   C1515
 *
 */

/* Includes */
#include "typedefs.h"
#include "tx_api.h"
#include "debug_support.h"
#include "XcpProf.h"
#include "SystemsImport.h"
#include "CO_Application.h"
#include "io_thread.h"
#include "ag_sys_integration.h"
#include "INTC.h"
#include "CANopen.h"
#include "ag_common.h"
#include "SystemsExportVars.h"
#include "supv_interface.h"
#include "ag_input_router.h"
#include "ag_output_router.h"
#include "param.h"
#include "potentiometer.h"
#pragma ghs startnomisra
#include "CalibrationSIMbuild.h"
#include "commonSIMbuild.h"
#include "EnergySourceSIMbuild.h"
#include "hydraulicSIMbuild.h"
#include "SteeringSIMbuild.h"
#include "TractionSIMbuild.h"
#include "HmsSim_BrakeBuild.h"
#include "HmsSim_CommunicationBuild.h"
#include "HmsSim_EdBuild.h"
#include "HmsSim_GpdBuild.h"
#include "HmsSim_HcmTcmBuild.h"
#include "HmsSim_HydraulicBuild.h"
#include "HmsSim_PowerSuppliesBuild.h"
#include "HmsSim_SteeringBuild.h"
#include "HmsSim_TractionBuild.h"
#include "HmsSim_VcmInputsBuild.h"
#include "VehicleFBSIMbuild.h"
#pragma ghs endnomisra
#include "pd_data.h"
#include "devrep_vcm_model.h"
#include "watchdog.h"
#include "XcpUserFlash.h"
#include "EventManager.h"
#include "quadrature_absolute.h"
#include "instrument.h"
#include "features_interface.h"
#include "ag_common.h"

/*========================================================================*
 *  SECTION - External variables that cannot be defined in header files   *
 *========================================================================*
 */

/*========================================================================*
 *  SECTION - Local function prototypes                                   *
 *========================================================================*
 */
void vAG_Common_ThreadEntry(uint32_t ulInput);
void vAG_Common_Thread_TimerEntry(uint32_t ulInput);
uint32_t gulAG_Common_Initialize(void);
os_thread_t *gpAG_Common_GetThreadPointer(void);

/*========================================================================*
 *  SECTION - Local variables                                             *
 *========================================================================*
 */

/** @brief Thread control block for the thread used to handle the common integration model execution
 */
static os_thread_t AG_Common_Thread;

/** @brief Define memory used for the common integration model thread stack
 */
static uint8_t ubAG_Common_ThreadStack[AG_COMMON_STACK_SIZE];

/** @brief Define a timer used to set the periodic execution rate for the common integration model thread
 */
static os_timer_t  AG_Common_Timer;

/** @brief This is used to show how a scale factor can be defined and included in @ref gpAG_ScaleFactorArray and @ref ag_sf_index_t.
 *         It could be something that is calibrated and stored in FRAM.
 *
 *  @todo  Eliminate this at some point
 */
static scale_factor_t sSF_MainHeight = { 1.0f, 0.0f };

/** @brief Thread identifier for the watchdog
 */
static uint8_t ubWatchdogID;

/** @brief Flag that shows if the Energy Source step function has been run
 */
boolean gfEnergySourceStepRun = FALSE;

/** @brief This is an array of pointers to scale factors that can be mapped to model bus or module I/O using
 *         the enumerated type @ref sb_var_type_t in the configuration parameters.
 */
const scale_factor_t * const gpAG_ScaleFactorArray[AG_SF_NUMB_ELEMENTS] =
{
    /* AG_SF_ARRAY_00      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[0],
    /* AG_SF_ARRAY_01      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[1],
    /* AG_SF_ARRAY_02      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[2],
    /* AG_SF_ARRAY_03      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[3],
    /* AG_SF_ARRAY_04      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[4],
    /* AG_SF_ARRAY_05      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[5],
    /* AG_SF_ARRAY_06      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[6],
    /* AG_SF_ARRAY_07      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[7],
    /* AG_SF_ARRAY_08      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[8],
    /* AG_SF_ARRAY_09      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[9],
    /* AG_SF_ARRAY_10      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[10],
    /* AG_SF_ARRAY_11      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[11],
    /* AG_SF_ARRAY_12      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[12],
    /* AG_SF_ARRAY_13      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[13],
    /* AG_SF_ARRAY_14      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[14],
    /* AG_SF_ARRAY_15      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[15],
    /* AG_SF_ARRAY_16      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[16],
    /* AG_SF_ARRAY_17      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[17],
    /* AG_SF_ARRAY_18      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[18],
    /* AG_SF_ARRAY_19      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[19],
    /* AG_SF_ARRAY_20      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[20],
    /* AG_SF_ARRAY_21      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[21],
    /* AG_SF_ARRAY_22      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[22],
    /* AG_SF_ARRAY_23      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[23],
    /* AG_SF_ARRAY_24      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[24],
    /* AG_SF_ARRAY_25      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[25],
    /* AG_SF_ARRAY_26      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[26],
    /* AG_SF_ARRAY_27      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[27],
    /* AG_SF_ARRAY_28      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[28],
    /* AG_SF_ARRAY_29      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[29],
    /* AG_SF_ARRAY_30      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[30],
    /* AG_SF_ARRAY_31      */  &gParameters.AG_MatrixConfiguration.FixedScaleFactors[31],
    /* AG_SF_MAIN_HGT_GAIN */  &sSF_MainHeight,
    /* AG_SF_4_20MA_INPUT  */  &g4_20mA_Input_ScaleFactor
};

/** @brief AG thread data *//*
    @@ INSTANCE  = sINST_Thread_AG
    @@ STRUCTURE = inst_thread_t
    @@ END
 */
static inst_thread_t sINST_Thread_AG;

/**
 * @fn      uint32_t gulAG_Common_Initialize(void)
 *
 * @brief   This function initializes the thread used to handle the common integration model
 *
 * @return  Status of thread creation service
 *   - @ref OS_SUCCESS
 *   - @ref OS_THREAD_ERROR
 *   - @ref OS_PTR_ERROR
 *   - @ref OS_SIZE_ERROR
 *   - @ref OS_PRIORITY_ERROR
 *   - @ref OS_THRESH_ERROR
 *   - @ref OS_START_ERROR
 *   - @ref OS_CALLER_ERROR
 *
 * @author  Walter Conley
 *
 * @note    N/A
 */
uint32_t gulAG_Common_Initialize(void)
{
    uint32_t ulStatus;
    watchdog_thread_info_t WDThreadInfo;

    /** ###Functional overview: */

    /** - Create the transmit thread and start automatically */
    ulStatus = gulOS_ThreadCreate(&AG_Common_Thread,
                                  AG_COMMON_THREAD_NAME,
                                  &vAG_Common_ThreadEntry,
                                  AG_COMMON_THREAD_ENTRY_INPUT,
                                  &ubAG_Common_ThreadStack[0],
                                  AG_COMMON_STACK_SIZE,
                                  AG_COMMON_INITIAL_THREAD_PRIORITY,
                                  AG_COMMON_THREAD_PREEMPT_THRESH,
                                  AG_COMMON_THREAD_TIME_SLICE,
                                  OS_AUTO_START
                                 );

    /** - Print status of thread creation using debug port */
    gvPrint_ThreadCreateStatus(AG_COMMON_THREAD_NAME, ulStatus);

    /** - Register with the watchdog if we aren't in hardware test mode */
    if ( TML_HW_TEST_ENABLE_VAL != gParameters.ulTmlHwTestModeEnabled )
    {
        WDThreadInfo.ulExecutionRate = AG_COMMON_THREAD_TICK_RATE;
        WDThreadInfo.uwEventID = DEMEV_VCM_AG_COMMON_THREAD_WATCHDOG;
        WDThreadInfo.ubTimeoutTolerance = (uint8_t)gParameters.ulWD_AgCommonTolerance;
        WDThreadInfo.ubConcernCountMax = (uint8_t)gParameters.ulWD_AgCommonConcernCountMax;
        gvWatchdog_RegisterThread(&ubWatchdogID, &WDThreadInfo, AG_COMMON_THREAD_NAME);
    }

    /** - Return status to caller */
    return ulStatus;
}

/**
 * @fn     void vAG_Common_ThreadEntry(uint32_t ulInput)
 *
 * @brief  This function is the entry point for the thread that executes the common integration model code.
 *
 * @param  ulInput = Argument passed to this function specified by gulOS_ThreadCreate()
 *
 * @return N/A
 *
 * @author Walter Conley
 *
 * @note   N/A
 *
 */
void vAG_Common_ThreadEntry(uint32_t ulInput)
{
    uint32_t ulStatus;
    boolean  fCommEstablished = FALSE;
    boolean  fFirstTimeThrough = TRUE;
    uint32_t ulEnergySourceCounter = 1u;
    uint32_t ulSlowModelProceesingCounter = 1u;
    static uint8_t subAmcPdoTimerMs = 0u;
    uint32_t ulOldPriority;

    /** ###Functional overview: */

    /** Thread initialization code: */

    /** - Initilaize integration model interface */
    gvAG_Initialize();

    /** - Wait until supervisor/CANOpen communication is established, and features are configured */
    while ( FALSE == fCommEstablished )
    {
        gulOS_ThreadSleep(AG_COMMON_VEHICLE_INTEGRATION_MODEL_RATE);

        /** - Check for CANOpen Application running */
        if ( TRUE == gfCO_AppRunning() )
        {
            /** - Test features and communications */
            if ( TRUE == gfSUPV_CommunicationEstablished() )
            {
                if ( TRUE == gfCO_PdosReceived() )
                {
                    if ( TRUE == gfFI_FeaturesConfigured() )
                    {
                        fCommEstablished = TRUE;
                    }
                }
            }

            /** - If features and communications aren't established, send out the necessary PDOs to
             *    avoid CAN timeouts
             */
            if ( FALSE == fCommEstablished )
            {
                /** - Process output buses after model execution */
                gvAG_OutputRouter_Process(PROCESS_SYS_MODEL);
                gvAG_Trigger_Periodic_PDOS(PROCESS_SYS_MODEL);
                gvAG_Trigger_Periodic_PDOS(PROCESS_AMC);
            }
        }
    }


    /** - Initialize the vehicle integration model code */
    commonSIMbuild_initialize();
    SteeringSIMbuild_initialize();
    TractionSIMbuild_initialize();
    hydraulicSIMbuild_initialize();
    EnergySourceSIMbuild_initialize();
    HmsSim_BrakeBuild_initialize();
    HmsSim_CommunicationBuild_initialize();
    HmsSim_EdBuild_initialize();
    HmsSim_GpdBuild_initialize();
    HmsSim_HcmTcmBuild_initialize();
    HmsSim_HydraulicBuild_initialize();
    HmsSim_PowerSuppliesBuild_initialize();
    HmsSim_SteeringBuild_initialize();
    HmsSim_TractionBuild_initialize();
    HmsSim_VcmInputsBuild_initialize();
    VehicleFBSIMbuild_initialize();
    CalibrationSIMbuild_initialize();

    /** - Set up model thread priority */
    gulOS_ThreadPriorityChange(&AG_Common_Thread, AG_COMMON_FINAL_THREAD_PRIORITY, &ulOldPriority);

    /** - Initialize the model event handler */
    gvDEVREP_VCM_Model_Initialize();

    /** - Call initialization of thread instrumentation function */
    gvINST_Thread_Init(&sINST_Thread_AG, AG_COMMON_THREAD_TICK_RATE * 1000u);

    /** - Set up timer to schedule task at desired periodic rate */
    ulStatus = gulOS_TimerCreate(&AG_Common_Timer, AG_COMMON_SCHEDULE_TIMER_NAME, &vAG_Common_Thread_TimerEntry, 0u, AG_COMMON_THREAD_TICK_RATE, AG_COMMON_THREAD_TICK_RATE, TRUE);

    /** - Print status of the timer creation */
    gvPrint_TimerCreateStatus(AG_COMMON_SCHEDULE_TIMER_NAME, ulStatus);

    /** - Suspend task until next time the scheduling timer expires */
    gulOS_ThreadSuspend(&AG_Common_Thread);

    /** Thread body: */
    /* polyspace<RTE:NTL:Not a defect:No action planned> */
    for (;;)
    {
        /** - Call start of thread instrumentation function */
        gvINST_Thread_Start(&sINST_Thread_AG);

        /** - Trap thread if FLASH has been reflashed to force a power cycle */
        gvXCP_ReflashTrap();

        /** - XCP STIM event */
        XcpEvent(4u);

        /** - Prepare input buses for fast model execution */
        gvAG_InputRouter_Process(PROCESS_FAST_SYS_MODEL);

        /** - Execute vehicle integration model code */
        VehicleFBSIMbuild_step(AG_COMMON_VEHICLE_FB_5MSRATE);

        /** - Begin 10 millisecond processing */
        {
            /** - decrement the counter */
            ulSlowModelProceesingCounter--;

            if ( 0u == ulSlowModelProceesingCounter )
            {
                /** - Prepare input buses for model execution */
                gvAG_InputRouter_Process(PROCESS_SYS_MODEL);

                /** - Execute vehicle integration model code */
                /** @todo Consider creating a seperate thread for each model subsystem */
                commonSIMbuild_step();
                SteeringSIMbuild_step();
                TractionSIMbuild_step();
                hydraulicSIMbuild_step();
                EnergySourceSIMbuild_step(AG_COMMON_ENERGY_SOURCE_10MSRATE);
                HmsSim_BrakeBuild_step();
                HmsSim_CommunicationBuild_step();
		        HmsSim_EdBuild_step();
		        HmsSim_GpdBuild_step();
		        HmsSim_HcmTcmBuild_step();
		        HmsSim_HydraulicBuild_step();
		        HmsSim_PowerSuppliesBuild_step();
		        HmsSim_SteeringBuild_step();
		        HmsSim_TractionBuild_step();
		        HmsSim_VcmInputsBuild_step();
                VehicleFBSIMbuild_step(AG_COMMON_VEHICLE_FB_10MSRATE);
                CalibrationSIMbuild_step();

                /** - Execute vehicle integration c-code */
                gvAG_SystemIntegration();

                /** - Process output buses after model execution */
                gvAG_OutputRouter_Process(PROCESS_SYS_MODEL);

                /** - Send PDOs for this thread */
                gvAG_Trigger_Periodic_PDOS(PROCESS_SYS_MODEL);

                /** - Report state of each model generated diagnostic */
                gvDEVREP_VCM_Model_Process(gubEV_Mgr);

                /** - detect direction changes of reach */
                gQUADAbsoluteDetectDirectionChange(&(gQuadratureAbsoluteObject[0]));

                /** - Store power-down data to FRAM */
                gvPD_WriteFram();

                /** - Reset the counter so the models only executes at the requested rate */
                ulSlowModelProceesingCounter = (AG_COMMON_VEHICLE_INTEGRATION_MODEL_RATE / AG_COMMON_THREAD_TICK_RATE);
            }

            /** - Begin 1 second processing */
            {
                /** - decrement the counter */
                ulEnergySourceCounter--;

                if ( 0u == ulEnergySourceCounter )
                {
                    /** - If the energy source counter is at zero, execute its step function */
                    EnergySourceSIMbuild_step(AG_COMMON_ENERGY_SOURCE_1SECRATE);

                    /** - Set flag showing that the Energy Source step function has been run at least once */
                    gfEnergySourceStepRun = TRUE;

                    /** - Reset the counter so the energy source model only executes at the requested rate */
                    ulEnergySourceCounter = (AG_COMMON_ENERGY_SOURCE_RATE / AG_COMMON_THREAD_TICK_RATE);
                }
            }

            /** - Send AMC PDOs */
            subAmcPdoTimerMs += AG_COMMON_THREAD_TICK_RATE;
            if ( subAmcPdoTimerMs >= gParameters.ulAmcPdoTxRateMs  )
            {
                gvAG_Trigger_Periodic_PDOS(PROCESS_AMC);
                subAmcPdoTimerMs = 0u;
            }

            /** - Process output buses after model execution */
            gvAG_OutputRouter_Process(PROCESS_FAST_SYS_MODEL);

            /** - Report execution to watchdog */
            gvWatchdog_ReportThreadExcecution(ubWatchdogID);

            /** - Check if this is the first pass through the loop */
            if ( TRUE == fFirstTimeThrough )
            {
                /** - Yes, clear flag */
                fFirstTimeThrough = FALSE;

                /** - Tell the watchdog to begin monitoring this thread */
                gvWatchdog_Begin(ubWatchdogID);
            }

            /** - XCP DAQ event */
            XcpEvent(3u);

            /** - Call end of thread instrumentation function */
            gvINST_Thread_End(&sINST_Thread_AG);

            /** - Suspend task until next time the scheduling timer expires */
            gulOS_ThreadSuspend(&AG_Common_Thread);
        }
    }
}

/**
 * @fn     os_thread_t *gpAG_Common_GetThreadPointer(void)
 *
 * @brief  This function returns a pointer to the common integration model thread control structure
 *
 * @return 32-bit address
 *
 * @author Walter Conley
 *
 * @note   N/A
 *
 */
os_thread_t *gpAG_Common_GetThreadPointer(void)
{
    /** ###Functional overview: */

    /** - Return address of the common integration model thread control structure */
    return(&AG_Common_Thread);
}

/**
 * @fn      void vAG_Common_Thread_TimerEntry(uint32_t ulInput)
 *
 * @brief   This function is called each time the scheduling timer expires
 *
 * @param   ulInput = Input value defined when the timer is created
 *
 * @return  N/A
 *
 * @author  Walter Conley
 *
 * @note    N/A
 *
 */
void vAG_Common_Thread_TimerEntry(uint32_t ulInput)
{
    /** ###Functional overview: */

    /** - Wake the thread up for the next execution cycle */
    gulOS_ThreadResume(&AG_Common_Thread);
}

