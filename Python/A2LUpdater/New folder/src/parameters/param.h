/**
 *  @file                   param.h
 *  @brief                  Interface for the parameter section in FLASH
 *  @copyright              2013-2014 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   06/19/2013
 *
 *  @remark Author:         Walter Conley
 *  @remark Project Tree:   C1515
 *
 */
#ifndef PARAM_H
#define PARAM_H 1

#include "IO_Thread.h"
#include "adc.h"
#include "ag_common.h"
#include "SystemsImport.h"
#include "ag_sys_integration.h"
#include "CO_Application.h"
#include "supv_interface.h"
#include "switch_interface.h"
#include "xint_interface.h"
#include "analog_interface.h"
#include "eTPU2_SPI_Thread.h"
#include "pwm.h"
#include "driver_input_router.h"
#include "AccumData.h"
#include "quadrature_absolute.h"
#include "INTC.h"
#include "io_thread.h"
#include "codrv_amc_001.h"
#include "codrv_iom0807_001.h"
#include "codrv_hdm_001.h"
#include "codrv_cec_001.h"
#include "Manifest_mgr.h"
#include "potentiometer.h"
#include "mscomm_monitor.h"
#include "4_20mA.h"
#include "gyro_driver.h"
#include "GPD5_Op_alrm.h"

/*========================================================================*
 *  SECTION - Global definitions
 *========================================================================*
 */

/** \defgroup FixedParameters Fixed Parameters
 *  @brief    Fixed parameters in dedicated parameter section in FLASH
 */

/** @struct  check_sequence
 *  @brief   Check sequence of the parameter sector in FLASH. It is composed of a 16 bit CRC and a 16 bit checksum.
 *
 *  @typedef check_sequence_t
 *  @brief   check_sequence type definition
 */
typedef struct check_sequence
{
    /*
    @@ SUB_GROUP = Parameters
    @@ DESCRIPTION = "Global Flash Parameter Objects"
    @@ END
    */
    /*
    @@ ELEMENT = uwCrc
    @@ STRUCTURE = check_sequence_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = UWORD
    @@ DESCRIPTION = "CRC of parameter sector."
    @@ END
    */
    uint16_t    uwCrc;          /**< CRC of parameter sector. */

    /*
    @@ ELEMENT = uwCks
    @@ STRUCTURE = check_sequence_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = UWORD
    @@ DESCRIPTION = "Checksum of parameter sector."
    @@ END
    */
    uint16_t    uwCks;          /**< Checksum of parameter sector. */

    /*
    @@ ELEMENT = ulSpare
    @@ STRUCTURE = check_sequence_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = ULONG
    @@ DESCRIPTION = "Spare bytes in the check sequence segment"
    @@ END
    */
    uint32_t    ulSpare;        /**< Spare bytes in the check sequence segment, the smallest program segment is 8-bytes for EEC FLASH */
}check_sequence_t;

/** @def   TRUCK_TYPE_PC
 *  @brief Definition for pallet truck: PC
 */
#define TRUCK_TYPE_PC   1u

/** @def   TRUCK_TYPE_GPC
 *  @brief Definition for pallet truck: GPC
 */
#define TRUCK_TYPE_GPC  2u

/** @def   TRUCK_TYPE_PE
 *  @brief Definition for pallet truck: PE
 */
#define TRUCK_TYPE_PE   3u

/** @def   TRUCK_TYPE_ESR_STD
 *  @brief Definition for reach truck: ESR_STD
 */
#define TRUCK_TYPE_ESR_STD   4u

/** @def   TRUCK_TYPE_RM_42
 *  @brief Definition for reach truck: RM 42
 */
#define TRUCK_TYPE_RM_42   5u

/** @def   TRUCK_TYPE_TML
 *  @brief Definition for TML truck parameters
 */
#define TRUCK_TYPE_TML   6u

/** @def   TRUCK_TYPE_ESR_REGEN
 *  @brief Definition for reach truck: ESR_REGEN
 */
#define TRUCK_TYPE_ESR_REGEN   7u

/** @def   TRUCK_TYPE_RM_48
 *  @brief Definition for reach truck: RM 48
 */
#define TRUCK_TYPE_RM_48   8u

/** @def   TRUCK_TYPE_RR_42
 *  @brief Definition for reach truck: RR 42
 */
#define TRUCK_TYPE_RR_42   9u

/** @def   TRUCK_TYPE_RR_48
 *  @brief Definition for reach truck: RR 48
 */
#define TRUCK_TYPE_RR_48   10u

/** @def   MODULE_TYPE_SMALL_PROTO1
 *  @brief Definition for small VCM, hardware level: Prototype 1
 */
#define MODULE_TYPE_SMALL_PROTO1   1u

/** @def   MODULE_TYPE_SMALL_DV
 *  @brief Definition for small VCM, hardware level: DV
 */
#define MODULE_TYPE_SMALL_DV   2u

/** @def   MODULE_TYPE_LARGE_PROTO1
 *  @brief Definition for large VCM, hardware level: Prototype 1
 */
#define MODULE_TYPE_LARGE_PROTO1   3u

/** @def   MODULE_TYPE_LARGE_DV
 *  @brief Definition for large VCM, hardware level: DV
 */
#define MODULE_TYPE_LARGE_DV   4u

/** @enum  vehicle_type
 *  @brief Enumeration of supported vehicle types.
 *
 *  @typedef vehicle_type_t
 *  @brief   vehicle_type type definition
 */
typedef enum vehicle_type
{
    TYPE_PC         = TRUCK_TYPE_PC,        /**< Pallet truck: PC */
    TYPE_GPC        = TRUCK_TYPE_GPC,       /**< Pallet truck: GPC */
    TYPE_PE         = TRUCK_TYPE_PE,        /**< Pallet truck: PE */
    TYPE_ESR_STD    = TRUCK_TYPE_ESR_STD,   /**< Reach truck: ESR STD*/
    TYPE_ESR_REGEN  = TRUCK_TYPE_ESR_REGEN, /**< Reach truck: ESR REGEN*/
    TYPE_RM_42      = TRUCK_TYPE_RM_42,     /**< Reach truck: RM 42 */
    TYPE_RM_48      = TRUCK_TYPE_RM_48,     /**< Reach truck: RM 48 */
    TYPE_RR_42      = TRUCK_TYPE_RR_42,     /**< Reach truck: RR 42 */
    TYPE_RR_48      = TRUCK_TYPE_RR_48,     /**< Reach truck: RR 48 */
    TYPE_TML        = TRUCK_TYPE_TML        /**< Unspecified truck: TML */
}vehicle_type_t;

/** @struct  info_params
 *  @brief   Info block of the parameter sector in FLASH.
 *
 *  @typedef info_params_t
 *  @brief   info_params type definition
 */
typedef struct info_params
{
    /*
    @@ SUB_STRUCTURE = CheckSequence
    @@ STRUCTURE = info_params_t
    @@ DATA_TYPE = STRUCTURE check_sequence_t
    @@ END
    */
    check_sequence_t     CheckSequence;     /**< Precomputed check sequence of parameter table */

    /*
    @@ ELEMENT = eVehicleType
    @@ STRUCTURE = info_params_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = ULONG
    @@ CONVERSION = TABLE 1 "TYPE_PC"    2 "TYPE_GPC" 3 "TYPE_PE" 4 "TYPE_ESR_STD"
    @@                    5 "TYPE_RM_42" 6 "TYPE_TML" 7 "TYPE_ESR_REGEN" 8 "TYPE_RM_48"
    @@ DESCRIPTION = "Type of present vehicle"
    @@ END
    */
    vehicle_type_t       eVehicleType;      /**< Type of present vehicle */

    /*
    @@ ELEMENT = ulRevision
    @@ STRUCTURE = info_params_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = ULONG
    @@ DESCRIPTION = "Parameter table revision number"
    @@ END
    */
    uint32_t             ulRevision;        /**< Parameter table revision number */

    /*
    @@ ELEMENT = ulParamsStartAddr
    @@ STRUCTURE = info_params_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = ULONG
    @@ DESCRIPTION = "Start address of parameter table in flash, needed for computing CRC."
    @@ END
    */
    uint32_t             ulParamsStartAddr; /**< Start address of parameter table in flash, needed for computing CRC. */

    /*
    @@ ELEMENT = ulParamsEndAddr
    @@ STRUCTURE = info_params_t
    @@ A2L_TYPE = PARAMETER
    @@ DATA_TYPE = ULONG
    @@ DESCRIPTION = "End address of parameter table in flash, needed for computing CRC."
    @@ END
    */
    uint32_t             ulParamsEndAddr;   /**< End address of parameter table in flash, needed for computing CRC. */
}info_params_t;

#pragma ghs startnomisra
/** @struct  fixed_params
 *  @brief   Fixed parameters that are downloaded from the IMM and stored in the parameter sector of FLASH.
 *
 *  @note    See "Variable Documentation" section for @ref gParameters in @ref param.c for initialization values.
 *
 *  @ingroup FixedParameters
 *
 *  @typedef fixed_params_t
 *  @brief   fixed_params type definition
 */
typedef struct fixed_params
{
    #include "param_truck.h"
    #include "param_diag.h"
    #include "param_model.h"
    #include "param_module.h"
    #include "param_general.h"
}fixed_params_t;
#pragma ghs endnomisra

/** @def   CRC_DUMMY
 *  @brief This is a dummy value. It will later be replaced by
 *         a computed CRC of the parameter sector in Flash.
 */
#define CRC_DUMMY 0xFFFFu

/** @def   CKS_DUMMY
 *  @brief This is a dummy value. It will later be replaced by
 *         a computed checksum of the parameter sector in Flash.
 */
#define CKS_DUMMY 0xFFFFu

/** @def   DEFAULT_VEHICLE_TYPE
 *  @brief This is the default value for the vehicle type in the
 *         parameter table.
 *  @note  Not used yet.
 */
#define DEFAULT_VEHICLE_TYPE (TYPE_PC)

/** @def   PARAMETER_REVISION
 *  @brief This is the revision number of the parameter table. Use to verify the application software is compatible with the parameter table
 */
#define PARAMETER_REVISION 1u

/** @def   PARAMS_START_ADDR_DEFAULT
 *  @brief Start address of the parameter sector in Flash. This
 *         is just a default value for the parameter table, not
 *         to be used by application.
 */
#define PARAMS_START_ADDR_DEFAULT 0x00040000u

/** @def   PARAMS_END_ADDR_DEFAULT
 *  @brief End address of the parameter sector in Flash. This
 *         is just a default value for the parameter table, not
 *         to be used by application.
 */
#define PARAMS_END_ADDR_DEFAULT 0x000BFFFFu

/** @def   TML_HW_TEST_ENABLE_VAL
 *  @brief Value used to indicate hardware test is enabled
 */
#define TML_HW_TEST_ENABLE_VAL 0xDEADBEEFu

/*========================================================================*
 *  SECTION - extern global variables (minimize global variable use)      *
 *========================================================================*
 */
extern const fixed_params_t gParameters;

/*========================================================================*
 *  SECTION - extern global functions                                     *
 *========================================================================*
 */

#endif  /* #ifndef PARAM_H */

