/**
 *  @file                   param_model.h
 *  @brief                  Defines the model portion of the parameter section in FLASH
 *  @copyright              2014 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   05/23/2014
 *
 *  @remark Author:         Chris Graunke
 *  @remark Project Tree:   C1515
 *
 */

#ifndef PARAM_MODEL_H
#define PARAM_MODEL_H 1

    /*
    @@ SUB_STRUCTURE = BUS_Autocalib
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE AutoCal_par_bus_t
    @@ END
    */
    AutoCal_par_bus_t                   BUS_Autocalib;                                          /**< Autocalibration bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_EvelFuse
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE E_Vel_Fuse_par_bus_t
    @@ END
    */
    E_Vel_Fuse_par_bus_t                BUS_EvelFuse;                                           /**< Velocity Fuse parameters bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_CutoutSwitch
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE Encode_cutoutswitch_bus_t
    @@ END
    */
    Encode_cutoutswitch_bus_t           BUS_CutoutSwitch;                                       /**< Cutout switch bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_EnergySourceParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE EnergySource_parameter_bus_t
    @@ END
    */
    EnergySource_parameter_bus_t        BUS_EnergySourceParameters;

    /*
    @@ SUB_STRUCTURE = BUS_ExpressLowerparam
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE Express_Lower_param_bus_t
    @@ END
    */
    Express_Lower_param_bus_t           BUS_ExpressLowerparam;

    /*
    @@ SUB_STRUCTURE = BUS_McoParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE MainCutOut_Setup_bus_t
    @@ END
    */
    MainCutOut_Setup_bus_t              BUS_McoParameters;                                      /**< Main cutout parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_QuickPickParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE QP_config_param_bus_t
    @@ END
    */
    QP_config_param_bus_t               BUS_QuickPickParameters;                                /**< QuickPick configuration parameters bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_ReachSlowDownSetup
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE ReachSlowDown_Setup_bus_t
    @@ END
    */
    ReachSlowDown_Setup_bus_t           BUS_ReachSlowDownSetup;                                 /**< Reach slow-down setup bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_RlCutConstants
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE RlCut_constants_bus_t
    @@ END
    */
    RlCut_constants_bus_t               BUS_RlCutConstants;                                     /**< Raise lower cutout constants bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_RlCutParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE RlCut_param_bus_t
    @@ END
    */
    RlCut_param_bus_t                   BUS_RlCutParameters;                                    /**< Raise lower cutout parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_SocParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE SOC_parameter_bus_t
    @@ END
    */
    SOC_parameter_bus_t                 BUS_SocParameters;                                      /**< SOC parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_TiltSlowDownSetup
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE TiltSlowDown_Setup_bus_t
    @@ END
    */
    TiltSlowDown_Setup_bus_t            BUS_TiltSlowDownSetup;                                  /**< Tilt slow-down parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_CutoutSetup
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE UserCutOuts_Type_bus_t
    @@ END
    */
    UserCutOuts_Type_bus_t              BUS_CutoutSetup;                                        /**< User cutout setup bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_VehicleFB_parameter
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE VehicleFB_parameter_bus_t
    @@ END
    */
    VehicleFB_parameter_bus_t              BUS_VehicleFB_parameter;                             /**< User vehicle FB bus used by systems integration model */


    /*
    @@ SUB_STRUCTURE = BUS_AdviseBitAssignment
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE advise_bit_assignment_bus_t
    @@ END
    */
    advise_bit_assignment_bus_t         BUS_AdviseBitAssignment;                                /**< Advise bit assignment bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_LogicParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE att_lgc_params_bus_t
    @@ END
    */
    att_lgc_params_bus_t                BUS_LogicParameters;                                    /**< Attended logic parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_SwitchParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE att_sw_params_bus_t
    @@ END
    */
    att_sw_params_bus_t                 BUS_SwitchParameters;                                   /**< Attended switch parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_BrakeCasterParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE brake_caster_parameter_bus_t
    @@ END
    */
    brake_caster_parameter_bus_t        BUS_BrakeCasterParameters;                              /**< Caster brake parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BrakeDX_Params
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE brake_dx_param_bus_t
    @@ END
    */
    brake_dx_param_bus_t                BrakeDX_Params;                                         /**< Brake DX parameters */

    /*
    @@ SUB_STRUCTURE = BUS_BrakeParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE brake_parameter_bus_t
    @@ END
    */
    brake_parameter_bus_t               BUS_BrakeParameters;                                    /**< Brake parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_BrakeLogicParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE brk_lgc_params_bus_t
    @@ END
    */
    brk_lgc_params_bus_t                BUS_BrakeLogicParameters;                               /**< Brake logic parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_BrakeSwitchParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE brk_sw_params_bus_t
    @@ END
    */
    brk_sw_params_bus_t                 BUS_BrakeSwitchParameters;                              /**< Brake switch parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_BvKeySwitchDxParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE bv_key_sw_dx_param_bus_t
    @@ END
    */
    bv_key_sw_dx_param_bus_t            BUS_BvKeySwitchDxParameters;                            /**< BV key switch diagnostic parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_CommonParameter
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE common_parameter_bus_t
    @@ END
    */
    common_parameter_bus_t              BUS_CommonParameter;                                    /**< Common parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_PowerDown_Current_GovenerParameter
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE current_governor_PowerDown_param_bus_t
    @@ END
    */
    current_governor_PowerDown_param_bus_t       BUS_PowerDown_Current_GovenerParameter;       /**< Power Down Current Govener parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_Current_GovenerParameter
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE current_governor_param_bus_t
    @@ END
    */
    current_governor_param_bus_t        BUS_Current_GovenerParameter;                          /**< Current Govener parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_EDParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE ed_param_bus_t
    @@ END
    */
    ed_param_bus_t                      BUS_EDParameters;                                       /**< ED parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_EngTest
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE eng_test_bus_t
    @@ END
    */
    eng_test_bus_t                      BUS_EngTest;                                            /**< Engineering test bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = Event_pars
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE event_pars_bus_t
    @@ END
    */
    event_pars_bus_t                    Event_pars;                                             /**< Event parameters */

    /*
    @@ SUB_STRUCTURE = BUS_Fan_Default
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE fan_param_bus_t
    @@ END
    */
    fan_param_bus_t                     BUS_Fan_Default;                                        /**< Default fan bus used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_Features_Default
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE features_bus_t
    @@ END
    */
    features_bus_t                      BUS_Features_Default;                                   /**< Default features bus used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_HydraulicsParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE hyd_parameter_bus_t
    @@ END
    */
    hyd_parameter_bus_t                 BUS_HydraulicsParameters;                               /**< Hydraulics parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_HydSystemDxParams
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE hyd_system_dx_params_bus_t
    @@ END
    */
    hyd_system_dx_params_bus_t          BUS_HydSystemDxParams;                                  /**< Hydraulics system dx parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_HydRunParams
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE hydrun_param_bus_t
    @@ END
    */
    hydrun_param_bus_t                  BUS_HydRunParams;                                       /**< Hydraulics run parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_HydRunSwCombos
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE hydrun_sw_combos_bus_t
    @@ END
    */
    hydrun_sw_combos_bus_t              BUS_HydRunSwCombos;                                     /**< Hydraulics run switch combinations bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_HydRunSwParams
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE hydrun_sw_param_bus_t
    @@ END
    */
    hydrun_sw_param_bus_t               BUS_HydRunSwParams;                                     /**< Hydraulics run switch parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_MastData
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE mast_data_bus_t
    @@ END
    */
    mast_data_bus_t                     BUS_MastData;                                           /**< Mast data bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_OpIntCfg
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE op_int_cfg_bus_t
    @@ END
    */
    op_int_cfg_bus_t                    BUS_OpIntCfg;                                           /**< Operator interface configurations used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_Performance_Default
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE performance_bus_t
    @@ END
    */
    performance_bus_t                   BUS_Performance_Default;                                /**< Default performance parameters used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_Sro1BitAssignment
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE sro1_bit_assignment_bus_t
    @@ END
    */
    sro1_bit_assignment_bus_t           BUS_Sro1BitAssignment;                                  /**< SRO 1 bit assignments bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_SroBitAssignment
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE sro_bit_assignment_bus_t
    @@ END
    */
    sro_bit_assignment_bus_t            BUS_SroBitAssignment;                                   /**< SRO bit assignments bus used by systems integration model */
    
    /*
    @@ SUB_STRUCTURE = BUS_SteerFaultParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE steer_fault_param_bus_t
    @@ END
    */
    steer_fault_param_bus_t             BUS_SteerFaultParameters;                             /**< Steering fault parameter bus used by systems integration model */
    
    /*
    @@ SUB_STRUCTURE = BUS_SteerParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE steer_param_bus_t
    @@ END
    */
    steer_param_bus_t                   BUS_SteerParameters;                                    /**< Steering parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_SteerPotDiagnosticParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE str_pot_diag_params_bus_t
    @@ END
    */
    str_pot_diag_params_bus_t           BUS_SteerPotDiagnosticParameters;                       /**< Steer pot diagnostics parameter bus defaults */

    /*
    @@ SUB_STRUCTURE = BUS_SteerspestdxParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE steer_sp_est_dx_param_bus_t
    @@ END
    */
    steer_sp_est_dx_param_bus_t         BUS_SteerspestdxParameters;

    /*
    @@ SUB_STRUCTURE = BUS_SteerSysDxParams
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE steer_sys_dx_param_bus_t
    @@ END
    */
    steer_sys_dx_param_bus_t            BUS_SteerSysDxParams;                                   /**< Steer system diagnostics parameter bus defaults */
    
    /*
    @@ SUB_STRUCTURE = BUS_SteerPotSignalDiagnosticParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE str_pot_signal_diag_params_bus_t
    @@ END
    */
    str_pot_signal_diag_params_bus_t    BUS_SteerPotSignalDiagnosticParameters;                 /**< Steer pot signal diagnostics parameter bus defaults */

    /*
    @@ SUB_STRUCTURE = BUS_SwDbParams
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE sw_db_param_bus_t
    @@ END
    */
    sw_db_param_bus_t                   BUS_SwDbParams;                                         /**< Switch debounce parameters used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_SwFlagParams
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE sw_flag_param_bus_t
    @@ END
    */
    sw_flag_param_bus_t                 BUS_SwFlagParams;                                       /**< Switch flag parameters used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_SwTestParams
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE sw_test_param_bus_t
    @@ END
    */
    sw_test_param_bus_t                 BUS_SwTestParams;                                       /**< Switch test parameters used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_SwTiedownCfg
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE sw_tiedown_cfg_bus_t
    @@ END
    */
    sw_tiedown_cfg_bus_t                BUS_SwTiedownCfg;                                       /**< Switch tiedown configurations used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_ThrottleParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE throttle_par_bus_t
    @@ END
    */
    throttle_par_bus_t                  BUS_ThrottleParameters;                                 /**< Deafult throttle parameter bus used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_TractionPerfTstDiagParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE tract_perf_tst_diag_params_bus_t
    @@ END
    */
    tract_perf_tst_diag_params_bus_t    BUS_TractionPerfTstDiagParameters;                      /**< Traction performance test diagnostic parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_TractionPerfTstParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE tract_perf_tst_params_bus_t
    @@ END
    */
    tract_perf_tst_params_bus_t         BUS_TractionPerfTstParameters;                          /**< Traction performance test parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_TractionParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE traction_parameter_bus_t
    @@ END
    */
    traction_parameter_bus_t            BUS_TractionParameters;                                 /**< Traction parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_TractionPerformance
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE traction_performance_bus_t
    @@ END
    */
    traction_performance_bus_t          BUS_TractionPerformance;                                /**< Traction performance parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_TractionTiedownParameters
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE trx_tiedown_cfg_bus_t
    @@ END
    */
    trx_tiedown_cfg_bus_t               BUS_TractionTiedownParameters;                          /**< Traction tiedown parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_TrxRunParams
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE trxrun_param_bus_t
    @@ END
    */
    trxrun_param_bus_t                  BUS_TrxRunParams;                                       /**< Traction run parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_TrxRunSwCombos
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE trxrun_sw_combos_bus_t
    @@ END
    */
    trxrun_sw_combos_bus_t              BUS_TrxRunSwCombos;                                     /**< Traction switch combinations bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_TrxRunSwParams
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE trxrun_sw_param_bus_t
    @@ END
    */
    trxrun_sw_param_bus_t               BUS_TrxRunSwParams;                                     /**< Traction switch parameter bus used by systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_Sensor_cals
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE sensor_cal_bus_t
    @@ END
    */
    sensor_cal_bus_t                    BUS_Sensor_cals;


    /*
    @@ SUB_STRUCTURE = BUS_ThrottleCalibration
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE throttle_calib_c2m_main_bus_t
    @@ END
    */
    throttle_calib_c2m_main_bus_t        BUS_ThrottleCalibration;                                /**< Default throttle calibration parameters used by the systems integration model */

    /*
    @@ SUB_STRUCTURE = BUS_ValveCals
    @@ STRUCTURE = fixed_params_t
    @@ DATA_TYPE = STRUCTURE valve_cal_bus_t
    @@ END
    */
    valve_cal_bus_t                     BUS_ValveCals;                                          /**< Valve calibration bus used by systems integration model */

#endif  /* #ifndef PARAM_MODEL_H */
