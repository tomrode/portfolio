/**
 *  @file                   param_module_large_DV.h
 *  @brief                  This file is used to populate the module specific portion of the parameter sector of internal FLASH for the large DV VCM
 *  @copyright              2014 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   05/23/2014
 *
 *  @remark Author:         Chris Graunke
 *  @remark Project Tree:   C1515
 *
 */

#ifndef PARAM_MODULE_SPECIFIC_H
#define PARAM_MODULE_SPECIFIC_H 1

    1000u,                                                  /* ulDemQueueChangeSetTimeout */
    3000u,                                                  /* ulDemAckSempahoreTimeout */
    132u,                                                   /* ulPin_Quadrature1A_GainSelect */
    133u,                                                   /* ulPin_Quadrature1B_GainSelect */
    141u,                                                   /* ulPin_PSP1_5V_12V_Shutdown */
    128u,                                                   /* ulPin_PSP1_5V_12V_VoltageSelect */
    INVALID_PIN_ASSIGNMENT,                                 /* ulGPD11_Reset_Pin */
    152u,                                                   /* ulBRAKE_OUTER_ENABLE_PIN  */
    153u,                                                   /* ulBRAKE_CASTER_ENABLE_PIN */
    154u,                                                   /* ulBRAKE_INNER_ENABLE_PIN  */
    143u,                                                   /* ulPin_PSP3_5V_12V_Enable */
    130u,                                                   /* ulPin_PSP3_5V_12V_VoltageSelect */
    0.8393665f,                                             /* rPotentiometer_Gain_5V */
    0.360086f,                                              /* rPotentiometer_Gain_12V */
    8u,                                                     /* ulNumPotUsed */
    {   /* ulPotentiometer_VoltageSelect_Pins[] */
        134u,                                               /* POT1 */
        136u,                                               /* POT3 */
        137u,                                               /* POT4 */
        441u,                                               /* POT5 */
        442u,                                               /* POT6 */
        443u,                                               /* POT7 */
        444u,                                               /* POT8 */
        445u                                                /* POT9 */
    },
    144u,                                                   /* ulPin_PSP4_5V_12V_Shutdown */
    131u,                                                   /* ulPin_PSP4_5V_12V_VoltageSelect */
    3u,                                                     /* ulNum_4_20mA_Used */
    {   /* ulPin_4_20mA_PrimaryFunctionSelect[] */
        135u,                                               /* 4-20 MA INPUT #1 */
        459u,                                               /* 4-20 MA INPUT #2 */
        461u                                                /* 4-20 MA INPUT #3 */
    },
    {   /* ulPin_4_20mA_VoltageSelect[] */
        138u,                                               /* 4-20 MA INPUT #1 */
        460u,                                               /* 4-20 MA INPUT #2 */
        462u                                                /* 4-20 MA INPUT #3 */
    },
    167u,                                                   /* ulWatchdogEnablePin */
    437u,                                                   /* ulWatchdogTicklePin */
    {
        /* ulIRQ_Number ulPAD_Number ulPriority ulVector              ulRisingEdge ulFallingEdge    XINT_Interrupt_Config[]                      */
        {  0u,          450u,        13u,       INTC_IRQ0_VECTOR,     1u,          1u },         /* XINT_Interrupt_Config[0]                     */
        {  1u,          451u,        13u,       INTC_IRQ1_VECTOR,     1u,          1u },         /* XINT_Interrupt_Config[1]                     */
        {  2u,          452u,        13u,       INTC_IRQ2_VECTOR,     1u,          1u },         /* XINT_Interrupt_Config[2]                     */
        {  3u,          453u,        13u,       INTC_IRQ3_VECTOR,     1u,          0u },         /* XINT_Interrupt_Config[3]                     */
        {  4u,          454u,        13u,       INTC_IRQ4_15_VECTOR,  0u,          0u },         /* XINT_Interrupt_Config[4]                     */
        {  5u,          455u,        13u,       INTC_IRQ4_15_VECTOR,  1u,          0u },         /* XINT_Interrupt_Config[5]                     */
        {  6u,          146u,        13u,       INTC_IRQ4_15_VECTOR,  0u,          1u }          /* XINT_Interrupt_Config[6]                     */
    },
    {   /* ulDrvIndex,   ulPin, eInitialState                              SDI_SW_GPIO_Config[]                                     */
        {  SDI_SW_DRV1,  448u,  GPIO_STATE_LOW },                       /* SDI_SW_GPIO_Config[0]                                    */
        {  SDI_SW_DRV2,  449u,  GPIO_STATE_LOW },                       /* SDI_SW_GPIO_Config[1]                                    */
        {  SDI_SW_DRV14, 447u,  GPIO_STATE_LOW },                       /* SDI_SW_GPIO_Config[2]                                    */
        {  SDI_SW_DRV15, 180u,  GPIO_STATE_LOW },                       /* SDI_SW_GPIO_Config[3]                                    */
        {  SDI_SW_DRV16, 179u,  GPIO_STATE_LOW }                        /* SDI_SW_GPIO_Config[4]                                    */
    },
    {   /* ulDrvIndex,   ulPin, eInitialState                              SDI_WC_GPIO_Config[]                                     */
        {  SDI_SW_DRV1,  115u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[0]                                    */
        {  SDI_SW_DRV2,  116u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[1]                                    */
        {  SDI_SW_DRV3,  117u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[2]                                    */
        {  SDI_SW_DRV4,  118u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[3]                                    */
        {  SDI_SW_DRV5,  119u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[4]                                    */
        {  SDI_SW_DRV6,  120u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[5]                                    */
        {  SDI_SW_DRV8,  122u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[6]                                    */
        {  SDI_SW_DRV10, 124u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[7]                                    */
        {  SDI_SW_DRV12, 126u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[8]                                    */
        {  SDI_SW_DRV13, 127u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[9]                                    */
        {  SDI_SW_DRV14, 114u,  GPIO_STATE_LOW },                       /* SDI_WC_GPIO_Config[10]                                   */
        {  SDI_SW_DRV38, 125u,  GPIO_STATE_LOW }                        /* SDI_WC_GPIO_Config[11]                                   */
    },
    12u,                                                        /* ulNumWcUsed          */
    0.5f,                                                       /* rDIGITAL_THRESHOLD   */
    {   /* ulNumChannelsForADCModule[] */
        79u,                                                    /* ADC_A0 */
        6u,                                                     /* ADC_A1 */
        47u,                                                    /* ADC_B0 */
        2u,                                                     /* ADC_B1 */
    },
    {   /* Analog_ChannelConfig[] */
        {   /* Analog_ChannelConfig[ADC_A0][] */
            { 0x00002600u, 40u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 0], ADCA - CH 38,  Buffer = 0,       +4.096V REF               */
            { 0x00002700u, 41u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 1], ADCA - CH 39,  Buffer = 0,       +0.372V REF               */
            { 0x00000300u, 5u,   { 0.047511f,   0.0f   } },             /* Analog_ChannelConfig[ 2], ADCA - CH 3,   Buffer = 0,       BATTERY_SENSE             */
            { 0x00088000u, 48u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 3], ADCA - CH 128, Buffer = 0,       Temperature Sensor        */
            { 0x00000000u, 0u,   { 0.683196f,   0.0f   } },             /* Analog_ChannelConfig[ 4], ADCA - CH 0,   Buffer = 0,       PS3_VM (5V range)         */
            { 0x00000000u, 104u, { 0.316969f,   0.0f   } },             /* Analog_ChannelConfig[ 5], ADCA - CH 0,   Buffer = 0,       PS3_VM (12V range)        */
            { 0x00000100u, 1u,   { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[ 6], ADCA - CH 1,   Buffer = 0,       POT1_AN (5V range)        */
            { 0x00000100u, 2u,   { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[ 7], ADCA - CH 1,   Buffer = 0,       POT1_AN (12V range)       */
            { 0x00000200u, 3u,   { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[ 8], ADCA - CH 2,   Buffer = 0,       POT3_AN (5V range)        */
            { 0x00000200u, 4u,   { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[ 9], ADCA - CH 2,   Buffer = 0,       POT3_AN (12V range)       */
            { 0x00000400u, 6u,   { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[10], ADCA - CH 4,   Buffer = 0,       POT4_AN (5V range)        */
            { 0x00000400u, 7u,   { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[11], ADCA - CH 4,   Buffer = 0,       POT4_AN (12V range)       */
            { 0x00000500u, 8u,   { 0.683196f,   0.0f   } },             /* Analog_ChannelConfig[12], ADCA - CH 5,   Buffer = 0,       PS1_VM_MAIN (5V range)    */
            { 0x00000500u, 105u, { 0.316969f,   0.0f   } },             /* Analog_ChannelConfig[13], ADCA - CH 5,   Buffer = 0,       PS1_VM_MAIN (12V range)   */
            { 0x00000600u, 9u,   { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[14], ADCA - CH 6,   Buffer = 0,       ENC1CHA_AN (5V range)     */
            { 0x00000600u, 10u,  { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[15], ADCA - CH 6,   Buffer = 0,       ENC1CHA_AN (12V range)    */
            { 0x00000700u, 11u,  { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[16], ADCA - CH 7,   Buffer = 0,       ENC1CHB_AN (5V range)     */
            { 0x00000700u, 12u,  { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[17], ADCA - CH 7,   Buffer = 0,       ENC1CHB_AN (12V range)    */
            { 0x00004000u, 13u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[18], ADCA - CH 64,  Buffer = 0,       SV1_VM                    */
            { 0x00004800u, 21u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[19], ADCA - CH 72,  Buffer = 0,       EDIN_VM                   */
            { 0x00005000u, 26u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[20], ADCA - CH 80,  Buffer = 0,       +BV_OUTER_VM              */
            { 0x00004100u, 110u, { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[21], ADCA - CH 65,  Buffer = 0,       SV2_VM                    */
            { 0x00004900u, 24u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[22], ADCA - CH 73,  Buffer = 0,       ED+_VM                    */
            { 0x00005100u, 30u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[23], ADCA - CH 81,  Buffer = 0,       +BV_INNER_VM              */
            { 0x00004200u, 15u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[24], ADCA - CH 66,  Buffer = 0,       SV3_VM                    */
            { 0x00004a00u, 23u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[25], ADCA - CH 74,  Buffer = 0,       ED-_VM                    */
            { 0x00005200u, 28u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[26], ADCA - CH 82,  Buffer = 0,       OUTER+_VM                 */
            { 0x00004300u, 19u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[27], ADCA - CH 67,  Buffer = 0,       SV4_VM                    */
            { 0x00004b00u, 25u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[28], ADCA - CH 75,  Buffer = 0,       SPARE (E3)                */
            { 0x00005300u, 32u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[29], ADCA - CH 83,  Buffer = 0,       OUTER-_VM                 */
            { 0x00004400u, 14u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[30], ADCA - CH 68,  Buffer = 0,       SV5_VM                    */
            { 0x00004c00u, 22u,  { 0.327485f,   0.0f   } },             /* Analog_ChannelConfig[31], ADCA - CH 76,  Buffer = 0,       PS2_COM_VM                */
            { 0x00005400u, 27u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[32], ADCA - CH 84,  Buffer = 0,       INNER+_VM                 */
            { 0x00004500u, 18u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[33], ADCA - CH 69,  Buffer = 0,       SV6_VM                    */
            { 0x00004d00u, 106u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[34], ADCA - CH 77,  Buffer = 0,       SPARE (E38)               */
            { 0x00005500u, 31u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[35], ADCA - CH 85,  Buffer = 0,       INNER-_VM                 */
            { 0x00004600u, 16u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[36], ADCA - CH 70,  Buffer = 0,       SV7_VM                    */
            { 0x00004e00u, 107u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[37], ADCA - CH 78,  Buffer = 0,       SPARE (E45)               */
            { 0x00005600u, 29u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[38], ADCA - CH 86,  Buffer = 0,       CASTER+_VM                */
            { 0x00004700u, 112u, { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[39], ADCA - CH 71,  Buffer = 0,       SV8_VM                    */
            { 0x00004F00u, 108u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[40], ADCA - CH 79,  Buffer = 0,       2.5VREF_VM                */
            { 0x00005700u, 33u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[41], ADCA - CH 87,  Buffer = 0,       CASTER-_VM                */
            { 0x00000B00u, 109u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[42], ADCA - CH 11,  Buffer = 0,       SPARE (E49)               */
            { 0x00000C00u, 34u,  { 0.327485f,   0.0f   } },             /* Analog_ChannelConfig[43], ADCA - CH 12,  Buffer = 0,       PS2_VM_MAIN               */
            { 0x00000D00u, 35u,  { 0.224022f,   0.0f   } },             /* Analog_ChannelConfig[44], ADCA - CH 13,  Buffer = 0,       PS4_VM                    */
            { 0x00000E00u, 17u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[45], ADCA - CH 14,  Buffer = 0,       KPM_VM                    */
            { 0x00000F00u, 111u, { 11.78947f,   0.279f } },             /* Analog_ChannelConfig[46], ADCA - CH 15,  Buffer = 0,       KPM_C                     */
            { 0x00001000u, 20u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[47], ADCA - CH 16,  Buffer = 0,       HORN_VM                   */
            { 0x00001100u, 113u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[48], ADCA - CH 17,  Buffer = 0,       SPARE (E50)               */
            { 0x00001200u, 36u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[49], ADCA - CH 18,  Buffer = 0,       SPARE (E11)               */
            { 0x00001300u, 37u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[50], ADCA - CH 19,  Buffer = 0,       SPARE (E14)               */
            { 0x00001400u, 38u,  { 0.203966f,   0.0f   } },             /* Analog_ChannelConfig[51], ADCA - CH 20,  Buffer = 0,       4_20MA#2 (mA)             */
            { 0x00001400u, 130u, { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[52], ADCB - CH 20,  Buffer = 0,       4_20MA#2 (5V range)       */
            { 0x00001400u, 131u, { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[53], ADCB - CH 20,  Buffer = 0,       4_20MA#2 (12V range)      */
            { 0x00001500u, 39u,  { 0.203966f,   0.0f   } },             /* Analog_ChannelConfig[54], ADCA - CH 21,  Buffer = 0,       4_20MA#3 (mA)             */
            { 0x00001500u, 132u, { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[55], ADCB - CH 21,  Buffer = 0,       4_20MA#3 (5V range)       */
            { 0x00001500u, 133u, { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[56], ADCB - CH 21,  Buffer = 0,       4_20MA#3 (12V range)      */
            { 0x00001600u, 114u, { 0.683196f,   0.0f   } },             /* Analog_ChannelConfig[57], ADCA - CH 22,  Buffer = 0,       PS5_VM (5V scale)         */
            { 0x00001600u, 123u, { 0.316969f,   0.0f   } },             /* Analog_ChannelConfig[58], ADCA - CH 22,  Buffer = 0,       PS5_VM (12V scale)        */
            { 0x00001700u, 115u, { 0.683196f,   0.0f   } },             /* Analog_ChannelConfig[59], ADCA - CH 23,  Buffer = 0,       PS6_VM (5V scale)         */
            { 0x00001700u, 124u, { 0.316969f,   0.0f   } },             /* Analog_ChannelConfig[60], ADCA - CH 23,  Buffer = 0,       PS6_VM (12V scale)        */
            { 0x00082800u, 42u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[61], ADCA - CH 40,  Buffer = 0,       VRH                       */
            { 0x00082900u, 43u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[62], ADCA - CH 41,  Buffer = 0,       VRL                       */
            { 0x00082A00u, 44u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[63], ADCA - CH 42,  Buffer = 0,       50% (VRH-VRL)             */
            { 0x00082B00u, 45u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[64], ADCA - CH 43,  Buffer = 0,       75% (VRH-VRL)             */
            { 0x00082C00u, 46u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[65], ADCA - CH 44,  Buffer = 0,       25% (VRH-VRL)             */
            { 0x00089100u, 49u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[66], ADCA - CH 145, Buffer = 0,       PMC Band Gap 0.62V        */
            { 0x00089200u, 50u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[67], ADCA - CH 146, Buffer = 0,       VDD                       */
            { 0x00089300u, 51u,  { 0.488998f,   0.0f   } },             /* Analog_ChannelConfig[68], ADCA - CH 147, Buffer = 0,       Internal 1.2V Regulator   */
            { 0x0008A200u, 52u,  { 0.5f,        0.0f   } },             /* Analog_ChannelConfig[69], ADCA - CH 162, Buffer = 0,       VDDEH1 50%                */
            { 0x0008A300u, 53u,  { 0.5f,        0.0f   } },             /* Analog_ChannelConfig[70], ADCA - CH 163, Buffer = 0,       VDDEH3 50%                */
            { 0x0008A400u, 54u,  { 0.5f,        0.0f   } },             /* Analog_ChannelConfig[71], ADCA - CH 164, Buffer = 0,       VDDEH4 50%                */
            { 0x0008A500u, 55u,  { 0.5f,        0.0f   } },             /* Analog_ChannelConfig[72], ADCA - CH 165, Buffer = 0,       VDDEH5 50%                */
            { 0x0008A600u, 56u,  { 0.5f,        0.0f   } },             /* Analog_ChannelConfig[73], ADCA - CH 166, Buffer = 0,       VDDEH6 50%                */
            { 0x0008A700u, 57u,  { 0.5f,        0.0f   } },             /* Analog_ChannelConfig[74], ADCA - CH 167, Buffer = 0,       VDDEH7 50%                */
            { 0x0008b400u, 58u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[75], ADCA - CH 180, Buffer = 0,       1.2V LVD VDD              */
            { 0x0008b500u, 59u,  { 0.18315f,    0.0f   } },             /* Analog_ChannelConfig[76], ADCA - CH 181, Buffer = 0,       3.3V Internal Regulator   */
            { 0x0008b600u, 60u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[77], ADCA - CH 182, Buffer = 0,       3.3V LVD VDDSYN           */
            { 0x8008b700u, 61u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[78], ADCA - CH 183, Buffer = 0 + EOQ, 5.0V LVD VDDREG           */
        },
        {   /* Analog_ChannelConfig[ADC_A1][] */
            { 0x00182600u, 65u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 0], ADCA - CH 38,  Buffer = 1,       +4.096V REF               */
            { 0x00182700u, 66u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 1], ADCA - CH 39,  Buffer = 1,       +0.372V REF               */
            { 0x00182d00u, 47u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 2], ADCA - CH 45,  Buffer = 1,       Local Band Gap 1220 mV    */
            { 0x0018C200u, 62u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 3], ADCA - CH 194, Buffer = 1,       Standby Regulator Out     */
            { 0x0018C300u, 63u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 4], ADCA - CH 195, Buffer = 1,       Standby Source Bias       */
            { 0x8018C400u, 64u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 5], ADCA - CH 196, Buffer = 1 + EOQ, 4.75V LVD VDDA            */
        },
        {   /* Analog_ChannelConfig[ADC_B0][] */
            { 0x00002600u, 119u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 0], ADCA - CH 38,  Buffer = 0,       +4.096V REF               */
            { 0x00002700u, 120u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 1], ADCA - CH 39,  Buffer = 0,       +0.372V REF               */
            { 0x00000000u, 67u,  { 0.203966f,   0.0f   } },             /* Analog_ChannelConfig[ 2], ADCB - CH 0,   Buffer = 0,       4_20MA (mA)               */
            { 0x00000000u, 68u,  { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[ 3], ADCB - CH 0,   Buffer = 0,       4_20MA (5V range)         */
            { 0x00000000u, 69u,  { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[ 4], ADCB - CH 0,   Buffer = 0,       4_20MA (12V range)        */
            { 0x00000100u, 70u,  { 3.0303f,     0.0f   } },             /* Analog_ChannelConfig[ 5], ADCB - CH 1,   Buffer = 0,       C_BRK_HS_CS               */
            { 0x00000200u, 71u,  { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[ 6], ADCB - CH 2,   Buffer = 0,       +BV_KEY                   */
            { 0x00000300u, 116u, { 0.047414f,   0.0f   } },             /* Analog_ChannelConfig[ 7], ADCB - CH 3,   Buffer = 0,       GPD1_VM                   */
            { 0x00000400u, 72u,  { 1.178947f,   0.22f  } },             /* Analog_ChannelConfig[ 8], ADCB - CH 4,   Buffer = 0,       ED- Current               */
            { 0x00000500u, 73u,  { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[ 9], ADCB - CH 5,   Buffer = 0,       POT5_AN (5V range)        */
            { 0x00000500u, 125u, { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[10], ADCB - CH 5,   Buffer = 0,       POT5_AN (12V range)       */
            { 0x00000600u, 75u,  { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[11], ADCB - CH 6,   Buffer = 0,       POT6_AN (5V range)        */
            { 0x00000600u, 126u, { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[12], ADCB - CH 6,   Buffer = 0,       POT6_AN (12V range)       */
            { 0x00000700u, 77u,  { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[13], ADCB - CH 7,   Buffer = 0,       POT7_AN (5V range)        */
            { 0x00000700u, 127u, { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[14], ADCB - CH 7,   Buffer = 0,       POT7_AN (12V range)       */
            { 0x00000800u, 74u,  { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[15], ADCB - CH 8,   Buffer = 0,       POT8_AN (5V range)        */
            { 0x00000800u, 128u, { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[16], ADCB - CH 8,   Buffer = 0,       POT8_AN (12V range)       */
            { 0x00000900u, 76u,  { 0.839367f,   0.0f   } },             /* Analog_ChannelConfig[17], ADCB - CH 9,   Buffer = 0,       POT9_AN (5V range)        */
            { 0x00000900u, 129u, { 0.360086f,   0.0f   } },             /* Analog_ChannelConfig[18], ADCB - CH 9,   Buffer = 0,       POT9_AN (12V range)       */
            { 0x00000A00u, 78u,  { 0.327485f,   0.0f   } },             /* Analog_ChannelConfig[19], ADCB - CH 10,  Buffer = 0,       PS5_COM_VM                */
            { 0x00000B00u, 79u,  { 0.222649f,   0.0f   } },             /* Analog_ChannelConfig[20], ADCB - CH 11,  Buffer = 0,       VSTBY_5V                  */
            { 0x00000C00u, 80u,  { 0.327485f,   0.0f   } },             /* Analog_ChannelConfig[21], ADCB - CH 12,  Buffer = 0,       PS1_COM_VM                */
            { 0x00000D00u, 81u,  { 0.327485f,   0.0f   } },             /* Analog_ChannelConfig[22], ADCB - CH 13,  Buffer = 0,       PS3_COM_VM                */
            { 0x00000E00u, 82u,  { 0.324785f,   0.0f   } },             /* Analog_ChannelConfig[23], ADCB - CH 14,  Buffer = 0,       SWITCH_GND_VM             */
            { 0x00000F00u, 83u,  { 0.224022f,   0.0f   } },             /* Analog_ChannelConfig[24], ADCB - CH 15,  Buffer = 0,       +15VL_VM                  */
            { 0x00001000u, 84u,  { 0.5f,        0.0f   } },             /* Analog_ChannelConfig[25], ADCB - CH 16,  Buffer = 0,       +5VL_VM                   */
            { 0x00001100u, 117u, { 0.327485f,   0.0f   } },             /* Analog_ChannelConfig[26], ADCB - CH 17,  Buffer = 0,       PS6_COM_VM                */
            { 0x00001200u, 85u,  { 0.29573f,    0.0f   } },             /* Analog_ChannelConfig[27], ADCB - CH 18,  Buffer = 0,       CANA_VM                   */
            { 0x00001300u, 86u,  { 0.29573f,    0.0f   } },             /* Analog_ChannelConfig[28], ADCB - CH 19,  Buffer = 0,       CANB_VM                   */
            { 0x00001400u, 87u,  { 0.29573f,    0.0f   } },             /* Analog_ChannelConfig[29], ADCB - CH 20,  Buffer = 0,       CANC_VM                   */
            { 0x00001500u, 88u,  { 0.29573f,    0.0f   } },             /* Analog_ChannelConfig[30], ADCB - CH 21,  Buffer = 0,       CAND_VM                   */
            { 0x00001600u, 89u,  { 0.29573f,    0.0f   } },             /* Analog_ChannelConfig[31], ADCB - CH 22,  Buffer = 0,       422RX_VM                  */
            { 0x00001700u, 90u,  { 0.29573f,    0.0f   } },             /* Analog_ChannelConfig[32], ADCB - CH 23,  Buffer = 0,       422TX_VM                  */
            { 0x00001800u, 91u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[33], ADCB - CH 24,  Buffer = 0,       Switch1                   */
            { 0x00001900u, 92u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[34], ADCB - CH 25,  Buffer = 0,       Switch2                   */
            { 0x00001A00u, 93u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[35], ADCB - CH 26,  Buffer = 0,       Switch3                   */
            { 0x00001B00u, 94u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[36], ADCB - CH 27,  Buffer = 0,       Switch4                   */
            { 0x00001C00u, 95u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[37], ADCB - CH 28,  Buffer = 0,       Switch5                   */
            { 0x00001D00u, 96u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[38], ADCB - CH 29,  Buffer = 0,       Switch6                   */
            { 0x00001E00u, 97u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[39], ADCB - CH 30,  Buffer = 0,       SPARE (E45)               */
            { 0x00001F00u, 98u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[40], ADCB - CH 31,  Buffer = 0,       Switch8                   */
            { 0x00002000u, 99u,  { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[41], ADCB - CH 32,  Buffer = 0,       SPARE (E46)               */
            { 0x00002100u, 100u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[42], ADCB - CH 33,  Buffer = 0,       Switch10                  */
            { 0x00002200u, 101u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[43], ADCB - CH 34,  Buffer = 0,       SPARE (E47)               */
            { 0x00002300u, 102u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[44], ADCB - CH 35,  Buffer = 0,       Switch12                  */
            { 0x00002400u, 103u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[45], ADCB - CH 36,  Buffer = 0,       Switch13                  */
            { 0x80002500u, 118u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[46], ADCB - CH 37,  Buffer = 0 + EOQ, Switch14                  */
        },
        {   /* Analog_ChannelConfig[ADC_B1][] */
            { 0x00102600u, 121u, { 1.0f,        0.0f   } },             /* Analog_ChannelConfig[ 0], ADCA - CH 38,  Buffer = 1,       +4.096V REF               */
            { 0x80102700u, 122u, { 1.0f,        0.0f   } }              /* Analog_ChannelConfig[ 1], ADCA - CH 39,  Buffer = 1 + EOQ, +0.372V REF               */
        }
    },
    {                                                         /* Analog_CcConfig         */
        13422u,                                               /* ulRef1Ideal             */
        1219u,                                                /* ulRef2Ideal             */
        {ADC_A0, 0u},                                         /* ADCA0Ref1Channel        */
        {ADC_A0, 1u},                                         /* ADCA0Ref2Channel        */
        {ADC_A1, 0u},                                         /* ADCA1Ref1Channel        */
        {ADC_A1, 1u},                                         /* ADCA1Ref2Channel        */
        {ADC_B0, 0u},                                         /* ADCB0Ref1Channel        */
        {ADC_B0, 1u},                                         /* ADCB0Ref2Channel        */
        {ADC_B1, 0u},                                         /* ADCB1Ref1Channel        */
        {ADC_B1, 1u},                                         /* ADCB1Ref2Channel        */
        (4.096f/13422.0f),                                    /* rMultiplier             */
        16u                                                   /* ulNumRefSamples         */
    },
    6u,                                                       /* ulAdcCalDataRevision */
    {ADC_A0, 2u},                                             /* AII_BatteryVoltage */
    {ADC_A0, 3u},                                             /* TempSenseAdcChannel */
    {ADC_A1, 2u},                                             /* BandgapAdcChannel */
    {   /* fram_accel_cal_data_t AccelerometerCalDefaults */
        0u,                                                   /* ulRevision */
        {   /* accel_cals_t OrientationData */
            {   /* ACCEL_X_ORIENTATION */
                {16000.0f, 0.0f},                             /* XAxisScaleFactor */
                {16000.0f, 0.0f},                             /* YAxisScaleFactor */
                {16000.0f, 0.0f}                              /* ZAxisScaleFactor */
            },
            {   /* ACCEL_Y_ORIENTATION */
                {16000.0f, 0.0f},                             /* XAxisScaleFactor */
                {16000.0f, 0.0f},                             /* YAxisScaleFactor */
                {16000.0f, 0.0f}                              /* ZAxisScaleFactor */
            },
            {   /* ACCEL_Z_ORIENTATION */
                {16000.0f, 0.0f},                             /* XAxisScaleFactor */
                {16000.0f, 0.0f},                             /* YAxisScaleFactor */
                {16000.0f, 0.0f}                              /* ZAxisScaleFactor */
            }
        }
    },
    8000u,                                                    /* ulPWM_CountsPerPeriod */
    147u,                                                     /* ulFault_LED_Pin */
    {                                                         /* GPD11_pwm_params */
        183u,                                                 /* ulOutputPin */
        4u,                                                   /* ulOutputChannel */
        8000u,                                                /* ulPeriodCount */
        1u,                                                   /* ulOutputDisabling */
        1u,                                                   /* ulDisableLevel */
        187u,                                                 /* ulDisablePin     */
        8u,                                                   /* ulDisableChannel */
        0u,                                                   /* ulDisableFLAG */
        0u                                                    /* ulRisingEdge */
    },
    {                                                         /* GPD18_pwm_params */
        184u,                                                 /* ulOutputPin */
        5u,                                                   /* ulOutputChannel */
        8000u,                                                /* ulPeriodCount */
        1u,                                                   /* ulOutputDisabling */
        1u,                                                   /* ulDisableLevel */
        188u,                                                 /* ulDisablePin     */
        9u,                                                   /* ulDisableChannel */
        0u,                                                   /* ulDisableFLAG */
        0u                                                    /* ulRisingEdge */
    },
    {                                                         /* GPD17_pwm_params */
        185u,                                                 /* ulOutputPin */
        6u,                                                   /* ulOutputChannel */
        8000u,                                                /* ulPeriodCount */
        1u,                                                   /* ulOutputDisabling */
        1u,                                                   /* ulDisableLevel */
        189u,                                                 /* ulDisablePin     */
        10u,                                                  /* ulDisableChannel */
        0u,                                                   /* ulDisableFLAG */
        0u                                                    /* ulRisingEdge */
    },
    {                                                         /* GPD19_pwm_params */
        186u,                                                 /* ulOutputPin */
        7u,                                                   /* ulOutputChannel */
        8000u,                                                /* ulPeriodCount */
        1u,                                                   /* ulOutputDisabling */
        1u,                                                   /* ulDisableLevel */
        190u,                                                 /* ulDisablePin     */
        11u,                                                  /* ulDisableChannel */
        0u,                                                   /* ulDisableFLAG */
        0u                                                    /* ulRisingEdge */
    },
    {                                                         /* QuadratureAbsoluteIOConfig[0] */
        434u,                                                 /* ulEMIOSPin_A */
        28u,                                                  /* ulEMIOSChannel_A */
        INTC_EMIOS28_VECTOR,                                  /* ulEMIOSVector_A */
        435u,                                                 /* ulEMIOSPin_B */
        29u,                                                  /* ulEMIOSChannel_B */
        INTC_EMIOS29_VECTOR                                   /* ulEMIOSVector_B */
    },
    {                                                            /* SUPV_io_xfer_list                                                                     */
       { 18u  },                                                 /* uwNumberOfEntries                                                                     */
       {                                                         /* Supv_config_objects                                                                   */
           { 0x3003u, 0u, 1u },                                  /* Supv_config_objects[ 0] - Supervisor pot configuration object(s)                      */
           { 0x3004u, 0u, 1u },                                  /* Supv_config_objects[ 1] - Supervisor power suppply #2 configuration object(s)         */
           { 0x3030u, 0u, 1u },                                  /* Supv_config_objects[ 2] - Supervisor GP1 configuration object(s)                      */
           { 0x3031u, 0u, 1u },                                  /* Supv_config_objects[ 3] - Supervisor GP2 configuration object(s)                      */
           { 0x3032u, 0u, 1u },                                  /* Supv_config_objects[ 4] - Supervisor GP3 configuration object(s)                      */
           { 0x3033u, 0u, 1u },                                  /* Supv_config_objects[ 5] - Supervisor GP4 configuration object(s)                      */
           { 0x3034u, 0u, 1u },                                  /* Supv_config_objects[ 6] - Supervisor GP5 configuration object(s)                      */
           { 0x3035u, 0u, 1u },                                  /* Supv_config_objects[ 7] - Supervisor GP6 configuration object(s)                      */
           { 0x3036u, 0u, 1u },                                  /* Supv_config_objects[ 8] - Supervisor GP7 configuration object(s)                      */
           { 0x3037u, 0u, 1u },                                  /* Supv_config_objects[ 9] - Supervisor GP8 configuration object(s)                      */
           { 0x3038u, 0u, 1u },                                  /* Supv_config_objects[10] - Supervisor GP9 configuration object(s)                      */
           { 0x3039u, 0u, 1u },                                  /* Supv_config_objects[11] - Supervisor GP10 configuration object(s)                     */
           { 0x303Au, 0u, 1u },                                  /* Supv_config_objects[12] - Supervisor GP11 configuration object(s)                     */
           { 0x303Bu, 0u, 1u },                                  /* Supv_config_objects[13] - Supervisor GP12 configuration object(s)                     */
           { 0x304Au, 0u, 2u },                                  /* Supv_config_objects[14] - Supervisor XCP configuration object(s)                      */
           { 0x3009u, 0u, 5u },                                  /* Supv_config_objects[15] - Supervisor Features-enabled bitfield                        */
           { 0x3082u, 0u, 26u},                                  /* Supv_config_objects[16] - Steer parameters                                            */
           { 0x3040u, 0u, 0u }                                   /* Supv_config_objects[17] - Last send to trigger I/O reconfiguration                    */
       }
    },
    {                                                                          /* IO_MatrixConfiguration */
        {                                                                      /* IO_MatrixConfiguration.FixedScaleFactors[] */
            { 1.0f,                                                   0.0f },  /* Use @ref IO_SF_ARRAY_00 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f/255.0f,                                            0.0f },  /* Use @ref IO_SF_ARRAY_01 in @ref gpAG_ScaleFactorArray[] to access this scale factor; used for normalization of 8 bit converted analog input in backrest PDO  */
            { 1.0f,                                                   0.0f },  /* Use @ref IO_SF_ARRAY_02 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref IO_SF_ARRAY_03 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref IO_SF_ARRAY_04 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
        },
        {                                                               /* IO_MatrixConfiguration.HardwareInputConfig[]             */
            { SB_UINT32,  SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_IN_NOT_USED_LOW                                       */
            { SB_UINT32,  SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_IN_NOT_USED_HIGH                                      */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS91                           */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS92                           */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS93                           */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS94                           */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS95                           */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS96                           */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS97                           */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS98                           */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS99                           */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS100                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS101                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS102                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS103                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_ANALOG_INPUT_SCALED_UNITS118                          */
            { SB_UINT8,   SB_RIGHT,      0x01u, 0u, IO_SF_ARRAY_00 },   /* IO_TCM_SWITCH1                                           */
            { SB_UINT8,   SB_RIGHT,      0x02u, 1u, IO_SF_ARRAY_00 },   /* IO_TCM_SWITCH2                                           */
            { SB_UINT8,   SB_RIGHT,      0x04u, 2u, IO_SF_ARRAY_00 },   /* IO_TCM_SWITCH3                                           */
            { SB_UINT8,   SB_RIGHT,      0x08u, 3u, IO_SF_ARRAY_00 },   /* IO_TCM_SWITCH4                                           */
            { SB_UINT8,   SB_RIGHT,      0x10u, 4u, IO_SF_ARRAY_00 },   /* IO_TCM_SWITCH5                                           */
            { SB_UINT8,   SB_RIGHT,      0x20u, 5u, IO_SF_ARRAY_00 },   /* IO_TCM_SWITCH6                                           */
            { SB_UINT8,   SB_RIGHT,      0x40u, 6u, IO_SF_ARRAY_00 },   /* IO_TCM_SWITCH7                                           */
            { SB_UINT8,   SB_RIGHT,      0x80u, 7u, IO_SF_ARRAY_00 },   /* IO_TCM_SWITCH8                                           */
            { SB_INT32,   SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SWITCH_DIGIN1                                         */
            { SB_INT32,   SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SWITCH_DIGIN2                                         */
            { SB_INT32,   SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SWITCH_DIGIN3                                         */
            { SB_INT32,   SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SWITCH_DIGIN4                                         */
            { SB_INT32,   SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SWITCH_DIGIN5                                         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SUPV_SWITCH1                                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SUPV_SWITCH2                                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SUPV_SWITCH3                                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SUPV_SWITCH4                                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SUPV_SWITCH5                                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SUPV_SWITCH6                                          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_SUPV_SWITCH7                                          */
            { SB_UINT8,   SB_RIGHT,      0x01u, 0u, IO_SF_ARRAY_00 },   /* IO_IN_IOM0807_1_SWITCH1                                  */
            { SB_UINT8,   SB_RIGHT,      0x02u, 1u, IO_SF_ARRAY_00 },   /* IO_IN_IOM0807_1_SWITCH2                                  */
            { SB_UINT8,   SB_RIGHT,      0x04u, 2u, IO_SF_ARRAY_00 },   /* IO_IN_IOM0807_1_SWITCH3                                  */
            { SB_UINT8,   SB_RIGHT,      0x08u, 3u, IO_SF_ARRAY_00 },   /* IO_IN_IOM0807_1_SWITCH4                                  */
            { SB_UINT8,   SB_RIGHT,      0x10u, 4u, IO_SF_ARRAY_00 },   /* IO_IN_IOM0807_1_SWITCH5                                  */
            { SB_UINT8,   SB_RIGHT,      0x20u, 5u, IO_SF_ARRAY_00 },   /* IO_IN_IOM0807_1_SWITCH6                                  */
            { SB_UINT8,   SB_RIGHT,      0x01u, 0u, IO_SF_ARRAY_00 },   /* IO_HCM_SWITCH1                                           */
            { SB_UINT8,   SB_RIGHT,      0x02u, 1u, IO_SF_ARRAY_00 },   /* IO_HCM_SWITCH2                                           */
            { SB_UINT8,   SB_RIGHT,      0x04u, 2u, IO_SF_ARRAY_00 },   /* IO_HCM_SWITCH3                                           */
            { SB_UINT8,   SB_RIGHT,      0x08u, 3u, IO_SF_ARRAY_00 },   /* IO_HCM_SWITCH4                                           */
            { SB_UINT8,   SB_RIGHT,      0x10u, 4u, IO_SF_ARRAY_00 },   /* IO_HCM_SWITCH5                                           */
            { SB_UINT8,   SB_RIGHT,      0x20u, 5u, IO_SF_ARRAY_00 },   /* IO_HCM_SWITCH6                                           */
            { SB_UINT8,   SB_RIGHT,      0x40u, 6u, IO_SF_ARRAY_00 },   /* IO_HCM_SWITCH7                                           */
            { SB_UINT8,   SB_RIGHT,      0x80u, 7u, IO_SF_ARRAY_00 },   /* IO_HCM_SWITCH8                                           */
            { SB_UINT8,   SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 },   /* IO_IN_ZP_CAN_INTERFACE_ANALOGIN3                         */
            { SB_UINT8,   SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00 }    /* IO_IN_ZP_CAN_INTERFACE_ANALOGIN4                         */
        },
        {                                                                   /* IO_MatrixConfiguration.DriverInputConfig[]               */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV1].Control.rSwitchDriverInput          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV2].Control.rSwitchDriverInput          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV3].Control.rSwitchDriverInput          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV4].Control.rSwitchDriverInput          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV5].Control.rSwitchDriverInput          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV6].Control.rSwitchDriverInput          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV7].Control.rSwitchDriverInput          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV8].Control.rSwitchDriverInput          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV9].Control.rSwitchDriverInput          */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV10].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV11].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV12].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_SWI_VLT_COMP },   /* gSwitch[SDI_SW_DRV13].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV14].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV15].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV16].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV17].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV18].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV19].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV20].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV21].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV22].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV23].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV24].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV25].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV26].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV27].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV28].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV29].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV30].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV31].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV32].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV33].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV34].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV35].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_01     },   /* gSwitch[SDI_SW_DRV36].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_01     },   /* gSwitch[SDI_SW_DRV37].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     },   /* gSwitch[SDI_SW_DRV38].Control.rSwitchDriverInput         */
            { SB_FLOAT32, SB_NO_BIT_MAN, 0u,    0u, IO_SF_ARRAY_00     }    /* gSwitch[SDI_SW_DRV39].Control.rSwitchDriverInput         */
        },
        {                                                               /* IO_MatrixConfiguration.InputRouting[]                    */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS91    },   /* gSwitch[SDI_SW_DRV1].Control.rSwitchDriverInput          */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS92    },   /* gSwitch[SDI_SW_DRV2].Control.rSwitchDriverInput          */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS93    },   /* gSwitch[SDI_SW_DRV3].Control.rSwitchDriverInput          */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS94    },   /* gSwitch[SDI_SW_DRV4].Control.rSwitchDriverInput          */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS95    },   /* gSwitch[SDI_SW_DRV5].Control.rSwitchDriverInput          */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS96    },   /* gSwitch[SDI_SW_DRV6].Control.rSwitchDriverInput          */
            { PROCESS_SW_DRV,    IO_SUPV_SWITCH4                   },   /* gSwitch[SDI_SW_DRV7].Control.rSwitchDriverInput          */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS98    },   /* gSwitch[SDI_SW_DRV8].Control.rSwitchDriverInput          */
            { PROCESS_SW_DRV,    IO_SUPV_SWITCH5                   },   /* gSwitch[SDI_SW_DRV9].Control.rSwitchDriverInput          */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS100   },   /* gSwitch[SDI_SW_DRV10].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_SUPV_SWITCH6                   },   /* gSwitch[SDI_SW_DRV11].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS102   },   /* gSwitch[SDI_SW_DRV12].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS103   },   /* gSwitch[SDI_SW_DRV13].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_SWITCH_DIGIN3                  },   /* gSwitch[SDI_SW_DRV14].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_SWITCH_DIGIN4                  },   /* gSwitch[SDI_SW_DRV15].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_SWITCH_DIGIN5                  },   /* gSwitch[SDI_SW_DRV16].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_SUPV_SWITCH1                   },   /* gSwitch[SDI_SW_DRV17].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_SUPV_SWITCH2                   },   /* gSwitch[SDI_SW_DRV18].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_SUPV_SWITCH3                   },   /* gSwitch[SDI_SW_DRV19].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_TCM_SWITCH1                    },   /* gSwitch[SDI_SW_DRV20].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_TCM_SWITCH2                    },   /* gSwitch[SDI_SW_DRV21].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_TCM_SWITCH3                    },   /* gSwitch[SDI_SW_DRV22].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_TCM_SWITCH4                    },   /* gSwitch[SDI_SW_DRV23].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_TCM_SWITCH5                    },   /* gSwitch[SDI_SW_DRV24].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_TCM_SWITCH6                    },   /* gSwitch[SDI_SW_DRV25].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_TCM_SWITCH7                    },   /* gSwitch[SDI_SW_DRV26].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_TCM_SWITCH8                    },   /* gSwitch[SDI_SW_DRV27].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_HCM_SWITCH1                    },   /* gSwitch[SDI_SW_DRV28].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_HCM_SWITCH2                    },   /* gSwitch[SDI_SW_DRV29].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_HCM_SWITCH3                    },   /* gSwitch[SDI_SW_DRV30].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_HCM_SWITCH4                    },   /* gSwitch[SDI_SW_DRV31].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_HCM_SWITCH5                    },   /* gSwitch[SDI_SW_DRV32].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_HCM_SWITCH6                    },   /* gSwitch[SDI_SW_DRV33].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_HCM_SWITCH7                    },   /* gSwitch[SDI_SW_DRV34].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_HCM_SWITCH8                    },   /* gSwitch[SDI_SW_DRV35].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_IN_ZP_CAN_INTERFACE_ANALOGIN3  },   /* gSwitch[SDI_SW_DRV36].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_IN_ZP_CAN_INTERFACE_ANALOGIN4  },   /* gSwitch[SDI_SW_DRV37].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_ANALOG_INPUT_SCALED_UNITS118   },   /* gSwitch[SDI_SW_DRV38].Control.rSwitchDriverInput         */
            { PROCESS_SW_DRV,    IO_SUPV_SWITCH7                   }    /* gSwitch[SDI_SW_DRV39].Control.rSwitchDriverInput         */
        }
    },
    {   /* eMode,      rHighThresh, rLowThresh, rHysteresis, ulWCMaxTime       SDI_SwitchDriverModuleConfig[]             */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV1]  */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV2]  */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV3]  */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV4]  */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV5]  */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV6]  */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV7]  */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV8]  */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV9]  */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV10] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV11] */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV12] */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV13] */
        {  SM_DIGITAL, 3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV14] */
        {  SM_DIGITAL, 3.21f,       0.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV15] */
        {  SM_DIGITAL, 3.21f,       0.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV16] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV17] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV18] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV19] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV20] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV21] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV22] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV23] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV24] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV25] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV26] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV27] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV28] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV29] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV30] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV31] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV32] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV33] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV34] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV35] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV36] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV37] */
        {  SM_ANALOG,  3.21f,       1.64f,      0.100f,      32000u },      /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV38] */
        {  SM_DIGITAL, 1.0f,        1.0f,       0.100f,      32000u }       /* SDI_SwitchDriverModuleConfig[SDI_SW_DRV39] */
    },
    15.0f,                                                    /* rNominalSwitchVoltage */
    83,                                                       /* ulSwitchVoltageInputChannel */
    {INTC_IRQ3_VECTOR, 3u},                                   /* MscommRxDoneIntNum */
    {142u, 0u},                                               /* MscommTxDoneIntNum */
    {ADC_A_TOVR_VECTOR, 0u},                                  /* AdcATovrIntNum */
    {ADC_A_NC_VECTOR, 0u},                                    /* AdcANcIntNum */
    {ADC_A_PAUSE_VECTOR, 0u},                                 /* AdcAPauseIntNum */
    {ADC_A_EOQ_VECTOR, 0u},                                   /* AdcAEoqIntNum */
    {ADC_B_TOVR_VECTOR, 0u},                                  /* AdcBTovrIntNum */
    {ADC_B_NC_VECTOR, 0u},                                    /* AdcBNcIntNum */
    {ADC_B_PAUSE_VECTOR, 0u},                                 /* AdcBPauseIntNum */
    {ADC_B_EOQ_VECTOR, 0u},                                   /* AdcBEoqIntNum */
    196u,                                                     /* ulPin_Undervoltage */
    17u,                                                      /* ulUndervoltage_EMIOS_Channel */
    {INTC_EMIOS17_VECTOR, 0u},                                /* UndervoltageIntNum */
    10u,                                                      /* ulUndervoltageRecoverTimeMs */
    {INTC_IRQ4_15_VECTOR, 4u},                                /* AccelIntNum */
    {INTC_IRQ4_15_VECTOR, 6u},                                /* Main5VResetIntNum */
    {136u, 0u},                                               /* MscommRxOverflowIntNum */
    {141u, 0u},                                               /* MscommTxUnderflowIntNum */
    2u,                                                       /* ulNumber_VCM_Processors */
    2u,                                                       /* ulAdcAConfigCommandNum */
    {                                                         /* ulAdcAConfigCommands */
        0x80880901u,    /* ADC0_CR:  ADC0_EN = 1 (Enable), ADC0_EMUX = 1 (Enable), ADC0_CLK_SEL = 0 (Prescalar), ADC0_CLK_PS = 0x1001 (264mhz/20 = 12.7mhz ADC clk) */
        0x82800901u     /* ADC1_CR:  ADC1_EN = 1 (Enable), ADC1_EMUX = 0 (Disable), ADC1_CLK_SEL = 0 (Prescalar), ADC1_CLK_PS = 0x1001 (264mhz/20 = 12.7mhz ADC clk) */
    },
    2u,                                                       /* ulAdcBConfigCommandNum */
    {                                                         /* ulAdcBConfigCommands */
        0x80800901u,    /* ADC0_CR:  ADC0_EN = 1 (Enable), ADC0_EMUX = 0 (Disable), ADC0_CLK_SEL = 0 (Prescalar), ADC0_CLK_PS = 0x1001 (264mhz/20 = 12.7mhz ADC clk) */
        0x82800901u     /* ADC1_CR:  ADC1_EN = 1 (Enable), ADC1_EMUX = 0 (Disable), ADC1_CLK_SEL = 0 (Prescalar), ADC1_CLK_PS = 0x1001 (264mhz/20 = 12.7mhz ADC clk) */
    },
    456u,                                                     /* ulPin_PSP5_5V_12V_Shutdown */
    457u,                                                     /* ulPin_PSP5_5V_12V_VoltageSelect */
    468u,                                                     /* ulPin_PSP6_5V_12V_Shutdown */
    469u,                                                     /* ulPin_PSP6_5V_12V_VoltageSelect */
    244u,                                                     /* ulPin_supv_synchronization */
    {   /* GyroParams */
        GYRO_INVENSENSE,                                      /* eDeviceType */
        GYRO_RANGE_SELECT_250,                                /* eRangeSelection */
        GYRO_FILTER_SELECT_10Hz,                              /* eFilterConfig */
        0u                                                    /* ubSampleRateDivider */
    },
    {   /* fram_gyro_cal_data_t GyroCalDefaults */
        0u,                                                   /* ulRevision */
        {   /* gyro_cals_t OrientationData */
            {   /* X Orientation */
                {
                    {1.0f, 0.0f},                             /* X AxisScaleFactor */
                    {1.0f, 0.0f},                             /* Y AxisScaleFactor */
                    {1.0f, 0.0f}                              /* Z AxisScaleFactor */
                }
            },
            {   /* Y Orientation */
                {
                    {1.0f, 0.0f},                             /* X AxisScaleFactor */
                    {1.0f, 0.0f},                             /* Y AxisScaleFactor */
                    {1.0f, 0.0f}                              /* Z AxisScaleFactor */
                }
            },
            {   /* Z Orientation */
                {
                    {1.0f, 0.0f},                             /* X AxisScaleFactor */
                    {1.0f, 0.0f},                             /* Y AxisScaleFactor */
                    {1.0f, 0.0f}                              /* Z AxisScaleFactor */
                }
            }
        }
    },
    {   /* ulEtpu2SpiFreqHz */
        1650000u,                                             /* ETPU2_SPI_INSTANCE_0 */
        950000u,                                              /* ETPU2_SPI_INSTANCE_1 */
    },
    {   /* ulEtpu2SpiTxBytes */
        16u,                                                  /* ETPU2_SPI_INSTANCE_0 */
        8u,                                                   /* ETPU2_SPI_INSTANCE_1 */
    },
    {INTC_IRQ4_15_VECTOR, 5u},                                /* GyroIntNum */
    4096u,                                                    /* ulMaxIOM0807ENC1Counts */
    65535u,                                                   /* ulMaxEncAmcCounts */
    20000u,                                                   /* ulMaxEncAmcTime */
    {   /* ulEMIOSChannel,        ulPin_ChannelA,         ulPin_ChannelB,         ulPin_DiagnosticAND,    ulPin_DiagnosticOR,     ulPin_Enables,          ulPin_SecondaryEnable        QuadratureModuleConfig[]               */
        {  1u,                    180u,                   179u,                   204u,                   432u,                   139u,                   140u                   }, /* QuadratureModuleConfig[0]  (Encoder 1) */
        {  28u,                   434u,                   433u,                   197u,                   198u,                   168u,                   INVALID_PIN_ASSIGNMENT }, /* QuadratureModuleConfig[1]  (Encoder 3) */
        {  INVALID_EMIOS_CHANNEL, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT }, /* QuadratureModuleConfig[2]  (Encoder 4) */
        {  INVALID_EMIOS_CHANNEL, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT, INVALID_PIN_ASSIGNMENT }, /* QuadratureModuleConfig[3]  (Encoder 5) */
    },
#endif  /* #ifndef PARAM_MODULE_SPECIFIC_H */
