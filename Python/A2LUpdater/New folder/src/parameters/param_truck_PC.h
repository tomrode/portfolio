/**
 *  @file                   param_truck_PC.h
 *  @brief                  This file is used to populate the truck specific portion of the parameter sector of internal FLASH for the PC truck
 *  @copyright              2014 Crown Equipment Corp., New Bremen, OH 45869
 *  @date                   05/23/2014
 *
 *  @remark Author:         Chris Graunke
 *  @remark Project Tree:   C1515
 *
 */

#ifndef PARAM_TRUCK_SPECIFIC_H
#define PARAM_TRUCK_SPECIFIC_H 1

    {                                                       /* ParamInfo */
        {                                                   /* CheckSequence */
            CRC_DUMMY,                                      /* uwCrc */
            CKS_DUMMY,                                      /* uwCks */
            0xFFFFFFFFu                                     /* ulSpare */
        },
        TYPE_PC,                                            /* eVehicleType */
        PARAMETER_REVISION,                                 /* ulRevision */
        PARAMS_START_ADDR_DEFAULT,                          /* ulParamsStartAddr */
        PARAMS_END_ADDR_DEFAULT                             /* ulParamsEndAddr */
    },
    1u,                                                     /* ulQuadrature1_Enable */
    GPIO_STATE_HIGH,                                        /* eQuadrature1A_PullupEnable */
    GPIO_STATE_LOW,                                         /* eQuadrature1A_GainSelect */
    GPIO_STATE_HIGH,                                        /* eQuadrature1B_PullupEnable */
    GPIO_STATE_LOW,                                         /* eQuadrature1B_GainSelect */
    GPIO_STATE_LOW,                                         /* ePS1_Shutdown */
    GPIO_STATE_LOW,                                         /* ePS1_VoltageSelect */
    1u,                                                     /* ulQuadrature3_Enable */
    {                                                       /* Driver1_Params */
        GO_MODE_VOLTAGE,                                    /* eMode */
        TRUE,                                               /* fUseFoldback */
        FALSE,                                              /* fUseDither */
        FALSE,                                              /* fDitherWhileOff */
        FALSE,                                              /* fOpenLoadDiag */
        100u,                                               /* ubDitherFrequency */
        0.5f,                                               /* rDitherAmplitude */
        5000u,                                              /* ulProcDriverRate */
        5000u,                                              /* ulProcOutputRate */
        26.0f,                                              /* rFoldbackInitialCommand */
        18.0f,                                              /* rFoldbackFinalCommand */
        5000000u,                                           /* ulFoldbackTimeout */
        0.0f,                                               /* rKp */
        0.0f,                                               /* rKi */
        5.0f,                                               /* rOpenLoadDutycycleThreshold */
        0.0f,                                               /* rOpenLoadCurrentThreshold */
        0u,                                                 /* ulOpenLoadConcernTime */
        5000u,                                              /* ulDriverRetryTime */
        4u,                                                 /* ulDriverNumRetries */
        FALSE,                                              /* fMaintainDriverOutputCmd */
        TRUE,                                               /* fDoTripCheck */
        0.0f,                                               /* rCurrentMinError */
        0.0f,                                               /* rCurrentMaxError */
        0.0f,                                               /* rDutyCycleClipMin */
        100.0f                                              /* rDutyCycleClipMax */
    },
    {                                                       /* Driver2_Params */
        GO_MODE_VOLTAGE,                                    /* eMode */
        FALSE,                                              /* fUseFoldback */
        FALSE,                                              /* fUseDither */
        FALSE,                                              /* fDitherWhileOff */
        FALSE,                                              /* fOpenLoadDiag */
        100u,                                               /* ubDitherFrequency */
        0.5f,                                               /* rDitherAmplitude */
        5000u,                                              /* ulProcDriverRate */
        5000u,                                              /* ulProcOutputRate */
        26.0f,                                              /* rFoldbackInitialCommand */
        18.0f,                                              /* rFoldbackFinalCommand */
        5000000u,                                           /* ulFoldbackTimeout */
        0.0f,                                               /* rKp */
        0.0f,                                               /* rKi */
        5.0f,                                               /* rOpenLoadDutycycleThreshold */
        0.0f,                                               /* rOpenLoadCurrentThreshold */
        0u,                                                 /* ulOpenLoadConcernTime */
        5000u,                                              /* ulDriverRetryTime */
        4u,                                                 /* ulDriverNumRetries */
        FALSE,                                              /* fMaintainDriverOutputCmd */
        TRUE,                                               /* fDoTripCheck */
        0.0f,                                               /* rCurrentMinError */
        0.0f,                                               /* rCurrentMaxError */
        0.0f,                                               /* rDutyCycleClipMin */
        100.0f                                              /* rDutyCycleClipMax */
    },
    {                                                       /* Driver8_Params */
        GO_MODE_DUTYCYCLE,                                  /* eMode */
        FALSE,                                              /* fUseFoldback */
        FALSE,                                              /* fUseDither */
        FALSE,                                              /* fDitherWhileOff */
        FALSE,                                              /* fOpenLoadDiag */
        100u,                                               /* ubDitherFrequency */
        0.5f,                                               /* rDitherAmplitude */
        5000u,                                              /* ulProcDriverRate */
        5000u,                                              /* ulProcOutputRate */
        26.0f,                                              /* rFoldbackInitialCommand */
        18.0f,                                              /* rFoldbackFinalCommand */
        5000000u,                                           /* ulFoldbackTimeout */
        0.0f,                                               /* rKp */
        0.0f,                                               /* rKi */
        5.0f,                                               /* rOpenLoadDutycycleThreshold */
        0.0f,                                               /* rOpenLoadCurrentThreshold */
        0u,                                                 /* ulOpenLoadConcernTime */
        5000u,                                              /* ulDriverRetryTime */
        4u,                                                 /* ulDriverNumRetries */
        FALSE,                                              /* fMaintainDriverOutputCmd */
        TRUE,                                               /* fDoTripCheck */
        0.0f,                                               /* rCurrentMinError */
        0.0f,                                               /* rCurrentMaxError */
        0.0f,                                               /* rDutyCycleClipMin */
        100.0f                                              /* rDutyCycleClipMax */
    },
    {                                                       /* DriverL1_Params */
        GO_MODE_DUTYCYCLE,                                  /* eMode */
        FALSE,                                              /* fUseFoldback */
        FALSE,                                              /* fUseDither */
        FALSE,                                              /* fDitherWhileOff */
        FALSE,                                              /* fOpenLoadDiag */
        100u,                                               /* ubDitherFrequency */
        0.5f,                                               /* rDitherAmplitude */
        5000u,                                              /* ulProcDriverRate */
        5000u,                                              /* ulProcOutputRate */
        26.0f,                                              /* rFoldbackInitialCommand */
        18.0f,                                              /* rFoldbackFinalCommand */
        5000000u,                                           /* ulFoldbackTimeout */
        0.0f,                                               /* rKp */
        0.0f,                                               /* rKi */
        5.0f,                                               /* rOpenLoadDutycycleThreshold */
        0.0f,                                               /* rOpenLoadCurrentThreshold */
        0u,                                                 /* ulOpenLoadConcernTime */
        5000u,                                              /* ulDriverRetryTime */
        4u,                                                 /* ulDriverNumRetries */
        FALSE,                                              /* fMaintainDriverOutputCmd */
        TRUE,                                               /* fDoTripCheck */
        0.0f,                                               /* rCurrentMinError */
        0.0f,                                               /* rCurrentMaxError */
        0.0f,                                               /* rDutyCycleClipMin */
        100.0f                                              /* rDutyCycleClipMax */
    },
    GPIO_STATE_HIGH,                                        /* eBrake_Outer_Enable_Initial_State  */
    GPIO_STATE_HIGH,                                        /* eBrake_Inner_Enable_Initial_State  */
    GPIO_STATE_HIGH,                                        /* eBrake_Caster_Enable_Initial_State */
    GPIO_STATE_LOW,                                         /* ePS3_Enable */
    GPIO_STATE_LOW,                                         /* ePS3_VoltageSelect */
    {                                                       /* ePotentiometer_Voltage_Selects[] */
        GPIO_STATE_LOW,                                     /* POT1 */
        GPIO_STATE_LOW,                                     /* POT3 */
        GPIO_STATE_LOW,                                     /* POT4 */
        GPIO_STATE_LOW,                                     /* POT5 */
        GPIO_STATE_LOW,                                     /* POT6 */
        GPIO_STATE_LOW,                                     /* POT7 */
        GPIO_STATE_LOW,                                     /* POT8 */
        GPIO_STATE_LOW                                      /* POT9 */
    },
    GPIO_STATE_LOW,                                         /* ePS4_Shutdown */
    GPIO_STATE_HIGH,                                        /* ePS4_VoltageSelect */
    {   /* e4_20mA_PrimaryFunctionSelect[] */
        GPIO_STATE_LOW,                                     /* 4-20 MA INPUT #1 */
        GPIO_STATE_LOW,                                     /* 4-20 MA INPUT #2 */
        GPIO_STATE_LOW                                      /* 4-20 MA INPUT #3 */
    },
    {   /* e4_20mA_VoltageSelect[] */
        GPIO_STATE_LOW,                                     /* 4-20 MA INPUT #1 */
        GPIO_STATE_LOW,                                     /* 4-20 MA INPUT #2 */
        GPIO_STATE_LOW                                      /* 4-20 MA INPUT #3 */
    },
    -4500,                                                  /* slScm_SteerWheelAngle */
    {                                                       /* gParameters.TCM */
        6500u,                                              /* ulRpmLimit */
        1u,                                                 /* ulMotorType */
        0u,                                                 /* ulAnalog2_HighMode */
        1u,                                                 /* ulDigin78_HighMode */
        0u,                                                 /* ulECR2_Enable */
        0u,                                                 /* ulFlipECR1 */
        0u,                                                 /* ulFlipECR2 */
        0u,                                                 /* ulTruckVoltage */
        250u,                                               /* ulVcmPdo1TimeoutMs */
        250u,                                               /* ulVcmPdo2TimeoutMs */
        250u,                                               /* ulVcmPdo3TimeoutMs */
        205u,                                               /* ulVoltageModulationLimit */
        3500u,                                              /* ulTopRPM */
        1000u,                                              /* ulRPM_Trigger */
        1000u,                                              /* ulBottomRPM */
        50u,                                                /* ulRPM_Cut */
        100u,                                               /* ulRPM_CutTime */
        20u,                                                /* ulCurrentWindow */
        1u,                                                 /* ulEnableSlipGovernor */
        0u,                                                 /* ulReversedMotor */
        0u,                                                 /* ulAn1Digin6Mode */
        {                                                   /* ulGP_ControlMode */
            1u,                                             /* GP1 - 0=Current Mode, 1=Voltage Mode, 2=On/Off Mode */
            2u,                                             /* GP2 - 0=Current Mode, 1=Voltage Mode, 2=On/Off Mode */
            0u,                                             /* GP3 - 0=Current Mode, 1=Voltage Mode, 2=On/Off Mode */
            2u,                                             /* GP4 - 0=Current Mode, 1=Voltage Mode, 2=On/Off Mode */
            2u                                              /* GP5 - Can only be 2=On/Off Mode */
        },
        {0u, 0u, 0u, 0u, 0u},                               /* ulGP_DitherEnable */
        {5u, 0u, 0u, 0u, 0u},                               /* ulGP_DitherRate */
        {10u, 0u, 0u, 0u, 0u},                              /* ulGP_DitherLevel */
        {                                                   /* ulGP_MinDitherCurrent */
            26214400u,                                      /* GP1 - Q19 */
            26214400u,                                      /* GP2 - Q19 */
            26214400u,                                      /* GP3 - Q19 */
            26214400u,                                      /* GP4 - Q19 */
            26214400u                                       /* GP5 - Q19 */
        },
        {1u, 1u, 1u, 1u, 1u},                               /* ulGP_RetriesEnabled */
        {3u, 3u, 3u, 3u, 3u},                               /* ulGP_RetryLimit */
        {250u, 250u, 250u, 250u, 500u},                     /* ulGP_RetryDwell */
        {3000u, 3000u, 3000u, 3000u, 3000u},                /* ulGP_RetryExpireTime */
        {                                                   /* ulGP_CurrentLimit */
            1048576000u,                                    /* GP1 - Q19 */
            1048576000u,                                    /* GP2 - Q19 */
            1048576000u,                                    /* GP3 - Q19 */
            1048576000u,                                    /* GP4 - Q19 */
            1048576000u                                     /* GP5 - Q19 */
        },
        {                                                   /* ulGP_VoltageLimit */
            37748736u,                                      /* GP1 - Q20 */
            25165824u,                                      /* GP2 - Q20 */
            25165824u,                                      /* GP3 - Q20 */
            25165824u,                                      /* GP4 - Q20 */
            83886080u                                       /* GP5 - Q20 */
        },
        {0u, 0u, 0u, 0u, 0u},                               /* ulGP_FoldbackEnable */
        {                                                   /* ulGP_PickupCurrentLevel */
            524288000u,                                     /* GP1 - Q19 */
            786432000u,                                     /* GP2 - Q19 */
            786432000u,                                     /* GP3 - Q19 */
            419430400u,                                     /* GP4 - Q19 */
            524288000u                                      /* GP5 - Q19 */
        },
        {                                                   /* ulGP_PickupVoltageLevel */
            25165824u,                                      /* GP1 - Q20 */
            25165824u,                                      /* GP2 - Q20 */
            25165824u,                                      /* GP3 - Q20 */
            25165824u,                                      /* GP4 - Q20 */
            83886080u                                       /* GP5 - Q20 */
        },
        {250u, 250u, 250u, 250u, 250u},                     /* ulGP_PickupTime */
        {                                                   /* ulGP_HoldCurrentLevel */
            314572800u,                                     /* GP1 - Q19 */
            786432000u,                                     /* GP2 - Q19 */
            786432000u,                                     /* GP3 - Q19 */
            419430400u,                                     /* GP4 - Q19 */
            157286400u                                      /* GP5 - Q19 */
        },
        {                                                   /* ulGP_HoldVoltageLevel */
            25165824u,                                      /* GP1 - Q20 */
            9437184u,                                       /* GP2 - Q20 */
            9437184u,                                       /* GP3 - Q20 */
            9437184u,                                       /* GP4 - Q20 */
            83886080u                                       /* GP5 - Q20 */
        },
        {1u, 1u, 0u, 0u, 0u},                               /* ulGP_OpenLoadCheckEnable */
        {0u, 0u, 0u, 0u, 0u},                               /* ulGP_RampUpEnable */
        {                                                   /* ulGP_OpenCurrentLevel */
            26214u,                                         /* GP1 - Q19 */
            26214u,                                         /* GP2 - Q19 */
            26214u,                                         /* GP3 - Q19 */
            26214u,                                         /* GP4 - Q19 */
            26214u                                          /* GP5 - Q19 */
        },
        {                                                   /* ulGP_ShortCurrentLevel */
            104857u,                                        /* GP1 - Q19 */
            104857u,                                        /* GP2 - Q19 */
            104857u,                                        /* GP3 - Q19 */
            104857u,                                        /* GP4 - Q19 */
            104857u                                         /* GP5 - Q19 */
        },
        {100u, 100u, 100u, 100u, 100u},                     /* ulGP_MaxPwmCurrentOpenCircuit */
        {                                                   /* ulGP_MaxPwmVoltageOpenCircuit */
            8388608u,                                       /* GP1 - Q20 */
            8388608u,                                       /* GP2 - Q20 */
            8388608u,                                       /* GP3 - Q20 */
            8388608u,                                       /* GP4 - Q20 */
            8388608u                                        /* GP5 - Q20 */
        },
        {                                                   /* ulGP_CurrentKp */
            5u,                                             /* GP1 - Q19 */
            5u,                                             /* GP2 - Q19 */
            5u,                                             /* GP3 - Q19 */
            5u,                                             /* GP4 - Q19 */
            5u                                              /* GP5 - Q19 */
        },
        {                                                   /* ulGP_CurrentKi */
            10485u,                                         /* GP1 - Q19 */
            10485u,                                         /* GP2 - Q19 */
            10485u,                                         /* GP3 - Q19 */
            10485u,                                         /* GP4 - Q19 */
            10485u                                          /* GP5 - Q19 */
        },
        {10u, 400u, 10u, 10u, 10u},                         /* ulGP_OpenLoadConcernLimit */
        {0u, 1u, 0u, 1u, 0u},                               /* ulGP_Enable */
        {0u, 0u, 0u, 0u, 0u},                               /* ulGP_TestOutputLevel */
        83886080u,                                          /* ulMaxKeyVoltsLimit - Q20 */
        1u                                                  /* ulBridgeModeInputCommand - 0=Disable, 1=Torque, 2=Iq, 3=VF, 4=DC, 5=Speed */
    },
    {                                                       /* gParameters.HCM */
        6500u,                                              /* ulRpmLimit */
        1u,                                                 /* ulMotorType */
        0u,                                                 /* ulAnalog2_HighMode */
        1u,                                                 /* ulDigin78_HighMode */
        1u,                                                 /* ulECR2_Enable */
        0u,                                                 /* ulFlipECR1 */
        1u,                                                 /* ulFlipECR2 */
        0u,                                                 /* ulTruckVoltage */
        250u,                                               /* ulVcmPdo1TimeoutMs */
        250u,                                               /* ulVcmPdo2TimeoutMs */
        250u,                                               /* ulVcmPdo3TimeoutMs */
        205u,                                               /* ulVoltageModulationLimit */
        3500u,                                              /* ulTopRPM */
        1000u,                                              /* ulRPM_Trigger */
        1000u,                                              /* ulBottomRPM */
        50u,                                                /* ulRPM_Cut */
        100u,                                               /* ulRPM_CutTime */
        20u,                                                /* ulCurrentWindow */
        1u,                                                 /* ulEnableSlipGovernor */
        0u,                                                 /* ulReversedMotor */
        0u,                                                 /* ulAn1Digin6Mode */
        {                                                   /* ulGP_ControlMode */
            1u,                                             /* GP1 - 0=Current Mode, 1=Voltage Mode, 2=On/Off Mode */
            2u,                                             /* GP2 - 0=Current Mode, 1=Voltage Mode, 2=On/Off Mode */
            0u,                                             /* GP3 - 0=Current Mode, 1=Voltage Mode, 2=On/Off Mode */
            2u,                                             /* GP4 - 0=Current Mode, 1=Voltage Mode, 2=On/Off Mode */
            2u                                              /* GP5 - Can only be 2=On/Off Mode */
        },
        {0u, 0u, 0u, 0u, 0u},                               /* ulGP_DitherEnable */
        {5u, 0u, 0u, 0u, 0u},                               /* ulGP_DitherRate */
        {10u, 0u, 0u, 0u, 0u},                              /* ulGP_DitherLevel */
        {                                                   /* ulGP_MinDitherCurrent */
            26214400u,                                      /* GP1 - Q19 */
            26214400u,                                      /* GP2 - Q19 */
            26214400u,                                      /* GP3 - Q19 */
            26214400u,                                      /* GP4 - Q19 */
            26214400u                                       /* GP5 - Q19 */
        },
        {1u, 1u, 1u, 1u, 1u},                               /* ulGP_RetriesEnabled */
        {3u, 3u, 3u, 3u, 3u},                               /* ulGP_RetryLimit */
        {250u, 250u, 250u, 250u, 500u},                     /* ulGP_RetryDwell */
        {3000u, 3000u, 3000u, 3000u, 3000u},                /* ulGP_RetryExpireTime */
        {                                                   /* ulGP_CurrentLimit */
            1048576000u,                                    /* GP1 - Q19 */
            1048576000u,                                    /* GP2 - Q19 */
            1048576000u,                                    /* GP3 - Q19 */
            1048576000u,                                    /* GP4 - Q19 */
            1048576000u                                     /* GP5 - Q19 */
        },
        {                                                   /* ulGP_VoltageLimit */
            37748736u,                                      /* GP1 - Q20 */
            25165824u,                                      /* GP2 - Q20 */
            25165824u,                                      /* GP3 - Q20 */
            25165824u,                                      /* GP4 - Q20 */
            83886080u                                       /* GP5 - Q20 */
        },
        {0u, 0u, 0u, 0u, 0u},                               /* ulGP_FoldbackEnable */
        {                                                   /* ulGP_PickupCurrentLevel */
            524288000u,                                     /* GP1 - Q19 */
            786432000u,                                     /* GP2 - Q19 */
            786432000u,                                     /* GP3 - Q19 */
            419430400u,                                     /* GP4 - Q19 */
            524288000u                                      /* GP5 - Q19 */
        },
        {                                                   /* ulGP_PickupVoltageLevel */
            25165824u,                                      /* GP1 - Q20 */
            25165824u,                                      /* GP2 - Q20 */
            25165824u,                                      /* GP3 - Q20 */
            25165824u,                                      /* GP4 - Q20 */
            83886080u                                       /* GP5 - Q20 */
        },
        {250u, 250u, 250u, 250u, 250u},                     /* ulGP_PickupTime */
        {                                                   /* ulGP_HoldCurrentLevel */
            314572800u,                                     /* GP1 - Q19 */
            786432000u,                                     /* GP2 - Q19 */
            786432000u,                                     /* GP3 - Q19 */
            419430400u,                                     /* GP4 - Q19 */
            157286400u                                      /* GP5 - Q19 */
        },
        {                                                   /* ulGP_HoldVoltageLevel */
            25165824u,                                      /* GP1 - Q20 */
            9437184u,                                       /* GP2 - Q20 */
            9437184u,                                       /* GP3 - Q20 */
            9437184u,                                       /* GP4 - Q20 */
            83886080u                                       /* GP5 - Q20 */
        },
        {1u, 1u, 0u, 0u, 0u},                               /* ulGP_OpenLoadCheckEnable */
        {0u, 0u, 0u, 0u, 0u},                               /* ulGP_RampUpEnable */
        {                                                   /* ulGP_OpenCurrentLevel */
            26214u,                                         /* GP1 - Q19 */
            26214u,                                         /* GP2 - Q19 */
            26214u,                                         /* GP3 - Q19 */
            26214u,                                         /* GP4 - Q19 */
            26214u                                          /* GP5 - Q19 */
        },
        {                                                   /* ulGP_ShortCurrentLevel */
            104857u,                                        /* GP1 - Q19 */
            104857u,                                        /* GP2 - Q19 */
            104857u,                                        /* GP3 - Q19 */
            104857u,                                        /* GP4 - Q19 */
            104857u                                         /* GP5 - Q19 */
        },
        {100u, 100u, 100u, 100u, 100u},                     /* ulGP_MaxPwmCurrentOpenCircuit */
        {                                                   /* ulGP_MaxPwmVoltageOpenCircuit */
            8388608u,                                       /* GP1 - Q20 */
            8388608u,                                       /* GP2 - Q20 */
            8388608u,                                       /* GP3 - Q20 */
            8388608u,                                       /* GP4 - Q20 */
            8388608u                                        /* GP5 - Q20 */
        },
        {                                                   /* ulGP_CurrentKp */
            5u,                                             /* GP1 - Q19 */
            5u,                                             /* GP2 - Q19 */
            5u,                                             /* GP3 - Q19 */
            5u,                                             /* GP4 - Q19 */
            5u                                              /* GP5 - Q19 */
        },
        {                                                   /* ulGP_CurrentKi */
            10485u,                                         /* GP1 - Q19 */
            10485u,                                         /* GP2 - Q19 */
            10485u,                                         /* GP3 - Q19 */
            10485u,                                         /* GP4 - Q19 */
            10485u                                          /* GP5 - Q19 */
        },
        {10u, 400u, 10u, 10u, 10u},                         /* ulGP_OpenLoadConcernLimit */
        {0u, 1u, 0u, 1u, 0u},                               /* ulGP_Enable */
        {0u, 0u, 0u, 0u, 0u},                               /* ulGP_TestOutputLevel */
        83886080u,                                          /* ulMaxKeyVoltsLimit - Q20 */
        5u                                                  /* ulBridgeModeInputCommand - 0=Disable, 1=Torque, 2=Iq, 3=VF, 4=DC, 5=Speed */
    },
    {                                                       /* gParameters.IOM0807_1  */ 
        0u                                                  /* kulDummyIOM_WriteValue */
    },
    {                                                       /* gParameters.HDM_1 */
        0u                                                  /* ulHDM_InitCommandWord */
    },
    {                                                       /* gParameters.CEC_1 */
        0u,                                                 /* ulCANencoderCardModel */
        0u,                                                 /* ulCANencoderCardAuxFunction */
        0u                                                  /* ulCANencoderCardBatteryType */
    },
    2000u,                                                  /* ulModuleDelayTimeMs                 */
    {                                                                          /* AG_MatrixConfiguration */
        {                                                                      /* AG_MatrixConfiguration.FixedScaleFactors[] */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_00 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 128.0f,                                                 0.0f },  /* Use @ref AG_SF_ARRAY_01 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f/128.0f,                                            0.0f },  /* Use @ref AG_SF_ARRAY_02 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f/524288.0f,                                         0.0f },  /* Use @ref AG_SF_ARRAY_03 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 4.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_04 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 10.0f/255.0f,                                           0.0f },  /* Use @ref AG_SF_ARRAY_05 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1000.0f,                                                0.0f },  /* Use @ref AG_SF_ARRAY_06 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { (1.0f/10.0f),                                           0.0f },  /* Use @ref AG_SF_ARRAY_07 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { ((100.0f + 4.99f) / 4.99f),                             0.0f },  /* Use @ref AG_SF_ARRAY_08 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 0.8393665f,                                             0.0f },  /* Use @ref AG_SF_ARRAY_09 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 0.360086f,                                              0.0f },  /* Use @ref AG_SF_ARRAY_10 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f/100.0f,                                            0.0f },  /* Use @ref AG_SF_ARRAY_11 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { (12.0f/32767.0f),                                       0.0f },  /* Use @ref AG_SF_ARRAY_12 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { (1.0f/1000.0f),                                         0.0f },  /* Use @ref AG_SF_ARRAY_13 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_14 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { -1.0f,                                                  0.0f },  /* Use @ref AG_SF_ARRAY_15 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 3.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_16 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 100.0f,                                                 0.0f },  /* Use @ref AG_SF_ARRAY_17 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_18 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_19 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_20 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_21 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_22 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_23 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_24 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_25 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_26 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_27 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_28 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_29 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f },  /* Use @ref AG_SF_ARRAY_30 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
            { 1.0f,                                                   0.0f }   /* Use @ref AG_SF_ARRAY_31 in @ref gpAG_ScaleFactorArray[] to access this scale factor  */
        },
        {                                                               /* AG_MatrixConfiguration.ModuleInputConfig[]               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_NOT_USED_LOW                                       */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_NOT_USED_HIGH                                      */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH1                                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH2                                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH3                                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH4                                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH5                                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH6                                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH7                                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH8                                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH9                                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH10                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH11                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH12                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH13                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH14                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH15                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH16                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH17                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH18                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH19                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH20                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH21                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH22                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH23                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH24                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH25                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH26                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH27                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH28                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH29                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH30                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH31                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH32                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH33                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH34                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH35                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH36                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH37                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH38                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SWITCH39                                           */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_MOTOR_SPEED                                 */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_TORQUE_ACHIEVED                             */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_BUS_VOLTAGE                                 */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_DC_EST_CURRENT                              */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_MOTOR_TEMP                                  */
            { AG_INT8,    AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_POWERBASE_TEMP                              */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_MAX_TORQUEOUT_MOTORING                      */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_MAX_TORQUEOUT_REGEN                         */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_ANALOG1                                     */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_ANALOG2                                     */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_POT                                         */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ANALOG_INPUT1_COUNTS                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SUPV_POT2_COUNTS                                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SUPV_POT10_COUNTS                                  */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ANALOG_INPUT40_COUNTS                              */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ANALOG_INPUT41_COUNTS                              */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ANALOG_INPUT65_COUNTS                              */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ANALOG_INPUT66_COUNTS                              */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ANALOG_INPUT119_COUNTS                             */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ANALOG_INPUT120_COUNTS                             */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ANALOG_INPUT121_COUNTS                             */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ANALOG_INPUT122_COUNTS                             */
            { AG_UINT8,   AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN1                                         */
            { AG_UINT8,   AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN2                                         */
            { AG_UINT8,   AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN3                                         */
            { AG_UINT8,   AG_RIGHT,      0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN4                                         */
            { AG_UINT8,   AG_RIGHT,      0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN5                                         */
            { AG_UINT8,   AG_RIGHT,      0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN6                                         */
            { AG_UINT8,   AG_RIGHT,      0x40u, 6u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN7                                         */
            { AG_UINT8,   AG_RIGHT,      0x80u, 7u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN8                                         */
            { AG_UINT8,   AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN9                                         */
            { AG_UINT8,   AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN10                                        */
            { AG_UINT8,   AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN11                                        */
            { AG_UINT8,   AG_RIGHT,      0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN12                                        */
            { AG_UINT8,   AG_RIGHT,      0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN13                                        */
            { AG_UINT8,   AG_RIGHT,      0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN14                                        */
            { AG_UINT8,   AG_RIGHT,      0x40u, 6u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN15                                        */
            { AG_UINT8,   AG_RIGHT,      0x80u, 7u, AG_SF_ARRAY_00 },   /* AG_IN_X10_DIGIN16                                        */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_X10_ANALOG1                                        */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_X10_ANALOG2                                        */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_X10_ANALOG3                                        */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_HCM_STATUS                                         */
            { AG_UINT16,  AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_HCM_STATUS_RDY_SW_ON                               */
            { AG_UINT16,  AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_HCM_STATUS_SWITCHED_ON                             */
            { AG_UINT16,  AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_HCM_STATUS_POWERSTAGE_ON                           */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_STATUS                                         */
            { AG_UINT16,  AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_STATUS_RDY_SW_ON                               */
            { AG_UINT16,  AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_STATUS_SWITCHED_ON                             */
            { AG_UINT16,  AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_STATUS_POWERSTAGE_ON                           */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCMSLV_STATUS                                      */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_TCM_STATUS                                         */
            { AG_UINT16,  AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_TCM_STATUS_RDY_SW_ON                               */
            { AG_UINT16,  AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_TCM_STATUS_SWITCHED_ON                             */
            { AG_UINT16,  AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_TCM_STATUS_POWERSTAGE_ON                           */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_TCM_ED_VOLTS                                       */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_DRIVE_UNIT_POSITIONFB1                         */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_DRIVE_UNIT_POSITIONFB2                         */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_STEERING_FB_SPEED                              */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_MOTOR_CURRENT                                  */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_STOP_TRACTION                                  */
            { AG_UINT32,  AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_TCM_MODULE_SWITCHED_ON                             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS0                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS1                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS2                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS3                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS4                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS5                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS6                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS7                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS8                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS9                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS10                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS11                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS12                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS13                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS14                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS15                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS16                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS17                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS18                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS19                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS20                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS21                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS22                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS23                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS24                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS25                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS26                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS27                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS28                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS29                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS30                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS31                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS32                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS33                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS34                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS35                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS36                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS37                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS38                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS39                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS40                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS41                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS42                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS43                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS44                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS45                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS46                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS47                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS48                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS49                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS50                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS51                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS52                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS53                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS54                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS55                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS56                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS57                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS58                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS59                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS60                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS61                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS62                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS63                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS64                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS65                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS66                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS67                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS68                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS69                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS70                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS71                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS72                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS73                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS74                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS75                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS76                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS77                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS78                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS79                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS80                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS81                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS82                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS83                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS84                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS85                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS86                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS87                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS88                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS89                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS90                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS91                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS92                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS93                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS94                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS95                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS96                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS97                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS98                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS99                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS100                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS101                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS102                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS103                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS104                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS105                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS106                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS107                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS108                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS109                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS110                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS111                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS112                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS113                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS114                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS115                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS116                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS117                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS118                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS119                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS120                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS121                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS122                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS123                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS124                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS125                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS126                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS127                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS128                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS129                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS130                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS131                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS132                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_ANALOG_INPUT_SCALED_UNITS133                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_BATTERY_WIDTH                                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_FORK_LENGTH                                           */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_MOTOR_SPEED                                 */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_DC_EST_CURRENT                              */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_STATUS1                                     */
            { AG_UINT16,  AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_STATUS1_PSTAGE_ON                           */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_STATUS2                                     */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_RMS_CURRENT                                 */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_WARNING                                     */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_CONTROLLER_TEMP                             */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_PROP_VALVE_CURRENT                          */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_MOTOR_TEMP                                  */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HDM_PUMP_CURRENT                                */
            { AG_UINT8,   AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_SWITCH1                                  */
            { AG_UINT8,   AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_SWITCH2                                  */
            { AG_UINT8,   AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_SWITCH3                                  */
            { AG_UINT8,   AG_RIGHT,      0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_SWITCH4                                  */
            { AG_UINT8,   AG_RIGHT,      0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_SWITCH5                                  */
            { AG_UINT8,   AG_RIGHT,      0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_SWITCH6                                  */
            { AG_UINT8,   AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP1_SHORT                                */
            { AG_UINT8,   AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP2_SHORT                                */
            { AG_UINT8,   AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP3_SHORT                                */
            { AG_UINT8,   AG_RIGHT,      0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP4_SHORT                                */
            { AG_UINT8,   AG_RIGHT,      0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP5_SHORT                                */
            { AG_UINT8,   AG_RIGHT,      0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP6_SHORT                                */
            { AG_UINT8,   AG_RIGHT,      0x40u, 6u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP7_SHORT                                */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_ECR_DELTA_TIME_US                        */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_ECR1                                     */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_ECR2                                     */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_ANALOG1                                  */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_ANALOG2                                  */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_ANALOG3                                  */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_ANALOG4                                  */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP1_FEEDBACK                             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP2_FEEDBACK                             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP3_FEEDBACK                             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP4_FEEDBACK                             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP5_FEEDBACK                             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP6_FEEDBACK                             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_IOM0807_1_GP7_FEEDBACK                             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_MOTOR_SPEED                                 */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_TORQUE_ACHIEVED                             */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_BUS_VOLTAGE                                 */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_DC_EST_CURRENT                              */
            { AG_UINT32,  AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_HCM_MODULE_SWITCHED_ON                             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_MAX_TORQUE_OUT_MOTORING                     */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_MAX_TORQUE_OUT_REGEN                        */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_STATUS                                      */
            { AG_INT8,    AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_POWERBASE_TEMP                              */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_MOTOR_TEMP                                  */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_ENCODER1                                    */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_ENCODER2                                    */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_ANALOG1                                     */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_ANALOG2                                     */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_HCM_POT                                         */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_MASTER_ENC1_COUNTS                                 */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SUPV_ENC2_COUNTS                                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SUPV_ENC2_CHA_VOLTS                                */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SUPV_POT2_VOLTS                                    */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SUPV_POT10_VOLTS                                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_SV1_CMD                                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_SV2_CMD                                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_SV3_CMD                                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_SV4_CMD                                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_SV5_CMD                                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_SV6_CMD                                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_SV7_CMD                                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_SV8_CMD                                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_ED_POS_CMD                                       */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_ED_NEG_CMD                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP1_I_Amps                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP2_I_Amps                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP3_I_Amps                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP4_I_Amps                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP5_I_Amps                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP6_I_Amps                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP7_I_Amps                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP8_I_Amps                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP9_I_Amps                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_GP10_I_Amps                                      */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_INNER_BRAKE_STATE                                */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_OUTER_BRAKE_STATE                                */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_SUPV_CASTER_BRAKE_STATE                               */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_DIST_TRAVELED                                      */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ACCUM_METER_ALWAYS_ON                              */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_16 },   /* AG_IN_BATT_AMP_HOURS                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_HCM_MOTOR_REQD                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_MAIN_RL_ACTIVE                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_HDM_MOTOR_REQD                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_TRUCK_MOVEMENT                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_TRUCK_RUNTIME                                      */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_TCM_HOUR_METER                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_HCM_HOUR_METER                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_U1_STR_SUPER_FRESH_CNT                             */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_HDM_HOUR_METER                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_HOUR_METER                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ATTENDED                                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ACCEL_X_AXIS                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ACCEL_Y_AXIS                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_ACCEL_Z_AXIS                                       */
            { AG_UINT8,   AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN1                            */
            { AG_UINT8,   AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN2                            */
            { AG_UINT8,   AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN3                            */
            { AG_UINT8,   AG_RIGHT,      0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN4                            */
            { AG_UINT8,   AG_RIGHT,      0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN5                            */
            { AG_UINT8,   AG_RIGHT,      0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN6                            */
            { AG_UINT8,   AG_RIGHT,      0x40u, 6u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN7                            */
            { AG_UINT8,   AG_RIGHT,      0x80u, 7u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN8                            */
            { AG_UINT8,   AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN9                            */
            { AG_UINT8,   AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN10                           */
            { AG_UINT8,   AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN11                           */
            { AG_UINT8,   AG_RIGHT,      0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN12                           */
            { AG_UINT8,   AG_RIGHT,      0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN13                           */
            { AG_UINT8,   AG_RIGHT,      0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_IN_ZP_CAN_INTERFACE_DIGIN14                           */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_POS_QUAD_ABS_1                                     */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_ENCODER1                                    */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_RX_TCM_ENCODER2                                    */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_CEC_STATUS_WORD                                    */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_CEC_Enc0Pulses                                     */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_CEC_Enc1Pulses                                     */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_MSTR_FB_FRCNT                                      */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCMSLV_STOP_TRACTION                               */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SLV_FB_FRCNT                                       */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_SCM_STRMCMD2                                       */
            { AG_UINT8,   AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_IN_VCM_CRSCHK                                         */
            { AG_UINT8,   AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_IN_VCM_STPTR_CRSCHK2                                  */
            { AG_UINT8,   AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_IN_VCM_MOTDIR_CRSCHK2                                 */
            { AG_UINT8,   AG_RIGHT,      0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_IN_VCM_STRSP_CRSCHK2                                  */
            { AG_UINT8,   AG_RIGHT,      0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_IN_VCM_STRFB_CRSCHK2                                  */
            { AG_UINT8,   AG_RIGHT,      0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_IN_VCM_SNSR_CRSCHK2                                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_GYRO_X_AXIS                                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_IN_GYRO_Y_AXIS                                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 }    /* AG_IN_GYRO_Z_AXIS                                        */
        },  
        {                                                               /* AG_MatrixConfiguration.ModelInputConfig[]                */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulBrs1                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulBrs2                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulBrs3                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulFps1                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulFps2                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulFps3                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulDms                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulGts                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulBres                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulStps                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulEns                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulSes                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulSpos                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulSls                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulLms1                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulLms2                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulLms3                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulLms4                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulLms5                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulChbs                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulChss                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulHms                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulQps                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulQcs                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulSas                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulHss                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulSbs                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulHps1                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulHps2                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulZss1                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulZss2                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulZss3                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulSl1s                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulSl2s                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulOrs                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gOp_int_inputs.ulFss                                     */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_tcm1.grMotorSpeed             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_02 },   /* gPowerbase_fb.Powerbase_fb_tcm1.grMotorTorqAchieved      */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_03 },   /* gPowerbase_fb.Powerbase_fb_tcm1.grBusVoltage             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_03 },   /* gPowerbase_fb.Powerbase_fb_tcm1.grDcCurrentEst           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_tcm1.rMotorTemp               */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_tcm1.swControlTemp            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_02 },   /* gPowerbase_fb.Powerbase_fb_tcm1.rMaxTorkOutMotoring      */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_02 },   /* gPowerbase_fb.Powerbase_fb_tcm1.rMaxTorkOutRegen         */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteer_inputs.slStrCmd1                                  */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_04 },   /* gSteer_inputs.slStrCmd2                                  */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteer_inputs.ulStrMstrStat                              */            
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteer_inputs.ulStrSlvStat                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_traction1.ulPosDirection      */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_traction1.ulNegDirection      */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_05 },   /* gThrottleIn.Throttle_input_traction1.rAnalogThrottle     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_traction1.ulStart             */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_traction2.ulPosDirection      */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_traction2.ulNegDirection      */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_traction2.ulStart             */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_mainhoist1.ulPosDirection     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_mainhoist1.ulNegDirection     */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_mainhoist1.rAnalogThrottle    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_mainhoist2.ulPosDirection     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_mainhoist2.ulNegDirection     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_auxhoist.ulPosDirection       */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_auxhoist.ulNegDirection       */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_tilt.ulPosDirection           */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_tilt.ulNegDirection           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_tilt.rAnalogThrottle          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_brake.rAnalogThrottle         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_reach_retract.rAnalogThrottle */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gThrottleIn.Throttle_input_sideshift.rAnalogThrottle     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHornBus.fRequest[0]                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHornBus.fRequest[1]                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHornBus.fRequest[2]                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHornBus.fRequest[3]                                     */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_03 },   /* gEd_inputs.rEDVolts                                      */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gEd_inputs.ulHcmStat                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gEd_inputs.ulScmStat                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gEd_inputs.ulTcmStat                                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gEd_inputs.ulHdmStat                                     */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_11 },   /* gSteering_fdbk.MstrFdbk.rStrPos                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteering_fdbk.MstrFdbk.rStrSpd                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteering_fdbk.MstrFdbk.rStrCur                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteering_fdbk.SlvFdbk.ulStopStr                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_11 },   /* gSteering_fdbk.SlvFdbk.rStrPos                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteering_fdbk.SlvFdbk.rStrSpd                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteering_fdbk.SlvFdbk.rStrCur                           */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteering_fdbk.SlvFdbk.ulStopStr                         */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gTractionIntegrationBus.fHourmeterRequest[0]             */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gTractionIntegrationBus.fHourmeterRequest[1]             */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gTractionIntegrationBus.fHourmeterRequest[2]             */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gTractionIntegrationBus.fHourmeterRequest[3]             */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gED_HS_Bus.fInhibit[0]                                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gED_HS_Bus.fInhibit[1]                                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gED_HS_Bus.fInhibit[2]                                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gED_HS_Bus.fInhibit[3]                                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gED_HS_Bus.fInhibit[4]                                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gED_HS_Bus.fInhibit[5]                                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gED_HS_Bus.fInhibit[6]                                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gED_HS_Bus.fInhibit[7]                                   */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* g4_20mA_Input_Bus.ubIndexInput[0]                        */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* g4_20mA_Input_Bus.ubIndexInput[1]                        */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* g4_20mA_Input_Bus.ubIndexInput[2]                        */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* g4_20mA_Input_Bus.ubIndexInput[3]                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gVehicle_fb_c2m_bus.rPriHgtCounts                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gVehicle_fb_c2m_bus.rSecHgtCounts                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_4_20MA_INPUT},/* gVehicle_fb_c2m_bus.rLoadSensePressureRaw                */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gVehicle_fb_c2m_bus.rAccy1Position                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gVehicle_fb_c2m_bus.rTiltPosition                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gVehicle_fb_c2m_bus.rAccerometerXaccel                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gVehicle_fb_c2m_bus.rAccerometerYaccel                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gVehicle_fb_c2m_bus.rAccerometerZaccel                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_13 },   /* gPowerbase_fb.Powerbase_fb_hdm.rMotorSpeed               */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_03 },   /* gPowerbase_fb.Powerbase_fb_hdm.rDcCurrentEst             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_hcm.rMotorSpeed               */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_hcm.rMotorTorqAchieved        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_03 },   /* gPowerbase_fb.Powerbase_fb_hcm.rBusVoltage               */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_03 },   /* gPowerbase_fb.Powerbase_fb_hcm.rDcCurrentEst             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_hcm.rMotorTemp                */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_hcm.swControlTemp             */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[0].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[1].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[2].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[3].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[4].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[5].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[6].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[7].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[8].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[9].ulValue                     */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[10].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[11].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[12].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[13].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[14].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[15].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[16].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[17].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[18].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[19].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[20].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[21].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[22].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[23].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[24].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[25].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[26].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[27].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[28].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[29].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[30].ulValue                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[31].ulValue                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[0].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[1].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[2].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[3].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[4].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[5].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[6].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[7].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[8].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[9].fAdvance                    */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[10].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[11].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[12].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[13].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[14].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[15].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[16].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[17].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[18].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[19].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[20].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[21].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[22].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[23].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[24].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[25].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[26].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[27].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[28].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[29].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[30].fAdvance                   */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gAccumDataMeters.Settings[31].fAdvance                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSOC_input.rVbatt                                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSOC_input.rPmLiftCutout                                 */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_07 },   /* gPowerbase_fb.Powerbase_fb_scm.rDcCurrentEst             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_scm.rMotorTemp                */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gAnalog_in_monitor_bus.rPot1                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gAnalog_in_monitor_bus.rPot2                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gAnalog_in_monitor_bus.rPot3                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gAnalog_in_monitor_bus.rPot4                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rSv1VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rSv2VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rSv3VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rSv4VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rSv5VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rSv6VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rSv7VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rSv8VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs1VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs1GndVoltMon                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rBrkOutNegVoltMon                      */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rBrkOutPosVoltMon                      */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rBrkOutBvVoltMon                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs4VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rSwGndVoltMon                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rEdInVoltMon                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rEdPosVoltMon                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rVstbyVoltMon                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r15vVoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r5vVoltMon                             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rVcmBvKeyIn                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rRS422RxVoltMon                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rRS422TxVoltMon                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rBattSense                             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rBrkInnerBvVoltMon                     */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rBrkInnerNegVoltMon                    */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rBrkInnerPosVoltMon                    */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rCanAVoltMon                           */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rEdNegVoltMon                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rBrkCasterNegVoltMon                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rBrkCasterPosVoltMon                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs2VoltMon                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs3VoltMon5v                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs3VoltMon12v                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs2GndVoltMon                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs3GndVoltMon                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rInternalVrhSupply                     */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rInternalVrlSupply                     */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rInternal3_3vSupply                    */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rInternal1_2vSupply                    */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rInternal2_5vRef                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r4_096vRefAdcA0                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r372mvRefAdcA0                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r4_096vRefAdcA1                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r372mvRefAdcA1                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r4_096vRefAdcB0                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r372mvRefAdcB0                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r4_096vRefAdcB1                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r372mvRefAdcB1                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r4_096vRefAdcA0Counts                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r372mvRefAdcA0Counts                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r4_096vRefAdcA1Counts                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r372mvRefAdcA1Counts                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r4_096vRefAdcB0Counts                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r372mvRefAdcB0Counts                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r4_096vRefAdcB1Counts                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.r372mvRefAdcB1Counts                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs5GndVoltMon                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs5VoltMon12v                         */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rPs5VoltMon5v                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rGpd17VoltMon                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rGpd18VoltMon                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gVoltage_mon_bus.rANA5VoltMon                           */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulSv1DriverSts                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulSv2DriverSts                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulSv3DriverSts                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulSv4DriverSts                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulSv5DriverSts                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulSv6DriverSts                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulSv7DriverSts                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulSv8DriverSts                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulEdPosDriverSts                 */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulEdNegDriverSts                 */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulGpd17DriverSts                 */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSv_driver_req_sts_bus.ulGpd18DriverSts                 */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rSv1Current                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rSv2Current                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rSv3Current                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rSv4Current                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rSv5Current                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rSv6Current                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rSv7Current                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rSv8Current                            */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rIBCurrent                             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rOBCurrent                             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.r4to20maCurrent                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rEdNegCurrent                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gCurrent_mon_bus.rGpd18Current                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gBrake_dx_in_bus.ulInnerBrkPosDriveState                */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gBrake_dx_in_bus.ulInnerBrkNegDriveState                */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gBrake_dx_in_bus.ulOuterBrkPosDriveState                */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gBrake_dx_in_bus.ulOuterBrkNegDriveState                */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gBrake_dx_in_bus.ulCasterBrkPosDriveState               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gBrake_dx_in_bus.ulCasterBrkNegDriveState               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSteer_dx_inputs.ulU2StrSuperFreshCount                 */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gSteer_dx_inputs.ulU2StrHndCdeFreshCount                */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gQP_throttle_input.ulQP_REV_S1                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gQP_throttle_input.ulQP_REV_S2                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gQP_throttle_input.ulQP_REV_S3                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gQP_throttle_input.ulQP_REV_S4                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gQP_throttle_input.ulQP_FWD_S1                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gQP_throttle_input.ulQP_FWD_S2                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gQP_throttle_input.ulQP_FWD_S3                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gQP_throttle_input.ulQP_FWD_S4                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* &gpVehicle_fb_c2m_bus->rAccy1Counts                      */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gVehicle_fb_c2m_bus.rPriHgtCountsAtPowerUp               */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gVehicle_fb_c2m_bus.rSecHgtCountsAtPowerUp               */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gArmrestKeyboardInputs.ubKeys                            */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gArmrestKeyboardInputs.swEncoder0Value                   */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gArmrestKeyboardInputs.swEncoder1Value                   */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteering_fdbk.MstrFdbk.ulFreshCnt                       */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteering_fdbk.SlvFdbk.ulStopTrx                         */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteering_fdbk.SlvFdbk.ulFreshCnt                        */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    2u, AG_SF_ARRAY_04 },   /* gSteer_inputs.slStrCmd2                                  */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSlave_steer_status.fStrU1OkCrossChk                     */      
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSlave_steer_status.fStrStopTractCrossChk2               */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSlave_steer_status.fStrMotionDirCrossChk2               */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSlave_steer_status.fStrSpCrossChk2                      */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSlave_steer_status.fStrFbCrossChk2                      */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 }    /* gSlave_steer_status.fStrSnsrCrossChk2                    */
        },  
        {                                                               /* AG_MatrixConfiguration.ModuleOutputConfig[]              */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_NOT_USED                                          */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_GP1_COMMAND                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP1_COMMAND                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP2_COMMAND                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP3_COMMAND                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP4_COMMAND                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP5_COMMAND                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP6_COMMAND                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP7_COMMAND                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP8_COMMAND                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP9_COMMAND                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP10_COMMAND                                 */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP11_COMMAND                                 */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_GP12_COMMAND                                 */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_TXN_SPEED_SETPOINT                                */
            { AG_UINT16,  AG_LEFT_RMW,   0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_GP1_COMMAND                                   */
            { AG_UINT16,  AG_LEFT_RMW,   0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_GP2_COMMAND                                   */
            { AG_UINT16,  AG_LEFT_RMW,   0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_GP3_COMMAND                                   */
            { AG_UINT16,  AG_LEFT_RMW,   0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_GP4_COMMAND                                   */
            { AG_UINT16,  AG_LEFT_RMW,   0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_GP5_COMMAND                                   */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_GP1_SETPOINT                                  */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_GP2_SETPOINT                                  */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_GP3_SETPOINT                                  */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_GP4_SETPOINT                                  */
            { AG_UINT16,  AG_LEFT_RMW,   0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_SWITCH_ON                                     */
            { AG_UINT16,  AG_LEFT_RMW,   0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_POWERSTAGE_ON                                 */
            { AG_UINT16,  AG_LEFT_RMW,   0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_HOURMETERS_ON                                 */
            { AG_UINT16,  AG_LEFT_RMW,   0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_RAISE_ACTIVE                                  */
            { AG_UINT16,  AG_LEFT_RMW,   0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_LOWER_ACTIVE                                  */
            { AG_UINT16,  AG_LEFT_RMW,   0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_SWITCH_ON                                     */
            { AG_UINT16,  AG_LEFT_RMW,   0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_POWERSTAGE_ON                                 */
            { AG_UINT16,  AG_LEFT_RMW,   0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_DEENERGIZE_STR                                */
            { AG_UINT16,  AG_LEFT_RMW,   0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_SYNC_DU                                       */
            { AG_UINT16,  AG_LEFT_RMW,   0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_SWITCH_ON                                     */
            { AG_UINT16,  AG_LEFT_RMW,   0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_POWERSTAGE_ON                                 */
            { AG_UINT16,  AG_LEFT_RMW,   0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_BUSLOAD_ON                                    */
            { AG_UINT16,  AG_LEFT_RMW,   0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_HOURMETERS_ON                                 */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_COMMAND_SETPOINT1                             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_COMMAND_SETPOINT2                             */
            { AG_INT8,    AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SUPV_MSTR_OK                                      */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM_FRESHNESS1                                    */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_FRESHNESS1                                    */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_FRESHNESS2                                    */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_FRESHNESS1                                    */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_MAX_STEER_SPEED                               */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_12 },   /* AG_OUT_HDM_SPEED_COMMAND                                 */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_HDM_COMMAND_WORD                                  */
            { AG_UINT8,   AG_LEFT_RMW,   0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP1_ENABLE                              */
            { AG_UINT8,   AG_LEFT_RMW,   0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP2_ENABLE                              */
            { AG_UINT8,   AG_LEFT_RMW,   0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP3_ENABLE                              */
            { AG_UINT8,   AG_LEFT_RMW,   0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP4_ENABLE                              */
            { AG_UINT8,   AG_LEFT_RMW,   0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP5_ENABLE                              */
            { AG_UINT8,   AG_LEFT_RMW,   0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP6_ENABLE                              */
            { AG_UINT8,   AG_LEFT_RMW,   0x40u, 6u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP7_ENABLE                              */
            { AG_UINT8,   AG_LEFT_RMW,   0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP1_BB_COMMAND                          */
            { AG_UINT8,   AG_LEFT_RMW,   0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP2_BB_COMMAND                          */
            { AG_UINT8,   AG_LEFT_RMW,   0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP3_BB_COMMAND                          */
            { AG_UINT8,   AG_LEFT_RMW,   0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP4_BB_COMMAND                          */
            { AG_UINT8,   AG_LEFT_RMW,   0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP5_BB_COMMAND                          */
            { AG_UINT8,   AG_LEFT_RMW,   0x20u, 5u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP6_BB_COMMAND                          */
            { AG_UINT8,   AG_LEFT_RMW,   0x40u, 6u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP7_BB_COMMAND                          */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP1_COMMAND_SETPOINT                    */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP2_COMMAND_SETPOINT                    */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP3_COMMAND_SETPOINT                    */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP4_COMMAND_SETPOINT                    */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP5_COMMAND_SETPOINT                    */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP6_COMMAND_SETPOINT                    */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_IOM0807_1_GP7_COMMAND_SETPOINT                    */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_01 },   /* AG_OUT_HYD_SPEED_SETPOINT                                */
            { AG_UINT16,  AG_LEFT_RMW,   0x01u, 0u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_GP1_COMMAND                                   */
            { AG_UINT16,  AG_LEFT_RMW,   0x02u, 1u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_GP2_COMMAND                                   */
            { AG_UINT16,  AG_LEFT_RMW,   0x04u, 2u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_GP3_COMMAND                                   */
            { AG_UINT16,  AG_LEFT_RMW,   0x08u, 3u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_GP4_COMMAND                                   */
            { AG_UINT16,  AG_LEFT_RMW,   0x10u, 4u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_GP5_COMMAND                                   */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_GP1_SETPOINT                                  */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_GP2_SETPOINT                                  */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_GP3_SETPOINT                                  */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_GP4_SETPOINT                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_GP2_COMMAND                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_17 },   /* AG_OUT_GP8_COMMAND                                       */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_GPL1_COMMAND                                      */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_HCM_CONTROLLER_TEMP                               */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_HDM_CONTROLLER_TEMP                               */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM1_CONTROLLER_TEMP                              */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_TCM2_CONTROLLER_TEMP                              */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_CONTROLLER_TEMP                               */
            { AG_UINT16,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_DIR_INFO_QUAD_ABS_1                               */
            { AG_INT16 ,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_MOTOR_SPD                                     */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_SCM_STRCMD1                                       */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* AG_OUT_TRC_SPD_CMD                                       */  
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 }    /* AG_OUT_STPTRC_OKCHK                                      */  
        },
        {                                                               /* AG_MatrixConfiguration.ModelOutputConfig[]               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulEdDriversOn                                */
            { AG_UINT32,  AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulHcmCmd - SWITCH_ON                         */
            { AG_UINT32,  AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulHcmCmd - POWERSTAGE_ON                     */
            { AG_UINT32,  AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulScmCmd - SWITCH_ON                         */
            { AG_UINT32,  AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulScmCmd - POWERSTAGE_ON                     */
            { AG_UINT32,  AG_RIGHT,      0x04u, 2u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulScmCmd - DEENERGIZE_STR                    */
            { AG_UINT32,  AG_RIGHT,      0x08u, 3u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulScmCmd - SYNC_DU                           */
            { AG_UINT32,  AG_RIGHT,      0x01u, 0u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulTcmCmd - SWITCH_ON                         */
            { AG_UINT32,  AG_RIGHT,      0x02u, 1u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulTcmCmd - POWERSTAGE_ON                     */
            { AG_UINT32,  AG_RIGHT,      0x08u, 3u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulTcmCmd - BUSLOAD_ON                        */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gTraction_output.rBrake1Cmd                              */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gTraction_output.rBrake2Cmd                              */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gTraction_output.rBrake3Cmd                              */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_01 },   /* gTraction_output.rTorqueCmd                              */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gTraction_m2c_output.ulTravelAlarmCmd                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSVML                                       */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSVMR                                       */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulLiftContactorCmd                           */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.slHDMSpeedCommand                            */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.slHCMSpeedCommand                            */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY1_SELECT                            */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY2_SELECT                            */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY3_SELECT                            */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_TILT_SELECT                             */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_ACCY1_POS                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_ACCY1_NEG                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_ACCY2_POS                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_ACCY2_NEG                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_ACCY3_POS                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_ACCY3_NEG                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY1_POS                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY1_NEG                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY2_POS                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY2_NEG                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY3_POS                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY3_NEG                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_TILT_UP                                 */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_TILT_DOWN                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_TILT_UP                                 */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_TILT_DOWN                               */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_MAIN_LOWER                              */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_ACCY_DIRECTION                          */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_CYL1                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_CYL2                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_CYL3                                    */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulPV_ALL_ACCY                                */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHyd_output.ulSV_BYPASS                                  */            
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHornBus.fHorn_on                                        */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteer_outputs.slMstrStrCmd                              */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteer_outputs.slSlvStrCmd                               */
            { AG_INT8,    AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteer_dx_out_bus.fStrU1OkCrossCk                        */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteer_outputs.slSteerSpeedMax                           */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gFreshnessBus.ubFreshnessCounter[0]                      */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gFreshnessBus.ubFreshnessCounter[1]                      */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gFreshnessBus.ubFreshnessCounter[2]                      */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gFreshnessBus.ubFreshnessCounter[3]                      */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gFreshnessBus.ubFreshnessCounter[4]                      */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gTractionIntegrationBus.Hourmeter_control.fActivateHours */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteeringIntegrationBus.slSteerSpeedMax                  */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gED_HS_Bus.fED_HS_on                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gHornBus.fHorn_on                                        */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gEd_outputs.ulHdmCmd                                     */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gFunction_active.fMainRaiseActive                        */
            { AG_BOOLEAN, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gFunction_active.fMainLowerActive                        */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_hcm.swControlTemp             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_hdm.swControlTemp             */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_tcm1.swControlTemp            */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_tcm2.swControlTemp            */
            { AG_INT16,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_scm.swControlTemp             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_13 },   /* gSteer_outputs.rTfdCmd                                   */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gKPM_bus.rCommand                                        */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gFunction_active.eAccyVelDir                             */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gPowerbase_fb.Powerbase_fb_tcm1.rMotorSpeed              */
            { AG_INT32,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteer_inputs.slStrCmd1                                  */
            { AG_FLOAT32, AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_06 },   /* gSteer_inputs.rTrxSpdLmtWhAngCmd                         */
            { AG_UINT8,   AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },   /* gSteer_dx_out_bus.fIsStopTractCrossCkDetect              */
            { AG_UINT32,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 },    /* gHyd_output.ulSV_SCV                                     */
            { AG_INT8  ,  AG_NO_BIT_MAN, 0u,    0u, AG_SF_ARRAY_00 }    /* gsbOprAlrmState                                     */
        },
        {                                                               /* AG_MatrixConfiguration.InputRouting[]                    */
            { PROCESS_SYS_MODEL, AG_IN_X10_DIGIN1                  },   /* gOp_int_inputs.ulBrs1                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulBrs2                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulBrs3                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulFps1                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulFps2                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulFps3                                    */
            { PROCESS_SYS_MODEL, AG_IN_SWITCH2                     },   /* gOp_int_inputs.ulDms                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulGts                                     */
            { PROCESS_SYS_MODEL, AG_IN_SWITCH20                    },   /* gOp_int_inputs.ulBres                                    */
            { PROCESS_SYS_MODEL, AG_IN_SWITCH7                     },   /* gOp_int_inputs.ulStps                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulEns                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulSes                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulSpos                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulSls                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulLms1                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulLms2                                    */
            { PROCESS_SYS_MODEL, AG_IN_SWITCH21                    },   /* gOp_int_inputs.ulLms3                                    */
            { PROCESS_SYS_MODEL, AG_IN_SWITCH22                    },   /* gOp_int_inputs.ulLms4                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulLms5                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulChbs                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulChss                                    */
            { PROCESS_SYS_MODEL, AG_IN_SWITCH8                     },   /* gOp_int_inputs.ulHms                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulQps                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulQcs                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulSas                                     */
            { PROCESS_SYS_MODEL, AG_IN_X10_DIGIN2                  },   /* gOp_int_inputs.ulHss                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulSbs                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulHps1                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulHps2                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulZss1                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulZss2                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulZss3                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulSl1s                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulSl2s                                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulOrs                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_HIGH               },   /* gOp_int_inputs.ulFss                                     */
            { PROCESS_SYS_MODEL, AG_IN_RX_TCM_MOTOR_SPEED          },   /* gPowerbase_fb.Powerbase_fb_tcm1.grMotorSpeed,            */
            { PROCESS_SYS_MODEL, AG_IN_RX_TCM_TORQUE_ACHIEVED      },   /* gPowerbase_fb.Powerbase_fb_tcm1.grMotorTorqAchieved      */
            { PROCESS_SYS_MODEL, AG_IN_RX_TCM_BUS_VOLTAGE          },   /* gPowerbase_fb.Powerbase_fb_tcm1.grBusVoltage,            */
            { PROCESS_SYS_MODEL, AG_IN_RX_TCM_DC_EST_CURRENT       },   /* gPowerbase_fb.Powerbase_fb_tcm1.grDcCurrentEst           */
            { PROCESS_SYS_MODEL, AG_IN_RX_TCM_MOTOR_TEMP           },   /* gPowerbase_fb.Powerbase_fb_tcm1.rMotorTemp               */
            { PROCESS_SYS_MODEL, AG_IN_RX_TCM_POWERBASE_TEMP       },   /* gPowerbase_fb.Powerbase_fb_tcm1.swControlTemp            */
            { PROCESS_SYS_MODEL, AG_IN_RX_TCM_MAX_TORQUEOUT_MOTORING},  /* gPowerbase_fb.Powerbase_fb_tcm1.rMaxTorkOutMotoring      */
            { PROCESS_SYS_MODEL, AG_IN_RX_TCM_MAX_TORQUEOUT_REGEN  },   /* gPowerbase_fb.Powerbase_fb_tcm1.rMaxTorkOutRegen         */
            { PROCESS_SYS_MODEL, AG_IN_ANALOG_INPUT1_COUNTS        },   /* gSteer_inputs.slStrCmd1                                  */
            { PROCESS_SYS_MODEL, AG_IN_SUPV_POT2_COUNTS            },   /* gSteer_inputs.slStrCmd2                                  */
            { PROCESS_SYS_MODEL, AG_IN_SCM_STATUS                  },   /* gSteer_inputs.ulStrMstrStat                              */
            { PROCESS_SYS_MODEL, AG_IN_SCMSLV_STATUS               },   /* gSteer_inputs.ulStrSlvStat                               */
            { PROCESS_SYS_MODEL, AG_IN_X10_DIGIN5                  },   /* gThrottleIn.Throttle_input_traction1.ulPosDirection      */
            { PROCESS_SYS_MODEL, AG_IN_X10_DIGIN4                  },   /* gThrottleIn.Throttle_input_traction1.ulNegDirection      */
            { PROCESS_SYS_MODEL, AG_IN_X10_ANALOG3                 },   /* gThrottleIn.Throttle_input_traction1.rAnalogThrottle     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_traction1.ulStart             */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_traction2.ulPosDirection      */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_traction2.ulNegDirection      */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_traction2.ulStart             */
            { PROCESS_SYS_MODEL, AG_IN_X10_DIGIN6                  },   /* gThrottleIn.Throttle_input_mainhoist1.ulPosDirection     */
            { PROCESS_SYS_MODEL, AG_IN_X10_DIGIN7                  },   /* gThrottleIn.Throttle_input_mainhoist1.ulNegDirection     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_mainhoist1.rAnalogThrottle    */
            { PROCESS_SYS_MODEL, AG_IN_SWITCH5                     },   /* gThrottleIn.Throttle_input_mainhoist2.ulPosDirection     */
            { PROCESS_SYS_MODEL, AG_IN_SWITCH6                     },   /* gThrottleIn.Throttle_input_mainhoist2.ulNegDirection     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_auxhoist.ulPosDirection       */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_auxhoist.ulNegDirection       */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_tilt.ulPosDirection           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_tilt.ulNegDirection           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_tilt.rAnalogThrottle          */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_brake.rAnalogThrottle         */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_reach_retract.rAnalogThrottle */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gThrottleIn.Throttle_input_sideshift.rAnalogThrottle     */
            { PROCESS_SYS_MODEL, AG_IN_SWITCH4                     },   /* gHornBus.fRequest[0]                                     */
            { PROCESS_SYS_MODEL, AG_IN_X10_DIGIN3                  },   /* gHornBus.fRequest[1]                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gHornBus.fRequest[2]                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gHornBus.fRequest[3]                                     */
            { PROCESS_SYS_MODEL, AG_IN_TCM_ED_VOLTS                },   /* gEd_inputs.rEDVolts,                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gEd_inputs.ulHcmStat                                     */
            { PROCESS_SYS_MODEL, AG_IN_SCM_STATUS                  },   /* gEd_inputs.ulScmStat                                     */
            { PROCESS_SYS_MODEL, AG_IN_TCM_STATUS                  },   /* gEd_inputs.ulTcmStat                                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gEd_inputs.ulHdmStat                                     */
            { PROCESS_SYS_MODEL, AG_IN_SCM_DRIVE_UNIT_POSITIONFB1  },   /* gSteering_fdbk.MstrFdbk.rStrPos                          */
            { PROCESS_SYS_MODEL, AG_IN_SCM_STEERING_FB_SPEED       },   /* gSteering_fdbk.MstrFdbk.rStrSpd                          */
            { PROCESS_SYS_MODEL, AG_IN_SCM_MOTOR_CURRENT           },   /* gSteering_fdbk.MstrFdbk.rStrCur                          */
            { PROCESS_SYS_MODEL, AG_IN_SCM_STOP_TRACTION           },   /* gSteering_fdbk.MstrFdbk.ulStopTrx                        */
            { PROCESS_SYS_MODEL, AG_IN_SCM_DRIVE_UNIT_POSITIONFB2  },   /* gSteering_fdbk.SlvFdbk.rStrPos                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gSteering_fdbk.SlvFdbk.rStrSpd                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gSteering_fdbk.SlvFdbk.rStrCur                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gSteering_fdbk.SlvFdbk.ulStopTrx                         */
            { PROCESS_SYS_MODEL, AG_IN_TCM_MODULE_SWITCHED_ON      },   /* gTractionIntegrationBus.Hourmeter_control.fRequest[0]    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gTractionIntegrationBus.Hourmeter_control.fRequest[1]    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gTractionIntegrationBus.Hourmeter_control.fRequest[2]    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gTractionIntegrationBus.Hourmeter_control.fRequest[3]    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gED_HS_Bus.fInhibit[0]                                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gED_HS_Bus.fInhibit[1]                                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gED_HS_Bus.fInhibit[2]                                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gED_HS_Bus.fInhibit[3]                                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gED_HS_Bus.fInhibit[4]                                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gED_HS_Bus.fInhibit[5]                                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gED_HS_Bus.fInhibit[6]                                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gED_HS_Bus.fInhibit[7]                                   */
            { PROCESS_ANALOG,    AG_BATTERY_WIDTH                  },   /* g4_20mA_Input_Bus.ubIndexInput[0]                        */
            { PROCESS_ANALOG,    AG_FORK_LENGTH                    },   /* g4_20mA_Input_Bus.ubIndexInput[1]                        */
            { PROCESS_ANALOG,    AG_IN_NOT_USED_LOW                },   /* g4_20mA_Input_Bus.ubIndexInput[2]                        */
            { PROCESS_ANALOG,    AG_IN_NOT_USED_LOW                },   /* g4_20mA_Input_Bus.ubIndexInput[3]                        */
            { PROCESS_SYS_MODEL, AG_IN_MASTER_ENC1_COUNTS          },   /* gVehicle_fb_c2m_bus.rPriHgtCounts                        */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gVehicle_fb_c2m_bus.rSecHgtCounts                        */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS67    },   /* gVehicle_fb_c2m_bus.rLoadSensePressureRaw                */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gVehicle_fb_c2m_bus.rAccy1Position                       */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gVehicle_fb_c2m_bus.rTiltPosition                        */
            { PROCESS_SYS_MODEL, AG_IN_ACCEL_X_AXIS                },   /* gVehicle_fb_c2m_bus.rAccerometerXaccel                   */
            { PROCESS_SYS_MODEL, AG_IN_ACCEL_Y_AXIS                },   /* gVehicle_fb_c2m_bus.rAccerometerYaccel                   */
            { PROCESS_SYS_MODEL, AG_IN_ACCEL_Z_AXIS                },   /* gVehicle_fb_c2m_bus.rAccerometerZaccel                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gPowerbase_fb.Powerbase_fb_hdm.rMotorSpeed               */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gPowerbase_fb.Powerbase_fb_hdm.rDcCurrentEst             */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gPowerbase_fb.Powerbase_fb_hcm.rMotorSpeed               */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gPowerbase_fb.Powerbase_fb_hcm.rMotorTorqAchieved        */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gPowerbase_fb.Powerbase_fb_hcm.rBusVoltage               */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gPowerbase_fb.Powerbase_fb_hcm.rDcCurrentEst             */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gPowerbase_fb.Powerbase_fb_hcm.rMotorTemp                */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gPowerbase_fb.Powerbase_fb_hcm.swControlTemp             */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[0].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[1].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_TCM_HOUR_METER              },   /* gAccumDataMeters.Settings[2].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_SCM_HOUR_METER              },   /* gAccumDataMeters.Settings[3].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[4].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[5].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[6].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[7].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[8].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_DIST_TRAVELED               },   /* gAccumDataMeters.Settings[9].ulValue                     */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[10].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[11].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[12].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[13].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[14].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[15].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[16].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[17].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[18].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[19].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[20].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_BATT_AMP_HOURS              },   /* gAccumDataMeters.Settings[21].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[22].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[23].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[24].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[25].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[26].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[27].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[28].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[29].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[30].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[31].ulValue                    */
            { PROCESS_SYS_MODEL, AG_IN_ACCUM_METER_ALWAYS_ON       },   /* gAccumDataMeters.Settings[0].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[1].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_ACCUM_METER_ALWAYS_ON       },   /* gAccumDataMeters.Settings[2].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_ACCUM_METER_ALWAYS_ON       },   /* gAccumDataMeters.Settings[3].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[4].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[5].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_TCM_STATUS_POWERSTAGE_ON    },   /* gAccumDataMeters.Settings[6].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_SCM_STATUS_POWERSTAGE_ON    },   /* gAccumDataMeters.Settings[7].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_ATTENDED                    },   /* gAccumDataMeters.Settings[8].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_ACCUM_METER_ALWAYS_ON       },   /* gAccumDataMeters.Settings[9].fAdvance                    */
            { PROCESS_SYS_MODEL, AG_IN_HCM_MOTOR_REQD              },   /* gAccumDataMeters.Settings[10].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_HCM_MOTOR_REQD              },   /* gAccumDataMeters.Settings[11].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_HCM_MOTOR_REQD              },   /* gAccumDataMeters.Settings[12].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_HCM_MOTOR_REQD              },   /* gAccumDataMeters.Settings[13].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[14].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_TRUCK_RUNTIME               },   /* gAccumDataMeters.Settings[15].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[16].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[17].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_TRUCK_MOVEMENT              },   /* gAccumDataMeters.Settings[18].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_TRUCK_MOVEMENT              },   /* gAccumDataMeters.Settings[19].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_TRUCK_MOVEMENT              },   /* gAccumDataMeters.Settings[20].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_ACCUM_METER_ALWAYS_ON       },   /* gAccumDataMeters.Settings[21].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[22].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[23].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[24].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[25].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[26].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[27].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[28].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[29].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[30].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gAccumDataMeters.Settings[31].fAdvance                   */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS5     },   /* gSOC_input.rVbatt                                        */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gSOC_input.rPmLiftCutout                                 */
            { PROCESS_SYS_MODEL, AG_IN_SCM_MOTOR_CURRENT           },   /* gPowerbase_fb.Powerbase_fb_scm.rDcCurrentEst             */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gPowerbase_fb.Powerbase_fb_scm.rMotorTemp                */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS1     },   /* gAnalog_in_monitor_bus.rPot1                             */
            { PROCESS_SYS_MODEL, AG_IN_SUPV_POT2_VOLTS             },   /* gAnalog_in_monitor_bus.rPot2                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS3     },   /* gAnalog_in_monitor_bus.rPot3                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS6     },   /* gAnalog_in_monitor_bus.rPot4                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS13    },   /* gVoltage_mon_bus.rSv1VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS17    },   /* gVoltage_mon_bus.rSv2VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS15    },   /* gVoltage_mon_bus.rSv3VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS19    },   /* gVoltage_mon_bus.rSv4VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS14    },   /* gVoltage_mon_bus.rSv5VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS18    },   /* gVoltage_mon_bus.rSv6VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS16    },   /* gVoltage_mon_bus.rSv7VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS20    },   /* gVoltage_mon_bus.rSv8VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS8     },   /* gVoltage_mon_bus.rPs1VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS80    },   /* gVoltage_mon_bus.rPs1GndVoltMon                          */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS32    },   /* gVoltage_mon_bus.rBrkOutNegVoltMon                       */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS28    },   /* gVoltage_mon_bus.rBrkOutPosVoltMon                       */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS26    },   /* gVoltage_mon_bus.rBrkOutBvVoltMon                        */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS35    },   /* gVoltage_mon_bus.rPs4VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS82    },   /* gVoltage_mon_bus.rSwGndVoltMon                           */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS21    },   /* gVoltage_mon_bus.rEdInVoltMon                            */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS24    },   /* gVoltage_mon_bus.rEdPosVoltMon                           */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS79    },   /* gVoltage_mon_bus.rVstbyVoltMon                           */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS83    },   /* gVoltage_mon_bus.r15vVoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS84    },   /* gVoltage_mon_bus.r5vVoltMon                              */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS71    },   /* gVoltage_mon_bus.rVcmBvKeyIn                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS89    },   /* gVoltage_mon_bus.rRS422RxVoltMon                         */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS90    },   /* gVoltage_mon_bus.rRS422TxVoltMon                         */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS5     },   /* gVoltage_mon_bus.rBattSense                              */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS30    },   /* gVoltage_mon_bus.rBrkInnerBvVoltMon                      */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS31    },   /* gVoltage_mon_bus.rBrkInnerNegVoltMon                     */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS27    },   /* gVoltage_mon_bus.rBrkInnerPosVoltMon                     */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS85    },   /* gVoltage_mon_bus.rCanAVoltMon                            */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS23    },   /* gVoltage_mon_bus.rEdNegVoltMon                           */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS33    },   /* gVoltage_mon_bus.rBrkCasterNegVoltMon                    */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS29    },   /* gVoltage_mon_bus.rBrkCasterPosVoltMon                    */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS34    },   /* gVoltage_mon_bus.rPs2VoltMon                             */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS0     },   /* gVoltage_mon_bus.rPs3VoltMon5v                           */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS104   },   /* gVoltage_mon_bus.rPs3VoltMon12v                          */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS22    },   /* gVoltage_mon_bus.rPs2GndVoltMon                          */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS81    },   /* gVoltage_mon_bus.rPs3GndVoltMon                          */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS42    },   /* gVoltage_mon_bus.rInternalVrhSupply                      */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS43    },   /* gVoltage_mon_bus.rInternalVrlSupply                      */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS60    },   /* gVoltage_mon_bus.rInternal3_3vSupply                     */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS51    },   /* gVoltage_mon_bus.rInternal1_2vSupply                     */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS108   },   /* gVoltage_mon_bus.rInternal2_5vRef                        */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS40    },   /* gVoltage_mon_bus.r4_096vRefAdcA0                         */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS41    },   /* gVoltage_mon_bus.r372mvRefAdcA0                          */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS65    },   /* gVoltage_mon_bus.r4_096vRefAdcA1                         */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS66    },   /* gVoltage_mon_bus.r372mvRefAdcA1                          */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS119   },   /* gVoltage_mon_bus.r4_096vRefAdcB0                         */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS120   },   /* gVoltage_mon_bus.r372mvRefAdcB0                          */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS121   },   /* gVoltage_mon_bus.r4_096vRefAdcB1                         */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS122   },   /* gVoltage_mon_bus.r372mvRefAdcB1                          */
            { PROCESS_SYS_MODEL, AG_IN_ANALOG_INPUT40_COUNTS       },   /* gVoltage_mon_bus.r4_096vRefAdcA0Counts                   */
            { PROCESS_SYS_MODEL, AG_IN_ANALOG_INPUT41_COUNTS       },   /* gVoltage_mon_bus.r372mvRefAdcA0Counts                    */
            { PROCESS_SYS_MODEL, AG_IN_ANALOG_INPUT65_COUNTS       },   /* gVoltage_mon_bus.r4_096vRefAdcA1Counts                   */
            { PROCESS_SYS_MODEL, AG_IN_ANALOG_INPUT66_COUNTS       },   /* gVoltage_mon_bus.r372mvRefAdcA1Counts                    */
            { PROCESS_SYS_MODEL, AG_IN_ANALOG_INPUT119_COUNTS      },   /* gVoltage_mon_bus.r4_096vRefAdcB0Counts                   */
            { PROCESS_SYS_MODEL, AG_IN_ANALOG_INPUT120_COUNTS      },   /* gVoltage_mon_bus.r372mvRefAdcB0Countss                   */
            { PROCESS_SYS_MODEL, AG_IN_ANALOG_INPUT121_COUNTS      },   /* gVoltage_mon_bus.r4_096vRefAdcB1Counts                   */
            { PROCESS_SYS_MODEL, AG_IN_ANALOG_INPUT122_COUNTS      },   /* gVoltage_mon_bus.r372mvRefAdcB1Counts                    */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gVoltage_mon_bus.rPs5GndVoltMon                          */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gVoltage_mon_bus.rPs5VoltMon12v                          */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gVoltage_mon_bus.rPs5VoltMon5v                           */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS20    },   /* gVoltage_mon_bus.rGpd17VoltMon                           */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS17    },   /* gVoltage_mon_bus.rGpd18VoltMon                           */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS69    },   /* gVoltage_mon_bus.rANA5VoltMon                            */
            { PROCESS_SYS_MODEL, AG_SUPV_SV1_CMD                   },   /* gSv_driver_req_sts_bus.ulSv1DriverSts                    */
            { PROCESS_SYS_MODEL, AG_SUPV_SV2_CMD                   },   /* gSv_driver_req_sts_bus.ulSv2DriverSts                    */
            { PROCESS_SYS_MODEL, AG_SUPV_SV3_CMD                   },   /* gSv_driver_req_sts_bus.ulSv3DriverSts                    */
            { PROCESS_SYS_MODEL, AG_SUPV_SV4_CMD                   },   /* gSv_driver_req_sts_bus.ulSv4DriverSts                    */
            { PROCESS_SYS_MODEL, AG_SUPV_SV5_CMD                   },   /* gSv_driver_req_sts_bus.ulSv5DriverSts                    */
            { PROCESS_SYS_MODEL, AG_SUPV_SV6_CMD                   },   /* gSv_driver_req_sts_bus.ulSv6DriverSts                    */
            { PROCESS_SYS_MODEL, AG_SUPV_SV7_CMD                   },   /* gSv_driver_req_sts_bus.ulSv7DriverSts                    */
            { PROCESS_SYS_MODEL, AG_SUPV_SV8_CMD                   },   /* gSv_driver_req_sts_bus.ulSv8DriverSts                    */
            { PROCESS_SYS_MODEL, AG_SUPV_ED_POS_CMD                },   /* gSv_driver_req_sts_bus.ulEdPosDriverSts                  */
            { PROCESS_SYS_MODEL, AG_SUPV_ED_NEG_CMD                },   /* gSv_driver_req_sts_bus.ulEdNegDriverSts                  */
            { PROCESS_SYS_MODEL, AG_SUPV_SV8_CMD                   },   /* gSv_driver_req_sts_bus.ulGpd17DriverSts                  */
            { PROCESS_SYS_MODEL, AG_SUPV_SV2_CMD                   },   /* gSv_driver_req_sts_bus.ulGpd18DriverSts                  */
            { PROCESS_SYS_MODEL, AG_SUPV_GP1_I_Amps                },   /* gCurrent_mon_bus.rSv1Current                             */
            { PROCESS_SYS_MODEL, AG_SUPV_GP2_I_Amps                },   /* gCurrent_mon_bus.rSv2Current                             */
            { PROCESS_SYS_MODEL, AG_SUPV_GP3_I_Amps                },   /* gCurrent_mon_bus.rSv3Current                             */
            { PROCESS_SYS_MODEL, AG_SUPV_GP4_I_Amps                },   /* gCurrent_mon_bus.rSv4Current                             */
            { PROCESS_SYS_MODEL, AG_SUPV_GP5_I_Amps                },   /* gCurrent_mon_bus.rSv5Current                             */
            { PROCESS_SYS_MODEL, AG_SUPV_GP6_I_Amps                },   /* gCurrent_mon_bus.rSv6Current                             */
            { PROCESS_SYS_MODEL, AG_SUPV_GP7_I_Amps                },   /* gCurrent_mon_bus.rSv7Current                             */
            { PROCESS_SYS_MODEL, AG_SUPV_GP8_I_Amps                },   /* gCurrent_mon_bus.rSv8Current                             */
            { PROCESS_SYS_MODEL, AG_SUPV_GP9_I_Amps                },   /* gCurrent_mon_bus.rIBCurrent                              */
            { PROCESS_SYS_MODEL, AG_SUPV_GP10_I_Amps               },   /* gCurrent_mon_bus.rOBCurrent                              */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS67    },   /* gCurrent_mon_bus.r4to20maCurrent                         */
            { PROCESS_SYS_MODEL, AG_ANALOG_INPUT_SCALED_UNITS72    },   /* gCurrent_mon_bus.rEdNegCurrent                           */
            { PROCESS_SYS_MODEL, AG_SUPV_GP2_I_Amps                },   /* gCurrent_mon_bus.rGpd18Current                           */
            { PROCESS_SYS_MODEL, AG_SUPV_INNER_BRAKE_STATE         },   /* gBrake_dx_in_bus.ulInnerBrkPosDriveState                 */
            { PROCESS_SYS_MODEL, AG_SUPV_INNER_BRAKE_STATE         },   /* gBrake_dx_in_bus.ulInnerBrkNegDriveState                 */
            { PROCESS_SYS_MODEL, AG_SUPV_OUTER_BRAKE_STATE         },   /* gBrake_dx_in_bus.ulOuterBrkPosDriveState                 */
            { PROCESS_SYS_MODEL, AG_SUPV_OUTER_BRAKE_STATE         },   /* gBrake_dx_in_bus.ulOuterBrkNegDriveState                 */
            { PROCESS_SYS_MODEL, AG_SUPV_CASTER_BRAKE_STATE        },   /* gBrake_dx_in_bus.ulCasterBrakePosDriveState              */
            { PROCESS_SYS_MODEL, AG_SUPV_CASTER_BRAKE_STATE        },   /* gBrake_dx_in_bus.ulCasterBrakeNegDriveState              */
            { PROCESS_SYS_MODEL, AG_IN_U1_STR_SUPER_FRESH_CNT      },   /* gSteer_dx_inputs.ulU2StrSuperFreshCount                  */
            { PROCESS_SYS_MODEL, AG_IN_U1_STR_SUPER_FRESH_CNT      },   /* gSteer_dx_inputs.ulU2StrHndCdeFreshCount                 */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gQP_throttle_input.ulQP_REV_S1                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gQP_throttle_input.ulQP_REV_S2                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gQP_throttle_input.ulQP_REV_S3                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gQP_throttle_input.ulQP_REV_S4                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gQP_throttle_input.ulQP_FWD_S1                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gQP_throttle_input.ulQP_FWD_S2                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gQP_throttle_input.ulQP_FWD_S3                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gQP_throttle_input.ulQP_FWD_S4                           */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gpVehicle_fb_c2m_bus->rAccy1Counts                       */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gVehicle_fb_c2m_bus.rPriHgtCountsAtPowerUp               */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gVehicle_fb_c2m_bus.rSecHgtCountsAtPowerUp               */
            { PROCESS_SYS_MODEL, AG_IN_CEC_STATUS_WORD             },   /* gArmrestKeyboardInputs.ubKeys                            */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gArmrestKeyboardInputs.swEncoder0Value                   */
            { PROCESS_SYS_MODEL, AG_IN_NOT_USED_LOW                },   /* gArmrestKeyboardInputs.swEncoder1Value                   */
            { PROCESS_SYS_MODEL, AG_IN_MSTR_FB_FRCNT               },   /* gSteering_fdbk.MstrFdbk.ulFreshCnt                       */
            { PROCESS_SYS_MODEL, AG_IN_SCMSLV_STOP_TRACTION        },   /* gSteering_fdbk.SlvFdbk.ulStopTrx                         */
            { PROCESS_SYS_MODEL, AG_IN_SLV_FB_FRCNT                },   /* gSteering_fdbk.SlvFdbk.ulFreshCnt                        */
            { PROCESS_SYS_MODEL, AG_IN_SCM_STRMCMD2                },   /* gSteer_inputs.slStrCmd2                                  */
            { PROCESS_SYS_MODEL, AG_IN_VCM_CRSCHK                  },   /* gSlave_steer_status.fStrU1OkCrossChk                     */
            { PROCESS_SYS_MODEL, AG_IN_VCM_STPTR_CRSCHK2           },   /* gSlave_steer_status.fStrStopTractCrossChk2               */
            { PROCESS_SYS_MODEL, AG_IN_VCM_MOTDIR_CRSCHK2          },   /* gSlave_steer_status.fStrMotionDirCrossChk2               */
            { PROCESS_SYS_MODEL, AG_IN_VCM_STRSP_CRSCHK2           },   /* gSlave_steer_status.fStrSpCrossChk2                      */
            { PROCESS_SYS_MODEL, AG_IN_VCM_STRFB_CRSCHK2           },   /* gSlave_steer_status.fStrFbCrossChk2                      */
            { PROCESS_SYS_MODEL, AG_IN_VCM_SNSR_CRSCHK2            }    /* gSlave_steer_status.fStrSnsrCrossChk2                    */
        },
        {                                                               /* AG_MatrixConfiguration.OutputRouting[]                   */
            { PROCESS_SYS_MODEL, AG_OUT_GP1_COMMAND                },   /* gEd_outputs.ulEdDriversOn                                */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gEd_outputs.ulHcmCmd - SWITCH_ON                         */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gEd_outputs.ulHcmCmd - POWERSTAGE_ON                     */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_SWITCH_ON              },   /* gEd_outputs.ulScmCmd - SWITCH_ON                         */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_POWERSTAGE_ON          },   /* gEd_outputs.ulScmCmd - POWERSTAGE_ON                     */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_DEENERGIZE_STR         },   /* gEd_outputs.ulScmCmd - DEENERGIZE_STR                    */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_SYNC_DU                },   /* gEd_outputs.ulScmCmd - SYNC_DU                           */
            { PROCESS_SYS_MODEL, AG_OUT_TCM_SWITCH_ON              },   /* gEd_outputs.ulTcmCmd - SWITCH_ON                         */
            { PROCESS_SYS_MODEL, AG_OUT_TCM_POWERSTAGE_ON          },   /* gEd_outputs.ulTcmCmd - POWERSTAGE_ON                     */
            { PROCESS_SYS_MODEL, AG_OUT_TCM_BUSLOAD_ON             },   /* gEd_outputs.ulTcmCmd - BUSLOAD_ON                        */
            { PROCESS_SYS_MODEL, AG_OUT_SUPV_GP10_COMMAND          },   /* gTraction_output.rBrake1Cmd                              */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gTraction_output.rBrake2Cmd                              */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gTraction_output.rBrake3Cmd                              */
            { PROCESS_SYS_MODEL, AG_OUT_TXN_SPEED_SETPOINT         },   /* gTraction_output.rTorqueCmd                              */
            { PROCESS_SYS_MODEL, AG_OUT_SUPV_GP4_COMMAND           },   /* gTraction_m2c_output.ulTravelAlarmCmd                    */
            { PROCESS_SYS_MODEL, AG_OUT_TCM_GP4_COMMAND            },   /* gHyd_output.ulSVML                                       */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSVMR                                       */
            { PROCESS_SYS_MODEL, AG_OUT_TCM_GP2_COMMAND            },   /* gHyd_output.ulLiftContactorCmd                           */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.slHDMSpeedCommand                            */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.slHCMSpeedCommand                            */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY1_SELECT                            */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY2_SELECT                            */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY3_SELECT                            */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_TILT_SELECT                             */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_ACCY1_POS                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_ACCY1_NEG                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_ACCY2_POS                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_ACCY2_NEG                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_ACCY3_POS                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_ACCY3_NEG                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY1_POS                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY1_NEG                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY2_POS                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY2_NEG                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY3_POS                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY3_NEG                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_TILT_UP                                 */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_TILT_DOWN                               */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_TILT_UP                                 */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_TILT_DOWN                               */
            { PROCESS_SYS_MODEL, AG_OUT_SUPV_GP1_COMMAND           },   /* gHyd_output.ulPV_MAIN_LOWER                              */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_ACCY_DIRECTION                          */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_CYL1                                    */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_CYL2                                    */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_CYL3                                    */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulPV_ALL_ACCY                                */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gHyd_output.ulSV_BYPASS                                  */
            { PROCESS_SYS_MODEL, AG_OUT_SUPV_GP8_COMMAND           },   /* gHornBus.fHorn_on                                        */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_COMMAND_SETPOINT1      },   /* gSteer_outputs.slMstrStrCmd                              */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_COMMAND_SETPOINT2      },   /* gSteer_outputs.slSlvStrCmd                               */
            { PROCESS_SYS_MODEL, AG_OUT_SUPV_MSTR_OK               },   /* gSteer_dx_out_bus.fStrU1OkCrossCk                        */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_MAX_STEER_SPEED        },   /* gSteer_outputs.slSteerSpeedMax                           */
            { PROCESS_SYS_MODEL, AG_OUT_TCM_FRESHNESS1             },   /* gFreshnessBus.ubFreshnessCounter[0]                      */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_FRESHNESS1             },   /* gFreshnessBus.ubFreshnessCounter[1]                      */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_FRESHNESS2             },   /* gFreshnessBus.ubFreshnessCounter[2]                      */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gFreshnessBus.ubFreshnessCounter[3]                      */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gFreshnessBus.ubFreshnessCounter[4]                      */
            { PROCESS_SYS_MODEL, AG_OUT_TCM_HOURMETERS_ON          },   /* gTractionIntegrationBus.fActivateHours                   */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gSteeringIntegrationBus.slSteerSpeedMax                  */
            { PROCESS_SYS_MODEL, AG_OUT_SUPV_GP12_COMMAND          },   /* gED_HS_Bus.fED_HS_on                                     */
            { PROCESS_SYS_MODEL, AG_OUT_GP8_COMMAND                },   /* gHornBus.fHorn_on                                        */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gEd_outputs.ulHdmCmd                                     */
            { PROCESS_SYS_MODEL, AG_OUT_HCM_RAISE_ACTIVE           },   /* gFunction_active.fMainRaiseActive                        */
            { PROCESS_SYS_MODEL, AG_OUT_HCM_LOWER_ACTIVE           },   /* gFunction_active.fMainLowerActive                        */
            { PROCESS_SYS_MODEL, AG_OUT_HCM_CONTROLLER_TEMP        },   /* gPowerbase_fb.Powerbase_fb_hcm.swControlTemp             */
            { PROCESS_SYS_MODEL, AG_OUT_HDM_CONTROLLER_TEMP        },   /* gPowerbase_fb.Powerbase_fb_hdm.swControlTemp             */
            { PROCESS_SYS_MODEL, AG_OUT_TCM1_CONTROLLER_TEMP       },   /* gPowerbase_fb.Powerbase_fb_tcm1.swControlTemp            */
            { PROCESS_SYS_MODEL, AG_OUT_TCM2_CONTROLLER_TEMP       },   /* gPowerbase_fb.Powerbase_fb_tcm2.swControlTemp            */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_CONTROLLER_TEMP        },   /* gPowerbase_fb.Powerbase_fb_scm.swControlTemp             */
            { PROCESS_SYS_MODEL, AG_OUT_SUPV_GP11_COMMAND          },   /* gSteer_outputs.rTfdCmd                                   */
            { PROCESS_SYS_MODEL, AG_OUT_GP2_COMMAND                },   /* gKPM_bus.rCommand                                        */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },   /* gFunction_active.eAccyVelDir                             */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_MOTOR_SPD              },   /* gPowerbase_fb.Powerbase_fb_tcm1.rMotorSpeed              */
            { PROCESS_SYS_MODEL, AG_OUT_SCM_STRCMD1                },   /* gSteer_inputs.slStrCmd1                                  */
            { PROCESS_SYS_MODEL, AG_OUT_TRC_SPD_CMD                },   /* gSteer_m2m_outputs.rTrxSpdLmtWhAngCmd                    */
            { PROCESS_SYS_MODEL, AG_OUT_STPTRC_OKCHK               },   /* gSteer_dx_out_bus.fIsStopTractCrossCkDetect              */
            { PROCESS_SYS_MODEL, AG_OUT_NOT_USED                   },    /* gHyd_output.ulSV_SCV                                     */
            { PROCESS_SYS_MODEL, AG_OUT_SUPV_GP5_COMMAND           }    /* gsbOprAlrmState                                     */
        }
    },
    {   /* ulDebounceTime, eDebounce, eInvertOutput, ulProcRate, eUseWC              SDI_SwitchDriverTruckConfig[]             */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV1]  */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV2]  */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV3]  */
        {  10000u,         SDB_BOTH,  SM_FALSE,      5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV4]  */
        {  10000u,         SDB_BOTH,  SM_FALSE,      5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV5]  */
        {  10000u,         SDB_BOTH,  SM_FALSE,      5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV6]  */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV7]  */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV8]  */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV9]  */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV10] */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV11] */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV12] */
        {  10000u,         SDB_BOTH,  SM_TRUE,       5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV13] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_TRUE  },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV14] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV15] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV16] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV17] */
        {  10000u,         SDB_BOTH,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV18] */
        {  10000u,         SDB_BOTH,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV19] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV20] */
        {  10000u,         SDB_NONE,  SM_TRUE,       5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV21] */
        {  10000u,         SDB_NONE,  SM_TRUE,       5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV22] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV23] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV24] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV25] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV26] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV27] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV28] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV29] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV30] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV31] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV32] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV33] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV34] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV35] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV36] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_FALSE },      /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV37] */
        {  10000u,         SDB_NONE,  SM_FALSE,      5000u,      SM_TRUE  }       /* SDI_SwitchDriverTruckConfig[SDI_SW_DRV38] */
    },
    8u,                                                         /* ulIO_THREAD_PREEMPT_THRESH */
    5u,                                                         /* ulIO_THREAD_TICK_RATE      */
    {                                                           /* supv_io_configuration */
        {                                                       /* pot_config_params_t POT2 */
            VOLTAGE_SELECT_5V                                   /* voltage_select_t eVoltageSelect */
        },
        {                                                       /* pot_config_params_t POT10 */
            VOLTAGE_SELECT_5V                                   /* voltage_select_t eVoltageSelect */
        },
        {                                                       /* encoder_config_params_t ENC2 */
            TRUE,                                               /* uint32_t ulEncoderEnabled */
            TRUE,                                               /* uint32_t ulCHaPullupEnabled */
            ANALOG_INPUT_CONFIG_5V,                             /* analog_input_configuration_t eCHaAnalogInputConfig */
            TRUE,                                               /* uint32_t ulCHbPullupEnabled */
            ANALOG_INPUT_CONFIG_5V                              /* analog_input_configuration_t eCHbAnalogInputConfig */
        },
        {                                                       /* switch_params_t PDS2           */
            {
                SM_DIGITAL,                                     /* swi_mode_t eMode               */
                1.0f,                                           /* float32_t rHighThresh          */
                0.0f,                                           /* float32_t rLowThresh           */
                0.0f,                                           /* float32_t rHysteresis          */
                32000u                                          /* uint32_t ulWCMaxTime           */
            },
            {
                10000u,                                         /* uint32_t ulDebounceTime        */
                SDB_NONE,                                       /* swi_debounce_t eDebounce       */
                SM_FALSE,                                       /* swi_true_false_t eInvertOutput */
                5000u,                                          /* uint32_t ulProcRate            */
                SM_TRUE                                         /* swi_true_false_t eUseWC        */ 
            }
        },
        {                                                       /* switch_params_t SW7            */
            {
                SM_ANALOG,                                      /* swi_mode_t eMode               */
                1.968f,                                         /* float32_t rHighThresh          */
                1.003f,                                         /* float32_t rLowThresh           */
                0.100f,                                         /* float32_t rHysteresis          */
                32000u                                          /* uint32_t ulWCMaxTime           */
            },
            {
                10000u,                                         /* uint32_t ulDebounceTime        */
                SDB_NONE,                                       /* swi_debounce_t eDebounce       */
                SM_FALSE,                                       /* swi_true_false_t eInvertOutput */
                5000u,                                          /* uint32_t ulProcRate            */
                SM_TRUE                                         /* swi_true_false_t eUseWC        */ 
            }
        },
        {                                                       /* switch_params_t SW9            */
            {
                SM_ANALOG,                                      /* swi_mode_t eMode               */
                1.968f,                                         /* float32_t rHighThresh          */
                1.003f,                                         /* float32_t rLowThresh           */
                0.100f,                                         /* float32_t rHysteresis          */
                32000u                                          /* uint32_t ulWCMaxTime           */
            },
            {
                10000u,                                         /* uint32_t ulDebounceTime        */
                SDB_NONE,                                       /* swi_debounce_t eDebounce       */
                SM_FALSE,                                       /* swi_true_false_t eInvertOutput */
                5000u,                                          /* uint32_t ulProcRate            */
                SM_TRUE                                         /* swi_true_false_t eUseWC        */ 
            }
        },
        {                                                       /* switch_params_t SW11           */
            {
                SM_ANALOG,                                      /* swi_mode_t eMode               */
                1.968f,                                         /* float32_t rHighThresh          */
                1.003f,                                         /* float32_t rLowThresh           */
                0.100f,                                         /* float32_t rHysteresis          */
                32000u                                          /* uint32_t ulWCMaxTime           */
            },
            {
                10000u,                                         /* uint32_t ulDebounceTime        */
                SDB_NONE,                                       /* swi_debounce_t eDebounce       */
                SM_FALSE,                                       /* swi_true_false_t eInvertOutput */
                5000u,                                          /* uint32_t ulProcRate            */
                SM_TRUE                                         /* swi_true_false_t eUseWC        */ 
            }
        },
        {                                                       /* ps_config_params_t PS2 */
            PS2_ENABLE,                                         /* uint32_t ulEnabled */
            VOLTAGE_SELECT_12V                                  /* voltage_select_t eVoltageSelect */
        },
        {                                                       /* XcpConfig */
            CAN_PORTB,                                          /* supv_xcp_port_t ePortSelect  */
            CAN_BAUD_500,                                       /* supv_xcp_baud_t eXcpBaudRate */
            FALSE                                               /* boolean fSaveChanges         */
        }
    },
    {   /* ulPDO_Number, ulCANline,      ulThread                PDO_ListConfig[]     */
        {  1u,           USR_k_CANLINE0, PROCESS_SYS_MODEL }, /* 0                    */
        {  5u,           USR_k_CANLINE0, PROCESS_SYS_MODEL }, /* 1                    */
        {  2u,           USR_k_CANLINE0, PROCESS_SYS_MODEL }, /* 2                    */
        {  3u,           USR_k_CANLINE0, PROCESS_SYS_MODEL }, /* 3                    */
        {  4u,           USR_k_CANLINE0, PROCESS_SYS_MODEL }, /* 4                    */
        { 12u,           USR_k_CANLINE0, PROCESS_INFOLINK  }, /* 5                    */
        { 13u,           USR_k_CANLINE0, PROCESS_INFOLINK  }, /* 6                    */
        { 14u,           USR_k_CANLINE0, PROCESS_INFOLINK  }, /* 7                    */
        { 15u,           USR_k_CANLINE0, PROCESS_INFOLINK  }, /* 8                    */
        { 16u,           USR_k_CANLINE0, PROCESS_SYS_MODEL }, /* 9                    */
        { 17u,           USR_k_CANLINE0, PROCESS_IO        }, /* 10                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 11                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 12                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 13                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 14                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 15                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 16                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 17                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 18                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 19                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 20                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 21                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }, /* 22                    */
        {  0u,           USR_k_CANLINE0, PROCESS_NONE      }  /* 23                    */
    },
    {   /* ulThread                                              Freshness_Config[]   */
        {  PROCESS_SYS_MODEL },                               /* 0                    */
        {  PROCESS_SYS_MODEL },                               /* 1                    */
        {  PROCESS_SYS_MODEL },                               /* 2                    */
        {  PROCESS_SYS_MODEL },                               /* 3                    */
        {  PROCESS_NONE      }                                /* 4                    */
    },
    4500,                                                     /* int32_t slMaxSteerSpeed            */
    2u,                                                       /* uint32_t ulSteerCalDataRevision    */
    1u,                                                       /* uint32_t ulTractionCalDataRevision */
    2u,                                                       /* uint32_t ul420mAInputCalDataRevision */
    0u,                                                       /* uint32_t ulSensorCalDataRevision */
    0u,                                                       /* uint32_t ulValveCalDataRevision */
    0u,                                                       /* uint32_t ulAutoCalStatusCalDataRevision*/
    ACCEL_Z_ORIENTATION,                                      /* eDefaultAccelOrientation */
    {                                                         /* Input_4_20_mA_Config */
        1u,                                                   /* ulUseFlashSettings   */
        {
            {1u, 1u},                                         /* SF_Inputs[0] */
            {2u, 1u},                                         /* SF_Inputs[1] */
            {0u, 0u},                                         /* SF_Inputs[2] */
            {0u, 0u}                                          /* SF_Inputs[3] */
        },
        {
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[0]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[1]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[2]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[3]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[4]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[5]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[6]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[7]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[8]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[9]  */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[10] */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[11] */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[12] */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[13] */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}, /* Scale[14] */
            {(250.0f * 6.89476729f), (-1000.0f * 6.89476729f)}  /* Scale[15] */
        }
    },
    {                                                         /* AccumDataMeterDefaults */
        0u,                                                   /* ulRevision */
        {
            0u,                                               /* ulMeterVals[0]  */
            0u,                                               /* ulMeterVals[1]  */
            0u,                                               /* ulMeterVals[2]  */
            0u,                                               /* ulMeterVals[3]  */
            0u,                                               /* ulMeterVals[4]  */
            0u,                                               /* ulMeterVals[5]  */
            0u,                                               /* ulMeterVals[6]  */
            0u,                                               /* ulMeterVals[7]  */
            0u,                                               /* ulMeterVals[8]  */
            0u,                                               /* ulMeterVals[9]  */
            0u,                                               /* ulMeterVals[10] */
            0u,                                               /* ulMeterVals[11] */
            0u,                                               /* ulMeterVals[12] */
            0u,                                               /* ulMeterVals[13] */
            0u,                                               /* ulMeterVals[14] */
            0u,                                               /* ulMeterVals[15] */
            0u,                                               /* ulMeterVals[16] */
            0u,                                               /* ulMeterVals[17] */
            0u,                                               /* ulMeterVals[18] */
            0u,                                               /* ulMeterVals[19] */
            0u,                                               /* ulMeterVals[20] */
            0u,                                               /* ulMeterVals[21] */
            0u,                                               /* ulMeterVals[22] */
            0u,                                               /* ulMeterVals[23] */
            0u,                                               /* ulMeterVals[24] */
            0u,                                               /* ulMeterVals[25] */
            0u,                                               /* ulMeterVals[26] */
            0u,                                               /* ulMeterVals[27] */
            0u,                                               /* ulMeterVals[28] */
            0u,                                               /* ulMeterVals[29] */
            0u,                                               /* ulMeterVals[30] */
            0u                                                /* ulMeterVals[31] */
        }
    },
    {
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[0].eType  */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[1].eType  */
        AT_SET_VAL,                                           /* AccumDataMeterTypeDefaults[2].eType  */
        AT_SET_VAL,                                           /* AccumDataMeterTypeDefaults[3].eType  */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[4].eType  */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[5].eType  */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[6].eType  */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[7].eType  */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[8].eType  */
        AT_ADDER,                                             /* AccumDataMeterTypeDefaults[9].eType  */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[10].eType */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[11].eType */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[12].eType */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[13].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[14].eType */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[15].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[16].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[17].eType */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[18].eType */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[19].eType */
        AT_TIMER,                                             /* AccumDataMeterTypeDefaults[20].eType */
        AT_ADDER_CLR,                                         /* AccumDataMeterTypeDefaults[21].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[22].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[23].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[24].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[25].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[26].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[27].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[28].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[29].eType */
        AT_NOT_USED,                                          /* AccumDataMeterTypeDefaults[30].eType */
        AT_NOT_USED                                           /* AccumDataMeterTypeDefaults[31].eType */
    },
    0.05f,                                                    /* rTractionZeroSpeed */
    50.0f,                                                    /* rTruckRuntimeNomPowerThresh */
    {                                                         /* QuadratureAbsoluteParameters[0] */
        0u,                                                   /* ulNumber */
        0u,                                                   /* ulEnable */
        QUAD_ABS_CH_B_FIRST,                                  /* ulLeadingCh */
        65,                                                   /* slMarkerPosition_1 */
        31,                                                   /* slMarkerPosition_4 */
        QUAD_ABS_1_SIG,                                       /* ulDirectionChangeMode */
        2u                                                    /* ulDirectionChangeNeutralValue */
    },
    100u,                                                     /* ulDemReportThreadSuspendTime */
    1000u,                                                    /* ulDemNvWriteRate */
    0u,                                                       /* ulTmlHwTestModeEnabled */
    {   /* eDriverEnum          ulCAN_port      ulNode_ID ulMonitor ulErrorNumber_init                                              ulErrorNumber_config                         ulErrorNumber_startup                                              eBaseAddr[]                                        */
        { CODRVSEL_AMC_001,     USR_k_CANLINE0, 0x60u,    TRUE,     DEMEV_VCM_ACCESS3_1_INVALID_MODULE_INITIALIZATION_ERROR,        DEMEV_VCM_ACCESS3_1_SDO_CONFIGURATION_ERROR, DEMEV_VCM_ACCESS3_1_PREOP_TO_OP_STATE_STARTUP_ERROR,               {CO_BASEADDR_PARAM_TCM_1, CO_BASEADDR_NULL   } },
        { CODRVSEL_SCM_001,     USR_k_CANLINE0, 0x06u,    TRUE,     DEMEV_VCM_ACCESS5_1_INVALID_MODULE_INITIALIZATION_ERROR,        DEMEV_VCM_ACCESS5_1_SDO_CONFIGURATION_ERROR, DEMEV_VCM_ACCESS5_1_PREOP_TO_OP_STATE_STARTUP_ERROR,               {CO_BASEADDR_NULL,        CO_BASEADDR_PD_DATA} },
        { CODRVSEL_SCMSLV_001,  USR_k_CANLINE0, 0x09u,    TRUE,     DEMEV_VCM_ACCESS5_1_INVALID_MODULE_INITIALIZATION_ERROR,        DEMEV_VCM_ACCESS5_1_SDO_CONFIGURATION_ERROR, DEMEV_VCM_ACCESS5_1_PREOP_TO_OP_STATE_STARTUP_ERROR,               {CO_BASEADDR_NULL,        CO_BASEADDR_NULL   } },
        { CODRVSEL_X10_001,     USR_k_CANLINE0, 0x50u,    TRUE,     DEMEV_VCM_X10_INVALID_MODULE_INITIALIZATION_ERROR,              DEMEV_VCM_X10_SDO_CONFIGURATION_ERROR,       DEMEV_VCM_X10_PREOP_TO_OP_STATE_STARTUP_ERROR,                     {CO_BASEADDR_NULL,        CO_BASEADDR_NULL   } },
        { CODRVSEL_SUP_001,     USR_k_CANLINE0, 0x33u,    TRUE,     DEMEV_VCM_ACCESS4_1_MICRO2_INVALID_MODULE_INITIALIZATION_ERROR, DEMEV_VCM_ACCESS4_1_MICRO2_SDO_CONFIGURATION_ERROR, DEMEV_VCM_ACCESS4_1_MICRO2_PREOP_TO_OP_STATE_STARTUP_ERROR, {CO_BASEADDR_NULL,        CO_BASEADDR_NULL   } },
        { CODRVSEL_NONE,        USR_k_CANLINE0, 0x00u,    FALSE,    0u,                                                             0u,                                          0u,                                                                {CO_BASEADDR_NULL,        CO_BASEADDR_NULL   } },
        { CODRVSEL_NONE,        USR_k_CANLINE0, 0x00u,    FALSE,    0u,                                                             0u,                                          0u,                                                                {CO_BASEADDR_NULL,        CO_BASEADDR_NULL   } },
        { CODRVSEL_NONE,        USR_k_CANLINE0, 0x00u,    FALSE,    0u,                                                             0u,                                          0u,                                                                {CO_BASEADDR_NULL,        CO_BASEADDR_NULL   } },
        { CODRVSEL_NONE,        USR_k_CANLINE0, 0x00u,    FALSE,    0u,                                                             0u,                                          0u,                                                                {CO_BASEADDR_NULL,        CO_BASEADDR_NULL   } },
        { CODRVSEL_NONE,        USR_k_CANLINE0, 0x00u,    FALSE,    0u,                                                             0u,                                          0u,                                                                {CO_BASEADDR_NULL,        CO_BASEADDR_NULL   } }
    },
    24.0f,                                                    /* rKPM_OnCommand */
    GPIO_STATE_HIGH,                                          /* ePS5_Shutdown */
    GPIO_STATE_HIGH,                                          /* ePS5_VoltageSelect */
    GPIO_STATE_HIGH,                                          /* ePS6_Shutdown */
    GPIO_STATE_HIGH,                                          /* ePS6_VoltageSelect */
    GPIO_STATE_LOW,                                           /* eQuad3_PullupsEnable */
    GPIO_STATE_LOW,                                           /* eSupv_Synchronization */
    10u,                                                      /* ulSupv_Synchronization_Rate */
    GYRO_X_AXIS,                                              /* eDefaultGyroOrientation */

#endif  /* #ifndef PARAM_TRUCK_SPECIFIC_H */
