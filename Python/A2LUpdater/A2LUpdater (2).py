# Name:     A2LUpdater.py
#
# Purpose:   This python script will copy Most current 161720-xxx-xx.a2l from Truck download zip file to IOModel/database for HIL 
#
# Author:    Thomas Rode
#
# Options:   None
#
# Note:
#
# Python Version: 3.4
#          
# Reference: https://www.python.org/ (open source)
#

import os
import glob
import time
from zipfile import ZipFile
import shutil
#from tkinter import Tk, Label, Button
from tkinter import Tk, Label, Button

import subprocess

TruckDownLoadDir = 'C:/Users/d0020452/AppData/Local/IMMDownloadUtil/app-0.10.2/resources/app/local_file_cache/ilmd_cache/'  #'C:/Users/t0028919/Desktop/Linker stuff'  
A2LFilePrefix = '161720-'
A2LPostFix = '.a2l'
CurrentA2LDst = 'C:/repos/HIL/Simulation/IOModel/database/'
DownLoadBundlePrefix = '154620'
A2LUpdaterMScriptDir = 'C:/repos/HIL/Simulation/IOModel/Ryder_5700RM6K/'
#DataBaseA2L = "C:\repos\HIL\Simulation\IOModel\database\"

class A2LUpdater:
    def __init__(self, master):
        self.master = master
        master.title("A2L Update")

        # New GUI dimensions
        root.geometry("200x200")

        # Set up GUI Label
        self.label = Label(master, text="Choose action!")
        self.label.pack()

        # Set up GUI Buttons
        self.greet_button = Button(master, text=" Change A2L Only ", command=self.changeA2Lonly,  width = 25)
        self.greet_button.pack()

        self.close_button = Button(master, text="Change A2L + Load Real-Time", command=self.changeA2Landload4realtime,   width = 25 )
        self.close_button.pack()

        self.close_button = Button(master, text="Quit", command=master.quit)
        self.close_button.pack()

    def changeA2Lonly(self):

        print("in changeA2Lonly")
        # Change directory to the Truck bundle download folder 
        os.chdir(TruckDownLoadDir)
        
        # Get all zipped folders specified within directory 
        items = os.listdir(".")
        newlist = []
        for names in items:
            if names.endswith(".zip"):
                newlist.append(names)
        #print (newlist)
        
        # Check to see which folder is the newest
        latest_zipped_subdir = max(newlist, key=os.path.getmtime)

        # Mask off prefix (167720-) and the .zip extension, ex. 167720-821-02.zip
        latest_zipped_subdir_masked = latest_zipped_subdir[7:13]
        
        #print (latest_zipped_subdir_masked )
       
        # Opening the zip file in READ mode       
        with ZipFile(latest_zipped_subdir, 'r') as zip:
            
            # printing all the contents of the zip file
            #zip.printdir()
            
            # extracting all the files
            print('Extracting all the files now...')
            zip.extractall()
            

        #Open extracted folder, pick out A2L Src to dst
        shutil.copy(TruckDownLoadDir  + (DownLoadBundlePrefix + '-' + latest_zipped_subdir_masked)+ '/'  +(A2LFilePrefix + latest_zipped_subdir_masked + A2LPostFix), (CurrentA2LDst + (A2LFilePrefix + latest_zipped_subdir_masked + A2LPostFix)) )

        print ( (A2LFilePrefix + latest_zipped_subdir_masked + A2LPostFix) , 'Is now copied at location ' + CurrentA2LDst) 
	       

        # Change directory to A2LUpdater.m Script
        os.chdir(A2LUpdaterMScriptDir)

        # Make a file and save the complete A2L name here
        file = open('CurrentA2LName.txt','w')
        file.write( A2LFilePrefix + latest_zipped_subdir_masked + A2LPostFix)
        file.close()
        print('>>>Updated CurrentA2LName.txt')

        # Call mscript to be constructed
        print(">>> Calling mscript!")

        # Call it 
        os.system("RunA2LUpdaterMscript.bat") 
        
       

    def changeA2Landload4realtime(self):
        print("Keep Same A2L!")

        # Change A2L rebuild model 
        #self.changeA2Lonly()

        # Change directory to A2LUpdater.m Script
        os.chdir('C:/MATLAB_MSCRIPT_TEST/')

        # Make a file and lock out until the Mscript completes
        file = open('A2LUpdateMscriptProgress.txt','w')
        file.write('A2LUpdaterInProgress')
        file.close()

        # Call it 
        os.system("RunA2LUpdaterMscript.bat")

        # Open the file again and test in While Loop
        file = open('A2LUpdateMscriptProgress.txt', 'r')
         
        # Wait until Mscript completes then Start Control Desk
        while (file.readline() !=  'A2LUpdaterComplete'):

            print("RunA2LUpdaterMscript.bat ----> Process In-Progress")

            # Add some delay then open file and test again
            time.sleep(.5)

            # Open the file again and test in While Loop
            file = open('A2LUpdateMscriptProgress.txt', 'r')


        print("RunA2LUpdaterMscript.bat ----> Process complete\n")
        print("Open control Desk ----> Process In-Progress\n")
        

root = Tk()
my_gui = A2LUpdater(root)
root.mainloop()
