
IgnoreList = ['Proj_C_Src/',
              'Proj_C_Headers/',
              'Proj_D_Src/Proj_D_Sub1/Proj_D_SubSub1/'
             ] 

print("Adding .gitignore")

# write file 
with open('.gitignore', 'w') as f:
    f.write('\n'.join(IgnoreList))

"""
f = open('TestScript.txt', "w")
f.writelines(List)
f.close()
"""
