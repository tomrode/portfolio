/***********************************************
 *      VectorCAST Test Harness Component      *
 *     Copyright 2018 Vector Software, Inc.    *
 *               6.4p (01/31/17)               *
 ***********************************************/
/***********************************************
 * VectorCAST Unit Information
 *
 * Name: imm_interface
 *
 * Path: C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/VCM_APP/src/general/imm_interface.c
 *
 * Type: stub-by-function
 *
 * Unit Number: 12
 *
 ***********************************************/
#ifndef VCAST_DRIVER_ONLY
/* Include the file which contains function prototypes
for stub processing and value/expected user code */
#include "vcast_uc_prototypes.h"
#include "vcast_basics.h"
/* STUB_DEPENDENCY_USER_CODE */
/* STUB_DEPENDENCY_USER_CODE_END */
#else
#include "vcast_env_defines.h"
#define __VCAST_BASICS_H__
#endif /* VCAST_DRIVER_ONLY */
#ifndef VCAST_DRIVER_ONLY
#ifndef VCAST_DONT_RENAME_EXIT
#ifdef __cplusplus
extern "C" {
#endif
void exit (int status);
#ifdef __cplusplus
}
#endif
/* used to capture the exit call */
#define exit VCAST_exit
#endif /* VCAST_DONT_RENAME_EXIT */
#endif /* VCAST_DRIVER_ONLY */
#ifndef VCAST_DRIVER_ONLY
#define VCAST_HEADER_EXPANSION
#ifdef VCAST_COVERAGE
#include "imm_interface_inst_prefix.c"
#else
#include "imm_interface_vcast_prefix.c"
#endif
#ifdef VCAST_COVERAGE
/* If coverage is enabled, include the instrumented UUT */
#include "imm_interface_inst.c"
#else
/* If coverage is not enabled, include the original UUT */
#include "imm_interface_vcast.c"
#endif
#ifdef VCAST_COVERAGE
#include "imm_interface_inst_appendix.c"
#else
#include "imm_interface_vcast_appendix.c"
#endif
#endif /* VCAST_DRIVER_ONLY */
#include "imm_interface_driver_prefix.c"
#ifdef VCAST_HEADER_EXPANSION
#ifdef VCAST_COVERAGE
#include "imm_interface_exp_inst_driver.c"
#else
#include "imm_interface_expanded_driver.c"
#endif /*VCAST_COVERAGE*/
#else
#include "S0000009.h"
#include "vcast_undef_12.h"
/* Include the file which contains function prototypes
for stub processing and value/expected user code */
#include "vcast_uc_prototypes.h"
/* Begin Function read_tbu */
long R_12_1;
/* Begin Function read_tbl */
long R_12_2;
#include "vcast_stubs_12.c"
/* begin declarations of inlined friends */
/* end declarations of inlined friends */
void VCAST_DRIVER_12( int VC_SUBPROGRAM, char *VC_EVENT_FLAGS, char *VC_SLOT_DESCR ) {
#ifdef VCAST_SBF_UNITS_AVAILABLE
  vCAST_MODIFY_SBF_TABLE(12, VC_SUBPROGRAM, vCAST_false);
#endif
  switch( VC_SUBPROGRAM ) {
    case 0:
      vCAST_SET_HISTORY_FLAGS ( 12, 0, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      break;
    case 1: {
      /* long read_tbu(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 1, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_12_1 = 
      ( read_tbu( ) );
      break; }
    case 2: {
      /* long read_tbl(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 2, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_12_2 = 
      ( read_tbl( ) );
      break; }
    case 3: {
      /* uint32_t gulIMM_CommunicationInitialize(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 3, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_12_3 = 
      ( gulIMM_CommunicationInitialize( ) );
      break; }
    case 4: {
      /* void gvIMM_SendSROMessage(uint32_t ulUpdateRateMs) */
      vCAST_SET_HISTORY_FLAGS ( 12, 4, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( gvIMM_SendSROMessage(
        ( P_12_4_1 ) ) );
      break; }
    case 5: {
      /* void gvIMM_SendTruckOperationMsg(uint32_t ulUpdateRateMs) */
      vCAST_SET_HISTORY_FLAGS ( 12, 5, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( gvIMM_SendTruckOperationMsg(
        ( P_12_5_1 ) ) );
      break; }
    case 6: {
      /* void vIMM_StatusReadCmdMessage_cb(uint16_t uwMessageID, char * pszMessage) */
      vCAST_SET_HISTORY_FLAGS ( 12, 6, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMM_StatusReadCmdMessage_cb(
        ( P_12_6_1 ),
        ( P_12_6_2 ) ) );
      break; }
    case 7: {
      /* void vIMM_WriteCmdMessage_cb(uint16_t uwMessageID, char * pszMessage) */
      vCAST_SET_HISTORY_FLAGS ( 12, 7, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMM_WriteCmdMessage_cb(
        ( P_12_7_1 ),
        ( P_12_7_2 ) ) );
      break; }
    case 8: {
      /* void gvIMM_SendCalibrationMessage(uint32_t ulUpdateRateMs) */
      vCAST_SET_HISTORY_FLAGS ( 12, 8, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( gvIMM_SendCalibrationMessage(
        ( P_12_8_1 ) ) );
      break; }
    case 9: {
      /* void vIMM_CalibReadCmdMessage_cb(uint16_t uwMessageID, char * pszMessage) */
      vCAST_SET_HISTORY_FLAGS ( 12, 9, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMM_CalibReadCmdMessage_cb(
        ( P_12_9_1 ),
        ( P_12_9_2 ) ) );
      break; }
    case 10: {
      /* void vIMM_CalibWriteCmdMessage_cb(uint16_t uwMessageID, char * pszMessage) */
      vCAST_SET_HISTORY_FLAGS ( 12, 10, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMM_CalibWriteCmdMessage_cb(
        ( P_12_10_1 ),
        ( P_12_10_2 ) ) );
      break; }
    case 11: {
      /* void vIMM_WriteRespCmdMessage_cb(uint16_t uwMessageID, char * pszMessage) */
      vCAST_SET_HISTORY_FLAGS ( 12, 11, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMM_WriteRespCmdMessage_cb(
        ( P_12_11_1 ),
        ( P_12_11_2 ) ) );
      break; }
    case 12: {
      /* void vIMM_FlagsCmdMessage_cb(uint16_t uwMessageID, char * pszMessage) */
      vCAST_SET_HISTORY_FLAGS ( 12, 12, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMM_FlagsCmdMessage_cb(
        ( P_12_12_1 ),
        ( P_12_12_2 ) ) );
      break; }
    case 13: {
      /* void vIMM_EventReport_cb(uint16_t uwMessageID, char * pszMessage) */
      vCAST_SET_HISTORY_FLAGS ( 12, 13, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMM_EventReport_cb(
        ( P_12_13_1 ),
        ( P_12_13_2 ) ) );
      break; }
    case 14: {
      /* boolean gfIMM_ConfigurationComplete(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 14, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_12_14 = 
      ( gfIMM_ConfigurationComplete( ) );
      break; }
    case 15: {
      /* boolean gfIMM_KpmEnabled(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 15, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_12_15 = 
      ( gfIMM_KpmEnabled( ) );
      break; }
    case 16: {
      /* boolean gfIMM_ShutdownCommanded(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 16, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_12_16 = 
      ( gfIMM_ShutdownCommanded( ) );
      break; }
    case 17: {
      /* void gvIMM_SetStatusBits(uint32_t ulBitmask, boolean fSetBits) */
      vCAST_SET_HISTORY_FLAGS ( 12, 17, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( gvIMM_SetStatusBits(
        ( P_12_17_1 ),
        ( P_12_17_2 ) ) );
      break; }
    case 18: {
      /* void gvIMM_ProcessIpsPDO1(ips_index_t eIpsIndex, uint8_t * pubDataBytes) */
      vCAST_SET_HISTORY_FLAGS ( 12, 18, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( gvIMM_ProcessIpsPDO1(
        ( P_12_18_1 ),
        ( P_12_18_2 ) ) );
      break; }
    case 19: {
      /* void gvIMM_ProcessIpsPDO2(ips_index_t eIpsIndex, uint8_t * pubDataBytes) */
      vCAST_SET_HISTORY_FLAGS ( 12, 19, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( gvIMM_ProcessIpsPDO2(
        ( P_12_19_1 ),
        ( P_12_19_2 ) ) );
      break; }
    case 20: {
      /* void vIMM_IpsBeginNotification(ips_index_t eIpsIndex, uint8_t ubEventNum, boolean fLockout) */
      vCAST_SET_HISTORY_FLAGS ( 12, 20, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMM_IpsBeginNotification(
        ( P_12_20_1 ),
        ( P_12_20_2 ),
        ( P_12_20_3 ) ) );
      break; }
    case 21: {
      /* void gvIMM_IpsProcessNotification(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 21, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( gvIMM_IpsProcessNotification( ) );
      break; }
    case 22: {
      /* void gvIMM_SendStreamingMsgs(uint16_t uwUpdateRateMs) */
      vCAST_SET_HISTORY_FLAGS ( 12, 22, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( gvIMM_SendStreamingMsgs(
        ( P_12_22_1 ) ) );
      break; }
    case 23: {
      /* boolean gfIMM_ShutDownEventsLoggingInvalid(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 23, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_12_23 = 
      ( gfIMM_ShutDownEventsLoggingInvalid( ) );
      break; }
    case 24: {
      /* void gvIMM_SendAHSMsg(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 24, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( gvIMM_SendAHSMsg( ) );
      break; }
    case 25: {
      /* boolean gfIMM_811_AlarmRequested(void) */
      vCAST_SET_HISTORY_FLAGS ( 12, 25, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_12_25 = 
      ( gfIMM_811_AlarmRequested( ) );
      break; }
    default:
      vectorcast_print_string("ERROR: Internal Tool Error\n");
      break;
  } /* switch */
}

void VCAST_SBF_12( int VC_SUBPROGRAM ) {
  switch( VC_SUBPROGRAM ) {
    case 3: {
      SBF_12_3 = 0;
      break; }
    case 4: {
      SBF_12_4 = 0;
      break; }
    case 5: {
      SBF_12_5 = 0;
      break; }
    case 6: {
      SBF_12_6 = 0;
      break; }
    case 7: {
      SBF_12_7 = 0;
      break; }
    case 8: {
      SBF_12_8 = 0;
      break; }
    case 9: {
      SBF_12_9 = 0;
      break; }
    case 10: {
      SBF_12_10 = 0;
      break; }
    case 11: {
      SBF_12_11 = 0;
      break; }
    case 12: {
      SBF_12_12 = 0;
      break; }
    case 13: {
      SBF_12_13 = 0;
      break; }
    case 14: {
      SBF_12_14 = 0;
      break; }
    case 15: {
      SBF_12_15 = 0;
      break; }
    case 16: {
      SBF_12_16 = 0;
      break; }
    case 17: {
      SBF_12_17 = 0;
      break; }
    case 18: {
      SBF_12_18 = 0;
      break; }
    case 19: {
      SBF_12_19 = 0;
      break; }
    case 20: {
      SBF_12_20 = 0;
      break; }
    case 21: {
      SBF_12_21 = 0;
      break; }
    case 22: {
      SBF_12_22 = 0;
      break; }
    case 23: {
      SBF_12_23 = 0;
      break; }
    case 24: {
      SBF_12_24 = 0;
      break; }
    case 25: {
      SBF_12_25 = 0;
      break; }
    default:
      break;
  } /* switch */
}
#include "vcast_ti_decls_12.h"
void VCAST_RUN_DATA_IF_12( int VCAST_SUB_INDEX, int VCAST_PARAM_INDEX ) {
  switch ( VCAST_SUB_INDEX ) {
    case 0: /* for global objects */
      switch( VCAST_PARAM_INDEX ) {
        case 56: /* for global object gDEM_ReportStructure */
          VCAST_TI_12_265 ( &(gDEM_ReportStructure));
          break;
        case 57: /* for global object gAnalog_in_monitor_bus */
          VCAST_TI_12_822 ( &(gAnalog_in_monitor_bus));
          break;
        case 58: /* for global object gCurrent_mon_bus */
          VCAST_TI_12_824 ( &(gCurrent_mon_bus));
          break;
        case 59: /* for global object gFeatures_bus */
          VCAST_TI_12_10 ( &(gFeatures_bus));
          break;
        case 60: /* for global object gPowerbase_fb */
          VCAST_TI_12_806 ( &(gPowerbase_fb));
          break;
        case 61: /* for global object gSteer_inputs */
          VCAST_TI_12_818 ( &(gSteer_inputs));
          break;
        case 62: /* for global object gSteering_fdbk */
          VCAST_TI_12_810 ( &(gSteering_fdbk));
          break;
        case 63: /* for global object gThrottleIn */
          VCAST_TI_12_814 ( &(gThrottleIn));
          break;
        case 64: /* for global object gVoltage_mon_bus */
          VCAST_TI_12_820 ( &(gVoltage_mon_bus));
          break;
        case 65: /* for global object gVehicle_fb_c2m_bus */
          VCAST_TI_12_816 ( &(gVehicle_fb_c2m_bus));
          break;
        case 66: /* for global object gAutoCalImmStat */
          VCAST_TI_12_5 ( &(gAutoCalImmStat));
          break;
        case 67: /* for global object gPerformance_bus */
          VCAST_TI_12_513 ( &(gPerformance_bus));
          break;
        case 68: /* for global object gTestOutputInputs */
          VCAST_TI_12_843 ( &(gTestOutputInputs));
          break;
        case 69: /* for global object gHourMeterInput */
          VCAST_TI_12_841 ( &(gHourMeterInput));
          break;
        case 70: /* for global object gRack_Status */
          VCAST_TI_12_22 ( &(gRack_Status));
          break;
        case 71: /* for global object gsbTravelDirection */
          VCAST_TI_12_25 ( &(gsbTravelDirection));
          break;
        case 72: /* for global object gAccumDataMeters */
          VCAST_TI_12_888 ( &(gAccumDataMeters));
          break;
        case 73: /* for global object gVehicle_Manifest */
          VCAST_TI_12_966 ( &(gVehicle_Manifest));
          break;
        case 74: /* for global object geSoundID */
          VCAST_TI_12_1000 ( &(geSoundID));
          break;
        case 75: /* for global object gEnergySource_output */
          VCAST_TI_12_1058 ( &(gEnergySource_output));
          break;
        case 76: /* for global object gFunction_active */
          VCAST_TI_12_1060 ( &(gFunction_active));
          break;
        case 77: /* for global object gHyd_output */
          VCAST_TI_12_1062 ( &(gHyd_output));
          break;
        case 78: /* for global object gSteer_m2m_outputs */
          VCAST_TI_12_1064 ( &(gSteer_m2m_outputs));
          break;
        case 79: /* for global object gSw_flag_outputs */
          VCAST_TI_12_1066 ( &(gSw_flag_outputs));
          break;
        case 80: /* for global object gTraction_output */
          VCAST_TI_12_1068 ( &(gTraction_output));
          break;
        case 81: /* for global object gvehicle_sro_word */
          VCAST_TI_12_1070 ( &(gvehicle_sro_word));
          break;
        case 82: /* for global object gVehicleFB_output */
          VCAST_TI_12_1072 ( &(gVehicleFB_output));
          break;
        case 83: /* for global object gBrake_analyzer_out_bus */
          VCAST_TI_12_1074 ( &(gBrake_analyzer_out_bus));
          break;
        case 84: /* for global object gSteer_dx_out_bus */
          VCAST_TI_12_1076 ( &(gSteer_dx_out_bus));
          break;
        case 85: /* for global object gAutoCalMccStat */
          VCAST_TI_12_1078 ( &(gAutoCalMccStat));
          break;
        case 86: /* for global object gAutoCalPvFormat */
          VCAST_TI_12_1080 ( &(gAutoCalPvFormat));
          break;
        case 87: /* for global object gAutoCalDataEntryFormat */
          VCAST_TI_12_1082 ( &(gAutoCalDataEntryFormat));
          break;
        case 88: /* for global object gAutoCalSteps */
          VCAST_TI_12_1084 ( &(gAutoCalSteps));
          break;
        case 89: /* for global object gAGV_output */
          VCAST_TI_12_1086 ( &(gAGV_output));
          break;
        case 90: /* for global object Info1_VehicleModel */
          VCAST_TI_12_1087 ( &(Info1_VehicleModel));
          break;
        case 91: /* for global object guwVcmStatusWord */
          VCAST_TI_9_16 ( &(guwVcmStatusWord));
          break;
        case 92: /* for global object gubKeyPressArmrestKeyboard */
          VCAST_TI_8_5 ( &(gubKeyPressArmrestKeyboard));
          break;
        case 93: /* for global object gsbKnobAsKeySigArmrestKeyboard */
          VCAST_TI_12_25 ( &(gsbKnobAsKeySigArmrestKeyboard));
          break;
        case 94: /* for global object gFRAM_AutoCalStatus */
          VCAST_TI_12_827 ( &(gFRAM_AutoCalStatus));
          break;
        case 95: /* for global object gFRAM_Usercutout_cals_c2m */
          VCAST_TI_12_839 ( &(gFRAM_Usercutout_cals_c2m));
          break;
        case 96: /* for global object gFRAM_RackSelectSetup_cals_c2m */
          VCAST_TI_12_831 ( &(gFRAM_RackSelectSetup_cals_c2m));
          break;
        case 97: /* for global object gFRAM_BirthCert */
          VCAST_TI_12_1093 ( &(gFRAM_BirthCert));
          break;
        case 2: /* for global object xf811_AlarmRequested */
          VCAST_TI_8_5 ( &(xf811_AlarmRequested));
          break;
        case 3: /* for global object xulIMM_bluetoothAddress */
          VCAST_TI_11_4 ( &(xulIMM_bluetoothAddress));
          break;
        case 4: /* for global object xubIpsPeakX */
          VCAST_TI_12_14 ( xubIpsPeakX);
          break;
        case 5: /* for global object xubIpsPeakY */
          VCAST_TI_12_14 ( xubIpsPeakY);
          break;
        case 6: /* for global object xubIpsPeakZ */
          VCAST_TI_12_14 ( xubIpsPeakZ);
          break;
        case 7: /* for global object xubIpsPeakImpulse */
          VCAST_TI_12_14 ( xubIpsPeakImpulse);
          break;
        case 8: /* for global object gIMMControl */
          VCAST_TI_12_993 ( &(gIMMControl));
          break;
        case 9: /* for global object gImpactSensorPdoData */
          VCAST_TI_12_11 ( gImpactSensorPdoData);
          break;
        case 10: /* for global object gubImpactSensorPdoVehicleData */
          VCAST_TI_12_1094 ( gubImpactSensorPdoVehicleData);
          break;
        case 11: /* for global object xrBvMin */
          VCAST_TI_8_3 ( &(xrBvMin));
          break;
        case 12: /* for global object xrBvMax */
          VCAST_TI_8_3 ( &(xrBvMax));
          break;
        case 13: /* for global object xszModelNumber */
          VCAST_TI_12_1095 ( xszModelNumber);
          break;
        case 14: /* for global object xfKpmUsed */
          VCAST_TI_8_5 ( &(xfKpmUsed));
          break;
        case 15: /* for global object xfConfigComplete */
          VCAST_TI_8_5 ( &(xfConfigComplete));
          break;
        case 16: /* for global object xubNumStreamMessages */
          VCAST_TI_8_5 ( &(xubNumStreamMessages));
          break;
        case 17: /* for global object xulStreamMessageEnables */
          VCAST_TI_11_4 ( &(xulStreamMessageEnables));
          break;
        case 18: /* for global object xuwStreamMessageRateMs */
          VCAST_TI_12_20 ( xuwStreamMessageRateMs);
          break;
        case 19: /* for global object xuwMaxStreamObjects */
          VCAST_TI_9_16 ( &(xuwMaxStreamObjects));
          break;
        case 20: /* for global object xulVehicleDataStatusFlags */
          VCAST_TI_11_4 ( &(xulVehicleDataStatusFlags));
          break;
        case 21: /* for global object xulObsoleteObject */
          VCAST_TI_11_4 ( &(xulObsoleteObject));
          break;
        case 22: /* for global object xIMMEventReport */
          VCAST_TI_12_265 ( &(xIMMEventReport));
          break;
        case 23: /* for global object xRack_Status */
          VCAST_TI_12_22 ( &(xRack_Status));
          break;
        case 24: /* for global object xullInitialNonlockoutImpactTime */
          VCAST_TI_12_19 ( &(xullInitialNonlockoutImpactTime));
          break;
        case 25: /* for global object xullInitialLockoutImpactTime */
          VCAST_TI_12_19 ( &(xullInitialLockoutImpactTime));
          break;
        case 26: /* for global object imm_SRO_msg_obj_list */
          VCAST_TI_12_1096 ( imm_SRO_msg_obj_list);
          break;
        case 27: /* for global object uwIMM_SRO_message_size */
          VCAST_TI_9_16 ( &(uwIMM_SRO_message_size));
          break;
        case 28: /* for global object imm_event_report_msg_obj_list */
          VCAST_TI_12_1097 ( imm_event_report_msg_obj_list);
          break;
        case 29: /* for global object uwIMM_event_report_message_size */
          VCAST_TI_9_16 ( &(uwIMM_event_report_message_size));
          break;
        case 30: /* for global object imm_event_ack_msg_obj_list */
          VCAST_TI_12_1098 ( imm_event_ack_msg_obj_list);
          break;
        case 31: /* for global object uwIMM_event_ack_message_size */
          VCAST_TI_9_16 ( &(uwIMM_event_ack_message_size));
          break;
        case 32: /* for global object imm_dem_report_msg_obj_list */
          VCAST_TI_12_1099 ( imm_dem_report_msg_obj_list);
          break;
        case 33: /* for global object uwIMM_dem_report_message_size */
          VCAST_TI_9_16 ( &(uwIMM_dem_report_message_size));
          break;
        case 34: /* for global object imm_dem_ack_msg_obj_list */
          VCAST_TI_12_1098 ( imm_dem_ack_msg_obj_list);
          break;
        case 35: /* for global object uwIMM_dem_ack_message_size */
          VCAST_TI_9_16 ( &(uwIMM_dem_ack_message_size));
          break;
        case 36: /* for global object imm_truck_op_obj_list */
          VCAST_TI_12_1098 ( imm_truck_op_obj_list);
          break;
        case 37: /* for global object uwIMM_truck_op_obj_list_size */
          VCAST_TI_9_16 ( &(uwIMM_truck_op_obj_list_size));
          break;
        case 38: /* for global object imm_command_msg_obj_list */
          VCAST_TI_12_3 ( imm_command_msg_obj_list);
          break;
        case 39: /* for global object uwIMM_command_message_size */
          VCAST_TI_9_16 ( &(uwIMM_command_message_size));
          break;
        case 40: /* for global object imm_calib_msg_obj_list */
          VCAST_TI_12_1102 ( imm_calib_msg_obj_list);
          break;
        case 41: /* for global object uwIMM_calib_msg_obj_list_size */
          VCAST_TI_9_16 ( &(uwIMM_calib_msg_obj_list_size));
          break;
        case 42: /* for global object imm_AHS_TX_msg_obj_list */
          VCAST_TI_12_1103 ( imm_AHS_TX_msg_obj_list);
          break;
        case 43: /* for global object uwIMM_AHSTX_msg_obj_list_size */
          VCAST_TI_9_16 ( &(uwIMM_AHSTX_msg_obj_list_size));
          break;
        case 44: /* for global object imm_AHS_GUI_msg_obj_list */
          VCAST_TI_12_1104 ( imm_AHS_GUI_msg_obj_list);
          break;
        case 45: /* for global object uwIMM_GUI_msg_obj_list_size */
          VCAST_TI_9_16 ( &(uwIMM_GUI_msg_obj_list_size));
          break;
        case 46: /* for global object uwIMM_calib_obj_access_list_size */
          VCAST_TI_9_16 ( &(uwIMM_calib_obj_access_list_size));
          break;
        case 47: /* for global object imm_calib_obj_access_list */
          VCAST_TI_12_1105 ( imm_calib_obj_access_list);
          break;
        case 48: /* for global object imm_armrest_keyboard_data_msg_obj_list */
          VCAST_TI_12_1106 ( imm_armrest_keyboard_data_msg_obj_list);
          break;
        case 49: /* for global object uwIMM_armrest_keyboard_data_msg_list_size */
          VCAST_TI_9_16 ( &(uwIMM_armrest_keyboard_data_msg_list_size));
          break;
        case 50: /* for global object imm_stream_msg_obj_list */
          VCAST_TI_12_1107 ( imm_stream_msg_obj_list);
          break;
        case 51: /* for global object uwIMM_stream_message_size */
          VCAST_TI_12_20 ( uwIMM_stream_message_size);
          break;
        case 52: /* for global object imm_vcm_command_obj_list */
          VCAST_TI_12_1110 ( imm_vcm_command_obj_list);
          break;
        case 53: /* for global object uwIMM_vcm_command_list_size */
          VCAST_TI_9_16 ( &(uwIMM_vcm_command_list_size));
          break;
        case 54: /* for global object imm_async_data_obj_list */
          VCAST_TI_12_1106 ( imm_async_data_obj_list);
          break;
        case 55: /* for global object uwIMM_async_data_list_size */
          VCAST_TI_9_16 ( &(uwIMM_async_data_list_size));
          break;
        default:
          vCAST_TOOL_ERROR = vCAST_true;
          break;
      } /* switch( VCAST_PARAM_INDEX ) */
      break; /* case 0 (global objects) */
    case 1: /* function read_tbu */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_25 ( &(R_12_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function read_tbu */
    case 2: /* function read_tbl */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_25 ( &(R_12_2));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function read_tbl */
    case 26: /* function gvDEM_ReportEventState */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_10_42_1));
          break;
        case 2:
          VCAST_TI_8_5 ( &(P_10_42_2));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvDEM_ReportEventState */
    case 27: /* function gvDEM_EventReportAcknowlege_cb */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_10_43_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_10_43_2));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvDEM_EventReportAcknowlege_cb */
    case 28: /* function gvAG_BeginImpactLockout */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_12_853 ( &(P_10_44_1));
          break;
        case 2:
          VCAST_TI_9_16 ( &(P_10_44_2));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvAG_BeginImpactLockout */
    case 29: /* function gvSUPV_sendBHMPairingCmd */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_10_45_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvSUPV_sendBHMPairingCmd */
    case 30: /* function gvAccumDataMeter_ImmUpdate_cb */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(P_10_46_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvAccumDataMeter_ImmUpdate_cb */
    case 31: /* function guwInfo1GetVcmStatusWord */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(R_10_47));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function guwInfo1GetVcmStatusWord */
    case 32: /* function gvNvMemory_SetBootSkipDlVal */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(P_10_48_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvNvMemory_SetBootSkipDlVal */
    case 34: /* function gullTimer_ReadTimer */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_12_19 ( &(R_10_50));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gullTimer_ReadTimer */
    case 35: /* function gulTimer_Counts_to_us */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_12_19 ( &(P_10_51_1));
          break;
        case 2:
          VCAST_TI_12_19 ( &(P_10_51_2));
          break;
        case 3:
          VCAST_TI_11_4 ( &(R_10_51));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulTimer_Counts_to_us */
    case 3: /* function gulIMM_CommunicationInitialize */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_4 ( &(R_12_3));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_3 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulIMM_CommunicationInitialize */
    case 4: /* function gvIMM_SendSROMessage */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_4 ( &(P_12_4_1));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_4 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIMM_SendSROMessage */
    case 5: /* function gvIMM_SendTruckOperationMsg */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_4 ( &(P_12_5_1));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_5 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIMM_SendTruckOperationMsg */
    case 6: /* function vIMM_StatusReadCmdMessage_cb */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_12_6_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_12_6_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_6 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMM_StatusReadCmdMessage_cb */
    case 7: /* function vIMM_WriteCmdMessage_cb */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_12_7_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_12_7_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_7 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMM_WriteCmdMessage_cb */
    case 8: /* function gvIMM_SendCalibrationMessage */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_4 ( &(P_12_8_1));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_8 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIMM_SendCalibrationMessage */
    case 9: /* function vIMM_CalibReadCmdMessage_cb */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_12_9_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_12_9_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_9 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMM_CalibReadCmdMessage_cb */
    case 10: /* function vIMM_CalibWriteCmdMessage_cb */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_12_10_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_12_10_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_10 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMM_CalibWriteCmdMessage_cb */
    case 11: /* function vIMM_WriteRespCmdMessage_cb */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_12_11_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_12_11_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_11 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMM_WriteRespCmdMessage_cb */
    case 12: /* function vIMM_FlagsCmdMessage_cb */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_12_12_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_12_12_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_12 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMM_FlagsCmdMessage_cb */
    case 13: /* function vIMM_EventReport_cb */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_12_13_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_12_13_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_13 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMM_EventReport_cb */
    case 14: /* function gfIMM_ConfigurationComplete */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(R_12_14));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_14 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfIMM_ConfigurationComplete */
    case 15: /* function gfIMM_KpmEnabled */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(R_12_15));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_15 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfIMM_KpmEnabled */
    case 16: /* function gfIMM_ShutdownCommanded */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(R_12_16));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_16 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfIMM_ShutdownCommanded */
    case 17: /* function gvIMM_SetStatusBits */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_4 ( &(P_12_17_1));
          break;
        case 2:
          VCAST_TI_8_5 ( &(P_12_17_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_17 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIMM_SetStatusBits */
    case 18: /* function gvIMM_ProcessIpsPDO1 */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_12_853 ( &(P_12_18_1));
          break;
        case 2:
          VCAST_TI_9_13 ( &(P_12_18_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_18 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIMM_ProcessIpsPDO1 */
    case 19: /* function gvIMM_ProcessIpsPDO2 */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_12_853 ( &(P_12_19_1));
          break;
        case 2:
          VCAST_TI_9_13 ( &(P_12_19_2));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_12_19 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIMM_ProcessIpsPDO2 */
    case 20: /* function vIMM_IpsBeginNotification */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_12_853 ( &(P_12_20_1));
          break;
        case 2:
          VCAST_TI_8_5 ( &(P_12_20_2));
          break;
        case 3:
          VCAST_TI_8_5 ( &(P_12_20_3));
          break;
        case 4:
          VCAST_TI_SBF_OBJECT( &SBF_12_20 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMM_IpsBeginNotification */
    case 21: /* function gvIMM_IpsProcessNotification */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_SBF_OBJECT( &SBF_12_21 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIMM_IpsProcessNotification */
    case 22: /* function gvIMM_SendStreamingMsgs */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_12_22_1));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_22 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIMM_SendStreamingMsgs */
    case 23: /* function gfIMM_ShutDownEventsLoggingInvalid */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(R_12_23));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_23 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfIMM_ShutDownEventsLoggingInvalid */
    case 24: /* function gvIMM_SendAHSMsg */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_SBF_OBJECT( &SBF_12_24 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIMM_SendAHSMsg */
    case 25: /* function gfIMM_811_AlarmRequested */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(R_12_25));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_12_25 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfIMM_811_AlarmRequested */
    default:
      vCAST_TOOL_ERROR = vCAST_true;
      break;
  } /* switch ( VCAST_SUB_INDEX ) */
}


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_265 ( struct dem_report_structure *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_265 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_265 ( struct dem_report_structure *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwNumberOfRecords */
      case 1: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwNumberOfRecords))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->uwSequenceNumberReported */
      case 2: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwSequenceNumberReported))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->uwSequenceNumberAcked */
      case 3: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwSequenceNumberAcked))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->Record */
      case 4: { 
        VCAST_TI_12_9 ( ((struct dem_report_record *)(vcast_param->Record)));
        break; /* end case 4*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_265 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_822 ( analog_in_monitor_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_822 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_822 ( analog_in_monitor_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rPot1 */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPot1))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rPot2 */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPot2))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rPot3 */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPot3))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rPot4 */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPot4))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rEnc3ChA */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rEnc3ChA))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rEnc3ChB */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rEnc3ChB))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rPot5 */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPot5))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->rPot6 */
      case 8: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPot6))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->rIomAna1 */
      case 9: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rIomAna1))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->rIomAna2 */
      case 10: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rIomAna2))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->rPot10 */
      case 11: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPot10))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->ubSOC_LastPowerDown_BHM */
      case 12: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubSOC_LastPowerDown_BHM))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->rBDIIndicator_BHM */
      case 13: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBDIIndicator_BHM))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->fEWS_Warning */
      case 14: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fEWS_Warning))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->rESS_FullPerformanceLevel */
      case 15: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rESS_FullPerformanceLevel))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->rESS_SOC */
      case 16: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rESS_SOC))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->rEWS_ShutdownPending */
      case 17: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rEWS_ShutdownPending))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->rESS_supply_Current */
      case 18: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rESS_supply_Current))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->swESS_supply_Temp */
      case 19: { 
        VCAST_TI_12_27 ( ((signed short *)(&(vcast_param->swESS_supply_Temp))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->fCanopen_BattPDOMissing */
      case 20: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fCanopen_BattPDOMissing))));
        break; /* end case 20*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_822 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_824 ( current_mon_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_824 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_824 ( current_mon_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rSv1Current */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv1Current))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rSv2Current */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv2Current))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rSv3Current */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv3Current))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rSv4Current */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv4Current))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rSv5Current */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv5Current))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rSv6Current */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv6Current))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rSv7Current */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv7Current))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->rSv8Current */
      case 8: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv8Current))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->rIBCurrent */
      case 9: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rIBCurrent))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->r4to20maCurrent */
      case 10: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r4to20maCurrent))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->rEdNegCurrent */
      case 11: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rEdNegCurrent))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->rOBCurrent */
      case 12: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rOBCurrent))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->rBrkCasterNegCurrent */
      case 13: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrkCasterNegCurrent))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->rGpd18Current */
      case 14: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGpd18Current))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->rGpd10Current */
      case 15: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGpd10Current))));
        break; /* end case 15*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_824 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_10 ( features_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_10 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_10 ( features_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->eHighSpeedLowerType */
      case 1: { 
        VCAST_TI_12_136 ( ((HighSpeedLowerType *)(&(vcast_param->eHighSpeedLowerType))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->eSteeringType */
      case 2: { 
        VCAST_TI_12_101 ( ((SteeringType *)(&(vcast_param->eSteeringType))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->eSteerDirection */
      case 3: { 
        VCAST_TI_12_152 ( ((SteerDirection *)(&(vcast_param->eSteerDirection))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->eAuxHoistType */
      case 4: { 
        VCAST_TI_12_45 ( ((AuxHoistType *)(&(vcast_param->eAuxHoistType))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rTRUCK_WEIGHT */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTRUCK_WEIGHT))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rCLPSD_HGT */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCLPSD_HGT))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rLIFT_HGT */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rLIFT_HGT))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->eZONESW */
      case 8: { 
        VCAST_TI_12_119 ( ((ZONE_SWITCH_TYPE *)(&(vcast_param->eZONESW))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->eRackSelect */
      case 9: { 
        VCAST_TI_12_87 ( ((RACK_SELECT_TYPE *)(&(vcast_param->eRackSelect))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->eUSeBatteryRestraintSwitch */
      case 10: { 
        VCAST_TI_12_79 ( ((Op_Int_Mask_Type *)(&(vcast_param->eUSeBatteryRestraintSwitch))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->eRegionType */
      case 11: { 
        VCAST_TI_12_91 ( ((RegionType *)(&(vcast_param->eRegionType))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->eForkType */
      case 12: { 
        VCAST_TI_12_57 ( ((ForkType *)(&(vcast_param->eForkType))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->eUseTilt */
      case 13: { 
        VCAST_TI_12_168 ( ((Use_tilt *)(&(vcast_param->eUseTilt))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->eTPA_Type */
      case 14: { 
        VCAST_TI_12_166 ( ((TPA_Type *)(&(vcast_param->eTPA_Type))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->eUseTiltSense */
      case 15: { 
        VCAST_TI_12_208 ( ((Tilt_Sense_type *)(&(vcast_param->eUseTiltSense))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->eUseAccy1 */
      case 16: { 
        VCAST_TI_12_170 ( ((Use_Accy1 *)(&(vcast_param->eUseAccy1))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->eUseAccy2 */
      case 17: { 
        VCAST_TI_12_158 ( ((Accy2Type *)(&(vcast_param->eUseAccy2))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->eUseAccy3 */
      case 18: { 
        VCAST_TI_12_172 ( ((Use_Accy3 *)(&(vcast_param->eUseAccy3))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->eTravelAlarm */
      case 19: { 
        VCAST_TI_12_111 ( ((TravelAlarmType *)(&(vcast_param->eTravelAlarm))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->eTRK_TYPE */
      case 20: { 
        VCAST_TI_12_113 ( ((TruckModel *)(&(vcast_param->eTRK_TYPE))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->eReachType */
      case 21: { 
        VCAST_TI_12_89 ( ((ReachType *)(&(vcast_param->eReachType))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->rReachDist */
      case 22: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rReachDist))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->eHiSpdLatched */
      case 23: { 
        VCAST_TI_12_174 ( ((HiSpdLatched *)(&(vcast_param->eHiSpdLatched))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->eHeightSenseType */
      case 24: { 
        VCAST_TI_12_59 ( ((HeightSenseType *)(&(vcast_param->eHeightSenseType))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxCapacity */
      case 25: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxCapacity))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->eUseLoadSense */
      case 26: { 
        VCAST_TI_12_176 ( ((LoadSense *)(&(vcast_param->eUseLoadSense))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->eReachSenseType */
      case 27: { 
        VCAST_TI_12_127 ( ((ReachSenseType *)(&(vcast_param->eReachSenseType))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->eOperatorRemoteStatus */
      case 28: { 
        VCAST_TI_12_123 ( ((OperatorRemoteStatus *)(&(vcast_param->eOperatorRemoteStatus))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->eWalkCoastObjectDetStatus */
      case 29: { 
        VCAST_TI_12_125 ( ((WalkCoastObjectDetStatus *)(&(vcast_param->eWalkCoastObjectDetStatus))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->eUseTransportTruckOption */
      case 30: { 
        VCAST_TI_12_178 ( ((Transport_option *)(&(vcast_param->eUseTransportTruckOption))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->eUseChainSlackDetection */
      case 31: { 
        VCAST_TI_12_180 ( ((ChainSlackDetection *)(&(vcast_param->eUseChainSlackDetection))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->eUseHeightCutouts_obs */
      case 32: { 
        VCAST_TI_12_182 ( ((UserHeightCutoutOption *)(&(vcast_param->eUseHeightCutouts_obs))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->eUseReachCutouts */
      case 33: { 
        VCAST_TI_12_184 ( ((UserReachCutoutOption *)(&(vcast_param->eUseReachCutouts))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->eUseStps */
      case 34: { 
        VCAST_TI_12_79 ( ((Op_Int_Mask_Type *)(&(vcast_param->eUseStps))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->eQPRA */
      case 35: { 
        VCAST_TI_12_121 ( ((QPRA *)(&(vcast_param->eQPRA))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->e360DegSteering */
      case 36: { 
        VCAST_TI_12_186 ( ((User360Select *)(&(vcast_param->e360DegSteering))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->eLoadSenseType */
      case 37: { 
        VCAST_TI_12_61 ( ((LoadSenseType *)(&(vcast_param->eLoadSenseType))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->eWalkAlongStatus */
      case 38: { 
        VCAST_TI_12_154 ( ((WalkAlongStatus *)(&(vcast_param->eWalkAlongStatus))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->eHssAntiTiedown */
      case 39: { 
        VCAST_TI_12_188 ( ((HSSAntiTieDownMode *)(&(vcast_param->eHssAntiTiedown))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->eHCMFanControl */
      case 40: { 
        VCAST_TI_12_156 ( ((FAN_CONTROL *)(&(vcast_param->eHCMFanControl))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->eTCM1FanControl */
      case 41: { 
        VCAST_TI_12_156 ( ((FAN_CONTROL *)(&(vcast_param->eTCM1FanControl))));
        break; /* end case 41*/
      } /* end case */
      /* Setting member variable vcast_param->eTCM2FanControl */
      case 42: { 
        VCAST_TI_12_156 ( ((FAN_CONTROL *)(&(vcast_param->eTCM2FanControl))));
        break; /* end case 42*/
      } /* end case */
      /* Setting member variable vcast_param->eHydMotorFanControl */
      case 43: { 
        VCAST_TI_12_156 ( ((FAN_CONTROL *)(&(vcast_param->eHydMotorFanControl))));
        break; /* end case 43*/
      } /* end case */
      /* Setting member variable vcast_param->eTrac1MotorFanControl */
      case 44: { 
        VCAST_TI_12_156 ( ((FAN_CONTROL *)(&(vcast_param->eTrac1MotorFanControl))));
        break; /* end case 44*/
      } /* end case */
      /* Setting member variable vcast_param->eTrac2MotorFanControl */
      case 45: { 
        VCAST_TI_12_156 ( ((FAN_CONTROL *)(&(vcast_param->eTrac2MotorFanControl))));
        break; /* end case 45*/
      } /* end case */
      /* Setting member variable vcast_param->eHDMFanControl */
      case 46: { 
        VCAST_TI_12_156 ( ((FAN_CONTROL *)(&(vcast_param->eHDMFanControl))));
        break; /* end case 46*/
      } /* end case */
      /* Setting member variable vcast_param->ulNumImpactSensors */
      case 47: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulNumImpactSensors))));
        break; /* end case 47*/
      } /* end case */
      /* Setting member variable vcast_param->rLoadBackRestHeight */
      case 48: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rLoadBackRestHeight))));
        break; /* end case 48*/
      } /* end case */
      /* Setting member variable vcast_param->eMastType */
      case 49: { 
        VCAST_TI_12_67 ( ((MastType *)(&(vcast_param->eMastType))));
        break; /* end case 49*/
      } /* end case */
      /* Setting member variable vcast_param->eMastDuty */
      case 50: { 
        VCAST_TI_12_65 ( ((MastDuty *)(&(vcast_param->eMastDuty))));
        break; /* end case 50*/
      } /* end case */
      /* Setting member variable vcast_param->eBatteryBoxType */
      case 51: { 
        VCAST_TI_12_160 ( ((BatteryBoxType *)(&(vcast_param->eBatteryBoxType))));
        break; /* end case 51*/
      } /* end case */
      /* Setting member variable vcast_param->eEnergySource */
      case 52: { 
        VCAST_TI_12_162 ( ((EnergySource *)(&(vcast_param->eEnergySource))));
        break; /* end case 52*/
      } /* end case */
      /* Setting member variable vcast_param->eGuidanceType */
      case 53: { 
        VCAST_TI_12_164 ( ((GuidanceType *)(&(vcast_param->eGuidanceType))));
        break; /* end case 53*/
      } /* end case */
      /* Setting member variable vcast_param->eAGV */
      case 54: { 
        VCAST_TI_12_190 ( ((AGV_Mode *)(&(vcast_param->eAGV))));
        break; /* end case 54*/
      } /* end case */
      /* Setting member variable vcast_param->eCapacityDataMonitor */
      case 55: { 
        VCAST_TI_12_194 ( ((CDM_Option *)(&(vcast_param->eCapacityDataMonitor))));
        break; /* end case 55*/
      } /* end case */
      /* Setting member variable vcast_param->eOperatorControlType */
      case 56: { 
        VCAST_TI_12_198 ( ((OperatorControlOption *)(&(vcast_param->eOperatorControlType))));
        break; /* end case 56*/
      } /* end case */
      /* Setting member variable vcast_param->eKeyLessPowerModule */
      case 57: { 
        VCAST_TI_12_206 ( ((KeylessPowerModule *)(&(vcast_param->eKeyLessPowerModule))));
        break; /* end case 57*/
      } /* end case */
      /* Setting member variable vcast_param->eSeatHeater */
      case 58: { 
        VCAST_TI_12_200 ( ((SeatHeater *)(&(vcast_param->eSeatHeater))));
        break; /* end case 58*/
      } /* end case */
      /* Setting member variable vcast_param->eSideShiftPositionAssist */
      case 59: { 
        VCAST_TI_12_196 ( ((SideShiftPositionAssistOption *)(&(vcast_param->eSideShiftPositionAssist))));
        break; /* end case 59*/
      } /* end case */
      /* Setting member variable vcast_param->eStrobeType */
      case 60: { 
        VCAST_TI_12_192 ( ((StrobeType *)(&(vcast_param->eStrobeType))));
        break; /* end case 60*/
      } /* end case */
      /* Setting member variable vcast_param->eTFD_presence */
      case 61: { 
        VCAST_TI_12_202 ( ((TFD_presence *)(&(vcast_param->eTFD_presence))));
        break; /* end case 61*/
      } /* end case */
      /* Setting member variable vcast_param->eBackRest_module */
      case 62: { 
        VCAST_TI_12_204 ( ((BackRest_module *)(&(vcast_param->eBackRest_module))));
        break; /* end case 62*/
      } /* end case */
      /* Setting member variable vcast_param->eTriggerTypeSpdCB1 */
      case 63: { 
        VCAST_TI_12_214 ( ((SpdCutTriggerType *)(&(vcast_param->eTriggerTypeSpdCB1))));
        break; /* end case 63*/
      } /* end case */
      /* Setting member variable vcast_param->eTriggerTypeSpdCB2 */
      case 64: { 
        VCAST_TI_12_214 ( ((SpdCutTriggerType *)(&(vcast_param->eTriggerTypeSpdCB2))));
        break; /* end case 64*/
      } /* end case */
      /* Setting member variable vcast_param->eTriggerTypeSpdCB3 */
      case 65: { 
        VCAST_TI_12_214 ( ((SpdCutTriggerType *)(&(vcast_param->eTriggerTypeSpdCB3))));
        break; /* end case 65*/
      } /* end case */
      /* Setting member variable vcast_param->ulForkLoadSpdCB1 */
      case 66: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulForkLoadSpdCB1))));
        break; /* end case 66*/
      } /* end case */
      /* Setting member variable vcast_param->ulForkLoadSpdCB2 */
      case 67: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulForkLoadSpdCB2))));
        break; /* end case 67*/
      } /* end case */
      /* Setting member variable vcast_param->ulForkLoadSpdCB3 */
      case 68: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulForkLoadSpdCB3))));
        break; /* end case 68*/
      } /* end case */
      /* Setting member variable vcast_param->ulHeightSpdCB1 */
      case 69: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulHeightSpdCB1))));
        break; /* end case 69*/
      } /* end case */
      /* Setting member variable vcast_param->ulHeightSpdCB2 */
      case 70: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulHeightSpdCB2))));
        break; /* end case 70*/
      } /* end case */
      /* Setting member variable vcast_param->ulHeightSpdCB3 */
      case 71: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulHeightSpdCB3))));
        break; /* end case 71*/
      } /* end case */
      /* Setting member variable vcast_param->eForkHomeSpdCB1 */
      case 72: { 
        VCAST_TI_12_216 ( ((ForkHome *)(&(vcast_param->eForkHomeSpdCB1))));
        break; /* end case 72*/
      } /* end case */
      /* Setting member variable vcast_param->eForkHomeSpdCB2 */
      case 73: { 
        VCAST_TI_12_216 ( ((ForkHome *)(&(vcast_param->eForkHomeSpdCB2))));
        break; /* end case 73*/
      } /* end case */
      /* Setting member variable vcast_param->eForkHomeSpdCB3 */
      case 74: { 
        VCAST_TI_12_216 ( ((ForkHome *)(&(vcast_param->eForkHomeSpdCB3))));
        break; /* end case 74*/
      } /* end case */
      /* Setting member variable vcast_param->eSpeedCutbackSROSpdCB1 */
      case 75: { 
        VCAST_TI_12_210 ( ((SpeedCutbackSRO *)(&(vcast_param->eSpeedCutbackSROSpdCB1))));
        break; /* end case 75*/
      } /* end case */
      /* Setting member variable vcast_param->eSpeedCutbackSROSpdCB2 */
      case 76: { 
        VCAST_TI_12_210 ( ((SpeedCutbackSRO *)(&(vcast_param->eSpeedCutbackSROSpdCB2))));
        break; /* end case 76*/
      } /* end case */
      /* Setting member variable vcast_param->eSpeedCutbackSROSpdCB3 */
      case 77: { 
        VCAST_TI_12_210 ( ((SpeedCutbackSRO *)(&(vcast_param->eSpeedCutbackSROSpdCB3))));
        break; /* end case 77*/
      } /* end case */
      /* Setting member variable vcast_param->eLoadWheelBrakes */
      case 78: { 
        VCAST_TI_12_220 ( ((LoadWheelBrakes *)(&(vcast_param->eLoadWheelBrakes))));
        break; /* end case 78*/
      } /* end case */
      /* Setting member variable vcast_param->eSpeedCutbackFeatureSpdCB1 */
      case 79: { 
        VCAST_TI_12_222 ( ((SpeedCutbackFeature *)(&(vcast_param->eSpeedCutbackFeatureSpdCB1))));
        break; /* end case 79*/
      } /* end case */
      /* Setting member variable vcast_param->eSpeedCutbackFeatureSpdCB2 */
      case 80: { 
        VCAST_TI_12_222 ( ((SpeedCutbackFeature *)(&(vcast_param->eSpeedCutbackFeatureSpdCB2))));
        break; /* end case 80*/
      } /* end case */
      /* Setting member variable vcast_param->eSpeedCutbackFeatureSpdCB3 */
      case 81: { 
        VCAST_TI_12_222 ( ((SpeedCutbackFeature *)(&(vcast_param->eSpeedCutbackFeatureSpdCB3))));
        break; /* end case 81*/
      } /* end case */
      /* Setting member variable vcast_param->eBatteryVoltage */
      case 82: { 
        VCAST_TI_12_226 ( ((Battery_Voltage *)(&(vcast_param->eBatteryVoltage))));
        break; /* end case 82*/
      } /* end case */
      /* Setting member variable vcast_param->ulCdmHeight1 */
      case 83: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCdmHeight1))));
        break; /* end case 83*/
      } /* end case */
      /* Setting member variable vcast_param->ulCdmWeight1 */
      case 84: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCdmWeight1))));
        break; /* end case 84*/
      } /* end case */
      /* Setting member variable vcast_param->ulCdmHeight2 */
      case 85: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCdmHeight2))));
        break; /* end case 85*/
      } /* end case */
      /* Setting member variable vcast_param->ulCdmWeight2 */
      case 86: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCdmWeight2))));
        break; /* end case 86*/
      } /* end case */
      /* Setting member variable vcast_param->ulCdmHeight3 */
      case 87: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCdmHeight3))));
        break; /* end case 87*/
      } /* end case */
      /* Setting member variable vcast_param->ulCdmWeight3 */
      case 88: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCdmWeight3))));
        break; /* end case 88*/
      } /* end case */
      /* Setting member variable vcast_param->ulCdmHeight4 */
      case 89: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCdmHeight4))));
        break; /* end case 89*/
      } /* end case */
      /* Setting member variable vcast_param->ulCdmWeight4 */
      case 90: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCdmWeight4))));
        break; /* end case 90*/
      } /* end case */
      /* Setting member variable vcast_param->slLoadedOffset */
      case 91: { 
        VCAST_TI_11_31 ( ((signed int *)(&(vcast_param->slLoadedOffset))));
        break; /* end case 91*/
      } /* end case */
      /* Setting member variable vcast_param->rWheelBase */
      case 92: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rWheelBase))));
        break; /* end case 92*/
      } /* end case */
      /* Setting member variable vcast_param->eLaserLine */
      case 93: { 
        VCAST_TI_12_238 ( ((LaserLineOption *)(&(vcast_param->eLaserLine))));
        break; /* end case 93*/
      } /* end case */
      /* Setting member variable vcast_param->eBlueLight */
      case 94: { 
        VCAST_TI_12_240 ( ((BlueLightOption *)(&(vcast_param->eBlueLight))));
        break; /* end case 94*/
      } /* end case */
      /* Setting member variable vcast_param->eEstopChannels */
      case 95: { 
        VCAST_TI_12_246 ( ((EstopChannels *)(&(vcast_param->eEstopChannels))));
        break; /* end case 95*/
      } /* end case */
      /* Setting member variable vcast_param->eBatteryPerfCurve */
      case 96: { 
        VCAST_TI_12_160 ( ((BatteryBoxType *)(&(vcast_param->eBatteryPerfCurve))));
        break; /* end case 96*/
      } /* end case */
      /* Setting member variable vcast_param->eBHM_Presence */
      case 97: { 
        VCAST_TI_12_250 ( ((BHM_Presence *)(&(vcast_param->eBHM_Presence))));
        break; /* end case 97*/
      } /* end case */
      /* Setting member variable vcast_param->eInhibit_Xpress_Lower */
      case 98: { 
        VCAST_TI_12_252 ( ((InhibitXpressLower *)(&(vcast_param->eInhibit_Xpress_Lower))));
        break; /* end case 98*/
      } /* end case */
      /* Setting member variable vcast_param->rFifthFunctionMaxOilFlow */
      case 99: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rFifthFunctionMaxOilFlow))));
        break; /* end case 99*/
      } /* end case */
      /* Setting member variable vcast_param->eEnergySource_EWSTypeIntegration */
      case 100: { 
        VCAST_TI_12_254 ( ((EnergySource_EWSTypeIntegration *)(&(vcast_param->eEnergySource_EWSTypeIntegration))));
        break; /* end case 100*/
      } /* end case */
      /* Setting member variable vcast_param->eTempSense */
      case 101: { 
        VCAST_TI_12_256 ( ((TempSense *)(&(vcast_param->eTempSense))));
        break; /* end case 101*/
      } /* end case */
      /* Setting member variable vcast_param->eFreezerHeater */
      case 102: { 
        VCAST_TI_12_258 ( ((FreezerHeater *)(&(vcast_param->eFreezerHeater))));
        break; /* end case 102*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_10 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_806 ( powerbase_fb_main_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_806 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_806 ( powerbase_fb_main_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->Powerbase_fb_tcm1 */
      case 1: { 
        VCAST_TI_12_804 ( ((powerbase_fb_bus_t *)(&(vcast_param->Powerbase_fb_tcm1))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->Powerbase_fb_tcm2 */
      case 2: { 
        VCAST_TI_12_804 ( ((powerbase_fb_bus_t *)(&(vcast_param->Powerbase_fb_tcm2))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->Powerbase_fb_hcm */
      case 3: { 
        VCAST_TI_12_804 ( ((powerbase_fb_bus_t *)(&(vcast_param->Powerbase_fb_hcm))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->Powerbase_fb_hdm */
      case 4: { 
        VCAST_TI_12_804 ( ((powerbase_fb_bus_t *)(&(vcast_param->Powerbase_fb_hdm))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->Powerbase_fb_scm */
      case 5: { 
        VCAST_TI_12_804 ( ((powerbase_fb_bus_t *)(&(vcast_param->Powerbase_fb_scm))));
        break; /* end case 5*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_806 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_818 ( steer_inputs_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_818 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_818 ( steer_inputs_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rStrCmd1 */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rStrCmd1))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rStrCmd2 */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rStrCmd2))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ulStrMstrStat */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulStrMstrStat))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ulStrSlvStat */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulStrSlvStat))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->fMstrCanStat */
      case 5: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMstrCanStat))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->fSlvCanStat */
      case 6: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSlvCanStat))));
        break; /* end case 6*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_818 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_810 ( steering_ms_fdbk_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_810 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_810 ( steering_ms_fdbk_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->MstrFdbk */
      case 1: { 
        VCAST_TI_12_808 ( ((steering_fdbk_bus_t *)(&(vcast_param->MstrFdbk))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->SlvFdbk */
      case 2: { 
        VCAST_TI_12_808 ( ((steering_fdbk_bus_t *)(&(vcast_param->SlvFdbk))));
        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_810 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_814 ( throttle_in_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_814 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_814 ( throttle_in_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->Throttle_input_traction1 */
      case 1: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_traction1))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->Throttle_input_traction2 */
      case 2: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_traction2))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->Throttle_input_mainhoist1 */
      case 3: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_mainhoist1))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->Throttle_input_mainhoist2 */
      case 4: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_mainhoist2))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->Throttle_input_auxhoist */
      case 5: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_auxhoist))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->Throttle_input_tilt */
      case 6: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_tilt))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->Throttle_input_reach_retract */
      case 7: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_reach_retract))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->Throttle_input_sideshift */
      case 8: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_sideshift))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->Throttle_input_pivot */
      case 9: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_pivot))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->Throttle_input_brake */
      case 10: { 
        VCAST_TI_12_812 ( ((throttle_input_bus_t *)(&(vcast_param->Throttle_input_brake))));
        break; /* end case 10*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_814 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_820 ( voltage_mon_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_820 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_820 ( voltage_mon_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rSv1VoltMon */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv1VoltMon))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rSv2VoltMon */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv2VoltMon))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rSv3VoltMon */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv3VoltMon))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rSv4VoltMon */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv4VoltMon))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rSv5VoltMon */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv5VoltMon))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rSv6VoltMon */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv6VoltMon))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rSv7VoltMon */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv7VoltMon))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->rSv8VoltMon */
      case 8: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSv8VoltMon))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->rPs1VoltMon */
      case 9: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPs1VoltMon))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->rPs1GndVoltMon */
      case 10: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPs1GndVoltMon))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->rBrkOutNegVoltMon */
      case 11: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrkOutNegVoltMon))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->rBrkOutPosVoltMon */
      case 12: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrkOutPosVoltMon))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->rBrkOutBvVoltMon */
      case 13: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrkOutBvVoltMon))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->rPs4VoltMon */
      case 14: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPs4VoltMon))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->rSwGndVoltMon */
      case 15: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSwGndVoltMon))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->rEdInVoltMon */
      case 16: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rEdInVoltMon))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->rEdPosVoltMon */
      case 17: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rEdPosVoltMon))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->rVstbyVoltMon */
      case 18: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rVstbyVoltMon))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->r15vVoltMon */
      case 19: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r15vVoltMon))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->r5vVoltMon */
      case 20: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r5vVoltMon))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->rVcmBvKeyIn */
      case 21: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rVcmBvKeyIn))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->rBrkInnerNegVoltMon */
      case 22: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrkInnerNegVoltMon))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->rBrkInnerPosVoltMon */
      case 23: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrkInnerPosVoltMon))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->rBrkInnerBvVoltMon */
      case 24: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrkInnerBvVoltMon))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->rCanAVoltMon */
      case 25: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCanAVoltMon))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->rRS422RxVoltMon */
      case 26: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRS422RxVoltMon))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->rBattSense */
      case 27: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBattSense))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->rEdNegVoltMon */
      case 28: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rEdNegVoltMon))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->rRS422TxVoltMon */
      case 29: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRS422TxVoltMon))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->rBrkCasterNegVoltMon */
      case 30: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrkCasterNegVoltMon))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->rBrkCasterPosVoltMon */
      case 31: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrkCasterPosVoltMon))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->rPs2VoltMon */
      case 32: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPs2VoltMon))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->rPs2GndVoltMon */
      case 33: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPs2GndVoltMon))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->rPs3VoltMon */
      case 34: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPs3VoltMon))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->rPs3GndVoltMon */
      case 35: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPs3GndVoltMon))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->rInternalVrhSupply */
      case 36: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rInternalVrhSupply))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->rInternalVrlSupply */
      case 37: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rInternalVrlSupply))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->rInternal3_3vSupply */
      case 38: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rInternal3_3vSupply))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->rInternal1_2vSupply */
      case 39: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rInternal1_2vSupply))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->rInternal2_5vRef */
      case 40: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rInternal2_5vRef))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->r4_096vRefAdcA0 */
      case 41: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r4_096vRefAdcA0))));
        break; /* end case 41*/
      } /* end case */
      /* Setting member variable vcast_param->r372mvRefAdcA0 */
      case 42: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r372mvRefAdcA0))));
        break; /* end case 42*/
      } /* end case */
      /* Setting member variable vcast_param->r4_096vRefAdcA1 */
      case 43: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r4_096vRefAdcA1))));
        break; /* end case 43*/
      } /* end case */
      /* Setting member variable vcast_param->r372mvRefAdcA1 */
      case 44: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r372mvRefAdcA1))));
        break; /* end case 44*/
      } /* end case */
      /* Setting member variable vcast_param->r4_096vRefAdcB0 */
      case 45: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r4_096vRefAdcB0))));
        break; /* end case 45*/
      } /* end case */
      /* Setting member variable vcast_param->r372mvRefAdcB0 */
      case 46: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r372mvRefAdcB0))));
        break; /* end case 46*/
      } /* end case */
      /* Setting member variable vcast_param->r4_096vRefAdcB1 */
      case 47: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r4_096vRefAdcB1))));
        break; /* end case 47*/
      } /* end case */
      /* Setting member variable vcast_param->r372mvRefAdcB1 */
      case 48: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r372mvRefAdcB1))));
        break; /* end case 48*/
      } /* end case */
      /* Setting member variable vcast_param->r4_096vRefAdcA0Counts */
      case 49: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r4_096vRefAdcA0Counts))));
        break; /* end case 49*/
      } /* end case */
      /* Setting member variable vcast_param->r372mvRefAdcA0Counts */
      case 50: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r372mvRefAdcA0Counts))));
        break; /* end case 50*/
      } /* end case */
      /* Setting member variable vcast_param->r4_096vRefAdcA1Counts */
      case 51: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r4_096vRefAdcA1Counts))));
        break; /* end case 51*/
      } /* end case */
      /* Setting member variable vcast_param->r372mvRefAdcA1Counts */
      case 52: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r372mvRefAdcA1Counts))));
        break; /* end case 52*/
      } /* end case */
      /* Setting member variable vcast_param->r4_096vRefAdcB0Counts */
      case 53: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r4_096vRefAdcB0Counts))));
        break; /* end case 53*/
      } /* end case */
      /* Setting member variable vcast_param->r372mvRefAdcB0Counts */
      case 54: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r372mvRefAdcB0Counts))));
        break; /* end case 54*/
      } /* end case */
      /* Setting member variable vcast_param->r4_096vRefAdcB1Counts */
      case 55: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r4_096vRefAdcB1Counts))));
        break; /* end case 55*/
      } /* end case */
      /* Setting member variable vcast_param->r372mvRefAdcB1Counts */
      case 56: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r372mvRefAdcB1Counts))));
        break; /* end case 56*/
      } /* end case */
      /* Setting member variable vcast_param->rPs5VoltMon */
      case 57: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPs5VoltMon))));
        break; /* end case 57*/
      } /* end case */
      /* Setting member variable vcast_param->rPs5GndVoltMon */
      case 58: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPs5GndVoltMon))));
        break; /* end case 58*/
      } /* end case */
      /* Setting member variable vcast_param->rGpd17VoltMon */
      case 59: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGpd17VoltMon))));
        break; /* end case 59*/
      } /* end case */
      /* Setting member variable vcast_param->rGpd18VoltMon */
      case 60: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGpd18VoltMon))));
        break; /* end case 60*/
      } /* end case */
      /* Setting member variable vcast_param->rANA5VoltMon */
      case 61: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rANA5VoltMon))));
        break; /* end case 61*/
      } /* end case */
      /* Setting member variable vcast_param->rTcmANA1VoltMon */
      case 62: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTcmANA1VoltMon))));
        break; /* end case 62*/
      } /* end case */
      /* Setting member variable vcast_param->rPsovMainVoltMon */
      case 63: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPsovMainVoltMon))));
        break; /* end case 63*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_820 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_816 ( vehicle_fb_c2m_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_816 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_816 ( vehicle_fb_c2m_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rSecHgtCounts */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSecHgtCounts))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxSecCounts */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxSecCounts))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rSecHgtCountsTime */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSecHgtCountsTime))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rSecHgtCountsAtPowerUp */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSecHgtCountsAtPowerUp))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rPriHgtCounts */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPriHgtCounts))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxPriCounts */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxPriCounts))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rPriHgtCountsTime */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPriHgtCountsTime))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->rPriHgtCountsAtPowerUp */
      case 8: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPriHgtCountsAtPowerUp))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxHgtCounts */
      case 9: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxHgtCounts))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxHgtCountsTime */
      case 10: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxHgtCountsTime))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxHgtCountsAtPowerUp */
      case 11: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxHgtCountsAtPowerUp))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->rLoadSensePressureRaw */
      case 12: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rLoadSensePressureRaw))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->rAccerometerXaccel */
      case 13: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccerometerXaccel))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->rAccerometerYaccel */
      case 14: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccerometerYaccel))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->rAccerometerZaccel */
      case 15: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccerometerZaccel))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->rGrade */
      case 16: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGrade))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->fResetMainCutoutStateMachine */
      case 17: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fResetMainCutoutStateMachine))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltPosition */
      case 18: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltPosition))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1Position_OBSOLETE */
      case 19: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1Position_OBSOLETE))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2Counts */
      case 20: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2Counts))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1Counts */
      case 21: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1Counts))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->fValveHeatingDone */
      case 22: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fValveHeatingDone))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->rGyroXrate */
      case 23: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGyroXrate))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->rGyroYrate */
      case 24: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGyroYrate))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->rGyroZrate */
      case 25: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGyroZrate))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->fAccelGyroSelfTest */
      case 26: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAccelGyroSelfTest))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->ubSchmersalLMSFlags */
      case 27: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubSchmersalLMSFlags))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->f811Active */
      case 28: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->f811Active))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->ubSideShiftAssistPos */
      case 29: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubSideShiftAssistPos))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->rTempSenseRaw */
      case 30: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTempSenseRaw))));
        break; /* end case 30*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_816 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_5 ( AutoCal_Cm_Calib_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_5 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_5 ( AutoCal_Cm_Calib_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->eMode */
      case 1: { 
        VCAST_TI_12_6 ( ((CalibMode *)(&(vcast_param->eMode))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->eUIKeyinput */
      case 2: { 
        VCAST_TI_12_146 ( ((CalibUIKeyInput *)(&(vcast_param->eUIKeyinput))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ubActionComplete */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubActionComplete))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rUIDataInput */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rUIDataInput))));
        break; /* end case 4*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_5 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_513 ( performance_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_513 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_513 ( performance_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rMainRaiseSpeedMax */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainRaiseSpeedMax))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rMainLowerSpeedMax */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainLowerSpeedMax))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rMainHSLLowerSpeedMax */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainHSLLowerSpeedMax))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rMainRaiseStageSpd */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainRaiseStageSpd))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rMainRaiseStageDist */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainRaiseStageDist))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rMainLowerStageSpd */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainLowerStageSpd))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rMainLowerStageDist */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainLowerStageDist))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltUpSpeedBelowFL */
      case 8: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltUpSpeedBelowFL))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltUpSpeedAboveFL */
      case 9: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltUpSpeedAboveFL))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltUpSpeedAboveHeightLimit1 */
      case 10: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltUpSpeedAboveHeightLimit1))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltDownSpeedBelowFL */
      case 11: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltDownSpeedBelowFL))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltDownSpeedAboveFL */
      case 12: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltDownSpeedAboveFL))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltDownSpeedAboveHeightLimit1 */
      case 13: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltDownSpeedAboveHeightLimit1))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1PosSpeedBelowFL */
      case 14: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1PosSpeedBelowFL))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1PosSpeedAboveFL */
      case 15: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1PosSpeedAboveFL))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1PosSpeedAboveHeightLimit1 */
      case 16: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1PosSpeedAboveHeightLimit1))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1NegSpeedBelowFL */
      case 17: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1NegSpeedBelowFL))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1NegSpeedAboveFL */
      case 18: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1NegSpeedAboveFL))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1NegSpeedAboveHeightLimit1 */
      case 19: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1NegSpeedAboveHeightLimit1))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2PosSpeedBelowFL */
      case 20: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2PosSpeedBelowFL))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2PosSpeedAboveFL */
      case 21: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2PosSpeedAboveFL))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2PosSpeedAboveHeightLimit1 */
      case 22: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2PosSpeedAboveHeightLimit1))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2NegSpeedBelowFL */
      case 23: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2NegSpeedBelowFL))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2NegSpeedAboveFL */
      case 24: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2NegSpeedAboveFL))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2NegSpeedAboveHeightLimit1 */
      case 25: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2NegSpeedAboveHeightLimit1))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltUpAccel */
      case 26: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltUpAccel))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltUpDecel */
      case 27: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltUpDecel))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltDownAccel */
      case 28: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltDownAccel))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltDownDecel */
      case 29: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltDownDecel))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1PosAccel */
      case 30: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1PosAccel))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1PosDecel */
      case 31: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1PosDecel))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1NegAccel */
      case 32: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1NegAccel))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1NegDecel */
      case 33: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1NegDecel))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2PosAccel */
      case 34: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2PosAccel))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2PosDecel */
      case 35: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2PosDecel))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2NegAccel */
      case 36: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2NegAccel))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2NegDecel */
      case 37: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2NegDecel))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->rMainRaiseAccel */
      case 38: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainRaiseAccel))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->rMainRaiseDecel */
      case 39: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainRaiseDecel))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->rMainLowerAccel */
      case 40: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainLowerAccel))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->rMainLowerDecel */
      case 41: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainLowerDecel))));
        break; /* end case 41*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxRaiseAccel */
      case 42: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxRaiseAccel))));
        break; /* end case 42*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxRaiseDecel */
      case 43: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxRaiseDecel))));
        break; /* end case 43*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxLowerAccel */
      case 44: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxLowerAccel))));
        break; /* end case 44*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxLowerDecel */
      case 45: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxLowerDecel))));
        break; /* end case 45*/
      } /* end case */
      /* Setting member variable vcast_param->rRetractSlowDownDist */
      case 46: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRetractSlowDownDist))));
        break; /* end case 46*/
      } /* end case */
      /* Setting member variable vcast_param->rExtendSlowDownDist */
      case 47: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rExtendSlowDownDist))));
        break; /* end case 47*/
      } /* end case */
      /* Setting member variable vcast_param->rMotorEnvelope_Torq */
      case 48: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rMotorEnvelope_Torq)));
        break; /* end case 48*/
      } /* end case */
      /* Setting member variable vcast_param->rMotorEnvelope_Speed */
      case 49: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rMotorEnvelope_Speed)));
        break; /* end case 49*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxSpeedFWD */
      case 50: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxSpeedFWD))));
        break; /* end case 50*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxSpeedREV */
      case 51: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxSpeedREV))));
        break; /* end case 51*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxHiSpeedFWD */
      case 52: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxHiSpeedFWD))));
        break; /* end case 52*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxHiSpeedREV */
      case 53: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxHiSpeedREV))));
        break; /* end case 53*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxWalkieSpeedFWD */
      case 54: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxWalkieSpeedFWD))));
        break; /* end case 54*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxWalkieSpeedREV */
      case 55: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxWalkieSpeedREV))));
        break; /* end case 55*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxPickSpeedFWD */
      case 56: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxPickSpeedFWD))));
        break; /* end case 56*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxPickSpeedREV */
      case 57: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxPickSpeedREV))));
        break; /* end case 57*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_VS_HEIGHT_FWD */
      case 58: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rSPEED_VS_HEIGHT_FWD)));
        break; /* end case 58*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_VS_HEIGHT_REV */
      case 59: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rSPEED_VS_HEIGHT_REV)));
        break; /* end case 59*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_VS_LOAD_FWD */
      case 60: { 
        VCAST_TI_12_483 ( ((float *)(vcast_param->rSPEED_VS_LOAD_FWD)));
        break; /* end case 60*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_VS_LOAD_FWD_LOAD */
      case 61: { 
        VCAST_TI_12_483 ( ((float *)(vcast_param->rSPEED_VS_LOAD_FWD_LOAD)));
        break; /* end case 61*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_VS_LOAD_REV */
      case 62: { 
        VCAST_TI_12_483 ( ((float *)(vcast_param->rSPEED_VS_LOAD_REV)));
        break; /* end case 62*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_VS_LOAD_REV_LOAD */
      case 63: { 
        VCAST_TI_12_483 ( ((float *)(vcast_param->rSPEED_VS_LOAD_REV_LOAD)));
        break; /* end case 63*/
      } /* end case */
      /* Setting member variable vcast_param->rACCEL_G_FOR0_5 */
      case 64: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rACCEL_G_FOR0_5)));
        break; /* end case 64*/
      } /* end case */
      /* Setting member variable vcast_param->rACCEL_G_REV0_5 */
      case 65: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rACCEL_G_REV0_5)));
        break; /* end case 65*/
      } /* end case */
      /* Setting member variable vcast_param->rWALKIESPEED_FORWARD_ACCEL */
      case 66: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rWALKIESPEED_FORWARD_ACCEL))));
        break; /* end case 66*/
      } /* end case */
      /* Setting member variable vcast_param->rWALKIESPEED_REVERSE_ACCEL */
      case 67: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rWALKIESPEED_REVERSE_ACCEL))));
        break; /* end case 67*/
      } /* end case */
      /* Setting member variable vcast_param->rHISPEED_FORWARD_ACCEL */
      case 68: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rHISPEED_FORWARD_ACCEL))));
        break; /* end case 68*/
      } /* end case */
      /* Setting member variable vcast_param->rHISPEED_REVERSE_ACCEL */
      case 69: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rHISPEED_REVERSE_ACCEL))));
        break; /* end case 69*/
      } /* end case */
      /* Setting member variable vcast_param->rTHROTTLE2_FORWARD_ACCEL */
      case 70: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTHROTTLE2_FORWARD_ACCEL))));
        break; /* end case 70*/
      } /* end case */
      /* Setting member variable vcast_param->rTHROTTLE2_REVERSE_ACCEL */
      case 71: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTHROTTLE2_REVERSE_ACCEL))));
        break; /* end case 71*/
      } /* end case */
      /* Setting member variable vcast_param->ePerformanceCurve */
      case 72: { 
        VCAST_TI_12_244 ( ((PerformanceCurve *)(&(vcast_param->ePerformanceCurve))));
        break; /* end case 72*/
      } /* end case */
      /* Setting member variable vcast_param->rPLUG_G_FOR0_5 */
      case 73: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rPLUG_G_FOR0_5)));
        break; /* end case 73*/
      } /* end case */
      /* Setting member variable vcast_param->rPLUG_G_REV0_5 */
      case 74: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rPLUG_G_REV0_5)));
        break; /* end case 74*/
      } /* end case */
      /* Setting member variable vcast_param->rBRAKE_G_FOR0_5 */
      case 75: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rBRAKE_G_FOR0_5)));
        break; /* end case 75*/
      } /* end case */
      /* Setting member variable vcast_param->rBRAKE_G_REV0_5 */
      case 76: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rBRAKE_G_REV0_5)));
        break; /* end case 76*/
      } /* end case */
      /* Setting member variable vcast_param->rCOAST_G_FOR0_5 */
      case 77: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rCOAST_G_FOR0_5)));
        break; /* end case 77*/
      } /* end case */
      /* Setting member variable vcast_param->rCOAST_G_REV0_5 */
      case 78: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rCOAST_G_REV0_5)));
        break; /* end case 78*/
      } /* end case */
      /* Setting member variable vcast_param->rHISPEED_FORWARD_COAST */
      case 79: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rHISPEED_FORWARD_COAST))));
        break; /* end case 79*/
      } /* end case */
      /* Setting member variable vcast_param->rHISPEED_REVERSE_COAST */
      case 80: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rHISPEED_REVERSE_COAST))));
        break; /* end case 80*/
      } /* end case */
      /* Setting member variable vcast_param->rWALKIESPEED_FORWARD_COAST */
      case 81: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rWALKIESPEED_FORWARD_COAST))));
        break; /* end case 81*/
      } /* end case */
      /* Setting member variable vcast_param->rWALKIESPEED_REVERSE_COAST */
      case 82: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rWALKIESPEED_REVERSE_COAST))));
        break; /* end case 82*/
      } /* end case */
      /* Setting member variable vcast_param->rTHROTTLE2_FORWARD_COAST */
      case 83: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTHROTTLE2_FORWARD_COAST))));
        break; /* end case 83*/
      } /* end case */
      /* Setting member variable vcast_param->rTHROTTLE2_REVERSE_COAST */
      case 84: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTHROTTLE2_REVERSE_COAST))));
        break; /* end case 84*/
      } /* end case */
      /* Setting member variable vcast_param->rTHROTTLE_DECEL_G_FOR0_5 */
      case 85: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rTHROTTLE_DECEL_G_FOR0_5)));
        break; /* end case 85*/
      } /* end case */
      /* Setting member variable vcast_param->rTHROTTLE_DECEL_G_REV0_5 */
      case 86: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rTHROTTLE_DECEL_G_REV0_5)));
        break; /* end case 86*/
      } /* end case */
      /* Setting member variable vcast_param->ulLiftLockoutSOCThreshold */
      case 87: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulLiftLockoutSOCThreshold))));
        break; /* end case 87*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_EMPTY_ANGLE */
      case 88: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_EMPTY_ANGLE)));
        break; /* end case 88*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_EMPTY_SPEED */
      case 89: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_EMPTY_SPEED)));
        break; /* end case 89*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_LOADED_ANGLE */
      case 90: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_LOADED_ANGLE)));
        break; /* end case 90*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_LOADED_SPEED */
      case 91: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_FWD_LOADED_SPEED)));
        break; /* end case 91*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_EMPTY_ANGLE */
      case 92: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_EMPTY_ANGLE)));
        break; /* end case 92*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_EMPTY_SPEED */
      case 93: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_EMPTY_SPEED)));
        break; /* end case 93*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE */
      case 94: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_ANGLE)));
        break; /* end case 94*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_SPEED */
      case 95: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_NORMAL_REV_LOADED_SPEED)));
        break; /* end case 95*/
      } /* end case */
      /* Setting member variable vcast_param->rCutBackSpeedSpdCB1 */
      case 96: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCutBackSpeedSpdCB1))));
        break; /* end case 96*/
      } /* end case */
      /* Setting member variable vcast_param->rCutBackSpeedSpdCB2 */
      case 97: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCutBackSpeedSpdCB2))));
        break; /* end case 97*/
      } /* end case */
      /* Setting member variable vcast_param->rCutBackSpeedSpdCB3 */
      case 98: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCutBackSpeedSpdCB3))));
        break; /* end case 98*/
      } /* end case */
      /* Setting member variable vcast_param->rCutBackSpeed_DECELSpdCB1 */
      case 99: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCutBackSpeed_DECELSpdCB1))));
        break; /* end case 99*/
      } /* end case */
      /* Setting member variable vcast_param->rCutBackSpeed_DECELSpdCB2 */
      case 100: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCutBackSpeed_DECELSpdCB2))));
        break; /* end case 100*/
      } /* end case */
      /* Setting member variable vcast_param->rCutBackSpeed_DECELSpdCB3 */
      case 101: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCutBackSpeed_DECELSpdCB3))));
        break; /* end case 101*/
      } /* end case */
      /* Setting member variable vcast_param->rLowerSpeedAboveCustomHeight */
      case 102: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rLowerSpeedAboveCustomHeight))));
        break; /* end case 102*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxRaiseSpeedMax */
      case 103: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxRaiseSpeedMax))));
        break; /* end case 103*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxLowerSpeedMax */
      case 104: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxLowerSpeedMax))));
        break; /* end case 104*/
      } /* end case */
      /* Setting member variable vcast_param->rCasterBrakePct */
      case 105: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCasterBrakePct))));
        break; /* end case 105*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3PosSpeedBelowFL */
      case 106: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3PosSpeedBelowFL))));
        break; /* end case 106*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3PosSpeedAboveFL */
      case 107: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3PosSpeedAboveFL))));
        break; /* end case 107*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3PosSpeedAboveHeightLimit1 */
      case 108: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3PosSpeedAboveHeightLimit1))));
        break; /* end case 108*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3NegSpeedBelowFL */
      case 109: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3NegSpeedBelowFL))));
        break; /* end case 109*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3NegSpeedAboveFL */
      case 110: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3NegSpeedAboveFL))));
        break; /* end case 110*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3NegSpeedAboveHeightLimit1 */
      case 111: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3NegSpeedAboveHeightLimit1))));
        break; /* end case 111*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3PosAccel */
      case 112: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3PosAccel))));
        break; /* end case 112*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3PosDecel */
      case 113: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3PosDecel))));
        break; /* end case 113*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3NegAccel */
      case 114: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3NegAccel))));
        break; /* end case 114*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy3NegDecel */
      case 115: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy3NegDecel))));
        break; /* end case 115*/
      } /* end case */
      /* Setting member variable vcast_param->rMinMotorRpmRaiseKnob */
      case 116: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMinMotorRpmRaiseKnob))));
        break; /* end case 116*/
      } /* end case */
      /* Setting member variable vcast_param->rMinMotorRpmLowerKnob */
      case 117: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMinMotorRpmLowerKnob))));
        break; /* end case 117*/
      } /* end case */
      /* Setting member variable vcast_param->rMainLowerBottomStopMinSpd */
      case 118: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainLowerBottomStopMinSpd))));
        break; /* end case 118*/
      } /* end case */
      /* Setting member variable vcast_param->rMainLowerBottomStopDist */
      case 119: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainLowerBottomStopDist))));
        break; /* end case 119*/
      } /* end case */
      /* Setting member variable vcast_param->eOptimizedCorneringSpeedCurve */
      case 120: { 
        VCAST_TI_12_242 ( ((OptimizedCorneringSpeedCurve *)(&(vcast_param->eOptimizedCorneringSpeedCurve))));
        break; /* end case 120*/
      } /* end case */
      /* Setting member variable vcast_param->rVpcCharged */
      case 121: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rVpcCharged))));
        break; /* end case 121*/
      } /* end case */
      /* Setting member variable vcast_param->rVpcDischarged */
      case 122: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rVpcDischarged))));
        break; /* end case 122*/
      } /* end case */
      /* Setting member variable vcast_param->rRetractSlowDownDist_RD */
      case 123: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRetractSlowDownDist_RD))));
        break; /* end case 123*/
      } /* end case */
      /* Setting member variable vcast_param->rExtendSlowDownDist_RD */
      case 124: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rExtendSlowDownDist_RD))));
        break; /* end case 124*/
      } /* end case */
      /* Setting member variable vcast_param->ulLiftLockoutSOCThreshold_ESS */
      case 125: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulLiftLockoutSOCThreshold_ESS))));
        break; /* end case 125*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_FWD_EMPTY_ANGLE */
      case 126: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_FWD_EMPTY_ANGLE)));
        break; /* end case 126*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_FWD_EMPTY_SPEED */
      case 127: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_FWD_EMPTY_SPEED)));
        break; /* end case 127*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_FWD_LOADED_ANGLE */
      case 128: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_FWD_LOADED_ANGLE)));
        break; /* end case 128*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_FWD_LOADED_SPEED */
      case 129: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_FWD_LOADED_SPEED)));
        break; /* end case 129*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_REV_EMPTY_ANGLE */
      case 130: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_REV_EMPTY_ANGLE)));
        break; /* end case 130*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_REV_EMPTY_SPEED */
      case 131: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_REV_EMPTY_SPEED)));
        break; /* end case 131*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_REV_LOADED_ANGLE */
      case 132: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_REV_LOADED_ANGLE)));
        break; /* end case 132*/
      } /* end case */
      /* Setting member variable vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_REV_LOADED_SPEED */
      case 133: { 
        VCAST_TI_12_497 ( ((float *)(vcast_param->rSPEED_LIMIT_VS_ANGLE_HISPEED_REV_LOADED_SPEED)));
        break; /* end case 133*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_513 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_843 ( TestOutput_input_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_843 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_843 ( TestOutput_input_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->fBeginTest */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBeginTest))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->fCancelTest */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fCancelTest))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->eActiveTestOutput */
      case 3: { 
        VCAST_TI_12_212 ( ((test_output_t *)(&(vcast_param->eActiveTestOutput))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_843 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_841 ( hour_meter_input_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_841 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_841 ( hour_meter_input_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulA4_1HourMeter */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulA4_1HourMeter))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ulA2_1HourMeter */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulA2_1HourMeter))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ulA3_1HourMeter */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulA3_1HourMeter))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ulA5_1HourMeter */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulA5_1HourMeter))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->ulA2_2HourMeter */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulA2_2HourMeter))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->ulM2_1HourMeter */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulM2_1HourMeter))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->ulM3_1HourMeter */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulM3_1HourMeter))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->ulM5_1HourMeter */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulM5_1HourMeter))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->ulPresenceHourMeter */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPresenceHourMeter))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->ulOdometer */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulOdometer))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->ulHydraulicsHourMeter */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulHydraulicsHourMeter))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->ulHydraulicsOilHourMeter */
      case 12: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulHydraulicsOilHourMeter))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->ulHydraulicsOilFilterHourMeter */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulHydraulicsOilFilterHourMeter))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->ulMainHoistPumpHourMeter */
      case 14: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulMainHoistPumpHourMeter))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->ulMastCableHourMeter */
      case 15: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulMastCableHourMeter))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->ulTruckRuntime */
      case 16: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTruckRuntime))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->ulLoginHourMeter */
      case 17: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulLoginHourMeter))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->ulKeyOnHourMeter */
      case 18: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulKeyOnHourMeter))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->ulTravelHourMeter */
      case 19: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTravelHourMeter))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->ulWorkHourMeter */
      case 20: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulWorkHourMeter))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->ulDriveUnitOilHourMeter */
      case 21: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulDriveUnitOilHourMeter))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->ulAmpHours */
      case 22: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAmpHours))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->ulM2_2HourMeter */
      case 23: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulM2_2HourMeter))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccyPumpHourMeter */
      case 24: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccyPumpHourMeter))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->ulM3_2HourMeter */
      case 25: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulM3_2HourMeter))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->ulA3_2HourMeter */
      case 26: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulA3_2HourMeter))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->ulM5_2HourMeter */
      case 27: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulM5_2HourMeter))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->ulH1_Access123 */
      case 28: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulH1_Access123))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->ulPmHourMeter */
      case 29: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPmHourMeter))));
        break; /* end case 29*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_841 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_22 ( Rack_Status_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_22 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_22 ( Rack_Status_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->fTargetSel */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTargetSel))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->fTargetReached */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTargetReached))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->fWrongDirection */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fWrongDirection))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->fError */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fError))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->fLoaded */
      case 5: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLoaded))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->fHeightValid */
      case 6: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHeightValid))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->fRackSelectWrongDirection */
      case 7: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRackSelectWrongDirection))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->fRackTargetReached */
      case 8: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRackTargetReached))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->fGetRackSelectError */
      case 9: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fGetRackSelectError))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->ubActiveRackGroup */
      case 10: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubActiveRackGroup))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->fRackSelectAllowed */
      case 11: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRackSelectAllowed))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->ubSelectedRack_to_Imm */
      case 12: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubSelectedRack_to_Imm))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->uwRackPosition_to_Imm */
      case 13: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwRackPosition_to_Imm))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->fLoaded_to_Imm */
      case 14: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLoaded_to_Imm))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->ubSelectedRack_to_VCM */
      case 15: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubSelectedRack_to_VCM))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->fLoaded_to_VCM */
      case 16: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLoaded_to_VCM))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->ubActionComplete */
      case 17: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubActionComplete))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->fRackSelected */
      case 18: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRackSelected))));
        break; /* end case 18*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_22 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An integer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_25 ( signed char *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_25 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_25 ( signed char *vcast_param ) 
{
  switch (vCAST_COMMAND) {
    case vCAST_PRINT :
      if ( vcast_param == 0)
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_integer(vCAST_OUTPUT_FILE, *vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      break;
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL :
    *vcast_param = ( signed char  ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL :
    *vcast_param = SCHAR_MIN;
    break;
  case vCAST_MID_VAL :
    *vcast_param = (SCHAR_MIN / 2) + (SCHAR_MAX / 2);
    break;
  case vCAST_LAST_VAL :
    *vcast_param = SCHAR_MAX;
    break;
  case vCAST_MIN_MINUS_1_VAL :
    *vcast_param = SCHAR_MIN;
    *vcast_param = *vcast_param - 1;
    break;
  case vCAST_MAX_PLUS_1_VAL :
    *vcast_param = SCHAR_MAX;
    *vcast_param = *vcast_param + 1;
    break;
  case vCAST_ZERO_VAL :
    *vcast_param = 0;
    break;
  default:
    break;
} /* end switch */
} /* end VCAST_TI_12_25 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_888 ( struct accum_data_meters *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_888 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_888 ( struct accum_data_meters *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->FramData */
      case 1: { 
        VCAST_TI_12_885 ( ((struct fram_accum_data_meters *)(&(vcast_param->FramData))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->Settings */
      case 2: { 
        VCAST_TI_12_889 ( ((struct accum_data *)(vcast_param->Settings)));
        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_888 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_966 ( struct mm_vehicle_manifest *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_966 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_966 ( struct mm_vehicle_manifest *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->szVehicleBundle_pn */
      case 1: { 
        VCAST_TI_12_967 ( ((char *)(vcast_param->szVehicleBundle_pn)));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->VCM */
      case 2: { 
        VCAST_TI_12_965 ( ((struct mm_local_proc *)(&(vcast_param->VCM))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->can */
      case 3: { 
        VCAST_TI_12_961 ( ((struct mm_can_module *)(&(vcast_param->can))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_966 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1000 ( enum alarm_tone_pattern *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1000 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1000 ( enum alarm_tone_pattern *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum alarm_tone_pattern ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = RESET_ALARM;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = MAX_ALARM;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_12_1000 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1058 ( EnergySource_output_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1058 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1058 ( EnergySource_output_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rVint */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rVint))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rVintRatio */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rVintRatio))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rRbatt */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRbatt))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rBatterySOC */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBatterySOC))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->uwBucketFillCount */
      case 5: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwBucketFillCount))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rRangeCheck1Percent */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRangeCheck1Percent))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rRangeCheck2Percent */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRangeCheck2Percent))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->rRangeCheck3Percent */
      case 8: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRangeCheck3Percent))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->ubRangeCheckAll */
      case 9: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubRangeCheckAll))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->rBatteryCurrent */
      case 10: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBatteryCurrent))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->rBatteryVolts */
      case 11: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBatteryVolts))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->rBatteryAh */
      case 12: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBatteryAh))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->rBatteryWh */
      case 13: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBatteryWh))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->fServiceBdiLiftEnabled */
      case 14: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fServiceBdiLiftEnabled))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->fPmLiftCutout */
      case 15: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fPmLiftCutout))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->ubGracePeriodsUsed */
      case 16: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubGracePeriodsUsed))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->ubHydLoadShedPct */
      case 17: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubHydLoadShedPct))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->rTxnLoadShedSpd */
      case 18: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTxnLoadShedSpd))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->rGracePeriodTimeElapsed */
      case 19: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGracePeriodTimeElapsed))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->r1Min_ESS_Irms */
      case 20: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r1Min_ESS_Irms))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->r5Min_ESS_Irms */
      case 21: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r5Min_ESS_Irms))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->r15Min_ESS_Irms */
      case 22: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r15Min_ESS_Irms))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->swAmbientEstimate */
      case 23: { 
        VCAST_TI_12_27 ( ((signed short *)(&(vcast_param->swAmbientEstimate))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->swMaxTemp */
      case 24: { 
        VCAST_TI_12_27 ( ((signed short *)(&(vcast_param->swMaxTemp))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->fFanRequired */
      case 25: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fFanRequired))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->rCurrentHist */
      case 26: { 
        VCAST_TI_12_1057 ( ((float *)(vcast_param->rCurrentHist)));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->fRegenActive */
      case 27: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRegenActive))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->rDisplayTCM1DCcurrentEst */
      case 28: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rDisplayTCM1DCcurrentEst))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->rDisplayTCM2DCcurrentEst */
      case 29: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rDisplayTCM2DCcurrentEst))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->rDisplayHCMDCcurrentEst */
      case 30: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rDisplayHCMDCcurrentEst))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->rDisplayHDMDCcurrentEst */
      case 31: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rDisplayHDMDCcurrentEst))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->rDisplaySCMDCcurrentEst */
      case 32: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rDisplaySCMDCcurrentEst))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->fLastPowerDownSOCBad */
      case 33: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLastPowerDownSOCBad))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->fVoltageInitComplete */
      case 34: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fVoltageInitComplete))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->rBatterySOC_Model */
      case 35: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBatterySOC_Model))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->rESS_FullPerformanceLevel */
      case 36: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rESS_FullPerformanceLevel))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->rESS_SOC */
      case 37: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rESS_SOC))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->rEWS_ShutdownPending */
      case 38: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rEWS_ShutdownPending))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->rESS_supply_Current */
      case 39: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rESS_supply_Current))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->swESS_supply_Temp */
      case 40: { 
        VCAST_TI_12_27 ( ((signed short *)(&(vcast_param->swESS_supply_Temp))));
        break; /* end case 40*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1058 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1060 ( function_active_output_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1060 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1060 ( function_active_output_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->fAuxRaiseActive */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAuxRaiseActive))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->fAuxLowerActive */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAuxLowerActive))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->mBullDozing */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->mBullDozing))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->mExtTiltActive */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->mExtTiltActive))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->mPivotActive */
      case 5: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->mPivotActive))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->mTraverseActive */
      case 6: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->mTraverseActive))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->mHydrOverrideActive */
      case 7: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->mHydrOverrideActive))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->fHcmSwMotorOverride */
      case 8: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHcmSwMotorOverride))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->fOsmLevel2ServiceModeActive */
      case 9: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fOsmLevel2ServiceModeActive))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->fOsmTestOutputsAllowed */
      case 10: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fOsmTestOutputsAllowed))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->fOsmOperationAllowed */
      case 11: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fOsmOperationAllowed))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->fShipModeActive */
      case 12: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fShipModeActive))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->fMainHoistActive */
      case 13: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainHoistActive))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->fMainRaiseActive */
      case 14: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainRaiseActive))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->fMainLowerActive */
      case 15: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainLowerActive))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->fMainRaiseGoActive */
      case 16: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainRaiseGoActive))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->fMainLowerGoActive */
      case 17: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainLowerGoActive))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->fLatchMainHydMtrOff */
      case 18: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLatchMainHydMtrOff))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->fHeatCycleLockoutActive */
      case 19: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHeatCycleLockoutActive))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->fShipModeInhibMRaiseActive */
      case 20: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fShipModeInhibMRaiseActive))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->fAnalyzerTimer */
      case 21: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAnalyzerTimer))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->fStrCmdActive */
      case 22: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStrCmdActive))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->fHydFlushActive */
      case 23: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHydFlushActive))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->fHydFlushAllowed */
      case 24: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHydFlushAllowed))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->fHydFlushInitiated */
      case 25: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHydFlushInitiated))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->fAutoAuxRaiseReqd */
      case 26: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAutoAuxRaiseReqd))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->fSpeedCutBackActive */
      case 27: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSpeedCutBackActive))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->fAccyFunctionActive */
      case 28: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAccyFunctionActive))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->fTpaActive */
      case 29: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTpaActive))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->fRaiseStopActive */
      case 30: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRaiseStopActive))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->fRaiseCutoutsActive */
      case 31: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRaiseCutoutsActive))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->fLowerCutoutsActive */
      case 32: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLowerCutoutsActive))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->eVelDir */
      case 33: { 
        VCAST_TI_12_117 ( ((VEL_TYPE *)(&(vcast_param->eVelDir))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->fReachActive */
      case 34: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fReachActive))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->fTiltActive */
      case 35: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTiltActive))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->fExtendActive */
      case 36: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fExtendActive))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->fSideshiftActive */
      case 37: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSideshiftActive))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->fSeenAccyNeutral */
      case 38: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSeenAccyNeutral))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->fTiltAtCutoutStop */
      case 39: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTiltAtCutoutStop))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->fAtTiltUpLimit */
      case 40: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAtTiltUpLimit))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->fAtTiltDownLimit */
      case 41: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAtTiltDownLimit))));
        break; /* end case 41*/
      } /* end case */
      /* Setting member variable vcast_param->eAccyVelDir */
      case 42: { 
        VCAST_TI_12_117 ( ((VEL_TYPE *)(&(vcast_param->eAccyVelDir))));
        break; /* end case 42*/
      } /* end case */
      /* Setting member variable vcast_param->fXpressActive */
      case 43: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fXpressActive))));
        break; /* end case 43*/
      } /* end case */
      /* Setting member variable vcast_param->eAuxVelDir */
      case 44: { 
        VCAST_TI_12_117 ( ((VEL_TYPE *)(&(vcast_param->eAuxVelDir))));
        break; /* end case 44*/
      } /* end case */
      /* Setting member variable vcast_param->fFifthFunctionActive */
      case 45: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fFifthFunctionActive))));
        break; /* end case 45*/
      } /* end case */
      /* Setting member variable vcast_param->eSideShiftAssistPos */
      case 46: { 
        VCAST_TI_12_232 ( ((SSP_Pos *)(&(vcast_param->eSideShiftAssistPos))));
        break; /* end case 46*/
      } /* end case */
      /* Setting member variable vcast_param->eTiltAssistPos */
      case 47: { 
        VCAST_TI_12_230 ( ((TPA_Pos *)(&(vcast_param->eTiltAssistPos))));
        break; /* end case 47*/
      } /* end case */
      /* Setting member variable vcast_param->fFifthFunctionSelected */
      case 48: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fFifthFunctionSelected))));
        break; /* end case 48*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1060 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1062 ( hyd_output_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1062 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1062 ( hyd_output_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulLiftContactorCmd */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulLiftContactorCmd))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->slHCMSpeedCommand */
      case 2: { 
        VCAST_TI_11_31 ( ((signed int *)(&(vcast_param->slHCMSpeedCommand))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rHCMTorqCommand */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rHCMTorqCommand))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->slHCMPWMCommand */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->slHCMPWMCommand))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->sbCurrentOffset */
      case 5: { 
        VCAST_TI_12_25 ( ((signed char *)(&(vcast_param->sbCurrentOffset))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->ulSVML */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSVML))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->ulSVMR */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSVMR))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->slHDMSpeedCommand */
      case 8: { 
        VCAST_TI_11_31 ( ((signed int *)(&(vcast_param->slHDMSpeedCommand))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY_DIRECTION */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY_DIRECTION))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY1_POS */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY1_POS))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY1_NEG */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY1_NEG))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY2_POS */
      case 12: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY2_POS))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY2_NEG */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY2_NEG))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY3_POS */
      case 14: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY3_POS))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY3_NEG */
      case 15: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY3_NEG))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_TILT_UP */
      case 16: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_TILT_UP))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_TILT_DOWN */
      case 17: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_TILT_DOWN))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY1_SELECT */
      case 18: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY1_SELECT))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY2_SELECT */
      case 19: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY2_SELECT))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_ACCY3_SELECT */
      case 20: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_ACCY3_SELECT))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_TILT_SELECT */
      case 21: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_TILT_SELECT))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_HOIST_BLOCKING */
      case 22: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_HOIST_BLOCKING))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_BYPASS */
      case 23: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_BYPASS))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->ulSV_SCV */
      case 24: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSV_SCV))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_MAIN_RAISE */
      case 25: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_MAIN_RAISE))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_MAIN_LOWER */
      case 26: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_MAIN_LOWER))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_AUX_RAISE */
      case 27: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_AUX_RAISE))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_AUX_LOWER */
      case 28: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_AUX_LOWER))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_ALL_ACCY */
      case 29: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_ALL_ACCY))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_ACCY1_POS */
      case 30: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_ACCY1_POS))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_ACCY1_NEG */
      case 31: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_ACCY1_NEG))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_ACCY2_POS */
      case 32: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_ACCY2_POS))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_ACCY2_NEG */
      case 33: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_ACCY2_NEG))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_ACCY3_POS */
      case 34: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_ACCY3_POS))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_ACCY3_NEG */
      case 35: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_ACCY3_NEG))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_TILT_UP */
      case 36: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_TILT_UP))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_TILT_DOWN */
      case 37: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_TILT_DOWN))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_CYL1 */
      case 38: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_CYL1))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_CYL2 */
      case 39: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_CYL2))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->fRackSelectWrongDirection */
      case 40: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRackSelectWrongDirection))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->RCO_STOP */
      case 41: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->RCO_STOP))));
        break; /* end case 41*/
      } /* end case */
      /* Setting member variable vcast_param->RCO_CUTOUT */
      case 42: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->RCO_CUTOUT))));
        break; /* end case 42*/
      } /* end case */
      /* Setting member variable vcast_param->fMainFuseTripped */
      case 43: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainFuseTripped))));
        break; /* end case 43*/
      } /* end case */
      /* Setting member variable vcast_param->gfStaticHoseBreaktripped */
      case 44: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->gfStaticHoseBreaktripped))));
        break; /* end case 44*/
      } /* end case */
      /* Setting member variable vcast_param->fPivotNotHome */
      case 45: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fPivotNotHome))));
        break; /* end case 45*/
      } /* end case */
      /* Setting member variable vcast_param->fTraverseNotHome */
      case 46: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTraverseNotHome))));
        break; /* end case 46*/
      } /* end case */
      /* Setting member variable vcast_param->fCenterHome */
      case 47: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fCenterHome))));
        break; /* end case 47*/
      } /* end case */
      /* Setting member variable vcast_param->fForkExtendIsHome */
      case 48: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fForkExtendIsHome))));
        break; /* end case 48*/
      } /* end case */
      /* Setting member variable vcast_param->fMainRaiseInMechLimit */
      case 49: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainRaiseInMechLimit))));
        break; /* end case 49*/
      } /* end case */
      /* Setting member variable vcast_param->fMainHoistThrotSRO */
      case 50: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainHoistThrotSRO))));
        break; /* end case 50*/
      } /* end case */
      /* Setting member variable vcast_param->fUseMainThrot1 */
      case 51: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fUseMainThrot1))));
        break; /* end case 51*/
      } /* end case */
      /* Setting member variable vcast_param->fMaxHeightNegSlowStatus */
      case 52: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMaxHeightNegSlowStatus))));
        break; /* end case 52*/
      } /* end case */
      /* Setting member variable vcast_param->fMaxHeightCutStatus */
      case 53: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMaxHeightCutStatus))));
        break; /* end case 53*/
      } /* end case */
      /* Setting member variable vcast_param->ulPV_CYL3 */
      case 54: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPV_CYL3))));
        break; /* end case 54*/
      } /* end case */
      /* Setting member variable vcast_param->ulSVARL */
      case 55: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSVARL))));
        break; /* end case 55*/
      } /* end case */
      /* Setting member variable vcast_param->fMaxHeightStopStatus */
      case 56: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMaxHeightStopStatus))));
        break; /* end case 56*/
      } /* end case */
      /* Setting member variable vcast_param->rThrottleScaledOut */
      case 57: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rThrottleScaledOut))));
        break; /* end case 57*/
      } /* end case */
      /* Setting member variable vcast_param->fHoistThrottleRampComplete */
      case 58: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHoistThrottleRampComplete))));
        break; /* end case 58*/
      } /* end case */
      /* Setting member variable vcast_param->fEnableValveHeater */
      case 59: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fEnableValveHeater))));
        break; /* end case 59*/
      } /* end case */
      /* Setting member variable vcast_param->fInSlowDownRegion */
      case 60: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fInSlowDownRegion))));
        break; /* end case 60*/
      } /* end case */
      /* Setting member variable vcast_param->fMainRaiseRequested */
      case 61: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainRaiseRequested))));
        break; /* end case 61*/
      } /* end case */
      /* Setting member variable vcast_param->fMainLowerRequested */
      case 62: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainLowerRequested))));
        break; /* end case 62*/
      } /* end case */
      /* Setting member variable vcast_param->ulSVMRL */
      case 63: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSVMRL))));
        break; /* end case 63*/
      } /* end case */
      /* Setting member variable vcast_param->fMaxHeightNegSlowConstSpdStatus */
      case 64: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMaxHeightNegSlowConstSpdStatus))));
        break; /* end case 64*/
      } /* end case */
      /* Setting member variable vcast_param->ulPRESSURE_CONTROL_VALVE */
      case 65: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPRESSURE_CONTROL_VALVE))));
        break; /* end case 65*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1062 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1064 ( steer_m2m_output_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1064 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1064 ( steer_m2m_output_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->fStopTrac */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStopTrac))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->uwSdmPwrCmdMode */
      case 2: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwSdmPwrCmdMode))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rWhAngTarget */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rWhAngTarget))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->fStrCmdActive */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStrCmdActive))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->fStrMtrMoving */
      case 5: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStrMtrMoving))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->eStrOpMode_State */
      case 6: { 
        VCAST_TI_12_103 ( ((StrOpModeState *)(&(vcast_param->eStrOpMode_State))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->fSteerOkClk */
      case 7: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSteerOkClk))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->rTrxSpdLmtWhAngCmd */
      case 8: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTrxSpdLmtWhAngCmd))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->fDuInRng */
      case 9: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fDuInRng))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->fStopSteering */
      case 10: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStopSteering))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->slFwdRevSteer */
      case 11: { 
        VCAST_TI_11_31 ( ((signed int *)(&(vcast_param->slFwdRevSteer))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->slStrPos_HndlPos */
      case 12: { 
        VCAST_TI_11_31 ( ((signed int *)(&(vcast_param->slStrPos_HndlPos))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->rStrSpd_DiffCnts */
      case 13: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rStrSpd_DiffCnts))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->slPosCmdDiff */
      case 14: { 
        VCAST_TI_11_31 ( ((signed int *)(&(vcast_param->slPosCmdDiff))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->fStrSpdDbDet */
      case 15: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStrSpdDbDet))));
        break; /* end case 15*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1064 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1066 ( sw_flag_output_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1066 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1066 ( sw_flag_output_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->fBrs1Flag */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBrs1Flag))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->fBrs2Flag */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBrs2Flag))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->fBrs3Flag */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBrs3Flag))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->fFps1Flag */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fFps1Flag))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->fFps2Flag */
      case 5: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fFps2Flag))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->fFps3Flag */
      case 6: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fFps3Flag))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->fDmsFlag */
      case 7: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fDmsFlag))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->fGtsFlag */
      case 8: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fGtsFlag))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->fBresFlag */
      case 9: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBresFlag))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->fStpsFlag */
      case 10: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStpsFlag))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->fEnsFlag */
      case 11: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fEnsFlag))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->fSesFlag */
      case 12: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSesFlag))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->fSposFlag */
      case 13: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSposFlag))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->fSlsFlag */
      case 14: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSlsFlag))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->fLms1Flag */
      case 15: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLms1Flag))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->fLms2Flag */
      case 16: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLms2Flag))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->fLms3Flag */
      case 17: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLms3Flag))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->fLms4Flag */
      case 18: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLms4Flag))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->fLms5Flag */
      case 19: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLms5Flag))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->fChbsFlag */
      case 20: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fChbsFlag))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->fChssFlag */
      case 21: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fChssFlag))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->fHmsFlag */
      case 22: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHmsFlag))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->fQpsFlag */
      case 23: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fQpsFlag))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->fQcsFlag */
      case 24: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fQcsFlag))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->fSasFlag */
      case 25: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSasFlag))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->fHssFlag */
      case 26: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHssFlag))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->fSbsFlag */
      case 27: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSbsFlag))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->fHps1Flag */
      case 28: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHps1Flag))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->fHps2Flag */
      case 29: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHps2Flag))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->fZss1Flag */
      case 30: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fZss1Flag))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->fZss2Flag */
      case 31: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fZss2Flag))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->fZss3Flag */
      case 32: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fZss3Flag))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->fSl2sFlag */
      case 33: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSl2sFlag))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->fSl1sFlag */
      case 34: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSl1sFlag))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->fOrsFlag */
      case 35: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fOrsFlag))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->fFssFlag */
      case 36: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fFssFlag))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->fGusFlag */
      case 37: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fGusFlag))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->fSbsEnsGtsFlag */
      case 38: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSbsEnsGtsFlag))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->fCtrlStopSwFlag */
      case 39: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fCtrlStopSwFlag))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->fRgsFlag */
      case 40: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRgsFlag))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->fBfsFlag */
      case 41: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBfsFlag))));
        break; /* end case 41*/
      } /* end case */
      /* Setting member variable vcast_param->fCtrlStopSw2Flag */
      case 42: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fCtrlStopSw2Flag))));
        break; /* end case 42*/
      } /* end case */
      /* Setting member variable vcast_param->fFssAccy3Flag */
      case 43: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fFssAccy3Flag))));
        break; /* end case 43*/
      } /* end case */
      /* Setting member variable vcast_param->fAHSFlag */
      case 44: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAHSFlag))));
        break; /* end case 44*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1066 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1068 ( traction_output_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1068 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1068 ( traction_output_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rTorqueCmd */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTorqueCmd))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rBrake1Cmd */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrake1Cmd))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rBrake2Cmd */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrake2Cmd))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rBrake3Cmd */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrake3Cmd))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rVehicleSpeed */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rVehicleSpeed))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rSpeedCommand */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSpeedCommand))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rDistanceTraveled */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rDistanceTraveled))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->ulFwdSwitch */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulFwdSwitch))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->ulRevSwitch */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulRevSwitch))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->fBrakeRequest */
      case 10: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBrakeRequest))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->fBrake_Coils_In_Lockout */
      case 11: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBrake_Coils_In_Lockout))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->eTractionState */
      case 12: { 
        VCAST_TI_12_109 ( ((TractionState *)(&(vcast_param->eTractionState))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->ulTravelAlarmCmd */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTravelAlarmCmd))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->eTractionMotorState */
      case 14: { 
        VCAST_TI_12_107 ( ((TractionMotorState *)(&(vcast_param->eTractionMotorState))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->fTrxAppClock */
      case 15: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTrxAppClock))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->fSlippageActiveStat */
      case 16: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSlippageActiveStat))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->rAccelCommanded */
      case 17: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccelCommanded))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->rAccelMeasured */
      case 18: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccelMeasured))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->rAccelCalculated */
      case 19: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccelCalculated))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->rX_Speed */
      case 20: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rX_Speed))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->slDirectionFlipper */
      case 21: { 
        VCAST_TI_11_31 ( ((signed int *)(&(vcast_param->slDirectionFlipper))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->fStatusWordOK */
      case 22: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStatusWordOK))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->fSpeedCutbackSRO */
      case 23: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSpeedCutbackSRO))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->ulStrobeCmd */
      case 24: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulStrobeCmd))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->rBrake1TorqueGap */
      case 25: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrake1TorqueGap))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->rBrake2TorqueGap */
      case 26: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBrake2TorqueGap))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->f811_TravelExpired */
      case 27: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->f811_TravelExpired))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->fThrottleSRO */
      case 28: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fThrottleSRO))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->fParkBrakeTestFail */
      case 29: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fParkBrakeTestFail))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->fParkBrakeTestActive */
      case 30: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fParkBrakeTestActive))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->fParkBrakeTestPending */
      case 31: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fParkBrakeTestPending))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->fSlippingInvsOut */
      case 32: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSlippingInvsOut))));
        break; /* end case 32*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1068 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1070 ( vehicle_sro_word_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1070 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1070 ( vehicle_sro_word_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulMainHoistSro */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulMainHoistSro))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ulMainHoistSro1 */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulMainHoistSro1))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ulMainHoistAdvise */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulMainHoistAdvise))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ulAuxHoistSro */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAuxHoistSro))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->ulAuxHoistSro1 */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAuxHoistSro1))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->ulAuxHoistAdvise */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAuxHoistAdvise))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->ulTiltSro */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTiltSro))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->ulTiltSro1 */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTiltSro1))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->ulTiltAdvise */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTiltAdvise))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy1Sro */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy1Sro))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy1Sro1 */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy1Sro1))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy1Advise */
      case 12: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy1Advise))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy2Sro */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy2Sro))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy2Sro1 */
      case 14: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy2Sro1))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy2Advise */
      case 15: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy2Advise))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy3Sro */
      case 16: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy3Sro))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy3Sro1 */
      case 17: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy3Sro1))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy3Advise */
      case 18: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy3Advise))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->ulTracSro */
      case 19: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTracSro))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->ulTracSro1 */
      case 20: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTracSro1))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->ulTracAdvise */
      case 21: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTracAdvise))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->ulTruckSro */
      case 22: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTruckSro))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->ulTruckSro1 */
      case 23: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTruckSro1))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->ulTruckAdvise */
      case 24: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTruckAdvise))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->ulMainHoistSro2 */
      case 25: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulMainHoistSro2))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->ulTiltSro2 */
      case 26: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTiltSro2))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->ulTracSro2 */
      case 27: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTracSro2))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy1Sro2 */
      case 28: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy1Sro2))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy2Sro2 */
      case 29: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy2Sro2))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->ulAccy3Sro2 */
      case 30: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAccy3Sro2))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->ulAuxHoistSro2 */
      case 31: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulAuxHoistSro2))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->ulTruckSro2 */
      case 32: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTruckSro2))));
        break; /* end case 32*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1070 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1072 ( VehicleFB_output_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1072 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1072 ( VehicleFB_output_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rForkHeight */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rForkHeight))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rForkLoad */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rForkLoad))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rMainHeight */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainHeight))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rMainHoistSpeed */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMainHoistSpeed))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxHeight */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxHeight))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rAuxHoistSpeed */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAuxHoistSpeed))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltPosition */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltPosition))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltSpeed */
      case 8: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltSpeed))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->rTiltLevelAngle */
      case 9: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTiltLevelAngle))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1Position */
      case 10: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1Position))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy1Speed */
      case 11: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy1Speed))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->fForkHome */
      case 12: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fForkHome))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2Position */
      case 13: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2Position))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->rAccy2Speed */
      case 14: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAccy2Speed))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->rGrade_truck_x_axis */
      case 15: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rGrade_truck_x_axis))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->rTruck_Pitch_angle */
      case 16: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTruck_Pitch_angle))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->rTruck_Roll_angle */
      case 17: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTruck_Roll_angle))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->cald_truck_x_axis_raw_accel */
      case 18: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->cald_truck_x_axis_raw_accel))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->cald_truck_y_axis_raw_accel */
      case 19: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->cald_truck_y_axis_raw_accel))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->cald_truck_z_axis_raw_accel */
      case 20: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->cald_truck_z_axis_raw_accel))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->eMastZone */
      case 21: { 
        VCAST_TI_12_69 ( ((MastZone *)(&(vcast_param->eMastZone))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->ePriHoistDirDelta */
      case 22: { 
        VCAST_TI_12_129 ( ((HoistDirectionBasedOnDelta *)(&(vcast_param->ePriHoistDirDelta))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->eSecHoistDirDelta */
      case 23: { 
        VCAST_TI_12_129 ( ((HoistDirectionBasedOnDelta *)(&(vcast_param->eSecHoistDirDelta))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->rPriHoistSpeed */
      case 24: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPriHoistSpeed))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->rSecHoistSpeed */
      case 25: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSecHoistSpeed))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->rPriHgtCountsCAL */
      case 26: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rPriHgtCountsCAL))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->rSecHgtCountsCAL */
      case 27: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSecHgtCountsCAL))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->fMainHydMtrEncoderOk */
      case 28: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fMainHydMtrEncoderOk))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->fAccyHydMtrEncoderOk */
      case 29: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAccyHydMtrEncoderOk))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->fTracMtr1EncoderOk */
      case 30: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTracMtr1EncoderOk))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->fTracMtr2EncoderOk */
      case 31: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTracMtr2EncoderOk))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->rFreeLiftHeight */
      case 32: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rFreeLiftHeight))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->rDisplay_Load_weight */
      case 33: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rDisplay_Load_weight))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->fTestForProductivityLoaded */
      case 34: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTestForProductivityLoaded))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->rTruckAccel_from_accelerometer */
      case 35: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTruckAccel_from_accelerometer))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->rVehicleSpeed_from_accelerometer */
      case 36: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rVehicleSpeed_from_accelerometer))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->rTruck_Yaw_Angle */
      case 37: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTruck_Yaw_Angle))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->rRaw_gyro_Truck_x_axis */
      case 38: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRaw_gyro_Truck_x_axis))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->rRaw_gyro_Truck_y_axis */
      case 39: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRaw_gyro_Truck_y_axis))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->rRaw_gyro_Truck_z_axis */
      case 40: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rRaw_gyro_Truck_z_axis))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->eAboveHeightLimit */
      case 41: { 
        VCAST_TI_12_218 ( ((AboveHeightLimit *)(&(vcast_param->eAboveHeightLimit))));
        break; /* end case 41*/
      } /* end case */
      /* Setting member variable vcast_param->fAbove_Free_lift_height */
      case 42: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAbove_Free_lift_height))));
        break; /* end case 42*/
      } /* end case */
      /* Setting member variable vcast_param->rDisplayHoistSpeed */
      case 43: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rDisplayHoistSpeed))));
        break; /* end case 43*/
      } /* end case */
      /* Setting member variable vcast_param->fVirtMainHeightPrecisionLoss */
      case 44: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fVirtMainHeightPrecisionLoss))));
        break; /* end case 44*/
      } /* end case */
      /* Setting member variable vcast_param->fVirtMainHeightValid */
      case 45: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fVirtMainHeightValid))));
        break; /* end case 45*/
      } /* end case */
      /* Setting member variable vcast_param->rVirtMainHeight_m */
      case 46: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rVirtMainHeight_m))));
        break; /* end case 46*/
      } /* end case */
      /* Setting member variable vcast_param->fTpaLevel */
      case 47: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTpaLevel))));
        break; /* end case 47*/
      } /* end case */
      /* Setting member variable vcast_param->ulCdmWeightHeightLimit */
      case 48: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCdmWeightHeightLimit))));
        break; /* end case 48*/
      } /* end case */
      /* Setting member variable vcast_param->ubCdmEvent */
      case 49: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubCdmEvent))));
        break; /* end case 49*/
      } /* end case */
      /* Setting member variable vcast_param->fLaserLine */
      case 50: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLaserLine))));
        break; /* end case 50*/
      } /* end case */
      /* Setting member variable vcast_param->fBlueLightPUF */
      case 51: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBlueLightPUF))));
        break; /* end case 51*/
      } /* end case */
      /* Setting member variable vcast_param->fBlueLightFF */
      case 52: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBlueLightFF))));
        break; /* end case 52*/
      } /* end case */
      /* Setting member variable vcast_param->rTemp */
      case 53: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTemp))));
        break; /* end case 53*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1072 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1074 ( brake_analyzer_out_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1074 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1074 ( brake_analyzer_out_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rCBResistance */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rCBResistance))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rIBResistance */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rIBResistance))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rOBResistance */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rOBResistance))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1074 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1076 ( steer_dx_out_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1076 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1076 ( steer_dx_out_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulU1StrSuperFreshCount */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulU1StrSuperFreshCount))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->fStrU1OkCrossCk */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStrU1OkCrossCk))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->fIsStopTractCrossCkDetect */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fIsStopTractCrossCkDetect))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->fStrSnsrCrossCkDetect */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fStrSnsrCrossCkDetect))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rTempStrPos */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTempStrPos))));
        break; /* end case 5*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1076 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1078 ( AutoCal_Mcc_Calib_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1078 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1078 ( AutoCal_Mcc_Calib_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulMode */
      case 1: { 
        VCAST_TI_12_6 ( ((CalibMode *)(&(vcast_param->ulMode))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubState */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubState))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ubMsgId */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubMsgId))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rProcessVar */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rProcessVar))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->ulAction */
      case 5: { 
        VCAST_TI_12_134 ( ((CALIB_ACTION *)(&(vcast_param->ulAction))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->fUsePv */
      case 6: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fUsePv))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->uwTimeDelay */
      case 7: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwTimeDelay))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->ubLevel */
      case 8: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubLevel))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->ubCancelAllowed */
      case 9: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubCancelAllowed))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->uwActiveOptions_obs */
      case 10: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwActiveOptions_obs))));
        break; /* end case 10*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1078 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1080 ( AutoCal_PvFormat_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1080 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1080 ( AutoCal_PvFormat_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rMin */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMin))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rMax */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMax))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rScaling */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rScaling))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->eUnits */
      case 4: { 
        VCAST_TI_12_132 ( ((CalibUnits *)(&(vcast_param->eUnits))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->ubDecDigits */
      case 5: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubDecDigits))));
        break; /* end case 5*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1080 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1082 ( AutoCal_DeFormat_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1082 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1082 ( AutoCal_DeFormat_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rMin */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMin))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rMax */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMax))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rScaling */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rScaling))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rIncrement */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rIncrement))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->eUnits */
      case 5: { 
        VCAST_TI_12_132 ( ((CalibUnits *)(&(vcast_param->eUnits))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->ubDecDigits */
      case 6: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubDecDigits))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->rInitValue */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rInitValue))));
        break; /* end case 7*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1082 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1084 ( AutoCal_Steps_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1084 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1084 ( AutoCal_Steps_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ubTotalSteps */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubTotalSteps))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubCurrentStep */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubCurrentStep))));
        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1084 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1086 ( AGV_output_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1086 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1086 ( AGV_output_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rHydraulicThrottle */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rHydraulicThrottle))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rTractionThrottle */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTractionThrottle))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rTractionSpeedCmd */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTractionSpeedCmd))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rSteerCommand1 */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSteerCommand1))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rSteerCommand2 */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rSteerCommand2))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->rHeight */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rHeight))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->sbHoistRaiseOrLower */
      case 7: { 
        VCAST_TI_12_25 ( ((signed char *)(&(vcast_param->sbHoistRaiseOrLower))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->fAutoMode */
      case 8: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAutoMode))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->fBrake */
      case 9: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBrake))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->ubTruckState */
      case 10: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubTruckState))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->fLostAutoSignal */
      case 11: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fLostAutoSignal))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->fParkBrake */
      case 12: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fParkBrake))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->fTestActive */
      case 13: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTestActive))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->fParkBrakeTestPendingAGV */
      case 14: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fParkBrakeTestPendingAGV))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->rTractionRate */
      case 15: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTractionRate))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->rTractionSpeedClip */
      case 16: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rTractionSpeedClip))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->ulPDS_Count */
      case 17: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPDS_Count))));
        break; /* end case 17*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_1086 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1087 ( struct info1_model *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1087 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1087 ( struct info1_model *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->pszModelNumber */
      case 1: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->pszModelNumber))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->uwModelNumber */
      case 2: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwModelNumber))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->uwModelDash */
      case 3: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwModelDash))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_1087 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_827 ( AutoCal_Status_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_827 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_827 ( AutoCal_Status_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulThrottle */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulThrottle))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ulSensor */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSensor))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ulValve */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulValve))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_827 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_839 ( UserCutOuts_calib_c2m_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_839 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_839 ( UserCutOuts_calib_c2m_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->slHt */
      case 1: { 
        VCAST_TI_12_834 ( ((signed int *)(vcast_param->slHt)));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->eType */
      case 2: { 
        VCAST_TI_12_835 ( ((CUTOUT_TYPE_TYPE *)(vcast_param->eType)));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->eZone */
      case 3: { 
        VCAST_TI_12_836 ( ((CUTOUT_ZONE_TYPE *)(vcast_param->eZone)));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->eTrigger */
      case 4: { 
        VCAST_TI_12_837 ( ((SD_TRIGGER_TYPE *)(vcast_param->eTrigger)));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rSpeed */
      case 5: { 
        VCAST_TI_12_479 ( ((float *)(vcast_param->rSpeed)));
        break; /* end case 5*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_839 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_831 ( RackSelectSetups_calib_c2m_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_831 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_831 ( RackSelectSetups_calib_c2m_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwRackHeight */
      case 1: { 
        VCAST_TI_12_829 ( ((unsigned short *)(vcast_param->uwRackHeight)));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->slLoadedOffset */
      case 2: { 
        VCAST_TI_12_830 ( ((signed int *)(vcast_param->slLoadedOffset)));
        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_831 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1093 ( struct fram_BirthCert *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1093 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1093 ( struct fram_BirthCert *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->Data */
      case 1: { 
        VCAST_TI_12_1088 ( ((struct fram_birthCert_data *)(&(vcast_param->Data))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->uwFramCRC */
      case 2: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwFramCRC))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->uwBCStringCRC */
      case 3: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwBCStringCRC))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_1093 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_14 ( unsigned char vcast_param[IPS_MAX_NUM] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_14 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_14 ( unsigned char vcast_param[IPS_MAX_NUM] ) 
{
  {
    int VCAST_TI_12_14_array_index = 0;
    int VCAST_TI_12_14_index = 0;
    int VCAST_TI_12_14_first, VCAST_TI_12_14_last;
    int VCAST_TI_12_14_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_14_local_field = 0;
    int VCAST_TI_12_14_value_printed = 0;
    int VCAST_TI_12_14_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_14_first, &VCAST_TI_12_14_last, &VCAST_TI_12_14_more_data);
    VCAST_TI_12_14_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_14_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IPS_MAX_NUM);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_14_upper = IPS_MAX_NUM;
      for (VCAST_TI_12_14_array_index=0; VCAST_TI_12_14_array_index< VCAST_TI_12_14_upper; VCAST_TI_12_14_array_index++){
        if ( (VCAST_TI_12_14_index >= VCAST_TI_12_14_first) && ( VCAST_TI_12_14_index <= VCAST_TI_12_14_last)){
          if ( VCAST_TI_12_14_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_14_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_12_14_index]));
          VCAST_TI_12_14_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_14_local_field;
        } /* if */
        if (VCAST_TI_12_14_index >= VCAST_TI_12_14_last)
          break;
        VCAST_TI_12_14_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_14_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_14 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_993 ( struct imm_control_info *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_993 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_993 ( struct imm_control_info *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulVcm2ImmFlags */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulVcm2ImmFlags))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ulImm2VcmFlags */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulImm2VcmFlags))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ubImm2VcmCmds */
      case 3: { 
        VCAST_TI_12_7 ( ((unsigned char *)(vcast_param->ubImm2VcmCmds)));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ubImm2VcmCommand */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubImm2VcmCommand))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rResetDelay */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rResetDelay))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->r811Time */
      case 6: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r811Time))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->r811Distance */
      case 7: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->r811Distance))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->ubTestOutputFlags */
      case 8: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubTestOutputFlags))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->ulUtilityTestFlags */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulUtilityTestFlags))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->ubShipModeFlag */
      case 10: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubShipModeFlag))));
        break; /* end case 10*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_993 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_11 ( struct imm_ips_data vcast_param[IPS_MAX_NUM] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_11 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_11 ( struct imm_ips_data vcast_param[IPS_MAX_NUM] ) 
{
  {
    int VCAST_TI_12_11_array_index = 0;
    int VCAST_TI_12_11_index = 0;
    int VCAST_TI_12_11_first, VCAST_TI_12_11_last;
    int VCAST_TI_12_11_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_11_local_field = 0;
    int VCAST_TI_12_11_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_11_first, &VCAST_TI_12_11_last, &VCAST_TI_12_11_more_data);
    VCAST_TI_12_11_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_11_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IPS_MAX_NUM);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_11_upper = IPS_MAX_NUM;
      for (VCAST_TI_12_11_array_index=0; VCAST_TI_12_11_array_index< VCAST_TI_12_11_upper; VCAST_TI_12_11_array_index++){
        if ( (VCAST_TI_12_11_index >= VCAST_TI_12_11_first) && ( VCAST_TI_12_11_index <= VCAST_TI_12_11_last)){
          VCAST_TI_12_994 ( &(vcast_param[VCAST_TI_12_11_index]));
          VCAST_TI_12_11_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_11_local_field;
        } /* if */
        if (VCAST_TI_12_11_index >= VCAST_TI_12_11_last)
          break;
        VCAST_TI_12_11_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_11_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_11 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1094 ( unsigned char vcast_param[8U - 4U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1094 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1094 ( unsigned char vcast_param[8U - 4U] ) 
{
  {
    int VCAST_TI_12_1094_array_index = 0;
    int VCAST_TI_12_1094_index = 0;
    int VCAST_TI_12_1094_first, VCAST_TI_12_1094_last;
    int VCAST_TI_12_1094_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1094_local_field = 0;
    int VCAST_TI_12_1094_value_printed = 0;
    int VCAST_TI_12_1094_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_1094_first, &VCAST_TI_12_1094_last, &VCAST_TI_12_1094_more_data);
    VCAST_TI_12_1094_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1094_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U - 4U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1094_upper = 8U - 4U;
      for (VCAST_TI_12_1094_array_index=0; VCAST_TI_12_1094_array_index< VCAST_TI_12_1094_upper; VCAST_TI_12_1094_array_index++){
        if ( (VCAST_TI_12_1094_index >= VCAST_TI_12_1094_first) && ( VCAST_TI_12_1094_index <= VCAST_TI_12_1094_last)){
          if ( VCAST_TI_12_1094_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_1094_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_12_1094_index]));
          VCAST_TI_12_1094_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1094_local_field;
        } /* if */
        if (VCAST_TI_12_1094_index >= VCAST_TI_12_1094_last)
          break;
        VCAST_TI_12_1094_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1094_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1094 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1095 ( char vcast_param[4U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1095 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1095 ( char vcast_param[4U] ) 
{
  {
    int VCAST_TI_12_1095_array_index = 0;
    int VCAST_TI_12_1095_index = 0;
    int VCAST_TI_12_1095_first, VCAST_TI_12_1095_last;
    int VCAST_TI_12_1095_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1095_local_field = 0;
    int VCAST_TI_12_1095_value_printed = 0;
    int VCAST_TI_12_1095_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_1095_first, &VCAST_TI_12_1095_last, &VCAST_TI_12_1095_more_data);
    VCAST_TI_12_1095_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1095_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1095_upper = 4U;
      for (VCAST_TI_12_1095_array_index=0; VCAST_TI_12_1095_array_index< VCAST_TI_12_1095_upper; VCAST_TI_12_1095_array_index++){
        if ( (VCAST_TI_12_1095_index >= VCAST_TI_12_1095_first) && ( VCAST_TI_12_1095_index <= VCAST_TI_12_1095_last)){
          if ( VCAST_TI_12_1095_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_1095_upper);
          else
            VCAST_TI_8_4 ( &(vcast_param[VCAST_TI_12_1095_index]));
          VCAST_TI_12_1095_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1095_local_field;
        } /* if */
        if (VCAST_TI_12_1095_index >= VCAST_TI_12_1095_last)
          break;
        VCAST_TI_12_1095_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1095_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1095 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_20 ( unsigned short vcast_param[8U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_20 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_20 ( unsigned short vcast_param[8U] ) 
{
  {
    int VCAST_TI_12_20_array_index = 0;
    int VCAST_TI_12_20_index = 0;
    int VCAST_TI_12_20_first, VCAST_TI_12_20_last;
    int VCAST_TI_12_20_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_20_local_field = 0;
    int VCAST_TI_12_20_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_20_first, &VCAST_TI_12_20_last, &VCAST_TI_12_20_more_data);
    VCAST_TI_12_20_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_20_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_20_upper = 8U;
      for (VCAST_TI_12_20_array_index=0; VCAST_TI_12_20_array_index< VCAST_TI_12_20_upper; VCAST_TI_12_20_array_index++){
        if ( (VCAST_TI_12_20_index >= VCAST_TI_12_20_first) && ( VCAST_TI_12_20_index <= VCAST_TI_12_20_last)){
          VCAST_TI_9_16 ( &(vcast_param[VCAST_TI_12_20_index]));
          VCAST_TI_12_20_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_20_local_field;
        } /* if */
        if (VCAST_TI_12_20_index >= VCAST_TI_12_20_last)
          break;
        VCAST_TI_12_20_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_20_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_20 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An integer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_19 ( unsigned long long *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_19 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_19 ( unsigned long long *vcast_param ) 
{
  switch (vCAST_COMMAND) {
    case vCAST_PRINT :
      if ( vcast_param == 0)
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_unsigned_long_long(vCAST_OUTPUT_FILE, *vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      break;
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL :
    *vcast_param = ( unsigned long long  ) vCAST_VALUE_UNSIGNED;
    break;
  case vCAST_FIRST_VAL :
    *vcast_param = 0;
    break;
  case vCAST_MID_VAL :
    *vcast_param = (ULLONG_MAX / 2);
    break;
  case vCAST_LAST_VAL :
    *vcast_param = ULLONG_MAX;
    break;
  case vCAST_MIN_MINUS_1_VAL :
    *vcast_param = 0;
    *vcast_param = *vcast_param - 1;
    break;
  case vCAST_MAX_PLUS_1_VAL :
    *vcast_param = ULLONG_MAX;
    *vcast_param = *vcast_param + 1;
    break;
  case vCAST_ZERO_VAL :
    *vcast_param = 0;
    break;
  default:
    break;
} /* end switch */
} /* end VCAST_TI_12_19 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1096 ( struct aecp_object vcast_param[4] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1096 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1096 ( struct aecp_object vcast_param[4] ) 
{
  {
    int VCAST_TI_12_1096_array_index = 0;
    int VCAST_TI_12_1096_index = 0;
    int VCAST_TI_12_1096_first, VCAST_TI_12_1096_last;
    int VCAST_TI_12_1096_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1096_local_field = 0;
    int VCAST_TI_12_1096_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1096_first, &VCAST_TI_12_1096_last, &VCAST_TI_12_1096_more_data);
    VCAST_TI_12_1096_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1096_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1096_upper = 4;
      for (VCAST_TI_12_1096_array_index=0; VCAST_TI_12_1096_array_index< VCAST_TI_12_1096_upper; VCAST_TI_12_1096_array_index++){
        if ( (VCAST_TI_12_1096_index >= VCAST_TI_12_1096_first) && ( VCAST_TI_12_1096_index <= VCAST_TI_12_1096_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1096_index]));
          VCAST_TI_12_1096_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1096_local_field;
        } /* if */
        if (VCAST_TI_12_1096_index >= VCAST_TI_12_1096_last)
          break;
        VCAST_TI_12_1096_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1096_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1096 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1097 ( struct aecp_object vcast_param[34] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1097 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1097 ( struct aecp_object vcast_param[34] ) 
{
  {
    int VCAST_TI_12_1097_array_index = 0;
    int VCAST_TI_12_1097_index = 0;
    int VCAST_TI_12_1097_first, VCAST_TI_12_1097_last;
    int VCAST_TI_12_1097_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1097_local_field = 0;
    int VCAST_TI_12_1097_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1097_first, &VCAST_TI_12_1097_last, &VCAST_TI_12_1097_more_data);
    VCAST_TI_12_1097_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1097_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,34);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1097_upper = 34;
      for (VCAST_TI_12_1097_array_index=0; VCAST_TI_12_1097_array_index< VCAST_TI_12_1097_upper; VCAST_TI_12_1097_array_index++){
        if ( (VCAST_TI_12_1097_index >= VCAST_TI_12_1097_first) && ( VCAST_TI_12_1097_index <= VCAST_TI_12_1097_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1097_index]));
          VCAST_TI_12_1097_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1097_local_field;
        } /* if */
        if (VCAST_TI_12_1097_index >= VCAST_TI_12_1097_last)
          break;
        VCAST_TI_12_1097_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1097_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1097 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1098 ( struct aecp_object vcast_param[1] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1098 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1098 ( struct aecp_object vcast_param[1] ) 
{
  {
    int VCAST_TI_12_1098_array_index = 0;
    int VCAST_TI_12_1098_index = 0;
    int VCAST_TI_12_1098_first, VCAST_TI_12_1098_last;
    int VCAST_TI_12_1098_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1098_local_field = 0;
    int VCAST_TI_12_1098_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1098_first, &VCAST_TI_12_1098_last, &VCAST_TI_12_1098_more_data);
    VCAST_TI_12_1098_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1098_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1098_upper = 1;
      for (VCAST_TI_12_1098_array_index=0; VCAST_TI_12_1098_array_index< VCAST_TI_12_1098_upper; VCAST_TI_12_1098_array_index++){
        if ( (VCAST_TI_12_1098_index >= VCAST_TI_12_1098_first) && ( VCAST_TI_12_1098_index <= VCAST_TI_12_1098_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1098_index]));
          VCAST_TI_12_1098_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1098_local_field;
        } /* if */
        if (VCAST_TI_12_1098_index >= VCAST_TI_12_1098_last)
          break;
        VCAST_TI_12_1098_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1098_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1098 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1099 ( struct aecp_object vcast_param[50] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1099 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1099 ( struct aecp_object vcast_param[50] ) 
{
  {
    int VCAST_TI_12_1099_array_index = 0;
    int VCAST_TI_12_1099_index = 0;
    int VCAST_TI_12_1099_first, VCAST_TI_12_1099_last;
    int VCAST_TI_12_1099_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1099_local_field = 0;
    int VCAST_TI_12_1099_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1099_first, &VCAST_TI_12_1099_last, &VCAST_TI_12_1099_more_data);
    VCAST_TI_12_1099_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1099_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,50);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1099_upper = 50;
      for (VCAST_TI_12_1099_array_index=0; VCAST_TI_12_1099_array_index< VCAST_TI_12_1099_upper; VCAST_TI_12_1099_array_index++){
        if ( (VCAST_TI_12_1099_index >= VCAST_TI_12_1099_first) && ( VCAST_TI_12_1099_index <= VCAST_TI_12_1099_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1099_index]));
          VCAST_TI_12_1099_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1099_local_field;
        } /* if */
        if (VCAST_TI_12_1099_index >= VCAST_TI_12_1099_last)
          break;
        VCAST_TI_12_1099_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1099_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1099 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_3 ( struct aecp_object vcast_param[8U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_3 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_3 ( struct aecp_object vcast_param[8U] ) 
{
  {
    int VCAST_TI_12_3_array_index = 0;
    int VCAST_TI_12_3_index = 0;
    int VCAST_TI_12_3_first, VCAST_TI_12_3_last;
    int VCAST_TI_12_3_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_3_local_field = 0;
    int VCAST_TI_12_3_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_3_first, &VCAST_TI_12_3_last, &VCAST_TI_12_3_more_data);
    VCAST_TI_12_3_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_3_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_3_upper = 8U;
      for (VCAST_TI_12_3_array_index=0; VCAST_TI_12_3_array_index< VCAST_TI_12_3_upper; VCAST_TI_12_3_array_index++){
        if ( (VCAST_TI_12_3_index >= VCAST_TI_12_3_first) && ( VCAST_TI_12_3_index <= VCAST_TI_12_3_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_3_index]));
          VCAST_TI_12_3_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_3_local_field;
        } /* if */
        if (VCAST_TI_12_3_index >= VCAST_TI_12_3_last)
          break;
        VCAST_TI_12_3_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_3_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_3 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1102 ( struct aecp_object vcast_param[12] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1102 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1102 ( struct aecp_object vcast_param[12] ) 
{
  {
    int VCAST_TI_12_1102_array_index = 0;
    int VCAST_TI_12_1102_index = 0;
    int VCAST_TI_12_1102_first, VCAST_TI_12_1102_last;
    int VCAST_TI_12_1102_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1102_local_field = 0;
    int VCAST_TI_12_1102_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1102_first, &VCAST_TI_12_1102_last, &VCAST_TI_12_1102_more_data);
    VCAST_TI_12_1102_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1102_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1102_upper = 12;
      for (VCAST_TI_12_1102_array_index=0; VCAST_TI_12_1102_array_index< VCAST_TI_12_1102_upper; VCAST_TI_12_1102_array_index++){
        if ( (VCAST_TI_12_1102_index >= VCAST_TI_12_1102_first) && ( VCAST_TI_12_1102_index <= VCAST_TI_12_1102_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1102_index]));
          VCAST_TI_12_1102_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1102_local_field;
        } /* if */
        if (VCAST_TI_12_1102_index >= VCAST_TI_12_1102_last)
          break;
        VCAST_TI_12_1102_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1102_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1102 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1103 ( struct aecp_object vcast_param[7] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1103 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1103 ( struct aecp_object vcast_param[7] ) 
{
  {
    int VCAST_TI_12_1103_array_index = 0;
    int VCAST_TI_12_1103_index = 0;
    int VCAST_TI_12_1103_first, VCAST_TI_12_1103_last;
    int VCAST_TI_12_1103_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1103_local_field = 0;
    int VCAST_TI_12_1103_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1103_first, &VCAST_TI_12_1103_last, &VCAST_TI_12_1103_more_data);
    VCAST_TI_12_1103_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1103_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1103_upper = 7;
      for (VCAST_TI_12_1103_array_index=0; VCAST_TI_12_1103_array_index< VCAST_TI_12_1103_upper; VCAST_TI_12_1103_array_index++){
        if ( (VCAST_TI_12_1103_index >= VCAST_TI_12_1103_first) && ( VCAST_TI_12_1103_index <= VCAST_TI_12_1103_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1103_index]));
          VCAST_TI_12_1103_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1103_local_field;
        } /* if */
        if (VCAST_TI_12_1103_index >= VCAST_TI_12_1103_last)
          break;
        VCAST_TI_12_1103_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1103_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1103 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1104 ( struct aecp_object vcast_param[3] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1104 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1104 ( struct aecp_object vcast_param[3] ) 
{
  {
    int VCAST_TI_12_1104_array_index = 0;
    int VCAST_TI_12_1104_index = 0;
    int VCAST_TI_12_1104_first, VCAST_TI_12_1104_last;
    int VCAST_TI_12_1104_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1104_local_field = 0;
    int VCAST_TI_12_1104_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1104_first, &VCAST_TI_12_1104_last, &VCAST_TI_12_1104_more_data);
    VCAST_TI_12_1104_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1104_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1104_upper = 3;
      for (VCAST_TI_12_1104_array_index=0; VCAST_TI_12_1104_array_index< VCAST_TI_12_1104_upper; VCAST_TI_12_1104_array_index++){
        if ( (VCAST_TI_12_1104_index >= VCAST_TI_12_1104_first) && ( VCAST_TI_12_1104_index <= VCAST_TI_12_1104_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1104_index]));
          VCAST_TI_12_1104_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1104_local_field;
        } /* if */
        if (VCAST_TI_12_1104_index >= VCAST_TI_12_1104_last)
          break;
        VCAST_TI_12_1104_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1104_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1104 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1105 ( struct aecp_object vcast_param[31U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1105 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1105 ( struct aecp_object vcast_param[31U] ) 
{
  {
    int VCAST_TI_12_1105_array_index = 0;
    int VCAST_TI_12_1105_index = 0;
    int VCAST_TI_12_1105_first, VCAST_TI_12_1105_last;
    int VCAST_TI_12_1105_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1105_local_field = 0;
    int VCAST_TI_12_1105_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1105_first, &VCAST_TI_12_1105_last, &VCAST_TI_12_1105_more_data);
    VCAST_TI_12_1105_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1105_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,31U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1105_upper = 31U;
      for (VCAST_TI_12_1105_array_index=0; VCAST_TI_12_1105_array_index< VCAST_TI_12_1105_upper; VCAST_TI_12_1105_array_index++){
        if ( (VCAST_TI_12_1105_index >= VCAST_TI_12_1105_first) && ( VCAST_TI_12_1105_index <= VCAST_TI_12_1105_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1105_index]));
          VCAST_TI_12_1105_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1105_local_field;
        } /* if */
        if (VCAST_TI_12_1105_index >= VCAST_TI_12_1105_last)
          break;
        VCAST_TI_12_1105_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1105_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1105 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1106 ( struct aecp_object vcast_param[2] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1106 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1106 ( struct aecp_object vcast_param[2] ) 
{
  {
    int VCAST_TI_12_1106_array_index = 0;
    int VCAST_TI_12_1106_index = 0;
    int VCAST_TI_12_1106_first, VCAST_TI_12_1106_last;
    int VCAST_TI_12_1106_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1106_local_field = 0;
    int VCAST_TI_12_1106_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1106_first, &VCAST_TI_12_1106_last, &VCAST_TI_12_1106_more_data);
    VCAST_TI_12_1106_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1106_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1106_upper = 2;
      for (VCAST_TI_12_1106_array_index=0; VCAST_TI_12_1106_array_index< VCAST_TI_12_1106_upper; VCAST_TI_12_1106_array_index++){
        if ( (VCAST_TI_12_1106_index >= VCAST_TI_12_1106_first) && ( VCAST_TI_12_1106_index <= VCAST_TI_12_1106_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1106_index]));
          VCAST_TI_12_1106_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1106_local_field;
        } /* if */
        if (VCAST_TI_12_1106_index >= VCAST_TI_12_1106_last)
          break;
        VCAST_TI_12_1106_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1106_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1106 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1107 ( struct aecp_object vcast_param[8U][28U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1107 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1107 ( struct aecp_object vcast_param[8U][28U] ) 
{
  {
    int VCAST_TI_12_1107_array_index = 0;
    int VCAST_TI_12_1107_index = 0;
    int VCAST_TI_12_1107_first, VCAST_TI_12_1107_last;
    int VCAST_TI_12_1107_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1107_local_field = 0;
    int VCAST_TI_12_1107_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1107_first, &VCAST_TI_12_1107_last, &VCAST_TI_12_1107_more_data);
    VCAST_TI_12_1107_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1107_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,28U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1107_upper = 8U;
      for (VCAST_TI_12_1107_array_index=0; VCAST_TI_12_1107_array_index< VCAST_TI_12_1107_upper; VCAST_TI_12_1107_array_index++){
        if ( (VCAST_TI_12_1107_index >= VCAST_TI_12_1107_first) && ( VCAST_TI_12_1107_index <= VCAST_TI_12_1107_last)){
          VCAST_TI_12_1108 ( vcast_param[VCAST_TI_12_1107_index]);
          VCAST_TI_12_1107_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1107_local_field;
        } /* if */
        if (VCAST_TI_12_1107_index >= VCAST_TI_12_1107_last)
          break;
        VCAST_TI_12_1107_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1107_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1107 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1110 ( struct aecp_object vcast_param[9] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1110 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1110 ( struct aecp_object vcast_param[9] ) 
{
  {
    int VCAST_TI_12_1110_array_index = 0;
    int VCAST_TI_12_1110_index = 0;
    int VCAST_TI_12_1110_first, VCAST_TI_12_1110_last;
    int VCAST_TI_12_1110_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1110_local_field = 0;
    int VCAST_TI_12_1110_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1110_first, &VCAST_TI_12_1110_last, &VCAST_TI_12_1110_more_data);
    VCAST_TI_12_1110_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1110_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1110_upper = 9;
      for (VCAST_TI_12_1110_array_index=0; VCAST_TI_12_1110_array_index< VCAST_TI_12_1110_upper; VCAST_TI_12_1110_array_index++){
        if ( (VCAST_TI_12_1110_index >= VCAST_TI_12_1110_first) && ( VCAST_TI_12_1110_index <= VCAST_TI_12_1110_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1110_index]));
          VCAST_TI_12_1110_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1110_local_field;
        } /* if */
        if (VCAST_TI_12_1110_index >= VCAST_TI_12_1110_last)
          break;
        VCAST_TI_12_1110_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1110_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1110 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_853 ( enum ips_index *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_853 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_853 ( enum ips_index *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum ips_index ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = IPS_1;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = IPS_MAX_NUM;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_12_853 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_9 ( struct dem_report_record vcast_param[16U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_9 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_9 ( struct dem_report_record vcast_param[16U] ) 
{
  {
    int VCAST_TI_12_9_array_index = 0;
    int VCAST_TI_12_9_index = 0;
    int VCAST_TI_12_9_first, VCAST_TI_12_9_last;
    int VCAST_TI_12_9_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_9_local_field = 0;
    int VCAST_TI_12_9_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_9_first, &VCAST_TI_12_9_last, &VCAST_TI_12_9_more_data);
    VCAST_TI_12_9_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_9_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_9_upper = 16U;
      for (VCAST_TI_12_9_array_index=0; VCAST_TI_12_9_array_index< VCAST_TI_12_9_upper; VCAST_TI_12_9_array_index++){
        if ( (VCAST_TI_12_9_index >= VCAST_TI_12_9_first) && ( VCAST_TI_12_9_index <= VCAST_TI_12_9_last)){
          VCAST_TI_12_264 ( &(vcast_param[VCAST_TI_12_9_index]));
          VCAST_TI_12_9_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_9_local_field;
        } /* if */
        if (VCAST_TI_12_9_index >= VCAST_TI_12_9_last)
          break;
        VCAST_TI_12_9_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_9_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_9 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An integer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_27 ( signed short *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_27 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_27 ( signed short *vcast_param ) 
{
  switch (vCAST_COMMAND) {
    case vCAST_PRINT :
      if ( vcast_param == 0)
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_short(vCAST_OUTPUT_FILE, *vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      break;
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL :
    *vcast_param = ( signed short  ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL :
    *vcast_param = SHRT_MIN;
    break;
  case vCAST_MID_VAL :
    *vcast_param = (SHRT_MIN / 2) + (SHRT_MAX / 2);
    break;
  case vCAST_LAST_VAL :
    *vcast_param = SHRT_MAX;
    break;
  case vCAST_MIN_MINUS_1_VAL :
    *vcast_param = SHRT_MIN;
    *vcast_param = *vcast_param - 1;
    break;
  case vCAST_MAX_PLUS_1_VAL :
    *vcast_param = SHRT_MAX;
    *vcast_param = *vcast_param + 1;
    break;
  case vCAST_ZERO_VAL :
    *vcast_param = 0;
    break;
  default:
    break;
} /* end switch */
} /* end VCAST_TI_12_27 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_136 ( HighSpeedLowerType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_136 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_136 ( HighSpeedLowerType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (HighSpeedLowerType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NO_HSL;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = HSL_TT_MAST;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_136 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_101 ( SteeringType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_101 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_101 ( SteeringType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (SteeringType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = MANUAL;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = HYD_CONTACTOR;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_101 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_152 ( SteerDirection *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_152 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_152 ( SteerDirection *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (SteerDirection ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Forward;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Reverse;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_152 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_45 ( AuxHoistType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_45 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_45 ( AuxHoistType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (AuxHoistType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NO_AUX;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = AUX_MAIN_HOIST;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_45 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_119 ( ZONE_SWITCH_TYPE *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_119 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_119 ( ZONE_SWITCH_TYPE *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (ZONE_SWITCH_TYPE ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = ZONE_SW_NO;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = ZONE_SW_YES;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_119 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_87 ( RACK_SELECT_TYPE *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_87 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_87 ( RACK_SELECT_TYPE *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (RACK_SELECT_TYPE ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = RACK_SELECT_OFF;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = RACK_SELECT_ON;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_87 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_79 ( Op_Int_Mask_Type *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_79 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_79 ( Op_Int_Mask_Type *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (Op_Int_Mask_Type ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = USE_INPUT;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = MASK_INPUT;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_79 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_91 ( RegionType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_91 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_91 ( RegionType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (RegionType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = AllRegions;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = NorthAmerica;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_91 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_57 ( ForkType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_57 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_57 ( ForkType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (ForkType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Telescopic;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TripleLength;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_57 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_168 ( Use_tilt *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_168 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_168 ( Use_tilt *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (Use_tilt ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Tilt_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Tilt_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_168 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_166 ( TPA_Type *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_166 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_166 ( TPA_Type *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (TPA_Type ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NO_TPA;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TPA_Load_Compensated_without_FSS;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_166 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_208 ( Tilt_Sense_type *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_208 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_208 ( Tilt_Sense_type *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (Tilt_Sense_type ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = No_Tilt_Sense;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Poti_Tilt_Sense;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_208 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_170 ( Use_Accy1 *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_170 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_170 ( Use_Accy1 *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (Use_Accy1 ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Accy1_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Accy1_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_170 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_158 ( Accy2Type *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_158 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_158 ( Accy2Type *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (Accy2Type ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NO_Accy2;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Accy2_without_FSS;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_158 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_172 ( Use_Accy3 *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_172 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_172 ( Use_Accy3 *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (Use_Accy3 ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Accy3_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Accy3_Two_Hand_Ops_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_172 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_111 ( TravelAlarmType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_111 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_111 ( TravelAlarmType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (TravelAlarmType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NO_ALARM;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = BOTH_ALARM;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_111 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_113 ( TruckModel *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_113 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_113 ( TruckModel *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (TruckModel ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NO_TRUCK;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TML;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_113 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_89 ( ReachType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_89 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_89 ( ReachType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (ReachType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = ReachNone;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = SpecialR;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_89 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_174 ( HiSpdLatched *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_174 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_174 ( HiSpdLatched *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (HiSpdLatched ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = HighSpdLatching_not_allowed;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = HighSpdLatching_allowed;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_174 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_59 ( HeightSenseType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_59 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_59 ( HeightSenseType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (HeightSenseType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NO_HEIGHT_SENSE;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = MAGNET_LIMIT_SWICHTES;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_59 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_176 ( LoadSense *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_176 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_176 ( LoadSense *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (LoadSense ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = LS_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = LS_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_176 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_127 ( ReachSenseType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_127 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_127 ( ReachSenseType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (ReachSenseType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = No_Reach_Sense;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Poti_Reach_Sense;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_127 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_123 ( OperatorRemoteStatus *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_123 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_123 ( OperatorRemoteStatus *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (OperatorRemoteStatus ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Remote_Status_Disable;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Remote_Status_Enable;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_123 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_125 ( WalkCoastObjectDetStatus *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_125 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_125 ( WalkCoastObjectDetStatus *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (WalkCoastObjectDetStatus ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Walk_Coast_Disable;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Walk_Coast_Enable;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_125 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_178 ( Transport_option *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_178 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_178 ( Transport_option *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (Transport_option ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = TO_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TO_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_178 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_180 ( ChainSlackDetection *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_180 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_180 ( ChainSlackDetection *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (ChainSlackDetection ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = CS_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = CS_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_180 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_182 ( UserHeightCutoutOption *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_182 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_182 ( UserHeightCutoutOption *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (UserHeightCutoutOption ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = UHCO_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = UHCO_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_182 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_184 ( UserReachCutoutOption *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_184 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_184 ( UserReachCutoutOption *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (UserReachCutoutOption ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = URCO_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = URCO_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_184 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_121 ( QPRA *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_121 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_121 ( QPRA *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (QPRA ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Not_Present;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Present;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_121 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_186 ( User360Select *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_186 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_186 ( User360Select *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (User360Select ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Max_180_Steer;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = User_Selectable;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_186 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_61 ( LoadSenseType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_61 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_61 ( LoadSenseType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (LoadSenseType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NoLoadSense;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Virtual;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_61 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_154 ( WalkAlongStatus *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_154 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_154 ( WalkAlongStatus *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (WalkAlongStatus ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = WalkAlong_Disable;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = WalkAlong_Enable;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_154 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_188 ( HSSAntiTieDownMode *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_188 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_188 ( HSSAntiTieDownMode *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (HSSAntiTieDownMode ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = HSS_before_Throttle;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = HSS_after_Throttle;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_188 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_156 ( FAN_CONTROL *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_156 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_156 ( FAN_CONTROL *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (FAN_CONTROL ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = ALWAYS_OFF;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TEMP_CONTROLLED;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_156 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_67 ( MastType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_67 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_67 ( MastType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (MastType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NOMAST;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TL_TILT;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_67 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_65 ( MastDuty *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_65 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_65 ( MastDuty *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (MastDuty ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = STD;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = LD;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_65 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_160 ( BatteryBoxType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_160 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_160 ( BatteryBoxType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (BatteryBoxType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = BAT_BOX_A;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = BAT_HGT_TALL;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_160 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_162 ( EnergySource *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_162 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_162 ( EnergySource *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (EnergySource ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = FloodedLeadAcid;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = FuelCell;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_162 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_164 ( GuidanceType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_164 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_164 ( GuidanceType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (GuidanceType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = GUIDE_NONE;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = AUTO;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_164 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_190 ( AGV_Mode *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_190 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_190 ( AGV_Mode *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (AGV_Mode ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = AGV_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = AGV_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_190 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_194 ( CDM_Option *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_194 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_194 ( CDM_Option *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (CDM_Option ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = CDM_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = CDM_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_194 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_198 ( OperatorControlOption *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_198 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_198 ( OperatorControlOption *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (OperatorControlOption ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = OC_Levers;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = OC_JOYSTICK;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_198 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_206 ( KeylessPowerModule *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_206 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_206 ( KeylessPowerModule *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (KeylessPowerModule ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = KPM_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = KPM_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_206 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_200 ( SeatHeater *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_200 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_200 ( SeatHeater *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (SeatHeater ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = SH_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = SH_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_200 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_196 ( SideShiftPositionAssistOption *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_196 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_196 ( SideShiftPositionAssistOption *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (SideShiftPositionAssistOption ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = SSP_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = SSP_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_196 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_192 ( StrobeType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_192 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_192 ( StrobeType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (StrobeType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = STROBE_OFF;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = STROBE_BOTH;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_192 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_202 ( TFD_presence *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_202 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_202 ( TFD_presence *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (TFD_presence ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = TFD_not_present;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TFD_present;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_202 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_204 ( BackRest_module *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_204 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_204 ( BackRest_module *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (BackRest_module ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = BRM_Not_Active;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = BRM_Active;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_204 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_214 ( SpdCutTriggerType *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_214 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_214 ( SpdCutTriggerType *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (SpdCutTriggerType ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = HEIGHT;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = LCS;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_214 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_216 ( ForkHome *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_216 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_216 ( ForkHome *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (ForkHome ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = ForkHome_Disable;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = ForkHome_Enable;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_216 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_210 ( SpeedCutbackSRO *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_210 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_210 ( SpeedCutbackSRO *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (SpeedCutbackSRO ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = SpeedCutbackSRO_Disable;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = SpeedCutbackSRO_Enable;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_210 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_220 ( LoadWheelBrakes *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_220 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_220 ( LoadWheelBrakes *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (LoadWheelBrakes ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = LWB_Not_Present;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = LWB_Present;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_220 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_222 ( SpeedCutbackFeature *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_222 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_222 ( SpeedCutbackFeature *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (SpeedCutbackFeature ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = SpeedCutback_Disable;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = SpeedCutback_Enable;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_222 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_226 ( Battery_Voltage *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_226 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_226 ( Battery_Voltage *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (Battery_Voltage ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Volts_24;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Volts_80;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_226 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_238 ( LaserLineOption *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_238 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_238 ( LaserLineOption *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (LaserLineOption ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = LaserLine_Disable;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = LaserLine_Enable;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_238 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_240 ( BlueLightOption *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_240 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_240 ( BlueLightOption *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (BlueLightOption ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = BlueLight_Disable;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = BlueLight_Travel_Both;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_240 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_246 ( EstopChannels *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_246 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_246 ( EstopChannels *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (EstopChannels ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = No_Estop;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Dual_Estop;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_246 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_250 ( BHM_Presence *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_250 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_250 ( BHM_Presence *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (BHM_Presence ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = BHM_not_present;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = BHM_Present;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_250 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_252 ( InhibitXpressLower *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_252 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_252 ( InhibitXpressLower *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (InhibitXpressLower ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = IXL_Not_Inhibited;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = IXL_Inhibited;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_252 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_254 ( EnergySource_EWSTypeIntegration *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_254 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_254 ( EnergySource_EWSTypeIntegration *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (EnergySource_EWSTypeIntegration ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = HardWired;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = None;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_254 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_256 ( TempSense *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_256 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_256 ( TempSense *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (TempSense ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NO_TEMP_SENSE;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TEMP_SENSE;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_256 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_258 ( FreezerHeater *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_258 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_258 ( FreezerHeater *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (FreezerHeater ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NO_FREEZER_HEATER;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = USE_FREEZER_HEATER;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_258 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_804 ( powerbase_fb_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_804 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_804 ( powerbase_fb_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rMotorSpeed */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMotorSpeed))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rMotorTorqAchieved */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMotorTorqAchieved))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rDcCurrentEst */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rDcCurrentEst))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->rBusVoltage */
      case 4: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBusVoltage))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rMotorTemp */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMotorTemp))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->ulStatusword */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulStatusword))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->ulWarnings */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulWarnings))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->swControlTemp */
      case 8: { 
        VCAST_TI_12_27 ( ((signed short *)(&(vcast_param->swControlTemp))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxTorkOutMotoring */
      case 9: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxTorkOutMotoring))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->rMaxTorkOutRegen */
      case 10: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rMaxTorkOutRegen))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->uwEncoder1counts */
      case 11: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwEncoder1counts))));
        break; /* end case 11*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_804 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_808 ( steering_fdbk_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_808 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_808 ( steering_fdbk_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rStrPos */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rStrPos))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->rStrSpd */
      case 2: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rStrSpd))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->rStrCur */
      case 3: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rStrCur))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ulStopTrx */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulStopTrx))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->ulFreshCnt */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulFreshCnt))));
        break; /* end case 5*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_808 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_812 ( throttle_input_bus_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_812 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_812 ( throttle_input_bus_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->rAnalogThrottle */
      case 1: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAnalogThrottle))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ulPosDirection */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPosDirection))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ulNegDirection */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulNegDirection))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ulStart */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulStart))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->rAnalogDirection */
      case 5: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rAnalogDirection))));
        break; /* end case 5*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_812 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_6 ( CalibMode *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_6 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_6 ( CalibMode *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (CalibMode ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Cal_Proc_None;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Cal_Proc_Accy3_Throt;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_6 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_146 ( CalibUIKeyInput *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_146 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_146 ( CalibUIKeyInput *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (CalibUIKeyInput ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = KI_BUTTON_NOT_PRESSED;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = KI_X_BUTTON;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_146 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_479 ( float vcast_param[6] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_479 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_479 ( float vcast_param[6] ) 
{
  {
    int VCAST_TI_12_479_array_index = 0;
    int VCAST_TI_12_479_index = 0;
    int VCAST_TI_12_479_first, VCAST_TI_12_479_last;
    int VCAST_TI_12_479_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_479_local_field = 0;
    int VCAST_TI_12_479_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_479_first, &VCAST_TI_12_479_last, &VCAST_TI_12_479_more_data);
    VCAST_TI_12_479_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_479_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_479_upper = 6;
      for (VCAST_TI_12_479_array_index=0; VCAST_TI_12_479_array_index< VCAST_TI_12_479_upper; VCAST_TI_12_479_array_index++){
        if ( (VCAST_TI_12_479_index >= VCAST_TI_12_479_first) && ( VCAST_TI_12_479_index <= VCAST_TI_12_479_last)){
          VCAST_TI_8_3 ( &(vcast_param[VCAST_TI_12_479_index]));
          VCAST_TI_12_479_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_479_local_field;
        } /* if */
        if (VCAST_TI_12_479_index >= VCAST_TI_12_479_last)
          break;
        VCAST_TI_12_479_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_479_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_479 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_483 ( float vcast_param[7] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_483 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_483 ( float vcast_param[7] ) 
{
  {
    int VCAST_TI_12_483_array_index = 0;
    int VCAST_TI_12_483_index = 0;
    int VCAST_TI_12_483_first, VCAST_TI_12_483_last;
    int VCAST_TI_12_483_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_483_local_field = 0;
    int VCAST_TI_12_483_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_483_first, &VCAST_TI_12_483_last, &VCAST_TI_12_483_more_data);
    VCAST_TI_12_483_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_483_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_483_upper = 7;
      for (VCAST_TI_12_483_array_index=0; VCAST_TI_12_483_array_index< VCAST_TI_12_483_upper; VCAST_TI_12_483_array_index++){
        if ( (VCAST_TI_12_483_index >= VCAST_TI_12_483_first) && ( VCAST_TI_12_483_index <= VCAST_TI_12_483_last)){
          VCAST_TI_8_3 ( &(vcast_param[VCAST_TI_12_483_index]));
          VCAST_TI_12_483_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_483_local_field;
        } /* if */
        if (VCAST_TI_12_483_index >= VCAST_TI_12_483_last)
          break;
        VCAST_TI_12_483_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_483_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_483 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_244 ( PerformanceCurve *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_244 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_244 ( PerformanceCurve *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (PerformanceCurve ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Productivity;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = Economy;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_244 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_497 ( float vcast_param[5] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_497 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_497 ( float vcast_param[5] ) 
{
  {
    int VCAST_TI_12_497_array_index = 0;
    int VCAST_TI_12_497_index = 0;
    int VCAST_TI_12_497_first, VCAST_TI_12_497_last;
    int VCAST_TI_12_497_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_497_local_field = 0;
    int VCAST_TI_12_497_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_497_first, &VCAST_TI_12_497_last, &VCAST_TI_12_497_more_data);
    VCAST_TI_12_497_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_497_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_497_upper = 5;
      for (VCAST_TI_12_497_array_index=0; VCAST_TI_12_497_array_index< VCAST_TI_12_497_upper; VCAST_TI_12_497_array_index++){
        if ( (VCAST_TI_12_497_index >= VCAST_TI_12_497_first) && ( VCAST_TI_12_497_index <= VCAST_TI_12_497_last)){
          VCAST_TI_8_3 ( &(vcast_param[VCAST_TI_12_497_index]));
          VCAST_TI_12_497_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_497_local_field;
        } /* if */
        if (VCAST_TI_12_497_index >= VCAST_TI_12_497_last)
          break;
        VCAST_TI_12_497_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_497_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_497 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_242 ( OptimizedCorneringSpeedCurve *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_242 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_242 ( OptimizedCorneringSpeedCurve *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (OptimizedCorneringSpeedCurve ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = OFF;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = CUSTOM_CURVE;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_242 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_212 ( test_output_t *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_212 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_212 ( test_output_t *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (test_output_t ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = TO_NO_TEST;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TO_LASER_LINE;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_212 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_885 ( struct fram_accum_data_meters *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_885 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_885 ( struct fram_accum_data_meters *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulRevision */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulRevision))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ulMeterVals */
      case 2: { 
        VCAST_TI_12_886 ( ((unsigned *)(vcast_param->ulMeterVals)));
        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_885 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_889 ( struct accum_data vcast_param[32U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_889 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_889 ( struct accum_data vcast_param[32U] ) 
{
  {
    int VCAST_TI_12_889_array_index = 0;
    int VCAST_TI_12_889_index = 0;
    int VCAST_TI_12_889_first, VCAST_TI_12_889_last;
    int VCAST_TI_12_889_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_889_local_field = 0;
    int VCAST_TI_12_889_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_889_first, &VCAST_TI_12_889_last, &VCAST_TI_12_889_more_data);
    VCAST_TI_12_889_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_889_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_889_upper = 32U;
      for (VCAST_TI_12_889_array_index=0; VCAST_TI_12_889_array_index< VCAST_TI_12_889_upper; VCAST_TI_12_889_array_index++){
        if ( (VCAST_TI_12_889_index >= VCAST_TI_12_889_first) && ( VCAST_TI_12_889_index <= VCAST_TI_12_889_last)){
          VCAST_TI_12_887 ( &(vcast_param[VCAST_TI_12_889_index]));
          VCAST_TI_12_889_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_889_local_field;
        } /* if */
        if (VCAST_TI_12_889_index >= VCAST_TI_12_889_last)
          break;
        VCAST_TI_12_889_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_889_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_889 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_967 ( char vcast_param[18U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_967 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_967 ( char vcast_param[18U] ) 
{
  {
    int VCAST_TI_12_967_array_index = 0;
    int VCAST_TI_12_967_index = 0;
    int VCAST_TI_12_967_first, VCAST_TI_12_967_last;
    int VCAST_TI_12_967_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_967_local_field = 0;
    int VCAST_TI_12_967_value_printed = 0;
    int VCAST_TI_12_967_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_967_first, &VCAST_TI_12_967_last, &VCAST_TI_12_967_more_data);
    VCAST_TI_12_967_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_967_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,18U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_967_upper = 18U;
      for (VCAST_TI_12_967_array_index=0; VCAST_TI_12_967_array_index< VCAST_TI_12_967_upper; VCAST_TI_12_967_array_index++){
        if ( (VCAST_TI_12_967_index >= VCAST_TI_12_967_first) && ( VCAST_TI_12_967_index <= VCAST_TI_12_967_last)){
          if ( VCAST_TI_12_967_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_967_upper);
          else
            VCAST_TI_8_4 ( &(vcast_param[VCAST_TI_12_967_index]));
          VCAST_TI_12_967_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_967_local_field;
        } /* if */
        if (VCAST_TI_12_967_index >= VCAST_TI_12_967_last)
          break;
        VCAST_TI_12_967_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_967_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_967 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_965 ( struct mm_local_proc *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_965 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_965 ( struct mm_local_proc *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwNumbProcs */
      case 1: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwNumbProcs))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->Master */
      case 2: { 
        VCAST_TI_12_963 ( ((struct mm_local_info *)(&(vcast_param->Master))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->Supv */
      case 3: { 
        VCAST_TI_12_963 ( ((struct mm_local_info *)(&(vcast_param->Supv))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_965 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_961 ( struct mm_can_module *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_961 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_961 ( struct mm_can_module *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwNumbDrivers */
      case 1: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwNumbDrivers))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->driver */
      case 2: { 
        VCAST_TI_12_962 ( ((struct mi_driver *)(vcast_param->driver)));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ulModuleCompatibility */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulModuleCompatibility))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_961 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1057 ( float vcast_param[15] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1057 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1057 ( float vcast_param[15] ) 
{
  {
    int VCAST_TI_12_1057_array_index = 0;
    int VCAST_TI_12_1057_index = 0;
    int VCAST_TI_12_1057_first, VCAST_TI_12_1057_last;
    int VCAST_TI_12_1057_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1057_local_field = 0;
    int VCAST_TI_12_1057_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1057_first, &VCAST_TI_12_1057_last, &VCAST_TI_12_1057_more_data);
    VCAST_TI_12_1057_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1057_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,15);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1057_upper = 15;
      for (VCAST_TI_12_1057_array_index=0; VCAST_TI_12_1057_array_index< VCAST_TI_12_1057_upper; VCAST_TI_12_1057_array_index++){
        if ( (VCAST_TI_12_1057_index >= VCAST_TI_12_1057_first) && ( VCAST_TI_12_1057_index <= VCAST_TI_12_1057_last)){
          VCAST_TI_8_3 ( &(vcast_param[VCAST_TI_12_1057_index]));
          VCAST_TI_12_1057_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1057_local_field;
        } /* if */
        if (VCAST_TI_12_1057_index >= VCAST_TI_12_1057_last)
          break;
        VCAST_TI_12_1057_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1057_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1057 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_117 ( VEL_TYPE *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_117 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_117 ( VEL_TYPE *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (VEL_TYPE ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = VEL_POS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = VEL_NEUTRAL;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_117 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_232 ( SSP_Pos *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_232 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_232 ( SSP_Pos *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (SSP_Pos ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = SSP_RIGHT;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = SSP_LEFT;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_232 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_230 ( TPA_Pos *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_230 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_230 ( TPA_Pos *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (TPA_Pos ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = TPS_TILTED_UP;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TPA_TILTED_DOWN;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_230 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_103 ( StrOpModeState *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_103 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_103 ( StrOpModeState *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (StrOpModeState ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = STR_OP_NoState;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = STR_OP_MODE_CAL_AFTER_INDEX;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_103 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_109 ( TractionState *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_109 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_109 ( TractionState *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (TractionState ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = TRACT_STATE_PARK;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = BRAKING_STATE_EMERGENCY_STOP;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_109 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_107 ( TractionMotorState *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_107 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_107 ( TractionMotorState *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (TractionMotorState ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = TRACT_MOTOR_STATE_MOTORING;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TRACT_MOTOR_STATE_REGEN;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_107 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_69 ( MastZone *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_69 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_69 ( MastZone *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (MastZone ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = BELOW_PRIMARY_RESET_SWITCH;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = BELOW_FREE_LIFT_HEIGHT;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_69 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_129 ( HoistDirectionBasedOnDelta *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_129 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_129 ( HoistDirectionBasedOnDelta *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (HoistDirectionBasedOnDelta ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = HgtFb_Raise;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = HgtFb_Stop;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_129 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_218 ( AboveHeightLimit *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_218 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_218 ( AboveHeightLimit *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (AboveHeightLimit ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = NOT_ABOVE_HEIGHT_LIMIT;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = ABOVE_HEIGHT_LIMIT2;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_218 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_134 ( CALIB_ACTION *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_134 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_134 ( CALIB_ACTION *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (CALIB_ACTION ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = CALIB_ACT_GET_KEYPRESS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = CALIB_ACT_TIME_DELAY;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_134 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_132 ( CalibUnits *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_132 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_132 ( CalibUnits *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (CalibUnits ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = Unitless;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = AH;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_132 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_834 ( signed int vcast_param[6] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_834 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_834 ( signed int vcast_param[6] ) 
{
  {
    int VCAST_TI_12_834_array_index = 0;
    int VCAST_TI_12_834_index = 0;
    int VCAST_TI_12_834_first, VCAST_TI_12_834_last;
    int VCAST_TI_12_834_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_834_local_field = 0;
    int VCAST_TI_12_834_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_834_first, &VCAST_TI_12_834_last, &VCAST_TI_12_834_more_data);
    VCAST_TI_12_834_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_834_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_834_upper = 6;
      for (VCAST_TI_12_834_array_index=0; VCAST_TI_12_834_array_index< VCAST_TI_12_834_upper; VCAST_TI_12_834_array_index++){
        if ( (VCAST_TI_12_834_index >= VCAST_TI_12_834_first) && ( VCAST_TI_12_834_index <= VCAST_TI_12_834_last)){
          VCAST_TI_11_31 ( &(vcast_param[VCAST_TI_12_834_index]));
          VCAST_TI_12_834_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_834_local_field;
        } /* if */
        if (VCAST_TI_12_834_index >= VCAST_TI_12_834_last)
          break;
        VCAST_TI_12_834_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_834_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_834 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_835 ( CUTOUT_TYPE_TYPE vcast_param[6] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_835 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_835 ( CUTOUT_TYPE_TYPE vcast_param[6] ) 
{
  {
    int VCAST_TI_12_835_array_index = 0;
    int VCAST_TI_12_835_index = 0;
    int VCAST_TI_12_835_first, VCAST_TI_12_835_last;
    int VCAST_TI_12_835_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_835_local_field = 0;
    int VCAST_TI_12_835_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_835_first, &VCAST_TI_12_835_last, &VCAST_TI_12_835_more_data);
    VCAST_TI_12_835_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_835_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_835_upper = 6;
      for (VCAST_TI_12_835_array_index=0; VCAST_TI_12_835_array_index< VCAST_TI_12_835_upper; VCAST_TI_12_835_array_index++){
        if ( (VCAST_TI_12_835_index >= VCAST_TI_12_835_first) && ( VCAST_TI_12_835_index <= VCAST_TI_12_835_last)){
          VCAST_TI_12_49 ( &(vcast_param[VCAST_TI_12_835_index]));
          VCAST_TI_12_835_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_835_local_field;
        } /* if */
        if (VCAST_TI_12_835_index >= VCAST_TI_12_835_last)
          break;
        VCAST_TI_12_835_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_835_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_835 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_836 ( CUTOUT_ZONE_TYPE vcast_param[6] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_836 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_836 ( CUTOUT_ZONE_TYPE vcast_param[6] ) 
{
  {
    int VCAST_TI_12_836_array_index = 0;
    int VCAST_TI_12_836_index = 0;
    int VCAST_TI_12_836_first, VCAST_TI_12_836_last;
    int VCAST_TI_12_836_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_836_local_field = 0;
    int VCAST_TI_12_836_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_836_first, &VCAST_TI_12_836_last, &VCAST_TI_12_836_more_data);
    VCAST_TI_12_836_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_836_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_836_upper = 6;
      for (VCAST_TI_12_836_array_index=0; VCAST_TI_12_836_array_index< VCAST_TI_12_836_upper; VCAST_TI_12_836_array_index++){
        if ( (VCAST_TI_12_836_index >= VCAST_TI_12_836_first) && ( VCAST_TI_12_836_index <= VCAST_TI_12_836_last)){
          VCAST_TI_12_51 ( &(vcast_param[VCAST_TI_12_836_index]));
          VCAST_TI_12_836_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_836_local_field;
        } /* if */
        if (VCAST_TI_12_836_index >= VCAST_TI_12_836_last)
          break;
        VCAST_TI_12_836_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_836_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_836 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_837 ( SD_TRIGGER_TYPE vcast_param[6] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_837 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_837 ( SD_TRIGGER_TYPE vcast_param[6] ) 
{
  {
    int VCAST_TI_12_837_array_index = 0;
    int VCAST_TI_12_837_index = 0;
    int VCAST_TI_12_837_first, VCAST_TI_12_837_last;
    int VCAST_TI_12_837_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_837_local_field = 0;
    int VCAST_TI_12_837_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_837_first, &VCAST_TI_12_837_last, &VCAST_TI_12_837_more_data);
    VCAST_TI_12_837_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_837_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_837_upper = 6;
      for (VCAST_TI_12_837_array_index=0; VCAST_TI_12_837_array_index< VCAST_TI_12_837_upper; VCAST_TI_12_837_array_index++){
        if ( (VCAST_TI_12_837_index >= VCAST_TI_12_837_first) && ( VCAST_TI_12_837_index <= VCAST_TI_12_837_last)){
          VCAST_TI_12_93 ( &(vcast_param[VCAST_TI_12_837_index]));
          VCAST_TI_12_837_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_837_local_field;
        } /* if */
        if (VCAST_TI_12_837_index >= VCAST_TI_12_837_last)
          break;
        VCAST_TI_12_837_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_837_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_837 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_829 ( unsigned short vcast_param[35] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_829 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_829 ( unsigned short vcast_param[35] ) 
{
  {
    int VCAST_TI_12_829_array_index = 0;
    int VCAST_TI_12_829_index = 0;
    int VCAST_TI_12_829_first, VCAST_TI_12_829_last;
    int VCAST_TI_12_829_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_829_local_field = 0;
    int VCAST_TI_12_829_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_829_first, &VCAST_TI_12_829_last, &VCAST_TI_12_829_more_data);
    VCAST_TI_12_829_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_829_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_829_upper = 35;
      for (VCAST_TI_12_829_array_index=0; VCAST_TI_12_829_array_index< VCAST_TI_12_829_upper; VCAST_TI_12_829_array_index++){
        if ( (VCAST_TI_12_829_index >= VCAST_TI_12_829_first) && ( VCAST_TI_12_829_index <= VCAST_TI_12_829_last)){
          VCAST_TI_9_16 ( &(vcast_param[VCAST_TI_12_829_index]));
          VCAST_TI_12_829_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_829_local_field;
        } /* if */
        if (VCAST_TI_12_829_index >= VCAST_TI_12_829_last)
          break;
        VCAST_TI_12_829_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_829_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_829 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_830 ( signed int vcast_param[35] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_830 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_830 ( signed int vcast_param[35] ) 
{
  {
    int VCAST_TI_12_830_array_index = 0;
    int VCAST_TI_12_830_index = 0;
    int VCAST_TI_12_830_first, VCAST_TI_12_830_last;
    int VCAST_TI_12_830_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_830_local_field = 0;
    int VCAST_TI_12_830_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_830_first, &VCAST_TI_12_830_last, &VCAST_TI_12_830_more_data);
    VCAST_TI_12_830_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_830_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_830_upper = 35;
      for (VCAST_TI_12_830_array_index=0; VCAST_TI_12_830_array_index< VCAST_TI_12_830_upper; VCAST_TI_12_830_array_index++){
        if ( (VCAST_TI_12_830_index >= VCAST_TI_12_830_first) && ( VCAST_TI_12_830_index <= VCAST_TI_12_830_last)){
          VCAST_TI_11_31 ( &(vcast_param[VCAST_TI_12_830_index]));
          VCAST_TI_12_830_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_830_local_field;
        } /* if */
        if (VCAST_TI_12_830_index >= VCAST_TI_12_830_last)
          break;
        VCAST_TI_12_830_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_830_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_830 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1088 ( struct fram_birthCert_data *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1088 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1088 ( struct fram_birthCert_data *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ubNumStrings */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubNumStrings))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->szAsciiKey */
      case 2: { 
        VCAST_TI_12_1089 ( ((char (*)[160U])(vcast_param->szAsciiKey)));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ubStringlen */
      case 3: { 
        VCAST_TI_12_1091 ( ((unsigned char *)(vcast_param->ubStringlen)));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ulPadding */
      case 4: { 
        VCAST_TI_12_1092 ( ((unsigned char *)(vcast_param->ulPadding)));
        break; /* end case 4*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_1088 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_7 ( unsigned char vcast_param[IMM2VCM_CMD_COUNT] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_7 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_7 ( unsigned char vcast_param[IMM2VCM_CMD_COUNT] ) 
{
  {
    int VCAST_TI_12_7_array_index = 0;
    int VCAST_TI_12_7_index = 0;
    int VCAST_TI_12_7_first, VCAST_TI_12_7_last;
    int VCAST_TI_12_7_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_7_local_field = 0;
    int VCAST_TI_12_7_value_printed = 0;
    int VCAST_TI_12_7_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_7_first, &VCAST_TI_12_7_last, &VCAST_TI_12_7_more_data);
    VCAST_TI_12_7_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_7_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IMM2VCM_CMD_COUNT);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_7_upper = IMM2VCM_CMD_COUNT;
      for (VCAST_TI_12_7_array_index=0; VCAST_TI_12_7_array_index< VCAST_TI_12_7_upper; VCAST_TI_12_7_array_index++){
        if ( (VCAST_TI_12_7_index >= VCAST_TI_12_7_first) && ( VCAST_TI_12_7_index <= VCAST_TI_12_7_last)){
          if ( VCAST_TI_12_7_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_7_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_12_7_index]));
          VCAST_TI_12_7_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_7_local_field;
        } /* if */
        if (VCAST_TI_12_7_index >= VCAST_TI_12_7_last)
          break;
        VCAST_TI_12_7_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_7_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_7 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_994 ( struct imm_ips_data *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_994 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_994 ( struct imm_ips_data *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ubTxData */
      case 1: { 
        VCAST_TI_12_995 ( ((unsigned char *)(vcast_param->ubTxData)));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubRxData1 */
      case 2: { 
        VCAST_TI_12_12 ( ((unsigned char *)(vcast_param->ubRxData1)));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ubRxData2 */
      case 3: { 
        VCAST_TI_12_12 ( ((unsigned char *)(vcast_param->ubRxData2)));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ubLastEventReceived */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubLastEventReceived))));
        break; /* end case 4*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_994 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1108 ( struct aecp_object vcast_param[28U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1108 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1108 ( struct aecp_object vcast_param[28U] ) 
{
  {
    int VCAST_TI_12_1108_array_index = 0;
    int VCAST_TI_12_1108_index = 0;
    int VCAST_TI_12_1108_first, VCAST_TI_12_1108_last;
    int VCAST_TI_12_1108_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1108_local_field = 0;
    int VCAST_TI_12_1108_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1108_first, &VCAST_TI_12_1108_last, &VCAST_TI_12_1108_more_data);
    VCAST_TI_12_1108_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1108_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,28U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1108_upper = 28U;
      for (VCAST_TI_12_1108_array_index=0; VCAST_TI_12_1108_array_index< VCAST_TI_12_1108_upper; VCAST_TI_12_1108_array_index++){
        if ( (VCAST_TI_12_1108_index >= VCAST_TI_12_1108_first) && ( VCAST_TI_12_1108_index <= VCAST_TI_12_1108_last)){
          VCAST_TI_9_20 ( &(vcast_param[VCAST_TI_12_1108_index]));
          VCAST_TI_12_1108_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1108_local_field;
        } /* if */
        if (VCAST_TI_12_1108_index >= VCAST_TI_12_1108_last)
          break;
        VCAST_TI_12_1108_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1108_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1108 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_264 ( struct dem_report_record *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_264 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_264 ( struct dem_report_record *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwEventReferenceID */
      case 1: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwEventReferenceID))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubState */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubState))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ubJ1939Count */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubJ1939Count))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_264 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_886 ( unsigned vcast_param[32U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_886 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_886 ( unsigned vcast_param[32U] ) 
{
  {
    int VCAST_TI_12_886_array_index = 0;
    int VCAST_TI_12_886_index = 0;
    int VCAST_TI_12_886_first, VCAST_TI_12_886_last;
    int VCAST_TI_12_886_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_886_local_field = 0;
    int VCAST_TI_12_886_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_886_first, &VCAST_TI_12_886_last, &VCAST_TI_12_886_more_data);
    VCAST_TI_12_886_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_886_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_886_upper = 32U;
      for (VCAST_TI_12_886_array_index=0; VCAST_TI_12_886_array_index< VCAST_TI_12_886_upper; VCAST_TI_12_886_array_index++){
        if ( (VCAST_TI_12_886_index >= VCAST_TI_12_886_first) && ( VCAST_TI_12_886_index <= VCAST_TI_12_886_last)){
          VCAST_TI_11_4 ( &(vcast_param[VCAST_TI_12_886_index]));
          VCAST_TI_12_886_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_886_local_field;
        } /* if */
        if (VCAST_TI_12_886_index >= VCAST_TI_12_886_last)
          break;
        VCAST_TI_12_886_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_886_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_886 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_887 ( struct accum_data *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_887 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_887 ( struct accum_data *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->eType */
      case 1: { 
        VCAST_TI_12_884 ( ((enum accum_type *)(&(vcast_param->eType))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ulValue */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulValue))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->fAdvance */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fAdvance))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ulUpdateValue */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulUpdateValue))));
        break; /* end case 4*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_887 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_963 ( struct mm_local_info *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_963 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_963 ( struct mm_local_info *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ubNodeID */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubNodeID))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->szDeviceID */
      case 2: { 
        VCAST_TI_12_964 ( ((char *)(vcast_param->szDeviceID)));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->info */
      case 3: { 
        VCAST_TI_12_896 ( ((struct codrv_info *)(&(vcast_param->info))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_963 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_962 ( struct mi_driver vcast_param[10U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_962 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_962 ( struct mi_driver vcast_param[10U] ) 
{
  {
    int VCAST_TI_12_962_array_index = 0;
    int VCAST_TI_12_962_index = 0;
    int VCAST_TI_12_962_first, VCAST_TI_12_962_last;
    int VCAST_TI_12_962_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_962_local_field = 0;
    int VCAST_TI_12_962_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_962_first, &VCAST_TI_12_962_last, &VCAST_TI_12_962_more_data);
    VCAST_TI_12_962_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_962_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,10U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_962_upper = 10U;
      for (VCAST_TI_12_962_array_index=0; VCAST_TI_12_962_array_index< VCAST_TI_12_962_upper; VCAST_TI_12_962_array_index++){
        if ( (VCAST_TI_12_962_index >= VCAST_TI_12_962_first) && ( VCAST_TI_12_962_index <= VCAST_TI_12_962_last)){
          VCAST_TI_12_955 ( &(vcast_param[VCAST_TI_12_962_index]));
          VCAST_TI_12_962_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_962_local_field;
        } /* if */
        if (VCAST_TI_12_962_index >= VCAST_TI_12_962_last)
          break;
        VCAST_TI_12_962_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_962_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_962 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_49 ( CUTOUT_TYPE_TYPE *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_49 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_49 ( CUTOUT_TYPE_TYPE *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (CUTOUT_TYPE_TYPE ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = CUTOUT_NONE;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = CUTOUT_NCT_LATCH;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_49 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_51 ( CUTOUT_ZONE_TYPE *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_51 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_51 ( CUTOUT_ZONE_TYPE *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (CUTOUT_ZONE_TYPE ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = CUTOUT_ALL;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = CUTOUT_ZONEC;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_51 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A typedef */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_93 ( SD_TRIGGER_TYPE *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_93 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_93 ( SD_TRIGGER_TYPE *vcast_param ) 
{
#if (defined(VCAST_NO_TYPE_SUPPORT))
  /* User code: type is not supported */
  vcast_not_supported();
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (SD_TRIGGER_TYPE ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = TR_NONE;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TR_SENSOR;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/

} /* end VCAST_TI_12_93 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1089 ( char vcast_param[2U][160U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1089 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1089 ( char vcast_param[2U][160U] ) 
{
  {
    int VCAST_TI_12_1089_array_index = 0;
    int VCAST_TI_12_1089_index = 0;
    int VCAST_TI_12_1089_first, VCAST_TI_12_1089_last;
    int VCAST_TI_12_1089_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1089_local_field = 0;
    int VCAST_TI_12_1089_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_12_1089_first, &VCAST_TI_12_1089_last, &VCAST_TI_12_1089_more_data);
    VCAST_TI_12_1089_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1089_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,160U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1089_upper = 2U;
      for (VCAST_TI_12_1089_array_index=0; VCAST_TI_12_1089_array_index< VCAST_TI_12_1089_upper; VCAST_TI_12_1089_array_index++){
        if ( (VCAST_TI_12_1089_index >= VCAST_TI_12_1089_first) && ( VCAST_TI_12_1089_index <= VCAST_TI_12_1089_last)){
          VCAST_TI_12_1090 ( vcast_param[VCAST_TI_12_1089_index]);
          VCAST_TI_12_1089_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1089_local_field;
        } /* if */
        if (VCAST_TI_12_1089_index >= VCAST_TI_12_1089_last)
          break;
        VCAST_TI_12_1089_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1089_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1089 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1091 ( unsigned char vcast_param[2U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1091 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1091 ( unsigned char vcast_param[2U] ) 
{
  {
    int VCAST_TI_12_1091_array_index = 0;
    int VCAST_TI_12_1091_index = 0;
    int VCAST_TI_12_1091_first, VCAST_TI_12_1091_last;
    int VCAST_TI_12_1091_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1091_local_field = 0;
    int VCAST_TI_12_1091_value_printed = 0;
    int VCAST_TI_12_1091_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_1091_first, &VCAST_TI_12_1091_last, &VCAST_TI_12_1091_more_data);
    VCAST_TI_12_1091_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1091_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1091_upper = 2U;
      for (VCAST_TI_12_1091_array_index=0; VCAST_TI_12_1091_array_index< VCAST_TI_12_1091_upper; VCAST_TI_12_1091_array_index++){
        if ( (VCAST_TI_12_1091_index >= VCAST_TI_12_1091_first) && ( VCAST_TI_12_1091_index <= VCAST_TI_12_1091_last)){
          if ( VCAST_TI_12_1091_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_1091_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_12_1091_index]));
          VCAST_TI_12_1091_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1091_local_field;
        } /* if */
        if (VCAST_TI_12_1091_index >= VCAST_TI_12_1091_last)
          break;
        VCAST_TI_12_1091_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1091_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1091 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1092 ( unsigned char vcast_param[((764U - sizeof(uint8_t)) - sizeof(uint8_t) * 2U) - (sizeof(char) * 2U) * 160U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1092 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1092 ( unsigned char vcast_param[((764U - sizeof(uint8_t)) - sizeof(uint8_t) * 2U) - (sizeof(char) * 2U) * 160U] ) 
{
  {
    int VCAST_TI_12_1092_array_index = 0;
    int VCAST_TI_12_1092_index = 0;
    int VCAST_TI_12_1092_first, VCAST_TI_12_1092_last;
    int VCAST_TI_12_1092_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1092_local_field = 0;
    int VCAST_TI_12_1092_value_printed = 0;
    int VCAST_TI_12_1092_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_1092_first, &VCAST_TI_12_1092_last, &VCAST_TI_12_1092_more_data);
    VCAST_TI_12_1092_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1092_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,((764U - sizeof(uint8_t)) - sizeof(uint8_t) * 2U) - (sizeof(char) * 2U) * 160U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1092_upper = ((764U - sizeof(uint8_t)) - sizeof(uint8_t) * 2U) - (sizeof(char) * 2U) * 160U;
      for (VCAST_TI_12_1092_array_index=0; VCAST_TI_12_1092_array_index< VCAST_TI_12_1092_upper; VCAST_TI_12_1092_array_index++){
        if ( (VCAST_TI_12_1092_index >= VCAST_TI_12_1092_first) && ( VCAST_TI_12_1092_index <= VCAST_TI_12_1092_last)){
          if ( VCAST_TI_12_1092_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_1092_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_12_1092_index]));
          VCAST_TI_12_1092_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1092_local_field;
        } /* if */
        if (VCAST_TI_12_1092_index >= VCAST_TI_12_1092_last)
          break;
        VCAST_TI_12_1092_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1092_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1092 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_995 ( unsigned char vcast_param[4U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_995 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_995 ( unsigned char vcast_param[4U] ) 
{
  {
    int VCAST_TI_12_995_array_index = 0;
    int VCAST_TI_12_995_index = 0;
    int VCAST_TI_12_995_first, VCAST_TI_12_995_last;
    int VCAST_TI_12_995_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_995_local_field = 0;
    int VCAST_TI_12_995_value_printed = 0;
    int VCAST_TI_12_995_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_995_first, &VCAST_TI_12_995_last, &VCAST_TI_12_995_more_data);
    VCAST_TI_12_995_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_995_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_995_upper = 4U;
      for (VCAST_TI_12_995_array_index=0; VCAST_TI_12_995_array_index< VCAST_TI_12_995_upper; VCAST_TI_12_995_array_index++){
        if ( (VCAST_TI_12_995_index >= VCAST_TI_12_995_first) && ( VCAST_TI_12_995_index <= VCAST_TI_12_995_last)){
          if ( VCAST_TI_12_995_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_995_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_12_995_index]));
          VCAST_TI_12_995_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_995_local_field;
        } /* if */
        if (VCAST_TI_12_995_index >= VCAST_TI_12_995_last)
          break;
        VCAST_TI_12_995_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_995_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_995 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_12 ( unsigned char vcast_param[8U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_12 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_12 ( unsigned char vcast_param[8U] ) 
{
  {
    int VCAST_TI_12_12_array_index = 0;
    int VCAST_TI_12_12_index = 0;
    int VCAST_TI_12_12_first, VCAST_TI_12_12_last;
    int VCAST_TI_12_12_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_12_local_field = 0;
    int VCAST_TI_12_12_value_printed = 0;
    int VCAST_TI_12_12_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_12_first, &VCAST_TI_12_12_last, &VCAST_TI_12_12_more_data);
    VCAST_TI_12_12_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_12_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_12_upper = 8U;
      for (VCAST_TI_12_12_array_index=0; VCAST_TI_12_12_array_index< VCAST_TI_12_12_upper; VCAST_TI_12_12_array_index++){
        if ( (VCAST_TI_12_12_index >= VCAST_TI_12_12_first) && ( VCAST_TI_12_12_index <= VCAST_TI_12_12_last)){
          if ( VCAST_TI_12_12_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_12_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_12_12_index]));
          VCAST_TI_12_12_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_12_local_field;
        } /* if */
        if (VCAST_TI_12_12_index >= VCAST_TI_12_12_last)
          break;
        VCAST_TI_12_12_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_12_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_12 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_884 ( enum accum_type *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_884 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_884 ( enum accum_type *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum accum_type ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = AT_NOT_USED;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = AT_SET_VAL;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_12_884 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_964 ( char vcast_param[36U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_964 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_964 ( char vcast_param[36U] ) 
{
  {
    int VCAST_TI_12_964_array_index = 0;
    int VCAST_TI_12_964_index = 0;
    int VCAST_TI_12_964_first, VCAST_TI_12_964_last;
    int VCAST_TI_12_964_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_964_local_field = 0;
    int VCAST_TI_12_964_value_printed = 0;
    int VCAST_TI_12_964_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_964_first, &VCAST_TI_12_964_last, &VCAST_TI_12_964_more_data);
    VCAST_TI_12_964_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_964_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,36U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_964_upper = 36U;
      for (VCAST_TI_12_964_array_index=0; VCAST_TI_12_964_array_index< VCAST_TI_12_964_upper; VCAST_TI_12_964_array_index++){
        if ( (VCAST_TI_12_964_index >= VCAST_TI_12_964_first) && ( VCAST_TI_12_964_index <= VCAST_TI_12_964_last)){
          if ( VCAST_TI_12_964_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_964_upper);
          else
            VCAST_TI_8_4 ( &(vcast_param[VCAST_TI_12_964_index]));
          VCAST_TI_12_964_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_964_local_field;
        } /* if */
        if (VCAST_TI_12_964_index >= VCAST_TI_12_964_last)
          break;
        VCAST_TI_12_964_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_964_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_964 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_896 ( struct codrv_info *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_896 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_896 ( struct codrv_info *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwParam_crc */
      case 1: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwParam_crc))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->uwParam_cks */
      case 2: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwParam_cks))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ulParam_veh_type */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulParam_veh_type))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ulParam_revision */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulParam_revision))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->szSoftware_pn */
      case 5: { 
        VCAST_TI_12_897 ( ((char *)(vcast_param->szSoftware_pn)));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->szSerial_number */
      case 6: { 
        VCAST_TI_12_897 ( ((char *)(vcast_param->szSerial_number)));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->szHardwarePn */
      case 7: { 
        VCAST_TI_12_897 ( ((char *)(vcast_param->szHardwarePn)));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->szModulePn */
      case 8: { 
        VCAST_TI_12_897 ( ((char *)(vcast_param->szModulePn)));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->szParameterPn */
      case 9: { 
        VCAST_TI_12_897 ( ((char *)(vcast_param->szParameterPn)));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->uwAppl_crc */
      case 10: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwAppl_crc))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->uwAppl_cks */
      case 11: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwAppl_cks))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->uwSetupsCfgCrc */
      case 12: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwSetupsCfgCrc))));
        break; /* end case 12*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_896 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_955 ( struct mi_driver *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_955 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_955 ( struct mi_driver *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->szName */
      case 1: { 
        VCAST_TI_12_897 ( ((char *)(vcast_param->szName)));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->data */
      case 2: { 
        VCAST_TI_12_953 ( ((struct mi_driver_data *)(&(vcast_param->data))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->config_table */
      case 3: { 
        VCAST_TI_12_954 ( ((struct mi_table *)(&(vcast_param->config_table))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->upload_table */
      case 4: { 
        VCAST_TI_12_954 ( ((struct mi_table *)(&(vcast_param->upload_table))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->upload_data */
      case 5: { 
        VCAST_TI_12_896 ( ((struct codrv_info *)(&(vcast_param->upload_data))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->uwOneTimeConfigPassValue */
      case 6: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwOneTimeConfigPassValue))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->fSendSdos */
      case 7: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSendSdos))));
        break; /* end case 7*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_955 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_1090 ( char vcast_param[160U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_1090 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_1090 ( char vcast_param[160U] ) 
{
  {
    int VCAST_TI_12_1090_array_index = 0;
    int VCAST_TI_12_1090_index = 0;
    int VCAST_TI_12_1090_first, VCAST_TI_12_1090_last;
    int VCAST_TI_12_1090_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_1090_local_field = 0;
    int VCAST_TI_12_1090_value_printed = 0;
    int VCAST_TI_12_1090_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_1090_first, &VCAST_TI_12_1090_last, &VCAST_TI_12_1090_more_data);
    VCAST_TI_12_1090_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_1090_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,160U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_1090_upper = 160U;
      for (VCAST_TI_12_1090_array_index=0; VCAST_TI_12_1090_array_index< VCAST_TI_12_1090_upper; VCAST_TI_12_1090_array_index++){
        if ( (VCAST_TI_12_1090_index >= VCAST_TI_12_1090_first) && ( VCAST_TI_12_1090_index <= VCAST_TI_12_1090_last)){
          if ( VCAST_TI_12_1090_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_1090_upper);
          else
            VCAST_TI_8_4 ( &(vcast_param[VCAST_TI_12_1090_index]));
          VCAST_TI_12_1090_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_1090_local_field;
        } /* if */
        if (VCAST_TI_12_1090_index >= VCAST_TI_12_1090_last)
          break;
        VCAST_TI_12_1090_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_1090_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_1090 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_897 ( char vcast_param[16U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_897 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_897 ( char vcast_param[16U] ) 
{
  {
    int VCAST_TI_12_897_array_index = 0;
    int VCAST_TI_12_897_index = 0;
    int VCAST_TI_12_897_first, VCAST_TI_12_897_last;
    int VCAST_TI_12_897_more_data; /* true if there is more data in the current command */
    int VCAST_TI_12_897_local_field = 0;
    int VCAST_TI_12_897_value_printed = 0;
    int VCAST_TI_12_897_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_12_897_first, &VCAST_TI_12_897_last, &VCAST_TI_12_897_more_data);
    VCAST_TI_12_897_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_12_897_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_12_897_upper = 16U;
      for (VCAST_TI_12_897_array_index=0; VCAST_TI_12_897_array_index< VCAST_TI_12_897_upper; VCAST_TI_12_897_array_index++){
        if ( (VCAST_TI_12_897_index >= VCAST_TI_12_897_first) && ( VCAST_TI_12_897_index <= VCAST_TI_12_897_last)){
          if ( VCAST_TI_12_897_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_12_897_upper);
          else
            VCAST_TI_8_4 ( &(vcast_param[VCAST_TI_12_897_index]));
          VCAST_TI_12_897_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_12_897_local_field;
        } /* if */
        if (VCAST_TI_12_897_index >= VCAST_TI_12_897_last)
          break;
        VCAST_TI_12_897_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_12_897_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_12_897 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_953 ( struct mi_driver_data *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_953 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_953 ( struct mi_driver_data *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->eDriverIndex */
      case 1: { 
        VCAST_TI_12_952 ( ((enum mi_devdrv *)(&(vcast_param->eDriverIndex))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubCAN_port */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubCAN_port))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ubNode_ID */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubNode_ID))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->fHB_Monitor */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fHB_Monitor))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->fConfigured */
      case 5: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fConfigured))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->fReadModule */
      case 6: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fReadModule))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->fOperational */
      case 7: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fOperational))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->ubNumbBaseAddr */
      case 8: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubNumbBaseAddr))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->uwErrorNumber_init */
      case 9: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwErrorNumber_init))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->uwErrorNumber_config */
      case 10: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwErrorNumber_config))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->uwErrorNumber_startup */
      case 11: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwErrorNumber_startup))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->pvBaseAddr */
      case 12: { 
vcast_not_supported();
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->eOneTimeConfig */
      case 13: { 
        VCAST_TI_12_892 ( ((enum codrv_otc_mod *)(&(vcast_param->eOneTimeConfig))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->eCfgMode */
      case 14: { 
        VCAST_TI_12_891 ( ((enum codrv_cfgmode *)(&(vcast_param->eCfgMode))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->fRestartPending */
      case 15: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRestartPending))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->fOnetimeConfigReset */
      case 16: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fOnetimeConfigReset))));
        break; /* end case 16*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_953 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_954 ( struct mi_table *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_954 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_954 ( struct mi_table *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwTableLength */
      case 1: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwTableLength))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->pTable */
      case 2: { 
        VCAST_TI_12_895 ( ((struct sdo_table **)(&(vcast_param->pTable))));
        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_954 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_952 ( enum mi_devdrv *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_952 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_952 ( enum mi_devdrv *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum mi_devdrv ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = CODRVSEL_AMC_001;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = CODRVSEL_NONE;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_12_952 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_892 ( enum codrv_otc_mod *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_892 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_892 ( enum codrv_otc_mod *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum codrv_otc_mod ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = OTC_MOD_NONE;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = OTC_MOD_ZAPI;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_12_892 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_891 ( enum codrv_cfgmode *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_891 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_891 ( enum codrv_cfgmode *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum codrv_cfgmode ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = CODRV_CFGMODE_REG;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = CODRV_CFGMODE_NA;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_12_891 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_895 ( struct sdo_table **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_895 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_895 ( struct sdo_table **vcast_param ) 
{
  {
    int VCAST_TI_12_895_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_12_895_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_12_895_memory_ptr = (void**)vcast_param;
            *VCAST_TI_12_895_memory_ptr = (void*)VCAST_malloc(VCAST_TI_12_895_array_size*(sizeof(struct sdo_table )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_12_895_array_size*(sizeof(struct sdo_table )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_12_895_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_12_895_index = vcast_get_param();
          VCAST_TI_12_894 ( &((*vcast_param)[VCAST_TI_12_895_index]));
        }
      }
    }
  }
} /* end VCAST_TI_12_895 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_894 ( struct sdo_table *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_894 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_894 ( struct sdo_table *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->obj */
      case 1: { 
        VCAST_TI_12_893 ( ((struct sdo_obj *)(&(vcast_param->obj))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubAddrSelect */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubAddrSelect))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->uwAddrOffset */
      case 3: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwAddrOffset))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->uwFeatureDepOffset */
      case 4: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwFeatureDepOffset))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->uwDisableValOffset */
      case 5: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwDisableValOffset))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->eCfgModeSelect */
      case 6: { 
        VCAST_TI_12_891 ( ((enum codrv_cfgmode *)(&(vcast_param->eCfgModeSelect))));
        break; /* end case 6*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_894 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_12_893 ( struct sdo_obj *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_12_893 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_12_893 ( struct sdo_obj *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwIdx */
      case 1: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwIdx))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubSub */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubSub))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ubSize */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubSize))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ubDataType */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubDataType))));
        break; /* end case 4*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_12_893 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


#ifdef VCAST_PARADIGM_ADD_SEGMENT
#pragma new_codesegment(1)
#endif
void VCAST_TI_RANGE_DATA_12 ( void ) {
#define VCAST_TI_SCALAR_TYPE "NEW_SCALAR\n"
#define VCAST_TI_ARRAY_TYPE  "NEW_ARRAY\n"
  /* Range Data for TI (scalar) VCAST_TI_11_4 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200001\n" );
  vectorcast_fprint_unsigned_integer (vCAST_OUTPUT_FILE,UINT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_integer (vCAST_OUTPUT_FILE,(UINT_MIN / 2) + (UINT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_integer (vCAST_OUTPUT_FILE,UINT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (array) VCAST_TI_12_3 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100636\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_7 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100637\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IMM2VCM_CMD_COUNT);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_9 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100638\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_11 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100639\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IPS_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_12 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100640\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_14 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100641\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IPS_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_14 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100642\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IPS_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_14 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100643\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IPS_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_14 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100644\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IPS_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_12 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100645\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (scalar) VCAST_TI_12_19 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200013\n" );
  vectorcast_fprint_unsigned_long_long (vCAST_OUTPUT_FILE,0 );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_long_long (vCAST_OUTPUT_FILE,(ULLONG_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_long_long (vCAST_OUTPUT_FILE,ULLONG_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (array) VCAST_TI_12_20 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100646\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (scalar) VCAST_TI_11_25 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200016\n" );
  vectorcast_fprint_long (vCAST_OUTPUT_FILE,LONG_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_long (vCAST_OUTPUT_FILE,(LONG_MIN / 2) + (LONG_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_long (vCAST_OUTPUT_FILE,LONG_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
#ifndef VCAST_NO_FLOAT
  /* Range Data for TI (scalar) VCAST_TI_8_3 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200017\n" );
  vectorcast_fprint_long_float (vCAST_OUTPUT_FILE,-(FLT_MAX) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_long_float (vCAST_OUTPUT_FILE,VCAST_FLT_MID );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_long_float (vCAST_OUTPUT_FILE,FLT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
#endif
  /* Range Data for TI (scalar) VCAST_TI_12_25 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200018\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,SCHAR_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(SCHAR_MIN / 2) + (SCHAR_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,SCHAR_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_8_5 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200019\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,UCHAR_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(UCHAR_MIN / 2) + (UCHAR_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,UCHAR_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_12_27 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200020\n" );
  vectorcast_fprint_short (vCAST_OUTPUT_FILE,SHRT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_short (vCAST_OUTPUT_FILE,(SHRT_MIN / 2) + (SHRT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_short (vCAST_OUTPUT_FILE,SHRT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_9_16 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200021\n" );
  vectorcast_fprint_unsigned_short (vCAST_OUTPUT_FILE,USHRT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_short (vCAST_OUTPUT_FILE,(USHRT_MIN / 2) + (USHRT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_short (vCAST_OUTPUT_FILE,USHRT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_11_31 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200023\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,INT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(INT_MIN / 2) + (INT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,INT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_8_4 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1200029\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,CHAR_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(CHAR_MIN/2) + (CHAR_MAX/2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,CHAR_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (array) VCAST_TI_12_263 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100759\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_291 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100760\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_292 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100761\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,AG_NUMB_MODULE_INPUTS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_293 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100762\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,431);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_294 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100763\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,AG_NUMB_MODULE_OUTPUTS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_295 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100764\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,160);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_296 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100765\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,431);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_297 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100766\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,160);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_303 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100770\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_304 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100771\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_305 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100772\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_306 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100773\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_313 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100777\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_314 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100778\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_315 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100779\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_316 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100780\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_317 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100781\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_318 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100782\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_319 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100783\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_320 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100784\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_321 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100785\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_322 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100786\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_323 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100787\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_324 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100788\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_325 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100789\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_326 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100790\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_327 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100791\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_328 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100792\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_329 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100793\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_330 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100794\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_331 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100795\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_332 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100796\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_347 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100804\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_348 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100805\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_351 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100807\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_352 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100808\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_357 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100811\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,11);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_358 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100812\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,11);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_377 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100822\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_378 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100823\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_379 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100824\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_380 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100825\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_381 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100826\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_382 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100827\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_383 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100828\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_384 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100829\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_385 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100830\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_386 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100831\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_387 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100832\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_388 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100833\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_389 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100834\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_390 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100835\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_391 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100836\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_392 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100837\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_393 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100838\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_396 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100840\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,15);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_436 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100861\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_437 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100862\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_438 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100863\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_439 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100864\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_440 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100865\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_441 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100866\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_442 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100867\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_443 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100868\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_444 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100869\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_445 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100870\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_446 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100871\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_447 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100872\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_448 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100873\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_449 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100874\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_458 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100879\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_459 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100880\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_460 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100881\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_461 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100882\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_462 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100883\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_463 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100884\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_464 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100885\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_465 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100886\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_466 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100887\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_467 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100888\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_468 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100889\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_469 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100890\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_470 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100891\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_471 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100892\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_472 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100893\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_473 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100894\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_474 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100895\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100898\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100899\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100900\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100901\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_483 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100902\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_483 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100903\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_483 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100904\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_483 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100905\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100906\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100907\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100908\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100909\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100910\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100911\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100912\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100913\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100914\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100915\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100916\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100917\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100918\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100919\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100920\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100921\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100922\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100923\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100924\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100925\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100926\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100927\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100928\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100929\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100930\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_497 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100931\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_537 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100944\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_538 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100945\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_539 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100946\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_540 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100947\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_541 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100948\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_542 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100949\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_543 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100950\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_544 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100951\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_571 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100965\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,17);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_578 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100969\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_579 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100970\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_580 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100971\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_581 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100972\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,28);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_582 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100973\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_583 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100974\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_584 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100975\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_585 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100976\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_586 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100977\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_587 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100978\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_588 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100979\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_589 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100980\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_590 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100981\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_591 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100982\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_592 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100983\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_593 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100984\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_594 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100985\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_595 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100986\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_596 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100987\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_597 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100988\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_598 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100989\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_599 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100990\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_600 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100991\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_601 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100992\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_602 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100993\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_603 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100994\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_604 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100995\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_605 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100996\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_606 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100997\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_607 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100998\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_608 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100999\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_609 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101000\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_610 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101001\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_611 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101002\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_612 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101003\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_613 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101004\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_614 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101005\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_615 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101006\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_616 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101007\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_619 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101009\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_620 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101010\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_621 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101011\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_622 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101012\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_623 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101013\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_624 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101014\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_625 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101015\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_626 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101016\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_627 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101017\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_628 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101018\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_629 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101019\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_630 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101020\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_631 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101021\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_632 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101022\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_633 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101023\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_634 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101024\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_635 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101025\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_636 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101026\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_637 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101027\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_638 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101028\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_639 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101029\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_640 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101030\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_641 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101031\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_642 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101032\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_643 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101033\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_644 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101034\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_645 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101035\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_646 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101036\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_647 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101037\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_648 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101038\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_649 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101039\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_650 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101040\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_651 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101041\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_652 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101042\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_653 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101043\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_654 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101044\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_655 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101045\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_656 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101046\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_657 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101047\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_658 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101048\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_659 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101049\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_660 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101050\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_661 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101051\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_662 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101052\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_663 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101053\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_664 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101054\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_665 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101055\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_666 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101056\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_667 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101057\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_668 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101058\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_669 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101059\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_670 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101060\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_671 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101061\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_672 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101062\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_673 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101063\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_674 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101064\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_675 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101065\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_676 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101066\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_677 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101067\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_678 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101068\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_679 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101069\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_680 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101070\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_681 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101071\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_682 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101072\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_683 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101073\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_684 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101074\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_685 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101075\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_686 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101076\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_687 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101077\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_688 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101078\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_689 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101079\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_690 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101080\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_691 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101081\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_692 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101082\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_693 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101083\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_694 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101084\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_695 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101085\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_696 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101086\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_697 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101087\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_698 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101088\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_699 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101089\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_700 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101090\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_701 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101091\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_702 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101092\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_703 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101093\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_704 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101094\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_705 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101095\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_706 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101096\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_707 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101097\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_708 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101098\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_709 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101099\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_710 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101100\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_711 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101101\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_712 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101102\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_713 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101103\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_714 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101104\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_715 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101105\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_716 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101106\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_717 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101107\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_718 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101108\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_719 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101109\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_720 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101110\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_721 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101111\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_722 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101112\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_723 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101113\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_724 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101114\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_725 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101115\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_750 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101128\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,20);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_751 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101129\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,20);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_754 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101131\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_755 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101132\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_758 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101134\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_759 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101135\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_760 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101136\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_761 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101137\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_762 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101138\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_763 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101139\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_764 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101140\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_771 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101144\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_772 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101145\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_773 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101146\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_774 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101147\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_775 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101148\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_778 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101150\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_779 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101151\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_780 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101152\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_781 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101153\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_782 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101154\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_783 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101155\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_784 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101156\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_785 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101157\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_786 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101158\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_787 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101159\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_788 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101160\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_789 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101161\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_790 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101162\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_791 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101163\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_792 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101164\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_793 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101165\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_794 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101166\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_795 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101167\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_796 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101168\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_797 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101169\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_798 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101170\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_799 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101171\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_800 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101172\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_801 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101173\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_829 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101188\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_830 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101189\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_834 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101192\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_835 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101193\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_836 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101194\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_837 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101195\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101196\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_851 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101199\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_852 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101200\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_857 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101201\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,18U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_880 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101202\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IO_SF_NUMB_ELEMENTS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_881 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101203\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IO_NUMB_HW_INPUTS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_882 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101204\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,46);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_883 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101205\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,46);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_886 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101206\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_889 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101207\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_897 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101208\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_897 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101209\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_897 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101210\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_897 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101211\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_897 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101212\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_904 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101213\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_905 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101214\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_906 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101215\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_908 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101216\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_909 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101217\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_910 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101218\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_911 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101219\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_912 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101220\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_913 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101221\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_914 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101222\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_915 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101223\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_916 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101224\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_917 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101225\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_918 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101226\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_919 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101227\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_920 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101228\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_921 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101229\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_922 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101230\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_923 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101231\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_924 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101232\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_925 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101233\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_926 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101234\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_927 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101235\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_928 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101236\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_929 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101237\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_930 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101238\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_931 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101239\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_932 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101240\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_933 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101241\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_936 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101242\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_940 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101243\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_941 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101244\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_942 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101245\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_949 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101246\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_950 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101247\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_951 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101248\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_897 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101250\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_959 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101251\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_960 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101252\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_962 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101253\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,10U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_964 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101254\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,36U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_967 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101255\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,18U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_975 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101256\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,GYRO_NUM_AXES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_977 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101257\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,GYRO_NUM_AXES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_978 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101258\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(124U - sizeof(uint32_t)) - (uint32_t)GYRO_NUM_AXES * sizeof(gyro_cals_t));
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_985 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101259\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_988 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101260\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ACCEL_NUM_AXES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_989 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101261\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(124U - sizeof(uint32_t)) - (uint32_t)ACCEL_NUM_AXES * sizeof(accel_cals_t));
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_995 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101262\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1003 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101263\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1006 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101264\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1008 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101265\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1010 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101266\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1012 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101267\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1013 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101268\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1014 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101269\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1015 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101270\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1016 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101271\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1017 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101272\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1018 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101273\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,GP_NUM_DRIVERS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1019 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101274\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1020 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101275\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1021 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101276\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1022 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101277\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,SDI_NUMB_OF_SWITCH_DRIVERS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1023 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101278\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,24U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1024 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101279\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1025 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101280\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1026 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101281\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1027 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101282\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,10U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1028 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101283\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,MAX_ALARM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1029 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101284\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,66);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1030 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101285\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1269U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1031 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101286\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1032 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101287\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1033 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101288\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1034 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101289\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1035 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101290\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1036 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101291\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1037 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101292\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1038 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101293\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,11U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1039 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101294\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,14U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1040 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101295\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ADC_NUM_MODULES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1041 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101296\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,90U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1042 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101297\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ADC_NUM_MODULES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,90U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1043 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101298\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1044 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101299\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,SDI_NUMB_OF_SWITCH_DRIVERS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1045 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101300\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1046 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101301\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1047 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101302\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1048 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101303\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1049 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101304\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,NUM_MOTION_DEVICES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1050 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101305\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,NUM_MOTION_DEVICES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1051 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101306\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,NUM_MOTION_DEVICES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1052 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101307\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ETPU2_SPI_NUM_INSTANCES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1053 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101308\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ETPU2_SPI_NUM_INSTANCES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1054 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101309\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1055 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101310\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1057 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101312\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,15);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1089 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101327\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,160U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1090 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101328\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,160U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1091 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101329\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1092 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101330\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,((764U - sizeof(uint8_t)) - sizeof(uint8_t) * 2U) - (sizeof(char) * 2U) * 160U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1094 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101331\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U - 4U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1095 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101332\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1096 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101333\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1097 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101334\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,34);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1098 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101335\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1099 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101336\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,50);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1098 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101337\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1098 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101338\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1102 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101339\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1103 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101340\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1104 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101341\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1105 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101342\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,31U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1106 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101343\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1107 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101344\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,28U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1108 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101345\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,28U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_20 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101347\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1110 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101348\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_12_1106 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"101349\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
}
/* Include the file which contains function implementations
for stub processing and value/expected user code */
#include "imm_interface_uc.c"

void vCAST_COMMON_STUB_PROC_12(
            int unitIndex,
            int subprogramIndex,
            int robjectIndex,
            int readEobjectData )
{
   vCAST_BEGIN_STUB_PROC_12(unitIndex, subprogramIndex);
   if ( robjectIndex )
      vCAST_READ_COMMAND_DATA_FOR_ONE_PARAM( unitIndex, subprogramIndex, robjectIndex );
   if ( readEobjectData )
      vCAST_READ_COMMAND_DATA_FOR_ONE_PARAM( unitIndex, subprogramIndex, 0 );
   vCAST_SET_HISTORY( unitIndex, subprogramIndex );
   vCAST_READ_COMMAND_DATA( vCAST_CURRENT_SLOT, unitIndex, subprogramIndex, vCAST_true, vCAST_false );
   vCAST_READ_COMMAND_DATA_FOR_USER_GLOBALS();
   vCAST_STUB_PROCESSING_12(unitIndex, subprogramIndex);
}
#endif /* VCAST_HEADER_EXPANSION */
