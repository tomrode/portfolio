#ifndef VCAST_NEVER_STUB_EXTERNS
/* Begin defined extern variables */

/* INITIALIZATION_OBJECTS_USER_CODE "44" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "44" */
dem_report_structure_t gDEM_ReportStructure 
/* INITIALIZATION_CODE_USER_CODE "44" */

/* INITIALIZATION_CODE_USER_CODE_END "44" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "45" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "45" */
analog_in_monitor_bus_t gAnalog_in_monitor_bus 
/* INITIALIZATION_CODE_USER_CODE "45" */

/* INITIALIZATION_CODE_USER_CODE_END "45" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "46" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "46" */
current_mon_bus_t gCurrent_mon_bus 
/* INITIALIZATION_CODE_USER_CODE "46" */

/* INITIALIZATION_CODE_USER_CODE_END "46" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "47" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "47" */
features_bus_t gFeatures_bus 
/* INITIALIZATION_CODE_USER_CODE "47" */

/* INITIALIZATION_CODE_USER_CODE_END "47" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "48" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "48" */
powerbase_fb_main_bus_t gPowerbase_fb 
/* INITIALIZATION_CODE_USER_CODE "48" */

/* INITIALIZATION_CODE_USER_CODE_END "48" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "49" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "49" */
steer_inputs_bus_t gSteer_inputs 
/* INITIALIZATION_CODE_USER_CODE "49" */

/* INITIALIZATION_CODE_USER_CODE_END "49" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "50" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "50" */
steering_ms_fdbk_bus_t gSteering_fdbk 
/* INITIALIZATION_CODE_USER_CODE "50" */

/* INITIALIZATION_CODE_USER_CODE_END "50" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "51" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "51" */
throttle_in_bus_t gThrottleIn 
/* INITIALIZATION_CODE_USER_CODE "51" */

/* INITIALIZATION_CODE_USER_CODE_END "51" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "52" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "52" */
voltage_mon_bus_t gVoltage_mon_bus 
/* INITIALIZATION_CODE_USER_CODE "52" */

/* INITIALIZATION_CODE_USER_CODE_END "52" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "53" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "53" */
vehicle_fb_c2m_bus_t gVehicle_fb_c2m_bus 
/* INITIALIZATION_CODE_USER_CODE "53" */

/* INITIALIZATION_CODE_USER_CODE_END "53" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "54" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "54" */
AutoCal_Cm_Calib_bus_t gAutoCalImmStat 
/* INITIALIZATION_CODE_USER_CODE "54" */

/* INITIALIZATION_CODE_USER_CODE_END "54" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "55" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "55" */
performance_bus_t gPerformance_bus 
/* INITIALIZATION_CODE_USER_CODE "55" */

/* INITIALIZATION_CODE_USER_CODE_END "55" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "56" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "56" */
TestOutput_input_bus_t gTestOutputInputs 
/* INITIALIZATION_CODE_USER_CODE "56" */

/* INITIALIZATION_CODE_USER_CODE_END "56" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "57" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "57" */
hour_meter_input_bus_t gHourMeterInput 
/* INITIALIZATION_CODE_USER_CODE "57" */

/* INITIALIZATION_CODE_USER_CODE_END "57" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "58" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "58" */
Rack_Status_bus_t gRack_Status 
/* INITIALIZATION_CODE_USER_CODE "58" */

/* INITIALIZATION_CODE_USER_CODE_END "58" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "59" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "59" */
int8_t gsbTravelDirection 
/* INITIALIZATION_CODE_USER_CODE "59" */

/* INITIALIZATION_CODE_USER_CODE_END "59" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "60" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "60" */
accum_data_meters_t gAccumDataMeters 
/* INITIALIZATION_CODE_USER_CODE "60" */

/* INITIALIZATION_CODE_USER_CODE_END "60" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "61" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "61" */
mm_vehicle_manifest_t gVehicle_Manifest 
/* INITIALIZATION_CODE_USER_CODE "61" */

/* INITIALIZATION_CODE_USER_CODE_END "61" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "62" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "62" */
alarm_tone_pattern_t geSoundID 
/* INITIALIZATION_CODE_USER_CODE "62" */

/* INITIALIZATION_CODE_USER_CODE_END "62" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "63" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "63" */
EnergySource_output_bus_t gEnergySource_output 
/* INITIALIZATION_CODE_USER_CODE "63" */

/* INITIALIZATION_CODE_USER_CODE_END "63" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "64" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "64" */
function_active_output_bus_t gFunction_active 
/* INITIALIZATION_CODE_USER_CODE "64" */

/* INITIALIZATION_CODE_USER_CODE_END "64" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "65" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "65" */
hyd_output_bus_t gHyd_output 
/* INITIALIZATION_CODE_USER_CODE "65" */

/* INITIALIZATION_CODE_USER_CODE_END "65" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "66" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "66" */
steer_m2m_output_bus_t gSteer_m2m_outputs 
/* INITIALIZATION_CODE_USER_CODE "66" */

/* INITIALIZATION_CODE_USER_CODE_END "66" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "67" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "67" */
sw_flag_output_bus_t gSw_flag_outputs 
/* INITIALIZATION_CODE_USER_CODE "67" */

/* INITIALIZATION_CODE_USER_CODE_END "67" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "68" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "68" */
traction_output_bus_t gTraction_output 
/* INITIALIZATION_CODE_USER_CODE "68" */

/* INITIALIZATION_CODE_USER_CODE_END "68" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "69" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "69" */
vehicle_sro_word_bus_t gvehicle_sro_word 
/* INITIALIZATION_CODE_USER_CODE "69" */

/* INITIALIZATION_CODE_USER_CODE_END "69" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "70" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "70" */
VehicleFB_output_bus_t gVehicleFB_output 
/* INITIALIZATION_CODE_USER_CODE "70" */

/* INITIALIZATION_CODE_USER_CODE_END "70" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "71" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "71" */
brake_analyzer_out_bus_t gBrake_analyzer_out_bus 
/* INITIALIZATION_CODE_USER_CODE "71" */

/* INITIALIZATION_CODE_USER_CODE_END "71" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "72" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "72" */
steer_dx_out_bus_t gSteer_dx_out_bus 
/* INITIALIZATION_CODE_USER_CODE "72" */

/* INITIALIZATION_CODE_USER_CODE_END "72" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "73" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "73" */
AutoCal_Mcc_Calib_bus_t gAutoCalMccStat 
/* INITIALIZATION_CODE_USER_CODE "73" */

/* INITIALIZATION_CODE_USER_CODE_END "73" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "74" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "74" */
AutoCal_PvFormat_bus_t gAutoCalPvFormat 
/* INITIALIZATION_CODE_USER_CODE "74" */

/* INITIALIZATION_CODE_USER_CODE_END "74" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "75" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "75" */
AutoCal_DeFormat_bus_t gAutoCalDataEntryFormat 
/* INITIALIZATION_CODE_USER_CODE "75" */

/* INITIALIZATION_CODE_USER_CODE_END "75" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "76" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "76" */
AutoCal_Steps_bus_t gAutoCalSteps 
/* INITIALIZATION_CODE_USER_CODE "76" */

/* INITIALIZATION_CODE_USER_CODE_END "76" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "77" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "77" */
AGV_output_bus_t gAGV_output 
/* INITIALIZATION_CODE_USER_CODE "77" */

/* INITIALIZATION_CODE_USER_CODE_END "77" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "78" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "78" */
info1_model_t Info1_VehicleModel 
/* INITIALIZATION_CODE_USER_CODE "78" */

/* INITIALIZATION_CODE_USER_CODE_END "78" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "79" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "79" */
uint16_t guwVcmStatusWord 
/* INITIALIZATION_CODE_USER_CODE "79" */

/* INITIALIZATION_CODE_USER_CODE_END "79" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "80" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "80" */
uint8_t gubKeyPressArmrestKeyboard 
/* INITIALIZATION_CODE_USER_CODE "80" */

/* INITIALIZATION_CODE_USER_CODE_END "80" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "81" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "81" */
int8_t gsbKnobAsKeySigArmrestKeyboard 
/* INITIALIZATION_CODE_USER_CODE "81" */

/* INITIALIZATION_CODE_USER_CODE_END "81" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "82" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "82" */
AutoCal_Status_bus_t gFRAM_AutoCalStatus 
/* INITIALIZATION_CODE_USER_CODE "82" */

/* INITIALIZATION_CODE_USER_CODE_END "82" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "83" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "83" */
UserCutOuts_calib_c2m_bus_t gFRAM_Usercutout_cals_c2m 
/* INITIALIZATION_CODE_USER_CODE "83" */

/* INITIALIZATION_CODE_USER_CODE_END "83" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "84" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "84" */
RackSelectSetups_calib_c2m_bus_t gFRAM_RackSelectSetup_cals_c2m 
/* INITIALIZATION_CODE_USER_CODE "84" */

/* INITIALIZATION_CODE_USER_CODE_END "84" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "85" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "85" */
fram_BirthCert_t gFRAM_BirthCert 
/* INITIALIZATION_CODE_USER_CODE "85" */

/* INITIALIZATION_CODE_USER_CODE_END "85" */
;

/* End defined extern variables */
/* Begin defined static member variables */
/* End defined static member variables */
#endif /*VCAST_NEVER_STUB_EXTERNS*/
/* BEGIN PROTOTYPE STUBS */
unsigned short P_10_42_1

;
unsigned char P_10_42_2

;
void gvDEM_ReportEventState(uint16_t uwEventReferenceID, uint8_t ubState)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_42
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_42
  if ( vcast_is_in_driver ) {
    P_10_42_1 = uwEventReferenceID;
    P_10_42_2 = ubState;
    vCAST_COMMON_STUB_PROC_12( 10, 42, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_42
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_42
  vCAST_USER_CODE_TIMER_START();
  return;
}


unsigned short P_10_43_1

;
char *P_10_43_2

;
void gvDEM_EventReportAcknowlege_cb(uint16_t uwMessageID, char * pszString)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_43
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_43
  if ( vcast_is_in_driver ) {
    P_10_43_1 = uwMessageID;
    P_10_43_2 = pszString;
    vCAST_COMMON_STUB_PROC_12( 10, 43, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_43
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_43
  vCAST_USER_CODE_TIMER_START();
  return;
}


enum ips_index P_10_44_1

;
unsigned short P_10_44_2

;
void gvAG_BeginImpactLockout(ips_index_t eIpsIndex, uint16_t uwEventID)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_44
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_44
  if ( vcast_is_in_driver ) {
    P_10_44_1 = eIpsIndex;
    P_10_44_2 = uwEventID;
    vCAST_COMMON_STUB_PROC_12( 10, 44, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_44
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_44
  vCAST_USER_CODE_TIMER_START();
  return;
}


unsigned short P_10_45_1

;
void gvSUPV_sendBHMPairingCmd(uint16_t uwMAC)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_45
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_45
  if ( vcast_is_in_driver ) {
    P_10_45_1 = uwMAC;
    vCAST_COMMON_STUB_PROC_12( 10, 45, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_45
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_45
  vCAST_USER_CODE_TIMER_START();
  return;
}


unsigned char P_10_46_1

;
void gvAccumDataMeter_ImmUpdate_cb(uint8_t ubMeterIndex)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_46
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_46
  if ( vcast_is_in_driver ) {
    P_10_46_1 = ubMeterIndex;
    vCAST_COMMON_STUB_PROC_12( 10, 46, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_46
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_46
  vCAST_USER_CODE_TIMER_START();
  return;
}


unsigned short R_10_47;
uint16_t guwInfo1GetVcmStatusWord(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_47
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_47
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_12( 10, 47, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_47
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_47
  vCAST_USER_CODE_TIMER_START();
  return R_10_47;
}


unsigned char P_10_48_1

;
void gvNvMemory_SetBootSkipDlVal(boolean fSkip)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_48
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_48
  if ( vcast_is_in_driver ) {
    P_10_48_1 = fSkip;
    vCAST_COMMON_STUB_PROC_12( 10, 48, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_48
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_48
  vCAST_USER_CODE_TIMER_START();
  return;
}


void gvBC_WriteFram(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_49
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_49
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_12( 10, 49, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_49
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_49
  vCAST_USER_CODE_TIMER_START();
  return;
}


unsigned long long R_10_50;
uint64_t gullTimer_ReadTimer(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_50
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_50
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_12( 10, 50, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_50
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_50
  vCAST_USER_CODE_TIMER_START();
  return R_10_50;
}


unsigned long long P_10_51_1

;
unsigned long long P_10_51_2

;
unsigned R_10_51;
uint32_t gulTimer_Counts_to_us(uint64_t ullStartTimer, uint64_t ullEndTimer)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_51
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_51
  if ( vcast_is_in_driver ) {
    P_10_51_1 = ullStartTimer;
    P_10_51_2 = ullEndTimer;
    vCAST_COMMON_STUB_PROC_12( 10, 51, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_51
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_51
  vCAST_USER_CODE_TIMER_START();
  return R_10_51;
}


/* END PROTOTYPE STUBS */
