#ifndef VCAST_NEVER_STUB_EXTERNS
/* Begin defined extern variables */

/* INITIALIZATION_OBJECTS_USER_CODE "1" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "1" */
test_mode_param_t gTestMode_Parameters 
/* INITIALIZATION_CODE_USER_CODE "1" */

/* INITIALIZATION_CODE_USER_CODE_END "1" */
;


/* INITIALIZATION_OBJECTS_USER_CODE "2" */
/* INITIALIZATION_OBJECTS_USER_CODE_END "2" */
const fixed_params_t gParameters 
/* INITIALIZATION_CODE_USER_CODE "2" */

/* INITIALIZATION_CODE_USER_CODE_END "2" */
;

/* End defined extern variables */
/* Begin defined static member variables */
/* End defined static member variables */
#endif /*VCAST_NEVER_STUB_EXTERNS*/
/* BEGIN PROTOTYPE STUBS */
struct TX_THREAD_STRUCT *P_10_1_1

;
char *P_10_1_2

;
void (*P_10_1_3)(uint32_t ulEntryInput)

;
unsigned P_10_1_4

;
unsigned P_10_1_5_memsize;
void *P_10_1_5

;
unsigned P_10_1_6

;
unsigned P_10_1_7

;
unsigned P_10_1_8

;
unsigned P_10_1_9

;
unsigned P_10_1_10

;
unsigned R_10_1;
uint32_t gulOS_ThreadCreate(os_thread_t * pThread, char * pszThreadName, void (* pvEntryPoint)(uint32_t ulEntryInput), uint32_t ulEntryInput, void * pvStackStart, uint32_t ulStackSize, uint32_t ulPriority, uint32_t ulPreemptThreashold, uint32_t ulTimeSlice, uint32_t ulAutoStart)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_1
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_1
  if ( vcast_is_in_driver ) {
    P_10_1_1 = pThread;
    P_10_1_2 = pszThreadName;
    P_10_1_3 = pvEntryPoint;
    P_10_1_4 = ulEntryInput;
    P_10_1_5 = pvStackStart;
    P_10_1_6 = ulStackSize;
    P_10_1_7 = ulPriority;
    P_10_1_8 = ulPreemptThreashold;
    P_10_1_9 = ulTimeSlice;
    P_10_1_10 = ulAutoStart;
    vCAST_COMMON_STUB_PROC_11( 10, 1, 11, 0 );
    if ( P_10_1_5_memsize > 0 )
      VCAST_memcpy ( pvStackStart, P_10_1_5, P_10_1_5_memsize );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_1
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_1
  vCAST_USER_CODE_TIMER_START();
  return R_10_1;
}


unsigned P_10_2_1

;
unsigned R_10_2;
uint32_t gulOS_ThreadSleep(uint32_t ulTimerTicks)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_2
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_2
  if ( vcast_is_in_driver ) {
    P_10_2_1 = ulTimerTicks;
    vCAST_COMMON_STUB_PROC_11( 10, 2, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_2
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_2
  vCAST_USER_CODE_TIMER_START();
  return R_10_2;
}


struct TX_QUEUE_STRUCT *P_10_3_1

;
char *P_10_3_2

;
unsigned P_10_3_3

;
unsigned P_10_3_4_memsize;
void *P_10_3_4

;
unsigned P_10_3_5

;
unsigned R_10_3;
uint32_t gulOS_QueueCreate(os_queue_t * pQueue, char * pszQueueName, uint32_t ulMessageSize, void * pvQueueStart, uint32_t ulQueueSize)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_3
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_3
  if ( vcast_is_in_driver ) {
    P_10_3_1 = pQueue;
    P_10_3_2 = pszQueueName;
    P_10_3_3 = ulMessageSize;
    P_10_3_4 = pvQueueStart;
    P_10_3_5 = ulQueueSize;
    vCAST_COMMON_STUB_PROC_11( 10, 3, 6, 0 );
    if ( P_10_3_4_memsize > 0 )
      VCAST_memcpy ( pvQueueStart, P_10_3_4, P_10_3_4_memsize );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_3
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_3
  vCAST_USER_CODE_TIMER_START();
  return R_10_3;
}


struct TX_QUEUE_STRUCT *P_10_4_1

;
unsigned P_10_4_2_memsize;
void *P_10_4_2

;
unsigned P_10_4_3

;
unsigned R_10_4;
uint32_t gulOS_QueueSend(os_queue_t * pQueue, void * pvSource, uint32_t ulWaitOption)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_4
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_4
  if ( vcast_is_in_driver ) {
    P_10_4_1 = pQueue;
    P_10_4_2 = pvSource;
    P_10_4_3 = ulWaitOption;
    vCAST_COMMON_STUB_PROC_11( 10, 4, 4, 0 );
    if ( P_10_4_2_memsize > 0 )
      VCAST_memcpy ( pvSource, P_10_4_2, P_10_4_2_memsize );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_4
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_4
  vCAST_USER_CODE_TIMER_START();
  return R_10_4;
}


struct TX_QUEUE_STRUCT *P_10_5_1

;
unsigned P_10_5_2_memsize;
void *P_10_5_2

;
unsigned P_10_5_3

;
unsigned R_10_5;
uint32_t gulOS_QueueReceive(os_queue_t * pQueue, void * pvDestination, uint32_t ulWaitOption)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_5
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_5
  if ( vcast_is_in_driver ) {
    P_10_5_1 = pQueue;
    P_10_5_2 = pvDestination;
    P_10_5_3 = ulWaitOption;
    vCAST_COMMON_STUB_PROC_11( 10, 5, 4, 0 );
    if ( P_10_5_2_memsize > 0 )
      VCAST_memcpy ( pvDestination, P_10_5_2, P_10_5_2_memsize );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_5
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_5
  vCAST_USER_CODE_TIMER_START();
  return R_10_5;
}


enum ip_init_status R_10_6;
ip_init_status_t geIP_Initialize(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_6
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_6
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 6, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_6
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_6
  vCAST_USER_CODE_TIMER_START();
  return R_10_6;
}


unsigned P_10_7_1

;
unsigned char R_10_7;
boolean gfIP_CheckLinkUp(uint32_t ulWaitOption)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_7
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_7
  if ( vcast_is_in_driver ) {
    P_10_7_1 = ulWaitOption;
    vCAST_COMMON_STUB_PROC_11( 10, 7, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_7
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_7
  vCAST_USER_CODE_TIMER_START();
  return R_10_7;
}


struct NX_TCP_SOCKET_STRUCT *P_10_8_1

;
char *P_10_8_2

;
void (*P_10_8_3)(NX_TCP_SOCKET * socket_ptr)

;
enum ip_socket_status R_10_8;
ip_socket_status_t geIP_SocketCreate(ip_tcp_socket_t * pSocket, char * pszName, vSOCKET_CREATE_CB pvDisconnect_cb)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_8
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_8
  if ( vcast_is_in_driver ) {
    P_10_8_1 = pSocket;
    P_10_8_2 = pszName;
    P_10_8_3 = pvDisconnect_cb;
    vCAST_COMMON_STUB_PROC_11( 10, 8, 4, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_8
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_8
  vCAST_USER_CODE_TIMER_START();
  return R_10_8;
}


struct NX_TCP_SOCKET_STRUCT *P_10_9_1

;
unsigned P_10_9_2

;
void (*P_10_9_3)(NX_TCP_SOCKET * socket_ptr, UINT port)

;
enum ip_socket_status R_10_9;
ip_socket_status_t geIP_SocketListen(ip_tcp_socket_t * pSocket, uint32_t ulPortNumber, vSOCKET_LISTEN_CB pvListenCallBack)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_9
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_9
  if ( vcast_is_in_driver ) {
    P_10_9_1 = pSocket;
    P_10_9_2 = ulPortNumber;
    P_10_9_3 = pvListenCallBack;
    vCAST_COMMON_STUB_PROC_11( 10, 9, 4, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_9
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_9
  vCAST_USER_CODE_TIMER_START();
  return R_10_9;
}


struct NX_TCP_SOCKET_STRUCT *P_10_10_1

;
unsigned P_10_10_2

;
enum ip_socket_status R_10_10;
ip_socket_status_t geIP_SocketAccept(ip_tcp_socket_t * pSocket, uint32_t ulWaitOption)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_10
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_10
  if ( vcast_is_in_driver ) {
    P_10_10_1 = pSocket;
    P_10_10_2 = ulWaitOption;
    vCAST_COMMON_STUB_PROC_11( 10, 10, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_10
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_10
  vCAST_USER_CODE_TIMER_START();
  return R_10_10;
}


unsigned P_10_11_1_memsize;
void *P_10_11_1

;
unsigned char *P_10_11_2

;
unsigned P_10_11_3

;
unsigned P_10_11_4

;
enum ip_socket P_10_11_5

;
unsigned P_10_11_6

;
unsigned P_10_11_7

;
enum ip_tx_status R_10_11;
ip_tx_status_t geIP_MessageSend(void * pSocket, uint8_t * pubBuffer, uint32_t ulMessageLength, uint32_t ulWaitOption, ip_socket_t eSocketType, uint32_t ulIpAddress, uint32_t ulPort)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_11
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_11
  if ( vcast_is_in_driver ) {
    P_10_11_1 = pSocket;
    P_10_11_2 = pubBuffer;
    P_10_11_3 = ulMessageLength;
    P_10_11_4 = ulWaitOption;
    P_10_11_5 = eSocketType;
    P_10_11_6 = ulIpAddress;
    P_10_11_7 = ulPort;
    vCAST_COMMON_STUB_PROC_11( 10, 11, 8, 0 );
    if ( P_10_11_1_memsize > 0 )
      VCAST_memcpy ( pSocket, P_10_11_1, P_10_11_1_memsize );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_11
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_11
  vCAST_USER_CODE_TIMER_START();
  return R_10_11;
}


struct NX_TCP_SOCKET_STRUCT *P_10_12_1

;
struct ip_packet_buffer *P_10_12_2

;
enum ip_rx_status *P_10_12_3

;
unsigned *P_10_12_4

;
char *R_10_12;
char *gszIP_MessageReceive(ip_tcp_socket_t * pSocket, ip_packet_buffer_t * pPacketBuffer, ip_rx_status_t * peStatus, uint32_t * pulMessageSize)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_12
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_12
  if ( vcast_is_in_driver ) {
    P_10_12_1 = pSocket;
    P_10_12_2 = pPacketBuffer;
    P_10_12_3 = peStatus;
    P_10_12_4 = pulMessageSize;
    vCAST_COMMON_STUB_PROC_11( 10, 12, 5, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_12
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_12
  vCAST_USER_CODE_TIMER_START();
  return R_10_12;
}


enum ip_init_status P_10_13_1

;
char *R_10_13;
char *gpszIP_GetInitializationStatus(ip_init_status_t eStatus)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_13
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_13
  if ( vcast_is_in_driver ) {
    P_10_13_1 = eStatus;
    vCAST_COMMON_STUB_PROC_11( 10, 13, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_13
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_13
  vCAST_USER_CODE_TIMER_START();
  return R_10_13;
}


enum ip_socket_status P_10_14_1

;
char *R_10_14;
char *gpszIP_GetSocketCreateStatus(ip_socket_status_t eStatus)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_14
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_14
  if ( vcast_is_in_driver ) {
    P_10_14_1 = eStatus;
    vCAST_COMMON_STUB_PROC_11( 10, 14, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_14
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_14
  vCAST_USER_CODE_TIMER_START();
  return R_10_14;
}


enum ip_tx_status P_10_15_1

;
char *R_10_15;
char *gpszIP_GetMessageSendStatus(ip_tx_status_t eStatus)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_15
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_15
  if ( vcast_is_in_driver ) {
    P_10_15_1 = eStatus;
    vCAST_COMMON_STUB_PROC_11( 10, 15, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_15
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_15
  vCAST_USER_CODE_TIMER_START();
  return R_10_15;
}


enum ip_rx_status P_10_16_1

;
char *R_10_16;
char *gpszIP_GetMessageReceiveStatus(ip_rx_status_t eStatus)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_16
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_16
  if ( vcast_is_in_driver ) {
    P_10_16_1 = eStatus;
    vCAST_COMMON_STUB_PROC_11( 10, 16, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_16
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_16
  vCAST_USER_CODE_TIMER_START();
  return R_10_16;
}


enum ppp_echo_status R_10_17;
ppp_echo_status_t geIP_SendLcpEchoRequest(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_17
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_17
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 17, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_17
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_17
  vCAST_USER_CODE_TIMER_START();
  return R_10_17;
}


unsigned char R_10_18;
boolean gfIP_LcpEchoResponseReceived(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_18
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_18
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 18, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_18
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_18
  vCAST_USER_CODE_TIMER_START();
  return R_10_18;
}


struct NX_TCP_SOCKET_STRUCT *P_10_19_1

;
unsigned P_10_19_2

;
enum ip_socket_status R_10_19;
ip_socket_status_t geIP_SocketDelete(ip_tcp_socket_t * pSocket, uint32_t ulPortNumber)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_19
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_19
  if ( vcast_is_in_driver ) {
    P_10_19_1 = pSocket;
    P_10_19_2 = ulPortNumber;
    vCAST_COMMON_STUB_PROC_11( 10, 19, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_19
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_19
  vCAST_USER_CODE_TIMER_START();
  return R_10_19;
}


enum ip_delete_status R_10_20;
ip_delete_status_t geIP_Delete(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_20
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_20
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 20, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_20
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_20
  vCAST_USER_CODE_TIMER_START();
  return R_10_20;
}


struct nx_packet_pool_info *P_10_21_1

;
void gvIP_GetAppPacketPoolInfo(nx_packet_pool_info_t * pInfo)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_21
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_21
  if ( vcast_is_in_driver ) {
    P_10_21_1 = pInfo;
    vCAST_COMMON_STUB_PROC_11( 10, 21, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_21
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_21
  vCAST_USER_CODE_TIMER_START();
  return;
}


struct nx_packet_pool_info *P_10_22_1

;
void gvIP_GetNetxPacketPoolInfo(nx_packet_pool_info_t * pInfo)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_22
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_22
  if ( vcast_is_in_driver ) {
    P_10_22_1 = pInfo;
    vCAST_COMMON_STUB_PROC_11( 10, 22, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_22
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_22
  vCAST_USER_CODE_TIMER_START();
  return;
}


struct nx_packet_pool_info *P_10_23_1

;
void gvIP_GetFtpPacketPoolInfo(nx_packet_pool_info_t * pInfo)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_23
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_23
  if ( vcast_is_in_driver ) {
    P_10_23_1 = pInfo;
    vCAST_COMMON_STUB_PROC_11( 10, 23, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_23
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_23
  vCAST_USER_CODE_TIMER_START();
  return;
}


struct NX_TCP_SOCKET_STRUCT *P_10_24_1

;
struct ip_socket_info *P_10_24_2

;
unsigned R_10_24;
uint32_t gulIP_GetSocketInfo(ip_tcp_socket_t * pSocket, ip_socket_info_t * pInfo)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_24
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_24
  if ( vcast_is_in_driver ) {
    P_10_24_1 = pSocket;
    P_10_24_2 = pInfo;
    vCAST_COMMON_STUB_PROC_11( 10, 24, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_24
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_24
  vCAST_USER_CODE_TIMER_START();
  return R_10_24;
}


struct NX_UDP_SOCKET_STRUCT *P_10_25_1

;
char *P_10_25_2

;
enum ip_socket_status R_10_25;
ip_socket_status_t geIP_UDPSocketCreate(ip_udp_socket_t * pSocket, char * pszName)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_25
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_25
  if ( vcast_is_in_driver ) {
    P_10_25_1 = pSocket;
    P_10_25_2 = pszName;
    vCAST_COMMON_STUB_PROC_11( 10, 25, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_25
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_25
  vCAST_USER_CODE_TIMER_START();
  return R_10_25;
}


struct NX_UDP_SOCKET_STRUCT *P_10_26_1

;
enum ip_socket_status R_10_26;
ip_socket_status_t geIP_UDPSocketDelete(ip_udp_socket_t * pSocket)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_26
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_26
  if ( vcast_is_in_driver ) {
    P_10_26_1 = pSocket;
    vCAST_COMMON_STUB_PROC_11( 10, 26, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_26
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_26
  vCAST_USER_CODE_TIMER_START();
  return R_10_26;
}


struct NX_UDP_SOCKET_STRUCT *P_10_27_1

;
unsigned P_10_27_2

;
unsigned P_10_27_3

;
enum ip_socket_status R_10_27;
ip_socket_status_t geIP_UDPSocket_Bind(ip_udp_socket_t * pSocket, uint32_t ulportnumber, uint32_t ulwait_option)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_27
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_27
  if ( vcast_is_in_driver ) {
    P_10_27_1 = pSocket;
    P_10_27_2 = ulportnumber;
    P_10_27_3 = ulwait_option;
    vCAST_COMMON_STUB_PROC_11( 10, 27, 4, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_27
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_27
  vCAST_USER_CODE_TIMER_START();
  return R_10_27;
}


enum ftp_status R_10_28;
ftp_status_t geIP_FtpServerCreate(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_28
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_28
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 28, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_28
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_28
  vCAST_USER_CODE_TIMER_START();
  return R_10_28;
}


enum ftp_status R_10_29;
ftp_status_t geIP_FtpServerDelete(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_29
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_29
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 29, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_29
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_29
  vCAST_USER_CODE_TIMER_START();
  return R_10_29;
}


unsigned char P_10_30_1

;
unsigned char P_10_30_2

;
unsigned char P_10_30_3

;
unsigned char P_10_30_4

;
unsigned R_10_30;
uint32_t gulIP_Address(uint8_t ubA, uint8_t ubB, uint8_t ubC, uint8_t ubD)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_30
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_30
  if ( vcast_is_in_driver ) {
    P_10_30_1 = ubA;
    P_10_30_2 = ubB;
    P_10_30_3 = ubC;
    P_10_30_4 = ubD;
    vCAST_COMMON_STUB_PROC_11( 10, 30, 5, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_30
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_30
  vCAST_USER_CODE_TIMER_START();
  return R_10_30;
}


char *P_10_31_1

;
void gvMyPrintf_b(char * pszString)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_31
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_31
  if ( vcast_is_in_driver ) {
    P_10_31_1 = pszString;
    vCAST_COMMON_STUB_PROC_11( 10, 31, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_31
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_31
  vCAST_USER_CODE_TIMER_START();
  return;
}


char *P_10_32_1

;
unsigned P_10_32_2_memsize;
void *P_10_32_2

;
signed int R_10_32;
int32_t gulPrintf1_b(char * pszString, const void * pvArg1)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_32
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_32
  if ( vcast_is_in_driver ) {
    P_10_32_1 = pszString;
    P_10_32_2 = ((void *)(pvArg1));
    vCAST_COMMON_STUB_PROC_11( 10, 32, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_32
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_32
  vCAST_USER_CODE_TIMER_START();
  return R_10_32;
}


char *P_10_33_1

;
unsigned P_10_33_2_memsize;
void *P_10_33_2

;
unsigned P_10_33_3_memsize;
void *P_10_33_3

;
signed int R_10_33;
int32_t gulPrintf2_b(char * pszString, const void * pvArg1, const void * pvArg2)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_33
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_33
  if ( vcast_is_in_driver ) {
    P_10_33_1 = pszString;
    P_10_33_2 = ((void *)(pvArg1));
    P_10_33_3 = ((void *)(pvArg2));
    vCAST_COMMON_STUB_PROC_11( 10, 33, 4, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_33
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_33
  vCAST_USER_CODE_TIMER_START();
  return R_10_33;
}


char *P_10_34_1

;
unsigned P_10_34_2

;
void gvPrint_ThreadCreateStatus(char * pszName, uint32_t ulStatus)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_34
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_34
  if ( vcast_is_in_driver ) {
    P_10_34_1 = pszName;
    P_10_34_2 = ulStatus;
    vCAST_COMMON_STUB_PROC_11( 10, 34, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_34
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_34
  vCAST_USER_CODE_TIMER_START();
  return;
}


char *P_10_35_1

;
unsigned P_10_35_2

;
void gvPrint_QueueCreateStatus(char * pszName, uint32_t ulStatus)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_35
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_35
  if ( vcast_is_in_driver ) {
    P_10_35_1 = pszName;
    P_10_35_2 = ulStatus;
    vCAST_COMMON_STUB_PROC_11( 10, 35, 3, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_35
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_35
  vCAST_USER_CODE_TIMER_START();
  return;
}


void gvXCP_ReflashTrap(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_36
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_36
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 36, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_36
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_36
  vCAST_USER_CODE_TIMER_START();
  return;
}


unsigned char R_10_37;
boolean gfXCP_ReflashInitiated(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_37
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_37
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 37, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_37
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_37
  vCAST_USER_CODE_TIMER_START();
  return R_10_37;
}


void gvRS422_Loopback_Test(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_38
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_38
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 38, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_38
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_38
  vCAST_USER_CODE_TIMER_START();
  return;
}


void gvRS422_Loopback_Init(void)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_39
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_39
  if ( vcast_is_in_driver ) {
    vCAST_COMMON_STUB_PROC_11( 10, 39, 1, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_39
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_39
  vCAST_USER_CODE_TIMER_START();
  return;
}


enum inst_queue_id P_10_40_1

;
struct TX_QUEUE_STRUCT *P_10_40_2

;
unsigned short P_10_40_3

;
unsigned char R_10_40;
boolean gfINST_ReportQueueDepth(inst_queue_id_t eQueue_ID, os_queue_t * pQueue, uint16_t uwDepth)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_40
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_40
  if ( vcast_is_in_driver ) {
    P_10_40_1 = eQueue_ID;
    P_10_40_2 = pQueue;
    P_10_40_3 = uwDepth;
    vCAST_COMMON_STUB_PROC_11( 10, 40, 4, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_40
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_40
  vCAST_USER_CODE_TIMER_START();
  return R_10_40;
}


enum inst_start_sequence P_10_41_1

;
void gvINST_LogTime(inst_start_sequence_t eSequence)
{
  vCAST_USER_CODE_TIMER_STOP();
#define BEGINNING_OF_STUB_USER_CODE_10_41
#include "vcast_configure_stub.c"
#undef BEGINNING_OF_STUB_USER_CODE_10_41
  if ( vcast_is_in_driver ) {
    P_10_41_1 = eSequence;
    vCAST_COMMON_STUB_PROC_11( 10, 41, 2, 0 );
  } /* vcast_is_in_driver */
#define END_OF_STUB_USER_CODE_10_41
#include "vcast_configure_stub.c"
#undef END_OF_STUB_USER_CODE_10_41
  vCAST_USER_CODE_TIMER_START();
  return;
}


/* END PROTOTYPE STUBS */
