/* Undefine all field names (disable by setting VCAST_DO_NOT_UNDEF_FIELDS) */
#undef AECP_SUCCESS
#undef AECP_RX_PARSE_ERROR
#undef AECP_CHAR
#undef AECP_STRING
#undef Data
#undef Procedures
#undef Object
#undef ubSize
#undef eType
#undef pvAddress
#undef uwID
#undef eMessageType
#undef puwNumberObjects
#undef puwMaxNumberObjects
#undef pObjectList
#undef vCallback
#undef fInitialized
#undef fUseChecksum
#undef uwNumberObjects
#undef pObjectDictionary
#undef uwNumberTxMessages
#undef pTxMessageList
#undef uwNumberRxMessages
#undef pRxMessageList
#undef uwIndex
#undef ubSubindex
#undef eEncodeMessage
#undef eDecodeMessage
#undef fLookupObject
#undef AECP_DATA_ONLY
#undef AECP_OBJS_WITH_DATA
