void VCAST_TI_11_101 ( void (**vcast_param)(struct NX_IP_STRUCT *, void *) ) ;

void VCAST_TI_11_108 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) ;

void VCAST_TI_11_110 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_112 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_1123 ( void (**vcast_param)(uint32_t ulEntryInput) ) ;

void VCAST_TI_11_114 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) ;

void VCAST_TI_11_116 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_118 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) ;

void VCAST_TI_11_121 ( void (**vcast_param)(struct NX_UDP_SOCKET_STRUCT * socket_ptr) ) ;

void VCAST_TI_11_125 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) ;

void VCAST_TI_11_128 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) ;

void VCAST_TI_11_130 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) ;

void VCAST_TI_11_132 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) ;

void VCAST_TI_11_134 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) ;

void VCAST_TI_11_138 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) ;

void VCAST_TI_11_140 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_142 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_144 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_149 ( void (**vcast_param)(NX_TCP_SOCKET * socket_ptr, UINT port) ) ;

void VCAST_TI_11_156 ( UINT (**vcast_param)(struct NX_IP_STRUCT *, struct NX_ARP_STRUCT **) ) ;

void VCAST_TI_11_158 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_160 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_162 ( void (**vcast_param)(struct NX_IP_STRUCT *, ULONG destination_ip, NX_INTERFACE * nx_interface) ) ;

void VCAST_TI_11_164 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) ;

void VCAST_TI_11_166 ( void (**vcast_param)(void *) ) ;

void VCAST_TI_11_168 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_170 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_172 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_175 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) ;

void VCAST_TI_11_180 ( void (**vcast_param)(struct NX_IP_DRIVER_STRUCT *) ) ;

void VCAST_TI_11_182 ( void (**vcast_param)(NX_TCP_SOCKET * socket_ptr) ) ;

void VCAST_TI_11_184 ( void (**vcast_param)(NX_TCP_SOCKET * socket_ptr, UINT port) ) ;

void VCAST_TI_11_56 ( void (**vcast_param)(ULONG id) ) ;

void VCAST_TI_11_62 ( void (**vcast_param)(struct TX_THREAD_STRUCT * thread_ptr, ULONG id) ) ;

void VCAST_TI_11_64 ( void (**vcast_param)(ULONG id) ) ;

void VCAST_TI_11_66 ( void (**vcast_param)(struct TX_THREAD_STRUCT * thread_ptr) ) ;

void VCAST_TI_11_84 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) ;

void VCAST_TI_11_86 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) ;

void VCAST_TI_11_88 ( UINT (**vcast_param)(struct NX_IP_STRUCT *, ULONG, NX_PACKET *) ) ;

void VCAST_TI_11_95 ( void (**vcast_param)(struct NX_IP_DRIVER_STRUCT *) ) ;

void VCAST_TI_11_97 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;

void VCAST_TI_11_99 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) ;


#ifdef __cplusplus
extern "C" {
#endif
void VCAST_TI_11_102 ( unsigned vcast_param[1] ) ;

void VCAST_TI_11_103 ( struct NX_INTERFACE_STRUCT *vcast_param[1] ) ;

void VCAST_TI_11_11 ( unsigned char vcast_param[3328U] ) ;

void VCAST_TI_11_1118 ( enum inst_queue_id *vcast_param ) ;

void VCAST_TI_11_1119 ( enum inst_start_sequence *vcast_param ) ;

void VCAST_TI_11_1120 ( enum immcomm_rx_states *vcast_param ) ;

void VCAST_TI_11_1121 ( struct nx_log *vcast_param ) ;

void VCAST_TI_11_12 ( struct NX_TCP_SOCKET_STRUCT **vcast_param ) ;

void VCAST_TI_11_122 ( struct NX_UDP_SOCKET_STRUCT *vcast_param ) ;

void VCAST_TI_11_123 ( struct NX_UDP_SOCKET_STRUCT *vcast_param[32U] ) ;

void VCAST_TI_11_13 ( struct NX_UDP_SOCKET_STRUCT **vcast_param ) ;

void VCAST_TI_11_135 ( struct NX_TCP_SOCKET_STRUCT *vcast_param ) ;

void VCAST_TI_11_136 ( struct NX_TCP_SOCKET_STRUCT *vcast_param[32U] ) ;

void VCAST_TI_11_145 ( struct NX_TCP_LISTEN_STRUCT **vcast_param ) ;

void VCAST_TI_11_146 ( struct NX_TCP_LISTEN_STRUCT *vcast_param ) ;

void VCAST_TI_11_15 ( struct ip_packet_buffer **vcast_param ) ;

void VCAST_TI_11_150 ( struct NX_TCP_LISTEN_STRUCT vcast_param[4] ) ;

void VCAST_TI_11_151 ( struct NX_ARP_STRUCT ***vcast_param ) ;

void VCAST_TI_11_152 ( struct NX_ARP_STRUCT **vcast_param ) ;

void VCAST_TI_11_153 ( struct NX_ARP_STRUCT *vcast_param ) ;

void VCAST_TI_11_154 ( struct NX_ARP_STRUCT *vcast_param[32] ) ;

void VCAST_TI_11_16 ( enum ip_rx_status **vcast_param ) ;

void VCAST_TI_11_17 ( unsigned **vcast_param ) ;

void VCAST_TI_11_173 ( struct NX_INTERFACE_STRUCT vcast_param[1 + 1] ) ;

void VCAST_TI_11_176 ( struct NXD_IPV6_ADDRESS_STRUCT **vcast_param ) ;

void VCAST_TI_11_177 ( unsigned vcast_param[4] ) ;

void VCAST_TI_11_178 ( struct NXD_IPV6_ADDRESS_STRUCT *vcast_param ) ;

void VCAST_TI_11_185 ( enum ip_init_status *vcast_param ) ;

void VCAST_TI_11_186 ( enum ip_delete_status *vcast_param ) ;

void VCAST_TI_11_187 ( enum ip_socket_status *vcast_param ) ;

void VCAST_TI_11_188 ( enum ip_tx_status *vcast_param ) ;

void VCAST_TI_11_189 ( enum ip_rx_status *vcast_param ) ;

void VCAST_TI_11_190 ( enum ppp_echo_status *vcast_param ) ;

void VCAST_TI_11_191 ( enum ftp_status *vcast_param ) ;

void VCAST_TI_11_192 ( enum ip_socket *vcast_param ) ;

void VCAST_TI_11_193 ( struct ip_packet_buffer *vcast_param ) ;

void VCAST_TI_11_194 ( char vcast_param[3U * (256U + (((16U + 20U) + 20U) + 0U))] ) ;

void VCAST_TI_11_195 ( struct ip_socket_info *vcast_param ) ;

void VCAST_TI_11_196 ( struct nx_packet_pool_info *vcast_param ) ;

void VCAST_TI_11_197 ( enum test_skip_dl_type *vcast_param ) ;

void VCAST_TI_11_198 ( struct test_mode_param *vcast_param ) ;

void VCAST_TI_11_20 ( struct TX_QUEUE_STRUCT **vcast_param ) ;

void VCAST_TI_11_21 ( unsigned char vcast_param[(uint32_t)((4U * 1U) * 32U)] ) ;

void VCAST_TI_11_23 ( struct nx_packet_pool_info **vcast_param ) ;

void VCAST_TI_11_24 ( struct ip_socket_info **vcast_param ) ;

void VCAST_TI_11_25 ( long *vcast_param ) ;

void VCAST_TI_11_31 ( signed int *vcast_param ) ;

void VCAST_TI_11_4 ( unsigned *vcast_param ) ;

void VCAST_TI_11_52 ( struct TX_TIMER_INTERNAL_STRUCT *vcast_param ) ;

void VCAST_TI_11_53 ( struct TX_TIMER_INTERNAL_STRUCT ***vcast_param ) ;

void VCAST_TI_11_54 ( struct TX_TIMER_INTERNAL_STRUCT **vcast_param ) ;

void VCAST_TI_11_57 ( struct TX_TIMER_STRUCT *vcast_param ) ;

void VCAST_TI_11_58 ( struct TX_TIMER_STRUCT **vcast_param ) ;

void VCAST_TI_11_59 ( struct TX_THREAD_STRUCT *vcast_param ) ;

void VCAST_TI_11_67 ( struct TX_MUTEX_STRUCT **vcast_param ) ;

void VCAST_TI_11_69 ( struct TX_MUTEX_STRUCT *vcast_param ) ;

void VCAST_TI_11_7 ( struct TX_THREAD_STRUCT **vcast_param ) ;

void VCAST_TI_11_70 ( struct TX_EVENT_FLAGS_GROUP_STRUCT *vcast_param ) ;

void VCAST_TI_11_71 ( struct TX_EVENT_FLAGS_GROUP_STRUCT **vcast_param ) ;

void VCAST_TI_11_72 ( struct TX_QUEUE_STRUCT *vcast_param ) ;

void VCAST_TI_11_75 ( struct NXD_ADDRESS_STRUCT *vcast_param ) ;

void VCAST_TI_11_76 ( struct NX_PACKET_STRUCT *vcast_param ) ;

void VCAST_TI_11_78 ( struct NX_PACKET_POOL_STRUCT **vcast_param ) ;

void VCAST_TI_11_79 ( struct NX_PACKET_POOL_STRUCT *vcast_param ) ;

void VCAST_TI_11_8 ( unsigned char vcast_param[1536U] ) ;

void VCAST_TI_11_80 ( struct NX_INTERFACE_STRUCT **vcast_param ) ;

void VCAST_TI_11_81 ( struct NX_IP_STRUCT **vcast_param ) ;

void VCAST_TI_11_82 ( struct NX_PACKET_STRUCT **vcast_param ) ;

void VCAST_TI_11_91 ( struct NX_IP_STRUCT *vcast_param ) ;

void VCAST_TI_11_92 ( struct NX_INTERFACE_STRUCT *vcast_param ) ;

void VCAST_TI_8_1 ( int *vcast_param ) ;

void VCAST_TI_8_2 ( unsigned long *vcast_param ) ;

void VCAST_TI_8_3 ( float *vcast_param ) ;

void VCAST_TI_8_4 ( char *vcast_param ) ;

void VCAST_TI_8_5 ( unsigned char *vcast_param ) ;

void VCAST_TI_9_1 ( struct aecp_message_map **vcast_param ) ;

void VCAST_TI_9_11 ( void **vcast_param ) ;

void VCAST_TI_9_13 ( unsigned char **vcast_param ) ;

void VCAST_TI_9_16 ( unsigned short *vcast_param ) ;

void VCAST_TI_9_2 ( struct aecp_obj_entry **vcast_param ) ;

void VCAST_TI_9_29 ( struct aecp_driver *vcast_param ) ;

void VCAST_TI_9_8 ( char **vcast_param ) ;


#ifdef __cplusplus
}
#endif
