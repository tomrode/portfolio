
#ifdef __cplusplus
extern "C" {
#endif
void VCAST_TI_8_1 ( int *vcast_param ) ;

void VCAST_TI_8_2 ( unsigned long *vcast_param ) ;

void VCAST_TI_8_3 ( float *vcast_param ) ;

void VCAST_TI_8_4 ( char *vcast_param ) ;

void VCAST_TI_8_5 ( unsigned char *vcast_param ) ;

void VCAST_TI_8_6 ( char vcast_param[8] ) ;

void VCAST_TI_8_7 ( int vcast_param[4] ) ;


#ifdef __cplusplus
}
#endif
