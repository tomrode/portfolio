/***********************************************
 *      VectorCAST Test Harness Component      *
 *     Copyright 2018 Vector Software, Inc.    *
 *               6.4p (01/31/17)               *
 ***********************************************/
/***********************************************
 * VectorCAST Unit Information
 *
 * Name: immcomm_driver
 *
 * Path: C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/VCM_APP/src/drivers/comm/immcomm_driver.c
 *
 * Type: stub-by-function
 *
 * Unit Number: 11
 *
 ***********************************************/
#ifndef VCAST_DRIVER_ONLY
/* Include the file which contains function prototypes
for stub processing and value/expected user code */
#include "vcast_uc_prototypes.h"
#include "vcast_basics.h"
/* STUB_DEPENDENCY_USER_CODE */
/* STUB_DEPENDENCY_USER_CODE_END */
#else
#include "vcast_env_defines.h"
#define __VCAST_BASICS_H__
#endif /* VCAST_DRIVER_ONLY */
#ifndef VCAST_DRIVER_ONLY
#ifndef VCAST_DONT_RENAME_EXIT
#ifdef __cplusplus
extern "C" {
#endif
void exit (int status);
#ifdef __cplusplus
}
#endif
/* used to capture the exit call */
#define exit VCAST_exit
#endif /* VCAST_DONT_RENAME_EXIT */
#endif /* VCAST_DRIVER_ONLY */
#ifndef VCAST_DRIVER_ONLY
#define VCAST_HEADER_EXPANSION
#ifdef VCAST_COVERAGE
#include "immcomm_driver_inst_prefix.c"
#else
#include "immcomm_driver_vcast_prefix.c"
#endif
#ifdef VCAST_COVERAGE
/* If coverage is enabled, include the instrumented UUT */
#include "immcomm_driver_inst.c"
#else
/* If coverage is not enabled, include the original UUT */
#include "immcomm_driver_vcast.c"
#endif
#ifdef VCAST_COVERAGE
#include "immcomm_driver_inst_appendix.c"
#else
#include "immcomm_driver_vcast_appendix.c"
#endif
#endif /* VCAST_DRIVER_ONLY */
#include "immcomm_driver_driver_prefix.c"
#ifdef VCAST_HEADER_EXPANSION
#ifdef VCAST_COVERAGE
#include "immcomm_driver_exp_inst_driver.c"
#else
#include "immcomm_driver_expanded_driver.c"
#endif /*VCAST_COVERAGE*/
#else
#include "S0000009.h"
#include "vcast_undef_11.h"
/* Include the file which contains function prototypes
for stub processing and value/expected user code */
#include "vcast_uc_prototypes.h"
/* Begin Function read_tbu */
long R_11_1;
/* Begin Function read_tbl */
long R_11_2;
#include "vcast_stubs_11.c"
/* begin declarations of inlined friends */
/* end declarations of inlined friends */
void VCAST_DRIVER_11( int VC_SUBPROGRAM, char *VC_EVENT_FLAGS, char *VC_SLOT_DESCR ) {
#ifdef VCAST_SBF_UNITS_AVAILABLE
  vCAST_MODIFY_SBF_TABLE(11, VC_SUBPROGRAM, vCAST_false);
#endif
  switch( VC_SUBPROGRAM ) {
    case 0:
      vCAST_SET_HISTORY_FLAGS ( 11, 0, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      break;
    case 1: {
      /* long read_tbu(void) */
      vCAST_SET_HISTORY_FLAGS ( 11, 1, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_11_1 = 
      ( read_tbu( ) );
      break; }
    case 2: {
      /* long read_tbl(void) */
      vCAST_SET_HISTORY_FLAGS ( 11, 2, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_11_2 = 
      ( read_tbl( ) );
      break; }
    case 3: {
      /* uint32_t gulIMMCOMM_DriverInitialize(uint16_t uwNumberObjects, const aecp_obj_entry_t * pObjectDictionary, uint16_t uwNumberTxMessages, const aecp_message_map_t * pTxMessageList, uint16_t uwNumberRxMessages, const aecp_message_map_t * pRxMessageList, boolean fUseChecksum) */
      vCAST_SET_HISTORY_FLAGS ( 11, 3, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_11_3 = 
      ( gulIMMCOMM_DriverInitialize(
        ( P_11_3_1 ),
        ( ((const struct aecp_obj_entry *)(P_11_3_2)) ),
        ( P_11_3_3 ),
        ( ((const struct aecp_message_map *)(P_11_3_4)) ),
        ( P_11_3_5 ),
        ( ((const struct aecp_message_map *)(P_11_3_6)) ),
        ( P_11_3_7 ) ) );
      break; }
    case 4: {
      /* void vIMMCOMM_RxThreadEntry(uint32_t ulInput) */
      vCAST_SET_HISTORY_FLAGS ( 11, 4, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMMCOMM_RxThreadEntry(
        ( P_11_4_1 ) ) );
      break; }
    case 5: {
      /* void vIMMCOMM_TxThreadEntry(uint32_t ulInput) */
      vCAST_SET_HISTORY_FLAGS ( 11, 5, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      ( vIMMCOMM_TxThreadEntry(
        ( P_11_5_1 ) ) );
      break; }
    case 6: {
      /* uint32_t gulIMMCOMM_SendMessage(uint16_t uwID, uint8_t ubOverrideNumbOjbs, uint32_t ulOS_WaitOption, ip_socket_t eSocketType) */
      vCAST_SET_HISTORY_FLAGS ( 11, 6, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_11_6 = 
      ( gulIMMCOMM_SendMessage(
        ( P_11_6_1 ),
        ( P_11_6_2 ),
        ( P_11_6_3 ),
        ( P_11_6_4 ) ) );
      break; }
    case 7: {
      /* boolean gfIMMCOMM_NetworkReady(void) */
      vCAST_SET_HISTORY_FLAGS ( 11, 7, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_11_7 = 
      ( gfIMMCOMM_NetworkReady( ) );
      break; }
    case 8: {
      /* os_thread_t *gpIMMCOMM_GetRxThreadPointer(void) */
      vCAST_SET_HISTORY_FLAGS ( 11, 8, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_11_8 = 
      ( gpIMMCOMM_GetRxThreadPointer( ) );
      break; }
    case 9: {
      /* os_thread_t *gpIMMCOMM_GetTxThreadPointer(void) */
      vCAST_SET_HISTORY_FLAGS ( 11, 9, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_11_9 = 
      ( gpIMMCOMM_GetTxThreadPointer( ) );
      break; }
    default:
      vectorcast_print_string("ERROR: Internal Tool Error\n");
      break;
  } /* switch */
}

void VCAST_SBF_11( int VC_SUBPROGRAM ) {
  switch( VC_SUBPROGRAM ) {
    case 3: {
      SBF_11_3 = 0;
      break; }
    case 4: {
      SBF_11_4 = 0;
      break; }
    case 5: {
      SBF_11_5 = 0;
      break; }
    case 6: {
      SBF_11_6 = 0;
      break; }
    case 7: {
      SBF_11_7 = 0;
      break; }
    case 8: {
      SBF_11_8 = 0;
      break; }
    case 9: {
      SBF_11_9 = 0;
      break; }
    default:
      break;
  } /* switch */
}
#include "vcast_ti_decls_11.h"
void VCAST_RUN_DATA_IF_11( int VCAST_SUB_INDEX, int VCAST_PARAM_INDEX ) {
  switch ( VCAST_SUB_INDEX ) {
    case 0: /* for global objects */
      switch( VCAST_PARAM_INDEX ) {
        case 17: /* for global object gTestMode_Parameters */
          VCAST_TI_11_198 ( &(gTestMode_Parameters));
          break;
        case 1: /* for global object fIMM_NetworkReady */
          VCAST_TI_8_5 ( &(fIMM_NetworkReady));
          break;
        case 2: /* for global object ubIMM_RxThreadStatck */
          VCAST_TI_11_8 ( ubIMM_RxThreadStatck);
          break;
        case 3: /* for global object IMM_RxThread */
          VCAST_TI_11_59 ( &(IMM_RxThread));
          break;
        case 4: /* for global object ubIMM_TxThreadStatck */
          VCAST_TI_11_11 ( ubIMM_TxThreadStatck);
          break;
        case 5: /* for global object IMM_TxThread */
          VCAST_TI_11_59 ( &(IMM_TxThread));
          break;
        case 6: /* for global object ubIMM_TxQueue */
          VCAST_TI_11_21 ( ubIMM_TxQueue);
          break;
        case 7: /* for global object IMM_TxQueue */
          VCAST_TI_11_72 ( &(IMM_TxQueue));
          break;
        case 8: /* for global object IMM_ServerSocket */
          VCAST_TI_11_135 ( &(IMM_ServerSocket));
          break;
        case 9: /* for global object IMM_UdpSocket */
          VCAST_TI_11_122 ( &(IMM_UdpSocket));
          break;
        case 10: /* for global object immcomm_driver */
          VCAST_TI_9_29 ( &(immcomm_driver));
          break;
        case 11: /* for global object geIP_Status */
          VCAST_TI_11_185 ( &(geIP_Status));
          break;
        case 12: /* for global object geSocket_Status */
          VCAST_TI_11_187 ( &(geSocket_Status));
          break;
        case 13: /* for global object gulLinkUp */
          VCAST_TI_11_4 ( &(gulLinkUp));
          break;
        case 14: /* for global object IMM_PacketBuffer */
          VCAST_TI_11_193 ( &(IMM_PacketBuffer));
          break;
        case 15: /* for global object sNetx_log */
          VCAST_TI_11_1121 ( &(sNetx_log));
          break;
        case 16: /* for global object eRxConnectionState */
          VCAST_TI_11_1120 ( &(eRxConnectionState));
          break;
        default:
          vCAST_TOOL_ERROR = vCAST_true;
          break;
      } /* switch( VCAST_PARAM_INDEX ) */
      break; /* case 0 (global objects) */
    case 1: /* function read_tbu */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_25 ( &(R_11_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function read_tbu */
    case 2: /* function read_tbl */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_25 ( &(R_11_2));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function read_tbl */
    case 10: /* function gulOS_ThreadCreate */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_7 ( &(P_10_1_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_10_1_2));
          break;
        case 3:
          VCAST_TI_11_1123 ( &(P_10_1_3));
          break;
        case 4:
          VCAST_TI_11_4 ( &(P_10_1_4));
          break;
        case 5:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_10_1_5_memsize );
          VCAST_TI_9_11 ( &(P_10_1_5));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 6:
          VCAST_TI_11_4 ( &(P_10_1_6));
          break;
        case 7:
          VCAST_TI_11_4 ( &(P_10_1_7));
          break;
        case 8:
          VCAST_TI_11_4 ( &(P_10_1_8));
          break;
        case 9:
          VCAST_TI_11_4 ( &(P_10_1_9));
          break;
        case 10:
          VCAST_TI_11_4 ( &(P_10_1_10));
          break;
        case 11:
          VCAST_TI_11_4 ( &(R_10_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulOS_ThreadCreate */
    case 11: /* function gulOS_ThreadSleep */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_4 ( &(P_10_2_1));
          break;
        case 2:
          VCAST_TI_11_4 ( &(R_10_2));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulOS_ThreadSleep */
    case 12: /* function gulOS_QueueCreate */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_20 ( &(P_10_3_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_10_3_2));
          break;
        case 3:
          VCAST_TI_11_4 ( &(P_10_3_3));
          break;
        case 4:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_10_3_4_memsize );
          VCAST_TI_9_11 ( &(P_10_3_4));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 5:
          VCAST_TI_11_4 ( &(P_10_3_5));
          break;
        case 6:
          VCAST_TI_11_4 ( &(R_10_3));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulOS_QueueCreate */
    case 13: /* function gulOS_QueueSend */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_20 ( &(P_10_4_1));
          break;
        case 2:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_10_4_2_memsize );
          VCAST_TI_9_11 ( &(P_10_4_2));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 3:
          VCAST_TI_11_4 ( &(P_10_4_3));
          break;
        case 4:
          VCAST_TI_11_4 ( &(R_10_4));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulOS_QueueSend */
    case 14: /* function gulOS_QueueReceive */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_20 ( &(P_10_5_1));
          break;
        case 2:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_10_5_2_memsize );
          VCAST_TI_9_11 ( &(P_10_5_2));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 3:
          VCAST_TI_11_4 ( &(P_10_5_3));
          break;
        case 4:
          VCAST_TI_11_4 ( &(R_10_5));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulOS_QueueReceive */
    case 15: /* function geIP_Initialize */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_185 ( &(R_10_6));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_Initialize */
    case 16: /* function gfIP_CheckLinkUp */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_4 ( &(P_10_7_1));
          break;
        case 2:
          VCAST_TI_8_5 ( &(R_10_7));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfIP_CheckLinkUp */
    case 17: /* function geIP_SocketCreate */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_12 ( &(P_10_8_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_10_8_2));
          break;
        case 3:
          VCAST_TI_11_182 ( &(P_10_8_3));
          break;
        case 4:
          VCAST_TI_11_187 ( &(R_10_8));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_SocketCreate */
    case 18: /* function geIP_SocketListen */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_12 ( &(P_10_9_1));
          break;
        case 2:
          VCAST_TI_11_4 ( &(P_10_9_2));
          break;
        case 3:
          VCAST_TI_11_184 ( &(P_10_9_3));
          break;
        case 4:
          VCAST_TI_11_187 ( &(R_10_9));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_SocketListen */
    case 19: /* function geIP_SocketAccept */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_12 ( &(P_10_10_1));
          break;
        case 2:
          VCAST_TI_11_4 ( &(P_10_10_2));
          break;
        case 3:
          VCAST_TI_11_187 ( &(R_10_10));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_SocketAccept */
    case 20: /* function geIP_MessageSend */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_10_11_1_memsize );
          VCAST_TI_9_11 ( &(P_10_11_1));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 2:
          VCAST_TI_9_13 ( &(P_10_11_2));
          break;
        case 3:
          VCAST_TI_11_4 ( &(P_10_11_3));
          break;
        case 4:
          VCAST_TI_11_4 ( &(P_10_11_4));
          break;
        case 5:
          VCAST_TI_11_192 ( &(P_10_11_5));
          break;
        case 6:
          VCAST_TI_11_4 ( &(P_10_11_6));
          break;
        case 7:
          VCAST_TI_11_4 ( &(P_10_11_7));
          break;
        case 8:
          VCAST_TI_11_188 ( &(R_10_11));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_MessageSend */
    case 21: /* function gszIP_MessageReceive */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_12 ( &(P_10_12_1));
          break;
        case 2:
          VCAST_TI_11_15 ( &(P_10_12_2));
          break;
        case 3:
          VCAST_TI_11_16 ( &(P_10_12_3));
          break;
        case 4:
          VCAST_TI_11_17 ( &(P_10_12_4));
          break;
        case 5:
          VCAST_TI_9_8 ( &(R_10_12));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gszIP_MessageReceive */
    case 22: /* function gpszIP_GetInitializationStatus */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_185 ( &(P_10_13_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(R_10_13));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gpszIP_GetInitializationStatus */
    case 23: /* function gpszIP_GetSocketCreateStatus */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_187 ( &(P_10_14_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(R_10_14));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gpszIP_GetSocketCreateStatus */
    case 24: /* function gpszIP_GetMessageSendStatus */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_188 ( &(P_10_15_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(R_10_15));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gpszIP_GetMessageSendStatus */
    case 25: /* function gpszIP_GetMessageReceiveStatus */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_189 ( &(P_10_16_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(R_10_16));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gpszIP_GetMessageReceiveStatus */
    case 26: /* function geIP_SendLcpEchoRequest */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_190 ( &(R_10_17));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_SendLcpEchoRequest */
    case 27: /* function gfIP_LcpEchoResponseReceived */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(R_10_18));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfIP_LcpEchoResponseReceived */
    case 28: /* function geIP_SocketDelete */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_12 ( &(P_10_19_1));
          break;
        case 2:
          VCAST_TI_11_4 ( &(P_10_19_2));
          break;
        case 3:
          VCAST_TI_11_187 ( &(R_10_19));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_SocketDelete */
    case 29: /* function geIP_Delete */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_186 ( &(R_10_20));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_Delete */
    case 30: /* function gvIP_GetAppPacketPoolInfo */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_23 ( &(P_10_21_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIP_GetAppPacketPoolInfo */
    case 31: /* function gvIP_GetNetxPacketPoolInfo */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_23 ( &(P_10_22_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIP_GetNetxPacketPoolInfo */
    case 32: /* function gvIP_GetFtpPacketPoolInfo */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_23 ( &(P_10_23_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvIP_GetFtpPacketPoolInfo */
    case 33: /* function gulIP_GetSocketInfo */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_12 ( &(P_10_24_1));
          break;
        case 2:
          VCAST_TI_11_24 ( &(P_10_24_2));
          break;
        case 3:
          VCAST_TI_11_4 ( &(R_10_24));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulIP_GetSocketInfo */
    case 34: /* function geIP_UDPSocketCreate */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_13 ( &(P_10_25_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_10_25_2));
          break;
        case 3:
          VCAST_TI_11_187 ( &(R_10_25));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_UDPSocketCreate */
    case 35: /* function geIP_UDPSocketDelete */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_13 ( &(P_10_26_1));
          break;
        case 2:
          VCAST_TI_11_187 ( &(R_10_26));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_UDPSocketDelete */
    case 36: /* function geIP_UDPSocket_Bind */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_13 ( &(P_10_27_1));
          break;
        case 2:
          VCAST_TI_11_4 ( &(P_10_27_2));
          break;
        case 3:
          VCAST_TI_11_4 ( &(P_10_27_3));
          break;
        case 4:
          VCAST_TI_11_187 ( &(R_10_27));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_UDPSocket_Bind */
    case 37: /* function geIP_FtpServerCreate */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_191 ( &(R_10_28));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_FtpServerCreate */
    case 38: /* function geIP_FtpServerDelete */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_191 ( &(R_10_29));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geIP_FtpServerDelete */
    case 39: /* function gulIP_Address */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(P_10_30_1));
          break;
        case 2:
          VCAST_TI_8_5 ( &(P_10_30_2));
          break;
        case 3:
          VCAST_TI_8_5 ( &(P_10_30_3));
          break;
        case 4:
          VCAST_TI_8_5 ( &(P_10_30_4));
          break;
        case 5:
          VCAST_TI_11_4 ( &(R_10_30));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulIP_Address */
    case 40: /* function gvMyPrintf_b */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_10_31_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvMyPrintf_b */
    case 41: /* function gulPrintf1_b */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_10_32_1));
          break;
        case 2:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_10_32_2_memsize );
          VCAST_TI_9_11 ( &(P_10_32_2));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 3:
          VCAST_TI_11_31 ( &(R_10_32));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulPrintf1_b */
    case 42: /* function gulPrintf2_b */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_10_33_1));
          break;
        case 2:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_10_33_2_memsize );
          VCAST_TI_9_11 ( &(P_10_33_2));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 3:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_10_33_3_memsize );
          VCAST_TI_9_11 ( &(P_10_33_3));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 4:
          VCAST_TI_11_31 ( &(R_10_33));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulPrintf2_b */
    case 43: /* function gvPrint_ThreadCreateStatus */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_10_34_1));
          break;
        case 2:
          VCAST_TI_11_4 ( &(P_10_34_2));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvPrint_ThreadCreateStatus */
    case 44: /* function gvPrint_QueueCreateStatus */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_10_35_1));
          break;
        case 2:
          VCAST_TI_11_4 ( &(P_10_35_2));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvPrint_QueueCreateStatus */
    case 46: /* function gfXCP_ReflashInitiated */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(R_10_37));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfXCP_ReflashInitiated */
    case 49: /* function gfINST_ReportQueueDepth */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_1118 ( &(P_10_40_1));
          break;
        case 2:
          VCAST_TI_11_20 ( &(P_10_40_2));
          break;
        case 3:
          VCAST_TI_9_16 ( &(P_10_40_3));
          break;
        case 4:
          VCAST_TI_8_5 ( &(R_10_40));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfINST_ReportQueueDepth */
    case 50: /* function gvINST_LogTime */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_1119 ( &(P_10_41_1));
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gvINST_LogTime */
    case 3: /* function gulIMMCOMM_DriverInitialize */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_11_3_1));
          break;
        case 2:
          VCAST_TI_9_2 ( &(P_11_3_2));
          break;
        case 3:
          VCAST_TI_9_16 ( &(P_11_3_3));
          break;
        case 4:
          VCAST_TI_9_1 ( &(P_11_3_4));
          break;
        case 5:
          VCAST_TI_9_16 ( &(P_11_3_5));
          break;
        case 6:
          VCAST_TI_9_1 ( &(P_11_3_6));
          break;
        case 7:
          VCAST_TI_8_5 ( &(P_11_3_7));
          break;
        case 8:
          VCAST_TI_11_4 ( &(R_11_3));
          break;
        case 9:
          VCAST_TI_SBF_OBJECT( &SBF_11_3 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulIMMCOMM_DriverInitialize */
    case 4: /* function vIMMCOMM_RxThreadEntry */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_4 ( &(P_11_4_1));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_11_4 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMMCOMM_RxThreadEntry */
    case 5: /* function vIMMCOMM_TxThreadEntry */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_4 ( &(P_11_5_1));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_11_5 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function vIMMCOMM_TxThreadEntry */
    case 6: /* function gulIMMCOMM_SendMessage */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_16 ( &(P_11_6_1));
          break;
        case 2:
          VCAST_TI_8_5 ( &(P_11_6_2));
          break;
        case 3:
          VCAST_TI_11_4 ( &(P_11_6_3));
          break;
        case 4:
          VCAST_TI_11_192 ( &(P_11_6_4));
          break;
        case 5:
          VCAST_TI_11_4 ( &(R_11_6));
          break;
        case 6:
          VCAST_TI_SBF_OBJECT( &SBF_11_6 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gulIMMCOMM_SendMessage */
    case 7: /* function gfIMMCOMM_NetworkReady */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_8_5 ( &(R_11_7));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_11_7 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gfIMMCOMM_NetworkReady */
    case 8: /* function gpIMMCOMM_GetRxThreadPointer */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_7 ( &(R_11_8));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_11_8 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gpIMMCOMM_GetRxThreadPointer */
    case 9: /* function gpIMMCOMM_GetTxThreadPointer */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_11_7 ( &(R_11_9));
          break;
        case 2:
          VCAST_TI_SBF_OBJECT( &SBF_11_9 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gpIMMCOMM_GetTxThreadPointer */
    default:
      vCAST_TOOL_ERROR = vCAST_true;
      break;
  } /* switch ( VCAST_SUB_INDEX ) */
}


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_198 ( struct test_mode_param *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_198 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_198 ( struct test_mode_param *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->fWriteBootParameters */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fWriteBootParameters))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->fUpdateSupervisorConfig_PS2 */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fUpdateSupervisorConfig_PS2))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ubEDHighSideTransmitPeriodMs */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubEDHighSideTransmitPeriodMs))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->fCAL_CANapeSaveToFram */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fCAL_CANapeSaveToFram))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->fSend_Enc2_Configuration */
      case 5: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSend_Enc2_Configuration))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->fDoInputRouting */
      case 6: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fDoInputRouting))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->fDoOutputRouting */
      case 7: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fDoOutputRouting))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->fSend_Pot2_Configuration */
      case 8: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSend_Pot2_Configuration))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->fSend_DIG15_Configuration */
      case 9: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSend_DIG15_Configuration))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->fSend_GP_Configuration */
      case 10: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSend_GP_Configuration))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->fSend_Driver_clear_command */
      case 11: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSend_Driver_clear_command))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->fWriteAdcCals */
      case 12: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fWriteAdcCals))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->fWriteAccelCals */
      case 13: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fWriteAccelCals))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->fWriteGyroCals */
      case 14: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fWriteGyroCals))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->rBatteryWidth */
      case 15: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rBatteryWidth))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->rForkLength */
      case 16: { 
        VCAST_TI_8_3 ( ((float *)(&(vcast_param->rForkLength))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->fZeroWheelAngle */
      case 17: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fZeroWheelAngle))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->fTML_RS422_Test */
      case 18: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fTML_RS422_Test))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->fInitFramPowerDownTest */
      case 19: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fInitFramPowerDownTest))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->fRunFramPowerDownTest */
      case 20: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fRunFramPowerDownTest))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->fGetPDTestBytesWritten */
      case 21: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fGetPDTestBytesWritten))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->fInitNorFlashWriteTest */
      case 22: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fInitNorFlashWriteTest))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->fInitNorFlashReadTest */
      case 23: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fInitNorFlashReadTest))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->fCalcTemperature */
      case 24: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fCalcTemperature))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->fSaveFeaturesToFram */
      case 25: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fSaveFeaturesToFram))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->fBypassWaitForPdos */
      case 26: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBypassWaitForPdos))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->fBC_CANapeSaveToFram */
      case 27: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBC_CANapeSaveToFram))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->fBC_CANapeReadData */
      case 28: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fBC_CANapeReadData))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->eSkipDlFlags */
      case 29: { 
        VCAST_TI_11_197 ( ((enum test_skip_dl_type *)(&(vcast_param->eSkipDlFlags))));
        break; /* end case 29*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_198 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_8 ( unsigned char vcast_param[1536U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_8 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_8 ( unsigned char vcast_param[1536U] ) 
{
  {
    int VCAST_TI_11_8_array_index = 0;
    int VCAST_TI_11_8_index = 0;
    int VCAST_TI_11_8_first, VCAST_TI_11_8_last;
    int VCAST_TI_11_8_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_8_local_field = 0;
    int VCAST_TI_11_8_value_printed = 0;
    int VCAST_TI_11_8_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_11_8_first, &VCAST_TI_11_8_last, &VCAST_TI_11_8_more_data);
    VCAST_TI_11_8_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_8_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1536U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_8_upper = 1536U;
      for (VCAST_TI_11_8_array_index=0; VCAST_TI_11_8_array_index< VCAST_TI_11_8_upper; VCAST_TI_11_8_array_index++){
        if ( (VCAST_TI_11_8_index >= VCAST_TI_11_8_first) && ( VCAST_TI_11_8_index <= VCAST_TI_11_8_last)){
          if ( VCAST_TI_11_8_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_11_8_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_11_8_index]));
          VCAST_TI_11_8_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_8_local_field;
        } /* if */
        if (VCAST_TI_11_8_index >= VCAST_TI_11_8_last)
          break;
        VCAST_TI_11_8_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_8_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_8 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_59 ( struct TX_THREAD_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_59 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_59 ( struct TX_THREAD_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->tx_thread_id */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_id))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_run_count */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_run_count))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_stack_ptr */
      case 3: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->tx_thread_stack_ptr))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_stack_start */
      case 4: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->tx_thread_stack_start))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_stack_end */
      case 5: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->tx_thread_stack_end))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_stack_size */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_stack_size))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_time_slice */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_time_slice))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_new_time_slice */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_new_time_slice))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_ready_next */
      case 9: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_thread_ready_next))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_ready_previous */
      case 10: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_thread_ready_previous))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_name */
      case 11: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->tx_thread_name))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_priority */
      case 12: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_priority))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_state */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_state))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_delayed_suspend */
      case 14: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_delayed_suspend))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_suspending */
      case 15: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_suspending))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_preempt_threshold */
      case 16: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_preempt_threshold))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_schedule_hook */
      case 17: { 
        VCAST_TI_11_62 ( ((void (**)(struct TX_THREAD_STRUCT * thread_ptr, ULONG id))(&(vcast_param->tx_thread_schedule_hook))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_entry */
      case 18: { 
        VCAST_TI_11_64 ( ((void (**)(ULONG id))(&(vcast_param->tx_thread_entry))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_entry_parameter */
      case 19: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_entry_parameter))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_timer */
      case 20: { 
        VCAST_TI_11_52 ( ((struct TX_TIMER_INTERNAL_STRUCT *)(&(vcast_param->tx_thread_timer))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_suspend_cleanup */
      case 21: { 
        VCAST_TI_11_66 ( ((void (**)(struct TX_THREAD_STRUCT * thread_ptr))(&(vcast_param->tx_thread_suspend_cleanup))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_suspend_control_block */
      case 22: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->tx_thread_suspend_control_block))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_suspended_next */
      case 23: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_thread_suspended_next))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_suspended_previous */
      case 24: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_thread_suspended_previous))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_suspend_info */
      case 25: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_suspend_info))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_additional_suspend_info */
      case 26: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->tx_thread_additional_suspend_info))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_suspend_option */
      case 27: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_suspend_option))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_suspend_status */
      case 28: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_suspend_status))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_eh_globals */
      case 29: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->tx_thread_eh_globals))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_created_next */
      case 30: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_thread_created_next))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_created_previous */
      case 31: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_thread_created_previous))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->Errno */
      case 32: { 
        VCAST_TI_8_1 ( ((int *)(&(vcast_param->Errno))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_filex_ptr */
      case 33: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->tx_thread_filex_ptr))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_user_priority */
      case 34: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_user_priority))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_user_preempt_threshold */
      case 35: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_user_preempt_threshold))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_inherit_priority */
      case 36: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_inherit_priority))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_owned_mutex_count */
      case 37: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_thread_owned_mutex_count))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_owned_mutex_list */
      case 38: { 
        VCAST_TI_11_67 ( ((struct TX_MUTEX_STRUCT **)(&(vcast_param->tx_thread_owned_mutex_list))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_stack_highest_ptr */
      case 39: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->tx_thread_stack_highest_ptr))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_execution_time_total */
      case 40: { 
        VCAST_TI_8_2 ( ((unsigned long *)(&(vcast_param->tx_thread_execution_time_total))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->tx_thread_execution_time_last_start */
      case 41: { 
        VCAST_TI_8_2 ( ((unsigned long *)(&(vcast_param->tx_thread_execution_time_last_start))));
        break; /* end case 41*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_59 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_11 ( unsigned char vcast_param[3328U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_11 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_11 ( unsigned char vcast_param[3328U] ) 
{
  {
    int VCAST_TI_11_11_array_index = 0;
    int VCAST_TI_11_11_index = 0;
    int VCAST_TI_11_11_first, VCAST_TI_11_11_last;
    int VCAST_TI_11_11_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_11_local_field = 0;
    int VCAST_TI_11_11_value_printed = 0;
    int VCAST_TI_11_11_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_11_11_first, &VCAST_TI_11_11_last, &VCAST_TI_11_11_more_data);
    VCAST_TI_11_11_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_11_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3328U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_11_upper = 3328U;
      for (VCAST_TI_11_11_array_index=0; VCAST_TI_11_11_array_index< VCAST_TI_11_11_upper; VCAST_TI_11_11_array_index++){
        if ( (VCAST_TI_11_11_index >= VCAST_TI_11_11_first) && ( VCAST_TI_11_11_index <= VCAST_TI_11_11_last)){
          if ( VCAST_TI_11_11_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_11_11_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_11_11_index]));
          VCAST_TI_11_11_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_11_local_field;
        } /* if */
        if (VCAST_TI_11_11_index >= VCAST_TI_11_11_last)
          break;
        VCAST_TI_11_11_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_11_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_11 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_21 ( unsigned char vcast_param[(uint32_t)((4U * 1U) * 32U)] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_21 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_21 ( unsigned char vcast_param[(uint32_t)((4U * 1U) * 32U)] ) 
{
  {
    int VCAST_TI_11_21_array_index = 0;
    int VCAST_TI_11_21_index = 0;
    int VCAST_TI_11_21_first, VCAST_TI_11_21_last;
    int VCAST_TI_11_21_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_21_local_field = 0;
    int VCAST_TI_11_21_value_printed = 0;
    int VCAST_TI_11_21_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_11_21_first, &VCAST_TI_11_21_last, &VCAST_TI_11_21_more_data);
    VCAST_TI_11_21_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_21_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(uint32_t)((4U * 1U) * 32U));
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_21_upper = (uint32_t)((4U * 1U) * 32U);
      for (VCAST_TI_11_21_array_index=0; VCAST_TI_11_21_array_index< VCAST_TI_11_21_upper; VCAST_TI_11_21_array_index++){
        if ( (VCAST_TI_11_21_index >= VCAST_TI_11_21_first) && ( VCAST_TI_11_21_index <= VCAST_TI_11_21_last)){
          if ( VCAST_TI_11_21_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_11_21_upper);
          else
            VCAST_TI_8_5 ( &(vcast_param[VCAST_TI_11_21_index]));
          VCAST_TI_11_21_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_21_local_field;
        } /* if */
        if (VCAST_TI_11_21_index >= VCAST_TI_11_21_last)
          break;
        VCAST_TI_11_21_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_21_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_21 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_72 ( struct TX_QUEUE_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_72 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_72 ( struct TX_QUEUE_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->tx_queue_id */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_queue_id))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_name */
      case 2: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->tx_queue_name))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_message_size */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_queue_message_size))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_capacity */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_queue_capacity))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_enqueued */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_queue_enqueued))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_available_storage */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_queue_available_storage))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_start */
      case 7: { 
        VCAST_TI_11_17 ( ((unsigned **)(&(vcast_param->tx_queue_start))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_end */
      case 8: { 
        VCAST_TI_11_17 ( ((unsigned **)(&(vcast_param->tx_queue_end))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_read */
      case 9: { 
        VCAST_TI_11_17 ( ((unsigned **)(&(vcast_param->tx_queue_read))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_write */
      case 10: { 
        VCAST_TI_11_17 ( ((unsigned **)(&(vcast_param->tx_queue_write))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_suspension_list */
      case 11: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_queue_suspension_list))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_suspended_count */
      case 12: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_queue_suspended_count))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_created_next */
      case 13: { 
        VCAST_TI_11_20 ( ((struct TX_QUEUE_STRUCT **)(&(vcast_param->tx_queue_created_next))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->tx_queue_created_previous */
      case 14: { 
        VCAST_TI_11_20 ( ((struct TX_QUEUE_STRUCT **)(&(vcast_param->tx_queue_created_previous))));
        break; /* end case 14*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_72 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_135 ( struct NX_TCP_SOCKET_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_135 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_135 ( struct NX_TCP_SOCKET_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nx_tcp_socket_id */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_id))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_name */
      case 2: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->nx_tcp_socket_name))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_client_type */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_client_type))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_port */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_port))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_mss */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_mss))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_connect_ip */
      case 6: { 
        VCAST_TI_11_75 ( ((struct NXD_ADDRESS_STRUCT *)(&(vcast_param->nx_tcp_socket_connect_ip))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_connect_port */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_connect_port))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_connect_mss */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_connect_mss))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_connect_interface */
      case 9: { 
        VCAST_TI_11_80 ( ((struct NX_INTERFACE_STRUCT **)(&(vcast_param->nx_tcp_socket_connect_interface))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_next_hop_address */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_next_hop_address))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_connect_mss2 */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_connect_mss2))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_tx_slow_start_threshold */
      case 12: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_tx_slow_start_threshold))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_state */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_state))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_tx_sequence */
      case 14: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_tx_sequence))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_rx_sequence */
      case 15: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_rx_sequence))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_rx_sequence_acked */
      case 16: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_rx_sequence_acked))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_delayed_ack_timeout */
      case 17: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_delayed_ack_timeout))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_fin_sequence */
      case 18: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_fin_sequence))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_fin_received */
      case 19: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_fin_received))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_tx_window_advertised */
      case 20: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_tx_window_advertised))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_tx_window_congestion */
      case 21: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_tx_window_congestion))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_tx_outstanding_bytes */
      case 22: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_tx_outstanding_bytes))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_ack_n_packet_counter */
      case 23: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_ack_n_packet_counter))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_duplicated_ack_received */
      case 24: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_duplicated_ack_received))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_need_fast_retransmit */
      case 25: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_need_fast_retransmit))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_rx_window_default */
      case 26: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_rx_window_default))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_rx_window_current */
      case 27: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_rx_window_current))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_rx_window_last_sent */
      case 28: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_rx_window_last_sent))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_packets_sent */
      case 29: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_packets_sent))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_bytes_sent */
      case 30: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_bytes_sent))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_packets_received */
      case 31: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_packets_received))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_bytes_received */
      case 32: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_bytes_received))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_retransmit_packets */
      case 33: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_retransmit_packets))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_checksum_errors */
      case 34: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_checksum_errors))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_ip_ptr */
      case 35: { 
        VCAST_TI_11_81 ( ((struct NX_IP_STRUCT **)(&(vcast_param->nx_tcp_socket_ip_ptr))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_type_of_service */
      case 36: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_type_of_service))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_time_to_live */
      case 37: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_time_to_live))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_fragment_enable */
      case 38: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_fragment_enable))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_receive_queue_count */
      case 39: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_receive_queue_count))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_receive_queue_head */
      case 40: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_tcp_socket_receive_queue_head))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_receive_queue_tail */
      case 41: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_tcp_socket_receive_queue_tail))));
        break; /* end case 41*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_transmit_queue_maximum */
      case 42: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_transmit_queue_maximum))));
        break; /* end case 42*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_transmit_sent_count */
      case 43: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_transmit_sent_count))));
        break; /* end case 43*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_transmit_sent_head */
      case 44: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_tcp_socket_transmit_sent_head))));
        break; /* end case 44*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_transmit_sent_tail */
      case 45: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_tcp_socket_transmit_sent_tail))));
        break; /* end case 45*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_timeout */
      case 46: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_timeout))));
        break; /* end case 46*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_timeout_rate */
      case 47: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_timeout_rate))));
        break; /* end case 47*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_timeout_retries */
      case 48: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_timeout_retries))));
        break; /* end case 48*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_timeout_max_retries */
      case 49: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_timeout_max_retries))));
        break; /* end case 49*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_timeout_shift */
      case 50: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_timeout_shift))));
        break; /* end case 50*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_keepalive_timeout */
      case 51: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_keepalive_timeout))));
        break; /* end case 51*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_keepalive_retries */
      case 52: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_keepalive_retries))));
        break; /* end case 52*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_bound_next */
      case 53: { 
        VCAST_TI_11_12 ( ((struct NX_TCP_SOCKET_STRUCT **)(&(vcast_param->nx_tcp_socket_bound_next))));
        break; /* end case 53*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_bound_previous */
      case 54: { 
        VCAST_TI_11_12 ( ((struct NX_TCP_SOCKET_STRUCT **)(&(vcast_param->nx_tcp_socket_bound_previous))));
        break; /* end case 54*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_bind_in_progress */
      case 55: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_tcp_socket_bind_in_progress))));
        break; /* end case 55*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_receive_suspension_list */
      case 56: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_tcp_socket_receive_suspension_list))));
        break; /* end case 56*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_receive_suspended_count */
      case 57: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_receive_suspended_count))));
        break; /* end case 57*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_transmit_suspension_list */
      case 58: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_tcp_socket_transmit_suspension_list))));
        break; /* end case 58*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_transmit_suspended_count */
      case 59: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_transmit_suspended_count))));
        break; /* end case 59*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_connect_suspended_thread */
      case 60: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_tcp_socket_connect_suspended_thread))));
        break; /* end case 60*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_disconnect_suspended_thread */
      case 61: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_tcp_socket_disconnect_suspended_thread))));
        break; /* end case 61*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_bind_suspension_list */
      case 62: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_tcp_socket_bind_suspension_list))));
        break; /* end case 62*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_bind_suspended_count */
      case 63: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_bind_suspended_count))));
        break; /* end case 63*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_created_next */
      case 64: { 
        VCAST_TI_11_12 ( ((struct NX_TCP_SOCKET_STRUCT **)(&(vcast_param->nx_tcp_socket_created_next))));
        break; /* end case 64*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_created_previous */
      case 65: { 
        VCAST_TI_11_12 ( ((struct NX_TCP_SOCKET_STRUCT **)(&(vcast_param->nx_tcp_socket_created_previous))));
        break; /* end case 65*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_urgent_data_callback */
      case 66: { 
        VCAST_TI_11_128 ( ((void (**)(struct NX_TCP_SOCKET_STRUCT * socket_ptr))(&(vcast_param->nx_tcp_urgent_data_callback))));
        break; /* end case 66*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_disconnect_callback */
      case 67: { 
        VCAST_TI_11_130 ( ((void (**)(struct NX_TCP_SOCKET_STRUCT * socket_ptr))(&(vcast_param->nx_tcp_disconnect_callback))));
        break; /* end case 67*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_receive_callback */
      case 68: { 
        VCAST_TI_11_132 ( ((void (**)(struct NX_TCP_SOCKET_STRUCT * socket_ptr))(&(vcast_param->nx_tcp_receive_callback))));
        break; /* end case 68*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_window_update_notify */
      case 69: { 
        VCAST_TI_11_134 ( ((void (**)(struct NX_TCP_SOCKET_STRUCT * socket_ptr))(&(vcast_param->nx_tcp_socket_window_update_notify))));
        break; /* end case 69*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_reserved_ptr */
      case 70: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->nx_tcp_socket_reserved_ptr))));
        break; /* end case 70*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_transmit_queue_maximum_default */
      case 71: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_transmit_queue_maximum_default))));
        break; /* end case 71*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_socket_keepalive_enabled */
      case 72: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_socket_keepalive_enabled))));
        break; /* end case 72*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_135 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_122 ( struct NX_UDP_SOCKET_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_122 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_122 ( struct NX_UDP_SOCKET_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nx_udp_socket_id */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_id))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_name */
      case 2: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->nx_udp_socket_name))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_port */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_port))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_ip_ptr */
      case 4: { 
        VCAST_TI_11_81 ( ((struct NX_IP_STRUCT **)(&(vcast_param->nx_udp_socket_ip_ptr))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_packets_sent */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_packets_sent))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_bytes_sent */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_bytes_sent))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_packets_received */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_packets_received))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_bytes_received */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_bytes_received))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_invalid_packets */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_invalid_packets))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_packets_dropped */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_packets_dropped))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_checksum_errors */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_checksum_errors))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_type_of_service */
      case 12: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_type_of_service))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_time_to_live */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_time_to_live))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_fragment_enable */
      case 14: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_fragment_enable))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_disable_checksum */
      case 15: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_disable_checksum))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_receive_count */
      case 16: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_receive_count))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_queue_maximum */
      case 17: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_queue_maximum))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_receive_head */
      case 18: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_udp_socket_receive_head))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_receive_tail */
      case 19: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_udp_socket_receive_tail))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_bound_next */
      case 20: { 
        VCAST_TI_11_13 ( ((struct NX_UDP_SOCKET_STRUCT **)(&(vcast_param->nx_udp_socket_bound_next))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_bound_previous */
      case 21: { 
        VCAST_TI_11_13 ( ((struct NX_UDP_SOCKET_STRUCT **)(&(vcast_param->nx_udp_socket_bound_previous))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_bind_in_progress */
      case 22: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_udp_socket_bind_in_progress))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_receive_suspension_list */
      case 23: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_udp_socket_receive_suspension_list))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_receive_suspended_count */
      case 24: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_receive_suspended_count))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_bind_suspension_list */
      case 25: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_udp_socket_bind_suspension_list))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_bind_suspended_count */
      case 26: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_udp_socket_bind_suspended_count))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_created_next */
      case 27: { 
        VCAST_TI_11_13 ( ((struct NX_UDP_SOCKET_STRUCT **)(&(vcast_param->nx_udp_socket_created_next))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_created_previous */
      case 28: { 
        VCAST_TI_11_13 ( ((struct NX_UDP_SOCKET_STRUCT **)(&(vcast_param->nx_udp_socket_created_previous))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_receive_callback */
      case 29: { 
        VCAST_TI_11_121 ( ((void (**)(struct NX_UDP_SOCKET_STRUCT * socket_ptr))(&(vcast_param->nx_udp_receive_callback))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_reserved_ptr */
      case 30: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->nx_udp_socket_reserved_ptr))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->nx_udp_socket_ip_interface */
      case 31: { 
        VCAST_TI_11_80 ( ((struct NX_INTERFACE_STRUCT **)(&(vcast_param->nx_udp_socket_ip_interface))));
        break; /* end case 31*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_122 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_185 ( enum ip_init_status *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_185 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_185 ( enum ip_init_status *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum ip_init_status ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = IP_INIT_SUCCESS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = IP_INIT_ALREADY_ENABLED;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_185 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_187 ( enum ip_socket_status *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_187 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_187 ( enum ip_socket_status *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum ip_socket_status ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = IP_SOCKET_SUCCESS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = IP_SOCKET_NOT_BOUND;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_187 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An integer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_4 ( unsigned *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_4 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_4 ( unsigned *vcast_param ) 
{
  switch (vCAST_COMMAND) {
    case vCAST_PRINT :
      if ( vcast_param == 0)
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_unsigned_integer(vCAST_OUTPUT_FILE, *vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      break;
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL :
    *vcast_param = ( unsigned  ) vCAST_VALUE_UNSIGNED;
    break;
  case vCAST_FIRST_VAL :
    *vcast_param = UINT_MIN;
    break;
  case vCAST_MID_VAL :
    *vcast_param = (UINT_MIN / 2) + (UINT_MAX / 2);
    break;
  case vCAST_LAST_VAL :
    *vcast_param = UINT_MAX;
    break;
  case vCAST_MIN_MINUS_1_VAL :
    *vcast_param = UINT_MIN;
    *vcast_param = *vcast_param - 1;
    break;
  case vCAST_MAX_PLUS_1_VAL :
    *vcast_param = UINT_MAX;
    *vcast_param = *vcast_param + 1;
    break;
  case vCAST_ZERO_VAL :
    *vcast_param = 0;
    break;
  default:
    break;
} /* end switch */
} /* end VCAST_TI_11_4 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_193 ( struct ip_packet_buffer *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_193 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_193 ( struct ip_packet_buffer *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulUnprocessedCharacters */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulUnprocessedCharacters))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ulCharactersInBuffer */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulCharactersInBuffer))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->szBuffer */
      case 3: { 
        VCAST_TI_11_194 ( ((char *)(vcast_param->szBuffer)));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_193 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_1121 ( struct nx_log *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_1121 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_1121 ( struct nx_log *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->AppPoolInfo */
      case 1: { 
        VCAST_TI_11_196 ( ((struct nx_packet_pool_info *)(&(vcast_param->AppPoolInfo))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->NetxPoolInfo */
      case 2: { 
        VCAST_TI_11_196 ( ((struct nx_packet_pool_info *)(&(vcast_param->NetxPoolInfo))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->FtpPoolInfo */
      case 3: { 
        VCAST_TI_11_196 ( ((struct nx_packet_pool_info *)(&(vcast_param->FtpPoolInfo))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->socket_info */
      case 4: { 
        VCAST_TI_11_195 ( ((struct ip_socket_info *)(&(vcast_param->socket_info))));
        break; /* end case 4*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_1121 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_1120 ( enum immcomm_rx_states *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_1120 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_1120 ( enum immcomm_rx_states *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum immcomm_rx_states ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = IMMCOMM_ST_INIT_LINK;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = IMMCOMM_ST_DELETE_LINK;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_1120 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An integer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_25 ( long *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_25 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_25 ( long *vcast_param ) 
{
  switch (vCAST_COMMAND) {
    case vCAST_PRINT :
      if ( vcast_param == 0)
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long(vCAST_OUTPUT_FILE, *vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      break;
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL :
    *vcast_param = ( long  ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL :
    *vcast_param = LONG_MIN;
    break;
  case vCAST_MID_VAL :
    *vcast_param = (LONG_MIN / 2) + (LONG_MAX / 2);
    break;
  case vCAST_LAST_VAL :
    *vcast_param = LONG_MAX;
    break;
  case vCAST_MIN_MINUS_1_VAL :
    *vcast_param = LONG_MIN;
    *vcast_param = *vcast_param - 1;
    break;
  case vCAST_MAX_PLUS_1_VAL :
    *vcast_param = LONG_MAX;
    *vcast_param = *vcast_param + 1;
    break;
  case vCAST_ZERO_VAL :
    *vcast_param = 0;
    break;
  default:
    break;
} /* end switch */
} /* end VCAST_TI_11_25 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_7 ( struct TX_THREAD_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_7 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_7 ( struct TX_THREAD_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_7_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_7_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_7_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_7_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_7_array_size*(sizeof(struct TX_THREAD_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_7_array_size*(sizeof(struct TX_THREAD_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_7_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_7_index = vcast_get_param();
          VCAST_TI_11_59 ( &((*vcast_param)[VCAST_TI_11_7_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_7 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_1123 ( void (**vcast_param)(uint32_t ulEntryInput) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_1123 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_1123 ( void (**vcast_param)(uint32_t ulEntryInput) ) 
{
  void (*vcast_local_ptr)(uint32_t ulEntryInput);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else if ( vcast_local_ptr = gvIMM_SendSROMessage, *vcast_param == vcast_local_ptr ) {
        vectorcast_fprint_integer (vCAST_OUTPUT_FILE, 0);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      else if ( vcast_local_ptr = gvIMM_SendTruckOperationMsg, *vcast_param == vcast_local_ptr ) {
        vectorcast_fprint_integer (vCAST_OUTPUT_FILE, 1);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      else if ( vcast_local_ptr = gvIMM_SendCalibrationMessage, *vcast_param == vcast_local_ptr ) {
        vectorcast_fprint_integer (vCAST_OUTPUT_FILE, 2);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      else if ( vcast_local_ptr = vIMMCOMM_RxThreadEntry, *vcast_param == vcast_local_ptr ) {
        vectorcast_fprint_integer (vCAST_OUTPUT_FILE, 3);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      else if ( vcast_local_ptr = vIMMCOMM_TxThreadEntry, *vcast_param == vcast_local_ptr ) {
        vectorcast_fprint_integer (vCAST_OUTPUT_FILE, 4);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
        case 0:
          vcast_local_ptr = gvIMM_SendSROMessage;
          *vcast_param = vcast_local_ptr;
          break;
        case 1:
          vcast_local_ptr = gvIMM_SendTruckOperationMsg;
          *vcast_param = vcast_local_ptr;
          break;
        case 2:
          vcast_local_ptr = gvIMM_SendCalibrationMessage;
          *vcast_param = vcast_local_ptr;
          break;
        case 3:
          vcast_local_ptr = vIMMCOMM_RxThreadEntry;
          *vcast_param = vcast_local_ptr;
          break;
        case 4:
          vcast_local_ptr = vIMMCOMM_TxThreadEntry;
          *vcast_param = vcast_local_ptr;
          break;
      }
      break;
  }
} /* end VCAST_TI_11_1123 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_20 ( struct TX_QUEUE_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_20 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_20 ( struct TX_QUEUE_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_20_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_20_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_20_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_20_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_20_array_size*(sizeof(struct TX_QUEUE_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_20_array_size*(sizeof(struct TX_QUEUE_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_20_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_20_index = vcast_get_param();
          VCAST_TI_11_72 ( &((*vcast_param)[VCAST_TI_11_20_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_20 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_12 ( struct NX_TCP_SOCKET_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_12 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_12 ( struct NX_TCP_SOCKET_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_12_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_12_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_12_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_12_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_12_array_size*(sizeof(struct NX_TCP_SOCKET_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_12_array_size*(sizeof(struct NX_TCP_SOCKET_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_12_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_12_index = vcast_get_param();
          VCAST_TI_11_135 ( &((*vcast_param)[VCAST_TI_11_12_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_12 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_182 ( void (**vcast_param)(NX_TCP_SOCKET * socket_ptr) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_182 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_182 ( void (**vcast_param)(NX_TCP_SOCKET * socket_ptr) ) 
{
  void (*vcast_local_ptr)(NX_TCP_SOCKET * socket_ptr);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_182 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_184 ( void (**vcast_param)(NX_TCP_SOCKET * socket_ptr, UINT port) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_184 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_184 ( void (**vcast_param)(NX_TCP_SOCKET * socket_ptr, UINT port) ) 
{
  void (*vcast_local_ptr)(NX_TCP_SOCKET * socket_ptr, UINT port);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_184 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_192 ( enum ip_socket *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_192 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_192 ( enum ip_socket *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum ip_socket ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = IP_TCP_SOCKET;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = IP_UDP_SOCKET;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_192 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_188 ( enum ip_tx_status *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_188 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_188 ( enum ip_tx_status *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum ip_tx_status ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = IP_TX_SUCCESS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = IP_TX_LENGTH_INCORRECT;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_188 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_15 ( struct ip_packet_buffer **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_15 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_15 ( struct ip_packet_buffer **vcast_param ) 
{
  {
    int VCAST_TI_11_15_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_15_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_15_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_15_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_15_array_size*(sizeof(struct ip_packet_buffer )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_15_array_size*(sizeof(struct ip_packet_buffer )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_15_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_15_index = vcast_get_param();
          VCAST_TI_11_193 ( &((*vcast_param)[VCAST_TI_11_15_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_15 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_16 ( enum ip_rx_status **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_16 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_16 ( enum ip_rx_status **vcast_param ) 
{
  {
    int VCAST_TI_11_16_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_16_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_16_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_16_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_16_array_size*(sizeof(enum ip_rx_status )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_16_array_size*(sizeof(enum ip_rx_status )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_16_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_16_index = vcast_get_param();
          VCAST_TI_11_189 ( &((*vcast_param)[VCAST_TI_11_16_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_16 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_17 ( unsigned **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_17 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_17 ( unsigned **vcast_param ) 
{
  {
    int VCAST_TI_11_17_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_17_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_17_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_17_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_17_array_size*(sizeof(unsigned )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_17_array_size*(sizeof(unsigned )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_17_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_17_index = vcast_get_param();
          VCAST_TI_11_4 ( &((*vcast_param)[VCAST_TI_11_17_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_17 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_189 ( enum ip_rx_status *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_189 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_189 ( enum ip_rx_status *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum ip_rx_status ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = IP_RX_SUCCESS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = IP_RX_BUFFER_TOO_SMALL;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_189 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_190 ( enum ppp_echo_status *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_190 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_190 ( enum ppp_echo_status *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum ppp_echo_status ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = PPP_ECHO_SUCCESS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = PPP_ECHO_NOT_ESTABLISHED;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_190 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_186 ( enum ip_delete_status *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_186 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_186 ( enum ip_delete_status *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum ip_delete_status ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = IP_DEL_SUCCESS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = IP_DEL_CALLER_ERROR;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_186 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_23 ( struct nx_packet_pool_info **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_23 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_23 ( struct nx_packet_pool_info **vcast_param ) 
{
  {
    int VCAST_TI_11_23_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_23_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_23_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_23_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_23_array_size*(sizeof(struct nx_packet_pool_info )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_23_array_size*(sizeof(struct nx_packet_pool_info )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_23_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_23_index = vcast_get_param();
          VCAST_TI_11_196 ( &((*vcast_param)[VCAST_TI_11_23_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_23 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_24 ( struct ip_socket_info **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_24 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_24 ( struct ip_socket_info **vcast_param ) 
{
  {
    int VCAST_TI_11_24_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_24_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_24_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_24_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_24_array_size*(sizeof(struct ip_socket_info )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_24_array_size*(sizeof(struct ip_socket_info )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_24_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_24_index = vcast_get_param();
          VCAST_TI_11_195 ( &((*vcast_param)[VCAST_TI_11_24_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_24 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_13 ( struct NX_UDP_SOCKET_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_13 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_13 ( struct NX_UDP_SOCKET_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_13_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_13_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_13_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_13_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_13_array_size*(sizeof(struct NX_UDP_SOCKET_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_13_array_size*(sizeof(struct NX_UDP_SOCKET_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_13_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_13_index = vcast_get_param();
          VCAST_TI_11_122 ( &((*vcast_param)[VCAST_TI_11_13_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_13 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_191 ( enum ftp_status *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_191 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_191 ( enum ftp_status *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum ftp_status ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = FTP_SUCCESS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = FTP_CALLER_ERROR;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_191 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An integer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_31 ( signed int *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_31 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_31 ( signed int *vcast_param ) 
{
  switch (vCAST_COMMAND) {
    case vCAST_PRINT :
      if ( vcast_param == 0)
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_integer(vCAST_OUTPUT_FILE, *vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      break;
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL :
    *vcast_param = ( signed int  ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL :
    *vcast_param = INT_MIN;
    break;
  case vCAST_MID_VAL :
    *vcast_param = (INT_MIN / 2) + (INT_MAX / 2);
    break;
  case vCAST_LAST_VAL :
    *vcast_param = INT_MAX;
    break;
  case vCAST_MIN_MINUS_1_VAL :
    *vcast_param = INT_MIN;
    *vcast_param = *vcast_param - 1;
    break;
  case vCAST_MAX_PLUS_1_VAL :
    *vcast_param = INT_MAX;
    *vcast_param = *vcast_param + 1;
    break;
  case vCAST_ZERO_VAL :
    *vcast_param = 0;
    break;
  default:
    break;
} /* end switch */
} /* end VCAST_TI_11_31 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_1118 ( enum inst_queue_id *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_1118 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_1118 ( enum inst_queue_id *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum inst_queue_id ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = INST_QUEUE_DEBUG_TX;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = INST_QUEUE_CO_EV;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_1118 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_1119 ( enum inst_start_sequence *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_1119 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_1119 ( enum inst_start_sequence *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum inst_start_sequence ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = INST_LED_OFF;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = INST_NUM_START_SEQ;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_1119 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_197 ( enum test_skip_dl_type *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_197 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_197 ( enum test_skip_dl_type *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum test_skip_dl_type ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = TEST_SKIP_DL_NO_ACTION;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = TEST_SKIP_DL_CLEAR;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_11_197 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_62 ( void (**vcast_param)(struct TX_THREAD_STRUCT * thread_ptr, ULONG id) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_62 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_62 ( void (**vcast_param)(struct TX_THREAD_STRUCT * thread_ptr, ULONG id) ) 
{
  void (*vcast_local_ptr)(struct TX_THREAD_STRUCT * thread_ptr, ULONG id);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_62 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_64 ( void (**vcast_param)(ULONG id) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_64 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_64 ( void (**vcast_param)(ULONG id) ) 
{
  void (*vcast_local_ptr)(ULONG id);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_64 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_52 ( struct TX_TIMER_INTERNAL_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_52 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_52 ( struct TX_TIMER_INTERNAL_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->tx_timer_internal_remaining_ticks */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_timer_internal_remaining_ticks))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_internal_re_initialize_ticks */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_timer_internal_re_initialize_ticks))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_internal_timeout_function */
      case 3: { 
        VCAST_TI_11_56 ( ((void (**)(ULONG id))(&(vcast_param->tx_timer_internal_timeout_function))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_internal_timeout_param */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_timer_internal_timeout_param))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_internal_active_next */
      case 5: { 
        VCAST_TI_11_54 ( ((struct TX_TIMER_INTERNAL_STRUCT **)(&(vcast_param->tx_timer_internal_active_next))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_internal_active_previous */
      case 6: { 
        VCAST_TI_11_54 ( ((struct TX_TIMER_INTERNAL_STRUCT **)(&(vcast_param->tx_timer_internal_active_previous))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_internal_list_head */
      case 7: { 
        VCAST_TI_11_53 ( ((struct TX_TIMER_INTERNAL_STRUCT ***)(&(vcast_param->tx_timer_internal_list_head))));
        break; /* end case 7*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_52 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_66 ( void (**vcast_param)(struct TX_THREAD_STRUCT * thread_ptr) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_66 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_66 ( void (**vcast_param)(struct TX_THREAD_STRUCT * thread_ptr) ) 
{
  void (*vcast_local_ptr)(struct TX_THREAD_STRUCT * thread_ptr);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_66 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_67 ( struct TX_MUTEX_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_67 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_67 ( struct TX_MUTEX_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_67_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_67_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_67_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_67_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_67_array_size*(sizeof(struct TX_MUTEX_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_67_array_size*(sizeof(struct TX_MUTEX_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_67_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_67_index = vcast_get_param();
          VCAST_TI_11_69 ( &((*vcast_param)[VCAST_TI_11_67_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_67 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_75 ( struct NXD_ADDRESS_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_75 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_75 ( struct NXD_ADDRESS_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nxd_ip_version */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nxd_ip_version))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ip_address */
      case 2: { 
#if ((defined(VCAST_NO_TYPE_SUPPORT))||(defined(VCAST_NO_UNION_SUPPORT)))
        /* User code: type is not supported */
        vcast_not_supported();
#else /*((defined(VCAST_NO_TYPE_SUPPORT))||(defined(VCAST_NO_UNION_SUPPORT)))*/
        {
#ifndef VCAST_VXWORKS
#ifndef VCAST_NO_SETJMP
          int VCAST_TI_11_74_jmpval;
          VCAST_TI_11_74_jmpval = setjmp ( VCAST_env );
          vcast_is_in_union = vCAST_false;
          if ( VCAST_TI_11_74_jmpval == 0 ) {
            vcast_is_in_union = vCAST_true;
#endif /* VCAST_VXWORKS */
#endif /* VCAST_NO_SETJMP */
            switch ( vcast_get_param () ) { /* Choose field member */
              /* Setting member variable vcast_param->nxd_ip_address.v4 */
              case 1: { 
                VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nxd_ip_address.v4))));
                break; /* end case 1*/
              } /* end case */
              default:
                vCAST_TOOL_ERROR = vCAST_true;
            } /* end switch */ 
#ifndef VCAST_VXWORKS
#ifndef VCAST_NO_SETJMP
          } else if ( vCAST_COMMAND == vCAST_PRINT )
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"invalid address\n");
#endif /* VCAST_VXWORKS */
#endif /* VCAST_NO_SETJMP */
        }
#endif /*((defined(VCAST_NO_TYPE_SUPPORT))||(defined(VCAST_NO_UNION_SUPPORT)))*/

        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_75 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_80 ( struct NX_INTERFACE_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_80 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_80 ( struct NX_INTERFACE_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_80_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_80_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_80_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_80_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_80_array_size*(sizeof(struct NX_INTERFACE_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_80_array_size*(sizeof(struct NX_INTERFACE_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_80_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_80_index = vcast_get_param();
          VCAST_TI_11_92 ( &((*vcast_param)[VCAST_TI_11_80_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_80 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_81 ( struct NX_IP_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_81 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_81 ( struct NX_IP_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_81_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_81_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_81_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_81_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_81_array_size*(sizeof(struct NX_IP_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_81_array_size*(sizeof(struct NX_IP_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_81_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_81_index = vcast_get_param();
          VCAST_TI_11_91 ( &((*vcast_param)[VCAST_TI_11_81_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_81 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_82 ( struct NX_PACKET_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_82 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_82 ( struct NX_PACKET_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_82_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_82_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_82_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_82_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_82_array_size*(sizeof(struct NX_PACKET_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_82_array_size*(sizeof(struct NX_PACKET_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_82_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_82_index = vcast_get_param();
          VCAST_TI_11_76 ( &((*vcast_param)[VCAST_TI_11_82_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_82 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_128 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_128 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_128 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) 
{
  void (*vcast_local_ptr)(struct NX_TCP_SOCKET_STRUCT * socket_ptr);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_128 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_130 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_130 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_130 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) 
{
  void (*vcast_local_ptr)(struct NX_TCP_SOCKET_STRUCT * socket_ptr);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_130 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_132 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_132 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_132 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) 
{
  void (*vcast_local_ptr)(struct NX_TCP_SOCKET_STRUCT * socket_ptr);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_132 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_134 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_134 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_134 ( void (**vcast_param)(struct NX_TCP_SOCKET_STRUCT * socket_ptr) ) 
{
  void (*vcast_local_ptr)(struct NX_TCP_SOCKET_STRUCT * socket_ptr);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_134 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_121 ( void (**vcast_param)(struct NX_UDP_SOCKET_STRUCT * socket_ptr) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_121 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_121 ( void (**vcast_param)(struct NX_UDP_SOCKET_STRUCT * socket_ptr) ) 
{
  void (*vcast_local_ptr)(struct NX_UDP_SOCKET_STRUCT * socket_ptr);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_121 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_194 ( char vcast_param[3U * (256U + (((16U + 20U) + 20U) + 0U))] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_194 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_194 ( char vcast_param[3U * (256U + (((16U + 20U) + 20U) + 0U))] ) 
{
  {
    int VCAST_TI_11_194_array_index = 0;
    int VCAST_TI_11_194_index = 0;
    int VCAST_TI_11_194_first, VCAST_TI_11_194_last;
    int VCAST_TI_11_194_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_194_local_field = 0;
    int VCAST_TI_11_194_value_printed = 0;
    int VCAST_TI_11_194_is_string = (VCAST_FIND_INDEX()==-1);


    vcast_get_range_value (&VCAST_TI_11_194_first, &VCAST_TI_11_194_last, &VCAST_TI_11_194_more_data);
    VCAST_TI_11_194_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_194_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U * (256U + (((16U + 20U) + 20U) + 0U)));
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_194_upper = 3U * (256U + (((16U + 20U) + 20U) + 0U));
      for (VCAST_TI_11_194_array_index=0; VCAST_TI_11_194_array_index< VCAST_TI_11_194_upper; VCAST_TI_11_194_array_index++){
        if ( (VCAST_TI_11_194_index >= VCAST_TI_11_194_first) && ( VCAST_TI_11_194_index <= VCAST_TI_11_194_last)){
          if ( VCAST_TI_11_194_is_string )
            VCAST_TI_STRING ( (char**)&vcast_param, sizeof ( vcast_param ), 1,VCAST_TI_11_194_upper);
          else
            VCAST_TI_8_4 ( &(vcast_param[VCAST_TI_11_194_index]));
          VCAST_TI_11_194_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_194_local_field;
        } /* if */
        if (VCAST_TI_11_194_index >= VCAST_TI_11_194_last)
          break;
        VCAST_TI_11_194_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_194_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_194 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_196 ( struct nx_packet_pool_info *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_196 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_196 ( struct nx_packet_pool_info *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulTotalPackets */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTotalPackets))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubPeakUsage */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubPeakUsage))));
        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_196 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_195 ( struct ip_socket_info *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_195 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_195 ( struct ip_socket_info *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->ulPackets_sent */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPackets_sent))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ulBytes_sent */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulBytes_sent))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->ulPackets_received */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPackets_received))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->ulBytes_received */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulBytes_received))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->ulRetransmit_packets */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulRetransmit_packets))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->ulPackets_queued */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulPackets_queued))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->ulChecksum_errors */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulChecksum_errors))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->ulSocket_state */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulSocket_state))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->ulTransmit_queue_depth */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTransmit_queue_depth))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->ulTransmit_window */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulTransmit_window))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->ulReceive_window */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->ulReceive_window))));
        break; /* end case 11*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_195 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_56 ( void (**vcast_param)(ULONG id) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_56 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_56 ( void (**vcast_param)(ULONG id) ) 
{
  void (*vcast_local_ptr)(ULONG id);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_56 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_54 ( struct TX_TIMER_INTERNAL_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_54 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_54 ( struct TX_TIMER_INTERNAL_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_54_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_54_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_54_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_54_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_54_array_size*(sizeof(struct TX_TIMER_INTERNAL_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_54_array_size*(sizeof(struct TX_TIMER_INTERNAL_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_54_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_54_index = vcast_get_param();
          VCAST_TI_11_52 ( &((*vcast_param)[VCAST_TI_11_54_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_54 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_53 ( struct TX_TIMER_INTERNAL_STRUCT ***vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_53 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_53 ( struct TX_TIMER_INTERNAL_STRUCT ***vcast_param ) 
{
  {
    int VCAST_TI_11_53_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_53_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_53_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_53_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_53_array_size*(sizeof(struct TX_TIMER_INTERNAL_STRUCT *)));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_53_array_size*(sizeof(struct TX_TIMER_INTERNAL_STRUCT *)));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_53_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_53_index = vcast_get_param();
          VCAST_TI_11_54 ( &((*vcast_param)[VCAST_TI_11_53_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_53 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_69 ( struct TX_MUTEX_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_69 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_69 ( struct TX_MUTEX_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->tx_mutex_id */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_mutex_id))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_name */
      case 2: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->tx_mutex_name))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_ownership_count */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_mutex_ownership_count))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_owner */
      case 4: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_mutex_owner))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_inherit */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_mutex_inherit))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_original_priority */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_mutex_original_priority))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_suspension_list */
      case 7: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_mutex_suspension_list))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_suspended_count */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_mutex_suspended_count))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_created_next */
      case 9: { 
        VCAST_TI_11_67 ( ((struct TX_MUTEX_STRUCT **)(&(vcast_param->tx_mutex_created_next))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_created_previous */
      case 10: { 
        VCAST_TI_11_67 ( ((struct TX_MUTEX_STRUCT **)(&(vcast_param->tx_mutex_created_previous))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_highest_priority_waiting */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_mutex_highest_priority_waiting))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_owned_next */
      case 12: { 
        VCAST_TI_11_67 ( ((struct TX_MUTEX_STRUCT **)(&(vcast_param->tx_mutex_owned_next))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->tx_mutex_owned_previous */
      case 13: { 
        VCAST_TI_11_67 ( ((struct TX_MUTEX_STRUCT **)(&(vcast_param->tx_mutex_owned_previous))));
        break; /* end case 13*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_69 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_92 ( struct NX_INTERFACE_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_92 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_92 ( struct NX_INTERFACE_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nx_interface_name */
      case 1: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->nx_interface_name))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_valid */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->nx_interface_valid))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_address_mapping_needed */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->nx_interface_address_mapping_needed))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_link_up */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->nx_interface_link_up))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_index */
      case 5: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->nx_interface_index))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_ip_instance */
      case 6: { 
        VCAST_TI_11_81 ( ((struct NX_IP_STRUCT **)(&(vcast_param->nx_interface_ip_instance))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_physical_address_msw */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_interface_physical_address_msw))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_physical_address_lsw */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_interface_physical_address_lsw))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_ip_address */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_interface_ip_address))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_ip_network_mask */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_interface_ip_network_mask))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_ip_network */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_interface_ip_network))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_interface_ipv6_address_list_head */
      case 12: { 
        VCAST_TI_11_176 ( ((struct NXD_IPV6_ADDRESS_STRUCT **)(&(vcast_param->nxd_interface_ipv6_address_list_head))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_ip_mtu_size */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_interface_ip_mtu_size))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ipv6_rtr_solicitation_max */
      case 14: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ipv6_rtr_solicitation_max))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ipv6_rtr_solicitation_count */
      case 15: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ipv6_rtr_solicitation_count))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ipv6_rtr_solicitation_interval */
      case 16: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ipv6_rtr_solicitation_interval))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ipv6_rtr_solicitation_timer */
      case 17: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ipv6_rtr_solicitation_timer))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_additional_link_info */
      case 18: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->nx_interface_additional_link_info))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->nx_interface_link_driver_entry */
      case 19: { 
        VCAST_TI_11_180 ( ((void (**)(struct NX_IP_DRIVER_STRUCT *))(&(vcast_param->nx_interface_link_driver_entry))));
        break; /* end case 19*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_92 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_91 ( struct NX_IP_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_91 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_91 ( struct NX_IP_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nx_ip_id */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_id))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_name */
      case 2: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->nx_ip_name))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_gateway_address */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_gateway_address))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_gateway_interface */
      case 4: { 
        VCAST_TI_11_80 ( ((struct NX_INTERFACE_STRUCT **)(&(vcast_param->nx_ip_gateway_interface))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_total_packet_send_requests */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_total_packet_send_requests))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_total_packets_sent */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_total_packets_sent))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_total_bytes_sent */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_total_bytes_sent))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_total_packets_received */
      case 8: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_total_packets_received))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_total_packets_delivered */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_total_packets_delivered))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_total_bytes_received */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_total_bytes_received))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_packets_forwarded */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_packets_forwarded))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_packets_reassembled */
      case 12: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_packets_reassembled))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_reassembly_failures */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_reassembly_failures))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_invalid_packets */
      case 14: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_invalid_packets))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_invalid_transmit_packets */
      case 15: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_invalid_transmit_packets))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_invalid_receive_address */
      case 16: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_invalid_receive_address))));
        break; /* end case 16*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_unknown_protocols_received */
      case 17: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_unknown_protocols_received))));
        break; /* end case 17*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_transmit_resource_errors */
      case 18: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_transmit_resource_errors))));
        break; /* end case 18*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_transmit_no_route_errors */
      case 19: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_transmit_no_route_errors))));
        break; /* end case 19*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_receive_packets_dropped */
      case 20: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_receive_packets_dropped))));
        break; /* end case 20*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_receive_checksum_errors */
      case 21: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_receive_checksum_errors))));
        break; /* end case 21*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_send_packets_dropped */
      case 22: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_send_packets_dropped))));
        break; /* end case 22*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_total_fragment_requests */
      case 23: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_total_fragment_requests))));
        break; /* end case 23*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_successful_fragment_requests */
      case 24: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_successful_fragment_requests))));
        break; /* end case 24*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_fragment_failures */
      case 25: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_fragment_failures))));
        break; /* end case 25*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_total_fragments_sent */
      case 26: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_total_fragments_sent))));
        break; /* end case 26*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_total_fragments_received */
      case 27: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_total_fragments_received))));
        break; /* end case 27*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_requests_sent */
      case 28: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_requests_sent))));
        break; /* end case 28*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_requests_received */
      case 29: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_requests_received))));
        break; /* end case 29*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_responses_sent */
      case 30: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_responses_sent))));
        break; /* end case 30*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_responses_received */
      case 31: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_responses_received))));
        break; /* end case 31*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_aged_entries */
      case 32: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_aged_entries))));
        break; /* end case 32*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_invalid_messages */
      case 33: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_invalid_messages))));
        break; /* end case 33*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_static_entries */
      case 34: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_static_entries))));
        break; /* end case 34*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_packets_sent */
      case 35: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_packets_sent))));
        break; /* end case 35*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_bytes_sent */
      case 36: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_bytes_sent))));
        break; /* end case 36*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_packets_received */
      case 37: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_packets_received))));
        break; /* end case 37*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_bytes_received */
      case 38: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_bytes_received))));
        break; /* end case 38*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_invalid_packets */
      case 39: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_invalid_packets))));
        break; /* end case 39*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_no_port_for_delivery */
      case 40: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_no_port_for_delivery))));
        break; /* end case 40*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_receive_packets_dropped */
      case 41: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_receive_packets_dropped))));
        break; /* end case 41*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_checksum_errors */
      case 42: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_checksum_errors))));
        break; /* end case 42*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_packets_sent */
      case 43: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_packets_sent))));
        break; /* end case 43*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_bytes_sent */
      case 44: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_bytes_sent))));
        break; /* end case 44*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_packets_received */
      case 45: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_packets_received))));
        break; /* end case 45*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_bytes_received */
      case 46: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_bytes_received))));
        break; /* end case 46*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_invalid_packets */
      case 47: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_invalid_packets))));
        break; /* end case 47*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_receive_packets_dropped */
      case 48: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_receive_packets_dropped))));
        break; /* end case 48*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_checksum_errors */
      case 49: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_checksum_errors))));
        break; /* end case 49*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_connections */
      case 50: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_connections))));
        break; /* end case 50*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_passive_connections */
      case 51: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_passive_connections))));
        break; /* end case 51*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_active_connections */
      case 52: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_active_connections))));
        break; /* end case 52*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_disconnections */
      case 53: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_disconnections))));
        break; /* end case 53*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_connections_dropped */
      case 54: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_connections_dropped))));
        break; /* end case 54*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_retransmit_packets */
      case 55: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_retransmit_packets))));
        break; /* end case 55*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_resets_received */
      case 56: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_resets_received))));
        break; /* end case 56*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_resets_sent */
      case 57: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_resets_sent))));
        break; /* end case 57*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_total_messages_received */
      case 58: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_icmp_total_messages_received))));
        break; /* end case 58*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_checksum_errors */
      case 59: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_icmp_checksum_errors))));
        break; /* end case 59*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_invalid_packets */
      case 60: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_icmp_invalid_packets))));
        break; /* end case 60*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_unhandled_messages */
      case 61: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_icmp_unhandled_messages))));
        break; /* end case 61*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_pings_sent */
      case 62: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_pings_sent))));
        break; /* end case 62*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_ping_timeouts */
      case 63: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_ping_timeouts))));
        break; /* end case 63*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_ping_threads_suspended */
      case 64: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_ping_threads_suspended))));
        break; /* end case 64*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_ping_responses_received */
      case 65: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_ping_responses_received))));
        break; /* end case 65*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_pings_received */
      case 66: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_pings_received))));
        break; /* end case 66*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_pings_responded_to */
      case 67: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_pings_responded_to))));
        break; /* end case 67*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_invalid_packets */
      case 68: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_igmp_invalid_packets))));
        break; /* end case 68*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_reports_sent */
      case 69: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_igmp_reports_sent))));
        break; /* end case 69*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_queries_received */
      case 70: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_igmp_queries_received))));
        break; /* end case 70*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_checksum_errors */
      case 71: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_igmp_checksum_errors))));
        break; /* end case 71*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_groups_joined */
      case 72: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_igmp_groups_joined))));
        break; /* end case 72*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_router_version */
      case 73: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_igmp_router_version))));
        break; /* end case 73*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_rarp_requests_sent */
      case 74: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_rarp_requests_sent))));
        break; /* end case 74*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_rarp_responses_received */
      case 75: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_rarp_responses_received))));
        break; /* end case 75*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_rarp_invalid_messages */
      case 76: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_rarp_invalid_messages))));
        break; /* end case 76*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_forward_packet_process */
      case 77: { 
        VCAST_TI_11_84 ( ((void (**)(struct NX_IP_STRUCT *, NX_PACKET *))(&(vcast_param->nx_ip_forward_packet_process))));
        break; /* end case 77*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_packet_id */
      case 78: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_packet_id))));
        break; /* end case 78*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_default_packet_pool */
      case 79: { 
        VCAST_TI_11_78 ( ((struct NX_PACKET_POOL_STRUCT **)(&(vcast_param->nx_ip_default_packet_pool))));
        break; /* end case 79*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_protection */
      case 80: { 
        VCAST_TI_11_69 ( ((struct TX_MUTEX_STRUCT *)(&(vcast_param->nx_ip_protection))));
        break; /* end case 80*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_initialize_done */
      case 81: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_initialize_done))));
        break; /* end case 81*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_driver_deferred_packet_head */
      case 82: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_driver_deferred_packet_head))));
        break; /* end case 82*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_driver_deferred_packet_tail */
      case 83: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_driver_deferred_packet_tail))));
        break; /* end case 83*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_driver_deferred_packet_handler */
      case 84: { 
        VCAST_TI_11_86 ( ((void (**)(struct NX_IP_STRUCT *, NX_PACKET *))(&(vcast_param->nx_ip_driver_deferred_packet_handler))));
        break; /* end case 84*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_deferred_received_packet_head */
      case 85: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_deferred_received_packet_head))));
        break; /* end case 85*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_deferred_received_packet_tail */
      case 86: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_deferred_received_packet_tail))));
        break; /* end case 86*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_raw_ip_processing */
      case 87: { 
        VCAST_TI_11_88 ( ((UINT (**)(struct NX_IP_STRUCT *, ULONG, NX_PACKET *))(&(vcast_param->nx_ip_raw_ip_processing))));
        break; /* end case 87*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_raw_received_packet_head */
      case 88: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_raw_received_packet_head))));
        break; /* end case 88*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_raw_received_packet_tail */
      case 89: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_raw_received_packet_tail))));
        break; /* end case 89*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_raw_received_packet_count */
      case 90: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_raw_received_packet_count))));
        break; /* end case 90*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_raw_received_packet_max */
      case 91: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_raw_received_packet_max))));
        break; /* end case 91*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_raw_packet_suspension_list */
      case 92: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_ip_raw_packet_suspension_list))));
        break; /* end case 92*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_raw_packet_suspended_count */
      case 93: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_raw_packet_suspended_count))));
        break; /* end case 93*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_thread */
      case 94: { 
        VCAST_TI_11_59 ( ((struct TX_THREAD_STRUCT *)(&(vcast_param->nx_ip_thread))));
        break; /* end case 94*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_events */
      case 95: { 
        VCAST_TI_11_70 ( ((struct TX_EVENT_FLAGS_GROUP_STRUCT *)(&(vcast_param->nx_ip_events))));
        break; /* end case 95*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_periodic_timer */
      case 96: { 
        VCAST_TI_11_57 ( ((struct TX_TIMER_STRUCT *)(&(vcast_param->nx_ip_periodic_timer))));
        break; /* end case 96*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_fragment_processing */
      case 97: { 
        VCAST_TI_11_95 ( ((void (**)(struct NX_IP_DRIVER_STRUCT *))(&(vcast_param->nx_ip_fragment_processing))));
        break; /* end case 97*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_fragment_assembly */
      case 98: { 
        VCAST_TI_11_97 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_fragment_assembly))));
        break; /* end case 98*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_fragment_timeout_check */
      case 99: { 
        VCAST_TI_11_99 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_fragment_timeout_check))));
        break; /* end case 99*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_timeout_fragment */
      case 100: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_timeout_fragment))));
        break; /* end case 100*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_received_fragment_head */
      case 101: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_received_fragment_head))));
        break; /* end case 101*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_received_fragment_tail */
      case 102: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_received_fragment_tail))));
        break; /* end case 102*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_fragment_assembly_head */
      case 103: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_fragment_assembly_head))));
        break; /* end case 103*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_fragment_assembly_tail */
      case 104: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_fragment_assembly_tail))));
        break; /* end case 104*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_address_change_notify */
      case 105: { 
        VCAST_TI_11_101 ( ((void (**)(struct NX_IP_STRUCT *, void *))(&(vcast_param->nx_ip_address_change_notify))));
        break; /* end case 105*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_address_change_notify_additional_info */
      case 106: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->nx_ip_address_change_notify_additional_info))));
        break; /* end case 106*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_join_list */
      case 107: { 
        VCAST_TI_11_102 ( ((unsigned *)(vcast_param->nx_ip_igmp_join_list)));
        break; /* end case 107*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_join_interface_list */
      case 108: { 
        VCAST_TI_11_103 ( ((struct NX_INTERFACE_STRUCT **)(vcast_param->nx_ip_igmp_join_interface_list)));
        break; /* end case 108*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_join_count */
      case 109: { 
        VCAST_TI_11_102 ( ((unsigned *)(vcast_param->nx_ip_igmp_join_count)));
        break; /* end case 109*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_update_time */
      case 110: { 
        VCAST_TI_11_102 ( ((unsigned *)(vcast_param->nx_ip_igmp_update_time)));
        break; /* end case 110*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_group_loopback_enable */
      case 111: { 
        VCAST_TI_11_102 ( ((unsigned *)(vcast_param->nx_ip_igmp_group_loopback_enable)));
        break; /* end case 111*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_global_loopback_enable */
      case 112: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_igmp_global_loopback_enable))));
        break; /* end case 112*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_packet_receive */
      case 113: { 
        VCAST_TI_11_108 ( ((void (**)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *))(&(vcast_param->nx_ip_igmp_packet_receive))));
        break; /* end case 113*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_periodic_processing */
      case 114: { 
        VCAST_TI_11_110 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_igmp_periodic_processing))));
        break; /* end case 114*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_queue_process */
      case 115: { 
        VCAST_TI_11_112 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_igmp_queue_process))));
        break; /* end case 115*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_igmp_queue_head */
      case 116: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_igmp_queue_head))));
        break; /* end case 116*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_sequence */
      case 117: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_icmp_sequence))));
        break; /* end case 117*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_packet_receive */
      case 118: { 
        VCAST_TI_11_114 ( ((void (**)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *))(&(vcast_param->nx_ip_icmp_packet_receive))));
        break; /* end case 118*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_queue_process */
      case 119: { 
        VCAST_TI_11_116 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_icmp_queue_process))));
        break; /* end case 119*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmpv4_packet_process */
      case 120: { 
        VCAST_TI_11_118 ( ((void (**)(struct NX_IP_STRUCT *, NX_PACKET *))(&(vcast_param->nx_ip_icmpv4_packet_process))));
        break; /* end case 120*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_queue_head */
      case 121: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_icmp_queue_head))));
        break; /* end case 121*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_ping_suspension_list */
      case 122: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_ip_icmp_ping_suspension_list))));
        break; /* end case 122*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_icmp_ping_suspended_count */
      case 123: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_icmp_ping_suspended_count))));
        break; /* end case 123*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_port_table */
      case 124: { 
        VCAST_TI_11_123 ( ((struct NX_UDP_SOCKET_STRUCT **)(vcast_param->nx_ip_udp_port_table)));
        break; /* end case 124*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_created_sockets_ptr */
      case 125: { 
        VCAST_TI_11_13 ( ((struct NX_UDP_SOCKET_STRUCT **)(&(vcast_param->nx_ip_udp_created_sockets_ptr))));
        break; /* end case 125*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_created_sockets_count */
      case 126: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_created_sockets_count))));
        break; /* end case 126*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_packet_receive */
      case 127: { 
        VCAST_TI_11_125 ( ((void (**)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *))(&(vcast_param->nx_ip_udp_packet_receive))));
        break; /* end case 127*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_udp_port_search_start */
      case 128: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_udp_port_search_start))));
        break; /* end case 128*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_port_table */
      case 129: { 
        VCAST_TI_11_136 ( ((struct NX_TCP_SOCKET_STRUCT **)(vcast_param->nx_ip_tcp_port_table)));
        break; /* end case 129*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_created_sockets_ptr */
      case 130: { 
        VCAST_TI_11_12 ( ((struct NX_TCP_SOCKET_STRUCT **)(&(vcast_param->nx_ip_tcp_created_sockets_ptr))));
        break; /* end case 130*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_created_sockets_count */
      case 131: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_created_sockets_count))));
        break; /* end case 131*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_packet_receive */
      case 132: { 
        VCAST_TI_11_138 ( ((void (**)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *))(&(vcast_param->nx_ip_tcp_packet_receive))));
        break; /* end case 132*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_periodic_processing */
      case 133: { 
        VCAST_TI_11_140 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_tcp_periodic_processing))));
        break; /* end case 133*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_fast_periodic_processing */
      case 134: { 
        VCAST_TI_11_142 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_tcp_fast_periodic_processing))));
        break; /* end case 134*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_queue_process */
      case 135: { 
        VCAST_TI_11_144 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_tcp_queue_process))));
        break; /* end case 135*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_queue_head */
      case 136: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_tcp_queue_head))));
        break; /* end case 136*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_queue_tail */
      case 137: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_tcp_queue_tail))));
        break; /* end case 137*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_received_packet_count */
      case 138: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_received_packet_count))));
        break; /* end case 138*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_server_listen_reqs */
      case 139: { 
        VCAST_TI_11_150 ( ((struct NX_TCP_LISTEN_STRUCT *)(vcast_param->nx_ip_tcp_server_listen_reqs)));
        break; /* end case 139*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_available_listen_requests */
      case 140: { 
        VCAST_TI_11_145 ( ((struct NX_TCP_LISTEN_STRUCT **)(&(vcast_param->nx_ip_tcp_available_listen_requests))));
        break; /* end case 140*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_active_listen_requests */
      case 141: { 
        VCAST_TI_11_145 ( ((struct NX_TCP_LISTEN_STRUCT **)(&(vcast_param->nx_ip_tcp_active_listen_requests))));
        break; /* end case 141*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_tcp_port_search_start */
      case 142: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_tcp_port_search_start))));
        break; /* end case 142*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_fast_periodic_timer_created */
      case 143: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_fast_periodic_timer_created))));
        break; /* end case 143*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_fast_periodic_timer */
      case 144: { 
        VCAST_TI_11_57 ( ((struct TX_TIMER_STRUCT *)(&(vcast_param->nx_ip_fast_periodic_timer))));
        break; /* end case 144*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_table */
      case 145: { 
        VCAST_TI_11_154 ( ((struct NX_ARP_STRUCT **)(vcast_param->nx_ip_arp_table)));
        break; /* end case 145*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_static_list */
      case 146: { 
        VCAST_TI_11_152 ( ((struct NX_ARP_STRUCT **)(&(vcast_param->nx_ip_arp_static_list))));
        break; /* end case 146*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_dynamic_list */
      case 147: { 
        VCAST_TI_11_152 ( ((struct NX_ARP_STRUCT **)(&(vcast_param->nx_ip_arp_dynamic_list))));
        break; /* end case 147*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_dynamic_active_count */
      case 148: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_dynamic_active_count))));
        break; /* end case 148*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_deferred_received_packet_head */
      case 149: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_arp_deferred_received_packet_head))));
        break; /* end case 149*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_deferred_received_packet_tail */
      case 150: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_arp_deferred_received_packet_tail))));
        break; /* end case 150*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_allocate */
      case 151: { 
        VCAST_TI_11_156 ( ((UINT (**)(struct NX_IP_STRUCT *, struct NX_ARP_STRUCT **))(&(vcast_param->nx_ip_arp_allocate))));
        break; /* end case 151*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_periodic_update */
      case 152: { 
        VCAST_TI_11_158 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_arp_periodic_update))));
        break; /* end case 152*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_queue_process */
      case 153: { 
        VCAST_TI_11_160 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_arp_queue_process))));
        break; /* end case 153*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_packet_send */
      case 154: { 
        VCAST_TI_11_162 ( ((void (**)(struct NX_IP_STRUCT *, ULONG destination_ip, NX_INTERFACE * nx_interface))(&(vcast_param->nx_ip_arp_packet_send))));
        break; /* end case 154*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_gratuitous_response_handler */
      case 155: { 
        VCAST_TI_11_164 ( ((void (**)(struct NX_IP_STRUCT *, NX_PACKET *))(&(vcast_param->nx_ip_arp_gratuitous_response_handler))));
        break; /* end case 155*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_collision_notify_response_handler */
      case 156: { 
        VCAST_TI_11_166 ( ((void (**)(void *))(&(vcast_param->nx_ip_arp_collision_notify_response_handler))));
        break; /* end case 156*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_collision_notify_parameter */
      case 157: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->nx_ip_arp_collision_notify_parameter))));
        break; /* end case 157*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_collision_notify_ip_address */
      case 158: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_collision_notify_ip_address))));
        break; /* end case 158*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_cache_memory */
      case 159: { 
        VCAST_TI_11_152 ( ((struct NX_ARP_STRUCT **)(&(vcast_param->nx_ip_arp_cache_memory))));
        break; /* end case 159*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_arp_total_entries */
      case 160: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_ip_arp_total_entries))));
        break; /* end case 160*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_rarp_periodic_update */
      case 161: { 
        VCAST_TI_11_168 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_rarp_periodic_update))));
        break; /* end case 161*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_rarp_queue_process */
      case 162: { 
        VCAST_TI_11_170 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_ip_rarp_queue_process))));
        break; /* end case 162*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_rarp_deferred_received_packet_head */
      case 163: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_rarp_deferred_received_packet_head))));
        break; /* end case 163*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_rarp_deferred_received_packet_tail */
      case 164: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_ip_rarp_deferred_received_packet_tail))));
        break; /* end case 164*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_created_next */
      case 165: { 
        VCAST_TI_11_81 ( ((struct NX_IP_STRUCT **)(&(vcast_param->nx_ip_created_next))));
        break; /* end case 165*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_created_previous */
      case 166: { 
        VCAST_TI_11_81 ( ((struct NX_IP_STRUCT **)(&(vcast_param->nx_ip_created_previous))));
        break; /* end case 166*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_reserved_ptr */
      case 167: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->nx_ip_reserved_ptr))));
        break; /* end case 167*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_deferred_cleanup_check */
      case 168: { 
        VCAST_TI_11_172 ( ((void (**)(struct NX_IP_STRUCT *))(&(vcast_param->nx_tcp_deferred_cleanup_check))));
        break; /* end case 168*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ip_interface */
      case 169: { 
        VCAST_TI_11_173 ( ((struct NX_INTERFACE_STRUCT *)(vcast_param->nx_ip_interface)));
        break; /* end case 169*/
      } /* end case */
      /* Setting member variable vcast_param->nx_ipv4_packet_receive */
      case 170: { 
        VCAST_TI_11_175 ( ((void (**)(struct NX_IP_STRUCT *, NX_PACKET *))(&(vcast_param->nx_ipv4_packet_receive))));
        break; /* end case 170*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_91 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_76 ( struct NX_PACKET_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_76 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_76 ( struct NX_PACKET_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nx_packet_pool_owner */
      case 1: { 
        VCAST_TI_11_78 ( ((struct NX_PACKET_POOL_STRUCT **)(&(vcast_param->nx_packet_pool_owner))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_queue_next */
      case 2: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_packet_queue_next))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_tcp_queue_next */
      case 3: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_packet_tcp_queue_next))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_next */
      case 4: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_packet_next))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_last */
      case 5: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_packet_last))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_fragment_next */
      case 6: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_packet_fragment_next))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_length */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_length))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_ip_interface */
      case 8: { 
        VCAST_TI_11_80 ( ((struct NX_INTERFACE_STRUCT **)(&(vcast_param->nx_packet_ip_interface))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_next_hop_address */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_next_hop_address))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_data_start */
      case 10: { 
        VCAST_TI_9_13 ( ((unsigned char **)(&(vcast_param->nx_packet_data_start))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_data_end */
      case 11: { 
        VCAST_TI_9_13 ( ((unsigned char **)(&(vcast_param->nx_packet_data_end))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_prepend_ptr */
      case 12: { 
        VCAST_TI_9_13 ( ((unsigned char **)(&(vcast_param->nx_packet_prepend_ptr))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_append_ptr */
      case 13: { 
        VCAST_TI_9_13 ( ((unsigned char **)(&(vcast_param->nx_packet_append_ptr))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_reassembly_time */
      case 14: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_reassembly_time))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_ip_version */
      case 15: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_ip_version))));
        break; /* end case 15*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_ip_header */
      case 16: { 
        VCAST_TI_9_13 ( ((unsigned char **)(&(vcast_param->nx_packet_ip_header))));
        break; /* end case 16*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_76 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_176 ( struct NXD_IPV6_ADDRESS_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_176 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_176 ( struct NXD_IPV6_ADDRESS_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_176_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_176_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_176_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_176_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_176_array_size*(sizeof(struct NXD_IPV6_ADDRESS_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_176_array_size*(sizeof(struct NXD_IPV6_ADDRESS_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_176_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_176_index = vcast_get_param();
          VCAST_TI_11_178 ( &((*vcast_param)[VCAST_TI_11_176_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_176 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_180 ( void (**vcast_param)(struct NX_IP_DRIVER_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_180 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_180 ( void (**vcast_param)(struct NX_IP_DRIVER_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_DRIVER_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_180 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_84 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_84 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_84 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, NX_PACKET *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_84 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_78 ( struct NX_PACKET_POOL_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_78 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_78 ( struct NX_PACKET_POOL_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_78_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_78_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_78_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_78_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_78_array_size*(sizeof(struct NX_PACKET_POOL_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_78_array_size*(sizeof(struct NX_PACKET_POOL_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_78_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_78_index = vcast_get_param();
          VCAST_TI_11_79 ( &((*vcast_param)[VCAST_TI_11_78_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_78 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_86 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_86 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_86 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, NX_PACKET *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_86 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_88 ( UINT (**vcast_param)(struct NX_IP_STRUCT *, ULONG, NX_PACKET *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_88 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_88 ( UINT (**vcast_param)(struct NX_IP_STRUCT *, ULONG, NX_PACKET *) ) 
{
  UINT (*vcast_local_ptr)(struct NX_IP_STRUCT *, ULONG, NX_PACKET *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_88 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_70 ( struct TX_EVENT_FLAGS_GROUP_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_70 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_70 ( struct TX_EVENT_FLAGS_GROUP_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->tx_event_flags_group_id */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_event_flags_group_id))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->tx_event_flags_group_name */
      case 2: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->tx_event_flags_group_name))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->tx_event_flags_group_current */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_event_flags_group_current))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->tx_event_flags_group_reset_search */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_event_flags_group_reset_search))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->tx_event_flags_group_suspension_list */
      case 5: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->tx_event_flags_group_suspension_list))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->tx_event_flags_group_suspended_count */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_event_flags_group_suspended_count))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->tx_event_flags_group_created_next */
      case 7: { 
        VCAST_TI_11_71 ( ((struct TX_EVENT_FLAGS_GROUP_STRUCT **)(&(vcast_param->tx_event_flags_group_created_next))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->tx_event_flags_group_created_previous */
      case 8: { 
        VCAST_TI_11_71 ( ((struct TX_EVENT_FLAGS_GROUP_STRUCT **)(&(vcast_param->tx_event_flags_group_created_previous))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->tx_event_flags_group_delayed_clear */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_event_flags_group_delayed_clear))));
        break; /* end case 9*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_70 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_57 ( struct TX_TIMER_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_57 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_57 ( struct TX_TIMER_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->tx_timer_id */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->tx_timer_id))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_name */
      case 2: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->tx_timer_name))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_internal */
      case 3: { 
        VCAST_TI_11_52 ( ((struct TX_TIMER_INTERNAL_STRUCT *)(&(vcast_param->tx_timer_internal))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_created_next */
      case 4: { 
        VCAST_TI_11_58 ( ((struct TX_TIMER_STRUCT **)(&(vcast_param->tx_timer_created_next))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->tx_timer_created_previous */
      case 5: { 
        VCAST_TI_11_58 ( ((struct TX_TIMER_STRUCT **)(&(vcast_param->tx_timer_created_previous))));
        break; /* end case 5*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_57 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_95 ( void (**vcast_param)(struct NX_IP_DRIVER_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_95 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_95 ( void (**vcast_param)(struct NX_IP_DRIVER_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_DRIVER_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_95 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_97 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_97 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_97 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_97 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_99 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_99 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_99 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_99 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_101 ( void (**vcast_param)(struct NX_IP_STRUCT *, void *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_101 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_101 ( void (**vcast_param)(struct NX_IP_STRUCT *, void *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, void *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_101 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_102 ( unsigned vcast_param[1] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_102 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_102 ( unsigned vcast_param[1] ) 
{
  {
    int VCAST_TI_11_102_array_index = 0;
    int VCAST_TI_11_102_index = 0;
    int VCAST_TI_11_102_first, VCAST_TI_11_102_last;
    int VCAST_TI_11_102_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_102_local_field = 0;
    int VCAST_TI_11_102_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_11_102_first, &VCAST_TI_11_102_last, &VCAST_TI_11_102_more_data);
    VCAST_TI_11_102_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_102_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_102_upper = 1;
      for (VCAST_TI_11_102_array_index=0; VCAST_TI_11_102_array_index< VCAST_TI_11_102_upper; VCAST_TI_11_102_array_index++){
        if ( (VCAST_TI_11_102_index >= VCAST_TI_11_102_first) && ( VCAST_TI_11_102_index <= VCAST_TI_11_102_last)){
          VCAST_TI_11_4 ( &(vcast_param[VCAST_TI_11_102_index]));
          VCAST_TI_11_102_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_102_local_field;
        } /* if */
        if (VCAST_TI_11_102_index >= VCAST_TI_11_102_last)
          break;
        VCAST_TI_11_102_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_102_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_102 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_103 ( struct NX_INTERFACE_STRUCT *vcast_param[1] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_103 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_103 ( struct NX_INTERFACE_STRUCT *vcast_param[1] ) 
{
  {
    int VCAST_TI_11_103_array_index = 0;
    int VCAST_TI_11_103_index = 0;
    int VCAST_TI_11_103_first, VCAST_TI_11_103_last;
    int VCAST_TI_11_103_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_103_local_field = 0;
    int VCAST_TI_11_103_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_11_103_first, &VCAST_TI_11_103_last, &VCAST_TI_11_103_more_data);
    VCAST_TI_11_103_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_103_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_103_upper = 1;
      for (VCAST_TI_11_103_array_index=0; VCAST_TI_11_103_array_index< VCAST_TI_11_103_upper; VCAST_TI_11_103_array_index++){
        if ( (VCAST_TI_11_103_index >= VCAST_TI_11_103_first) && ( VCAST_TI_11_103_index <= VCAST_TI_11_103_last)){
          VCAST_TI_11_80 ( &(vcast_param[VCAST_TI_11_103_index]));
          VCAST_TI_11_103_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_103_local_field;
        } /* if */
        if (VCAST_TI_11_103_index >= VCAST_TI_11_103_last)
          break;
        VCAST_TI_11_103_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_103_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_103 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_108 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_108 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_108 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_108 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_110 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_110 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_110 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_110 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_112 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_112 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_112 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_112 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_114 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_114 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_114 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_114 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_116 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_116 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_116 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_116 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_118 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_118 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_118 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, NX_PACKET *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_118 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_123 ( struct NX_UDP_SOCKET_STRUCT *vcast_param[32U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_123 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_123 ( struct NX_UDP_SOCKET_STRUCT *vcast_param[32U] ) 
{
  {
    int VCAST_TI_11_123_array_index = 0;
    int VCAST_TI_11_123_index = 0;
    int VCAST_TI_11_123_first, VCAST_TI_11_123_last;
    int VCAST_TI_11_123_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_123_local_field = 0;
    int VCAST_TI_11_123_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_11_123_first, &VCAST_TI_11_123_last, &VCAST_TI_11_123_more_data);
    VCAST_TI_11_123_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_123_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_123_upper = 32U;
      for (VCAST_TI_11_123_array_index=0; VCAST_TI_11_123_array_index< VCAST_TI_11_123_upper; VCAST_TI_11_123_array_index++){
        if ( (VCAST_TI_11_123_index >= VCAST_TI_11_123_first) && ( VCAST_TI_11_123_index <= VCAST_TI_11_123_last)){
          VCAST_TI_11_13 ( &(vcast_param[VCAST_TI_11_123_index]));
          VCAST_TI_11_123_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_123_local_field;
        } /* if */
        if (VCAST_TI_11_123_index >= VCAST_TI_11_123_last)
          break;
        VCAST_TI_11_123_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_123_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_123 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_125 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_125 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_125 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_125 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_136 ( struct NX_TCP_SOCKET_STRUCT *vcast_param[32U] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_136 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_136 ( struct NX_TCP_SOCKET_STRUCT *vcast_param[32U] ) 
{
  {
    int VCAST_TI_11_136_array_index = 0;
    int VCAST_TI_11_136_index = 0;
    int VCAST_TI_11_136_first, VCAST_TI_11_136_last;
    int VCAST_TI_11_136_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_136_local_field = 0;
    int VCAST_TI_11_136_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_11_136_first, &VCAST_TI_11_136_last, &VCAST_TI_11_136_more_data);
    VCAST_TI_11_136_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_136_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_136_upper = 32U;
      for (VCAST_TI_11_136_array_index=0; VCAST_TI_11_136_array_index< VCAST_TI_11_136_upper; VCAST_TI_11_136_array_index++){
        if ( (VCAST_TI_11_136_index >= VCAST_TI_11_136_first) && ( VCAST_TI_11_136_index <= VCAST_TI_11_136_last)){
          VCAST_TI_11_12 ( &(vcast_param[VCAST_TI_11_136_index]));
          VCAST_TI_11_136_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_136_local_field;
        } /* if */
        if (VCAST_TI_11_136_index >= VCAST_TI_11_136_last)
          break;
        VCAST_TI_11_136_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_136_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_136 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_138 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_138 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_138 ( void (**vcast_param)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, struct NX_PACKET_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_138 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_140 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_140 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_140 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_140 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_142 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_142 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_142 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_142 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_144 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_144 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_144 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_144 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_150 ( struct NX_TCP_LISTEN_STRUCT vcast_param[4] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_150 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_150 ( struct NX_TCP_LISTEN_STRUCT vcast_param[4] ) 
{
  {
    int VCAST_TI_11_150_array_index = 0;
    int VCAST_TI_11_150_index = 0;
    int VCAST_TI_11_150_first, VCAST_TI_11_150_last;
    int VCAST_TI_11_150_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_150_local_field = 0;
    int VCAST_TI_11_150_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_11_150_first, &VCAST_TI_11_150_last, &VCAST_TI_11_150_more_data);
    VCAST_TI_11_150_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_150_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_150_upper = 4;
      for (VCAST_TI_11_150_array_index=0; VCAST_TI_11_150_array_index< VCAST_TI_11_150_upper; VCAST_TI_11_150_array_index++){
        if ( (VCAST_TI_11_150_index >= VCAST_TI_11_150_first) && ( VCAST_TI_11_150_index <= VCAST_TI_11_150_last)){
          VCAST_TI_11_146 ( &(vcast_param[VCAST_TI_11_150_index]));
          VCAST_TI_11_150_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_150_local_field;
        } /* if */
        if (VCAST_TI_11_150_index >= VCAST_TI_11_150_last)
          break;
        VCAST_TI_11_150_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_150_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_150 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_145 ( struct NX_TCP_LISTEN_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_145 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_145 ( struct NX_TCP_LISTEN_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_145_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_145_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_145_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_145_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_145_array_size*(sizeof(struct NX_TCP_LISTEN_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_145_array_size*(sizeof(struct NX_TCP_LISTEN_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_145_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_145_index = vcast_get_param();
          VCAST_TI_11_146 ( &((*vcast_param)[VCAST_TI_11_145_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_145 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_154 ( struct NX_ARP_STRUCT *vcast_param[32] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_154 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_154 ( struct NX_ARP_STRUCT *vcast_param[32] ) 
{
  {
    int VCAST_TI_11_154_array_index = 0;
    int VCAST_TI_11_154_index = 0;
    int VCAST_TI_11_154_first, VCAST_TI_11_154_last;
    int VCAST_TI_11_154_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_154_local_field = 0;
    int VCAST_TI_11_154_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_11_154_first, &VCAST_TI_11_154_last, &VCAST_TI_11_154_more_data);
    VCAST_TI_11_154_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_154_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_154_upper = 32;
      for (VCAST_TI_11_154_array_index=0; VCAST_TI_11_154_array_index< VCAST_TI_11_154_upper; VCAST_TI_11_154_array_index++){
        if ( (VCAST_TI_11_154_index >= VCAST_TI_11_154_first) && ( VCAST_TI_11_154_index <= VCAST_TI_11_154_last)){
          VCAST_TI_11_152 ( &(vcast_param[VCAST_TI_11_154_index]));
          VCAST_TI_11_154_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_154_local_field;
        } /* if */
        if (VCAST_TI_11_154_index >= VCAST_TI_11_154_last)
          break;
        VCAST_TI_11_154_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_154_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_154 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_152 ( struct NX_ARP_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_152 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_152 ( struct NX_ARP_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_152_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_152_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_152_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_152_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_152_array_size*(sizeof(struct NX_ARP_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_152_array_size*(sizeof(struct NX_ARP_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_152_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_152_index = vcast_get_param();
          VCAST_TI_11_153 ( &((*vcast_param)[VCAST_TI_11_152_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_152 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_156 ( UINT (**vcast_param)(struct NX_IP_STRUCT *, struct NX_ARP_STRUCT **) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_156 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_156 ( UINT (**vcast_param)(struct NX_IP_STRUCT *, struct NX_ARP_STRUCT **) ) 
{
  UINT (*vcast_local_ptr)(struct NX_IP_STRUCT *, struct NX_ARP_STRUCT **);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_156 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_158 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_158 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_158 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_158 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_160 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_160 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_160 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_160 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_162 ( void (**vcast_param)(struct NX_IP_STRUCT *, ULONG destination_ip, NX_INTERFACE * nx_interface) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_162 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_162 ( void (**vcast_param)(struct NX_IP_STRUCT *, ULONG destination_ip, NX_INTERFACE * nx_interface) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, ULONG destination_ip, NX_INTERFACE * nx_interface);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_162 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_164 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_164 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_164 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, NX_PACKET *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_164 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_166 ( void (**vcast_param)(void *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_166 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_166 ( void (**vcast_param)(void *) ) 
{
  void (*vcast_local_ptr)(void *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_166 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_168 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_168 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_168 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_168 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_170 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_170 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_170 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_170 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_172 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_172 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_172 ( void (**vcast_param)(struct NX_IP_STRUCT *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_172 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_173 ( struct NX_INTERFACE_STRUCT vcast_param[1 + 1] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_173 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_173 ( struct NX_INTERFACE_STRUCT vcast_param[1 + 1] ) 
{
  {
    int VCAST_TI_11_173_array_index = 0;
    int VCAST_TI_11_173_index = 0;
    int VCAST_TI_11_173_first, VCAST_TI_11_173_last;
    int VCAST_TI_11_173_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_173_local_field = 0;
    int VCAST_TI_11_173_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_11_173_first, &VCAST_TI_11_173_last, &VCAST_TI_11_173_more_data);
    VCAST_TI_11_173_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_173_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1 + 1);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_173_upper = 1 + 1;
      for (VCAST_TI_11_173_array_index=0; VCAST_TI_11_173_array_index< VCAST_TI_11_173_upper; VCAST_TI_11_173_array_index++){
        if ( (VCAST_TI_11_173_index >= VCAST_TI_11_173_first) && ( VCAST_TI_11_173_index <= VCAST_TI_11_173_last)){
          VCAST_TI_11_92 ( &(vcast_param[VCAST_TI_11_173_index]));
          VCAST_TI_11_173_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_173_local_field;
        } /* if */
        if (VCAST_TI_11_173_index >= VCAST_TI_11_173_last)
          break;
        VCAST_TI_11_173_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_173_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_173 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_175 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_175 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_175 ( void (**vcast_param)(struct NX_IP_STRUCT *, NX_PACKET *) ) 
{
  void (*vcast_local_ptr)(struct NX_IP_STRUCT *, NX_PACKET *);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_175 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_178 ( struct NXD_IPV6_ADDRESS_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_178 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_178 ( struct NXD_IPV6_ADDRESS_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nxd_ipv6_address_valid */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->nxd_ipv6_address_valid))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ipv6_address_type */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->nxd_ipv6_address_type))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ipv6_address_state */
      case 3: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->nxd_ipv6_address_state))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ipv6_address_prefix_length */
      case 4: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->nxd_ipv6_address_prefix_length))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ipv6_address_attached */
      case 5: { 
        VCAST_TI_11_80 ( ((struct NX_INTERFACE_STRUCT **)(&(vcast_param->nxd_ipv6_address_attached))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ipv6_address */
      case 6: { 
        VCAST_TI_11_177 ( ((unsigned *)(vcast_param->nxd_ipv6_address)));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ipv6_address_next */
      case 7: { 
        VCAST_TI_11_176 ( ((struct NXD_IPV6_ADDRESS_STRUCT **)(&(vcast_param->nxd_ipv6_address_next))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ipv6_address_DupAddrDetectTransmit */
      case 8: { 
        VCAST_TI_8_4 ( ((char *)(&(vcast_param->nxd_ipv6_address_DupAddrDetectTransmit))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ipv6_address_ConfigurationMethod */
      case 9: { 
        VCAST_TI_8_4 ( ((char *)(&(vcast_param->nxd_ipv6_address_ConfigurationMethod))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->nxd_ipv6_address_index */
      case 10: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->nxd_ipv6_address_index))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->reserved */
      case 11: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->reserved))));
        break; /* end case 11*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_178 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_79 ( struct NX_PACKET_POOL_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_79 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_79 ( struct NX_PACKET_POOL_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nx_packet_pool_id */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_pool_id))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_name */
      case 2: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->nx_packet_pool_name))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_available */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_pool_available))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_total */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_pool_total))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_empty_requests */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_pool_empty_requests))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_empty_suspensions */
      case 6: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_pool_empty_suspensions))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_invalid_releases */
      case 7: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_pool_invalid_releases))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_available_list */
      case 8: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_packet_pool_available_list))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_start */
      case 9: { 
        VCAST_TI_9_8 ( ((char **)(&(vcast_param->nx_packet_pool_start))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_size */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_pool_size))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_payload_size */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_pool_payload_size))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_suspension_list */
      case 12: { 
        VCAST_TI_11_7 ( ((struct TX_THREAD_STRUCT **)(&(vcast_param->nx_packet_pool_suspension_list))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_suspended_count */
      case 13: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_packet_pool_suspended_count))));
        break; /* end case 13*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_created_next */
      case 14: { 
        VCAST_TI_11_78 ( ((struct NX_PACKET_POOL_STRUCT **)(&(vcast_param->nx_packet_pool_created_next))));
        break; /* end case 14*/
      } /* end case */
      /* Setting member variable vcast_param->nx_packet_pool_created_previous */
      case 15: { 
        VCAST_TI_11_78 ( ((struct NX_PACKET_POOL_STRUCT **)(&(vcast_param->nx_packet_pool_created_previous))));
        break; /* end case 15*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_79 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_71 ( struct TX_EVENT_FLAGS_GROUP_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_71 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_71 ( struct TX_EVENT_FLAGS_GROUP_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_71_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_71_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_71_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_71_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_71_array_size*(sizeof(struct TX_EVENT_FLAGS_GROUP_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_71_array_size*(sizeof(struct TX_EVENT_FLAGS_GROUP_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_71_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_71_index = vcast_get_param();
          VCAST_TI_11_70 ( &((*vcast_param)[VCAST_TI_11_71_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_71 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_58 ( struct TX_TIMER_STRUCT **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_58 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_58 ( struct TX_TIMER_STRUCT **vcast_param ) 
{
  {
    int VCAST_TI_11_58_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_58_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_58_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_58_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_58_array_size*(sizeof(struct TX_TIMER_STRUCT )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_58_array_size*(sizeof(struct TX_TIMER_STRUCT )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_58_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_58_index = vcast_get_param();
          VCAST_TI_11_57 ( &((*vcast_param)[VCAST_TI_11_58_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_58 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_146 ( struct NX_TCP_LISTEN_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_146 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_146 ( struct NX_TCP_LISTEN_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nx_tcp_listen_port */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_listen_port))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_listen_callback */
      case 2: { 
        VCAST_TI_11_149 ( ((void (**)(NX_TCP_SOCKET * socket_ptr, UINT port))(&(vcast_param->nx_tcp_listen_callback))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_listen_socket_ptr */
      case 3: { 
        VCAST_TI_11_12 ( ((struct NX_TCP_SOCKET_STRUCT **)(&(vcast_param->nx_tcp_listen_socket_ptr))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_listen_queue_maximum */
      case 4: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_listen_queue_maximum))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_listen_queue_current */
      case 5: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_tcp_listen_queue_current))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_listen_queue_head */
      case 6: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_tcp_listen_queue_head))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_listen_queue_tail */
      case 7: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_tcp_listen_queue_tail))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_listen_next */
      case 8: { 
        VCAST_TI_11_145 ( ((struct NX_TCP_LISTEN_STRUCT **)(&(vcast_param->nx_tcp_listen_next))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->nx_tcp_listen_previous */
      case 9: { 
        VCAST_TI_11_145 ( ((struct NX_TCP_LISTEN_STRUCT **)(&(vcast_param->nx_tcp_listen_previous))));
        break; /* end case 9*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_146 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_153 ( struct NX_ARP_STRUCT *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_153 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_153 ( struct NX_ARP_STRUCT *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->nx_arp_route_static */
      case 1: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_arp_route_static))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_entry_next_update */
      case 2: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_arp_entry_next_update))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_retries */
      case 3: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_arp_retries))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_pool_next */
      case 4: { 
        VCAST_TI_11_152 ( ((struct NX_ARP_STRUCT **)(&(vcast_param->nx_arp_pool_next))));
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_pool_previous */
      case 5: { 
        VCAST_TI_11_152 ( ((struct NX_ARP_STRUCT **)(&(vcast_param->nx_arp_pool_previous))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_active_next */
      case 6: { 
        VCAST_TI_11_152 ( ((struct NX_ARP_STRUCT **)(&(vcast_param->nx_arp_active_next))));
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_active_previous */
      case 7: { 
        VCAST_TI_11_152 ( ((struct NX_ARP_STRUCT **)(&(vcast_param->nx_arp_active_previous))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_active_list_head */
      case 8: { 
        VCAST_TI_11_151 ( ((struct NX_ARP_STRUCT ***)(&(vcast_param->nx_arp_active_list_head))));
        break; /* end case 8*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_ip_address */
      case 9: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_arp_ip_address))));
        break; /* end case 9*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_physical_address_msw */
      case 10: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_arp_physical_address_msw))));
        break; /* end case 10*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_physical_address_lsw */
      case 11: { 
        VCAST_TI_11_4 ( ((unsigned *)(&(vcast_param->nx_arp_physical_address_lsw))));
        break; /* end case 11*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_ip_interface */
      case 12: { 
        VCAST_TI_11_80 ( ((struct NX_INTERFACE_STRUCT **)(&(vcast_param->nx_arp_ip_interface))));
        break; /* end case 12*/
      } /* end case */
      /* Setting member variable vcast_param->nx_arp_packets_waiting */
      case 13: { 
        VCAST_TI_11_82 ( ((struct NX_PACKET_STRUCT **)(&(vcast_param->nx_arp_packets_waiting))));
        break; /* end case 13*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_11_153 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An array */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_177 ( unsigned vcast_param[4] ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_177 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_177 ( unsigned vcast_param[4] ) 
{
  {
    int VCAST_TI_11_177_array_index = 0;
    int VCAST_TI_11_177_index = 0;
    int VCAST_TI_11_177_first, VCAST_TI_11_177_last;
    int VCAST_TI_11_177_more_data; /* true if there is more data in the current command */
    int VCAST_TI_11_177_local_field = 0;
    int VCAST_TI_11_177_value_printed = 0;


    vcast_get_range_value (&VCAST_TI_11_177_first, &VCAST_TI_11_177_last, &VCAST_TI_11_177_more_data);
    VCAST_TI_11_177_local_field = vCAST_DATA_FIELD;
    if ( vCAST_SIZE && (!VCAST_TI_11_177_more_data)) { /* get the size of the array */
      vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
      vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n");
    } else {
      int VCAST_TI_11_177_upper = 4;
      for (VCAST_TI_11_177_array_index=0; VCAST_TI_11_177_array_index< VCAST_TI_11_177_upper; VCAST_TI_11_177_array_index++){
        if ( (VCAST_TI_11_177_index >= VCAST_TI_11_177_first) && ( VCAST_TI_11_177_index <= VCAST_TI_11_177_last)){
          VCAST_TI_11_4 ( &(vcast_param[VCAST_TI_11_177_index]));
          VCAST_TI_11_177_value_printed = 1;
          vCAST_DATA_FIELD = VCAST_TI_11_177_local_field;
        } /* if */
        if (VCAST_TI_11_177_index >= VCAST_TI_11_177_last)
          break;
        VCAST_TI_11_177_index++;
      } /* loop */
      if ((vCAST_COMMAND == vCAST_PRINT)&&(!VCAST_TI_11_177_value_printed))
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<past end of array>>\n");
    } /* if */
  }
} /* end VCAST_TI_11_177 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_149 ( void (**vcast_param)(NX_TCP_SOCKET * socket_ptr, UINT port) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_149 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_149 ( void (**vcast_param)(NX_TCP_SOCKET * socket_ptr, UINT port) ) 
{
  void (*vcast_local_ptr)(NX_TCP_SOCKET * socket_ptr, UINT port);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_11_149 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_11_151 ( struct NX_ARP_STRUCT ***vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_11_151 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_11_151 ( struct NX_ARP_STRUCT ***vcast_param ) 
{
  {
    int VCAST_TI_11_151_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_11_151_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_11_151_memory_ptr = (void**)vcast_param;
            *VCAST_TI_11_151_memory_ptr = (void*)VCAST_malloc(VCAST_TI_11_151_array_size*(sizeof(struct NX_ARP_STRUCT *)));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_11_151_array_size*(sizeof(struct NX_ARP_STRUCT *)));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_11_151_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_11_151_index = vcast_get_param();
          VCAST_TI_11_152 ( &((*vcast_param)[VCAST_TI_11_151_index]));
        }
      }
    }
  }
} /* end VCAST_TI_11_151 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


#ifdef VCAST_PARADIGM_ADD_SEGMENT
#pragma new_codesegment(1)
#endif
void VCAST_TI_RANGE_DATA_11 ( void ) {
#define VCAST_TI_SCALAR_TYPE "NEW_SCALAR\n"
#define VCAST_TI_ARRAY_TYPE  "NEW_ARRAY\n"
  /* Range Data for TI (scalar) VCAST_TI_11_4 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100006\n" );
  vectorcast_fprint_unsigned_integer (vCAST_OUTPUT_FILE,UINT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_integer (vCAST_OUTPUT_FILE,(UINT_MIN / 2) + (UINT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_integer (vCAST_OUTPUT_FILE,UINT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_8_1 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100007\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,INT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(INT_MIN / 2) + (INT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,INT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (array) VCAST_TI_11_8 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100004\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1536U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_11 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100005\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3328U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (scalar) VCAST_TI_8_2 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100015\n" );
  vectorcast_fprint_unsigned_long (vCAST_OUTPUT_FILE,ULONG_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_long (vCAST_OUTPUT_FILE,(ULONG_MIN / 2) + (ULONG_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_long (vCAST_OUTPUT_FILE,ULONG_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (array) VCAST_TI_11_21 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100006\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(uint32_t)((4U * 1U) * 32U));
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (scalar) VCAST_TI_11_25 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100025\n" );
  vectorcast_fprint_long (vCAST_OUTPUT_FILE,LONG_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_long (vCAST_OUTPUT_FILE,(LONG_MIN / 2) + (LONG_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_long (vCAST_OUTPUT_FILE,LONG_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
#ifndef VCAST_NO_FLOAT
  /* Range Data for TI (scalar) VCAST_TI_8_3 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100026\n" );
  vectorcast_fprint_long_float (vCAST_OUTPUT_FILE,-(FLT_MAX) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_long_float (vCAST_OUTPUT_FILE,VCAST_FLT_MID );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_long_float (vCAST_OUTPUT_FILE,FLT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
#endif
  /* Range Data for TI (scalar) VCAST_TI_8_5 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100027\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,UCHAR_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(UCHAR_MIN / 2) + (UCHAR_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,UCHAR_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_11_28 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100028\n" );
  vectorcast_fprint_short (vCAST_OUTPUT_FILE,SHRT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_short (vCAST_OUTPUT_FILE,(SHRT_MIN / 2) + (SHRT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_short (vCAST_OUTPUT_FILE,SHRT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_9_16 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100029\n" );
  vectorcast_fprint_unsigned_short (vCAST_OUTPUT_FILE,USHRT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_short (vCAST_OUTPUT_FILE,(USHRT_MIN / 2) + (USHRT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_short (vCAST_OUTPUT_FILE,USHRT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_11_31 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100032\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,INT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(INT_MIN / 2) + (INT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,INT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_8_4 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"1100041\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,CHAR_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(CHAR_MIN/2) + (CHAR_MAX/2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,CHAR_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (array) VCAST_TI_11_102 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100008\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_103 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100009\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_102 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100010\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_102 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100011\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_102 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100012\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_123 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100013\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_136 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100014\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_150 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100015\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_154 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100016\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_173 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100017\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1 + 1);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_177 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100018\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_194 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100019\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U * (256U + (((16U + 20U) + 20U) + 0U)));
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_224 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100020\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_225 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100021\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,AG_NUMB_MODULE_INPUTS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_226 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100022\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,431);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_227 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100023\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,AG_NUMB_MODULE_OUTPUTS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_228 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100024\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,160);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_229 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100025\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,431);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_230 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100026\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,160);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_429 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100125\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_435 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100129\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_436 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100130\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_437 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100131\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_438 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100132\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_445 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100136\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_446 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100137\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_447 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100138\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_448 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100139\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_449 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100140\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_450 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100141\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_451 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100142\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_452 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100143\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_453 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100144\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_454 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100145\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_455 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100146\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_456 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100147\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_457 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100148\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_458 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100149\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_459 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100150\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_460 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100151\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_461 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100152\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_462 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100153\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_463 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100154\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_464 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100155\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_479 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100163\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_480 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100164\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_483 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100166\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_484 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100167\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_489 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100170\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,11);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_490 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100171\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,11);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_509 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100181\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_510 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100182\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_511 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100183\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_512 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100184\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_513 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100185\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_514 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100186\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_515 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100187\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_516 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100188\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_517 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100189\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_518 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100190\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_519 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100191\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_520 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100192\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_521 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100193\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_522 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100194\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_523 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100195\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_524 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100196\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_525 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100197\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_528 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100199\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,15);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_569 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100220\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_570 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100221\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_571 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100222\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_572 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100223\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_573 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100224\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_574 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100225\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_575 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100226\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_576 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100227\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_577 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100228\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_578 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100229\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_579 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100230\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_580 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100231\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_581 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100232\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_582 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100233\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_591 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100238\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_592 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100239\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_593 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100240\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_594 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100241\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_595 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100242\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_596 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100243\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_597 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100244\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_598 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100245\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_599 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100246\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_600 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100247\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_601 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100248\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_602 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100249\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_603 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100250\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_604 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100251\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_605 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100252\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_606 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100253\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_607 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100254\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,30);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_612 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100257\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_613 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100258\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_614 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100259\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_615 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100260\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_616 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100261\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_617 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100262\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_618 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100263\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_619 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100264\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_620 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100265\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_621 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100266\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_622 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100267\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_623 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100268\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_624 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100269\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_625 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100270\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_626 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100271\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_627 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100272\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_628 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100273\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_629 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100274\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_630 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100275\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_631 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100276\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_632 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100277\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_633 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100278\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_634 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100279\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_635 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100280\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_636 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100281\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_637 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100282\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_638 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100283\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_639 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100284\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_640 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100285\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_641 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100286\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_642 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100287\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_643 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100288\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_644 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100289\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_645 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100290\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_670 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100303\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_671 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100304\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_672 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100305\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_673 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100306\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_674 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100307\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_675 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100308\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_676 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100309\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_677 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100310\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_704 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100324\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,17);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_711 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100328\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_712 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100329\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_713 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100330\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_714 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100331\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,28);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_715 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100332\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_716 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100333\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_717 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100334\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_718 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100335\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_719 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100336\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_720 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100337\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_721 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100338\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_722 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100339\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_723 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100340\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_724 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100341\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_725 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100342\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_726 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100343\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_727 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100344\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_728 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100345\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_729 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100346\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_730 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100347\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_731 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100348\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_732 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100349\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_733 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100350\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_734 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100351\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_735 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100352\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_736 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100353\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_737 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100354\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_738 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100355\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_739 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100356\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_740 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100357\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_741 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100358\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_742 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100359\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_743 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100360\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_744 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100361\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_745 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100362\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_746 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100363\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_747 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100364\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_748 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100365\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_749 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100366\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_752 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100368\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_753 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100369\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_754 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100370\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_755 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100371\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_756 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100372\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_757 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100373\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_758 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100374\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_759 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100375\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_760 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100376\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_761 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100377\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_762 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100378\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_763 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100379\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_764 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100380\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_765 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100381\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_766 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100382\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_767 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100383\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_768 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100384\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_769 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100385\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_770 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100386\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_771 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100387\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_772 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100388\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_773 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100389\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_774 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100390\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_775 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100391\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_776 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100392\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_777 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100393\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_778 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100394\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_779 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100395\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_780 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100396\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_781 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100397\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_782 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100398\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_783 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100399\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_784 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100400\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_785 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100401\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_786 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100402\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_787 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100403\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_788 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100404\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_789 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100405\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_790 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100406\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_791 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100407\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_792 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100408\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_793 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100409\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_794 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100410\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_795 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100411\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_796 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100412\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_797 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100413\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_798 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100414\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_799 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100415\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_800 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100416\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_801 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100417\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_802 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100418\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_803 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100419\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_804 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100420\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_805 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100421\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_806 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100422\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_807 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100423\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_808 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100424\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_809 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100425\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_810 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100426\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_811 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100427\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_812 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100428\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_813 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100429\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_814 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100430\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_815 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100431\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_816 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100432\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_817 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100433\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_818 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100434\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_819 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100435\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_820 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100436\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_821 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100437\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_822 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100438\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_823 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100439\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_824 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100440\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_825 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100441\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_826 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100442\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_827 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100443\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_828 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100444\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_829 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100445\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_830 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100446\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_831 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100447\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_832 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100448\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_833 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100449\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_834 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100450\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_835 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100451\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_836 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100452\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_837 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100453\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_838 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100454\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_839 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100455\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_840 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100456\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_841 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100457\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_842 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100458\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_843 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100459\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_844 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100460\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_845 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100461\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_846 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100462\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_847 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100463\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_848 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100464\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_849 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100465\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_850 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100466\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_851 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100467\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_852 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100468\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_853 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100469\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_854 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100470\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_855 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100471\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_856 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100472\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_857 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100473\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_858 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100474\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_883 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100487\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,20);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_884 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100488\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,20);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_887 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100490\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_888 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100491\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,35);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_891 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100493\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_892 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100494\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_893 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100495\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_894 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100496\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_895 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100497\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_896 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100498\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_897 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100499\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_904 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100503\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_905 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100504\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_906 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100505\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_907 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100506\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_908 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100507\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_911 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100509\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_912 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100510\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_913 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100511\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_914 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100512\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_915 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100513\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_916 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100514\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_917 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100515\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_918 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100516\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_919 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100517\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_920 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100518\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_921 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100519\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_922 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100520\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_923 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100521\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_924 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100522\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_925 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100523\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_926 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100524\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_927 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100525\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_928 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100526\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_929 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100527\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_930 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100528\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_931 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100529\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_932 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100530\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_933 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100531\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_934 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100532\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_943 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100533\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_944 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100534\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_948 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100535\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,18U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_971 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100536\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IO_SF_NUMB_ELEMENTS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_972 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100537\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,IO_NUMB_HW_INPUTS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_973 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100538\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,46);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_974 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100539\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,46);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_977 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100540\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_981 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100541\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_982 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100542\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_983 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100543\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_985 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100544\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_986 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100545\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_987 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100546\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_988 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100547\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_989 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100548\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_990 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100549\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_991 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100550\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_992 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100551\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_993 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100552\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_994 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100553\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_995 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100554\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_996 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100555\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_997 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100556\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_998 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100557\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_999 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100558\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1000 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100559\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1001 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100560\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1002 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100561\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1003 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100562\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1004 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100563\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1005 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100564\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1006 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100565\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1007 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100566\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1008 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100567\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1009 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100568\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1010 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100569\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1013 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100570\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1017 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100571\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1018 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100572\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1019 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100573\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1026 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100574\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1027 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100575\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1028 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100576\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U + 1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1032 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100577\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1033 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100578\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1041 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100579\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,GYRO_NUM_AXES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1043 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100580\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,GYRO_NUM_AXES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1044 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100581\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(124U - sizeof(uint32_t)) - (uint32_t)GYRO_NUM_AXES * sizeof(gyro_cals_t));
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1051 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100582\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,2);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1054 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100583\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ACCEL_NUM_AXES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1055 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100584\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(124U - sizeof(uint32_t)) - (uint32_t)ACCEL_NUM_AXES * sizeof(accel_cals_t));
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1065 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100585\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,4U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1068 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100586\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,12U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1070 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100587\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,8U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1072 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100588\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,16U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1074 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100589\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1075 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100590\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1076 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100591\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1077 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100592\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1078 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100593\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1079 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100594\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1080 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100595\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,GP_NUM_DRIVERS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1081 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100596\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1082 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100597\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1083 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100598\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1084 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100599\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,SDI_NUMB_OF_SWITCH_DRIVERS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1085 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100600\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,24U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1086 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100601\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1087 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100602\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,32U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1088 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100603\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1089 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100604\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,10U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1090 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100605\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,MAX_ALARM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1091 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100606\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,66);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1092 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100607\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1269U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1093 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100608\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1094 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100609\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1095 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100610\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,PSP_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1096 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100611\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,9U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1097 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100612\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1098 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100613\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1099 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100614\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,7U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1100 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100615\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,11U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1101 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100616\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,14U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1102 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100617\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ADC_NUM_MODULES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1103 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100618\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,90U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1104 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100619\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ADC_NUM_MODULES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,90U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1105 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100620\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,1U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1106 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100621\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,SDI_NUMB_OF_SWITCH_DRIVERS);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1107 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100622\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1108 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100623\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,6U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1109 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100624\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,5U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1110 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100625\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1111 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100626\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,NUM_MOTION_DEVICES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1112 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100627\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,NUM_MOTION_DEVICES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1113 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100628\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,NUM_MOTION_DEVICES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1114 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100629\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ETPU2_SPI_NUM_INSTANCES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1115 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100630\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,ETPU2_SPI_NUM_INSTANCES);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1116 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100631\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,QUAD_MAX_NUM);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
  /* Range Data for TI (array) VCAST_TI_11_1117 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_ARRAY_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"100632\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,3U);
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"%%\n");
}
/* Include the file which contains function implementations
for stub processing and value/expected user code */
#include "immcomm_driver_uc.c"

void vCAST_COMMON_STUB_PROC_11(
            int unitIndex,
            int subprogramIndex,
            int robjectIndex,
            int readEobjectData )
{
   vCAST_BEGIN_STUB_PROC_11(unitIndex, subprogramIndex);
   if ( robjectIndex )
      vCAST_READ_COMMAND_DATA_FOR_ONE_PARAM( unitIndex, subprogramIndex, robjectIndex );
   if ( readEobjectData )
      vCAST_READ_COMMAND_DATA_FOR_ONE_PARAM( unitIndex, subprogramIndex, 0 );
   vCAST_SET_HISTORY( unitIndex, subprogramIndex );
   vCAST_READ_COMMAND_DATA( vCAST_CURRENT_SLOT, unitIndex, subprogramIndex, vCAST_true, vCAST_false );
   vCAST_READ_COMMAND_DATA_FOR_USER_GLOBALS();
   vCAST_STUB_PROCESSING_11(unitIndex, subprogramIndex);
}
#endif /* VCAST_HEADER_EXPANSION */
