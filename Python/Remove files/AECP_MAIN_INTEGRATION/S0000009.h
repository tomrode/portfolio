/***********************************************
 *      VectorCAST Test Harness Component      *
 *     Copyright 2018 Vector Software, Inc.    *
 *               6.4p (01/31/17)               *
 ***********************************************/
#ifndef __S9_H__
#define __S9_H__

#include "B0000001.h"
void VCAST_DRIVER_8( int VC_SUBPROGRAM, char *VC_EVENT_FLAGS, char *VC_SLOT_DESCR );
void VCAST_DRIVER_9( int VC_SUBPROGRAM, char *VC_EVENT_FLAGS, char *VC_SLOT_DESCR );void VCAST_SBF_9( int VC_SUBPROGRAM );
void VCAST_DRIVER_11( int VC_SUBPROGRAM, char *VC_EVENT_FLAGS, char *VC_SLOT_DESCR );void VCAST_SBF_11( int VC_SUBPROGRAM );
void VCAST_DRIVER_12( int VC_SUBPROGRAM, char *VC_EVENT_FLAGS, char *VC_SLOT_DESCR );void VCAST_SBF_12( int VC_SUBPROGRAM );

#endif
