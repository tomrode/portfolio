/***********************************************
 *      VectorCAST Test Harness Component      *
 *     Copyright 2018 Vector Software, Inc.    *
 *               6.4p (01/31/17)               *
 ***********************************************/
/***********************************************
 * VectorCAST Unit Information
 *
 * Name: aecp_main
 *
 * Path: C:/rtc_workspaces/TmlVcmMasterBuildDef001_Sandbox/VCM_APP/src/drivers/aecp/aecp_main.c
 *
 * Type: stub-by-function
 *
 * Unit Number: 9
 *
 ***********************************************/
#ifndef VCAST_DRIVER_ONLY
/* Include the file which contains function prototypes
for stub processing and value/expected user code */
#include "vcast_uc_prototypes.h"
#include "vcast_basics.h"
/* STUB_DEPENDENCY_USER_CODE */
/* STUB_DEPENDENCY_USER_CODE_END */
#else
#include "vcast_env_defines.h"
#define __VCAST_BASICS_H__
#endif /* VCAST_DRIVER_ONLY */
#ifndef VCAST_DRIVER_ONLY
#ifndef VCAST_DONT_RENAME_EXIT
#ifdef __cplusplus
extern "C" {
#endif
void exit (int status);
#ifdef __cplusplus
}
#endif
/* used to capture the exit call */
#define exit VCAST_exit
#endif /* VCAST_DONT_RENAME_EXIT */
#endif /* VCAST_DRIVER_ONLY */
#ifndef VCAST_DRIVER_ONLY
#define VCAST_HEADER_EXPANSION
#ifdef VCAST_COVERAGE
#include "aecp_main_inst_prefix.c"
#else
#include "aecp_main_vcast_prefix.c"
#endif
#ifdef VCAST_COVERAGE
/* If coverage is enabled, include the instrumented UUT */
#include "aecp_main_inst.c"
#else
/* If coverage is not enabled, include the original UUT */
#include "aecp_main_vcast.c"
#endif
#ifdef VCAST_COVERAGE
#include "aecp_main_inst_appendix.c"
#else
#include "aecp_main_vcast_appendix.c"
#endif
#endif /* VCAST_DRIVER_ONLY */
#include "aecp_main_driver_prefix.c"
#ifdef VCAST_HEADER_EXPANSION
#ifdef VCAST_COVERAGE
#include "aecp_main_exp_inst_driver.c"
#else
#include "aecp_main_expanded_driver.c"
#endif /*VCAST_COVERAGE*/
#else
#include "S0000009.h"
#include "vcast_undef_9.h"
/* Include the file which contains function prototypes
for stub processing and value/expected user code */
#include "vcast_uc_prototypes.h"
#include "vcast_stubs_9.c"
/* begin declarations of inlined friends */
/* end declarations of inlined friends */
void VCAST_DRIVER_9( int VC_SUBPROGRAM, char *VC_EVENT_FLAGS, char *VC_SLOT_DESCR ) {
#ifdef VCAST_SBF_UNITS_AVAILABLE
  vCAST_MODIFY_SBF_TABLE(9, VC_SUBPROGRAM, vCAST_false);
#endif
  switch( VC_SUBPROGRAM ) {
    case 0:
      vCAST_SET_HISTORY_FLAGS ( 9, 0, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      break;
    case 1: {
      /* aecp_status_t geAECP_Initialize(aecp_driver_t * pDriver, uint16_t uwNumberObjects, const aecp_obj_entry_t * pObjectDictionary, uint16_t uwNumberTxMessages, const aecp_message_map_t * pTxMessageList, uint16_t uwNumberRxMessages, const aecp_message_map_t * pRxMessageList, boolean fUseChecksum) */
      vCAST_SET_HISTORY_FLAGS ( 9, 1, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_1 = 
      ( geAECP_Initialize(
        ( P_9_1_1 ),
        ( P_9_1_2 ),
        ( ((const struct aecp_obj_entry *)(P_9_1_3)) ),
        ( P_9_1_4 ),
        ( ((const struct aecp_message_map *)(P_9_1_5)) ),
        ( P_9_1_6 ),
        ( ((const struct aecp_message_map *)(P_9_1_7)) ),
        ( P_9_1_8 ) ) );
      break; }
    case 2: {
      /* aecp_status_t eAECP_EncodeMessage(aecp_data_t * pDriverData, char * pszMessage, uint16_t uwMaxChars, uint16_t uwMessageID, uint16_t uwOverrideNumberObjects) */
      vCAST_SET_HISTORY_FLAGS ( 9, 2, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_2 = 
      ( eAECP_EncodeMessage(
        ( P_9_2_1 ),
        ( P_9_2_2 ),
        ( P_9_2_3 ),
        ( P_9_2_4 ),
        ( P_9_2_5 ) ) );
      break; }
    case 3: {
      /* uint16_t uwAECP_EncodeObjectData(char * pszMessage, uint16_t uwMaxChars, const aecp_obj_entry_t * pObjectEntry) */
      vCAST_SET_HISTORY_FLAGS ( 9, 3, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_3 = 
      ( uwAECP_EncodeObjectData(
        ( P_9_3_1 ),
        ( P_9_3_2 ),
        ( ((const struct aecp_obj_entry *)(P_9_3_3)) ) ) );
      break; }
    case 4: {
      /* aecp_status_t eAECP_DecodeMessage(aecp_data_t * pDriverData, char * pszMessage) */
      vCAST_SET_HISTORY_FLAGS ( 9, 4, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_4 = 
      ( eAECP_DecodeMessage(
        ( P_9_4_1 ),
        ( P_9_4_2 ) ) );
      break; }
    case 5: {
      /* uint16_t uwAECP_EncodeHexBlock(char * pszMessage, uint16_t uwMaxChars, void * pvData, uint8_t ubNumBytes, aecp_data_types_t eDataType) */
      vCAST_SET_HISTORY_FLAGS ( 9, 5, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_5 = 
      ( uwAECP_EncodeHexBlock(
        ( P_9_5_1 ),
        ( P_9_5_2 ),
        ( P_9_5_3 ),
        ( P_9_5_4 ),
        ( P_9_5_5 ) ) );
      break; }
    case 6: {
      /* uint16_t uwAECP_DecodeHexBlock(char * pszMessage, void * pvData, uint8_t ubNumBytes, aecp_data_types_t eDataType) */
      vCAST_SET_HISTORY_FLAGS ( 9, 6, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_6 = 
      ( uwAECP_DecodeHexBlock(
        ( P_9_6_1 ),
        ( P_9_6_2 ),
        ( P_9_6_3 ),
        ( P_9_6_4 ) ) );
      break; }
    case 7: {
      /* uint16_t uwAECP_AddCharacter(char * pszMessage, uint16_t uwMaxChars, char chCharacter) */
      vCAST_SET_HISTORY_FLAGS ( 9, 7, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_7 = 
      ( uwAECP_AddCharacter(
        ( P_9_7_1 ),
        ( P_9_7_2 ),
        ( P_9_7_3 ) ) );
      break; }
    case 8: {
      /* uint16_t uwAECP_HexToAscii(char * pszMessage, uint8_t * pubData) */
      vCAST_SET_HISTORY_FLAGS ( 9, 8, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_8 = 
      ( uwAECP_HexToAscii(
        ( P_9_8_1 ),
        ( P_9_8_2 ) ) );
      break; }
    case 9: {
      /* uint16_t uwAECP_AsciiToHex(char * pszMessage, uint8_t * pubData) */
      vCAST_SET_HISTORY_FLAGS ( 9, 9, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_9 = 
      ( uwAECP_AsciiToHex(
        ( P_9_9_1 ),
        ( P_9_9_2 ) ) );
      break; }
    case 10: {
      /* const aecp_obj_entry_t *pAECP_GetOjectDictionaryEntry(aecp_data_t * pDriverData, uint16_t uwIndex, uint8_t ubSubindex) */
      vCAST_SET_HISTORY_FLAGS ( 9, 10, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_10 = 
      ((struct aecp_obj_entry *)(( pAECP_GetOjectDictionaryEntry(
        ( P_9_10_1 ),
        ( P_9_10_2 ),
        ( P_9_10_3 ) ) )));
      break; }
    case 11: {
      /* const aecp_message_map_t *pAECP_GetMessageMapEntry(const aecp_message_map_t * pMessageList, uint16_t uwListLength, uint16_t uwID) */
      vCAST_SET_HISTORY_FLAGS ( 9, 11, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_11 = 
      ((struct aecp_message_map *)(( pAECP_GetMessageMapEntry(
        ( ((const struct aecp_message_map *)(P_9_11_1)) ),
        ( P_9_11_2 ),
        ( P_9_11_3 ) ) )));
      break; }
    case 12: {
      /* uint16_t uwAECP_DecodeObjectData(char * pszMessage, const aecp_obj_entry_t * pObjectEntry) */
      vCAST_SET_HISTORY_FLAGS ( 9, 12, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_12 = 
      ( uwAECP_DecodeObjectData(
        ( P_9_12_1 ),
        ( ((const struct aecp_obj_entry *)(P_9_12_2)) ) ) );
      break; }
    case 13: {
      /* uint16_t uwAECP_DecodeString(char * pszMessage, char * pszDest) */
      vCAST_SET_HISTORY_FLAGS ( 9, 13, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_13 = 
      ( uwAECP_DecodeString(
        ( P_9_13_1 ),
        ( P_9_13_2 ) ) );
      break; }
    case 14: {
      /* uint8_t ubAECP_CalcChecksum(char * pszMessage, uint16_t uwCharCount) */
      vCAST_SET_HISTORY_FLAGS ( 9, 14, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_14 = 
      ( ubAECP_CalcChecksum(
        ( P_9_14_1 ),
        ( P_9_14_2 ) ) );
      break; }
    case 15: {
      /* boolean fAECP_LookupObject(aecp_data_t * pDriverData, void * pvAddress, aecp_object_t * pObject) */
      vCAST_SET_HISTORY_FLAGS ( 9, 15, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_15 = 
      ( fAECP_LookupObject(
        ( P_9_15_1 ),
        ( P_9_15_2 ),
        ( P_9_15_3 ) ) );
      break; }
    case 16: {
      /* char *gpszAECP_GetInitializationStatus(aecp_status_t eStatus) */
      vCAST_SET_HISTORY_FLAGS ( 9, 16, VC_EVENT_FLAGS, VC_SLOT_DESCR );
      R_9_16 = 
      ( gpszAECP_GetInitializationStatus(
        ( P_9_16_1 ) ) );
      break; }
    default:
      vectorcast_print_string("ERROR: Internal Tool Error\n");
      break;
  } /* switch */
}

void VCAST_SBF_9( int VC_SUBPROGRAM ) {
  switch( VC_SUBPROGRAM ) {
    case 1: {
      SBF_9_1 = 0;
      break; }
    case 2: {
      SBF_9_2 = 0;
      break; }
    case 3: {
      SBF_9_3 = 0;
      break; }
    case 4: {
      SBF_9_4 = 0;
      break; }
    case 5: {
      SBF_9_5 = 0;
      break; }
    case 6: {
      SBF_9_6 = 0;
      break; }
    case 7: {
      SBF_9_7 = 0;
      break; }
    case 8: {
      SBF_9_8 = 0;
      break; }
    case 9: {
      SBF_9_9 = 0;
      break; }
    case 10: {
      SBF_9_10 = 0;
      break; }
    case 11: {
      SBF_9_11 = 0;
      break; }
    case 12: {
      SBF_9_12 = 0;
      break; }
    case 13: {
      SBF_9_13 = 0;
      break; }
    case 14: {
      SBF_9_14 = 0;
      break; }
    case 15: {
      SBF_9_15 = 0;
      break; }
    case 16: {
      SBF_9_16 = 0;
      break; }
    default:
      break;
  } /* switch */
}
#include "vcast_ti_decls_9.h"
void VCAST_RUN_DATA_IF_9( int VCAST_SUB_INDEX, int VCAST_PARAM_INDEX ) {
  switch ( VCAST_SUB_INDEX ) {
    case 0: /* for global objects */
      switch( VCAST_PARAM_INDEX ) {
        default:
          vCAST_TOOL_ERROR = vCAST_true;
          break;
      } /* switch( VCAST_PARAM_INDEX ) */
      break; /* case 0 (global objects) */
    case 1: /* function geAECP_Initialize */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_3 ( &(P_9_1_1));
          break;
        case 2:
          VCAST_TI_9_16 ( &(P_9_1_2));
          break;
        case 3:
          VCAST_TI_9_2 ( &(P_9_1_3));
          break;
        case 4:
          VCAST_TI_9_16 ( &(P_9_1_4));
          break;
        case 5:
          VCAST_TI_9_1 ( &(P_9_1_5));
          break;
        case 6:
          VCAST_TI_9_16 ( &(P_9_1_6));
          break;
        case 7:
          VCAST_TI_9_1 ( &(P_9_1_7));
          break;
        case 8:
          VCAST_TI_8_5 ( &(P_9_1_8));
          break;
        case 9:
          VCAST_TI_9_17 ( &(R_9_1));
          break;
        case 10:
          VCAST_TI_SBF_OBJECT( &SBF_9_1 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function geAECP_Initialize */
    case 2: /* function eAECP_EncodeMessage */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_7 ( &(P_9_2_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_9_2_2));
          break;
        case 3:
          VCAST_TI_9_16 ( &(P_9_2_3));
          break;
        case 4:
          VCAST_TI_9_16 ( &(P_9_2_4));
          break;
        case 5:
          VCAST_TI_9_16 ( &(P_9_2_5));
          break;
        case 6:
          VCAST_TI_9_17 ( &(R_9_2));
          break;
        case 7:
          VCAST_TI_SBF_OBJECT( &SBF_9_2 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function eAECP_EncodeMessage */
    case 3: /* function uwAECP_EncodeObjectData */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_9_3_1));
          break;
        case 2:
          VCAST_TI_9_16 ( &(P_9_3_2));
          break;
        case 3:
          VCAST_TI_9_2 ( &(P_9_3_3));
          break;
        case 4:
          VCAST_TI_9_16 ( &(R_9_3));
          break;
        case 5:
          VCAST_TI_SBF_OBJECT( &SBF_9_3 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function uwAECP_EncodeObjectData */
    case 4: /* function eAECP_DecodeMessage */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_7 ( &(P_9_4_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_9_4_2));
          break;
        case 3:
          VCAST_TI_9_17 ( &(R_9_4));
          break;
        case 4:
          VCAST_TI_SBF_OBJECT( &SBF_9_4 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function eAECP_DecodeMessage */
    case 5: /* function uwAECP_EncodeHexBlock */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_9_5_1));
          break;
        case 2:
          VCAST_TI_9_16 ( &(P_9_5_2));
          break;
        case 3:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_9_5_3_memsize );
          VCAST_TI_9_11 ( &(P_9_5_3));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 4:
          VCAST_TI_8_5 ( &(P_9_5_4));
          break;
        case 5:
          VCAST_TI_9_19 ( &(P_9_5_5));
          break;
        case 6:
          VCAST_TI_9_16 ( &(R_9_5));
          break;
        case 7:
          VCAST_TI_SBF_OBJECT( &SBF_9_5 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function uwAECP_EncodeHexBlock */
    case 6: /* function uwAECP_DecodeHexBlock */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_9_6_1));
          break;
        case 2:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_9_6_2_memsize );
          VCAST_TI_9_11 ( &(P_9_6_2));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 3:
          VCAST_TI_8_5 ( &(P_9_6_3));
          break;
        case 4:
          VCAST_TI_9_19 ( &(P_9_6_4));
          break;
        case 5:
          VCAST_TI_9_16 ( &(R_9_6));
          break;
        case 6:
          VCAST_TI_SBF_OBJECT( &SBF_9_6 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function uwAECP_DecodeHexBlock */
    case 7: /* function uwAECP_AddCharacter */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_9_7_1));
          break;
        case 2:
          VCAST_TI_9_16 ( &(P_9_7_2));
          break;
        case 3:
          VCAST_TI_8_4 ( &(P_9_7_3));
          break;
        case 4:
          VCAST_TI_9_16 ( &(R_9_7));
          break;
        case 5:
          VCAST_TI_SBF_OBJECT( &SBF_9_7 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function uwAECP_AddCharacter */
    case 8: /* function uwAECP_HexToAscii */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_9_8_1));
          break;
        case 2:
          VCAST_TI_9_13 ( &(P_9_8_2));
          break;
        case 3:
          VCAST_TI_9_16 ( &(R_9_8));
          break;
        case 4:
          VCAST_TI_SBF_OBJECT( &SBF_9_8 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function uwAECP_HexToAscii */
    case 9: /* function uwAECP_AsciiToHex */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_9_9_1));
          break;
        case 2:
          VCAST_TI_9_13 ( &(P_9_9_2));
          break;
        case 3:
          VCAST_TI_9_16 ( &(R_9_9));
          break;
        case 4:
          VCAST_TI_SBF_OBJECT( &SBF_9_9 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function uwAECP_AsciiToHex */
    case 10: /* function pAECP_GetOjectDictionaryEntry */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_7 ( &(P_9_10_1));
          break;
        case 2:
          VCAST_TI_9_16 ( &(P_9_10_2));
          break;
        case 3:
          VCAST_TI_8_5 ( &(P_9_10_3));
          break;
        case 4:
          VCAST_TI_9_2 ( &(R_9_10));
          break;
        case 5:
          VCAST_TI_SBF_OBJECT( &SBF_9_10 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function pAECP_GetOjectDictionaryEntry */
    case 11: /* function pAECP_GetMessageMapEntry */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_1 ( &(P_9_11_1));
          break;
        case 2:
          VCAST_TI_9_16 ( &(P_9_11_2));
          break;
        case 3:
          VCAST_TI_9_16 ( &(P_9_11_3));
          break;
        case 4:
          VCAST_TI_9_1 ( &(R_9_11));
          break;
        case 5:
          VCAST_TI_SBF_OBJECT( &SBF_9_11 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function pAECP_GetMessageMapEntry */
    case 12: /* function uwAECP_DecodeObjectData */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_9_12_1));
          break;
        case 2:
          VCAST_TI_9_2 ( &(P_9_12_2));
          break;
        case 3:
          VCAST_TI_9_16 ( &(R_9_12));
          break;
        case 4:
          VCAST_TI_SBF_OBJECT( &SBF_9_12 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function uwAECP_DecodeObjectData */
    case 13: /* function uwAECP_DecodeString */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_9_13_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(P_9_13_2));
          break;
        case 3:
          VCAST_TI_9_16 ( &(R_9_13));
          break;
        case 4:
          VCAST_TI_SBF_OBJECT( &SBF_9_13 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function uwAECP_DecodeString */
    case 14: /* function ubAECP_CalcChecksum */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_8 ( &(P_9_14_1));
          break;
        case 2:
          VCAST_TI_9_16 ( &(P_9_14_2));
          break;
        case 3:
          VCAST_TI_8_5 ( &(R_9_14));
          break;
        case 4:
          VCAST_TI_SBF_OBJECT( &SBF_9_14 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function ubAECP_CalcChecksum */
    case 15: /* function fAECP_LookupObject */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_7 ( &(P_9_15_1));
          break;
        case 2:
          if( vCAST_COMMAND == vCAST_SET_VAL )
            VCAST_SET_GLOBAL_SIZE( &P_9_15_2_memsize );
          VCAST_TI_9_11 ( &(P_9_15_2));
          VCAST_SET_GLOBAL_SIZE( VCAST_NULL );
          break;
        case 3:
          VCAST_TI_9_12 ( &(P_9_15_3));
          break;
        case 4:
          VCAST_TI_8_5 ( &(R_9_15));
          break;
        case 5:
          VCAST_TI_SBF_OBJECT( &SBF_9_15 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function fAECP_LookupObject */
    case 16: /* function gpszAECP_GetInitializationStatus */
      switch ( VCAST_PARAM_INDEX ) {
        case 1:
          VCAST_TI_9_17 ( &(P_9_16_1));
          break;
        case 2:
          VCAST_TI_9_8 ( &(R_9_16));
          break;
        case 3:
          VCAST_TI_SBF_OBJECT( &SBF_9_16 );
          break;
      } /* switch ( VCAST_PARAM_INDEX ) */
      break; /* function gpszAECP_GetInitializationStatus */
    default:
      vCAST_TOOL_ERROR = vCAST_true;
      break;
  } /* switch ( VCAST_SUB_INDEX ) */
}


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_3 ( struct aecp_driver **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_3 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_3 ( struct aecp_driver **vcast_param ) 
{
  {
    int VCAST_TI_9_3_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_9_3_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_9_3_memory_ptr = (void**)vcast_param;
            *VCAST_TI_9_3_memory_ptr = (void*)VCAST_malloc(VCAST_TI_9_3_array_size*(sizeof(struct aecp_driver )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_9_3_array_size*(sizeof(struct aecp_driver )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_9_3_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_9_3_index = vcast_get_param();
          VCAST_TI_9_29 ( &((*vcast_param)[VCAST_TI_9_3_index]));
        }
      }
    }
  }
} /* end VCAST_TI_9_3 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An integer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_16 ( unsigned short *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_16 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_16 ( unsigned short *vcast_param ) 
{
  switch (vCAST_COMMAND) {
    case vCAST_PRINT :
      if ( vcast_param == 0)
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_unsigned_short(vCAST_OUTPUT_FILE, *vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      break;
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL :
    *vcast_param = ( unsigned short  ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL :
    *vcast_param = USHRT_MIN;
    break;
  case vCAST_MID_VAL :
    *vcast_param = (USHRT_MIN / 2) + (USHRT_MAX / 2);
    break;
  case vCAST_LAST_VAL :
    *vcast_param = USHRT_MAX;
    break;
  case vCAST_MIN_MINUS_1_VAL :
    *vcast_param = USHRT_MIN;
    *vcast_param = *vcast_param - 1;
    break;
  case vCAST_MAX_PLUS_1_VAL :
    *vcast_param = USHRT_MAX;
    *vcast_param = *vcast_param + 1;
    break;
  case vCAST_ZERO_VAL :
    *vcast_param = 0;
    break;
  default:
    break;
} /* end switch */
} /* end VCAST_TI_9_16 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_2 ( struct aecp_obj_entry **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_2 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_2 ( struct aecp_obj_entry **vcast_param ) 
{
  {
    int VCAST_TI_9_2_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_9_2_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_9_2_memory_ptr = (void**)vcast_param;
            *VCAST_TI_9_2_memory_ptr = (void*)VCAST_malloc(VCAST_TI_9_2_array_size*(sizeof(struct aecp_obj_entry )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_9_2_array_size*(sizeof(struct aecp_obj_entry )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_9_2_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_9_2_index = vcast_get_param();
          VCAST_TI_9_21 ( &((*vcast_param)[VCAST_TI_9_2_index]));
        }
      }
    }
  }
} /* end VCAST_TI_9_2 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_1 ( struct aecp_message_map **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_1 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_1 ( struct aecp_message_map **vcast_param ) 
{
  {
    int VCAST_TI_9_1_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_9_1_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_9_1_memory_ptr = (void**)vcast_param;
            *VCAST_TI_9_1_memory_ptr = (void*)VCAST_malloc(VCAST_TI_9_1_array_size*(sizeof(struct aecp_message_map )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_9_1_array_size*(sizeof(struct aecp_message_map )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_9_1_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_9_1_index = vcast_get_param();
          VCAST_TI_9_22 ( &((*vcast_param)[VCAST_TI_9_1_index]));
        }
      }
    }
  }
} /* end VCAST_TI_9_1 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_17 ( enum aecp_status *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_17 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_17 ( enum aecp_status *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum aecp_status ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = AECP_SUCCESS;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = AECP_RX_PARSE_ERROR;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_9_17 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_7 ( struct aecp_data **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_7 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_7 ( struct aecp_data **vcast_param ) 
{
  {
    int VCAST_TI_9_7_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_9_7_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_9_7_memory_ptr = (void**)vcast_param;
            *VCAST_TI_9_7_memory_ptr = (void*)VCAST_malloc(VCAST_TI_9_7_array_size*(sizeof(struct aecp_data )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_9_7_array_size*(sizeof(struct aecp_data )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_9_7_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_9_7_index = vcast_get_param();
          VCAST_TI_9_24 ( &((*vcast_param)[VCAST_TI_9_7_index]));
        }
      }
    }
  }
} /* end VCAST_TI_9_7 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_8 ( char **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_8 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_8 ( char **vcast_param ) 
{
  {
    int VCAST_TI_9_8_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_9_8_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_9_8_memory_ptr = (void**)vcast_param;
            *VCAST_TI_9_8_memory_ptr = (void*)VCAST_malloc(VCAST_TI_9_8_array_size*(sizeof(char )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_9_8_array_size*(sizeof(char )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_9_8_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          if (VCAST_FIND_INDEX() == -1 )
            VCAST_TI_STRING ( (char**)vcast_param, sizeof ( vcast_param ), 0,-1);
          else {
            VCAST_TI_9_8_index = vcast_get_param();
            VCAST_TI_8_4 ( &((*vcast_param)[VCAST_TI_9_8_index]));
          }
        }
      }
    }
  }
} /* end VCAST_TI_9_8 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_11 ( void **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_11 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_11 ( void **vcast_param ) 
{
  {
    int VCAST_TI_9_11_object_index = vcast_get_param();
    if (( vCAST_COMMAND == vCAST_PRINT ) && (*vcast_param == NULL)) {
      vectorcast_fprint_string (vCAST_OUTPUT_FILE, "NULL\n");
      return;
    }
    switch ( VCAST_TI_9_11_object_index ) {
      case 0: {
        if ( vCAST_COMMAND == vCAST_PRINT )
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "##ADDRESS##\n");
        else if ( vCAST_COMMAND == vCAST_SET_VAL )
          *vcast_param = (void*)NULL;
        break;
      } /* end case 0 */
      case 1:{
        if ( vCAST_COMMAND == vCAST_SET_VAL ) {
          *vcast_param = (void*) &VECTORCAST_CHAR1;
          if ( VCAST_GET_GLOBAL_SIZE() )
            *VCAST_GET_GLOBAL_SIZE() = sizeof (VECTORCAST_CHAR1);
        } else if ( vCAST_COMMAND == vCAST_PRINT ) {
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "VECTORCAST_CHAR1\n");
          VCAST_memcpy(&VECTORCAST_CHAR1, (void *)*vcast_param, sizeof (VECTORCAST_CHAR1));
        } /* end else if */
        break;
      } /* end case 1 */
      case 2:{
        if ( vCAST_COMMAND == vCAST_SET_VAL ) {
          *vcast_param = (void*) &VECTORCAST_INT1;
          if ( VCAST_GET_GLOBAL_SIZE() )
            *VCAST_GET_GLOBAL_SIZE() = sizeof (VECTORCAST_INT1);
        } else if ( vCAST_COMMAND == vCAST_PRINT ) {
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "VECTORCAST_INT1\n");
          VCAST_memcpy(&VECTORCAST_INT1, (void *)*vcast_param, sizeof (VECTORCAST_INT1));
        } /* end else if */
        break;
      } /* end case 2 */
      case 3:{
        if ( vCAST_COMMAND == vCAST_SET_VAL ) {
          *vcast_param = (void*) &VECTORCAST_INT2;
          if ( VCAST_GET_GLOBAL_SIZE() )
            *VCAST_GET_GLOBAL_SIZE() = sizeof (VECTORCAST_INT2);
        } else if ( vCAST_COMMAND == vCAST_PRINT ) {
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "VECTORCAST_INT2\n");
          VCAST_memcpy(&VECTORCAST_INT2, (void *)*vcast_param, sizeof (VECTORCAST_INT2));
        } /* end else if */
        break;
      } /* end case 3 */
      case 4:{
        if ( vCAST_COMMAND == vCAST_SET_VAL ) {
          *vcast_param = (void*) &VECTORCAST_INT3;
          if ( VCAST_GET_GLOBAL_SIZE() )
            *VCAST_GET_GLOBAL_SIZE() = sizeof (VECTORCAST_INT3);
        } else if ( vCAST_COMMAND == vCAST_PRINT ) {
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "VECTORCAST_INT3\n");
          VCAST_memcpy(&VECTORCAST_INT3, (void *)*vcast_param, sizeof (VECTORCAST_INT3));
        } /* end else if */
        break;
      } /* end case 4 */
      case 5:{
        if ( vCAST_COMMAND == vCAST_SET_VAL ) {
          *vcast_param = (void*) &VECTORCAST_UINT8;
          if ( VCAST_GET_GLOBAL_SIZE() )
            *VCAST_GET_GLOBAL_SIZE() = sizeof (VECTORCAST_UINT8);
        } else if ( vCAST_COMMAND == vCAST_PRINT ) {
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "VECTORCAST_UINT8\n");
          VCAST_memcpy(&VECTORCAST_UINT8, (void *)*vcast_param, sizeof (VECTORCAST_UINT8));
        } /* end else if */
        break;
      } /* end case 5 */
      case 6:{
        if ( vCAST_COMMAND == vCAST_SET_VAL ) {
          *vcast_param = (void*) &VECTORCAST_ULONG;
          if ( VCAST_GET_GLOBAL_SIZE() )
            *VCAST_GET_GLOBAL_SIZE() = sizeof (VECTORCAST_ULONG);
        } else if ( vCAST_COMMAND == vCAST_PRINT ) {
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "VECTORCAST_ULONG\n");
          VCAST_memcpy(&VECTORCAST_ULONG, (void *)*vcast_param, sizeof (VECTORCAST_ULONG));
        } /* end else if */
        break;
      } /* end case 6 */
      case 7:{
        if ( vCAST_COMMAND == vCAST_SET_VAL ) {
          *vcast_param = (void*) &VECTORCAST_FLT1;
          if ( VCAST_GET_GLOBAL_SIZE() )
            *VCAST_GET_GLOBAL_SIZE() = sizeof (VECTORCAST_FLT1);
        } else if ( vCAST_COMMAND == vCAST_PRINT ) {
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "VECTORCAST_FLT1\n");
          VCAST_memcpy(&VECTORCAST_FLT1, (void *)*vcast_param, sizeof (VECTORCAST_FLT1));
        } /* end else if */
        break;
      } /* end case 7 */
      case 8:{
        if ( vCAST_COMMAND == vCAST_SET_VAL ) {
          *vcast_param = (void*) &VECTORCAST_STR1;
          if ( VCAST_GET_GLOBAL_SIZE() )
            *VCAST_GET_GLOBAL_SIZE() = sizeof (VECTORCAST_STR1);
        } else if ( vCAST_COMMAND == vCAST_PRINT ) {
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "VECTORCAST_STR1\n");
          VCAST_memcpy(&VECTORCAST_STR1, (void *)*vcast_param, sizeof (VECTORCAST_STR1));
        } /* end else if */
        break;
      } /* end case 8 */
      case 9:{
        if ( vCAST_COMMAND == vCAST_SET_VAL ) {
          *vcast_param = (void*) &VECTORCAST_BUFFER;
          if ( VCAST_GET_GLOBAL_SIZE() )
            *VCAST_GET_GLOBAL_SIZE() = sizeof (VECTORCAST_BUFFER);
        } else if ( vCAST_COMMAND == vCAST_PRINT ) {
          vectorcast_fprint_string (vCAST_OUTPUT_FILE, "VECTORCAST_BUFFER\n");
          VCAST_memcpy(&VECTORCAST_BUFFER, (void *)*vcast_param, sizeof (VECTORCAST_BUFFER));
        } /* end else if */
        break;
      } /* end case 9 */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */

    if ( VCAST_TI_9_11_object_index > 0 )
      vCAST_GLOBALS_TOUCHED[VCAST_TI_9_11_object_index-1] = vCAST_true;
  }
} /* end VCAST_TI_9_11 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_19 ( enum aecp_data_types *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_19 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_19 ( enum aecp_data_types *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum aecp_data_types ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = AECP_CHAR;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = AECP_STRING;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_9_19 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_13 ( unsigned char **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_13 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_13 ( unsigned char **vcast_param ) 
{
  {
    int VCAST_TI_9_13_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_9_13_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_9_13_memory_ptr = (void**)vcast_param;
            *VCAST_TI_9_13_memory_ptr = (void*)VCAST_malloc(VCAST_TI_9_13_array_size*(sizeof(unsigned char )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_9_13_array_size*(sizeof(unsigned char )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_9_13_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          if (VCAST_FIND_INDEX() == -1 )
            VCAST_TI_STRING ( (char**)vcast_param, sizeof ( vcast_param ), 0,-1);
          else {
            VCAST_TI_9_13_index = vcast_get_param();
            VCAST_TI_8_5 ( &((*vcast_param)[VCAST_TI_9_13_index]));
          }
        }
      }
    }
  }
} /* end VCAST_TI_9_13 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_12 ( struct aecp_object **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_12 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_12 ( struct aecp_object **vcast_param ) 
{
  {
    int VCAST_TI_9_12_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_9_12_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_9_12_memory_ptr = (void**)vcast_param;
            *VCAST_TI_9_12_memory_ptr = (void*)VCAST_malloc(VCAST_TI_9_12_array_size*(sizeof(struct aecp_object )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_9_12_array_size*(sizeof(struct aecp_object )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_9_12_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_9_12_index = vcast_get_param();
          VCAST_TI_9_20 ( &((*vcast_param)[VCAST_TI_9_12_index]));
        }
      }
    }
  }
} /* end VCAST_TI_9_12 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_29 ( struct aecp_driver *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_29 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_29 ( struct aecp_driver *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->Data */
      case 1: { 
        VCAST_TI_9_24 ( ((struct aecp_data *)(&(vcast_param->Data))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->Procedures */
      case 2: { 
        VCAST_TI_9_25 ( ((struct aecp_procedures *)(&(vcast_param->Procedures))));
        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_9_29 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_21 ( struct aecp_obj_entry *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_21 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_21 ( struct aecp_obj_entry *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->Object */
      case 1: { 
        VCAST_TI_9_20 ( ((struct aecp_object *)(&(vcast_param->Object))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubSize */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubSize))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->eType */
      case 3: { 
        VCAST_TI_9_19 ( ((enum aecp_data_types *)(&(vcast_param->eType))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->pvAddress */
      case 4: { 
        VCAST_TI_9_11 ( ((void **)(&(vcast_param->pvAddress))));
        break; /* end case 4*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_9_21 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_22 ( struct aecp_message_map *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_22 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_22 ( struct aecp_message_map *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwID */
      case 1: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwID))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->eMessageType */
      case 2: { 
        VCAST_TI_9_18 ( ((enum aecp_msg_type *)(&(vcast_param->eMessageType))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->puwNumberObjects */
      case 3: { 
        VCAST_TI_9_9 ( ((unsigned short **)(&(vcast_param->puwNumberObjects))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->puwMaxNumberObjects */
      case 4: { 
vcast_not_supported();
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->pObjectList */
      case 5: { 
        VCAST_TI_9_12 ( ((struct aecp_object **)(&(vcast_param->pObjectList))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->vCallback */
      case 6: { 
        VCAST_TI_9_14 ( ((void (**)(uint16_t uwID, char * pszString))(&(vcast_param->vCallback))));
        break; /* end case 6*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_9_22 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_24 ( struct aecp_data *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_24 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_24 ( struct aecp_data *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->fInitialized */
      case 1: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fInitialized))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->fUseChecksum */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->fUseChecksum))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->uwNumberObjects */
      case 3: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwNumberObjects))));
        break; /* end case 3*/
      } /* end case */
      /* Setting member variable vcast_param->pObjectDictionary */
      case 4: { 
vcast_not_supported();
        break; /* end case 4*/
      } /* end case */
      /* Setting member variable vcast_param->uwNumberTxMessages */
      case 5: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwNumberTxMessages))));
        break; /* end case 5*/
      } /* end case */
      /* Setting member variable vcast_param->pTxMessageList */
      case 6: { 
vcast_not_supported();
        break; /* end case 6*/
      } /* end case */
      /* Setting member variable vcast_param->uwNumberRxMessages */
      case 7: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwNumberRxMessages))));
        break; /* end case 7*/
      } /* end case */
      /* Setting member variable vcast_param->pRxMessageList */
      case 8: { 
vcast_not_supported();
        break; /* end case 8*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_9_24 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_20 ( struct aecp_object *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_20 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_20 ( struct aecp_object *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->uwIndex */
      case 1: { 
        VCAST_TI_9_16 ( ((unsigned short *)(&(vcast_param->uwIndex))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->ubSubindex */
      case 2: { 
        VCAST_TI_8_5 ( ((unsigned char *)(&(vcast_param->ubSubindex))));
        break; /* end case 2*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_9_20 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A struct */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_25 ( struct aecp_procedures *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_25 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_25 ( struct aecp_procedures *vcast_param ) 
{
  {
    switch ( vcast_get_param () ) { /* Choose field member */
      /* Setting member variable vcast_param->eEncodeMessage */
      case 1: { 
        VCAST_TI_9_4 ( ((aecp_status_t (**)(aecp_data_t * pDriverData, char * pszMessage, uint16_t uwMaxCharacters, uint16_t uwMessageID, uint16_t uwNumObjects))(&(vcast_param->eEncodeMessage))));
        break; /* end case 1*/
      } /* end case */
      /* Setting member variable vcast_param->eDecodeMessage */
      case 2: { 
        VCAST_TI_9_5 ( ((aecp_status_t (**)(aecp_data_t * pDriverData, char * pszMessage))(&(vcast_param->eDecodeMessage))));
        break; /* end case 2*/
      } /* end case */
      /* Setting member variable vcast_param->fLookupObject */
      case 3: { 
        VCAST_TI_9_6 ( ((boolean (**)(aecp_data_t * pDriverData, void * pvAddress, aecp_object_t * pObject))(&(vcast_param->fLookupObject))));
        break; /* end case 3*/
      } /* end case */
      default:
        vCAST_TOOL_ERROR = vCAST_true;
    } /* end switch */ 
  }
} /* end VCAST_TI_9_25 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* An enumeration */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_18 ( enum aecp_msg_type *vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_18 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_18 ( enum aecp_msg_type *vcast_param ) 
{
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT: {
      if ( vcast_param == 0 )
        vectorcast_fprint_string (vCAST_OUTPUT_FILE,"null\n");
      else {
        vectorcast_fprint_long_long(vCAST_OUTPUT_FILE, (VCAST_LONGEST_INT)*vcast_param);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      } /* end else */
      } /* end vCAST_PRINT block */
      break; /* end case vCAST_PRINT */
    case vCAST_KEEP_VAL:
      break; /* KEEP doesn't do anything */
  case vCAST_SET_VAL:
    *vcast_param = (enum aecp_msg_type ) vCAST_VALUE_INT;
    break;
  case vCAST_FIRST_VAL:
    *vcast_param = AECP_DATA_ONLY;
    break; /* end case vCAST_FIRST_VAL */
  case vCAST_LAST_VAL:
    *vcast_param = AECP_OBJS_WITH_DATA;
    break; /* end case vCAST_LAST_VAL */
  default:
    vCAST_TOOL_ERROR = vCAST_true;
    break; /* end case default */
} /* end switch */
} /* end VCAST_TI_9_18 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_9 ( unsigned short **vcast_param ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_9 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_9 ( unsigned short **vcast_param ) 
{
  {
    int VCAST_TI_9_9_index;
    if (((*vcast_param) == 0) && (vCAST_COMMAND != vCAST_ALLOCATE)){
      if ( vCAST_COMMAND == vCAST_PRINT )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"null\n");
    } else {
      if ( (vCAST_COMMAND_IS_MIN_MAX == vCAST_true) &&( VCAST_FIND_INDEX() < 0 ) ) {
        switch ( vCAST_COMMAND ) {
          case vCAST_PRINT     :
            vectorcast_fprint_string(vCAST_OUTPUT_FILE,"0\n");
          case vCAST_FIRST_VAL :
          case vCAST_LAST_VAL  :
          case vCAST_POS_INF_VAL  :
          case vCAST_NEG_INF_VAL  :
          case vCAST_NAN_VAL  :
            break;
          default :
            vCAST_TOOL_ERROR = vCAST_true;
        }
      } else {
        if (vCAST_COMMAND == vCAST_ALLOCATE && vcast_proc_handles_command(1)) {
          int VCAST_TI_9_9_array_size = (int) vCAST_VALUE;
          if (VCAST_FIND_INDEX() == -1) {
            void **VCAST_TI_9_9_memory_ptr = (void**)vcast_param;
            *VCAST_TI_9_9_memory_ptr = (void*)VCAST_malloc(VCAST_TI_9_9_array_size*(sizeof(unsigned short )));
            VCAST_memset((void*)*vcast_param, 0x0, VCAST_TI_9_9_array_size*(sizeof(unsigned short )));
#ifndef VCAST_NO_MALLOC
            VCAST_Add_Allocated_Data(*VCAST_TI_9_9_memory_ptr);
#endif
          }
        } else if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
          if (VCAST_FIND_INDEX() == -1)
            *vcast_param = 0;
        } else {
          VCAST_TI_9_9_index = vcast_get_param();
          VCAST_TI_9_16 ( &((*vcast_param)[VCAST_TI_9_9_index]));
        }
      }
    }
  }
} /* end VCAST_TI_9_9 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_14 ( void (**vcast_param)(uint16_t uwID, char * pszString) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_14 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_14 ( void (**vcast_param)(uint16_t uwID, char * pszString) ) 
{
  void (*vcast_local_ptr)(uint16_t uwID, char * pszString);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
      }
      break;
  }
} /* end VCAST_TI_9_14 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_4 ( aecp_status_t (**vcast_param)(aecp_data_t * pDriverData, char * pszMessage, uint16_t uwMaxCharacters, uint16_t uwMessageID, uint16_t uwNumObjects) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_4 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_4 ( aecp_status_t (**vcast_param)(aecp_data_t * pDriverData, char * pszMessage, uint16_t uwMaxCharacters, uint16_t uwMessageID, uint16_t uwNumObjects) ) 
{
  aecp_status_t (*vcast_local_ptr)(aecp_data_t * pDriverData, char * pszMessage, uint16_t uwMaxCharacters, uint16_t uwMessageID, uint16_t uwNumObjects);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else if ( vcast_local_ptr = eAECP_EncodeMessage, *vcast_param == vcast_local_ptr ) {
        vectorcast_fprint_integer (vCAST_OUTPUT_FILE, 0);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
        case 0:
          vcast_local_ptr = eAECP_EncodeMessage;
          *vcast_param = vcast_local_ptr;
          break;
      }
      break;
  }
} /* end VCAST_TI_9_4 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_5 ( aecp_status_t (**vcast_param)(aecp_data_t * pDriverData, char * pszMessage) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_5 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_5 ( aecp_status_t (**vcast_param)(aecp_data_t * pDriverData, char * pszMessage) ) 
{
  aecp_status_t (*vcast_local_ptr)(aecp_data_t * pDriverData, char * pszMessage);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else if ( vcast_local_ptr = eAECP_DecodeMessage, *vcast_param == vcast_local_ptr ) {
        vectorcast_fprint_integer (vCAST_OUTPUT_FILE, 0);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
        case 0:
          vcast_local_ptr = eAECP_DecodeMessage;
          *vcast_param = vcast_local_ptr;
          break;
      }
      break;
  }
} /* end VCAST_TI_9_5 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


/* A pointer */
#if (defined(VCAST_NO_TYPE_SUPPORT))
void VCAST_TI_9_6 ( boolean (**vcast_param)(aecp_data_t * pDriverData, void * pvAddress, aecp_object_t * pObject) ) 
{
  /* User code: type is not supported */
  vcast_not_supported();
} /* end VCAST_TI_9_6 */
#else /*(defined(VCAST_NO_TYPE_SUPPORT))*/
void VCAST_TI_9_6 ( boolean (**vcast_param)(aecp_data_t * pDriverData, void * pvAddress, aecp_object_t * pObject) ) 
{
  boolean (*vcast_local_ptr)(aecp_data_t * pDriverData, void * pvAddress, aecp_object_t * pObject);
  switch ( vCAST_COMMAND ) {
    case vCAST_PRINT:
      if ( !*vcast_param )
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "<<null>>\n");
      else if ( vcast_local_ptr = fAECP_LookupObject, *vcast_param == vcast_local_ptr ) {
        vectorcast_fprint_integer (vCAST_OUTPUT_FILE, 0);
        vectorcast_fprint_string(vCAST_OUTPUT_FILE, "\n");
      }
      else
        vectorcast_fprint_string(vCAST_OUTPUT_FILE,"<<unknown>>\n");
      break;
    case vCAST_SET_VAL:
      if (vCAST_VALUE_NUL == vCAST_true && vcast_proc_handles_command(1)) {
        if (VCAST_FIND_INDEX() == -1)
          *vcast_param = 0;
        return;
      }
      switch ( (int) vCAST_VALUE ) {
        case 0:
          vcast_local_ptr = fAECP_LookupObject;
          *vcast_param = vcast_local_ptr;
          break;
      }
      break;
  }
} /* end VCAST_TI_9_6 */
#endif /*(defined(VCAST_NO_TYPE_SUPPORT))*/


#ifdef VCAST_PARADIGM_ADD_SEGMENT
#pragma new_codesegment(1)
#endif
void VCAST_TI_RANGE_DATA_9 ( void ) {
#define VCAST_TI_SCALAR_TYPE "NEW_SCALAR\n"
#define VCAST_TI_ARRAY_TYPE  "NEW_ARRAY\n"
  /* Range Data for TI (scalar) VCAST_TI_8_4 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"900012\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,CHAR_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(CHAR_MIN/2) + (CHAR_MAX/2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,CHAR_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_8_5 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"900021\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,UCHAR_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,(UCHAR_MIN / 2) + (UCHAR_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_integer (vCAST_OUTPUT_FILE,UCHAR_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  /* Range Data for TI (scalar) VCAST_TI_9_16 */
  vectorcast_fprint_string (vCAST_OUTPUT_FILE, VCAST_TI_SCALAR_TYPE );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"900022\n" );
  vectorcast_fprint_unsigned_short (vCAST_OUTPUT_FILE,USHRT_MIN );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_short (vCAST_OUTPUT_FILE,(USHRT_MIN / 2) + (USHRT_MAX / 2) );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
  vectorcast_fprint_unsigned_short (vCAST_OUTPUT_FILE,USHRT_MAX );
  vectorcast_fprint_string (vCAST_OUTPUT_FILE,"\n" );
}
/* Include the file which contains function implementations
for stub processing and value/expected user code */
#include "aecp_main_uc.c"

void vCAST_COMMON_STUB_PROC_9(
            int unitIndex,
            int subprogramIndex,
            int robjectIndex,
            int readEobjectData )
{
   vCAST_BEGIN_STUB_PROC_9(unitIndex, subprogramIndex);
   if ( robjectIndex )
      vCAST_READ_COMMAND_DATA_FOR_ONE_PARAM( unitIndex, subprogramIndex, robjectIndex );
   if ( readEobjectData )
      vCAST_READ_COMMAND_DATA_FOR_ONE_PARAM( unitIndex, subprogramIndex, 0 );
   vCAST_SET_HISTORY( unitIndex, subprogramIndex );
   vCAST_READ_COMMAND_DATA( vCAST_CURRENT_SLOT, unitIndex, subprogramIndex, vCAST_true, vCAST_false );
   vCAST_READ_COMMAND_DATA_FOR_USER_GLOBALS();
   vCAST_STUB_PROCESSING_9(unitIndex, subprogramIndex);
}
#endif /* VCAST_HEADER_EXPANSION */
