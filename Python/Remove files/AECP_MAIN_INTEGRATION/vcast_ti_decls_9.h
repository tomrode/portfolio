void VCAST_TI_9_14 ( void (**vcast_param)(uint16_t uwID, char * pszString) ) ;

void VCAST_TI_9_4 ( aecp_status_t (**vcast_param)(aecp_data_t * pDriverData, char * pszMessage, uint16_t uwMaxCharacters, uint16_t uwMessageID, uint16_t uwNumObjects) ) ;

void VCAST_TI_9_5 ( aecp_status_t (**vcast_param)(aecp_data_t * pDriverData, char * pszMessage) ) ;

void VCAST_TI_9_6 ( boolean (**vcast_param)(aecp_data_t * pDriverData, void * pvAddress, aecp_object_t * pObject) ) ;


#ifdef __cplusplus
extern "C" {
#endif
void VCAST_TI_8_4 ( char *vcast_param ) ;

void VCAST_TI_8_5 ( unsigned char *vcast_param ) ;

void VCAST_TI_9_1 ( struct aecp_message_map **vcast_param ) ;

void VCAST_TI_9_11 ( void **vcast_param ) ;

void VCAST_TI_9_12 ( struct aecp_object **vcast_param ) ;

void VCAST_TI_9_13 ( unsigned char **vcast_param ) ;

void VCAST_TI_9_16 ( unsigned short *vcast_param ) ;

void VCAST_TI_9_17 ( enum aecp_status *vcast_param ) ;

void VCAST_TI_9_18 ( enum aecp_msg_type *vcast_param ) ;

void VCAST_TI_9_19 ( enum aecp_data_types *vcast_param ) ;

void VCAST_TI_9_2 ( struct aecp_obj_entry **vcast_param ) ;

void VCAST_TI_9_20 ( struct aecp_object *vcast_param ) ;

void VCAST_TI_9_21 ( struct aecp_obj_entry *vcast_param ) ;

void VCAST_TI_9_22 ( struct aecp_message_map *vcast_param ) ;

void VCAST_TI_9_24 ( struct aecp_data *vcast_param ) ;

void VCAST_TI_9_25 ( struct aecp_procedures *vcast_param ) ;

void VCAST_TI_9_29 ( struct aecp_driver *vcast_param ) ;

void VCAST_TI_9_3 ( struct aecp_driver **vcast_param ) ;

void VCAST_TI_9_7 ( struct aecp_data **vcast_param ) ;

void VCAST_TI_9_8 ( char **vcast_param ) ;

void VCAST_TI_9_9 ( unsigned short **vcast_param ) ;


#ifdef __cplusplus
}
#endif
