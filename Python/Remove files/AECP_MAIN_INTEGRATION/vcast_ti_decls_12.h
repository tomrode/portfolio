
#ifdef __cplusplus
extern "C" {
#endif
void VCAST_TI_11_25 ( long *vcast_param ) ;

void VCAST_TI_11_31 ( signed int *vcast_param ) ;

void VCAST_TI_11_4 ( unsigned *vcast_param ) ;

void VCAST_TI_12_10 ( features_bus_t *vcast_param ) ;

void VCAST_TI_12_1000 ( enum alarm_tone_pattern *vcast_param ) ;

void VCAST_TI_12_101 ( SteeringType *vcast_param ) ;

void VCAST_TI_12_103 ( StrOpModeState *vcast_param ) ;

void VCAST_TI_12_1057 ( float vcast_param[15] ) ;

void VCAST_TI_12_1058 ( EnergySource_output_bus_t *vcast_param ) ;

void VCAST_TI_12_1060 ( function_active_output_bus_t *vcast_param ) ;

void VCAST_TI_12_1062 ( hyd_output_bus_t *vcast_param ) ;

void VCAST_TI_12_1064 ( steer_m2m_output_bus_t *vcast_param ) ;

void VCAST_TI_12_1066 ( sw_flag_output_bus_t *vcast_param ) ;

void VCAST_TI_12_1068 ( traction_output_bus_t *vcast_param ) ;

void VCAST_TI_12_107 ( TractionMotorState *vcast_param ) ;

void VCAST_TI_12_1070 ( vehicle_sro_word_bus_t *vcast_param ) ;

void VCAST_TI_12_1072 ( VehicleFB_output_bus_t *vcast_param ) ;

void VCAST_TI_12_1074 ( brake_analyzer_out_bus_t *vcast_param ) ;

void VCAST_TI_12_1076 ( steer_dx_out_bus_t *vcast_param ) ;

void VCAST_TI_12_1078 ( AutoCal_Mcc_Calib_bus_t *vcast_param ) ;

void VCAST_TI_12_1080 ( AutoCal_PvFormat_bus_t *vcast_param ) ;

void VCAST_TI_12_1082 ( AutoCal_DeFormat_bus_t *vcast_param ) ;

void VCAST_TI_12_1084 ( AutoCal_Steps_bus_t *vcast_param ) ;

void VCAST_TI_12_1086 ( AGV_output_bus_t *vcast_param ) ;

void VCAST_TI_12_1087 ( struct info1_model *vcast_param ) ;

void VCAST_TI_12_1088 ( struct fram_birthCert_data *vcast_param ) ;

void VCAST_TI_12_1089 ( char vcast_param[2U][160U] ) ;

void VCAST_TI_12_109 ( TractionState *vcast_param ) ;

void VCAST_TI_12_1090 ( char vcast_param[160U] ) ;

void VCAST_TI_12_1091 ( unsigned char vcast_param[2U] ) ;

void VCAST_TI_12_1092 ( unsigned char vcast_param[((764U - sizeof(uint8_t)) - sizeof(uint8_t) * 2U) - (sizeof(char) * 2U) * 160U] ) ;

void VCAST_TI_12_1093 ( struct fram_BirthCert *vcast_param ) ;

void VCAST_TI_12_1094 ( unsigned char vcast_param[8U - 4U] ) ;

void VCAST_TI_12_1095 ( char vcast_param[4U] ) ;

void VCAST_TI_12_1096 ( struct aecp_object vcast_param[4] ) ;

void VCAST_TI_12_1097 ( struct aecp_object vcast_param[34] ) ;

void VCAST_TI_12_1098 ( struct aecp_object vcast_param[1] ) ;

void VCAST_TI_12_1099 ( struct aecp_object vcast_param[50] ) ;

void VCAST_TI_12_11 ( struct imm_ips_data vcast_param[IPS_MAX_NUM] ) ;

void VCAST_TI_12_1102 ( struct aecp_object vcast_param[12] ) ;

void VCAST_TI_12_1103 ( struct aecp_object vcast_param[7] ) ;

void VCAST_TI_12_1104 ( struct aecp_object vcast_param[3] ) ;

void VCAST_TI_12_1105 ( struct aecp_object vcast_param[31U] ) ;

void VCAST_TI_12_1106 ( struct aecp_object vcast_param[2] ) ;

void VCAST_TI_12_1107 ( struct aecp_object vcast_param[8U][28U] ) ;

void VCAST_TI_12_1108 ( struct aecp_object vcast_param[28U] ) ;

void VCAST_TI_12_111 ( TravelAlarmType *vcast_param ) ;

void VCAST_TI_12_1110 ( struct aecp_object vcast_param[9] ) ;

void VCAST_TI_12_113 ( TruckModel *vcast_param ) ;

void VCAST_TI_12_117 ( VEL_TYPE *vcast_param ) ;

void VCAST_TI_12_119 ( ZONE_SWITCH_TYPE *vcast_param ) ;

void VCAST_TI_12_12 ( unsigned char vcast_param[8U] ) ;

void VCAST_TI_12_121 ( QPRA *vcast_param ) ;

void VCAST_TI_12_123 ( OperatorRemoteStatus *vcast_param ) ;

void VCAST_TI_12_125 ( WalkCoastObjectDetStatus *vcast_param ) ;

void VCAST_TI_12_127 ( ReachSenseType *vcast_param ) ;

void VCAST_TI_12_129 ( HoistDirectionBasedOnDelta *vcast_param ) ;

void VCAST_TI_12_132 ( CalibUnits *vcast_param ) ;

void VCAST_TI_12_134 ( CALIB_ACTION *vcast_param ) ;

void VCAST_TI_12_136 ( HighSpeedLowerType *vcast_param ) ;

void VCAST_TI_12_14 ( unsigned char vcast_param[IPS_MAX_NUM] ) ;

void VCAST_TI_12_146 ( CalibUIKeyInput *vcast_param ) ;

void VCAST_TI_12_152 ( SteerDirection *vcast_param ) ;

void VCAST_TI_12_154 ( WalkAlongStatus *vcast_param ) ;

void VCAST_TI_12_156 ( FAN_CONTROL *vcast_param ) ;

void VCAST_TI_12_158 ( Accy2Type *vcast_param ) ;

void VCAST_TI_12_160 ( BatteryBoxType *vcast_param ) ;

void VCAST_TI_12_162 ( EnergySource *vcast_param ) ;

void VCAST_TI_12_164 ( GuidanceType *vcast_param ) ;

void VCAST_TI_12_166 ( TPA_Type *vcast_param ) ;

void VCAST_TI_12_168 ( Use_tilt *vcast_param ) ;

void VCAST_TI_12_170 ( Use_Accy1 *vcast_param ) ;

void VCAST_TI_12_172 ( Use_Accy3 *vcast_param ) ;

void VCAST_TI_12_174 ( HiSpdLatched *vcast_param ) ;

void VCAST_TI_12_176 ( LoadSense *vcast_param ) ;

void VCAST_TI_12_178 ( Transport_option *vcast_param ) ;

void VCAST_TI_12_180 ( ChainSlackDetection *vcast_param ) ;

void VCAST_TI_12_182 ( UserHeightCutoutOption *vcast_param ) ;

void VCAST_TI_12_184 ( UserReachCutoutOption *vcast_param ) ;

void VCAST_TI_12_186 ( User360Select *vcast_param ) ;

void VCAST_TI_12_188 ( HSSAntiTieDownMode *vcast_param ) ;

void VCAST_TI_12_19 ( unsigned long long *vcast_param ) ;

void VCAST_TI_12_190 ( AGV_Mode *vcast_param ) ;

void VCAST_TI_12_192 ( StrobeType *vcast_param ) ;

void VCAST_TI_12_194 ( CDM_Option *vcast_param ) ;

void VCAST_TI_12_196 ( SideShiftPositionAssistOption *vcast_param ) ;

void VCAST_TI_12_198 ( OperatorControlOption *vcast_param ) ;

void VCAST_TI_12_20 ( unsigned short vcast_param[8U] ) ;

void VCAST_TI_12_200 ( SeatHeater *vcast_param ) ;

void VCAST_TI_12_202 ( TFD_presence *vcast_param ) ;

void VCAST_TI_12_204 ( BackRest_module *vcast_param ) ;

void VCAST_TI_12_206 ( KeylessPowerModule *vcast_param ) ;

void VCAST_TI_12_208 ( Tilt_Sense_type *vcast_param ) ;

void VCAST_TI_12_210 ( SpeedCutbackSRO *vcast_param ) ;

void VCAST_TI_12_212 ( test_output_t *vcast_param ) ;

void VCAST_TI_12_214 ( SpdCutTriggerType *vcast_param ) ;

void VCAST_TI_12_216 ( ForkHome *vcast_param ) ;

void VCAST_TI_12_218 ( AboveHeightLimit *vcast_param ) ;

void VCAST_TI_12_22 ( Rack_Status_bus_t *vcast_param ) ;

void VCAST_TI_12_220 ( LoadWheelBrakes *vcast_param ) ;

void VCAST_TI_12_222 ( SpeedCutbackFeature *vcast_param ) ;

void VCAST_TI_12_226 ( Battery_Voltage *vcast_param ) ;

void VCAST_TI_12_230 ( TPA_Pos *vcast_param ) ;

void VCAST_TI_12_232 ( SSP_Pos *vcast_param ) ;

void VCAST_TI_12_238 ( LaserLineOption *vcast_param ) ;

void VCAST_TI_12_240 ( BlueLightOption *vcast_param ) ;

void VCAST_TI_12_242 ( OptimizedCorneringSpeedCurve *vcast_param ) ;

void VCAST_TI_12_244 ( PerformanceCurve *vcast_param ) ;

void VCAST_TI_12_246 ( EstopChannels *vcast_param ) ;

void VCAST_TI_12_25 ( signed char *vcast_param ) ;

void VCAST_TI_12_250 ( BHM_Presence *vcast_param ) ;

void VCAST_TI_12_252 ( InhibitXpressLower *vcast_param ) ;

void VCAST_TI_12_254 ( EnergySource_EWSTypeIntegration *vcast_param ) ;

void VCAST_TI_12_256 ( TempSense *vcast_param ) ;

void VCAST_TI_12_258 ( FreezerHeater *vcast_param ) ;

void VCAST_TI_12_264 ( struct dem_report_record *vcast_param ) ;

void VCAST_TI_12_265 ( struct dem_report_structure *vcast_param ) ;

void VCAST_TI_12_27 ( signed short *vcast_param ) ;

void VCAST_TI_12_3 ( struct aecp_object vcast_param[8U] ) ;

void VCAST_TI_12_45 ( AuxHoistType *vcast_param ) ;

void VCAST_TI_12_479 ( float vcast_param[6] ) ;

void VCAST_TI_12_483 ( float vcast_param[7] ) ;

void VCAST_TI_12_49 ( CUTOUT_TYPE_TYPE *vcast_param ) ;

void VCAST_TI_12_497 ( float vcast_param[5] ) ;

void VCAST_TI_12_5 ( AutoCal_Cm_Calib_bus_t *vcast_param ) ;

void VCAST_TI_12_51 ( CUTOUT_ZONE_TYPE *vcast_param ) ;

void VCAST_TI_12_513 ( performance_bus_t *vcast_param ) ;

void VCAST_TI_12_57 ( ForkType *vcast_param ) ;

void VCAST_TI_12_59 ( HeightSenseType *vcast_param ) ;

void VCAST_TI_12_6 ( CalibMode *vcast_param ) ;

void VCAST_TI_12_61 ( LoadSenseType *vcast_param ) ;

void VCAST_TI_12_65 ( MastDuty *vcast_param ) ;

void VCAST_TI_12_67 ( MastType *vcast_param ) ;

void VCAST_TI_12_69 ( MastZone *vcast_param ) ;

void VCAST_TI_12_7 ( unsigned char vcast_param[IMM2VCM_CMD_COUNT] ) ;

void VCAST_TI_12_79 ( Op_Int_Mask_Type *vcast_param ) ;

void VCAST_TI_12_804 ( powerbase_fb_bus_t *vcast_param ) ;

void VCAST_TI_12_806 ( powerbase_fb_main_bus_t *vcast_param ) ;

void VCAST_TI_12_808 ( steering_fdbk_bus_t *vcast_param ) ;

void VCAST_TI_12_810 ( steering_ms_fdbk_bus_t *vcast_param ) ;

void VCAST_TI_12_812 ( throttle_input_bus_t *vcast_param ) ;

void VCAST_TI_12_814 ( throttle_in_bus_t *vcast_param ) ;

void VCAST_TI_12_816 ( vehicle_fb_c2m_bus_t *vcast_param ) ;

void VCAST_TI_12_818 ( steer_inputs_bus_t *vcast_param ) ;

void VCAST_TI_12_820 ( voltage_mon_bus_t *vcast_param ) ;

void VCAST_TI_12_822 ( analog_in_monitor_bus_t *vcast_param ) ;

void VCAST_TI_12_824 ( current_mon_bus_t *vcast_param ) ;

void VCAST_TI_12_827 ( AutoCal_Status_bus_t *vcast_param ) ;

void VCAST_TI_12_829 ( unsigned short vcast_param[35] ) ;

void VCAST_TI_12_830 ( signed int vcast_param[35] ) ;

void VCAST_TI_12_831 ( RackSelectSetups_calib_c2m_bus_t *vcast_param ) ;

void VCAST_TI_12_834 ( signed int vcast_param[6] ) ;

void VCAST_TI_12_835 ( CUTOUT_TYPE_TYPE vcast_param[6] ) ;

void VCAST_TI_12_836 ( CUTOUT_ZONE_TYPE vcast_param[6] ) ;

void VCAST_TI_12_837 ( SD_TRIGGER_TYPE vcast_param[6] ) ;

void VCAST_TI_12_839 ( UserCutOuts_calib_c2m_bus_t *vcast_param ) ;

void VCAST_TI_12_841 ( hour_meter_input_bus_t *vcast_param ) ;

void VCAST_TI_12_843 ( TestOutput_input_bus_t *vcast_param ) ;

void VCAST_TI_12_853 ( enum ips_index *vcast_param ) ;

void VCAST_TI_12_87 ( RACK_SELECT_TYPE *vcast_param ) ;

void VCAST_TI_12_884 ( enum accum_type *vcast_param ) ;

void VCAST_TI_12_885 ( struct fram_accum_data_meters *vcast_param ) ;

void VCAST_TI_12_886 ( unsigned vcast_param[32U] ) ;

void VCAST_TI_12_887 ( struct accum_data *vcast_param ) ;

void VCAST_TI_12_888 ( struct accum_data_meters *vcast_param ) ;

void VCAST_TI_12_889 ( struct accum_data vcast_param[32U] ) ;

void VCAST_TI_12_89 ( ReachType *vcast_param ) ;

void VCAST_TI_12_891 ( enum codrv_cfgmode *vcast_param ) ;

void VCAST_TI_12_892 ( enum codrv_otc_mod *vcast_param ) ;

void VCAST_TI_12_893 ( struct sdo_obj *vcast_param ) ;

void VCAST_TI_12_894 ( struct sdo_table *vcast_param ) ;

void VCAST_TI_12_895 ( struct sdo_table **vcast_param ) ;

void VCAST_TI_12_896 ( struct codrv_info *vcast_param ) ;

void VCAST_TI_12_897 ( char vcast_param[16U] ) ;

void VCAST_TI_12_9 ( struct dem_report_record vcast_param[16U] ) ;

void VCAST_TI_12_91 ( RegionType *vcast_param ) ;

void VCAST_TI_12_93 ( SD_TRIGGER_TYPE *vcast_param ) ;

void VCAST_TI_12_952 ( enum mi_devdrv *vcast_param ) ;

void VCAST_TI_12_953 ( struct mi_driver_data *vcast_param ) ;

void VCAST_TI_12_954 ( struct mi_table *vcast_param ) ;

void VCAST_TI_12_955 ( struct mi_driver *vcast_param ) ;

void VCAST_TI_12_961 ( struct mm_can_module *vcast_param ) ;

void VCAST_TI_12_962 ( struct mi_driver vcast_param[10U] ) ;

void VCAST_TI_12_963 ( struct mm_local_info *vcast_param ) ;

void VCAST_TI_12_964 ( char vcast_param[36U] ) ;

void VCAST_TI_12_965 ( struct mm_local_proc *vcast_param ) ;

void VCAST_TI_12_966 ( struct mm_vehicle_manifest *vcast_param ) ;

void VCAST_TI_12_967 ( char vcast_param[18U] ) ;

void VCAST_TI_12_993 ( struct imm_control_info *vcast_param ) ;

void VCAST_TI_12_994 ( struct imm_ips_data *vcast_param ) ;

void VCAST_TI_12_995 ( unsigned char vcast_param[4U] ) ;

void VCAST_TI_8_3 ( float *vcast_param ) ;

void VCAST_TI_8_4 ( char *vcast_param ) ;

void VCAST_TI_8_5 ( unsigned char *vcast_param ) ;

void VCAST_TI_9_13 ( unsigned char **vcast_param ) ;

void VCAST_TI_9_16 ( unsigned short *vcast_param ) ;

void VCAST_TI_9_20 ( struct aecp_object *vcast_param ) ;

void VCAST_TI_9_8 ( char **vcast_param ) ;


#ifdef __cplusplus
}
#endif
