import glob
import os
import shutil

try:
    from pathlib import Path
    p = Path('New folder/memory.ld')
    p.rename(p.with_suffix('.txt'))
    
except:
    print("Could not remove")

os.chmod('New folder', 0o777)
shutil.rmtree('New folder')


for f in glob.glob("*.env"): # find all env files
    if not f.endswith("0.env"):
        os.remove(f)  # if file name ends in 0.env, delete it
