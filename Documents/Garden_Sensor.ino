/**
*    @file                    Garden Sensor Sketch
*    @brief                   This file contains all functions related to This Project
*    @copyright               Thomas Rode., Delphos, OH  45833
*    @date                    5/24/2017
*
*    @remark Author:          Tom Rode
*    @remark Project Tree:    Garden_Sensor_Proj
*
*    @note        Ver 1.1     This is the code to run, This version has the ability to be put into low power mode if MICRO_LOWPOWER_EN is defined
*    
*/

#include <math.h>         //loads the more advanced math functions
#include <SoftwareSerial.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>    // looking on how to effecitly use this 
#include <avr/power.h> 
#include "ESP8266.h"
#define  SUPPLY_VOLTAGE_3_3
//#define  MICRO_LOWPOWER_EN

/* -------------------Enumerations  ------------------------------*/
typedef enum
{
  ADC_SOIL_SENSOR           = A0,   /** < Line 0 */
  VAL1                      = 1,  /** < Line 0 */
  VAL2                      = 2,  /** < Line 1 */
  VAL3                      = 3,  /** < Line 1 */
  VAL4                      = 4,  /** < Line 1 */
  NTC_THERMISTOR            = A1,  /** < Line 1  */
  
}ADCChn_t;
/* --------------------CONSTANT'S-----------------------------------*/
const byte  FALSE                                   = 0;
const byte  TRUE                                    = 1;
const int   FIVE_HUNDERD_MS                         = 500;
const int   ONE_THOUSAND_MS                         = 1000;
const int   SIXTY_THOUSAND_MS                       = 60000;
const byte  THIRTY_MINUTES                          = 30;
const byte  FIFTEEN_MINUTES                         = 15;
const byte  TEN_MINUTES                             = 10;
const byte  FIVE_SEC                                = 10;
const byte  FIVE_MINUTES                            = 5;
const byte  FOUR_MINUTES                            = 4;
const float VOLTS_SOIL_SENSOR_3_3                   = 1.193;
 
/* ---------------------PIN ASSIGNMENTS-----------------------------*/
const byte  SEN_PWR                                 = 8; // Pin 14 AtMeaga 328 Arduino UNO
const byte  NTC_IN                                  = NTC_THERMISTOR;   //pin 24 AtMega 328
const byte  MOISTURE_IN                             = ADC_SOIL_SENSOR;  //pin 23 AtMega 328 // Analog input pin that the potentiometer is attached to
const byte  SFW_RX                                  = 5; // Software serial Rx Pin 12 AtMeaga 328 Arduino UNO
const byte  SFW_TX                                  = 6; // Software serial Tx Pin 11 AtMeaga 328 Arduino UNO
const byte  BLINK_LED                               = 13; // Pin 19 AtMeaga 328
const byte  ESP8266_PWR                             = 9; // Pin 15 AtMeaga 328
//int wakePin                                       = 2; // pin used for waking up Assigned in gvSetSleep() 
/* ---------------------Wifi Settings-----------------------------*/
const char *SSID     = "CrownGuest";//"NETGEAR36_2GEXT";//"CrownGuest";//"WIFI-SSID";
const char *PASSWORD = "Access123";//"festiveapple513";//"Access123";//"WIFI-PASWWORD";

/* -------------------Global Variables------------------------------*/
int uwFiveHundredMsTaskCnt;
int uwThousandMsTaskCnt;
int uwSixtyThousandMsTaskCnt;
byte ubInitialMsgFlg;
char MODULE_LED;

// replace with your channel's thingspeak API key Write API key this is Garden Waterer//TomsTest
String apiKey = "H7Z21QYQ36G7VTAD";//"GIU176KPWXU1VYXY";
SoftwareSerial ser(SFW_RX, SFW_TX); // Rx, Tx SoftwareSerial pins for MEGA/Uno. For other boards see: https://www.arduino.cc/en/Reference/SoftwareSerial
ESP8266 wifi(ser);




void wakeUpNow()        // here the interrupt is handled after wakeup
{

  // execute code here after wake-up before returning to the loop() function


  // timers and code using timers (serial.print and more...) will not work here.

  // we don't really need to execute any special functions here, since we

  // just want the thing to wake up

}


/**
   @fn     setup()

   @brief   Initializations here .

   @param   N/A

   @return  N/A

   @note    H/W and varaibles setup.

   @author  Tom Rode

   @date    5/24/2017

*/
void setup() 
{
    /** ###Functional overview: */
    
    /** initialize serial communications at 57600 bps: */
    Serial.begin(57600);
    Serial.println(  "Initilization of Garden Sensor" );
   
    //To reduce power, setup all pins as inputs with no pullups
    for(int x = 7 ; x < 18 ; x++)
    {
           
        if ( (x != SFW_RX) || (x != SFW_TX)) 
        {
            pinMode(x, INPUT);
            digitalWrite(x, LOW);
        }
    }
  
    /** Transistor High Side turn on for Moisture sensor*/
    pinMode(SEN_PWR, OUTPUT);

    /** - Set Indicator as output */
    pinMode(BLINK_LED, OUTPUT);

    /** - Set ESP8266 Power Transistor for control */
    pinMode(ESP8266_PWR, OUTPUT);
    //while(1)
    //{
    /** Turn on transistor*/
    //digitalWrite(ESP8266_PWR, FALSE);   // turn the ESP8266 on (HIGH is the voltage level) 

    //delay(2000);
   
    /** Turn on transistor*/
    //digitalWrite(ESP8266_PWR, TRUE);   // turn the ESP8266 on (HIGH is the voltage level) 
    //delay(2000);
   //}
    /** - Enable Internal Watchdog */
    wdt_enable(WDTO_8S);
     

    if (!wifi.init(SSID, PASSWORD))
    {
        
        Serial.println("Wifi Init failed. Check configuration.");
    
        while (true) ; // loop eternally  
    } 
    /** Pet Watchdog*/
    wdt_reset();
    
    Serial.println( wifi.getVersion());
   
    /** - Get Indicator to output */
    gBlink(50, TRUE); 

    /** - Allow for intial message */
    ubInitialMsgFlg = TRUE; 

    /** - Power management of unused two wire peripheral initial 6.97 ma */
    power_twi_disable(); 

    /** - Power management of unused SPI peripherals initial 6.60 ma */
    power_spi_disable();

    /** - Power management of unused timer2 peripherals initial 6.40 ma */
    power_timer2_disable();  

    /** - Power management of unused adc peripherals initial 6.25 ma need to be re-enabled once running seeems its done automatically*/
    power_adc_disable(); 

    /** - Fire off 60000ms task initially */
    gvSixtyThousandMsTask(); 

   /** - Sleep setup */
  //sleep_enable(); 
  //set_sleep_mode(SLEEP_MODE_PWR_SAVE); 
             
}

/**
   @fn     loop()

   @brief   Main Loop

   @param   N/A

   @return  N/A

   @note    Task called here.

   @author  Tom Rode

   @date    5/24/2017

*/
void loop()
{
  /** ###Functional overview: */
  /** Idle State */
  NullTask();
  
  /* 500 MS TASK*/
  if ( FIVE_HUNDERD_MS == uwFiveHundredMsTaskCnt )
  {
    /** Fire off the task now */
    gvFiveHundredMsTask();

    /** reset the count to one */
    uwFiveHundredMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwFiveHundredMsTaskCnt++;
  }

  /* 1000 MS TASK*/
  if ( ONE_THOUSAND_MS == uwThousandMsTaskCnt )
  {

    /** Fire off the task now */
    gvOneThousandMsTask();

    /** reset the count to one */
    uwThousandMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
    uwThousandMsTaskCnt++;
  }

   /* 60000 MS TASK or ONE Minute*/
  if ( SIXTY_THOUSAND_MS == uwSixtyThousandMsTaskCnt )
  {

    /** Fire off the task now */
    gvSixtyThousandMsTask();

    /** reset the count to one */
    uwSixtyThousandMsTaskCnt = 1;
  }
  else
  {
    /** increment count */
   uwSixtyThousandMsTaskCnt++;
  }
 

  /** Wait for one millisecond*/
  delay(1);
}

/**
   @fn      NullTask()

   @brief   Task used idle state.

   @param   N/A

   @return  N/A

   @note    N/A.

   @author  Tom Rode

   @date    5/24/2017

*/
void NullTask()
{
  /** ###Functional overview: */

  /** Pet Watchdog*/
  wdt_reset();

  /** - Pet the Hardware watch dog */
  //gvHWWatchdog();
}

/**
   @fn      gvFiveHundredMsTask()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
void gvFiveHundredMsTask()
{ 
   static byte FiveSecCnt;
   
  
   if(FiveSecCnt == FIVE_SEC) 
   {
       /**  - Give a 500 ms blink  turn on*/
       gBlink(TRUE, FALSE);

       FiveSecCnt++;
    
   }
   else if (FiveSecCnt > FIVE_SEC) 
   {
       /**  - Give a 500 ms blink  turn off*/
       gBlink(TRUE, FALSE); 

       /**- reset the count */
       FiveSecCnt = 0;
   }
   else
   { 
         digitalWrite(BLINK_LED, LOW);   // turn the LED on (HIGH is the voltage level)
         FiveSecCnt++;
   }
   
}

/**
   @fn      gvOneThousandMsTask()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
void gvOneThousandMsTask()
{   
      
    /** - This is an indication of application */
    //gBlink(TRUE, TRUE);
             
}

/**
   @fn      gvSixtyThousandMsTask()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
void gvSixtyThousandMsTask()
{
    double NTCVal;
    float MoistureSensor; 
    String inData;
    String cmd;
    static byte ThirtyMinuteCnt;
    static byte FifteenMinuteCnt;
    static byte TenMinuteCnt;
    static byte FiveMinuteCnt;
    static byte FourMinuteCnt;
    static byte WifiConnectRetry;
    static byte AllowWifiConn;
    static byte SendWifiDataFlg; 

    
    /** ###Functional overview: */
    //sleep_disable(); 
    if( ((THIRTY_MINUTES <= ThirtyMinuteCnt ) || (WifiConnectRetry == TRUE)) || ( ubInitialMsgFlg == TRUE ) )
    { 
       /** - Only have this reinitialize after the first time */
       if(ubInitialMsgFlg == FALSE)
       {     /** Try to Re-Establish Wifi Connection but not initially */
            if(!wifi.init(SSID, PASSWORD))
            { 
                Serial.println("Wifi Init failed. Check configuration.");
                WifiConnectRetry = TRUE;
            }
       }    
        /** Pet Watchdog*/
        wdt_reset();
       
        /** - Collect values from Sensors */
        NTCVal = gvNTCThermisterRead();
        MoistureSensor = gvSoilRead();//gADCread(ADC_SOIL_SENSOR);

        /** - Adjust readings if Supply voltage at 3.3Vdc */
        #ifdef  SUPPLY_VOLTAGE_3_3
            MoistureSensor = (MoistureSensor * VOLTS_SOIL_SENSOR_3_3);
        #else
            MoistureSensor = MoistureSensor;
        #endif       
     
        /** - Send data out of ESP8266 module */
        Serial.println("Sending Request to ThingsSpeak");
        esp_8266(NTCVal,MoistureSensor);

        /** - Try to put ESP8266 into Deep Sleep have to have this delay*/
        delay(600);
        ser.println("AT+GSLP=2000000");//300000 is 4:27 sec//240000"); 240000 //is 3:27 sec 270000 is 3:50 sec 540000 is 7:32 sec 600000 is 8:21 sec 1040000 is 15:10 sec 2080000 is 30:18 sec 2060000 is 30:12 sec or 2060000 is 31:03 ofter complete loop 2020000 is 29:37
        Serial.println("AT+GSLP=2000000"); //2000000 is 29:21 sec
        Serial.println("");

        /** - Zero out Timer */
        ThirtyMinuteCnt = 1;

        /** - Display timinig status */
        if( ubInitialMsgFlg == TRUE )
        {
            Serial.println("Initial flag was set");   
        }
        else
        {
            Serial.println("30 Minutes Elapsed");
        } 

        /** - Disable initial message */ 
        ubInitialMsgFlg = FALSE;      
    }
    else
    {
        ThirtyMinuteCnt++;
    } 

    /** - Put Micro to low power state */
    #ifdef MICRO_LOWPOWER_EN
        gvSetSleep();
    #endif         
}


/**
   @fn      gvSetSleepNow()

   @brief   ATMega 328 sleep 

   @param   N/A

   @return  N/A

   @note    TBD.

   @author  Tom Rode

   @date    6/7/2017

*/
void  gvSetSleep()
{
    int wakePin = 2;                 // pin used for waking up
    static byte CntTest;
   
    /** - set pin 2 INT0 as an input */
    pinMode(wakePin, INPUT);

    attachInterrupt(0, wakeUpNow, RISING); // use interrupt 0 (pin 2) and run function
   
    wdt_disable();

    /* Choose our preferred sleep mode:
      SLEEP_MODE_IDLE         -the least power savings 
      SLEEP_MODE_ADC
      SLEEP_MODE_PWR_SAVE     // Seems to retain ram value  520 uAmps
      SLEEP_MODE_STANDBY      // seems to retain ran value  230 uAmps
      SLEEP_MODE_PWR_DOWN     -the most power savings total cpu reset no ram retension 10-20 uAmps
     */
    Serial.println(CntTest++);
    Serial.println("SLEEP_MODE_STANDBY  ");
    set_sleep_mode(SLEEP_MODE_STANDBY  ); 
    //cli(); 
 
    // Set sleep enable (SE) bit:
    sleep_enable();
    
    sleep_bod_disable();
    //sei();

    // Delay before it sleeps 
    delay(200);

    // Put the device to sleep:
    //sleep_mode();
    sleep_cpu();
    
    // Upon waking up, sketch continues from this point.
    sleep_disable();  

    detachInterrupt(0);      // disables interrupt 0 on pin 2 so the 
}

/**
   @fn      gADCread()

   @brief   All ADC conversion read.

   @param   ADC Channel

   @return  ubTemp = 16bit ADC read

   @note    TBD.

   @author  Tom Rode

   @date    12/28/2015

*/
uint16_t gADCread(ADCChn_t ADChn)
{
  /** ###Functional overview: */
uint8_t ubTemp;
int sensorValue = 0;                   // value read from the pot
byte TruncatedSoilSenOutputValue = 0; // 8 bit value
float AdcVoltsRead;


     /** - Power management enabled */
     power_adc_enable();
     
    if( ADChn == ADC_SOIL_SENSOR)
    {
        /** read the analog in value */
        sensorValue = analogRead(MOISTURE_IN);

        /** truncated lower 8 bits from 10 bit conversion */
        TruncatedSoilSenOutputValue = ( sensorValue >> 2 );

        /** make the read readable in volts */
        AdcVoltsRead = TruncatedSoilSenOutputValue * (5.0 / 255.0);

        /** print the results to the serial monitor: */
        //Serial.print(" ADC 8bit = ");
        //Serial.print(  TruncatedSoilSenOutputValue);
        //Serial.print(" sensor in volts = ");
        //Serial.println(  AdcVoltsRead);

       /** output to be returned*/
       return(TruncatedSoilSenOutputValue);
    }
    else if ( ADChn == NTC_THERMISTOR )
    {
        sensorValue=analogRead(NTC_IN); //Read the analog port 1 and store the value in val
        return(sensorValue);
    }

     /** - Power management dis-abled */
     power_adc_disable();
}

/**
   @fn      gvNTCThermisterRead()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
double gvNTCThermisterRead()
{ 
   
    int val;                //Create an integer variable
    double temp;            //Variable to hold a temperature value
   
    /** ###Functional overview: */
    
    val=gADCread(NTC_THERMISTOR);
    temp=Thermister(val);   //Runs the fancy math on the raw analog value
    //Serial.println(temp);   //Print the value to the serial port
    
    return temp;
}

/**
   @fn      double Thermister(int RawADC)

   @brief   None.

   @param   N/A

   @return  double

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
double Thermister(int RawADC) 
{
    
    double Temp;

    /** ###Functional overview: */

    /** - Function to perform the fancy math of the Steinhart-Hart equation */
    Temp = log(((10240000/RawADC) - 10000));
    Temp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * Temp * Temp ))* Temp );
    Temp = Temp - 273.15;              // Convert Kelvin to Celsius
    Temp = (Temp * 9.0)/ 5.0 + 32.0; // Celsius to Fahrenheit - comment out this line if you need Celsius
    
    return Temp;
}

/**
   @fn      uint8_t gvSoilRead(void)

   @brief   None.

   @param   N/A

   @return  8Bit

   @note    

   @author  Tom Rode

   @date    5/24/2017

*/
uint8_t gvSoilRead(void)
{
    uint8_t ubTemp;

    /** ###Functional overview: */

    /** Turn on transistor*/
    digitalWrite(SEN_PWR, TRUE);   // turn the LED on (HIGH is the voltage level)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    AgvUl1tA~44444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444c5x                                                                            traSonic();

    /** Wait for four millisecond*/
    delay(4);

    /** ADC READ */
    ubTemp = gADCread(ADC_SOIL_SENSOR);

    /** Turn off transistor*/
    digitalWrite(SEN_PWR, FALSE);

    /** - return soil reading */
    return ubTemp;
  
}
/**
   @fn      void gBlink(int UwTimes)

   @brief   Blink an output pin for indication.

   @param   UwTimes =  Amount of times to blink,
   @param   ubRepeat = if to blink

   @return  N/A

   @note    TBD.

   @author  Tom Rode

   @date    12/28/2015

*/
void gBlink(int UwTimes, byte ubRepeat)
{
  int uwindex;

  /** ###Functional overview: */

  /** Toggle LED */
  if ( FALSE == ubRepeat )
  {
    /** Back and fourth */
    if ( TRUE == MODULE_LED )
    {
      /** Toggle LED */
      MODULE_LED = FALSE;
      digitalWrite(BLINK_LED, TRUE);   // turn the LED on (HIGH is the voltage level)
    }
    else
    {
      MODULE_LED = TRUE;
      digitalWrite(BLINK_LED, LOW);   // turn the LED on (HIGH is the voltage level)
    }
  }
  else
  {
    /** Amount to blink a pin */
    for ( uwindex = 0; uwindex < UwTimes; uwindex++ )
    {
      /** Blink the indicator LED specified amount of times*/
      if ( TRUE == MODULE_LED )
      {
        MODULE_LED = FALSE;
        digitalWrite(BLINK_LED, HIGH);   // turn the LED on (HIGH is the voltage level)
      }
      else
      {
        MODULE_LED = TRUE;
        digitalWrite(BLINK_LED, LOW);   // turn the LED on (HIGH is the voltage level)
      }

      /** - Delay will be visible */
      delay(100);
    }
  }
}

/**
   @fn      esp_8266()

   @brief   None.

   @param   N/A

   @return  N/A

   @note    

   @author  Tom Rode

   @date     5/24/2017

*/
static int Cnt;
void esp_8266(double NTCValue, float MoistureSensor)
{
 // convert to string
  char buf[32];

  //String strVolt = dtostrf( bat_volt, 4, 1, buf);
  /** - Display the readings before outputting them */
  Serial.print(MoistureSensor);
  Serial.println(" Moisture Reading");
  Serial.print(NTCValue);
  Serial.println(" NTC Value");
  Serial.print(Cnt);
  Serial.println(" Counts of loop");
  
  // TCP connection
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += "184.106.153.149"; // api.thingspeak.com
  cmd += "\",80";
  ser.println(cmd);
   
  if(ser.find("Error")){
    Serial.println("AT+CIPSTART error");
    return;
  }
  
  // prepare GET string
  String getStr = "GET /update?api_key=";
  getStr += apiKey;
  getStr +="&field1=";
  getStr += String(98.6);//NTCValue);   // remove this bogus value
  getStr +="&field2=";
  getStr += String(MoistureSensor);
  getStr += "\r\n\r\n";

  // send data length
  cmd = "AT+CIPSEND=";
  cmd += String(getStr.length());
  ser.println(cmd);
  if(ser.find(">")){
    /* Test to see whats going out */
    //Serial.print(getStr);
    ser.print(getStr);    
  }
  else{
    ser.println("AT+CIPCLOSE");
    // alert user
    Serial.println("AT+CIPCLOSE");

    //if (!wifi.init(SSID, PASSWORD))
    //{ 
    //    Serial.println("Wifi Init failed. Check configuration.");
    //}    
  }
   
  
  // thingspeak needs 15 sec delay between updates
  //delay(16000);//(600000);//(120000);//(16000); 
  Cnt++; 
}

