package com.visuality.nq.client.samples;

import com.visuality.nq.auth.Credentials;
import com.visuality.nq.auth.PasswordCredentials;
import com.visuality.nq.client.Client;
import com.visuality.nq.client.File;
import com.visuality.nq.client.Mount;
import com.visuality.nq.common.Buffer;
import com.visuality.nq.common.NqException;

public class SimplestExample {

	public SimplestExample() {
		try {

			/**
			 * Create a mount instance with the specific credentials and mount information
			 */
			Credentials credentials = new PasswordCredentials("lilia", "12345", "ESXIG9VS.INTERNAL");
			Mount mount = new Mount("192.168.19.85", "Clienttest2", credentials);
			
			/**
			 * Create a new file.
			 */
			File.Params fileParams = new File.Params(
					File.ACCESS_READ | File.ACCESS_WRITE | File.ACCESS_DELETE, File.SHARE_FULL, File.DISPOSITION_OPEN_IF, false);
			File file = new File(mount, "myCreatedFile.txt", fileParams);

			/**
			 * Write some data to the file and then close the file
			 */
			String someData = "This is some data";
			Buffer buffer = new Buffer(someData.getBytes(), 0, someData.getBytes().length);
			file.write(buffer);
			file.close();
			file = null;
			
			/**
			 *  Closing the mount in this example is not necessary but, especially when there are multiple mounts, it is a good
			 *  practice to close unneeded mount instances.
			 */
			mount.close();
			mount = null;

			/**
			 * Now let's start all over and try to delete the file
			 */
			mount = new Mount("192.168.19.85", "Clienttest2", credentials);
			fileParams = new File.Params(File.ACCESS_DELETE, File.SHARE_DELETE, File.DISPOSITION_OPEN, false);
			file = new File(mount, "myCreatedFile.txt", fileParams);
			
			file.deleteOnClose();
			file.close();
			
			
			/**
			 * It is a good practice to terminate jNQ with this call.  All threads will be terminated.
			 */
			Client.stop();
			
		} catch (NqException e) {
			System.err.println("Unable to create file. Error = " + e.getMessage() + ", error code = " + e.getErrCode());
		}
	}
	
	
	public static void main(String[] args) {
		new SimplestExample();
	}

}
