from win32com.client import Dispatch

## a. Open SYNECT via Dispatch Application from Python
Application             = Dispatch( 'SYNECT.Application' )

## b. Connect to the database. You will need to provide the Server URL and form of authentication (username/password or Windows). If windows, the connection will be automatic.
Application.DBConnection.Connect( True )  ## True => allow untrusted certificate

server_script_name = 'myServerScript'
try:        script = filter( lambda x: x.Name == server_script_name, Application.Server.Scripts )[ 0 ]
except:           raise Exception( 'Failed to retrieve server script with name "{}"'.format( server_script_name ) )

## c. Once connected you need to open the desired workspace and desired Test Management project
Application.Navigator.OpenWorkspace( workspaceId )

workspaceId, tmProjectId = Application.Server.CallMethod( script, 'get_data_from_server', 'myWorkspace;myTmProject' ).split( ';' )
if not workspaceId or not tmProjectId:          return

## e. Import the execution results using the import adapter API
plugin = filter( lambda x: x.DisplayName == 'my import plugin', Application.TM.Plugins.ExecutionsImportPlugins )
Application.TM.ImportExecutions( plugin, workspaceId, tmProjectId, 'file/to/import.ext', 'path/to/plugin.ecxml' )  ## not sure about this part, you will need to verify via API

# these might be taken care of by step e, else, you will need a way to determine WHICH pending plugin is yours ( parse id from json? )
## d. Navigate to the pending execution and get the handle of the execution that is currently in processing state
## f. Set the state of the execution to “Finished”


## Server-side ( myServerScript.py )
class myServerScript( object ):

      def get_data_from_server( self, ServerContext, args ):
            workspaceName, tmProjectName = args.split( ';' )

            try:
                  workspaceId = filter( lambda x: x.Name == workspaceName, ServerContext.Workspaces   )[0].UniqueID
                  tmProjectId = filter( lambda x: x.Name == tmProjectName, workspace.Projects         )[0].UniqueID
                  return '{};{}'.format( workspaceId, tmProjectId )
            except:
                  return ''
