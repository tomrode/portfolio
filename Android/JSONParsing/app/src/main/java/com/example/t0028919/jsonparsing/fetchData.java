package com.example.t0028919.jsonparsing;


import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import android.util.Log;

/**
 * Created by Abhishek Panwar on 7/14/2017.
 */

public class fetchData extends AsyncTask<Void,Void,Void> {
    String data ="";
    String dataParsed = "";
    String singleParsed ="";
    private static final String TAG = "TomsMessage";
    @Override
    protected Void doInBackground(Void... voids) {
        try {
            Log.i(TAG, "IN AsyncTask ---------------------------->:" );   // debug msg
            URL url = new URL("https://api.myjson.com/bins/1ecanl");
            //URL url = new URL("https://engstl1.us.crownlift.net/PIL_SYNECT_TEXT_PLAN/Ready_for_execution");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while(line != null){
                line = bufferedReader.readLine();
                data = data + line;
            }

            JSONArray JA = new JSONArray(data);
            for(int i =0 ;i <JA.length(); i++){
                JSONObject JO = (JSONObject) JA.get(i);

                singleParsed =  "ActiveTestFileName:" + JO.get("ActiveTestFileName") + "\n"+
                        "TruckSerialNumber:" + JO.get("TruckSerialNumber") + "\n"+
                        "TestRequestNumber:" + JO.get("TestRequestNumber") + "\n"+
                        "SoftWareVersion:" + JO.get("SoftWareVersion") + "\n"+
                        "Synect_TC#:" + JO.get("Synect_TC#") + "\n"+
                        "Group_TC#:" + JO.get("Group_TC#") + "\n"+
                        "DNG_iRTC#:" + JO.get("DNG_iRTC#") + "\n"+
                        "NumberOfTestCases:" + JO.get("NumberOfTestCases") + "\n"+
                        "\n"+
                        "TC1Steps:" + JO.get("TC1Steps") + "\n"+
                        "TC1ExpectedResult:" + JO.get("TC1ExpectedResult") + "\n"+
                        "TC1Result:" + JO.get("TC1Result") + "\n"+
                        "TC1Notes:" + JO.get("TC1Notes") + "\n"+
                        "TC1Operator:" + JO.get("TC1Operator") + "\n"+
                        "TC1Testtime:" + JO.get("TC1Testtime") + "\n"+
                        "\n"+
                        "TC2Steps:" + JO.get("TC2Steps") + "\n"+
                        "TC2ExpectedResult:" + JO.get("TC2ExpectedResult") + "\n"+
                        "TC2Result:" + JO.get("TC2Result") + "\n"+
                        "TC2Notes:" + JO.get("TC2Notes") + "\n"+
                        "TC2Operator:" + JO.get("TC2Operator") + "\n"+
                        "TC2Testtime:" + JO.get("TC2Testtime") + "\n"+
                        "\n"+
                        "TC3Steps:" + JO.get("TC3Steps") + "\n"+
                        "TC3ExpectedResult:" + JO.get("TC3ExpectedResult") + "\n"+
                        "TC3Result:" + JO.get("TC3Result") + "\n"+
                        "TC3Notes:" + JO.get("TC3Notes") + "\n"+
                        "TC3Operator:" + JO.get("TC3Operator") + "\n"+
                        "TC3Testtime:" + JO.get("TC3Testtime") + "\n"+
                        "\n" +
                        "TC4Steps:" + JO.get("TC4Steps") + "\n"+
                        "TC4ExpectedResult:" + JO.get("TC4ExpectedResult") + "\n"+
                        "TC4Result:" + JO.get("TC4Result") + "\n"+
                        "TC4Notes:" + JO.get("TC4Notes") + "\n"+
                        "TC4Operator:" + JO.get("TC4Operator") + "\n"+
                        "TC4Testtime:" + JO.get("TC4Testtime") + "\n"+
                        "\n" +
                        "TC5Steps:" + JO.get("TC5Steps") + "\n"+
                        "TC5ExpectedResult:" + JO.get("TC5ExpectedResult") + "\n"+
                        "TC5Result:" + JO.get("TC5Result") + "\n"+
                        "TC5Notes:" + JO.get("TC5Notes") + "\n"+
                        "TC5Operator:" + JO.get("TC5Operator") + "\n"+
                        "TC5Testtime:" + JO.get("TC5Testtime") + "\n"+
                        "\n" +
                        "TC6Steps:" + JO.get("TC6Steps") + "\n"+
                        "TC6ExpectedResult:" + JO.get("TC6ExpectedResult") + "\n"+
                        "TC6Result:" + JO.get("TC6Result") + "\n"+
                        "TC6Notes:" + JO.get("TC6Notes") + "\n"+
                        "TC6Operator:" + JO.get("TC6Operator") + "\n"+
                        "TC6Testtime:" + JO.get("TC6Testtime") + "\n"+
                        "\n"+
                        "TC7Steps:" + JO.get("TC7Steps") + "\n"+
                        "TC7ExpectedResult:" + JO.get("TC7ExpectedResult") + "\n"+
                        "TC7Result:" + JO.get("TC7Result") + "\n"+
                        "TC7Notes:" + JO.get("TC7Notes") + "\n"+
                        "TC7Operator:" + JO.get("TC7Operator") + "\n"+
                        "TC7Testtime:" + JO.get("TC7Testtime") + "\n"+
                        "\n"+
                        "TC8Steps:" + JO.get("TC8Steps") + "\n"+
                        "TC8ExpectedResult:" + JO.get("TC8ExpectedResult") + "\n"+
                        "TC8Result:" + JO.get("TC8Result") + "\n"+
                        "TC8Notes:" + JO.get("TC8Notes") + "\n"+
                        "TC8Operator:" + JO.get("TC8Operator") + "\n"+
                        "TC8Testtime:" + JO.get("TC8Testtime") + "\n"+
                        "\n" +
                        "TC9Steps:" + JO.get("TC9Steps") + "\n"+
                        "TC9ExpectedResult:" + JO.get("TC9ExpectedResult") + "\n"+
                        "TC9Result:" + JO.get("TC9Result") + "\n"+
                        "TC9Notes:" + JO.get("TC9Notes") + "\n"+
                        "TC9Operator:" + JO.get("TC9Operator") + "\n"+
                        "TC9Testtime:" + JO.get("TC9Testtime") + "\n"+
                        "\n" +
                        "TC10Steps:" + JO.get("TC10Steps") + "\n"+
                        "TC10ExpectedResult:" + JO.get("TC10ExpectedResult") + "\n"+
                        "TC10Result:" + JO.get("TC10Result") + "\n"+
                        "TC10Notes:" + JO.get("TC10Notes") + "\n"+
                        "TC10Operator:" + JO.get("TC10Operator") + "\n"+
                        "TC10Testtime:" + JO.get("TC10Testtime") + "\n"+
                        "\n" +
                        "TC11Steps:" + JO.get("TC11Steps") + "\n"+
                        "TC11ExpectedResult:" + JO.get("TC11ExpectedResult") + "\n"+
                        "TC11Result:" + JO.get("TC11Result") + "\n"+
                        "TC11Notes:" + JO.get("TC11Notes") + "\n"+
                        "TC11Operator:" + JO.get("TC11Operator") + "\n"+
                        "TC11Testtime:" + JO.get("TC11Testtime") + "\n"+
                        "\n"+
                        "TC12Steps:" + JO.get("TC12Steps") + "\n"+
                        "TC12ExpectedResult:" + JO.get("TC12ExpectedResult") + "\n"+
                        "TC12Result:" + JO.get("TC12Result") + "\n"+
                        "TC12Notes:" + JO.get("TC12Notes") + "\n"+
                        "TC12Operator:" + JO.get("TC12Operator") + "\n"+
                        "TC12Testtime:" + JO.get("TC12Testtime") + "\n"+
                        "\n"+
                        "TC13Steps:" + JO.get("TC13Steps") + "\n"+
                        "TC13ExpectedResult:" + JO.get("TC13ExpectedResult") + "\n"+
                        "TC13Result:" + JO.get("TC13Result") + "\n"+
                        "TC13Notes:" + JO.get("TC13Notes") + "\n"+
                        "TC13Operator:" + JO.get("TC13Operator") + "\n"+
                        "TC13Testtime:" + JO.get("TC13Testtime") + "\n"+
                        "\n" +
                        "TC14Steps:" + JO.get("TC14Steps") + "\n"+
                        "TC14ExpectedResult:" + JO.get("TC14ExpectedResult") + "\n"+
                        "TC14Result:" + JO.get("TC14Result") + "\n"+
                        "TC14Notes:" + JO.get("TC14Notes") + "\n"+
                        "TC14Operator:" + JO.get("TC14Operator") + "\n"+
                        "TC14Testtime:" + JO.get("TC14Testtime") + "\n"+
                        "\n" +
                        "TC15Steps:" + JO.get("TC15Steps") + "\n"+
                        "TC15ExpectedResult:" + JO.get("TC15ExpectedResult") + "\n"+
                        "TC15Result:" + JO.get("TC15Result") + "\n"+
                        "TC15Notes:" + JO.get("TC15Notes") + "\n"+
                        "TC15Operator:" + JO.get("TC15Operator") + "\n"+
                        "TC15Testtime:" + JO.get("TC15Testtime") + "\n"+
                        "\n" +
                        "TC16Steps:" + JO.get("TC16Steps") + "\n"+
                        "TC16ExpectedResult:" + JO.get("TC16ExpectedResult") + "\n"+
                        "TC16Result:" + JO.get("TC16Result") + "\n"+
                        "TC16Notes:" + JO.get("TC16Notes") + "\n"+
                        "TC16Operator:" + JO.get("TC16Operator") + "\n"+
                        "TC16Testtime:" + JO.get("TC16Testtime") + "\n"+
                        "\n"+
                        "TC17Steps:" + JO.get("TC17Steps") + "\n"+
                        "TC17ExpectedResult:" + JO.get("TC17ExpectedResult") + "\n"+
                        "TC17Result:" + JO.get("TC17Result") + "\n"+
                        "TC17Notes:" + JO.get("TC17Notes") + "\n"+
                        "TC17Operator:" + JO.get("TC17Operator") + "\n"+
                        "TC17Testtime:" + JO.get("TC17Testtime") + "\n"+
                        "\n"+
                        "TC18Steps:" + JO.get("TC18Steps") + "\n"+
                        "TC18ExpectedResult:" + JO.get("TC18ExpectedResult") + "\n"+
                        "TC18Result:" + JO.get("TC18Result") + "\n"+
                        "TC18Notes:" + JO.get("TC18Notes") + "\n"+
                        "TC18Operator:" + JO.get("TC18Operator") + "\n"+
                        "TC18Testtime:" + JO.get("TC18Testtime") + "\n"+
                        "\n" +
                        "TC19Steps:" + JO.get("TC19Steps") + "\n"+
                        "TC19ExpectedResult:" + JO.get("TC19ExpectedResult") + "\n"+
                        "TC19Result:" + JO.get("TC19Result") + "\n"+
                        "TC19Notes:" + JO.get("TC19Notes") + "\n"+
                        "TC19Operator:" + JO.get("TC19Operator") + "\n"+
                        "TC19Testtime:" + JO.get("TC19Testtime") + "\n"+
                        "\n" +
                        "TC20Steps:" + JO.get("TC20Steps") + "\n"+
                        "TC20ExpectedResult:" + JO.get("TC20ExpectedResult") + "\n"+
                        "TC20Result:" + JO.get("TC20Result") + "\n"+
                        "TC20Notes:" + JO.get("TC20Notes") + "\n"+
                        "TC20Operator:" + JO.get("TC20Operator") + "\n"+
                        "TC20Testtime:" + JO.get("TC20Testtime") + "\n"+
                        "\n" +
                        "TC21Steps:" + JO.get("TC21Steps") + "\n"+
                        "TC21ExpectedResult:" + JO.get("TC21ExpectedResult") + "\n"+
                        "TC21Result:" + JO.get("TC21Result") + "\n"+
                        "TC21Notes:" + JO.get("TC21Notes") + "\n"+
                        "TC21Operator:" + JO.get("TC21Operator") + "\n"+
                        "TC21Testtime:" + JO.get("TC21Testtime") + "\n"+
                        "\n" +
                        "TC22Steps:" + JO.get("TC22Steps") + "\n"+
                        "TC22ExpectedResult:" + JO.get("TC22ExpectedResult") + "\n"+
                        "TC22Result:" + JO.get("TC22Result") + "\n"+
                        "TC22Notes:" + JO.get("TC22Notes") + "\n"+
                        "TC22Operator:" + JO.get("TC22Operator") + "\n"+
                        "TC22Testtime:" + JO.get("TC22Testtime") + "\n"+
                        "\n" +
                        "TC23Steps:" + JO.get("TC23Steps") + "\n"+
                        "TC23ExpectedResult:" + JO.get("TC23ExpectedResult") + "\n"+
                        "TC23Result:" + JO.get("TC23Result") + "\n"+
                        "TC23Notes:" + JO.get("TC23Notes") + "\n"+
                        "TC23Operator:" + JO.get("TC23Operator") + "\n"+
                        "TC23Testtime:" + JO.get("TC23Testtime") + "\n"+
                        "\n" +
                        "TC24Steps:" + JO.get("TC24Steps") + "\n"+
                        "TC24ExpectedResult:" + JO.get("TC24ExpectedResult") + "\n"+
                        "TC24Result:" + JO.get("TC24Result") + "\n"+
                        "TC24Notes:" + JO.get("TC24Notes") + "\n"+
                        "TC24Operator:" + JO.get("TC24Operator") + "\n"+
                        "TC24Testtime:" + JO.get("TC24Testtime") + "\n"+
                        "\n" +
                        "TC25Steps:" + JO.get("TC25Steps") + "\n"+
                        "TC25ExpectedResult:" + JO.get("TC25ExpectedResult") + "\n"+
                        "TC25Result:" + JO.get("TC25Result") + "\n"+
                        "TC25Notes:" + JO.get("TC25Notes") + "\n"+
                        "TC25Operator:" + JO.get("TC25Operator") + "\n"+
                        "TC25Testtime:" + JO.get("TC25Testtime") + "\n"+
                        "\n";

                dataParsed = dataParsed + singleParsed +"\n" ;


            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        MainActivity.data.setText(this.dataParsed);

    }
}