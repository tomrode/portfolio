package com.example.t0028919.person_in_looptesttool;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.visuality.nq.auth.PasswordCredentials;
import com.visuality.nq.client.Client;
import com.visuality.nq.client.Directory;
import com.visuality.nq.client.File;
import com.visuality.nq.client.Mount;
import com.visuality.nq.common.Buffer;
import com.visuality.nq.common.BufferReader;
import com.visuality.nq.common.BufferWriter;
import com.visuality.nq.common.NqException;
import com.visuality.nq.config.Config;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static android.app.PendingIntent.getActivity;
import static android.os.Environment.getExternalStorageDirectory;
import static android.support.v4.app.ActivityCompat.finishAffinity;
import static com.example.t0028919.person_in_looptesttool.MainActivity.TRUE;
import static com.visuality.nq.client.File.delete;
//import static com.visuality.nq.common.Capture.ENABLECAPTUREPACKETS;
import static java.lang.String.valueOf;
import static java.lang.System.*;

/* Author: Thomas Rode
 *
 * Intent: This class is the driver used for Test plan completion data JSON files which will be sent back up to Network drive.
 *
*/

public class SendDeleteReadyForExecutionData extends AsyncTask<String, Void, String> {

    // debug message
    private static final String TAG = "TomsMessage";

    // SMB ConnectionStatus
    String ConnectionStatusReadyForExecutionDelete;
  

    @Override
    protected void onPreExecute() {
        Log.i(TAG, "in SendDeleteReadyForExecutionData onPreExecute");   // debug msg
    }


    @Override
    protected String doInBackground(String... params) {

        // Paths with EngSTL1 server t delete
        String ServerPathDel = "MANUAL_SYNECT_TEST_PLAN/Ready_for_Execution";

        // This will have to open an internal file to get credentials and determine which file and folder
        String StUserName = params[0];
        String StPassWord = params[1];
        String gVFileToBeUsed = params[2];
        String gVTruckPickedInSpinner = params[3];

        // debug PCAP
        /*String ENABLECAPTUREPACKETS = "ENABLECAPTUREPACKETS";
        try {
            Config.jnq.set(ENABLECAPTUREPACKETS, TRUE);
        } catch (NqException e) {
            e.printStackTrace();
            Log.i(TAG, "in SendDeleteReadyForExecutionData doInBackground PCAP exception" + e);
        }*/

        // debug msg
        Log.i(TAG, "in SendDeleteReadyForExecutionData doInBackground");

        //  Remove file in Ready_for_Execution out of EngSTL1 server
        try {
            // StUserName, StPassWord
            PasswordCredentials cr = new PasswordCredentials(StUserName, StPassWord, "us.crownlift.net");

            // Server init
            Mount mt = new Mount("engstl1", "Vol1", cr);

            // Parameter settings From Joe to delete
            File.Params pr  = new File.Params(File.ACCESS_DELETE, File.SHARE_DELETE, File.DISPOSITION_OPEN, false);

            // Concatenate and construct server path: EngSTL1 folder + Truck folder + file
            String DirForjNQDelRead_Ex = ServerPathDel + "/" + gVTruckPickedInSpinner + "/" + gVFileToBeUsed;

            // File instance
            File file = new File(mt, DirForjNQDelRead_Ex, pr);

            // See if this is a directory exists or not argument: isDir - true if a directory is expected, false if a file is expected.
            if(File.isExist(mt,DirForjNQDelRead_Ex,false ))
            {
                // After this operation the file is still accessible until all its handles will be closed. After the last handle on this file is clsoed, it will be deleted on server.
                file.deleteOnClose();

                // Close file
                file.close();

                // terminate jNQ client All threads will be terminated
                Client.stop();

                // LogCat debug message
                Log.i(TAG, "<-------------------------- FILE EXISTS TO DELETE IN READY FOR EXECUTION FOLDER ------------------------>:");

                // Report status
                ConnectionStatusReadyForExecutionDelete = "Delete Server Ready_For_Execution file Successful";
            }

            // Close the mount point
            mt.close();
        }
        catch (Exception e) {
            Log.i(TAG, "IN doInBackground method Exception SMB jNQ remove Ready_for_Execution---------------------------->:" + e);   // debug msg
            //ConnectionStatusReadyForExecutionDelete = ("SMB Server remove Error," + e);
            e.printStackTrace();
        }
      
        return null;
    }


    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);

        // Display this status on UI
        Performtestcases.ServerConnectionPerformtestCases.setText(this.ConnectionStatusReadyForExecutionDelete);

    }

}