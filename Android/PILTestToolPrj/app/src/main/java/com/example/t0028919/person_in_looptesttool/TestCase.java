package com.example.t0028919.person_in_looptesttool;

import java.io.Serializable;
/**
 * Created by t0028919 on 1/31/2018.
 */

public class TestCase implements Serializable
{
    String expectedresults;
    String results;
    String notes;
    String operator;
    String steps;
    String testtime;
    String h1_hourmeter;
    String odometer;
    String dng_irtc;
    String synect_tc_num;
    String group_tc_num;
    String guid;
    String date;
    String application_software_tested;
    String soc;
    String truck_serial_number;
    String events;
    String pass_fail_skip_btn_state;
    String battery_serial_number;

    /*public TestCase(String expectedresults, String results, String notes, String operator, String steps, int testtime)
    {
        this.expectedresults = expectedresults;
        this.results = results;
        this.notes = notes;
        this.operator = operator;
        this.steps = steps;
        this.testtime = testtime;
    }
    */

    // Getters and Setters
    public String getExpectedResults()
    {
        return expectedresults;
    }
    public void setExpectedResults(String expectedresults)
    {
        this.expectedresults = expectedresults;
    }

    public String getResults()
    {
        return results;
    }
    public void setResults(String results)
    {
        this.results = results;
    }

    public String getNotes()
    {
        return notes;
    }
    public void setNotes(String notes)
    {
        this.notes = notes;
    }

    public String getOperator()
    {
        return operator;
    }
    public void setOperator(String operator)
    {
        this.operator = operator;
    }

    public String getSteps()
    {
        return steps;
    }
    public void setSteps(String steps)
    {
        this.steps = steps;
    }

    public String getTesttime()
    {
        return testtime;
    }
    public void setTesttime(String testtime)
    {
        this.testtime = testtime;
    }

    public String getH1HourMeter()
    {
        return h1_hourmeter;
    }
    public void setH1HourMeter(String h1_hourmeter)
    {
        this.h1_hourmeter = h1_hourmeter;
    }

    public String getOdometer()
    {
        return odometer;
    }
    public void setOdometer(String odometer)
    {
        this.odometer = odometer;
    }

    public String getDngIrtc()
    {
        return dng_irtc;
    }
    public void setDngIrtc(String dng_irtc)
    {
        this.dng_irtc = dng_irtc;
    }

    public String getSynectTcNum()
    {
        return synect_tc_num;
    }
    public void setSynectTcNum(String synect_tc_num)
    {
        this.synect_tc_num = synect_tc_num;
    }

    public String getGroupTcNum()
    {
        return group_tc_num;
    }
    public void setGroupTcNum(String group_tc_num)
    {
        this.group_tc_num = group_tc_num;
    }

    public String getGuid()
    {
        return guid;
    }
    public void setGuid(String guid)
    {
        this.guid = guid;
    }

    public String getDate()
    {
        return date;
    }
    public void setDate(String date)
    {
        this.date = date;
    }

    public String getApplicationSoftwareTested()
    {
        return application_software_tested;
    }
    public void setApplicationSoftwareTested(String application_software_tested)
    {
        this.application_software_tested = application_software_tested;
    }

    public String getSoc()
    {
        return soc;
    }
    public void setSoc(String soc)
    {
        this.soc = soc;
    }

    public String getTruckSerialNumber()
    {
        return truck_serial_number;
    }
    public void setTruckSerialNumber(String truck_serial_number)
    {
        this.truck_serial_number = truck_serial_number;
    }

    public String getEvents()
    {
        return events;
    }
    public void setEvents(String events)
    {
        this.events = events;
    }

    public String getPassFailSkipBtnState()
    {
        return pass_fail_skip_btn_state;
    }
    public void setPassFailSkipBtnState(String pass_fail_skip_btn_state)
    {
        this.pass_fail_skip_btn_state = pass_fail_skip_btn_state;
    }

    public String getBatterySerialNumber()
    {
        return battery_serial_number;
    }
    public void setBatterySerialNumber(String battery_serial_number)
    {
        this.battery_serial_number = battery_serial_number;
    }

    @Override
    public String toString()
    {
        //return "TestCase [expectedresults=" + expectedresults +", results=" + results + ", notes=" + notes +", operator=" + operator +", steps=" + steps + ", testtime=" + testtime + ", h1_hourmeter=" + h1_hourmeter + ", odometer=" + odometer + ", dng_irtc=" + dng_irtc + ", synect_tc_num=" + synect_tc_num + ", group_tc_num=" + group_tc_num + ", guid=" + guid + ", date=" + date + " ]";
        return "TestCase [expectedresults=" + expectedresults +", results=" + results + ", notes=" + notes +", operator=" + operator +", steps=" + steps + ", testtime=" + testtime + ", h1_hourmeter=" + h1_hourmeter + ", odometer=" + odometer + ", dng_irtc=" + dng_irtc + ", synect_tc_num=" + synect_tc_num + ", group_tc_num=" + group_tc_num + ", guid=" + guid + ", date=" + date + ", truck_serial_number=" + truck_serial_number + ", soc=" + soc + ", application_software_tested=" + application_software_tested + " , events=" + events + ", pass_fail_skip_btn_state=" + pass_fail_skip_btn_state + ", battery_serial_number=" + battery_serial_number + " ]";
    }
}
