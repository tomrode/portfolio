package com.example.t0028919.person_in_looptesttool;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Message;
import android.os.PowerManager;
import android.provider.DocumentsContract;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.content.Intent;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.AccessControlContext;
import java.util.Objects;

import static android.app.PendingIntent.getActivity;
import static android.provider.Telephony.Mms.Part.FILENAME;
import static java.lang.Math.*;
import static java.security.AccessController.getContext;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.xml.datatype.Duration;


/**
 * Created by t0028919 on 1/18/2018.
 *
 * Author: Thomas Rode
 *
 * Intent: This Activity instructs the user to perform a given step in a test case. The "PASS/YES" or "FAIL/NO" must be hit to proceed to the next test case.
 *         User may input comments, Hourmeters, Odometer, text which will be saved. Buttons are self explanatory.
 */

public class Performtestcases extends Activity {

    // log message
    private static final String TAG = "TomsMessage";

    // request code to return from last activity
    private static final int REQUEST_CODE_FUNCTION = 100;

    // request code to return from Events activity
    private static final int REQUEST_CODE_EVENTS = 101;

    // pass fail button variables
    private static final String PASS = "PASS_YES";
    private static final String FAIL = "FAIL_NO";
    public String PassFailResult = "";


    // Count down timer pre-set value
    private static final long START_TIME_IN_MILLIS = 10000; //600000;
    private static final long THOUSAND_MILLISECONDS = 1000;
    private long mTimerLeftInMillis = START_TIME_IN_MILLIS;
    private long mCountDownInterval = THOUSAND_MILLISECONDS;
    private CountDownTimer mCountDownTimer;
    //private int mTimerUpTick;
    private int mTimerUpTck1;
    private String mTimerUpTick;

    // Previous button logic state
    public boolean PreviousButtonHit = false;

    // Forward button logic state
    public boolean  ForwardButtonHit = false;

    // Forward button lock out
    public boolean  ForwardButtonLockOut = false;

    // Lockout until checkbox and test completed
    public boolean  CompletionAndCheckedLockOut = false;

    // Lockout for checkbox logic
    public boolean CompletionAndCheckedLockOutPrevious = false;

    // This flag will be used to rewrite only one item at a time
    private String fReWrite = "false";

    // Test step count
    private int TestStepCnt;

    // Test step count used in previous/Forward button
    private int TestStepCntPrevFwd;

    // NumberOftestcases used to drive the logic in this activity
    private String NumberOfTestCases;

    // This variable is the operator note/comments
    private String GetNotes = "";

    // The operator
    private String CurrentOperator = "";

    // this flag determines if file should be saved while in progress
    public boolean fSaveWhileInProgress;

    // This flag is used to determine if testcase is to proceed, user must hit pass or fail each iteration
    public boolean fPassOrFailOrSkipBtnHit = false;

    // This flag is used to determine if The Events substring if current or not
    public boolean fWriteEventsSubString = false;

    // This flag is used to determine if one if Skip, SkipAll Pass or Fail was hit before next will work
    public boolean fSkipPassFailSkipAllHit;

    // This will be used for Current Battery Serial number
    private String CurrentBatterySerialNumber = "";

    private boolean fAllowNextBtnToupdateExecTestCases;

    // This flag is used st the end of test plan to transition nicely to last Activity locks out Skip,skipAll passs, fail btns
    private boolean fLockSkPaFaSaBtnsForCompletion = false;

    // Specific file with private access, no other apps can access this file
    String internalfilename = "";

    // UI variables
    TextView TvActiveFileName;
    TextView TvOperator;
    TextView TvSoftwareVersion;
    TextView TvTestStepsBox;
    TextView TvSynectTestCaseNum;
    TextView mTextViewCount;
    TextView TvGroupTestCase;
    TextView TVGroupTestCaseCnt;
    TextView TvCurrentTestStepCount;
    TextView TvDrawingRtcNumber;
    TextView TvNumberOfTestCases;
    TextView TvChangeBatterySerialNumber;
    EditText EtNotes;
    EditText EtOperator;
    EditText EtBatterySerialNumber;
    CheckBox CbTestPlanComplete;
    EditText EtOdometer;
    EditText EtH1HourMeter;
    Button BtPassYes;
    Button BtFailNo;
    Button BtSkip;
    Button BtNext;
    Button BtSkipAll;
    EditText EtSOC;
    EditText EtEvents;


    // Global variables to pass out of activity
    String gVAcvtiveFileName = "";
    String gVSoftwareVersion = "";
    String gVNumberOfTestCases = "";
    String gVGroupTestCaseNumber = "";
    String gVTestRequestNumber = "";
    String gVBatterySerialNumber = "";
    String gVTruckSerialNumber = "";
    String gVTestInProgress = "";
    String gVNumberOfExecutedTestCases = "";
    String gVFileToBeUsed = "";
    String gVTruckPickedInSpinner = "";
    String gVH1HourMeter = "";
    String gVOdometer = "";
    String gVEvents = "";
    String gVExecutionGUID = "";
    String gVStUserName = "";
    String gVStPassWord = "";
    String gVSOC = "";
    String gVGetNotes = "";
    String gVNewEvents = "";
    String gVPassFailSkipBtn;
    String gVOperator;

    // Global internal read variables
    String gVInternalFileTestInProgress = "";
    String gVInternalNumberOfExecutedTestCases = "";

    // An Array of TestCase classed variables declaration and instantiation
    TestCase TestCaseAr[]=new TestCase[2000];

    // SMB Server message
    public static TextView ServerConnectionPerformtestCases;

    // Initiate Wake-Lock
    private PowerManager.WakeLock mWakeLock = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "OnCreate in Performtestcases envoked" );   // debug msg
        setContentView(R.layout.performtestcases);

        // Initialize this Activity to the beginning
        TestStepCnt = 1;
        gVGroupTestCaseNumber = String.valueOf(TestStepCnt);

        // Setup UI
        TvActiveFileName = (TextView) findViewById(R.id.TvActiveFileName);
        TvOperator = (TextView) findViewById(R.id.TvOperator);
        TvSoftwareVersion = (TextView) findViewById(R.id.TvSoftwareVersion);
        TvTestStepsBox = (TextView) findViewById(R.id.TmTestStepsBox);
        TvSynectTestCaseNum = (TextView) findViewById(R.id.TvSynectTestCaseNumber);
        TvGroupTestCase = (TextView) findViewById(R.id.TvGroupTestCaseNumber);
        mTextViewCount = (TextView) findViewById(R.id.TvTextViewCount);
        EtNotes = (EditText) findViewById(R.id.EtComments);
        EtOperator = (EditText) findViewById(R.id.EtOperator);
        EtBatterySerialNumber = (EditText) findViewById(R.id.EtBatterySerialNumber);
        TvCurrentTestStepCount = (TextView) findViewById(R.id.TvCurrentTestStepCount);
        CbTestPlanComplete = (CheckBox) findViewById(R.id.CbTestPlanComplete);
        TvDrawingRtcNumber = (TextView)findViewById(R.id.TvDrawingRtcNumber);
        TvNumberOfTestCases = (TextView)findViewById(R.id.TvNumberOfTestCases);
        TvChangeBatterySerialNumber = (TextView)findViewById(R.id.TvChangeBatterySerialNumber);
        EtOdometer = (EditText)findViewById(R.id.EtOdometer);
        EtH1HourMeter = (EditText)findViewById(R.id.EtH1HourMeter);
        BtPassYes = (Button)findViewById(R.id.BtPassYes);
        BtFailNo = (Button)findViewById(R.id.BtFailNo);
        BtSkip = (Button)findViewById(R.id.BtSkip);
        BtSkipAll = (Button)findViewById(R.id.BtSkipAll);
        BtNext = (Button)findViewById(R.id.BtNext);
        ServerConnectionPerformtestCases = (TextView)findViewById(R.id.TvServerConnectionPerformtestcase);
        EtSOC = (EditText) findViewById(R.id.EtSOC);
        EtEvents = (EditText) findViewById(R.id.EtEvents);


        // set the checkbox to enabled
        CbTestPlanComplete.setChecked(false);
        // set the checkbox invisible until the test are complete
        CbTestPlanComplete.setVisibility(View.INVISIBLE);

        // Get previous variables from activity
        Intent intent = getIntent();

        // get value from MainActivity
        CurrentOperator = (intent.getStringExtra("OperatorName"));
        gVAcvtiveFileName = (intent.getStringExtra("ActiveFileName"));
        gVSoftwareVersion = (intent.getStringExtra("SoftwareVersion"));
        gVNumberOfTestCases = (intent.getStringExtra("NumberOfTestCases"));
        gVBatterySerialNumber = (intent.getStringExtra("BatterySerialNumber"));
        gVTestRequestNumber = (intent.getStringExtra("TestRequestNumber"));
        gVTruckSerialNumber = (intent.getStringExtra("TruckSerialNumber"));
        gVTestInProgress = (intent.getStringExtra("TestInProgress"));
        gVNumberOfExecutedTestCases = (intent.getStringExtra("NumberOfExecutedTestCases"));
        gVFileToBeUsed = (intent.getStringExtra("FileToBeUsed"));
        gVTruckPickedInSpinner = (intent.getStringExtra("TruckPickedInSpinner"));
        gVTestRequestNumber = (intent.getStringExtra("TestRequestNumber"));
        gVExecutionGUID = (intent.getStringExtra("ExecutionGUID"));

        // setTextView from MainActivity
        TvOperator.append(CurrentOperator);
        TvActiveFileName.append(gVAcvtiveFileName);
        TvSoftwareVersion.append(gVSoftwareVersion);
        TvNumberOfTestCases.append(gVNumberOfTestCases);
        TvChangeBatterySerialNumber.append(gVBatterySerialNumber);


        // read in from internal memeory to see if this activity reset
        internalfilename = gVAcvtiveFileName;
        readInternalMessage ();

        // read internal file to get Credentials
        readInternalCredentialMessage();

        // Determine if test was in-progress from before or the activity got reset
        if(gVTestInProgress.equals("In-Progress") && (!gVInternalFileTestInProgress.equals("In-Progress") ))
        {
            // Get the value from .json and add one since just the amount of executed but dont exceed it
            TestStepCnt = Integer.parseInt(gVNumberOfExecutedTestCases);
            if (gVNumberOfExecutedTestCases.equals(gVNumberOfTestCases) )
            {
                TestStepCnt =  Integer.parseInt(gVNumberOfTestCases);
            }
            else
            {
                TestStepCnt += 1;
            }

        }
        else if (gVInternalFileTestInProgress.equals("In-Progress") )
        {
            // Get the value from .json and add one since just the amount of executed
            TestStepCnt = Integer.parseInt(gVInternalNumberOfExecutedTestCases);

            if (gVInternalNumberOfExecutedTestCases.equals(gVNumberOfTestCases) )
            {
                TestStepCnt =  Integer.parseInt(gVNumberOfTestCases);
            }
            else
            {
                TestStepCnt += 1;
            }

        }


        // get Serializer data from entire class worth of data this works
        int i;
        String Serializer = "";
        for (i=1; i<= Integer.parseInt(gVNumberOfTestCases); i++)
        {
            Serializer = ("TC" + i + "MainActivity");
            TestCaseAr[i] = (TestCase) intent.getSerializableExtra(Serializer);
        }

        // clear out old value, set the Current test step count
        TvCurrentTestStepCount.setText("");
        TvCurrentTestStepCount.append(String.valueOf(TestStepCnt));

        TvSynectTestCaseNum.setText("");
        TvSynectTestCaseNum.append(TestCaseAr[TestStepCnt].getSynectTcNum());

        TvDrawingRtcNumber.setText("");
        TvDrawingRtcNumber.append(TestCaseAr[TestStepCnt].getDngIrtc());

        TvGroupTestCase.setText("");
        TvGroupTestCase.append(TestCaseAr[TestStepCnt].getGroupTcNum());

        // set text Multiline from Mainactivity
        TvTestStepsBox.append(TestCaseAr[TestStepCnt].getSteps());

        // How many test cases
        NumberOfTestCases = gVNumberOfTestCases;

        // Set the test In-Progress
        gVTestInProgress = "In-Progress";
        gVInternalFileTestInProgress = "In-Progress";

        // Determine if a Activity reset happened
        if(savedInstanceState != null)
        {
            mTimerUpTck1 = savedInstanceState.getInt("SavedCount");
        }

        // call the timer initially
        Timer();

        // Set the save while test still in-progress flag to false
        fSaveWhileInProgress = false;

        // NEW******** Remove current test file on Ready_for_Execution in EngSTL1 server **********
        String StUserName = gVStUserName;
        String StPassWord = gVStPassWord;
        SendDeleteReadyForExecutionData process = new SendDeleteReadyForExecutionData();
        process.execute(StUserName, StPassWord, gVFileToBeUsed, gVTruckPickedInSpinner);

        // call the initial internal file for lost keys check to see if we can correct issue here
        File file = new File(getFilesDir(),(internalfilename));
        //File file = new File(getFilesDir(),(internalfilename + "_Initial"));
        if(file.exists())
        {
            readInternalLostKeys();
        }

        // Under Construction see if this works
        CbTestPlanComplete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                // Lockout until checkbox and test completed
                //boolean  CompletionAndCheckedLockOutPrevious = false;

                if( (CbTestPlanComplete.isChecked()) &&  (CompletionAndCheckedLockOut == true) )
                {
                    Toast.makeText(getApplicationContext(), "Check box checked and Test is Finished "  ,Toast.LENGTH_LONG).show();

                    // Set the Next button green to prompt user
                    BtNext.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green
                    // This flag will un-lock out Next Button until set true
                    fSkipPassFailSkipAllHit = true;
                    // Check for next time around
                    CompletionAndCheckedLockOutPrevious = true;
                }
                else if ( (CompletionAndCheckedLockOutPrevious == true) )
                {
                    // Check for next time around
                    CompletionAndCheckedLockOutPrevious = false;
                    // Set the Next button grey to prompt user
                    BtNext.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is grey
                    // This flag will un-lock out Next Button until set true
                    fSkipPassFailSkipAllHit = false;
                }
                // update your model (or other business logic) based on isChecked
            }
        });

        // Add listener for SMB connection message
        ServerConnectionPerformtestCases.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if(s.toString().equals("Successful copied EngSTL1:Completed_Execution"))
                {
                    Toast.makeText(getApplicationContext(), "Successful copied EngSTL1:Completed_Execution",Toast.LENGTH_LONG).show();

                    // This will transition over to Exporttestresults activity
                    Intent i = new Intent(Performtestcases.this, Exporttestresults.class);

                    // This passes the individual variables to new Exporttestresults activity
                    i.putExtra("ActiveFileName", gVAcvtiveFileName);
                    i.putExtra("SoftwareVersion", gVSoftwareVersion);

                    // Goto Exporttestresults activity
                    startActivityForResult(i, REQUEST_CODE_FUNCTION);

                }
                if(s.toString().equals("Successful write, to Server In-Progress folder"))
                {
                    //Toast.makeText(getApplicationContext(), "Successful write, to Server In-Progress folder",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        // End Construction
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        // call the timer initially

        Log.i(TAG, "OnStart in Performtestcases envoked " );   // debug msg

        //Timer();

    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onStop()
    {
        super.onStop();

        // Pausing
        Log.i(TAG, "OnStop in Performtestcases envoked" );   // debug msg

        // See if flag sets for low power mode
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (powerManager.isPowerSaveMode())
        {
            Log.i(TAG, "OnPause in Performtestcases in PowersaveMode" );   // debug msg
        }

        // Initiate Wake-Lock, PARTIAL_WAKE_LOCK: CPU on, Screem Off, Keyboard off
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        mWakeLock.acquire(3600000);

        // See if a save of internal file initial
        internalfilename = internalfilename ;
        writeInternalMessage();
        if(CbTestPlanComplete.isChecked() )
        {
            // ensure this file is deleted once this is checked
            File file = new File(getFilesDir(),internalfilename);
            deleteFile(internalfilename);
            boolean deleted = file.delete();
        }
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        // Pausing
        Log.i(TAG, "OnPause in Performtestcases envoked" );   // debug msg
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // Resume
        Log.i(TAG, "OnResume in Performtestcases envoked" );   // debug msg
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onDestroy()
    {

        if (mWakeLock.isHeld())
            mWakeLock.release();
        // call the superclass method first
        super.onDestroy();
        // Resume
        Log.i(TAG, "OnDestroy in Performtestcases envoked" );   // debug msg
    }

    // The Next Button
    public void onButtonNextClick(View v)
    {
        // See if Prevooius or Forward button hit to allow user to determine if they want to go any further
        if( (v.getId() == R.id.BtNext) && ((PreviousButtonHit) || (ForwardButtonHit)) )
        {
            // Indicate to user
            Toast.makeText(getApplicationContext(), " HITTING NEXT BUTTON AGAIN WILL OVER WRITE CURRENT STEP AND RESET -> NumberOfExecutedTestCases = " + TestStepCnt ,Toast.LENGTH_LONG).show();

            // Previous button not hit
            PreviousButtonHit = false;

            // Forward button not hit
            ForwardButtonHit = false;
        }
        else if( (v.getId() == R.id.BtNext) && (fSkipPassFailSkipAllHit == true)  )
        {
            // Next button thinks its this Teststep
            //Toast.makeText(getApplicationContext(), " Next button current teststep ->" + TestStepCnt ,Toast.LENGTH_LONG).show();

            // this flag allows NumberOfExecutedtestcases to increase
            fAllowNextBtnToupdateExecTestCases = true;

            // relockout this button until flag from others set true
            fSkipPassFailSkipAllHit = false;

            // Previous button not hit
            PreviousButtonHit = false;

            // Forward button not hit
            ForwardButtonHit = false;

            // Reinstate the Green button
            BtPassYes.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

            // Reinstate the Red button
            BtFailNo.setBackgroundColor(Color.rgb(0xFF, 0x00, 0x00)); //SET YOUR COLOR this code is Red

            // Reinstate the Skip button
            BtSkip.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

            // Reinstate the SkipAll button
            BtSkipAll.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

            // Reinstate the Next button
            BtNext.setBackgroundColor(Color.rgb(0xE0, 0xE0, 0xE0)); //SET YOUR COLOR this code is grey

            // If count under threshold then its a valid test step otherwise it reached the end, exit this activity
            if (TestStepCnt <= Integer.parseInt(NumberOfTestCases))
            {

                // This is used for In_Progress Windows server folder this will write each time if In_Progress folders exist on device
                String StUserName = gVStUserName;
                String StPassWord = gVStPassWord;
                SendInProgressData process = new SendInProgressData();
                process.execute(StUserName, StPassWord, gVFileToBeUsed, gVTruckPickedInSpinner, String.valueOf(TestStepCnt));

                // Call the TestStep Driver
                //Toast.makeText(getApplicationContext(), "On Next button TestStepCnt -> " + TestStepCnt ,Toast.LENGTH_SHORT).show();
                TestSteps(TestStepCnt, mTimerUpTick);

                // Issue with Keyboard appearing within Input text and resetting Activity, this will save and resume if this happens in the right spot
                // Set the save while test still in-progress flag to false
                fSaveWhileInProgress = true;

                // Write both internal\external file with .json partially completed
                writeExternalMessage();
                writeInternalMessage();

                // Reset the Test execution time counter
                mTimerUpTick = "0";
                mTimerUpTck1 = 0;
            } else if (TestStepCnt > Integer.parseInt(NumberOfTestCases))
            {
                // Quick test latch in last state
                TestStepCnt = Integer.parseInt(NumberOfTestCases);

                // This will transition over to Exporttestresults activity
                /*Intent i = new Intent(Performtestcases.this, Exporttestresults.class);

                // This passes the individual variables to new Exporttestresults activity
                i.putExtra("ActiveFileName", gVAcvtiveFileName);
                i.putExtra("SoftwareVersion", gVSoftwareVersion);
                */
                // Write file to External memory NEW
                writeExternalMessage();
                writeInternalMessage();

                //See if check box is hit to write given truck .json or not
                if(CbTestPlanComplete.isChecked() )
                {
                    // Write file to External memory NEW
                    writeExternalMessage();
                    writeInternalMessage();

                    // unlock the Next button
                    //fSkipPassFailSkipAllHit = true;

                    // Set the test In-Progress
                    gVTestInProgress = "Finished";
                    gVInternalFileTestInProgress = "Finished";
                    gVInternalNumberOfExecutedTestCases = "0";
                    writeInternalMessage();

                    fSaveWhileInProgress = false;

                    // Delete file and from the In-Progress folder
                    File Root = Environment.getExternalStorageDirectory();
                    File filedel = new File(( Root+ "/PIL_SYNECT_TEST_PLAN/In_Progress/" + gVTruckPickedInSpinner + "/"  + gVFileToBeUsed));
                    boolean deleted1 = filedel.delete();

                    // Delete The In-Progress file in Server
                    String StUserName = gVStUserName;
                    String StPassWord = gVStPassWord;
                    SendInProgressDataDelete process = new SendInProgressDataDelete();
                    process.execute(StUserName, StPassWord, gVFileToBeUsed, gVTruckPickedInSpinner);

                    // Delete internal file instance
                    //Toast.makeText(getApplicationContext(), "Internal file deleted:  " + internalfilename ,Toast.LENGTH_LONG).show();
                    File file = new File(getFilesDir(),internalfilename);
                    deleteFile(internalfilename);
                    boolean deleted = file.delete();

                }

                // Write file to External memory
                writeExternalMessage();
                //writeInternalMessage();

                // this flag dis-allows NumberOfExecutedtestcases to increase
                fAllowNextBtnToupdateExecTestCases = false;

                // Goto Exporttestresults activity
                //startActivityForResult(i, REQUEST_CODE_FUNCTION);
            }
        }
    }



    // STACK OVERFLOW SOLUTION REMOVE ME IF NOGO
    private static boolean canWriteToExternalStorage(Context context) {
        return ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    // This method will write output results to the external storage
    public void writeExternalMessage()
    {
        String State;
        String ExternalFileName = "ExternalTestResultsFile.json";
        String ExternalPath = "";
        String ExternalTruckSubFolder = "";
        String TC = "TC";
        String Notes = "Comment";  //                //"Notes";
        String Operator = "Operator"; //
        String Steps = "Steps"; //
        String Testtime = "Execution Duration";  //  //"Testtime";
        String Expectedresults = "ExpectedResult"; //
        String Results = "Verdict";  //              //"Result";
        String H1_Hourmeter = "H1_Hourmeter"; //
        String Odometer = "Odometer"; //
        String SynectTcNum = "TestCaseNumber"; //
        String GroupTcNum = "Name";//"Group#"; //
        String DngIrtc = "DNG_iRTC#"; //
        String Guid = "GUID"; //
        String Date = "Date"; //
        String Truck_Serial_Number = "TruckSerialNumber";//
        String SOC = "SOC_FUEL"; //
        String Application_Software_Tested = "Application Software Tested"; //
        String Events = "Events";
        String BatterySerialNumber = "BatterySerialNumber"; // Place Battery Serial Number

        int i;
        int j;
        int  NextTestStepCnt = 0;
        // Previous button logic state
        boolean fTestCasesAfter = false;

        // Get the truck subfolder from global variable
        ExternalTruckSubFolder = gVTruckPickedInSpinner;

        // Determine if In-Progress folder needs to be written
        if (fSaveWhileInProgress == true)
        {   // Get the file name from Global variable
            ExternalFileName = ( gVFileToBeUsed  );
            // name of sub-folder
            ExternalPath = "/PIL_SYNECT_TEST_PLAN/In_Progress";
        }
        else if (gVTestInProgress == "Finished")
        {
            // Get the file name from Global variable
            ExternalFileName = (  gVFileToBeUsed );

            // name of sub-folder
            ExternalPath = "/PIL_SYNECT_TEST_PLAN/Completed_Execution";

            //Toast.makeText(getApplicationContext(), "External File at finished state: "   ,Toast.LENGTH_SHORT).show();
        }

        // Convert variables back as JSONObjects
        JSONObject jsonObject = new JSONObject();

        try {                      // Key                        // Value
            jsonObject.put("ActiveTestFileName",           gVAcvtiveFileName);
            jsonObject.put("Application Software Tested",  gVSoftwareVersion);
            jsonObject.put("NumberOfTestCases",            gVNumberOfTestCases);
            //jsonObject.put("BatterySerialNumber",          gVBatterySerialNumber);
            jsonObject.put("TestRequestNumber",            gVTestRequestNumber);
            jsonObject.put("TruckSerialNumber",            gVTruckSerialNumber);
            jsonObject.put("TestInProgress",               gVTestInProgress);
            jsonObject.put("NumberOfExecutedTestCases",    gVNumberOfExecutedTestCases);
            jsonObject.put("ExecutionGUID",                gVExecutionGUID);

            // This area is for adding in the TestCase class attributes
            for (i = 1; i <= Integer.parseInt(gVNumberOfTestCases); i++)
            {
                // ReWrite only these at this step
                if( (fReWrite.equals("true")) && (i == TestStepCnt) )
                {

                    // This is the TestCase that changed
                    NextTestStepCnt = TestStepCnt +1;
                    jsonObject.put(TC + TestStepCnt + Odometer, TestCaseAr[TestStepCnt].getOdometer()); // TC + TestStepCnt
                    jsonObject.put(TC + TestStepCnt + SOC, TestCaseAr[TestStepCnt].getSoc());
                    jsonObject.put(TC + TestStepCnt + Notes, TestCaseAr[TestStepCnt].getNotes());
                    jsonObject.put(TC + TestStepCnt + H1_Hourmeter, TestCaseAr[TestStepCnt].getH1HourMeter());
                    jsonObject.put(TC + TestStepCnt + Events, TestCaseAr[TestStepCnt].getEvents());
                    jsonObject.put(TC + TestStepCnt + Operator, TestCaseAr[TestStepCnt].getOperator());
                    jsonObject.put(TC + TestStepCnt + BatterySerialNumber, TestCaseAr[TestStepCnt].getBatterySerialNumber());

                    j = i;
                }
                else
                {

                    // this will hopefully realign these items this really is moot now with the i but Ill leave alone
                    j = ((fReWrite.equals("true")) && ( i >= TestStepCnt ))?(i):(i);//+1):(i);


                    jsonObject.put(TC + j + Notes, TestCaseAr[i].getNotes());
                    jsonObject.put(TC + j + H1_Hourmeter, TestCaseAr[i].getH1HourMeter());
                    jsonObject.put(TC + j + Odometer, TestCaseAr[i].getOdometer());
                    jsonObject.put(TC + j + SOC, TestCaseAr[i].getSoc());
                    jsonObject.put(TC + j + Events, TestCaseAr[i].getEvents());
                    jsonObject.put(TC + j + Operator, TestCaseAr[i].getOperator());
                    jsonObject.put(TC + j + BatterySerialNumber, TestCaseAr[i].getBatterySerialNumber());

                    // put the index back for all the rest
                    j = i;
                }


                // Format the Key name to follow the Testcase
                jsonObject.put(TC + j + Expectedresults, TestCaseAr[i].getExpectedResults());
                jsonObject.put(TC + j + Results, TestCaseAr[i].getResults());
                jsonObject.put(TC + j + Steps, TestCaseAr[i].getSteps());
                jsonObject.put(TC + j + Testtime, TestCaseAr[i].getTesttime());
                jsonObject.put(TC + j + SynectTcNum, TestCaseAr[i].getSynectTcNum());
                jsonObject.put(TC + j + GroupTcNum, TestCaseAr[i].getGroupTcNum());
                jsonObject.put(TC + j + DngIrtc, TestCaseAr[i].getDngIrtc());
                jsonObject.put(TC + j + Guid, TestCaseAr[i].getGuid());
                jsonObject.put(TC + j + Date, TestCaseAr[i].getDate());
                jsonObject.put(TC + j + Truck_Serial_Number, TestCaseAr[i].getTruckSerialNumber());
                jsonObject.put(TC + j + Application_Software_Tested, TestCaseAr[i].getApplicationSoftwareTested());
                //jsonObject.put(TC + j + BatterySerialNumber, TestCaseAr[i].getBatterySerialNumber());

            }

            // ReWrite only these at this step
            /*if (fReWrite.equals("true"))
            {
                jsonObject.put(TC + NextTestStepCnt + Odometer, TestCaseAr[NextTestStepCnt].getOdometer());
                jsonObject.put(TC + NextTestStepCnt + SOC, TestCaseAr[NextTestStepCnt].getSoc());
                jsonObject.put(TC + NextTestStepCnt + Notes, TestCaseAr[NextTestStepCnt].getNotes());
                jsonObject.put(TC + NextTestStepCnt + H1_Hourmeter, TestCaseAr[NextTestStepCnt].getH1HourMeter());
                jsonObject.put(TC + NextTestStepCnt + Events, TestCaseAr[NextTestStepCnt].getEvents());
                jsonObject.put(TC + NextTestStepCnt + Operator, TestCaseAr[NextTestStepCnt].getOperator());
            }*/

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Check if SD card installed
        State = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(State))
        {
            // Create a folder
            File Root = Environment.getExternalStorageDirectory();

            File Dir = new File (Root. getAbsolutePath() + ExternalPath + "/" + ExternalTruckSubFolder);

            // See if folder exists then make it
            if(!Dir.exists() )
            {
                Dir.mkdirs();
                //Toast.makeText(getApplicationContext(), " In Dir.mkdirs(); "  ,Toast.LENGTH_SHORT).show();
            }

            File file = new File(Dir,ExternalFileName);
            String Message = jsonObject.toString();
            // Add back in the []
            Message = ("[" + Message + "]");

            try
            {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                fileOutputStream.write(Message.getBytes());
                fileOutputStream.close();
            }
            catch (FileNotFoundException e)
            {
                //Toast.makeText(getApplicationContext(), "  FileNotFoundException " + e ,Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "SD Card not found "  ,Toast.LENGTH_SHORT).show();
        }
    }



    // This method will write output results to the internal storage This is not as maintained
    public void writeInternalMessage()
    {
        // Specific file with private access, no other apps can access this file
        //String internalfilename = "InternalTestResultsFileInProgress";
        String string = "";
        String TC = "TC";
        String Notes = "Comment";
        String Operator = "Operator";
        String Steps = "Steps";
        String Testtime = "Execution Duration";
        String Expectedresults = "ExpectedResult";
        String Results = "Verdict";
        String H1_Hourmeter = "H1_Hourmeter";
        String Odometer = "Odometer";
        String SynectTcNum = "TestCaseNumber";
        String GroupTcNum = "Name";//"Group#";
        String DngIrtc = "DNG_iRTC#";
        String Guid = "GUID";
        String Date = "Date";
        String Truck_Serial_Number = "TruckSerialNumber";
        String SOC = "SOC_FUEL";
        String Application_Software_Tested = "Application Software Tested";
        String Events = "Events";
        String PassFailSkipBtn = "PassFailSkipBtn";
        String BatterySerialNumber = "BatterySerialNumber"; // Place Battery Serial Number
        int NextTestStepCnt = 0;
        int i;
        int j;

        // Convert variables back as JSONObjects
        JSONObject jsonObject = new JSONObject();

        try {                      // Key                       // Value
            jsonObject.put("ActiveTestFileName",          gVAcvtiveFileName);
            jsonObject.put("Application Software Tested", gVSoftwareVersion);
            jsonObject.put("NumberOfTestCases",           gVNumberOfTestCases);
            //jsonObject.put("BatterySerialNumber",         gVBatterySerialNumber);
            jsonObject.put("TestRequestNumber",           gVTestRequestNumber);
            jsonObject.put("TruckSerialNumber",           gVTruckSerialNumber);
            jsonObject.put("TestInProgress",              gVInternalFileTestInProgress);//gVTestInProgress);
            jsonObject.put("NumberOfExecutedTestCases",   gVInternalNumberOfExecutedTestCases);//gVNumberOfExecutedTestCases);
            jsonObject.put("ExecutionGUID",               gVExecutionGUID);

            // This area is for adding in the TestCase class attributes lets write only new items with gVNumberOfExecutedTestCases as reference point
            for (i = 1; i <= Integer.parseInt(gVNumberOfTestCases); i++)
            //for (i = Integer.parseInt(gVNumberOfExecutedTestCases); i <= Integer.parseInt(gVNumberOfTestCases); i++)
            {
                // ReWrite only these at this step
                if( (fReWrite.equals("true")) && (i == TestStepCnt) )
                {
                    // Write the next one
                    NextTestStepCnt = TestStepCnt + 1;
                    jsonObject.put(TC + TestStepCnt + Odometer, TestCaseAr[TestStepCnt].getOdometer());  //TC + TestStepCnt for all
                    jsonObject.put(TC + TestStepCnt + SOC, TestCaseAr[TestStepCnt].getSoc());
                    jsonObject.put(TC + TestStepCnt + Notes, TestCaseAr[TestStepCnt].getNotes());
                    jsonObject.put(TC + TestStepCnt + H1_Hourmeter, TestCaseAr[TestStepCnt].getH1HourMeter());
                    jsonObject.put(TC + TestStepCnt + Events, TestCaseAr[TestStepCnt].getEvents());
                    jsonObject.put(TC + TestStepCnt + Operator, TestCaseAr[TestStepCnt].getOperator());
                    jsonObject.put(TC + TestStepCnt + PassFailSkipBtn, TestCaseAr[TestStepCnt].getPassFailSkipBtnState());
                    jsonObject.put(TC + TestStepCnt + BatterySerialNumber, TestCaseAr[TestStepCnt].getBatterySerialNumber());
                    // put the index back for all the rest
                    j = i;

                }
                else
                {
                    // this will hopefully realign these items this really is moot now with the i but Ill leave alone
                    j = ((fReWrite.equals("true")) && ( i >= TestStepCnt ))?(i):(i);//+1):(i);

                    jsonObject.put(TC + j + Notes, TestCaseAr[i].getNotes());
                    jsonObject.put(TC + j + H1_Hourmeter, TestCaseAr[i].getH1HourMeter());
                    jsonObject.put(TC + j + Odometer, TestCaseAr[i].getOdometer());
                    jsonObject.put(TC + j + SOC, TestCaseAr[i].getSoc());
                    jsonObject.put(TC + j + Events, TestCaseAr[i].getEvents());
                    jsonObject.put(TC + j + Operator, TestCaseAr[i].getOperator());
                    jsonObject.put(TC + j + PassFailSkipBtn, TestCaseAr[i].getPassFailSkipBtnState());
                    jsonObject.put(TC + j + BatterySerialNumber, TestCaseAr[i].getBatterySerialNumber());
                    // Lets see if this get written
                    //jsonObject.put(TC + j + Date, TestCaseAr[i].getDate());

                    // put the index back for all the rest
                    j = i;
                }

                // Format the Key name to follow the Testcase replace i with j to make json key
                jsonObject.put(TC + j + Expectedresults, TestCaseAr[i].getExpectedResults());
                jsonObject.put(TC + j + Results, TestCaseAr[i].getResults());
                jsonObject.put(TC + j + Steps, TestCaseAr[i].getSteps());
                jsonObject.put(TC + j + Testtime, TestCaseAr[i].getTesttime());
                jsonObject.put(TC + j + SynectTcNum, TestCaseAr[i].getSynectTcNum());
                jsonObject.put(TC + j + GroupTcNum, TestCaseAr[i].getGroupTcNum());
                jsonObject.put(TC + j + DngIrtc, TestCaseAr[i].getDngIrtc());
                jsonObject.put(TC + j + Guid, TestCaseAr[i].getGuid());
                jsonObject.put(TC + j + Date, TestCaseAr[i].getDate());
                jsonObject.put(TC + j + Truck_Serial_Number, TestCaseAr[i].getTruckSerialNumber());
                jsonObject.put(TC + j + Application_Software_Tested, TestCaseAr[i].getApplicationSoftwareTested());
                //jsonObject.put(TC + j + BatterySerialNumber, TestCaseAr[i].getBatterySerialNumber());
            }

            // rewrite only these
            // ReWrite only these at this step
            /*if (fReWrite.equals("true"))
            {
                jsonObject.put(TC + NextTestStepCnt + Odometer, TestCaseAr[NextTestStepCnt].getOdometer());
                jsonObject.put(TC + NextTestStepCnt + SOC, TestCaseAr[NextTestStepCnt].getSoc());
                jsonObject.put(TC + NextTestStepCnt + Notes, TestCaseAr[NextTestStepCnt].getNotes());
                jsonObject.put(TC + NextTestStepCnt + H1_Hourmeter, TestCaseAr[NextTestStepCnt].getH1HourMeter());
                jsonObject.put(TC + NextTestStepCnt + Events, TestCaseAr[NextTestStepCnt].getEvents());
                jsonObject.put(TC + NextTestStepCnt + PassFailSkipBtn, TestCaseAr[NextTestStepCnt].getPassFailSkipBtnState());
                jsonObject.put(TC + NextTestStepCnt + Operator, TestCaseAr[NextTestStepCnt].getOperator());
            }*/


        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Write the file with JSON data
        FileOutputStream outputStream;

        try
        {
            outputStream = openFileOutput(internalfilename, MODE_PRIVATE);
            string = jsonObject.toString();
            outputStream.write(string.getBytes());

            outputStream.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        // set the write flag back
        fReWrite = "false";
    }

    public void readInternalMessage ()
    {
        String TC = "TC";
        String Notes = "Comment";
        String H1_Hourmeter = "H1_Hourmeter";
        String SOC = "SOC_FUEL";
        String Odometer = "Odometer";
        String Events = "Events";
        String PassFailSkipBtn = "PassFailSkipBtn";
        String Operator = "Operator";
        String Date = "Date";
        String Testtime = "Execution Duration";
        String BatterySerialNumber = "BatterySerialNumber"; // Place Battery Serial Number


        //Toast.makeText(getApplicationContext(), " ReadOut from Internal File" , Toast.LENGTH_SHORT).show();
        Log.i(TAG, "Internal file read" );   // debug msg
        // Read out file which will eventually be exported out
        try
        {
            String Message;
            FileInputStream fileInputStream = openFileInput(internalfilename);//"InternalTestResultsFileInProgress");
            InputStreamReader inputStreamReader= new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while ( (Message = bufferedReader.readLine()) != null )
            {
                stringBuffer.append(Message + "\n");
            }

            try
            {
                //Toast.makeText(getApplicationContext(), " ReadOut from Internal File in the TRY" , Toast.LENGTH_SHORT).show();
                // The Previous or Forward button hit therefore fill in these items back to Activity
                //if( (PreviousButtonHit ) || ( ForwardButtonHit ) )
                //{
                    JSONObject mainObject = new JSONObject(String.valueOf(stringBuffer));

                    // Get data out from internal saved file for previous user comment
                    gVGetNotes = mainObject.getString(TC + TestStepCnt + Notes);

                    // Get data out from internal saved file for previous SOC
                    gVSOC = mainObject.getString(TC + TestStepCnt + SOC);

                    // Get data out of internal saved file for previous H1_hourmeter
                    gVH1HourMeter =  mainObject.getString(TC + TestStepCnt + H1_Hourmeter);

                    // Get data out of internal saved file for previous Odometer
                    gVOdometer =  mainObject.getString(TC + TestStepCnt + Odometer);

                    // Get data out of internal saved file for previous Odometer
                    gVNewEvents =  mainObject.getString(TC + TestStepCnt + Events);

                    // Get data out of internal saved file for which of these buttons hit
                    gVPassFailSkipBtn =  mainObject.getString(TC + TestStepCnt + PassFailSkipBtn);

                    // Get data out of internal saved file for the operator
                    gVOperator =  mainObject.getString(TC + TestStepCnt + Operator);

                    // Get data out of internal saved file for the Battery Serial Number
                    gVBatterySerialNumber = mainObject.getString(TC + TestStepCnt + BatterySerialNumber);

                    // Get data out from internal saved file
                    gVInternalFileTestInProgress = mainObject.getString("TestInProgress");
                    gVInternalNumberOfExecutedTestCases = mainObject.getString("NumberOfExecutedTestCases");

                    // See if the date is retreived
                    // This area is for adding in the TestCase class attributes This may be a way to place this back into TestCaseAr
                    Log.i(TAG, "Internal Read before date and gVNumberOfTestCases -----> " + gVNumberOfTestCases );   // debug msg

            } catch (JSONException e)
            {
                e.printStackTrace();
                Log.i(TAG, "Internal file read second Try" + e );   // debug msg
                Log.i(TAG, "Internal file read second Try internal file" + internalfilename );   // debug msg

            }


        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
            Log.i(TAG, "Internal file read first Try" + e );   // debug msg
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // Under Construction
    public void readInternalLostKeys ()
    {
        // Read out file which will eventually be exported out
        try
        {
            String Message;
            FileInputStream fileInputStream = openFileInput(internalfilename );//+ "_Initial");
            InputStreamReader inputStreamReader= new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while ( (Message = bufferedReader.readLine()) != null )
            {
                stringBuffer.append(Message + "\n");

            }

            try
            {
                JSONObject mainObject = new JSONObject(String.valueOf(stringBuffer));

                // Get data out from internal saved file for a given Testcase
                int i;
                for (i = 1; i <= Integer.parseInt(gVNumberOfExecutedTestCases); i++)
                {
                    Log.i(TAG, "Internal Read of SOC_FUEL from Initial file -----> " + mainObject.getString("TC" + i + "SOC_FUEL"));
                    Log.i(TAG, "Internal Read of Events from Initial file -----> " + mainObject.getString("TC" + i + "Events"));
                    Log.i(TAG, "Internal Read of Date from Initial file -----> " + mainObject.getString("TC" + i + "Date"));
                    Log.i(TAG, "Internal Read of TruckSerialNumber from Initial file -----> " + mainObject.getString("TC" + i + "TruckSerialNumber"));
                    Log.i(TAG, "Internal Read of Application Software Tested from Initial file -----> " + mainObject.getString("TC" + i + "Application Software Tested"));
                    Log.i(TAG, "Internal Read of PassFailSkipBtn from Initial file -----> " + mainObject.getString("TC" + i + "PassFailSkipBtn"));

                    //  Rewrite TestCase Ar
                    TestCaseAr[i].setSoc(mainObject.getString("TC" + i + "SOC_FUEL"));
                    TestCaseAr[i].setEvents(mainObject.getString("TC" + i + "Events"));
                    TestCaseAr[i].setDate(mainObject.getString("TC" + i + "Date"));
                    TestCaseAr[i].setTruckSerialNumber(mainObject.getString("TC" + i + "TruckSerialNumber"));
                    TestCaseAr[i].setApplicationSoftwareTested(mainObject.getString("TC" + i + "Application Software Tested"));
                    TestCaseAr[i].setPassFailSkipBtnState(mainObject.getString("TC" + i + "PassFailSkipBtn"));

                    // These items already there below may not be necessary
                    /*TestCaseAr[i].setNotes(mainObject.getString("TC" + i + "Comment"));
                    TestCaseAr[i].setH1HourMeter(mainObject.getString("TC" + i + "H1_Hourmeter"));
                    TestCaseAr[i].setOdometer(mainObject.getString("TC" + i + "Odometer"));
                    TestCaseAr[i].setOperator(mainObject.getString("TC" + i + "Operator"));
                    TestCaseAr[i].setExpectedResults(mainObject.getString("TC" + i + "ExpectedResult"));
                    TestCaseAr[i].setResults(mainObject.getString("TC" + i + "Verdict"));
                    TestCaseAr[i].setSteps(mainObject.getString("TC" + i + "Steps"));
                    TestCaseAr[i].setTesttime(mainObject.getString("TC" + i + "Execution Duration"));
                    TestCaseAr[i].setSynectTcNum(mainObject.getString("TC" + i + "TestCaseNumber"));
                    TestCaseAr[i].setGroupTcNum(mainObject.getString("TC" + i + "Name"));
                    TestCaseAr[i].setDngIrtc(mainObject.getString("TC" + i + "DNG_iRTC#"));
                    TestCaseAr[i].setGuid(mainObject.getString("TC" + i + "GUID"));
                    */
                }

            } catch (JSONException e)
            {
                e.printStackTrace();
            }


        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }


    // End Construction
    public void readInternalCredentialMessage ()
    {
        // Read out file which will eventually be exported out
        try
        {
            String Message;
            FileInputStream fileInputStream = openFileInput("InternalCredentialsFile");
            InputStreamReader inputStreamReader= new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while ( (Message = bufferedReader.readLine()) != null )
            {
                stringBuffer.append(Message + "\n");

            }

            try
            {
                JSONObject mainObject = new JSONObject(String.valueOf(stringBuffer));//JSONStringWithoutNull);;
                // Get data out from internal saved file
                gVStUserName = mainObject.getString("UserName");
                gVStPassWord = mainObject.getString("Password");
                Log.i(TAG, " ReadOut from Internal File InternalCredentialsFile" + gVStUserName + gVStPassWord );   // debug msg

            } catch (JSONException e)
            {
                e.printStackTrace();
            }


        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // This method will convert raw seconds to hours:minutes:seconds
    public String TimeConvert(long InputSecondsLong)
    {
        double Seconds = 0;
        double Minutes = 0;
        String TensSeconds = "0";
        String SecondsCombined;
        String TensMinutes = "0";
        String MinutesCombined;
        Integer Hours;
        String TensHours = "0";
        String HoursCombined;
        String FormatedTime = "" ;
        int intmin = 0;
        int intsec = 0;

        // Calculate the Hours
        if( InputSecondsLong >= 3600 )
        {
            Hours = (int)InputSecondsLong / 3600;

            // Is there ten hours and greater to add the HH place holder in the tens spot?
            if(Hours < 10)
            {
                HoursCombined = (TensHours + String.valueOf(Hours));
            }
            else
            {
                HoursCombined = (String.valueOf(Hours));
            }
        }
        else
        {
            Hours = 0;
            HoursCombined = (TensHours + String.valueOf(Hours));
        }

        // Calculate Minutes
        if( InputSecondsLong >= 60)
        {
            // round on ly to the whole number and recast
            Seconds = (int)InputSecondsLong - (floor(Hours) * 3600);
            Minutes = round((int)Seconds) / 60;
            intmin = (int)Minutes;

            // Is there ten minutes and greater to add the MM place holder in the tens spot?
            if (intmin < 10)
            {
                MinutesCombined = (TensMinutes + String.valueOf(intmin));
            }
            else
            {
                MinutesCombined = (String.valueOf(intmin));
            }

            // Calculate the seconds
            Seconds = (Seconds - (Minutes * 60));
            intsec = (int)Seconds;
        }
        else
        {
            Minutes = 0;
            MinutesCombined = (TensMinutes + String.valueOf(intmin));

            // Calculate the seconds
            Seconds = (int)InputSecondsLong;
            intsec = (int)Seconds;
        }

        // Is there ten seconds and greater to add the SS place holder in the tens spot?
        if(intsec < 10)
        {
            SecondsCombined = (TensSeconds + String.valueOf(intsec));
        }
        else
        {
            SecondsCombined = (String.valueOf(intsec));
        }

        // Format the time and cast it back as a string
        FormatedTime = ( HoursCombined + ':' + MinutesCombined + ':' + SecondsCombined );

        return FormatedTime;
    }

    // This is the Driver for the Multiline Test steps using count and placement of Testcase attributes
    public void TestSteps (int TestStepCntIn, String TestTime)
    {

        // if either forward or previois buttons then override normal count
        if((PreviousButtonHit ) || (ForwardButtonHit))
        {
            TestStepCnt=TestStepCntIn;
        }

        // Clear out text Multiline box
        TvTestStepsBox.setText("");

        // Set the intent
        Intent intent = getIntent();

        // Reset the Test execution time counter
        mTimerUpTick = "0";

        // Getting text from edittext, place the notes into the proper test case
        GetNotes = EtNotes.getText().toString();

        // See if an Event happened
        if (fWriteEventsSubString)
        {
            // Write also the substring to the note
            GetNotes = (GetNotes + ", ") + gVEvents;

            // Write to global variable
            gVGetNotes = GetNotes;

            //clear out substring
            gVEvents = "";
        }

        // Place the note into the Testcase item
        TestCaseAr[TestStepCnt].setNotes(GetNotes);

        // Clear out old note
        EtNotes.setText("");

        // Getting text from edittext, place into global variable
        gVOdometer = EtOdometer.getText().toString();

        // Place the odometer into the Testcase item
        TestCaseAr[TestStepCnt].setOdometer(gVOdometer);

        // Clear out old Odometer
        EtOdometer.setText("");

        // Getting text from edittext, place into global variable
        gVH1HourMeter = EtH1HourMeter.getText().toString();

        // Place the HiHourMeter into the Testcase item
        TestCaseAr[TestStepCnt].setH1HourMeter(gVH1HourMeter);

        // Clear out old Hourmeter
        EtH1HourMeter.setText("");

        // Getting text from edittext, place into global variable
        gVSOC = EtSOC.getText().toString();

        // place the SOC into Testcase item
        TestCaseAr[TestStepCnt].setSoc(gVSOC);

        // Clear out old SOC
        EtSOC.setText("");

        // Getting text from edittext, place into global variable
        gVNewEvents = EtEvents.getText().toString();

        // place the Events into Testcase item
        TestCaseAr[TestStepCnt].setEvents(gVNewEvents);

        // Clear out old Events
        EtEvents.setText("");

        // Place SW from SYNECT .json into application software tested
        TestCaseAr[TestStepCnt].setApplicationSoftwareTested(gVSoftwareVersion);

        // Place Truck serial number from inputted in from user
        TestCaseAr[TestStepCnt].setTruckSerialNumber(gVTruckSerialNumber);

        // Place the operator into the TestCase item
        TestCaseAr[TestStepCnt].setOperator(CurrentOperator);

        // Place Battery Serial Number
        TestCaseAr[TestStepCnt].setBatterySerialNumber(gVBatterySerialNumber);

        // Under Construction previous button placing text
        if((PreviousButtonHit) || (ForwardButtonHit))
        {
            // Do a read of External memory on the file name
            readInternalMessage();

            // Place Comments into Text via internal file read
            EtNotes.setText(gVGetNotes);
            // Place the note into the Testcase item
            TestCaseAr[TestStepCnt].setNotes(gVGetNotes);

            // Place SOC into Text via internal file read
            EtSOC.setText(gVSOC);
            // place the SOC into Testcase item
            TestCaseAr[TestStepCnt].setSoc(gVSOC);

            // Place H1HourMeter into Text via internal file read
            EtH1HourMeter.setText(gVH1HourMeter);
            // Place the HiHourMeter into the Testcase item
            TestCaseAr[TestStepCnt].setH1HourMeter(gVH1HourMeter);

            // Place Odometer into Text via internal file read
            EtOdometer.setText(gVOdometer);
            // Place the odometer into the Testcase item
            TestCaseAr[TestStepCnt].setOdometer(gVOdometer);

            // Place Events into Text via internal file read
            EtEvents.setText(gVNewEvents);
            // Place the event into the Testcase item
            TestCaseAr[TestStepCnt].setEvents(gVNewEvents);

            // Place into color output to button
            ReadPassSkipFailButton();
            // Place the button into the Testcase item
            TestCaseAr[TestStepCnt].setPassFailSkipBtnState(PassFailResult);

            //Place operator into Text via internal file
            // Clear out text View
            TvOperator.setText("");
            // setTextView for the operator
            TvOperator.append(gVOperator);
            // Place the operator into the TestCase item
            TestCaseAr[TestStepCnt].setOperator(gVOperator);

            // Clear out old value
            TvChangeBatterySerialNumber.setText("");
            //Place Battery Serial Number imtp text via internal file
            TvChangeBatterySerialNumber.setText(gVBatterySerialNumber);
            // Place Battery Serial Number into testcase item
            TestCaseAr[TestStepCnt].setBatterySerialNumber(gVBatterySerialNumber);
        }
        else
        {
            // place the test time into given TestCase item
            TestCaseAr[TestStepCnt].setTesttime(TimeConvert(Long.parseLong(TestTime)));

            // Get Time Date stamp
            TestCaseAr[TestStepCnt].setDate(GetDateTime());
        }

        // If expectant is different then what was reported then the TC fails (Ex Expected == Fail_NO && PassFailResult == FAIL_NO then it passes, Else TC fails)
        String GetCurrentExpectedResult = TestCaseAr[TestStepCnt].getExpectedResults();
        if(Objects.equals(PassFailResult, GetCurrentExpectedResult))
        {
            // Place the pass fail result into the Testcase item
            TestCaseAr[TestStepCnt].setResults("Passed"); //PASS_YES");
        }
        else
        {
            // Place the pass fail result into the Testcase item
            TestCaseAr[TestStepCnt].setResults("Failed"); //FAIL_NO");
        }

        // Did the Skip button get hit therefore supercede last
        if(PassFailResult == "SKIP")
        {
            // Place the pass fail result into the Testcase item
            TestCaseAr[TestStepCnt].setResults("Skipped"); //FAIL_NO");
            //Toast.makeText(getApplicationContext(),(TestCaseAr[TestStepCnt].getResults()),Toast.LENGTH_LONG).show();
        }

        // Place the state of the PassFailSkip button into TestCaseAr using the status of either of these buttons
        TestCaseAr[TestStepCnt].setPassFailSkipBtnState(PassFailResult);

        // Only ticks up if Next button hit
        if(fAllowNextBtnToupdateExecTestCases == true)
        {
            // Update the number of Executed test cases so far
            gVNumberOfExecutedTestCases = String.valueOf(TestStepCnt);
            gVInternalNumberOfExecutedTestCases = String.valueOf(TestStepCnt);
        }

        if ( PreviousButtonHit == false)
        {
            // Has either pass or fail buttons hit since last increment?
            if (fPassOrFailOrSkipBtnHit == true)
            {
                // Tick up this count every time button hit
                TestStepCnt++;

                //Toast.makeText(getApplicationContext(), " In TestSteps() PreviousButtonHit == false " + TestStepCnt , Toast.LENGTH_SHORT).show();
            }

            // This flag is un-set to dis-allow testcase to proceed, user must hit pass or fail each iteration
            fPassOrFailOrSkipBtnHit = false;
        }
        else
        {}

        if ( ForwardButtonHit == false)
        {
            // Has either pass or fail buttons hit since last increment?
            if (fPassOrFailOrSkipBtnHit == true)
            {
                // Tick up this count every time button hit
                TestStepCnt--;

                //Toast.makeText(getApplicationContext(), " In TestSteps() ForwardButtonHit == false " + TestStepCnt , Toast.LENGTH_SHORT).show();
            }

            // This flag is un-set to dis-allow testcase to proceed, user must hit pass or fail each iteration
            fPassOrFailOrSkipBtnHit = false;
        }
        else
        {}


        if (TestStepCnt <= Integer.parseInt(NumberOfTestCases ) )
        {
            //Set the array to the proper TestCase
            TvTestStepsBox.append(TestCaseAr[TestStepCnt].getSteps());  // See if this works to align steps correctly
        }

        // Group test step UI indication
        if (TestStepCnt <= Integer.parseInt(NumberOfTestCases))
        {
            // Set the test In-Progress
            gVTestInProgress = "In-Progress";

            // set the checkbox invisible the tests are incomplete
            CbTestPlanComplete.setVisibility(View.INVISIBLE);

            // clear out old value
            TvCurrentTestStepCount.setText("");
            TvCurrentTestStepCount.append(String.valueOf(TestStepCnt));

            // Clear and set
            TvSynectTestCaseNum.setText("");
            TvSynectTestCaseNum.append(TestCaseAr[TestStepCnt].getSynectTcNum());

            TvDrawingRtcNumber.setText("");
            TvDrawingRtcNumber.append(TestCaseAr[TestStepCnt].getDngIrtc());

            TvGroupTestCase.setText("");
            TvGroupTestCase.append(TestCaseAr[TestStepCnt].getGroupTcNum());
        }
        else
        {
            // Indicate test complete

            // set the checkbox visible the tests are complete
            CbTestPlanComplete.setVisibility(View.VISIBLE);

            // clear out old value
            TvCurrentTestStepCount.setText("");
            TvCurrentTestStepCount.setText("Finished");

            // Lockout the forward button
            ForwardButtonLockOut = true;

            // Lock out Pass, Skip, Fail, SkipAll Grey them out, Enable Next button make it Green and unlock it
            fLockSkPaFaSaBtnsForCompletion = true;

            // allow the logic to run for the Checkbox to lockout Next button
            CompletionAndCheckedLockOut = true;

            // Grey out Pass button
            BtPassYes.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey
            // Grey out SkipAll button
            BtSkipAll.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey
            // Grey out Fail button
            BtFailNo.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey
            // Grey out Skip button
            BtSkip.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey

            // Set the Next button grey to prompt user of lock out
            BtNext.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey
            // This flag will lock out Next Button until set true by falg and Test Plan complete check
            fSkipPassFailSkipAllHit = false;

            // end construction

        }

    }

    // This Method will return a system date and time stamp in "MM/dd/yyyy hh:mm:ss format
    public String GetDateTime()
    {
        // time fetch and format
        Calendar c= Calendar.getInstance();

        //SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        SimpleDateFormat sdf= new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
        String DateTime=sdf.format(c.getTime());

        return DateTime;
    }

    // This is the ReEWRITE button to save what ever was re done
    // This is the FWD (Forward) button to load in next set of steps
    public void onButtonReWriteclick(View v)
    {
        if(v.getId() == R.id.BtReWrite)
        {
            // Collect current UI entry

            // Getting text from edittext, Place the note into the Testcase item
            TestCaseAr[TestStepCnt].setNotes(EtNotes.getText().toString());

            // Getting text from edittext, place the SOC into Testcase item
            TestCaseAr[TestStepCnt].setSoc(EtSOC.getText().toString());

            // Getting text from edittext, Place the HiHourMeter into the Testcase item
            TestCaseAr[TestStepCnt].setH1HourMeter(EtH1HourMeter.getText().toString());

            // Place the odometer into the Testcase item
            TestCaseAr[TestStepCnt].setOdometer(EtOdometer.getText().toString());

            // Place the event into the Testcase item
            TestCaseAr[TestStepCnt].setEvents(EtEvents.getText().toString());

            // If expectant is different then what was reported then the TC fails (Ex Expected == Fail_NO && PassFailResult == FAIL_NO then it passes, Else TC fails)
            String GetCurrentExpectedResult = TestCaseAr[TestStepCnt].getExpectedResults();
            if(Objects.equals(PassFailResult, GetCurrentExpectedResult))
            {
                // Place the pass fail result into the Testcase item
                TestCaseAr[TestStepCnt].setResults("Passed"); //PASS_YES");
            }
            else
            {
                // Place the pass fail result into the Testcase item
                TestCaseAr[TestStepCnt].setResults("Failed"); //FAIL_NO");
            }

            // Place the state of the PassFailSkip button into TestCaseAr using the status of either of these buttons
            TestCaseAr[TestStepCnt].setPassFailSkipBtnState(PassFailResult);

            // Did the Skip button get hit therefore supercede last
            if(PassFailResult == "SKIP")
            {
                // Place the pass fail result into the Testcase item
                TestCaseAr[TestStepCnt].setResults("Skipped"); //FAIL_NO");
                Toast.makeText(getApplicationContext(),(TestCaseAr[TestStepCnt].getResults()),Toast.LENGTH_LONG).show();
            }

            // Place the event into the Testcase item
            TestCaseAr[TestStepCnt].setOperator(TvOperator.getText().toString());

            // Place Battery Serial number into The TestCase item
            TestCaseAr[TestStepCnt].setBatterySerialNumber(TvChangeBatterySerialNumber.getText().toString());

            // allow to write while in progress
            fSaveWhileInProgress = true;

            // allow to only write one item
            fReWrite = "true";

            // Write file to External memory
            writeExternalMessage();
            writeInternalMessage();
            fReWrite = "false";

            Toast.makeText(getApplicationContext(), " Re-Saving this entry to Internal and External memory current TestStepCount and stuff writing " + TestStepCnt +  TestCaseAr[TestStepCnt].getNotes() , Toast.LENGTH_LONG).show();

        }

    }


    // This is the FWD (Forward) button to load in next set of steps
    public void onButtonForwardclick(View v)
    {
        if( (v.getId() == R.id.BtForward) && (ForwardButtonLockOut == false) )
        {
            // Only ticks up if Next button hit
            fAllowNextBtnToupdateExecTestCases = false;

            // button hit
            ForwardButtonHit = true;

            // get the current count
            TestStepCntPrevFwd = TestStepCnt;

            // Check to make sure this does not exceed either of these number of executed test cases this could exceed and crash app
            if(TestStepCnt <= Integer.parseInt( gVNumberOfExecutedTestCases ))
            {
                // Tick up the test step other wise
                TestStepCntPrevFwd++;
                // Call the TestStep Driver
                TestSteps(TestStepCntPrevFwd,"0");

                // Lock out the button to not allow app to crash
                if( Integer.parseInt(gVNumberOfExecutedTestCases) == TestStepCnt )
                {
                    // New unlock the fwd button lockout
                    ForwardButtonLockOut = true;
                }
            }

            // Reset the Test execution time counter
            mTimerUpTick = "0";
            mTimerUpTck1 = 0;

            // Make a check and latch to last un-executed state
            if( TestStepCnt > Integer.parseInt( gVNumberOfExecutedTestCases ))
            {
                Toast.makeText(getApplicationContext(),(" YOU ARE NOW BACK TO CURRENT UN-EXECUTED TEST CASE"   ),Toast.LENGTH_LONG).show();

                // clear out text
                EtNotes.setText("");
                EtSOC.setText("");
                EtH1HourMeter.setText("");
                EtOdometer.setText("");
                EtEvents.setText("");

                //Reset the Button colors
                // Reinstate the Green button
                BtPassYes.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green
                // Reinstate the Red button
                BtFailNo.setBackgroundColor(Color.rgb(0xFF, 0x00, 0x00)); //SET YOUR COLOR this code is Red
                // Reinstate the Skip button
                BtSkip.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange
            }
        }
    }

    // This is the Previous button to load in previous steps
    public void onButtonPrevClick(View v)
    {
        if(v.getId() == R.id.BtPrev)
        {
            // button hit
            PreviousButtonHit = true;

            // when this is at end Un-Lock out Pass, Skip, Fail, SkipAll Grey them out, Enable Next button make it Green and unlock it
            fLockSkPaFaSaBtnsForCompletion = false;

            // get the current count
            TestStepCntPrevFwd = TestStepCnt;

            // Only ticks up if Next button hit
            fAllowNextBtnToupdateExecTestCases = false;

            if (TestStepCnt >= 1)
            {
                // keep latched to 1 to not crash app
                if(TestStepCnt == 1)
                {
                    TestStepCnt = 1;
                    TestStepCntPrevFwd = 1;
                }
                else
                {
                    // Tick down the test step other wise
                    //TestStepCnt--;
                    TestStepCntPrevFwd--;
                }

                // Reset the Test execution time counter
                mTimerUpTick = "0";
                mTimerUpTck1 = 0;
            }

            // Call the TestStep Driver
            TestSteps(TestStepCntPrevFwd,"0");

            // New unlock the fwd button lockout
            ForwardButtonLockOut = false;

        }
    }

    // this is the change operator Button
    public void onButtonChangeOperatorclick(View v)
    {
        if (v.getId() == R.id.BtChangeOperator)
        {
            // Getting text from edittext, place the operator into the proper test case
            CurrentOperator = EtOperator.getText().toString();

            // Clear out text View
            TvOperator.setText("");

            // setTextView for the operator
            TvOperator.append(CurrentOperator);
        }
    }

    // this is the change Battery serial number Button
    public void onButtonChangeBatterySerialNumberclick(View v)
    {
        if (v.getId() == R.id.BtChangeBatterySerialNumber)
        {
            // Getting text from edittext, place the battery serial number into the proper test case
            CurrentBatterySerialNumber = EtBatterySerialNumber.getText().toString();

            // Clear out text View
            TvChangeBatterySerialNumber.setText("");

            // setTextView for the operator
            TvChangeBatterySerialNumber.append(CurrentBatterySerialNumber);

            // set the global variable to new value;
            gVBatterySerialNumber = CurrentBatterySerialNumber;
        }
    }

    // this will set or grey out buttons for use while in Previous or Forward
    public void ReadPassSkipFailButton()
    {

        // Next button thinks its this Teststep
        switch(gVPassFailSkipBtn)
        {
            case "PASS_YES":
                // Grey out this button
                BtPassYes.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey

                // Reinstate the Red button
                BtFailNo.setBackgroundColor(Color.rgb(0xFF, 0x00, 0x00)); //SET YOUR COLOR this code is Red

                // Reinstate the Skip button
                BtSkip.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

                // Set the variable
                PassFailResult = "PASS_YES"; //PASS;


                break;

            case "FAIL_NO":
                // Grey out this button
                BtFailNo.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey

                // Reinstate the Green button
                BtPassYes.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

                // Reinstate the Skip button
                BtSkip.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

                // Set the variable
                PassFailResult = "FAIL_NO"; //PASS;

                break;

            case "SKIP":
                // Grey out this button
                BtSkip.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey

                // Reinstate the Green button
                BtPassYes.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

                // Reinstate the Red button
                BtFailNo.setBackgroundColor(Color.rgb(0xFF, 0x00, 0x00)); //SET YOUR COLOR this code is Red

                // Set the variable
                PassFailResult = "SKIP"; //PASS;

                break;

            default:
                // Reinstate the Green button
                BtPassYes.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

                // Reinstate the Red button
                BtFailNo.setBackgroundColor(Color.rgb(0xFF, 0x00, 0x00)); //SET YOUR COLOR this code is Red

                // Reinstate the Skip button
                BtSkip.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

                break;
        }
    }


    // this is the pass Button
    public void onButtonPassYesclick(View v)
    {
        if( (v.getId() == R.id.BtPassYes) && (fLockSkPaFaSaBtnsForCompletion == false) )
        {
            // This flag wil lock out Next Button until set true
            fSkipPassFailSkipAllHit = true;

            // Set the variable
            PassFailResult = "PASS_YES"; //PASS;

            // This flag is set to allow testcase to proceed, user must hit pass or fail each iteration
            fPassOrFailOrSkipBtnHit = true;

            // Grey out this button
            BtPassYes.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey

            // Reinstate the Red button
            BtFailNo.setBackgroundColor(Color.rgb(0xFF, 0x00, 0x00)); //SET YOUR COLOR this code is Red

            // Reinstate the Skip button
            BtSkip.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

            // Reinstate the SkipAll button
            BtSkipAll.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

            // Set the Next button green to prompt user
            BtNext.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

            // Clear out the Server status message
            ServerConnectionPerformtestCases.setText("ServerConnection");
        }
    }

    // this is the fail Button
    public void onButtonFailNoclick(View v)
    {
        if( (v.getId() == R.id.BtFailNo) && (fLockSkPaFaSaBtnsForCompletion == false) )
        {
            // This flag wil lock out Next Button until set true
            fSkipPassFailSkipAllHit = true;

            // Set the variable
            PassFailResult = "FAIL_NO"; //FAIL;

            // This flag is set to allow testcase to proceed, user must hit pass or fail each iteration
            fPassOrFailOrSkipBtnHit = true;

            // Grey out this button
            BtFailNo.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey

            // Reinstate the Green button
            BtPassYes.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

            // Reinstate the SkipAll button
            BtSkipAll.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

            // Reinstate the Skip button
            BtSkip.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

            // Set the Next button green to prompt user
            BtNext.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

            // Clear out the Server status message
            ServerConnectionPerformtestCases.setText("ServerConnection");

        }
    }

    // this is the skip Button
    public void onButtonSkipclick(View v)
    {
        if( (v.getId() == R.id.BtSkip) && (fLockSkPaFaSaBtnsForCompletion == false) )
        {
            // This flag wil lock out Next Button until set true
            fSkipPassFailSkipAllHit = true;

            // Set the variable
            PassFailResult = "SKIP"; //PASS;

            // This flag is set to allow testcase to proceed, user must hit pass or fail each iteration
            fPassOrFailOrSkipBtnHit = true;

            // Grey out this button
            BtSkip.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey

            // Reinstate the SkipAll button
            BtSkipAll.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

            // Reinstate the Green button
            BtPassYes.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

            // Reinstate the Red button
            BtFailNo.setBackgroundColor(Color.rgb(0xFF, 0x00, 0x00)); //SET YOUR COLOR this code is Red

            // Set the Next button green to prompt user
            BtNext.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

            // Clear out the Server status message
            ServerConnectionPerformtestCases.setText("ServerConnection");
        }
    }

    // This is the SkipAll button
    public void onButtonSkipAllclick(View v)
    {
        if( (v.getId() == R.id.BtSkipAll) && (fLockSkPaFaSaBtnsForCompletion == false) )
        {
            // This flag wil lock out Next Button until set true
            fSkipPassFailSkipAllHit = true;

            // Grey out this button
            BtSkipAll.setBackgroundColor(Color.rgb(0xC0, 0xC0, 0xC0)); //SET YOUR COLOR this code is Grey

            // Reinstate the Skip button
            BtSkip.setBackgroundColor(Color.rgb(0xFF, 0x99, 0x33)); //SET YOUR COLOR this code is orange

            // Reinstate the Green button
            BtPassYes.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

            // Reinstate the Red button
            BtFailNo.setBackgroundColor(Color.rgb(0xFF, 0x00, 0x00)); //SET YOUR COLOR this code is Red

            // Set the Next button green to prompt user
            BtNext.setBackgroundColor(Color.rgb(0x00, 0xFF, 0x00)); //SET YOUR COLOR this code is Green

            // Clear out the Server status message
            ServerConnectionPerformtestCases.setText("ServerConnection");

            // set this to the end of test cases
            TestStepCnt = Integer.parseInt(NumberOfTestCases);

        }
    }

    // This is the Save and Exit button
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onButtonSaveAndExitclick(View v)
    {
        if (v.getId() == R.id.BtSaveAndExit)
        {
            // show what is saved
            Toast.makeText(getApplicationContext(),(gVFileToBeUsed + " Will be saved to the In-Progress folder" ),Toast.LENGTH_LONG).show();
            // Kill the App not yet
            //  moveTaskToBack(true);
            //  android.os.Process.killProcess(android.os.Process.myPid());
            //  System.exit(1);
            finishAffinity();
            System.exit(0);

        }
    }

    // This method is where the Exporttestresults Activity returns from I hope
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_CODE_FUNCTION)
        {
            if (resultCode == RESULT_OK)
            {
                // set this back to the end of test cases
                TestStepCnt = Integer.parseInt(NumberOfTestCases);

                // Call the TestStep Driver should be set to last state
                TestSteps(TestStepCnt,"0");

                // Reset the Test execution time counter
                mTimerUpTick = "0";
                mTimerUpTck1 = 0;

            }
        }
        // This is returning form the Events Activity
        if (requestCode == REQUEST_CODE_EVENTS)
        {
            if (resultCode == RESULT_OK)
            {
                // Get previous variables from activity
                gVEvents = data.getStringExtra("OutStringFromEvents"); //(intent.getStringExtra("OutStringFromEvents"));
                Toast.makeText(getApplicationContext(),(" Returned from Events Activity gVEvents " + gVEvents ),Toast.LENGTH_LONG).show();

                // Enable the Substring to be written
                fWriteEventsSubString = true;
            }
        }
    }

    // This is the Abort button to kill app
    /*public void onButtonAbortclick(View v)
    {
        if(v.getId() == R.id.BtAbortTestPlan)
        {
            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
    }*/

    // This is the Event activity launcher button
    public void onButtonEventclick(View v)
    {
        if(v.getId() == R.id.BtEvent)
        {
            // This will transition over to Exporttestresults activity
            Intent i = new Intent(Performtestcases.this, Events.class);

            // Start the Event activity
            startActivityForResult(i, REQUEST_CODE_EVENTS);
            //startActivity(i);
        }
    }

    //This method is the Timer function
    public void Timer()
    {
        mCountDownTimer = new CountDownTimer(mTimerLeftInMillis, mCountDownInterval)
        {
            @Override
            public void onTick(long millisUntilFinished)
            {
                // Tick up this counter every 1 second show on UI
                mTimerUpTck1++;
                mTimerUpTick = String.valueOf(mTimerUpTck1);
                mTextViewCount.setText(String.valueOf(TimeConvert(Long.parseLong(mTimerUpTick))));
            }

            @Override
            public void onFinish()
            {
                // Keep timer going
                Timer();
            }
        }.start();
    }

    // This method will save the timer count if keyboard or screen orientation changes
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putInt("SavedCount", mTimerUpTck1);
    }
}

