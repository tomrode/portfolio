package com.example.t0028919.person_in_looptesttool;

import android.os.AsyncTask;
import android.os.Environment;
import android.os.Message;
import android.os.StatFs;
import android.provider.DocumentsContract;
import android.util.Log;
import android.content.Intent;
import android.widget.Toast;

import com.visuality.nq.auth.PasswordCredentials;
import com.visuality.nq.client.Directory;
import com.visuality.nq.client.File;
import com.visuality.nq.client.Mount;
import com.visuality.nq.common.Buffer;
import com.visuality.nq.common.BufferReader;
import com.visuality.nq.config.Config;

import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.logging.Handler;

import static android.support.v4.content.ContextCompat.startActivity;
import static java.lang.Byte.parseByte;
import static java.lang.Byte.valueOf;

/**
 * Created by t0028919 on 2/6/2018.
 */
public class FetchInProgressData extends AsyncTask<String, Void, String> {

    // Debug message Tag
    private static final String TAG = "TomsMessage";

    // Example from internet
    static final String USER_NAME = "t0028919";
    static final String PASSWORD = "Lift0091";

    // SMB ConnectionStatus
    String ConnectionStatus;

    @Override
    protected void onPreExecute() {
        Log.i(TAG, "in AsyncTask in FetchInProgressData onPreExecute");
    }
    @Override
    protected String doInBackground(String... params) {

        // Paths within EngSTL1 server
        String ServerPath = "MANUAL_SYNECT_TEST_PLAN/In_Progress";

        // Tablets Internal Storage Paths
        String TabletPaths = "/PIL_SYNECT_TEST_PLAN/In_Progress_From_Server";

        // Credentials from user via MainActivity
        String StUserName = params[0];
        String StPassWord = params[1];

        // File buffer size
        Long FileSize;

        try{
            Log.i(TAG, "IN AsyncTask in try here is params UserName ---------------------------->:" + StUserName );   // debug msg
            Log.i(TAG, "IN AsyncTask in try here is params Password ---------------------------->:" + StPassWord );   // debug msg

            // First Test to write a file to EngSTL1 using jNQ-1.0.1.S8.R2775.Beta Under Construction below this point
            //PasswordCredentials cr = new PasswordCredentials(USER_NAME, PASSWORD, "us.crownlift.net");
            PasswordCredentials cr = new PasswordCredentials(StUserName, StPassWord, "us.crownlift.net");

            // Debug PCAP
            Config.jnq.set("CAPTUREFILE", "/home/Tom/folder");

            // Server init
            Mount mt = new Mount("engstl1","Vol1", cr);
            //Mount mt = new Mount("172.20.62.42","vol1", cr);

            Log.i(TAG, "IN AsyncTask in try after Mount ---------------------------->:"  );   // debug msg

            // Use browse director, look at this root, list the folders that reside
            Directory dir = new Directory(mt, ServerPath);
            Directory.Entry Folderentry;
            Directory.Entry Fileentry;

            //System.out.println(DIR + " scan:");
            do {
                // specific folders
                Folderentry = dir.next();
                if (Folderentry == null)
                {
                    break;
                }
                if (null != Folderentry)
                    System.out.println(Folderentry.name + " : size = " + Folderentry.info.eof);

                // files within folder
                Directory files= new Directory(mt, ServerPath + "/" + Folderentry );
                do{
                    // specific files in a given folder
                    Fileentry = files.next();
                    if (Fileentry == null)
                    {
                        break;
                    }
                    if (null != Fileentry)
                        System.out.println(Fileentry.name + " : size = " + Fileentry.info.eof);
                        FileSize = Long.valueOf(Fileentry.info.eof);
                    // Lets see if we can save to the device here
                    // Get public external storage folder ( /storage/emulated/0 ).

                    String State;
                    State = Environment.getExternalStorageState();

                    if (Environment.MEDIA_MOUNTED.equals(State))
                    {
                        // Create a folder
                        java.io.File Root = Environment.getExternalStorageDirectory();

                        // Give a specific directory
                        java.io.File Dir = new java.io.File(Root.getAbsolutePath() + TabletPaths + "/" + Folderentry );

                        // See if folder exists then make it
                        if(!Dir.exists() )
                        {
                            Dir.mkdirs();
                        }
                        // Get the Server file via jNQ stack  ACCESS_WRITE File.ATTR_READONLY, ACCESS_READ
                        File.Params pr = new File.Params(File.ACCESS_READ, File.SHARE_FULL,
                                File.DISPOSITION_OPEN, false);

                        // Concatenate and construct external storage folder + Truck folder + file
                        String DirForjNQ =  ServerPath + "/" + Folderentry + "/" + Fileentry;

                        // Set up File instance
                        File jNQfile = new File(mt, DirForjNQ  , pr);

                        // Dynamically size the buffer according to file size
                        Buffer buff = new Buffer(Integer.valueOf(String.valueOf(FileSize)));//5000);

                        // read DATASIZE from the beginning of the file
                        jNQfile.read(buff);

                        // Send out contents for verification
                        Log.i(TAG, "IN AsyncTask in after read in jNQ buff ---------------------------->:" + buff );   // debug msg
                        Log.i(TAG, "IN AsyncTask in after read in jNQ the path ---------------------------->:" + DirForjNQ );   // debug msg

                        // External memory set up
                        String StFileentry = String.valueOf(Fileentry);
                        java.io.File file = new java.io.File(Dir,StFileentry);

                        // Checking the stats on the file getting the length of it
                        Log.i(TAG, "IN AsyncTask in getting length of file ---------------------------->:" + file.length() );   // debug msg

                        try
                        {
                            FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf(file));
                            fileOutputStream.write(buff.data); // Yes it works so good!!!!
                            fileOutputStream.close();
                            Log.i(TAG, "IN AsyncTask in try here is params fileOutputStream ---------------------------->:"  );   // debug msg
                        }
                        catch (FileNotFoundException e)
                        {
                            e.printStackTrace();
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Log.i(TAG, "SD Card not found ---------------------------->:"  );   // debug msg
                    }
                }while (Fileentry != null);
            } while (Folderentry != null);
            // Report status
            ConnectionStatus = "Successful";
            // End construction of First Test to write a file to EngSTL1 using jNQ-1.0.1.S8.R2775.Beta
        } catch (Exception e) {
            Log.i(TAG, "IN doInBackground method Exception---------------------------->:" );   // debug msg

            e.printStackTrace();

            ConnectionStatus = ("SMB Error:" + e);
        }
        return ConnectionStatus;//null;
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);

        Log.i(TAG, "in PostExecute with in ASYNC task" );   // debug msg

        MainActivity.ServerConnection.setText(this.ConnectionStatus);

        //Intent i = new Intent(MainActivity.this, MainActivity.class );
        //finish();
        //startActivity(i);
    }
}