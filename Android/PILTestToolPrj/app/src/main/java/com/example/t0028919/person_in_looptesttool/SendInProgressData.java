package com.example.t0028919.person_in_looptesttool;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.visuality.nq.auth.PasswordCredentials;
import com.visuality.nq.client.Client;
import com.visuality.nq.client.Directory;
import com.visuality.nq.client.File;
import com.visuality.nq.client.Mount;
import com.visuality.nq.common.Buffer;
import com.visuality.nq.common.BufferReader;
import com.visuality.nq.common.BufferWriter;
import com.visuality.nq.config.Config;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static android.app.PendingIntent.getActivity;
import static android.os.Environment.getExternalStorageDirectory;
import static android.support.v4.app.ActivityCompat.finishAffinity;
import static java.lang.String.valueOf;
import static java.lang.System.*;

/* Author: Thomas Rode
 *
 * Intent: This class is the driver used for Test plan completion data JSON files which will be sent back up to Network drive.
 *
*/

public class SendInProgressData extends AsyncTask<String, Void, String> {

    private static final String TAG = "TomsMessage";

    // this flag determines if file should be deleted from Ready For Execution
    private boolean fDeleteFileReadyForExecution = true;

    // SMB ConnectionStatus
    String ConnectionStatus;
    String ConnectionStatusReadyForExecutionDelete;
    String ConnectionStatusDelete;

    @Override
    protected void onPreExecute() {
        Log.i(TAG, "in SendInProgressData onPreExecute");   // debug msg
    }


    @Override
    protected String doInBackground(String... params) {

        // Paths within EngSTL1 server
        String ServerPath = "MANUAL_SYNECT_TEST_PLAN/In_Progress";

        // Paths with EngSTL1 server t delete
        String ServerPathDel = "MANUAL_SYNECT_TEST_PLAN/Ready_for_Execution";

        // Tablets Internal Storage Paths
        String TabletPaths = "/PIL_SYNECT_TEST_PLAN/In_Progress";

        // This will have to open an internal file to get credentials and determine which file and folder
        String StUserName = params[0];
        String StPassWord = params[1];
        String gVFileToBeUsed = params[2];
        String gVTruckPickedInSpinner = params[3];
        String TestStepCnt =  params[4];

        // current count
        Log.i(TAG, "IN SenInProgressData TestStepCnt ---------------------------->:" + TestStepCnt);   // this got the file name

        // File buffer size
        Long FileSize;

		// debug msg
        Log.i(TAG, "in SendInProgressData doInBackground");


        // Delete File out of In_Progress in EngSTL1 server
        try {
            // First Test to write a file to EngSTL1 using jNQ-1.0.1.S8.R2775.Beta Under Construction below this point will be changed using StUserName, StPassWord
            PasswordCredentials cr = new PasswordCredentials(StUserName, StPassWord, "us.crownlift.net");

            // Server init
            Mount mt = new Mount("engstl1", "Vol1", cr);

            // Parameter settings to write
            File.Params pr = new File.Params(File.ACCESS_WRITE, File.SHARE_FULL,
                    File.DISPOSITION_OPEN_IF, false);

            // Concatenate and construct server path: EngSTL1 folder + Truck folder + file
            String DirForjNQDel = ServerPath + "/" + gVTruckPickedInSpinner + "/" + gVFileToBeUsed;

            // File instance NEW
            File file = new File(mt, DirForjNQDel, pr);

            // See if this is a directory or not argument: isDir - true if a directory is expected, false if a file is expected.
            if(File.isExist(mt,DirForjNQDel,false ))
            {
                // NEW After this operation the file is still accessible until all its handles will be closed. After the last handle on this file is clsoed, it will be deleted on server.
                //file.deleteOnClose();

                // NEW Close file
                file.close();

                // NEW terminate jNQ client All threads will be terminated
                Client.stop();

                // Delete this specific file here
                //File.delete(mt, DirForjNQDel);
            }

            // Close the mount point
            mt.close();

            // Report status
            ConnectionStatusDelete = "to Server In-Progress folder";

            Log.i(TAG, "Delete Server In-Progress file Successful " );   // debug msg
        }
        catch (Exception e) {
            Log.i(TAG, "SMB Delete Error In-Progress ---------------------------->:" + e);   // debug msg

            e.printStackTrace();

            // Report status
            ConnectionStatusDelete = ("SMB Delete Error: " + e);
        }

        // SD card files and folders to Read External Storage
        String State = Environment.getExternalStorageState();

        // Is file mounted and readable
        if ((Environment.MEDIA_MOUNTED.equals(State)) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(State)) {

           // Need to loop through "/PIL_SYNECT_TEST_PLAN/In_Progress";
            java.io.File listFolder = new  java.io.File( (getExternalStorageDirectory() + TabletPaths ));
            java.io.File folderdir = new java.io.File(valueOf(listFolder));
            java.io.File[] allfolders = folderdir.listFiles();
            for (int i = 0; i < allfolders.length; i++) {
                // Folder name
                Log.i(TAG, "IN AsyncTask, getting folder name ---------------------------->:" +  allfolders[i].getName() );   // this got the folder name

                // This is the point where we loop through allfolders[ x ] to get specific folder to be spelled out so to get file contents
                java.io.File listFile = new java.io.File((getExternalStorageDirectory() + TabletPaths + "/" + allfolders[i].getName()));
                java.io.File filedir = new java.io.File(valueOf(listFile));
                java.io.File[] allfiles = filedir.listFiles();

                // Loop through the file in a given folder
                for (int j = 0; j < allfiles.length; j++) {
                    // File name
                    Log.i(TAG, "IN AsyncTask, getting file name ---------------------------->:" + allfiles[j].getName());   // this got the file name

                    // Beginning of Extracting data out of specific file
                    Log.i(TAG, "IN AsyncTask MEDIA_MOUNTER");   // debug msg

                    // Sting builder
                    StringBuilder sb = new StringBuilder();

                    try {
                        // Create a folder                                                                              //Change this and        this
                        java.io.File textFile = new java.io.File((getExternalStorageDirectory() + TabletPaths + "/" + allfolders[i].getName() ), allfiles[j].getName());

                        java.io.FileInputStream fis = new FileInputStream(textFile);

                        // Checking the status on the file getting the length of it
                        FileSize = Long.valueOf(textFile.length());
                        Log.i(TAG, "IN AsyncTask, getting length of file ---------------------------->:" + FileSize);

                        if (fis != null) {
                            InputStreamReader isr = new InputStreamReader(fis);
                            BufferedReader buffer = new BufferedReader(isr);

                            String line = null;
                            while ((line = buffer.readLine()) != null) {
                                sb.append(line + "\n");
                            }
                            fis.close();
                        }

                        String CastsbStr = String.valueOf(sb);
                        Log.i(TAG, "IN AsyncTask in try here is CastsbStr file contents ---------------------------->:" + CastsbStr);   // debug msg
                        // Here is the point where content data from the variable sb" to be ported out jNQ stack START

                        try {
                            // First Test to write a file to EngSTL1 using jNQ-1.0.1.S8.R2775.Beta Under Construction below this point will be changed using StUserName, StPassWord
                            PasswordCredentials cr = new PasswordCredentials(StUserName, StPassWord, "us.crownlift.net");
                            //PasswordCredentials cr = new PasswordCredentials(USER_NAME, PASSWORD, "us.crownlift.net");

                            // Server init
                            Mount mt = new Mount("engstl1", "Vol1", cr);

                            // Parameter settings to write
                            File.Params pr = new File.Params(File.ACCESS_WRITE, File.SHARE_FULL,
                                    File.DISPOSITION_OPEN_IF, false);

                            // Concatenate and construct server path: EngSTL1 folder + Truck folder + file
                            String DirForjNQ = ServerPath + "/" + allfolders[i].getName() + "/" + allfiles[j].getName();//Folderentry + "/" + Fileentry;

                            // Set up File instance
                            File jNQfile = new File(mt, DirForjNQ, pr);

                            // Dynamically size the buffer according to file size
                            Buffer buff = new Buffer((int) textFile.length());

                            // Put the raw bytes to jNQ buffer
                            buff.data = CastsbStr.getBytes();

                            // Write the buffer up to server
                            jNQfile.write(buff);

                            // Close the file.
                            jNQfile.close();

                            // close the mount point
                            mt.close();

                            // Report status
                            ConnectionStatus = "Successful write, ";
                            Log.i(TAG, "IN AsyncTask in try Successful Write to EngSTL1 In-Progress" );   // debug msg
                        }
                        catch (Exception e) {
                            Log.i(TAG, "IN doInBackground method Exception SMB jNQ ---------------------------->:" + e);   // debug msg
                            ConnectionStatus = ("SMB Server copy Error," + e);
                            e.printStackTrace();
                        }
                        // Here is the point where content data from the variable sb" to be ported out jNQ stack END
                    } catch (IOException e) {
                        Log.i(TAG, "IN AsyncTask got IOException e ");   // debug msg
                    }
                }
            }// End of for loop
        }
        else
        {
            Log.i(TAG, "Error in Mounting External MEDIA ");   // debug msg
        }

        return null;
    }


    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);

        // Display this status on UI
        Performtestcases.ServerConnectionPerformtestCases.setText(this.ConnectionStatus + ConnectionStatusDelete);
        //Performtestcases.ServerConnectionPerformtestCases.setText(this.ConnectionStatus + ConnectionStatusReadyForExecutionDelete + ConnectionStatusDelete);

    }

}