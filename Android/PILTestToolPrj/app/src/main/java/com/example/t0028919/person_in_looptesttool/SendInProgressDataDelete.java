package com.example.t0028919.person_in_looptesttool;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.visuality.nq.auth.PasswordCredentials;
import com.visuality.nq.client.Directory;
import com.visuality.nq.client.File;
import com.visuality.nq.client.Mount;
import com.visuality.nq.common.Buffer;
import com.visuality.nq.common.BufferReader;
import com.visuality.nq.common.BufferWriter;
import com.visuality.nq.config.Config;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static android.app.PendingIntent.getActivity;
import static android.os.Environment.getExternalStorageDirectory;
import static android.support.v4.app.ActivityCompat.finishAffinity;
import static java.lang.String.valueOf;
import static java.lang.System.*;

/* Author: Thomas Rode
 *
 * Intent: This class is the driver used for Test plan erasing data JSON files on Network drive.
 *
*/

public class SendInProgressDataDelete extends AsyncTask<String, Void, String> {

    // LogCat debug message
    private static final String TAG = "TomsMessage";

    // SMB ConnectionStatus
    String ConnectionStatusDelete;

    @Override
    protected void onPreExecute() {
        Log.i(TAG, "in SendInProgressData onPreExecute");   // debug msg
    }


    @Override
    protected String doInBackground(String... params) {

        // Paths within EngSTL1 server
        String TabletPathComplete = "/PIL_SYNECT_TEST_PLAN/Completed_Execution";

        // Paths with EngSTL1 server to delete
        String ServerPathDel = "MANUAL_SYNECT_TEST_PLAN/In_Progress";

        // Server EngSTL1 Completed_Execution Storage Paths
        String ServerPathComplete = "MANUAL_SYNECT_TEST_PLAN/Completed_Execution";

        // This will have to open an internal file to get credentials and determine which file and folder
        String StUserName = params[0];
        String StPassWord = params[1];
        String gVFileToBeUsed = params[2];
        String gVTruckPickedInSpinner = params[3];

        // The data string used from external file
        String CastsbStr = "";
        
        // File buffer size
        Long FileSize;

		// debug msg
        Log.i(TAG, "in SendInProgressDataDelete doInBackground");

        // Delete File an server section
        try {
            // First Test to write a file to EngSTL1 using jNQ-1.0.1.S8.R2775.Beta Under Construction below this point will be changed using StUserName, StPassWord
            PasswordCredentials cr = new PasswordCredentials(StUserName, StPassWord, "us.crownlift.net");

            // Server init
            Mount mt = new Mount("engstl1", "Vol1", cr);

            // Parameter settings to write
            File.Params pr = new File.Params(File.ACCESS_WRITE, File.SHARE_FULL,
                    File.DISPOSITION_OPEN_IF, false);

            // Concatenate and construct server path: EngSTL1 folder + Truck folder + file
            String DirForjNQDelIn_Progress = ServerPathDel + "/" + gVTruckPickedInSpinner + "/" + gVFileToBeUsed;

            // See if this is a directory or not TODO TRY THIS
            if(File.isExist(mt,DirForjNQDelIn_Progress,false ))
            {
                Log.i(TAG, "<-------------------------- FILE EXISTS TO DELETE IN In_Progress FOLDER ------------------------>:");   // debug msg

                // Delete this specific file here
                File.delete(mt, DirForjNQDelIn_Progress);

                // Report status
                ConnectionStatusDelete = "Delete Server In_Progress file Successful";
            }

            // close the mount point
            mt.close();

            Log.i(TAG, ConnectionStatusDelete );   // debug msg
        }
        catch (Exception e) {
            Log.i(TAG, "SMB Delete Error In-Progress ---------------------------->:" + e);   // debug msg

            e.printStackTrace();

            // Report status
            ConnectionStatusDelete = ("SMB Error:" + e);
        }

        // Open External file in completed_Execution then copy and send to EngSTL1 then delete External file
        // SD card files and folders to Read External Storage
        String State = Environment.getExternalStorageState();

        // Is file mounted and readable
        if ((Environment.MEDIA_MOUNTED.equals(State)) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(State)) {

            // Sting builder
            StringBuilder sb = new StringBuilder();

            // Create a folder
            java.io.File textFile = new java.io.File(getExternalStorageDirectory() + TabletPathComplete + "/" + gVTruckPickedInSpinner + "/" + gVFileToBeUsed );


            try
            {
                // Create a folder
                //java.io.File textFile = new java.io.File(getExternalStorageDirectory() + TabletPathComplete + "/" + gVTruckPickedInSpinner + "/" + gVFileToBeUsed );

                java.io.FileInputStream fis = new FileInputStream(textFile);

                // Checking the status on the file getting the length of it
                FileSize = Long.valueOf(textFile.length());
                Log.i(TAG, "IN AsyncTask, getting length of file ---------------------------->:" + FileSize);

                if (fis != null)
                {
                    InputStreamReader isr = new InputStreamReader(fis);
                    BufferedReader buffer = new BufferedReader(isr);

                    String line = null;
                    while ((line = buffer.readLine()) != null)
                    {
                        sb.append(line + "\n");
                    }
                    fis.close();
                }

                CastsbStr = String.valueOf(sb);
                Log.i(TAG, "IN AsyncTask in try here is CastsbStr file contents ---------------------------->:" + CastsbStr);   // debug msg
            }

            catch (IOException e)
            {
                //You'll need to add proper error handling here
            }
            try {
                // First Test to write a file to EngSTL1 using jNQ-1.0.1.S8.R2775.Beta Under Construction below this point will be changed using StUserName, StPassWord
                PasswordCredentials cr = new PasswordCredentials(StUserName, StPassWord, "us.crownlift.net");
                //PasswordCredentials cr = new PasswordCredentials(USER_NAME, PASSWORD, "us.crownlift.net");

                // Server init
                Mount mt = new Mount("engstl1", "Vol1", cr);

                // Parameter settings to write
                File.Params pr = new File.Params(File.ACCESS_WRITE, File.SHARE_FULL,
                        File.DISPOSITION_OPEN_IF, false);

                // Concatenate and construct server path: EngSTL1 folder + Truck folder + file
                String DirForjNQ = ServerPathComplete + "/" + gVTruckPickedInSpinner + "/" + gVFileToBeUsed ;

                // Set up File instance
                File jNQfile = new File(mt, DirForjNQ, pr);

                // Dynamically size the buffer according to file size
                Buffer buff = new Buffer((int) textFile.length());

                // Put the raw bytes to jNQ buffer
                buff.data = CastsbStr.getBytes();

                // Write the buffer up to server
                jNQfile.write(buff);

                // Close the file.
                jNQfile.close();

                // Report status
                ConnectionStatusDelete= "Successful copied EngSTL1:Completed_Execution";
            }

            catch (Exception e) {
                Log.i(TAG, "IN doInBackground method Exception SMB jNQ ---------------------------->:" + e);   // debug msg
                ConnectionStatusDelete= ("SMB Error:" + e);
                e.printStackTrace();
            }

            // Delete file and from the Ready_for_Execution on tablet folder
            java.io.File Root = Environment.getExternalStorageDirectory();
            java.io.File filedel = new java.io.File(( Root+ "/PIL_SYNECT_TEST_PLAN/Ready_for_Execution/" + gVTruckPickedInSpinner + "/"  + gVFileToBeUsed));
            boolean deleted1 = filedel.delete();

            // Delete file and from the In_Progress_From_Server  on tablet folder
            java.io.File Root2 = Environment.getExternalStorageDirectory();
            java.io.File filedel2 = new java.io.File(( Root+ "/PIL_SYNECT_TEST_PLAN/In_Progress_From_Server/" + gVTruckPickedInSpinner + "/"  + gVFileToBeUsed));
            boolean deleted2 = filedel2.delete();


        }

        return null;
    }


    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);

        // Display this status on UI
        Performtestcases.ServerConnectionPerformtestCases.setText(this.ConnectionStatusDelete);

        // Kill the App not yet
        //moveTaskToBack(true);
        //android.os.Process.killProcess(android.os.Process.myPid());
        //getActivity().finish();
        //System.exit(0);
    }

}