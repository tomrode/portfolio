package com.example.t0028919.person_in_looptesttool;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;
import android.content.Intent;
import android.widget.Toast;
import android.util.Log;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by t0028919 on 1/26/2018.
 *
 * Author: Thomas Rode
 *
 * Intent: This Activity will envoke as a result of user completing a TestCase from Performtestcases Activity. Hitting UI button " EXPORT" will start
 *         ES File Explorer app to allow test data to be sent to Croen Server EngSTL1. It is possible to open an internally saved file and send an email
 *         from methods below, these however are not in use.
 */


public class Exporttestresults extends Activity
{
    // log message
    private static final String TAG = "TomsMessage";

    // Global variables to pass out of activity
    String gVAcvtiveFileName = "";
    String gVSoftwareVersion = "";
    String gVSynectTestCaseNumber = "";
    String gVNumberOfTestCases = "";
    String gVDNGiRTCNumber = "";
    String gVCurrentOperator = "";
    String gVStUserName = "";
    String gVStPassWord = "";

    // UI variables
    TextView TvActiveFileName;
    TextView TvSoftwareVersion;
    EditText UserName;
    EditText PassWord;
    public static TextView ServerConnectionExport;

    // TestCase class variables
    TestCase TC1Exporttestresults;
    TestCase TC2PerformTestCases;
    TestCase TC3PerformTestCases;
    TestCase TC4PerformTestCases;

    // JSON data
    StringBuffer JSONData;

    // temporary TextView multiline remove ne once done
    TextView TvSavedData;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exporttestresults);

        // logging message
        Log.i(TAG, "In the onCreate in EXPORTTESTRESULTS");   // debug msg

        // Setup UI
        TvActiveFileName = (TextView) findViewById(R.id.TvActiveFileName);
        TvSoftwareVersion = (TextView) findViewById(R.id.TvSoftwareVersion);
        //ServerConnectionExport = (TextView)findViewById(R.id.TvServerConnectionExport);

        // Temporary multiline text view and hide it
        TvSavedData = (TextView) findViewById(R.id.TvSavedData);
        TvSavedData.setVisibility(View.INVISIBLE);


        // Get previous variables from activity
        Intent intent = getIntent();

        // get value from Performtestcases activity
        gVAcvtiveFileName = (intent.getStringExtra("ActiveFileName"));
        gVSoftwareVersion = (intent.getStringExtra("SoftwareVersion"));
        gVSynectTestCaseNumber = (intent.getStringExtra("SynectTestCaseNumber"));
        gVNumberOfTestCases = (intent.getStringExtra("NumberOfTestCases"));
        gVDNGiRTCNumber = (intent.getStringExtra("DNGiRTCNumber"));
        gVCurrentOperator = (intent.getStringExtra("OperatorName"));


        // setTextView from MainActivity
        TvActiveFileName.append(gVAcvtiveFileName);
        TvSoftwareVersion.append(gVSoftwareVersion);

        // call internal file for credentials
        readInternalCredentialMessage();

    }
    // Deprecated
    public void readMessage ()
    {
        // Read out file which will eventually be exported out
        try
        {
            String Message;
            FileInputStream fileInputStream = openFileInput("InternalTestResultsFile");
            InputStreamReader inputStreamReader= new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while ( (Message = bufferedReader.readLine()) != null )
            {
                stringBuffer.append(Message + "\n");

            }
            // write out to Multiline
            //TvSavedData.append(String.valueOf(stringBuffer));
            //TvSavedData.setVisibility(View.VISIBLE);

            JSONData = stringBuffer;

        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    // This is the Previous button to Go back to last activity
    public void onButtonPrevClick(View v)
    {
        if(v.getId() == R.id.BtPrev)
        {
            // This will transition back over to Performtestcases activity
            Intent i = new Intent();
            setResult(RESULT_OK, i);
            finish();
        }
    }

    // This is the close App button
    public void onButtonCloseAppClick(View v)
    {
        if(v.getId() == R.id.BtCloseApp)
        {
            Toast.makeText(getApplicationContext(), " CLOSING APP; "  ,Toast.LENGTH_SHORT).show();
            // Kill the App not yet
            finishAffinity();
            System.exit(0);
        }
    }

    // This is the Export button to send file up
    //Deprecated
    /* public void onButtonExportClick(View v)
    {

        if (v.getId() == R.id.BtExport)
        {
            // Getting text from edittext
            String StUserName = gVStUserName;//UserName.getText().toString();
            String StPassWord = gVStPassWord;//PassWord.getText().toString();

            // send data
            SendData process = new SendData();
            process.execute(StUserName, StPassWord);

            //SendInProgressData process = new SendInProgressData();
            //process.execute(StUserName, StPassWord, gVFileToBeUsed, gVTruckPickedInSpinner, String.valueOf(TestStepCnt));
        }
    }*/

    public void readInternalCredentialMessage ()
    {
        // Read out file which will eventually be exported out
        try
        {
            String Message;
            FileInputStream fileInputStream = openFileInput("InternalCredentialsFile");
            InputStreamReader inputStreamReader= new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while ( (Message = bufferedReader.readLine()) != null )
            {
                stringBuffer.append(Message + "\n");

            }

            try
            {
                JSONObject mainObject = new JSONObject(String.valueOf(stringBuffer));//JSONStringWithoutNull);;
                // Get data out from internal saved file
                gVStUserName = mainObject.getString("UserName");
                gVStPassWord = mainObject.getString("Password");
                Log.i(TAG, " ReadOut from Internal File InternalCredentialsFile" + gVStUserName + gVStPassWord );   // debug msg

            } catch (JSONException e)
            {
                e.printStackTrace();
            }


        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    protected void sendEmail() {
        Log.i("Send email", "From App");

        String[] TO = {"tom.rode@crown.com"};
        String[] CC = {"mike.kovack@crown.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");


        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        //emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "JSON Data File from C-Course app");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "JSON Data attached");//(Serializable) JSONData);/" Small people eat small things");//JSONData);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Toast.makeText(getApplicationContext(),("EMAILING: "),Toast.LENGTH_LONG).show();
            //Log.i("Finished sending email...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            //Toast.makeText(MainActivity.this,
                    //"There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }
}
