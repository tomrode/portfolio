package com.example.t0028919.person_in_looptesttool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by t0028919 on 3/6/2018.
 *
 * Author: Thomas Rode
 *
 * Intent: This Activity will envoke as a result of user hitting "EVENTS?" Button from Perfortestcases activity. UI has six categorical sets of checkbox
 *         items. Each item has a String type that will append. Once UI "DONE" button hit these string are added together and an declared intent to pass
 *         this string using .putExtra() method back to Perfortestcases activity.
 */
public class Events extends Activity
{
    // setup UI variables
    CheckBox CbForksFirst;
    CheckBox CbPowerUnitFirst;
    CheckBox CbParkingBrake;
    CheckBox CbBraking;
    CheckBox CbPlugging;
    CheckBox CbUpGrade;
    CheckBox CbDownGrade;
    CheckBox CbRaise;
    CheckBox CbLower;
    CheckBox CbTiltUp;
    CheckBox CbTiltDown;
    CheckBox CbReach;
    CheckBox CbRetract;
    CheckBox CbSideShift;
    CheckBox CbClockWise;
    CheckBox CbCounterclockWise;
    CheckBox CbZeroPercentLoad;
    CheckBox CbTwentyFivePercentLoad;
    CheckBox CbFiftyPercentLoad;
    CheckBox CbSeventyFivePercentLoad;
    CheckBox CbOneHundredPercentLoad;
    CheckBox CbBlending;
    CheckBox CbRekeyTruckToClearCode;
    EditText EtEventsFreeField;

    // Substrings used by check Boxes
    String gVEventFreeField= "";
    String gVTravel = "";
    String gVHydraulic = "";
    String gVSteering = "";
    String gVLaden = "";
    String gVMisc = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events);

        // Setup UI
        CbForksFirst = (CheckBox) findViewById(R.id.CbForksFirst);
        CbPowerUnitFirst = (CheckBox) findViewById(R.id.CbPowerUnitFirst);
        CbParkingBrake = (CheckBox) findViewById(R.id.CbParkingBrake);
        CbBraking = (CheckBox) findViewById(R.id.CbBraking);
        CbPlugging = (CheckBox) findViewById(R.id.CbPlugging);
        CbUpGrade = (CheckBox) findViewById(R.id.CbUpGrade);
        CbDownGrade = (CheckBox) findViewById(R.id.CbDownGrade);

        CbRaise = (CheckBox) findViewById(R.id.CbRaise);
        CbLower = (CheckBox) findViewById(R.id.CbLower);
        CbTiltUp = (CheckBox) findViewById(R.id.CbTiltUp);
        CbTiltDown = (CheckBox) findViewById(R.id.CbTiltDown);
        CbReach = (CheckBox) findViewById(R.id.CbReach);
        CbRetract = (CheckBox) findViewById(R.id.CbRetract);
        CbSideShift = (CheckBox) findViewById(R.id.CbSideShift);

        CbClockWise = (CheckBox) findViewById(R.id.CbClockWise);
        CbCounterclockWise = (CheckBox) findViewById(R.id.CbCounterclockWise);

        CbZeroPercentLoad = (CheckBox) findViewById(R.id.CbZeroPercentLoad);
        CbTwentyFivePercentLoad = (CheckBox) findViewById(R.id.CbTwentyFivePercentLoad);
        CbFiftyPercentLoad = (CheckBox) findViewById(R.id.CbFiftyPercentLoad);
        CbSeventyFivePercentLoad = (CheckBox) findViewById(R.id.CbSeventyFivePercentLoad);
        CbOneHundredPercentLoad = (CheckBox) findViewById(R.id.CbOneHundredPercentLoad);

        CbBlending = (CheckBox) findViewById(R.id.CbBlending);
        CbRekeyTruckToClearCode = (CheckBox) findViewById(R.id.CbRekeyTruckToClearCode);

        EtEventsFreeField = (EditText)findViewById(R.id.EtEventsFreeField);

    }

    public void IsCheckBoxChecked()
    {
        // Part of the Travel Group
        if(CbForksFirst.isChecked() )
        {
            // Append this string
            gVTravel +=  "FF, ";

        }
        if( CbPowerUnitFirst.isChecked() )
        {
            // Append this string
            gVTravel +=  "PUF, ";
        }
        if( CbParkingBrake.isChecked() )
        {
            // Append this string
            gVTravel += "Parking Brake, ";
        }
        if( CbBraking.isChecked() )
        {
            // Append this string
            gVTravel += "Braking, ";
        }
        if( CbPlugging.isChecked() )
        {
            // Append this string
            gVTravel += "Plugging, ";
        }
        if( CbUpGrade.isChecked() )
        {
            // Append this string
            gVTravel += "UpGrade, ";
        }
        if(  CbDownGrade.isChecked() )
        {
            // Append this string
            gVTravel += "DownGrade, ";
        }

        // Part of the Hydraulics Group
        if(  CbRaise.isChecked() )
        {
            // Append this string
            gVHydraulic += "Raise, ";
        }
        if( CbLower.isChecked() )
        {
            // Append this string
            gVHydraulic += "Lower, ";
        }
        if(  CbTiltUp.isChecked() )
        {
            // Append this string
            gVHydraulic += "TiltUp, ";
        }
        if(  CbTiltDown.isChecked() )
        {
            // Append this string
            gVHydraulic += "TiltDown, ";
        }
        if(  CbReach.isChecked() )
        {
            // Append this string
            gVHydraulic += "Reach, ";
        }
        if(  CbRetract.isChecked() )
        {
            // Append this string
            gVHydraulic += "Retract, ";
        }
        if(  CbSideShift.isChecked() )
        {
            // Append this string
            gVHydraulic += "SideShift, ";
        }

        // Part of the Steering Group
        if(  CbClockWise.isChecked() )
        {
            // Append this string
            gVSteering += "CW, ";
        }
        if(  CbCounterclockWise.isChecked() )
        {
            // Append this string
            gVSteering += "CCW, ";
        }

        // Part of the Laden Group
        if(  CbZeroPercentLoad.isChecked() )
        {
            // Append this string
            gVLaden += "0% Load, ";
        }
        if(  CbTwentyFivePercentLoad.isChecked() )
        {
            // Append this string
            gVLaden += "25% Load, ";
        }
        if(  CbFiftyPercentLoad.isChecked() )
        {
            // Append this string
            gVLaden += "50% Load, ";
        }
        if(  CbSeventyFivePercentLoad.isChecked() )
        {
            // Append this string
            gVLaden += "75% Load, ";
        }
        if(  CbOneHundredPercentLoad.isChecked() )
        {
            // Append this string
            gVLaden += "100% Load, ";
        }

        // Part of the Misc Group
        if(  CbBlending.isChecked() )
        {
            // Append this string
            gVMisc += "Blending, ";
        }
        if(  CbRekeyTruckToClearCode.isChecked() )
        {
            // Append this string
            gVMisc += "RekeyTruckToClearCode, ";
        }
    }

    // This is the Previous button to Go back to last activity
    public void onButtonDoneClick(View v)
    {
        if(v.getId() == R.id.BtDone)
        {
            // Get the string from the edit text
            gVEventFreeField = ((EtEventsFreeField.getText().toString()) + ", ");

            // Run the checkbox is checked method
            IsCheckBoxChecked();
            String OutStringFromEvents = (gVEventFreeField) + gVTravel + gVHydraulic + gVSteering + gVLaden + gVMisc;
            //Toast.makeText(getApplicationContext(),("gVEventFreeField+gVTravel+gVHydraulic+gVSteering+gVLaden+misc: "+  OutStringFromEvents ),Toast.LENGTH_LONG).show();

            // This passes the individual variables to new Performtestcases activity
            Intent i = new Intent(Events.this, Performtestcases.class );
            i.putExtra("OutStringFromEvents",  OutStringFromEvents);
            setResult(RESULT_OK, i);
            finish();
        }
    }

}
