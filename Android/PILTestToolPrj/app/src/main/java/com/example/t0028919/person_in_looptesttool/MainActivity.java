package com.example.t0028919.person_in_looptesttool;


import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.OnLifecycleEvent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.content.Intent;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.lang.Object;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

//import static android.support.v4.provider.DocumentsContractApi21.listFiles;

/* Author: Thomas Rode
 *
 * Intent: This is the Main Activity once App is called. User must pick a test plan from spinner "Truck Model", then a specific test from spinner "Specific Test" These folders reside locally within
 *         External memory. User may also import folders using "IMPORT TEST CASES" button which envokes ES File Explorer. Test Request number must be filled and "LOAD TEST PLAN" button hit to move
 *         to the Performtestcases activity. Test Plans are JSON formatted.
 *
*/

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "TomsMessage";
    public static final byte TRUE = 1;
    public static final byte FALSE = 0;
    byte fAllowNext;

    // Global variables to pass out of activity
    String gVOperator = "";
    String gVAcvtiveFileName = "";
    String gVSoftwareVersion = "";
    String gVSynectTestCaseNumber = "";
    String gVDNGiRTCNumber = "";
    String gVNumberOfTestCases = "";
    String gVGroupTestCaseNumber = "";
    String gvTestRequestNumber = "";
    String gVTruckSerialNumber = "";
    String gVBatterySerialNumber = "";
    String gVTestInProgress = "";
    String gVNumberOfExecutedTestCases = "";
    String gVTruckPickedInSpinner = "";
    String gVExecutionGUID = "";

    // files in specifeid folder array
   // private List<String> filelist = new ArrayList<String>();

    // UI variables
    TextView ActiveFileName;
    TextView SoftwareVersion;
    public static TextView ServerConnection;
    EditText TestRequestNumber;
    EditText SerialNumber;
    EditText BatterySerialNumber;
    EditText Operator;
    EditText SoftWareVersionInput;
    EditText UserName;
    EditText PassWord;
    Button LoadTestPlan;
    Button LoadTestCases;
    Button Reset;
    Button BtImportInProgress;
    Button BtImportNewTestCases;

    // This is a bogus declaration to appease Release build
    TextView TvTestRequestnumber;

    // UI variable to show files and spinner
    Spinner PicktestPlan;
    Spinner SelectSpecificTestPlan;
    ArrayAdapter<CharSequence> adapter;
    ArrayList<String> arrayTruckList;

    // An Array of TestCase classed variables declaration and instantiation
    TestCase TestCaseAr[]=new TestCase[2000];

    // TestCase class variables
    Contact myfirstContact;

    // Test variable to see is JSON data in
    public  static TextView data;

    //JSON OBJECTS
    public String JSONString;

    //Spinner used for picking Specific Truck
    public int TruckPickedViaSpinner;
    public String TruckPickedInSpinner = "";
    private String AllFiles;
    public boolean fAllowWriteOfSpinnerVal = false;

    // This Variable will be the specific .json the user picked
    public String gVFileToBeUsed = "";

    // This flag is an interlock to make sure TestCase entered correctly
    public boolean fAllowPerformTestCasesActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set the orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Setup UI
        ActiveFileName = (TextView)findViewById(R.id.TvActiveFileName);
        SoftwareVersion = (TextView)findViewById(R.id.TvSoftwareVersion);
        ServerConnection = (TextView)findViewById(R.id.TvServerConnection);
        TestRequestNumber = (EditText)findViewById(R.id.EtTestRequestnumber);
        SerialNumber = (EditText)findViewById(R.id.EtSerialNumber);
        BatterySerialNumber = (EditText) findViewById(R.id.EtBatterySerialNumber);
        Operator = (EditText)findViewById(R.id.EtOperator);
        LoadTestPlan = (Button)findViewById(R.id.Btloadtestplan);
        LoadTestCases = (Button)findViewById(R.id.BtImportNewTestCases);
        SoftWareVersionInput = (EditText) findViewById(R.id.EtSoftWareVersionInput);
        UserName = (EditText) findViewById(R.id.EtUserName);
        PassWord = (EditText) findViewById(R.id.EtPassWord);
        Reset = (Button)findViewById(R.id.BtReset);
        BtImportInProgress = (Button)findViewById(R.id.BtImportInProgress);
        BtImportNewTestCases = (Button)findViewById(R.id.BtImportNewTestCases);

        Reset.setVisibility(View.INVISIBLE);
        //ServerConnection = (TextView)findViewById(R.id.TvServerConnection);

        // Under Construction for allowing automatic External storage access
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
        // End Construction

        // Set up folder structure if not already there
        CreateExternalStorageFolders();

        // try to get a list from external stroage directory
        // Create a folder
        File Root = Environment.getExternalStorageDirectory();
        File Dir = null;
        try
        {
            // Determine if user wanted to install In-Progress test cases the load from corresponding folder else it comes from Ready_for_Execution as usual
            if (readInternalInProgressFromServer ().equals("true"))
            {
                Dir = new File (Root. getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/In_Progress_From_Server" );

                // Write internal file flag variable set it false so it locks out the change in this path
                //writeInternalInProgressFromServer( "false" );
            }
            else
            {
                Dir = new File (Root. getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/Ready_for_Execution" );
            }

            //Dir = new File (Root. getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/Ready_for_Execution" );

            File[] files = Dir.listFiles();

            //Log.i(TAG, "IN ONCREATE TO SEE DIRECTORY ---------------------------->:" +  Dir);   // debug msg
            arrayTruckList = new ArrayList<String>();
            for (int i = 0; i < files.length; i++)
            {
                AllFiles = (files[i].getName() );//+ "\n"); <---- Caused hours of time to diagnose

                // Collect current set of trucks from the folder dynamically;
                arrayTruckList.add(AllFiles);
            }
            Log.i(TAG, "IN ONCREATE TO SEE DIRECTORY FOLDERS in arrayTruckList ------------------------>:" +  arrayTruckList);   // debug msg

            // UI for the spinner
            PicktestPlan = (Spinner)findViewById(R.id.SpShowTruck);
            //adapter = ArrayAdapter.createFromResource(this,R.array.TestCasesSelection,android.R.layout.simple_spinner_item);
            ArrayAdapter<String> adapter =  new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item, arrayTruckList);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            PicktestPlan.setAdapter(adapter);
            PicktestPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                    // This is a result of the selection
                    Toast.makeText(getBaseContext(),(parent.getItemAtPosition(position) + " selected"), Toast.LENGTH_SHORT).show();

                    // This is the truck the user picked fo its specific testcases
                    TruckPickedViaSpinner = position;

                    // allow to access this variable
                    fAllowWriteOfSpinnerVal = true;

                    // set the string to correct Truck Folder
                    TruckPickedInSpinner = (String) parent.getItemAtPosition(position);

                    // set the global variable for truck sub-folder
                    gVTruckPickedInSpinner = TruckPickedInSpinner;

                    // set up file array to show folder contents and use the global variable to get the correct file to execute
                    ListDir();

                    Log.i(TAG, "IN ONCREATE setOnItemSelectedListener To see if I can get TruckPickedInSpinner ------------------------>:" + TruckPickedInSpinner);   // debug msg
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

        }
        catch (Exception e)
        {
            Toast.makeText(getBaseContext(), ( Root + "/PIL_SYNECT_TEST_PLAN/Ready_for_Execution" + "---> MISSING, IMPORT TEST CASE then Restart App" ), Toast.LENGTH_LONG).show();
        }

        if(fAllowWriteOfSpinnerVal == false)
        {
            // set the string to correct Truck Folder
            TruckPickedInSpinner = "/C-5 DV123";
        }
        // testing the class
        myfirstContact = new Contact("Tom", "Rode", "2484210862");
        System.out.println("Name:"+ myfirstContact.getName());

        // Lockout the next button
        fAllowNext = FALSE;


        // eventhandling for button using OnClickListener interface
        LoadTestPlan.setOnClickListener(new View.OnClickListener()
        {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view)
            {

                // Now json data will come from internal folder and now longer FetchData method here
                // Step 1. Open the correct .json
                // Step 2. Make the contents a json object strip out the [] and null
                // Step 3. Set the string to JSONString below and hope it works
                // Create a folder
                String data ="";
                String jString = null;
                try {
                    FileInputStream stream = null;
                    //InputStream stream = null;
                    try {

                        // See if there is a file within th In-Progress folder
                        // See if folder exists then make it
                        // Create a folder
                        File InProgress = Environment.getExternalStorageDirectory();
                        File Dir = new File (InProgress. getAbsolutePath() +"/PIL_SYNECT_TEST_PLAN/In_Progress/" + TruckPickedInSpinner + "/"  + gVFileToBeUsed);
                        if(Dir.exists() )
                        {
                            Toast.makeText(getApplicationContext(),("This file in the directory EXISTS: " + Dir ),Toast.LENGTH_LONG).show();
                            stream = new FileInputStream(Dir);
                        }
                        else
                        {
                            File Root = Environment.getExternalStorageDirectory();

                            // Determine if user wanted to install In-Progress test cases the load from corresponding folder else it comes from Ready_for_Execution as usual
                            if (readInternalInProgressFromServer ().equals("true"))
                            {
                                String CompletePath = ( Root+ "/PIL_SYNECT_TEST_PLAN/In_Progress_From_Server/" + TruckPickedInSpinner + "/"  + gVFileToBeUsed);
                                stream = new FileInputStream(CompletePath);
                            }
                            else
                            {
                                String CompletePath = ( Root+ "/PIL_SYNECT_TEST_PLAN/Ready_for_Execution/" + TruckPickedInSpinner + "/"  + gVFileToBeUsed);
                                stream = new FileInputStream(CompletePath);
                            }
                        }

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    //String jString = null;
                    FileChannel fc = stream.getChannel();
                    MappedByteBuffer bb = null;
                    try {
                        bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    /* Instead of using default, pass in a decoder. */
                    jString = Charset.defaultCharset().decode(bb).toString();
                    Log.i(TAG, "onClick  jString ------------------------>:" + jString);   // debug msg
                }
                finally {
                    //stream.close();
                }

                String JSONStringWithoutBrackets =  jString.replace("[", "");
                String JSONStringWithoutBrackets2 =  JSONStringWithoutBrackets.replace("]", "");
                Log.i(TAG, "onClick  JSONStringWithoutBrackets2 ------------------------>:" + JSONStringWithoutBrackets2);   // debug msg

                // Parse the json string brought in from FetchData
                String TestRequestNumberJSON1= "";
               try
               {
                   // Setup JSON Object and parse out the values from keys
                   JSONObject mainObject = new JSONObject(JSONStringWithoutBrackets2);//JSONStringWithoutNull);

                   // Getting text from edittext, Make sure N/A or n/a or Txx-xxxx is typed input or texted input txx - xxx
                   gvTestRequestNumber = TestRequestNumber.getText().toString();
                   String TChar = "";
                   String DashChar = "";
                   String DashCharSpoken = "";

                   if(gvTestRequestNumber.equals("NA") || gvTestRequestNumber.equals("na"))
                   {
                       // this will allow the next activity to be accessed
                       Toast.makeText(getApplicationContext(),("TestRequestNumber is NA or na"),Toast.LENGTH_LONG).show();
                       fAllowPerformTestCasesActivity = true;
                   }

                   else
                   {
                       // This will ensure app does not crash if nothing entered
                       try
                       {
                           // Get only the first character check for "T" or "t"
                           TChar = gvTestRequestNumber.substring(0,1);

                           // Get the fourth character  "-" or spoken "-" fifth character
                           DashChar = gvTestRequestNumber.substring(3,4);
                           DashCharSpoken = gvTestRequestNumber.substring(4,5);

                           //if((gvTestRequestNumber.equals("N/A") || gvTestRequestNumber.equals("n/a")) || (TChar.equals("T") || (TChar.equals("t") && DashChar.equals("-") )))
                           if( (TChar.equals("T") || TChar.equals("t"))  && (DashChar.equals("-") || DashCharSpoken.equals("-")  ) )
                           {
                               // this will allow the next activity to be accessed
                               //Toast.makeText(getApplicationContext(),("TestRequestNumber CORRECT FORMAT"),Toast.LENGTH_LONG).show();
                               fAllowPerformTestCasesActivity = true;
                           }
                           else
                           {
                               Toast.makeText(getApplicationContext(),("TestRequestNumber INCORRECT.... Please Enter N/A or n/a OR TC Number Format Txx-xxxxx"),Toast.LENGTH_LONG).show();
                               gvTestRequestNumber = "";
                           }
                       }
                       catch (Exception e)
                       {

                           Toast.makeText(getApplicationContext(), ("Please Enter a Test Request Number or NA"), Toast.LENGTH_LONG).show();

                       }
                   }

                   gVAcvtiveFileName = mainObject.getString("ActiveTestFileName");
                   // Get value from edit text
                   gVTruckSerialNumber = SerialNumber.getText().toString();//mainObject.getString("TruckSerialNumber");
                   gVNumberOfTestCases = mainObject.getString("NumberOfTestCases");
                   gVTestInProgress = mainObject.getString("TestInProgress");
                   gVNumberOfExecutedTestCases = mainObject.getString("NumberOfExecutedTestCases");
                   // Get battery serial number from edit text
                   gVBatterySerialNumber =  BatterySerialNumber.getText().toString();//mainObject.getString("BatterySerialNumber"); // Getting text from edittext, place into global variable
                   gVExecutionGUID = mainObject.getString("ExecutionGUID");
                   // Test Case populate from json
                   int i;
                   for (i=1; i<= Integer.parseInt(gVNumberOfTestCases); i++)
                   {
                       TestCaseAr[i] = new TestCase();
                       TestCaseAr[i].setExpectedResults(mainObject.getString("TC" + i +"ExpectedResult"));
                       TestCaseAr[i].setNotes(mainObject.getString("TC" + i + "Comment"));              //"Notes"));
                       TestCaseAr[i].setOperator(mainObject.getString("TC" + i + "Operator"));
                       TestCaseAr[i].setResults(mainObject.getString("TC" + i + "Verdict"));            //"Result"));
                       TestCaseAr[i].setSteps(mainObject.getString("TC" + i + "Steps"));
                       TestCaseAr[i].setTesttime(mainObject.getString("TC" + i + "Execution Duration"));//"Testtime"));
                       TestCaseAr[i].setH1HourMeter(mainObject.getString("TC" + i + "H1_Hourmeter"));
                       TestCaseAr[i].setOdometer(mainObject.getString("TC" + i + "Odometer"));
                       TestCaseAr[i].setSynectTcNum(mainObject.getString("TC" + i + "TestCaseNumber"));      // New
                       TestCaseAr[i].setGroupTcNum(mainObject.getString("TC" + i + "Name"));   //"Group#"));   // New
                       TestCaseAr[i].setDngIrtc(mainObject.getString("TC" + i + "DNG_iRTC#"));           // New
                       TestCaseAr[i].setGuid(mainObject.getString("TC" + i + "GUID"));           // New
                   }
                   Log.i(TAG, "TestCaseAr[1] ------------------------>:" +"/" +  TestCaseAr[1]);   // debug msg
                   Log.i(TAG, "TestCaseAr[2] ------------------------>:" +"/" +  TestCaseAr[2]);   // debug msg
                   Log.i(TAG, "TestCaseAr[3] ------------------------>:" +"/" +  TestCaseAr[3]);   // debug msg
                   Log.i(TAG, "TestCaseAr[4] ------------------------>:" +"/" +  TestCaseAr[4]);   // debug msg
                   Log.i(TAG, "TestCaseAr[5] ------------------------>:" +"/" +  TestCaseAr[5]);   // debug msg


               } catch (JSONException e)
                {
                    e.printStackTrace();
                }

                // Unlock the next button
                fAllowNext = TRUE;

                // Getting text from edittext
                String OperatorName = Operator.getText().toString();
                String TestRequestNumber1 = TestRequestNumber.getText().toString();
                String SerialNumber1 = SerialNumber.getText().toString();
                String BatterySerialNumber1 = BatterySerialNumber.getText().toString();
                String SoftWareVersion = SoftWareVersionInput.getText().toString();

                // Always get new Test request number
                gvTestRequestNumber = TestRequestNumber1;
                gVOperator = OperatorName;
                gVSoftwareVersion = SoftWareVersion;


                // Clear out old values in case button hit again
                ActiveFileName.setText("");
                SoftwareVersion.setText("");

                //Append Text view text
                ActiveFileName.append("Active File Name:" + gVAcvtiveFileName);
                SoftwareVersion.append("S/W Version: " + gVSoftwareVersion);

                // See if there were an amount of previous in-progress tests if so then populate this from the file values
                if( (gVTestInProgress.equals("No")) || (gVTestInProgress.equals("")) )
                {
                    gVTruckSerialNumber = SerialNumber1;
                    gVBatterySerialNumber = BatterySerialNumber1;

                }
            }
        });

    }

    @Override
    public void onStart(){
        super.onStart();
        if ( ServerConnection.getText().equals("Successful"))
        {
            Toast.makeText(getApplicationContext(),("HERE IS THE SERVERCONNECTION Success: "),Toast.LENGTH_SHORT).show();
        }

    }


    void ListDir()
    {
        String TestString = arrayTruckList.get(TruckPickedViaSpinner);
        Log.i(TAG, "INListDir() Looking at TestString ------------------------>:" +"/" +  TestString);   // debug msg

        // Create a folder
        File Root = Environment.getExternalStorageDirectory();

        final File Dir;

        // Determine if user needs to run an in_progress test then load accordingly
        if (readInternalInProgressFromServer ().equals("true"))
        {
            Dir = new File(Root.getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/In_Progress_From_Server" + "/"+ TruckPickedInSpinner);

            // Write internal file flag variable set it false so it locks out the change in this path
            //writeInternalInProgressFromServer( "false" );
        }
        else
        {
            Dir = new File(Root.getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/Ready_for_Execution" + "/"+ TruckPickedInSpinner);
        }
        //Dir = new File(Root.getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/Ready_for_Execution" + "/"+ TruckPickedInSpinner);

        File[] files = Dir.listFiles();

        // this is used for next spinner selections
        ArrayList<String> arrayTestCaseList = new ArrayList<String>();

        // Capture the current size of arrayTestCastList
        int arrayTestCastListSize = files.length;
        for (int i = 0; i < files.length; i++)
        {
            AllFiles = (files[i].getName() );//+ "\n");

            // Collect current set of test cases;
            arrayTestCaseList.add(AllFiles);

        }
        //Remove the all ready completed item from array
        // See if folder exists then make it
        // Create a folder

        File InProgress = Environment.getExternalStorageDirectory();
        File DirComplete = new File (InProgress. getAbsolutePath() +"/PIL_SYNECT_TEST_PLAN/Completed_Execution/" + TruckPickedInSpinner); //+ "/"  + gVFileToBeUsed);
        File[] filesComplete = DirComplete.listFiles();
        ArrayList<String> arrayTestCaseListFiles = new ArrayList<String>();
        int fileCompleteIndex = 0;
        if(DirComplete.exists() )
        {
            for (int i = 0; i < filesComplete.length; i++)
            {
                AllFiles = (filesComplete[i].getName() );

                // Collect current set of test cases;
                arrayTestCaseListFiles.add(AllFiles);

                // Place holder index
                fileCompleteIndex++;

                // See if possible to remove from array here
            }
            //Toast.makeText(getApplicationContext(),("These are the files  in arrayTestCaseListFiles in the directory EXISTS: " + arrayTestCaseListFiles ),Toast.LENGTH_LONG).show();

            // At this point these arrayList can be different Sizes, so pad the smaller with data
            for (int i = 0; i < files.length; i++)
            {
                // Get these lengths
                int filesLength = files.length;
                int filesCompleteLength = filesComplete.length;

                if( filesCompleteLength < filesLength)
                {
                    arrayTestCaseListFiles.add("Padding");
                    break;

                }
            }
        }

       // Check to see what files are the same in both ArrayLists, now they are both the same size
        ArrayList<String> arrayTestCaseListFilesFinal = new ArrayList<String>();
        for (int i = 0; i < (files.length); i++)
        {
            // Remove the line item from the array list
            if(arrayTestCaseListFiles.contains(arrayTestCaseList.get(i)) == false)
            {
                // add only wat left from original arrayList
                arrayTestCaseListFilesFinal.add(arrayTestCaseList.get(i));
            }
        }
        //Toast.makeText(getApplicationContext(),("This is what will be seen in FINAL: " + arrayTestCaseListFilesFinal ),Toast.LENGTH_LONG).show();

        //Log.i(TAG, "IN ListDir() THE Dir ------------------->>>>>>>" +  Dir);   // debug msg

        // Selection of the spinner
        SelectSpecificTestPlan = (Spinner) findViewById(R.id.SpSelectSpecificTestPlan);

        // Application of the Array to the Spinner
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item,arrayTestCaseListFilesFinal); //arrayTestCaseList);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        SelectSpecificTestPlan.setAdapter(spinnerArrayAdapter);

        // Set up a Listener to do something
        SelectSpecificTestPlan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                // This is a result of the selection

                String FileToBeUsed = (String) parent.getItemAtPosition(position);
                gVFileToBeUsed = (String)parent.getItemAtPosition(position);
                Log.i(TAG, "THE gVFileToBeUsed:" +  gVFileToBeUsed);   // debug msg

                // This is a result of the selection
                Toast.makeText(getBaseContext(), (gVFileToBeUsed + "---------- The file Selected"), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    // Reset the Main Activity
    public void onButtonResetClick(View v)
    {
        // Reset the Activity
        if(v.getId() == R.id.BtReset)
        {
            // See if Async task onPostExecute() complete
            if ( ServerConnection.getText().equals("Successful"))
            {
                Toast.makeText(getApplicationContext(),("HERE IS THE SERVER CONNECTION Success: "),Toast.LENGTH_SHORT).show();
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        }
    }


    // Import test cases from Server in Ready_for_Execution
    public void onButtonImportTestCasesclick(View v)
    {
        if(v.getId() == R.id.BtImportNewTestCases)
        {
            // Grey out the button to indicate it pressed
            BtImportNewTestCases.setTextColor(Color.GRAY);

            // Let user know that data is fetching
            Toast.makeText(getApplicationContext(),("Going to Fetch Data from engSTL1 server Ready_for_Execution folder  " ),Toast.LENGTH_LONG).show();

            // Getting text from edittext
            String StUserName = UserName.getText().toString();
            String StPassWord = PassWord.getText().toString();

            // Write internal file for Credential
            writeInternalCredentialMessage(StPassWord, StUserName);

            // This is used for eventual Windows server fetch this will be for an undetermined amount of time then reset
            FetchData process = new FetchData();
            process.execute(StUserName, StPassWord);

            // Write internal file flag variable set it true so once reset the In_Progress_From_Server folder is to be selected from
            writeInternalInProgressFromServer( "false" );

            // Set the Reset button visible
            Reset.setVisibility(View.VISIBLE);
        }
    }

    // Import test cases from Server In_Progress folder
    public void onButtonImportInProgressClick(View v)
    {
        if (v.getId() == R.id.BtImportInProgress)
        {

            // Grey out the button to indicate it pressed
            BtImportInProgress.setTextColor(Color.GRAY);

            // Getting text from edittext
            String StUserName = UserName.getText().toString();
            String StPassWord = PassWord.getText().toString();

            // Show user That a fetch occuring to Server
            Toast.makeText(getApplicationContext(),("Going to Fetch Data from engSTL1 server In_Progress folder  " ),Toast.LENGTH_LONG).show();

            // This is used for Windows server fetch in-progress this will be for an undetermined amount of time then reset
            FetchInProgressData process = new FetchInProgressData();
            process.execute(StUserName, StPassWord);

            // Write internal file flag variable set it true so once reset the In_Progress_From_Server folder is to be selected from
            writeInternalInProgressFromServer( "true" );

            // Set the Reset button visible
            Reset.setVisibility(View.VISIBLE);
        }
    }
    // Set up folder structure if not already there
    void CreateExternalStorageFolders()
    {
        String State;

        // Check if SD card installed
        State = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(State))
        {
            // External store path
            File Root = Environment.getExternalStorageDirectory();

            // Create a root folder named PIL_SYNECT_TEST_PLAN
            File PSTP_Dir = new File(Root.getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/");
            // See if folder exists then make it
            if (!PSTP_Dir.exists())
            {
                // Generate External Storage File folder
                PSTP_Dir.mkdirs();
                Toast.makeText(getApplicationContext(),(" Generation directory folder from server " + PSTP_Dir),Toast.LENGTH_SHORT).show();
            }

            // Create a sub-root folder named In_Progress_From_Server
            File IPFSDir = new File(Root.getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/In_Progress_From_Server/");
            // See if folder exists then make it
            if (!IPFSDir.exists())
            {
                // Generate External Storage File folder
                IPFSDir.mkdirs();
                Toast.makeText(getApplicationContext(),(" Generation directory folder from server " + IPFSDir),Toast.LENGTH_SHORT).show();
            }

            // Create a sub-root folder named In_Progress
            File IPDir = new File(Root.getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/In_Progress/");
            // See if folder exists then make it
            if (!IPDir.exists())
            {
                // Generate External Storage File folder
                IPDir.mkdirs();
                Toast.makeText(getApplicationContext(),(" Generation directory folder from server " + IPDir),Toast.LENGTH_SHORT).show();
            }

            // Create a sub-root folder named Ready_for_Execution
            File RFEDir = new File(Root.getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/Ready_for_Execution/");
            // See if folder exists then make it
            if (!RFEDir.exists())
            {
                // Generate External Storage File folder
                RFEDir.mkdirs();
                Toast.makeText(getApplicationContext(),(" Generation directory folder from server " + RFEDir),Toast.LENGTH_SHORT).show();
            }

            // Create a sub-root folder named Completed_Execution
            File CEDir = new File(Root.getAbsolutePath() + "/PIL_SYNECT_TEST_PLAN/Completed_Execution/");
            // See if folder exists then make it
            if (!CEDir.exists())
            {
                // Generate External Storage File folder
                CEDir.mkdirs();
                Toast.makeText(getApplicationContext(),(" Generation directory folder from server " + CEDir),Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "SD Card not found "  ,Toast.LENGTH_SHORT).show();
        }

    }

    // This is the next button that changed to Activity
    public void onButtonNextClick(View v)
    {
       if((v.getId() == R.id.BtNext) && (fAllowNext == TRUE) && (fAllowPerformTestCasesActivity) )
       {

           Intent i = new Intent(MainActivity.this, Performtestcases.class );

           // Set off toast to show the operator
           //Toast.makeText(getApplicationContext(),("This is the NumberOfTestcases: " + gVNumberOfTestCases),Toast.LENGTH_LONG).show();

           // This passes the individual variables to new Performtestcases activity
           i.putExtra("OperatorName",  gVOperator);
           i.putExtra("ActiveFileName", gVAcvtiveFileName);
           i.putExtra("SoftwareVersion", gVSoftwareVersion);
           i.putExtra("NumberOfTestCases", gVNumberOfTestCases);
           i.putExtra("TruckSerialNumber",gVTruckSerialNumber);
           i.putExtra("BatterySerialNumber",gVBatterySerialNumber);
           i.putExtra("TestInProgress",gVTestInProgress);
           i.putExtra("NumberOfExecutedTestCases",gVNumberOfExecutedTestCases);
           i.putExtra("FileToBeUsed",gVFileToBeUsed);
           i.putExtra("TruckPickedInSpinner",gVTruckPickedInSpinner);
           i.putExtra("TestRequestNumber",gvTestRequestNumber);
           i.putExtra("ExecutionGUID",gVExecutionGUID);

           // Test Case populate from json
           int j;
           for (j=1; j<= Integer.parseInt(gVNumberOfTestCases); j++)
           {
               i.putExtra("TC" + j + "MainActivity", TestCaseAr[j]);
           }

           // start next activity
           startActivity(i);
       }
    }

    // This method will write output results to the internal storage containing flag to see if need to get from server in-progress folder
    public void writeInternalInProgressFromServer(String fAllow )
    {
        // Specific file with private access, no other apps can access this file
        String string = "";
        String InternalInProgressFromServer = "InternalInProgressFromServerFile";

        // Convert variables back as JSONObjects
        JSONObject jsonObject = new JSONObject();

        try
        {                   // Key                            // Value
            jsonObject.put("fAllow",                  fAllow);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        // Write the file with JSON data
        FileOutputStream outputStream;

        try
        {
            outputStream = openFileOutput(InternalInProgressFromServer, MODE_PRIVATE);
            string = jsonObject.toString();
            outputStream.write(string.getBytes());
            outputStream.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public String readInternalInProgressFromServer ()
    {
        String result = "";

        // Read out file which will eventually be exported out
        try
        {
            String Message;
            FileInputStream fileInputStream = openFileInput("InternalInProgressFromServerFile");
            InputStreamReader inputStreamReader= new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while ( (Message = bufferedReader.readLine()) != null )
            {
                stringBuffer.append(Message + "\n");

            }

            try
            {
                JSONObject mainObject = new JSONObject(String.valueOf(stringBuffer));//JSONStringWithoutNull);;
                // Get data out from internal saved file
                result = mainObject.getString("fAllow");

                Log.i(TAG, " ReadOut from Internal File readInternalInProgessFromServer" + result );   // debug msg

            } catch (JSONException e)
            {
                e.printStackTrace();
            }


        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return result;
    }



    // This method will write output results to the internal storage containing login credentials
    public void writeInternalCredentialMessage(String StPassWord, String StUserNam )
    {
        // Specific file with private access, no other apps can access this file
        String string = "";
        String InternalCredentials = "InternalCredentialsFile";

        // Convert variables back as JSONObjects
        JSONObject jsonObject = new JSONObject();

        try
        {                   // Key                            // Value
            jsonObject.put("Password",                  StPassWord);
            jsonObject.put("UserName",                  StUserNam);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        // Write the file with JSON data
        FileOutputStream outputStream;

        try
        {
            outputStream = openFileOutput(InternalCredentials, MODE_PRIVATE);
            string = jsonObject.toString();
            outputStream.write(string.getBytes());
            outputStream.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    // THis class will have the same attributes for each test case
    public class Contact
    {
        String name;
        String lastname;
        String phonenumber;

        public Contact(String name, String lastname, String phonenumber)
        {
            this.name = name;
            this.lastname = lastname;
            this.phonenumber = phonenumber;
        }

        public String getName()
        {
            return name;
        }

        public String getLasttname()
        {
            return lastname;
        }

        public String getPhonenumber()
        {
            return phonenumber;
        }
    }

}