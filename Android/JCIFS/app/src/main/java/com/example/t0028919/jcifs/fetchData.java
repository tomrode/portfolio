package com.example.t0028919.jcifs;


import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import android.util.Log;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;


/**
 * Created by Abhishek Panwar on 7/14/2017.
 */

public class fetchData extends AsyncTask<Void,Void,Void> {
    String data ="";
    String dataParsed = "";
    String singleParsed ="";
    private static final String TAG = "TomsMessage";
    // Example from internet
    static final String USER_NAME = "t0028919";
    static final String PASSWORD = "Lift0091";
    //e.g. Assuming your network folder is: \my.myserver.netsharedpublicphotos
    static final String NETWORK_FOLDER = "smb://engstl1.us.crownlift.net/PIL_SYNECT_TEXT_PLAN/Ready_for_execution";
    static final String fileContent = "Here is something";
    static boolean successful;
    @Override
    protected Void doInBackground(Void... voids) {

        Log.i(TAG, "IN AsyncTask ---------------------------->:" );   // debug msg
       try{
            String user = USER_NAME + ":" + PASSWORD;
            System.out.println("User: " + user);

            NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(user);
            String path = NETWORK_FOLDER + "testfile.txt";
            System.out.println("Path: " + path);

            SmbFile sFile = new SmbFile(path, auth);
            SmbFileOutputStream sfos = new SmbFileOutputStream(sFile);
            sfos.write(fileContent.getBytes());

            successful = true;
            System.out.println("Successful" + successful);
        } catch (Exception e) {
            successful = false;
            e.printStackTrace();
        }
        return null;
    }



       /*try {

            URL url = new URL("https://api.myjson.com/bins/1ecanl");
            //Fully qualified name: engstl1.us.crownlift.net/PIL_SYNECT_TEXT_PLAN/Ready_for_execution
            //URL url = new URL("https://engstl1.us.crownlift.net/PIL_SYNECT_TEXT_PLAN/Ready_for_execution");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while(line != null){
                line = bufferedReader.readLine();
                data = data + line;
            }

            JSONArray JA = new JSONArray(data);
            for(int i =0 ;i <JA.length(); i++){
                JSONObject JO = (JSONObject) JA.get(i);

                singleParsed =  "ActiveTestFileName:" + JO.get("ActiveTestFileName") + "\n"+
                        "TruckSerialNumber:" + JO.get("TruckSerialNumber") + "\n"+
                        "TestRequestNumber:" + JO.get("TestRequestNumber") + "\n"+
                        "SoftWareVersion:" + JO.get("SoftWareVersion") + "\n"+
                        "Synect_TC#:" + JO.get("Synect_TC#") + "\n"+
                        "Group_TC#:" + JO.get("Group_TC#") + "\n"+
                        "DNG_iRTC#:" + JO.get("DNG_iRTC#") + "\n"+
                        "NumberOfTestCases:" + JO.get("NumberOfTestCases") + "\n";

                dataParsed = dataParsed + singleParsed +"\n" ;

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dataParsed = null;
        return null;


    }  */

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        MainActivity.data.setText(this.dataParsed);

    }
}