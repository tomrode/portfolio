## Synopsis

    - This project will allow C-Course drivers to run test sequences as prescribed by means of a tablet computer.

## Code Example

Show what the library does as concisely as possible, developers should be able to figure out **how** your project solves their problem by looking at the code example. Make sure the API you are showing off is obvious, and that your code is short and concise.

## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Installation
	Needed installations:
		1. Java Development Kits. https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html.
		   - Download from Oracle site proper jdk for the operating system to be used. i.e. (jdk-9.0.1_windows-x64_bin.exe) for Windows 64 bit version.
		   - Double click executable and let install process. Leave every thing in default location.
		   - Add an Environmental variable named -> 'JAVA_HOME' and its variable is the installed jdk Variable value -> 'C:\Program Files\Java\jdk-9.0.1' 
		     This is necessary for Android studio.  
			 
		2. Android Studio. https://developer.android.com/studio/.
		   - Download (android-studio-ide-171.4443003-windows.exe) or which is the latest version.
           - Install defaults and let process complete.
           - The initial open of Android studio may not have all packages. Open android studio as Administrator, Select 'Configure' pick 'SDK Manager'. 
             The window 'Android SDK Manager' appears and if prompted 'Install Packages', let process complete. Then close Android Studio, open again to see if changes take effect.      		   


## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests
	Typical tests should be run on target device, Emulators can be slow.
    
	If debugging, making a Toast is a quick way to check logic or variable contents please however remove them once debugging complete.
														Examples 
	Toast.makeText(getApplicationContext(),(" YOU ARE NOW BACK TO CURRENT UN-EXECUTED TEST CASE"   ),Toast.LENGTH_LONG).show();  // this will last for two seconds since "LENGTH_LONG".
	
	Another debugging approach is with the 'Logcat' if using an actual device. In the IDE hit ALT+6 and the bottom will pop up the Logcat. To add debug Logs add your TAG and Log function
	
													// log message
													import android.util.Log;
													
													private static final String TAG = "TomsMessage";
													Log.i(TAG, "OnDestroy in Performtestcases envoked" );   // debug msg	
													
	Once you place Log.i function where you need, Once the function is called you will see this result in the consul. Also it may be desired to filter just for your TAG. 
	This can be done in the Logcat in the top right next to the Regex option.
	
	
## Contributors

Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.

## License

A short snippet describing the license (MIT, Apache, etc.)

# Project Sdk Version
 applicationId "com.crown.t0028919.person_in_looptesttool"
 minSdkVersion 19
 targetSdkVersion 26
 versionCode 1
 versionName "1.9r smb"

## Naming Convention
   -  Java activity Source files shall start with Upper case letter 
      - MainActivity.java, Activityone.java  
   -  User interface activity files ".xml" shall have the same name but lower case 
      - activityone.xml
   -  Widgets ID names should pre-pend with what kind of widget it it. Example a Button widget variable name should start with Btxxx, TextView = Tvxxx   
      - TvVersion
	  
## Change Log
  ** Date ** Author **
    - Description
	
	** 1/11/2018 ** Tom Rode **
    - Adding in this initial ReadMe into project. 
    - Adding AppDesignData folder to the project. 	
	
	** 1/18/2018 ** Tom Rode **
    - Did an initial trial Android project . 
    - Added in folder "PILTestToolPrj" and a start to Person-in-loop Test tool this will be the folder going forward. 	

	** 1/23/2018 ** Tom Rode **
    - Got JSON example to work but its in own project. 
    - UI work. 	
	
	** 1/24/2018 ** Tom Rode **
    - asked IT about https://engstl1.us.crownlift.net. Need this to get .json info 
    - front page UI work and some logic in MainActivity.java. 

	** 1/25/2018 ** Tom Rode **
    - Got MainActivity variables to pass to Performtestcases activity using an intent    
    - A lot UI work on Performtestcases.xml and more logic in Performtestcases.java. 
    - Got a hold of Matt Schumaker about where .json should reside, He will investigate since this seems like a "Cloudish" data-base, Some talk about "ONE_DRIVE" 
      SYNECT may not have WEB interface.
	  
	** 1/26/2018 ** Tom Rode **
    - More front page UI work and logic in Performtestcases.java. 
	- Added UI exporttestresults.xml and Exporttestresults.java, also in manifest. 
	- Coming to the conclusion MainActivity will pass the .json variables eventually, also had meeting with DSPACE about json format they want us to provide how this will look
	
	** 1/29/2018 ** Tom Rode **
	- Added UI exporttestresults.xml and Exporttestresults.java, also in manifest. 
	- Trying to finalize JSON format, wish to have test steps associated with one key only, did this proof of concept on JSONParsing project
	
	** 1/30/2018 ** Tom Rode **
	- Within Exporttestresults.java got the logic in place to call back Performtestcases activity. 
	- Did some more work on json file JSONParsing project to bench mark how it handles larger amounts of data, still might need to make some class to contain it all.
	- Looking into timers for "next" button 
	
	** 1/31/2018 ** Tom Rode **
	- Got timers for "next" button working, using a count down timer scheme which counts up a variable then the next button resets this counter once hit.  
	- Made a new class for Test cases in the hope to pass between activities using the intents for "putExtra". 
	
	** 2/01/2018 ** Tom Rode **
	- Made a new class for Test cases and does pass between activities using the intents for "putExtra".
    - Debugging issue with Edittext in performtestcases make app crash 	
	
    ** 2/01/2018 ** Tom Rode **
	- issue with TestCase class transfer to Exporttestresults activities using the intents for "putExtra".
    - About have all logic working in performtestcases  

    ** 2/05/2018 ** Tom Rode **
	- Bypassed this issue with TestCase class transfer to Exporttestresults activities using the intents for "putExtra".
      Simply saving JSON formatted file done in performtestcases activity, which is verified in exporttestresults activity.   	
	  
	** 2/06/2018 ** Tom Rode **
	- Fixed Group Test Case # to output the TestCase step count.
    - Integrated in the JSON example made new class FetchData and got data with a process.get() call, still issue with JSON parsing 	
	 
	** 2/09/2018 ** Tom Rode **
	- Got parsing issue resolved, had to remove the null at the end of the json string and the "[]" needed to be stripped out since its not an array.
	- Got Output JSON file to save "EXTERNALLY",, this will require another step on the app its self to have permissions allowed to write to storage device.
	- Fixed small observation Howard made about clearing out comments line each time next test case comes up.
	- Added (Maybe just for a short time) the ability to email the results, wanted to see the end to end process work.
	
	** 2/14/2018 ** Tom Rode **
	- Finally able to access EngSTL1, Setup for each device will be required VPN, Need to know Domain and server.
    - Added in two drop down spinner UI elements used to pick the test cases desired to run. 
    - Added some new .json objects,
    - Tried to add in jcfis.jar library but was not able to get gradle build script to work correctly. this is needed to potential have access programatically to EngStl1 to read and write directly. 	
	
	** 2/14/2018 ** Tom Rode **
	- Finally got both Truck and .json test case selection spinners to wok dynamically if these folders change.
	- Added the ground work .json keys for saving in-progress test cases and what point the test stopped at.
	- Added a check box in performtestcases UI. It will remain invisible if test are not finished the become visible once finished.
	- Changed gradle minSdkVersion from 14 to 19 may not need this. 
  
    ** 2/20/2018 ** Tom Rode **
	- Implemented all .json saving strategies for saving in-progress, Complete, and where test cases should resume from while in-progress.
	- Fixed ExternalWrite method so it writes output.json files complete keys.
	- changed Testime member with in TestCase.java class to a string from an integer.
	
	** 2/21/2018 ** Tom Rode **
	- Update UI for performtestcases according to Mikes 
    - Did clean-up in the code 
    - Added  TvDrawingRtcNumber.append(gVDNGiRTCNumber); to performtestcases .java
	
	** 2/23/2018 ** Tom Rode **
	- Successfully added lib jcif-1.3.19.jar to try SMB for accessing network drives but to no avail, Therefore The Import button in MainActivity.java and Export in Exporttestresults 
      Uses an Intent "Intent.ACTION_VIEW" an uses ESExplorer at package "com.estrongs.android.pop".	
    - Will have to clean out old SMB trial code. 
	
	** 2/26/2018 ** Tom Rode **
	- Added some robustness in case PIL_SYNECT_TEST_PLAN/Ready_for_Execution folder initially missing added the below toast to prompt user with in the Oncreate method in MainActivity .	
	  Toast.makeText(getBaseContext(), ( Root + "/PIL_SYNECT_TEST_PLAN/Ready_for_Execution" + "---> MISSING, IMPORT TEST CASE then Restart App" ), Toast.LENGTH_LONG).show();
	  
	** 2/27/2018 ** Tom Rode **
	- Did a build in release and made a key for the project, password is lift0076 
	- Did a little more house keeping on the code
	
	** 2/28/2018 ** Tom Rode **
	- Added in logic to lock out Next button in performtestcases Activity unless either Pas or Fail hit each time.
	- Added logic to check if TestRequest number has the proper formatting else if not it will not allow to call performtestcases Activity.
    - Investigating on tablet how to set up user profiles with limited access 	

	** 3/02/2018 ** Tom Rode **
	- Changed placement of MainActivity UI Load test plan and user as per Meeting in Celina.
	- Added in Battery S/N in to performtestcases Activity with this having the ability to change this.
    - Got Voice to text enabled on tablet.
    - Re-factored performtestcases UI to fit input text field.
	
	** 3/05/2018 ** Tom Rode **
	- Added in h1_hourmeter and odometer to performtestcases Activity and UI.
	
    *************** Version 1.1***********************  	
	** 3/06/2018 ** Tom Rode **
	- Added in Events Activity and UI as per Scott P.
	
	** 3/06/2018 ** Tom Rode **
	- Added Input Text to allow to write SW version on Main Activity.
	- app/build.gradle versionname "1.1r".
	
	*************** Version 1.2***********************  
	** 3/09/2018 ** Tom Rode **
	- Fixed issue with Performtestcases activity resetting if the device is moved which brings up keyboard to a input text view.
	- app/build.gradle versionname "1.2r".
	
	** 3/16/2018 ** Tom Rode **
	- Added documentation to Java files.
	
	** 3/13/2018 ** Tom Rode **
	- Fixed performtestcases Activity so it does not crash App if previous button goes all the way back to beginning.
	- Added Skip button and logic. 
	- Looking into adding flexibility to the amount of TestCase class variable using an array instead of a single instance. 
	
	** 3/14/2018 ** Tom Rode **
	- Fixed limitation with the amount of test cases by using TestCaseAr[2000].	
	
	** 3/15/2018 ** Tom Rode **
	- Added String dng_irtc; String synect_tc_num; String group_tc_num; to TestCase class
	
	** 3/15/2018 ** Tom Rode **
	- Intermediate delivery Synect name changes
	
	** 3/15/2018 ** Tom Rode **
	- Finished Synect name changes
	
	*************** Version 1.3***********************  
	** 4/16/2018 ** Tom Rode **
	- Added in at the test plan level "ExecutionGUID" added in at the test case level "TCxGUID".
	- Replaced "Group#" with "Name"                       
	- Formating Execution tuime to HH:MM:SS format
	
	** 4/17/2018 ** Tom Rode **
	- Completed Execution time to HH:MM:SS format
    - Changed over UI TvTextViewCount to new time format 

	** 4/24/2018 ** Tom Rode **
	- Added "Date" to TestCase class, and added method to return string as ("MM/dd/yyyy hh:mm:ss a") format
	
	** 5/3/2018 ** Tom Rode **
	- Updated build.gradle versionName "1.3r"
	
	************** Version 1.4***********************
    ** 5/3/2018 ** Tom Rode **	
	- Fixed mTimerUpTck1 so if the performtestcase activity gets reset (keyboard or device rotates) this value does not get lost.
	- Changed Check box TestCaseComplete to TestCaseComplete
	- Made ActiveFileName and SoftwareVersion UI text view clear out old value and append with new value with in MainActivity 
	- Change version on build.gradel script "1.4r"
	
	************** Version 1.5***********************
    ** 5/7/2018 ** Tom Rode **	
	- Changed writeExternalMessage()   // Key  "Application Software Version" to "Application Software Tested"
	- Change version on build.gradel script "1.5r"
	
	** 5/22/2018 ** Tom Rode **	
	- Changing FetchData.java to run SMB 

	************** Version 1.6 smb***********************
    ** 9/04/2018 ** Tom Rode **	
	- Integrated in SMB library "jNQ-1.0.1.R3355.GA.jar" for automatic file import/export to EngSTL1 
	- Change version on build.gradel script "1.6r smb"
	
	************** Version 1.7 smb***********************
    ** 9/06/2018 ** Tom Rode **	
	- Added logic to grey out Skip, Pass, Fail and highlight Next button Green between Test cases 
	
	** 9/07/2018 ** Tom Rode **	
	- Added logic to SkipAll test cases, this sets: TestStepCnt = Integer.parseInt(NumberOfTestCases);
	
	** 9/11/2018 ** Tom Rode **	
	- Added SendInProgressData.java to copy internal memory to ENGSTL1 In-Progress Folder
	
	** 9/13/2018 ** Tom Rode **	
	- Got logic to erase file out of ENGSTL1:\MANUAL_SYNECT_TEST_PLAN\Ready_for_Execution once second TC complete (not sure why Exception thrown if using first)
	  in SendInProgressData.java, logic will also update ENGSTL1 In-Progress Folder for a given file.
	
	** 9/14/2018 ** Tom Rode **	
	- Added static text in MainActivty for Importing and executing TestPlan, Added file Exist method (from SMB library) to determine in file there or not.
      Added new SendInProgressDatadelete.java which will remove a given file out of In-Progress in EngSTL1. Added Internal file for Credentials (Login , Pswd0.
	  Deleted UI for credential input in exporttestresult.xml 
	  
	** 9/19/2018 ** Tom Rode **	
	- Added to SendInProgressDatadelete.java a way to read external memory then copy to Completed_Execution. then removes this file from external memory.
      This change made the removal of a file in the Ready_for_Execution folder not work until app is closed for some reason	
	  
	** 9/20/2018 ** Tom Rode **
    - Broke out SendInProgressDatadelete.java file still not delete from Ready_for_execution still with a new Async task

	** 9/25/2018 ** Tom Rode **
	- Fixed issue with deleting file from Ready_for_execution in SendInProgressDatadelete.java
    - Added SOC, ApplicationSoftwareTested and Truck Serialnumber to be added to each test case
    - Cleaned out some commented out code
    - Added a check for gVTestInProgress.equals("No")  | gVTestInProgress.equals("")
    - Added UI value to  performtestcase.xml for SOC	
	
	************** Version 1.8 smb***********************
	- Added WAKE_LOCK Permissions in manifest, added keepscreenOn=true in activity_main.xml, added PowerManger Wakelock  and acquired it once onStop occurs in performtestcases.java
    - Added in onResume        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON); 
    - Added in onPause        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	- This helps but time is still lost once switched over to another activity and the display is set so low power mode occurs 
	
	** 10/2/2018 ** Tom Rode **
	- work on In_Progress from server to tablet 
	
	** 10/3/2018 ** Tom Rode **
	- Fixed shutting down app in perfortestcases and added button in Exporttestresults.
	- Added logic to grey out text in IMPORT buttons in MainActivity
	- Added logic in SendInProgressDelete to delete in internal file, java.io.File filedel2 = new java.io.File(( Root+ "/PIL_SYNECT_TEST_PLAN/In_Progress_From_Server/" + gVTruckPickedInSpinner + "/"  + gVFileToBeUsed));
    - Shifted over S/W Version in perfortestcases and MainActivity to fit Active File Name.
	
	************** Version 1.9 smb***********************
    ** 10/3/2018 ** Tom Rode **
	- Adding logic to Previous button to recall EtComments, EtSOC, EtH1Hourmeter, EtOdometer this will read internal file and place these keys accordingly.
    - TODO add a forward button to do the opposite.	
	
	** 10/19/2018 ** Tom Rode **
	- Logic to run previous and Forward button and save seems to be working
    - Added "Events" variable to TestCase class.
    - Fixed Truck SN and Batt SN ui in Main Activity	
	
	** 10/22/2018 ** Tom Rode **
	- Logic to output Pass,Fail,Skip buttons on Forward and Previous traversing. 
    - Added variable to TestCase.java "String pass_fail_skip_btn_state" this will hold what button was hit and not rely on Verdict.
	
	** 10/22/2018 ** Tom Rode **
	- Add lostkey function to rewrite missing keys back to .json files 
	
	** 10/26/2018 ** Tom Rode **
	- Orientation to landscape in MainActivity.
    - Made flow transition better at the end of test plan 
    - Changed Events to send out "Comments" 
    - Generated 1.9 beta smb to try 

	** 10/29/2018 ** Tom Rode **
	- Fixed events.java removed "Comments" from OutStringFromEvent as per fixes.
	- Added CbTestPlanComplete.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() did a check for ischecked() if check box is checked then allow Next button.
      This check is now independent of Next button.	
	- Locked out Forward button if  "if( Integer.parseInt(gVNumberOfExecutedTestCases) == TestStepCnt )" to not allow crash.

	** 10/30/2018 ** Tom Rode **
	- Added BatterySerailNumber to TestCase.java
	- Added logic to Next Button to show user if Previous or Forward was hit then hit the button again will rewrite and change number of executed steps.
    - Added	listener for SMB connection message which will launch if(s.toString().equals("Successful copied EngSTL1:Completed_Execution")) Exporttestresults if feedback occurs 

	** 10/31/2018 ** Tom Rode **
	- Added automatic request Permissions for Read and write External storage, No need for Storage inside of App Permissions
	- Added space after , in events to clean up string for readability.

	
    
  